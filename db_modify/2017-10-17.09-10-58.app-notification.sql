CREATE TABLE api_cloud_message
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  sender_id INT,
  sent_at INT,
  title VARCHAR(64),
  content VARCHAR(255),
  sound_on TINYINT,
  phone_number VARCHAR(16),
  cloud_message_token VARCHAR(255),
  status VARCHAR(16)
);
CREATE INDEX api_cloud_message_sender_id_index ON api_cloud_message (sender_id);
CREATE INDEX api_cloud_message_sent_at_index ON api_cloud_message (sent_at);
CREATE INDEX api_cloud_message_sound_on_index ON api_cloud_message (sound_on);
CREATE INDEX api_cloud_message_cloud_message_token_index ON api_cloud_message (cloud_message_token);
CREATE INDEX api_cloud_message_title_index ON api_cloud_message (title);
CREATE INDEX api_cloud_message_content_index ON api_cloud_message (content);
CREATE INDEX api_cloud_message_status_index ON api_cloud_message (status);

ALTER TABLE api_cloud_message ADD user_uuid VARCHAR(16) NULL;
CREATE INDEX api_cloud_message_user_uuid_index ON api_cloud_message (user_uuid);
ALTER TABLE api_cloud_message
  MODIFY COLUMN user_uuid VARCHAR(16) AFTER phone_number;

ALTER TABLE api_cloud_message ADD multicast_id VARCHAR(32) NULL;
ALTER TABLE api_cloud_message ADD message_id VARCHAR(64) NULL;

ALTER TABLE api_cloud_message ADD failure TINYINT DEFAULT 0 NOT NULL;
ALTER TABLE api_cloud_message CHANGE status success TINYINT NOT NULL DEFAULT 0;
ALTER TABLE api_cloud_message
  MODIFY COLUMN failure TINYINT NOT NULL DEFAULT 0 AFTER success;

ALTER TABLE api_cloud_message MODIFY sent_at DATETIME;

ALTER TABLE api_cloud_message CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE api_cloud_message ADD customer_number VARCHAR(16) NULL;
ALTER TABLE api_cloud_message
  MODIFY COLUMN customer_number VARCHAR(16) AFTER cloud_message_token;

ALTER TABLE api_cloud_message MODIFY customer_number VARCHAR(10);
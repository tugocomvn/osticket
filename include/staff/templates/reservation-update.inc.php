<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<form class="update_form" action="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php" method="post" id="<?php echo $tour['id'] ?>" target="hold_frame__<?php echo $tour['id'] ?>">
    <input type="hidden" name="tour_id" value="<?php echo $reservation->tour_id ?>">
    <input type="hidden" name="reservation_save" value="1">
    <input type="hidden" name="reservation_id" value="<?php echo $reservation->id ?>">
    <?php csrf_token() ?>
    <table class="update_table">
        <caption>Reservation Update</caption>
        <tr>
            <td>Quantity</td>
            <td>
                Hold <input type="number" class="input-field" max="40" min="0" step="1" name="hold_qty" value="<?php
                if (isset($reservation->hold_qty) && $reservation->hold_qty) echo $reservation->hold_qty ?>">

                - Sure
                <input type="number" class="input-field" max="40" min="0" step="1" name="sure_qty" value="<?php
                if (isset($reservation->sure_qty) && $reservation->sure_qty) echo $reservation->sure_qty ?>">
            </td>

            <td>Staff</td>
            <td>
                <select name="staff_id" id="">
                    <option value="0">- TUGO -</option>
                    <?php
                    if($users) {
                        foreach($users as $id => $name) {
                            $s = " ";
                            if ($id == $reservation->staff_id)
                                $s="selected";
                            echo sprintf('<option value="%s" %s>%s</option>', $id, $s, $name);
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>Customer Name</td>
            <td>
                <input type="text" class="input-field" name="customer_name" value="<?php
                if (isset($reservation->customer_name) && $reservation->customer_name) echo $reservation->customer_name ?>">
            </td>
            <td>Phone number</td>
            <td>
                <input type="text" class="input-field" name="phone_number" value="<?php
                if (isset($reservation->phone_number) && $reservation->phone_number) echo $reservation->phone_number ?>">
            </td>
        </tr>

        <tr>
            <td>Due date</td>
            <td>
                <input type="text" name="due" value="<?php
                if (!is_null($reservation->due_at) && $reservation->due_at != '0000-00-00 00:00:00' && strtotime($reservation->due_at))
                    echo date('d/m/Y', strtotime($reservation->due_at)); ?>" readonly>
            </td>
            <td>
                <?php if($country_list && $reservation->visa_only): ?>
                    Country
                <?php endif; ?>
            </td>
            <td>
                <?php
                if ($country_list && $reservation->visa_only) {
                    ?>
                    <select name="country" id="country" class="input-field">
                        <option value>- Select -</option>
                        <?php foreach($country_list as $_item): ?>
                            <option value="<?php echo $_item['id'] ?>"
                                    <?php if( $_item['id'] == $reservation->country) echo "selected"; ?>
                            ><?php echo $_item['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td>Other</td>
            <td>
                <?php if (isset($reservation->visa_only) && $reservation->visa_only): ?>
                    <label class="label label-default" for=""><input name="visa_only" value="1" type="checkbox" onclick="return false;" checked> Visa Only</label>
                <?php else: ?>
                    <label class="label label-default" for=""><input name="fe" value="1" type="checkbox" <?php if (isset($reservation->fe) && $reservation->fe)
                            echo 'checked';?>> FE</label>
                    <label class="label label-default" for=""><input name="security_deposit" value="1" type="checkbox" <?php if (isset($reservation->security_deposit) && $reservation->security_deposit)
                            echo 'checked';?>> Security Deposit</label>
                    <label class="label label-default" for=""><input name="visa_ready" value="1" type="checkbox" <?php if (isset($reservation->visa_ready) && $reservation->visa_ready)
                            echo 'checked';?>> Visa Ready</label>
                    <label class="label label-default" for=""><input name="one_way" value="1" type="checkbox" <?php if (isset($reservation->one_way) && $reservation->one_way)
                            echo 'checked';?>> One way</label>
                <?php endif; ?>
            </td>
            <td>Infant</td>
            <td>
                <input type="number" step="1" min="0" max="10" name="infant" class="input-field" value="<?php
                if (isset($reservation->infant) && $reservation->infant) echo $reservation->infant ?>">
            </td>
        </tr>
        <tr>
            <td>Note</td>
            <td>
                <textarea name="note" id="" cols="30" rows="10" class="input-field"><?php
                    if (isset($reservation->note) && $reservation->note) echo trim($reservation->note) ?></textarea>
            </td>
            <td>Booking Code</td>
            <td>
                <input type="text" name="booking_code" value="<?php echo $reservation->booking_code ?>" class="input-field"
                <?php if((int)$reservation->status !== 0) echo 'readonly'?>>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button class="btn_sm btn-danger cancel_btn" name="action" value="sure" type="button">Cancel</button>
            </td>
            <td colspan="2">
                <button class="btn_sm btn-primary save_btn" name="action" value="hold" type="submit">Save</button>

            </td>
        </tr>
    </table>
</form>
<iframe class="hold_frame" src="" frameborder="0" name="hold_frame__<?php echo $tour['id'] ?>">

</iframe>

<script>
    (function($) {
        $('.update_form').parents('table').find('tr td').css('cursor', 'not-allowed');
        $('.update_form').parents('tr').find('td').css('cursor', 'default');
        $('.update_form').find('tr td').css('cursor', 'default');

        $('.dp_due').datepicker({
            changeYear: true,
            yearRange: "<?php echo date('Y') ?>:<?php echo intval(date('Y'))+1 ?>",
            shortYearCutoff: 50,
            showButtonPanel: true,
            selectOtherMonths: false,
            numberOfMonths: 1,
            minDate: new Date(<?php echo (time())*1000 ?>),
            maxDate: new Date(<?php echo (time() + 4*24*3600)*1000 ?>),
            dateFormat: 'dd/mm/yy'
        });

        $('.cancel_btn').off('click').on('click', function(e) {
            _self = $(e.target);
            _self.parents('form').parents('tr').remove();
            $('.edit_btn').show();
        });

        $('.save_btn').off('click').on('click', function(e) {
            if (!confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {
                e.preventDefault();
                return false;
            }
        });
    })(jQuery);
</script>
<?php
/**
 * Created by PhpStorm.
 * User: cosmos
 * Date: 7/11/16
 * Time: 10:37 AM
 */

/**
 * Class _String
 * String manipulation library
 */
include_once INCLUDE_DIR.'class.format.php';

class _String {
    const UNIVERSAL_PHONE_NUMBER_REGEX = '(?<=[^0-9]|^)(((\+?84|[0oO][\.\- ]?)?(([3456789]([\.\-\s])?([0-9]([\.\-\s])?){8})))|((\+?84|[0oO][\.\- ]?)?1[\.\- ]?[2689][\.\- ]?([0-9][\.\- ]?){7}[0-9]))(?=[^0-9]|$)';
    const UNIVERSAL_PHONE_NUMBER_REGEX_MYSQL = '((([+]?84|[0oO]([-. ])?)?(([3456789]([-. ])?([0-9]([-. ])?){8})))|(([+]?84|[0oO]([-. ])?)?1[-. ]?[2689][-. ]?([0-9][-. ]?){7}[0-9]))';
    const UNIVERSAL_EMAIL_REGEX = '([^a-zA-Z0-9]|^)([a-zA-Z0-9][a-zA-Z0-9\-\_\.]+[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9\-\_\.]+[a-zA-Z0-9])([^a-zA-Z0-9]|$)';
    const UNIVERSAL_EMAIL_REGEX_MYSQL = '([^a-zA-Z0-9]|^)([a-zA-Z0-9][-a-zA-Z0-9_.]+[a-zA-Z0-9]@[a-zA-Z0-9][-a-zA-Z0-9_.]+[a-zA-Z0-9])([^a-zA-Z0-9]|$)';

    static $prefix = [
        "0123" => "083",
        "0124" => "084",
        "0125" => "085",
        "0127" => "081",
        "0129" => "082",
        "0120" => "070",
        "0121" => "079",
        "0122" => "077",
        "0126" => "076",
        "0128" => "078",
        "0162" => "032",
        "0163" => "033",
        "0164" => "034",
        "0165" => "035",
        "0166" => "036",
        "0167" => "037",
        "0168" => "038",
        "0169" => "039",
        "0186" => "056",
        "0188" => "058",
        "0199" => "059",
    ];

    // (?<=[^0-9]|^)((([0oO][\\.\\- ]?)?[^2][\\.\\- ]?([0-9][\\.\\- ]?){7}[0-9])|(([0oO][\\.\\- ]?)?1[\\.\\- ]?[2689][\\.\\- ]?([0-9][\\.\\- ]?){7}[0-9]))(?=[^0-9]|$)
    public static function removeInvisibleCharacters($string) {
        $badchar = array(
            // control characters
            chr(0), chr(1), chr(2), chr(3), chr(4), chr(5), chr(6), chr(7), chr(8), chr(9), chr(10),
            chr(11), chr(12), chr(13), chr(14), chr(15), chr(16), chr(17), chr(18), chr(19), chr(20),
            chr(21), chr(22), chr(23), chr(24), chr(25), chr(26), chr(27), chr(28), chr(29), chr(30),
            chr(31),
            // non-printing characters
            chr(127)
        );

        return str_replace($badchar, '', $string);
    }

    public static function removeInvalidCharactersFromEmail($email) {
        return filter_var($email, FILTER_SANITIZE_EMAIL);
    }

    public static function khongdau($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
        $str = preg_replace("/(đ)/", "d", $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
        $str = preg_replace("/(Đ)/", "D", $str);
        return $str;
    }

    public static function getEmails($string, $from)
    {
        $from = $from ? "From" : "To";
        $email = [];
        if (
            // case 1 // To: =?utf-8?Q?Nguy=E1=BB=85n_Th=E1=BB=8B_Th=E1=BA=A3o_Vy?=
            // <anhvy2001@yahoo.com> > 2
            preg_match(sprintf('![^-]%s:("?.*"?)?\s*<\s*(.*?)\s*>!', $from), $string, $email)
            && isset($email[2])
            && filter_var(trim($email[2]), FILTER_VALIDATE_EMAIL)
        ) return trim($email[2]);

        elseif (
            // case 2 // To:"Bao Nguyen" <test@mail.buu.vn> > 3
            preg_match(sprintf("/[^-]%s:(\"(.)+\"\ )?\<?((.)+@(.)+)\>/", $from), $string, $email)
            && isset($email[3])
            && filter_var(trim($email[3]), FILTER_VALIDATE_EMAIL)
        ) return trim($email[3]);

        elseif (
            // case 3 // "To:phamquocbuu121212121@gmail.com" > 2
            preg_match(sprintf("/[^-]%s:(\ )*((.)+@(.)+)/", $from), $string, $email)
            && isset($email[2])
            && filter_var(trim($email[2]), FILTER_VALIDATE_EMAIL)
        ) return trim($email[2]);

        elseif (
            // case 4 // To: =?utf-8?Q?Nguy=E1=BB=85n_Th=E1=BB=8B_Th=E1=BA=A3o_Vy?=
            // <anhvy2001@yahoo.com> > 4
            preg_match(sprintf("/[^-]%s:(\"(.)+\"\ )?(.)+\<((.)+@(.)+)\>/", $from), $string, $email)
            && isset($email[4])
            && filter_var(trim($email[4]), FILTER_VALIDATE_EMAIL)
        ) return trim($email[4]);

        return false;
    }

    public static function getFromEmail($string)
    {
        return self::getEmails($string, true);
    }

    public static function getToEmail($string)
    {
        return self::getEmails($string, false);
    }

    public static function getEmailsFromString($string) {
        $matches = [];
        preg_match_all('/'.static::UNIVERSAL_EMAIL_REGEX.'/', $string, $matches);
        if (!$matches || !isset($matches[2]) || !is_array($matches[2])) return [];

        $res = $matches[2];
        foreach ($res as $index => $email) {
            $res[ $index ] = strtolower($email);
        }

        return array_filter(array_unique($res));
    }

    public static function getBoundary($string)
    {
        $check = [];
        preg_match_all('/boundary\=\"((.)+)\"/', $string, $check);
        if (isset($check[1][0])) return trim($check[1][0]);
        return false;
    }

    public static function getQuoteMsg($string)
    {
        $boundary = self::getBoundary($string);
        if (!$boundary) return false;
        $text_arr = explode($boundary, $string);
        if (isset($text_arr[3]) && ($text_arr = explode("Content-Type: text/html; charset=utf-8", $text_arr[3]))) {
            if (!isset($text_arr[1])) return false;
            return base64_decode($text_arr[1]);
        }

        if (isset($text_arr[2]) && ($text_arr = explode("Content-Type: text/plain; charset=utf-8", $text_arr[2]))) {
            if (!isset($text_arr[1])) return false;
            return base64_decode($text_arr[1]);
        }

        return false;
    }

    public static function str_replace_first($from, $to, $subject)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $subject, 1);
    }

    public static function json_decode($string) {
        if ($string && ((is_string($string) && is_object(json_decode($string))) || is_array($string))) {
            $value_arr = is_array($string) ? $string : (Array)json_decode($string);
            reset($value_arr);
            return current($value_arr);
        }
        return $string;
    }

    public static function abbrev($string) {
        $string = self::khongdau($string);
        $string = preg_replace("/[\r\n ]+/", "", $string);
        return $string;
    }

    public static function smsCharFilter($string) {
        $new_string = '';
        $allow = [];
        for ($i = 32; $i <= 126; $i++) {
            $allow[] = $i;
        }
        $allow[] = 10;
        $allow[] = 13;

        $strLen = strlen($string);
        for ($i = 0; $i < $strLen; $i++) {
            if (in_array(ord($string[$i]), $allow))
                $new_string .= $string[$i];
        }

        return $new_string;
    }

    public static function isMobileNumber($number = '') {
        if (!$number) return false;
        return count(self::getPhoneNumbers($number)) ? true : false;
    }

    public static function isEmailAddress($email = '') {
        if (!$email) return false;
        return count(self::getEmailsFromString($email)) ? true : false;
    }

    public static function getPhoneNumbers($text) {
        $matches = [];
        preg_match_all('/'.self::UNIVERSAL_PHONE_NUMBER_REGEX.'/', $text, $matches);

        if (!isset($matches[0]))
            return [];

        $phones = [];
        foreach ($matches[0] as $_key => $_val)
            $phones[] = self::formatPhoneNumber($_val);

        return $phones;
    }

    public static function splitAndNormalize($string) { // for Facebook offline data
        $phone_list = explode('/', $string);
        foreach ($phone_list as $index => $phone) {
            $phone_list[ $index ] = self::formatPhoneNumber($phone, true);
            if (empty(trim($phone_list[ $index ])) || strlen($phone_list[ $index ]) < 10) {
                unset($phone_list[ $index ]);
            }
            if (empty(trim($phone_list[ $index ])) || strlen($phone_list[ $index ]) < 10) {
                $phone_list[ $index ] = substr($phone_list[ $index ], 0, 10);
            }
        }

        if (is_array($phone_list) && $phone_list)
            return array_unique(array_filter($phone_list));

        return null;
    }

    public static function simpleCheckMobilePhone(&$number) {
        $number = self::formatPhoneNumber($number);

        if (preg_match('/^0[^2]/', $number) && strlen($number) == 10)
            return true;

        return false;
    }

    public static function formatPhoneNumber($number, $country_code = false) {
        $number = preg_replace('/^(\+)?84/', '0', $number);
        $number = preg_replace('/[^0-9]/', '', $number);
        if (!preg_match('/^0/', $number))
            $number = '0'.$number;

        if(strlen($number) > 10)
            $number = substr($number, 0, 10);

        if ($country_code)
            $number = preg_replace('/^0/', '84', $number);

        return $number;
    }

    public static function formatPhoneNumberSMS($phone_number) {
        return static::formatPhoneNumber($phone_number, true);
    }

    public static function convertMobilePhoneNumber($ori, $to_new = false) {
        $phone_number = self::formatPhoneNumber($ori);
        $prefix = substr($phone_number, 0, 4);

        if (!isset(static::$prefix[ $prefix ]) && !$to_new) {
            $prefix = substr($phone_number, 0, 3);
            $new_prefix = array_search($prefix, static::$prefix);
            if ($new_prefix === false) return $ori;

            return $new_prefix.substr($phone_number, 3);
        } else {
            $new_prefix = static::$prefix[ $prefix ];
            return $new_prefix.substr($phone_number, 4);
        }

    }

    public static function time_elapsed_string($datetime, $full = false) {
        $dbtime = !is_numeric($datetime) ? $datetime : date('Y-m-d H:i:s', $datetime);
        $now = new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh'));
        $ago = new DateTime($dbtime, new DateTimeZone('Asia/Ho_Chi_Minh'));
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function getFirstElementValue($json_string) {
        $arr =  json_decode($json_string, true);
        $item = array_shift($arr);
        return $item;
    }

    public static function getIp() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;

    }

    public static function trimBookingCode($code) {
        $code = str_replace(BOOKING_CODE_PREFIX_SHORT, '', $code);
        $code = str_replace(BOOKING_CODE_PREFIX, '', $code);
        $code = ltrim($code, '0');
        $code = trim($code);
        return $code;
    }

    public static function formatBookingCode($code) {
        return 'TG123-'.static::trimBookingCode($code);
    }

    public static function generateRandomString($length = 10, $allow_uppercase = false) {
        $x = $allow_uppercase
            ? '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
            : '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(
            str_shuffle(
                str_repeat(
                    $x, ceil($length / strlen($x))
                )
            ), 1, $length
        );
    }

    public static function getUrl($text) {
        $pattern = "%(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?%iu";
        $matches = [];
        preg_match_all($pattern, $text, $matches);
        if (!$matches || !is_array($matches)
            || !isset($matches[0]) || !is_array($matches[0])) return null;
        $matches = array_filter($matches[0]);
        if (!$matches || !is_array($matches)) return null;
        return $matches;
    }

    public static function putParamToUrl($text, $urls, $param, $shorten = false) {
        if (!$urls) return $text;

        foreach($urls as $url) {
            if (empty(trim($url))) continue;

            $old = $url;
            if (strpos($url, '?') !== false) {
                $url .= '&'.$param;
            } else {
                $url .= '?'.$param;
            }

            if ($shorten)
                $url = static::shortenUrl($url);

            $text = str_replace($old, $url, $text);
        }

        return $text;
    }

    public static function shortenUrl($url) {
        return file_get_contents(URL_SHORTENER.'?format=text&url='.urlencode($url));
    }

    public static function spaceToUnderscore($string) {
        if (( !is_string($string) && !is_numeric($string) ) || empty(trim($string))) return "";
        $string = mb_strtolower(trim($string));
        return implode('_', explode(' ', static::khongdau($string)));
    }
}

class _Format {
    public static function db_short_datetime($time) {
        $diff = ((time() - Misc::db2gmtime($time))/(3600*24))/300 > 1;
        if ($diff)
            return Format::userdate('Y M d, h:i:s A', Misc::db2gmtime($time));
        else
            return Format::userdate('M d, h:i:s A', Misc::db2gmtime($time));
    }

    public static function dateInSameYear($from, $to, $from_format = 'H:i, d/m', $to_format = 'd/m/Y', $connector = '&#8594;') {
        if ($to) {
            if (date('Y', strtotime($from)) === date('Y', strtotime($to))) {
                echo date($from_format, strtotime($from)).' '.$connector.' '.date($to_format, strtotime($to));
            } else {
                echo date('H:i d/m/Y', strtotime($from)).' '.$connector.' '.date($to_format, strtotime($to));
            }
        } else {
            if (date('Y', strtotime($from)) === date('Y')) {
                echo date($from_format, strtotime($from));
            } else {
                echo date('H:i d/m/Y', strtotime($from));
            }
        }

    }
}

class _Date {
    public static function isUnixTimeStamp($timestamp) {
        return (
            ((string) (int) $timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX)
        );
    }

    public static function isInYearTimeStamp($timestamp) {
        return self::isUnixTimeStamp($timestamp) && (abs(time()-$timestamp) < 365*24*3600);
    }
}

class _UUID {
    private static $cookie_name = '__my_ost_uuid';
    private static $expire = 365; // days

    public static function generate() {
        return md5(
            uniqid(self::$cookie_name, true)
            .time()
            .microtime(true)
            .rand(1, rand(100, 99999))
        );
    }

    public static function getName() {
        return self::$cookie_name;
    }

    public static function setName($name) {
        self::$cookie_name = $name;
    }

    public static function getExpiredTime() {
        return self::$expire;
    }

    public static function setCookie($uuid) {
        if (!self::getCookie())
            setcookie(
                self::$cookie_name,
                $uuid,
                time()+self::$expire,
                '/'
            );
    }

    public static function getCookie() {
        if (isset($_COOKIE[self::$cookie_name]) && $_COOKIE[self::$cookie_name])
            return $_COOKIE[self::$cookie_name];
        return null;
    }
}

create table mkt_wifi
(
  id int auto_increment,
  macAddress varchar(64) null,
  timeline varchar(8) null,
  eventLabel varchar(64) null,
  placeId varchar(32) null,
  year smallint null,
  month tinyint null,
  day tinyint null,
  constraint mkt_wifi_pk
    primary key (id)
);

create index mkt_wifi_day_index
  on mkt_wifi (day);

create index mkt_wifi_eventLabel_index
  on mkt_wifi (eventLabel);

create index mkt_wifi_macAddress_index
  on mkt_wifi (macAddress);

create index mkt_wifi_month_index
  on mkt_wifi (month);

create index mkt_wifi_placeId_index
  on mkt_wifi (placeId);

create index mkt_wifi_year_index
  on mkt_wifi (year);

create table mkt_wifi_eventobject
(
  id int auto_increment,
  wifi_id int null,
  name varchar(64) null,
  email varchar(128) null,
  phone_number varchar(16) null,
  constraint mkt_wifi_eventobject_pk
    primary key (id)
);

create index mkt_wifi_eventobject_phone_number_index
  on mkt_wifi_eventobject (phone_number);

create index mkt_wifi_eventobject_wifi_id_index
  on mkt_wifi_eventobject (wifi_id);

alter table mkt_wifi
  add created_at datetime null;

alter table mkt_wifi_eventobject
  add created_at datetime null;

alter table mkt_wifi_eventobject
  add voucher_id int null;

create index mkt_wifi_eventobject_voucher_id_index
  on mkt_wifi_eventobject (voucher_id);

alter table mkt_wifi_eventobject
  add voucher_time datetime null;


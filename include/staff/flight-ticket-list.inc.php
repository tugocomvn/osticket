<?php 
if (!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<style>
    td br {
        margin-bottom: 0.5em;
        display: block;
        content: "";
    }

    table.list tbody td, table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }

    table.list {
        margin-left: -100px;
    }
</style>
<h2><?php echo __('Flight Ticket List');?></h2>
<div class="clearfix"></div>
    <form action="<?php echo $cfg->getUrl() ?>scp/flight-ticket-list.php" method="get">
        <div style="padding-left:2px;">
            <table>
                <tr>
                    <td>Mã vé</td>
                    <td><input type="text" class="input-field" name="flight_ticket_code"
                            value="<?php if(isset($_REQUEST['flight_ticket_code']))
                                echo $_REQUEST['flight_ticket_code'] ?>"></td>
                </tr>
                <tr>
                    <td>Hãng hàng không</td>
                    <td>
                        <select name="airline" id="airline" class="input-field" style="height:30px">
                        <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                            <?php while ($airlines_list && ($row = db_fetch_array($airlines_list))){
                                echo sprintf('<option value="%d" %s>%s</option>',
                                $row['id'],($row['id']?'"selected"':''),$row['airline_name']);
                            }?>
                        </select>
                    </td>
                    <td>Số hiệu chuyến bay</td>
                    <td><input type="text" class="input-field" name="flight_code"
                            value="<?php if(isset($_REQUEST['flight_code']))
                                echo $_REQUEST['flight_code'] ?>"></td>
                </tr>
                <tr>
                    <td>Ngày bay đi</td>
                    <td><input class="dp input-field" size=15 name="departure_at"
                            value="<?php if (strtotime($_REQUEST['departure_at']))
                                echo date('d/m/Y', strtotime($_REQUEST['departure_at'])); ?>"
                            autocomplete=OFF></td>
                    <td>Ngày bay về</td>
                    <td><input class="dp input-field" size=15 name="arrival_at"
                            value="<?php if (strtotime($_REQUEST['arrival_at']))
                                echo date('d/m/Y', strtotime($_REQUEST['arrival_at'])); ?>"
                            autocomplete=OFF></td>
                </tr>
                <tr>
                    <td>Số lượng vé</td>
                    <td><input type="text" class="input-field" name="total_quantity"
                            value="<?php if(isset($_REQUEST['total_quantity']))
                                echo $_REQUEST['total_quantity'] ?>"></td>
                    <td>Trạng Thái</td>
                </tr>
                <tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" class="btn_sm btn-primary"
                            value="<?php echo __('Search');?>" />
                        <input type="reset" class="btn_sm btn-default"
                            value="<?php echo __('Reset');?>"
                            onclick="reset_form(this)" /></td>
                </tr>
                <script>
                    function reset_form(event) {
                        console.log(event);
                        form = $(event).parents('form');
                        form.find('input[type="text"], select, .hasDatepicker').val('');
                        form.submit();
                    }
                </script>
            </table>
        </div>
    </form>
    <table class="list" border="0" cellspacing="1" cellpadding="2" width="1258">
        <caption><?php echo $pageNav->showing() ?></caption>
        <thead>
            <tr>
                <th>No.</th>
                <th>Mã Vé</th>
                <th>Hành trình</th>
                <th>Số hiệu chuyến bay</th>
                <th>Ngày bay đi</th>
                <th>Ngày bay về</th>
                <th>Hãng hàng không</th>
                <th>Số lượng vé</th>
                <th>Trạng Thái</th>
            </tr>
        </thead>
        <tbody>
        <?php $no = $offset; ?>
            <?php while($results && ($row = db_fetch_array($results))): ?>
                <tr>
                    <td><?php echo ++$offset; ?></td>
                    <td>
                        <?php if(isset($row['flight_ticket_code'])) echo $row['flight_ticket_code'] ?>
                    </td>
                    <td>
                        <?php if(isset($row['airport_code_from']) && isset($row['airport_code_to'])) echo $row['airport_code_from']."-".$row['airport_code_to']?>
                    </td>
                    <td>
                        <?php if(isset($row['flight_code'])) echo $row['flight_code'] ?>
                    </td>
                    <td>
                        <?php if(isset($row['departure_at'])) echo date('H:i d/m/Y', strtotime($row['departure_at'])) ?>
                    </td>
                    <td>
                        <?php if(isset($row['arrival_at'])) echo date('H:i d/m/Y', strtotime($row['arrival_at'])) ?>
                    </td>
                    <td>
                        <?php if(isset($row['airline_id'])) echo Flight::getNameAirline($row['airline_id']) ?>
                    </td>
                    <td>
                        <?php if(isset($row['total_quantity'])) echo $row['total_quantity'] ?>
                    </td>
                    <td>
                    </td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
<div> Page:
    <?php echo $pageNav->getPageLinks(); ?>
</div>;
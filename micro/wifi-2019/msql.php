<?php

function createConnect($servername, $username, $password)
{
// Create connection
    global $conn;

    $conn = new mysqli($servername, $username, $password);

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}
function connectDatabase($servername, $username, $password,$dbname)
{
    global $conn;

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);

    }
}


function closeDatabase(){
    global $conn;
    $conn->close();
}




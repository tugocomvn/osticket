<?php
$callin_type_by_day = TicketAnalytics::callAnalytics($_REQUEST['from'], $_REQUEST['to'], 'callin_type_by_day', $_REQUEST);
$callin_type_by_day_arr = [];
$dates_source = [];
while($callin_type_by_day && ($row = db_fetch_array($callin_type_by_day))) {
    $type = _String::json_decode($row['type']);
    if (!isset($callin_type_by_day_arr[ $type ]))
        $callin_type_by_day_arr[ $type ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $dates_source[] = $row['week'].'-'.$row['year'];
            $callin_type_by_day_arr[ $type ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $dates_source[] = $row['month'].'-'.$row['year'];
            $callin_type_by_day_arr[ $type ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $callin_type_by_day_arr[ $type ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}

$dates_source = array_filter(array_unique($dates_source));

foreach ($callin_type_by_day_arr as $_type => $_date_data) {
    foreach ($dates_source as $_date) {
        if (!isset($callin_type_by_day_arr[ $_type ][ $_date ]))
            $callin_type_by_day_arr[ $_type ][ $_date ] = 0;
    }

    uksort($callin_type_by_day_arr[ $_type ], "___date_compare");
}
?>
<td colspan="">
    <h2>Call IN by Day</h2>
    <canvas id="call_in" width="500" height="300"></canvas>
</td>
<script>
    var call_in_elm = document.getElementById("call_in").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(<?php echo count($callin_type_by_day_arr) ?>);
    var call_in = new Chart(call_in_elm, {
        type: 'line',
        data: {
            datasets: [
                <?php foreach($callin_type_by_day_arr as $_type => $_date): if ($i>10) break; ?>
                {
                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                    borderColor:  colors[<?php echo $i++ ?>],
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    showLine: <?php if ($i>5) echo 'false'; else echo 'true'; ?>,
                    fill: false
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>

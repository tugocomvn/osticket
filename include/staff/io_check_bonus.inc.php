<?php
require('staff.inc.php');
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canCreatePayments()) die('Access Denied');
?>

<style>
    textarea {
        width: 50%;
        min-width: 25%;
        height: 20em;
        resize: none;
        margin-left: 25%;
        margin-top: 2em;
        font-size: 14px;
    }

    form {
        width: 100%;
    }

    td {
        height: 1.5em;
    }
</style>
<p>
    <a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/report_commission.php?action=list_report"?>">Back</a>
</p>
<h2>Kiểm tra hoa hồng của <span><?php echo $allStaff[$timeReport[$id_report][1]].'-'.$timeReport[$id_report][0] ?></span></h2>
<form  action="<?php  echo $cfg->getUrl()."scp/report_commission.php?action=check_booking"?>" method="post" >
    <?php csrf_token(); ?>
    <input type="hidden" name="id" value="<?php echo $id_report; ?>">
    <?php if ($form_check):?>
        <textarea name="code"></textarea>
        <p class="centered">Điền mã booking vào ô bên trên. Mỗi mã là 1 dòng. Sau đó bấm Kiếm tra.</p>
        <p class="centered">
            <button type="submit" style="display: block;margin: auto;" class="btn_sm btn-primary" name="check" value="check" >Kiểm tra</button>
        </p>
    <?php endif; ?>

<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption>Bảng báo cáo</caption>
    <thead>
    <tr>
        <th scope="col">STT</th>
        <th scope="col">Mã booking</th>
        <th scope="col">Nhân viên xử lí</th>
        <th scope="col">Tổng doanh thu</th>
        <th scope="col">Số lượng khách</th>
        <th scope="col"">Ghi chú</th>
        <th scope="col">Hiện trạng</th>
    </tr>
    </thead>
    <tbody>
    <?php $has_error =false;
        $report_available = 0;
        if(empty($code) or (isset($_REQUEST['save']) and 'save' === $_REQUEST['save'])):
            $i = 1;
        foreach ($results as $data):
        $link = $cfg->getUrl() . "scp/report_commission.php?action=check_booking&id=".$data['report_id'];
        ?>
        <tr>
            <td><?php echo $i++ ?></td>
            <td><?php echo $data['booking_code'] ?></td>
            <td><?php echo (isset($data['staff'])?$data['staff']:$allStaff[$data['user_id']]) ?></td>
            <td><?php echo number_format(floatval($data['total_retail_price']), 0, ',', '.') ?></td>
            <td><?php echo $data['total_quantity'] ?></td>
            <td>
                <?php  $note = 'note_'.$data['booking_code'];
                if(isset($data['report_id']) && (int)$data['report_id'] <> $id_report): ?>
                    <input type="text" value="<?php echo $data['note']?>" readonly/>
                <?php else: ?>
                    <input type="text" name="<?php echo $note ?>" value="<?php echo $data['note']?>"/>
                <?php endif; ?>
            </td>

            <td>
                <?php if(isset($data['report_id']) && ((int)$data['report_id'] <> $id_report)): ?>

                    <a href="<?php echo $link; ?>" class="label label-danger">
                        <?php echo 'Đã có trong báo cáo tháng '.$timeReport[$data['report_id']][0]; ?>
                    </a>
                <?php else:if(!isset($data['report_id'])):?>
                    <span class="label label-success">Báo cáo mới</span>
                <?php endif;endif?>
            </td>
        </tr>
        <?php endforeach;
   else: ?>

    <?php  $i=1; foreach ($code as $key => $code_booking):
        $has= false;
        $not_coincide = true;
        for ($j = $key+1; $j < count($code); $j++):
            //Kiem tra ma booking trùng nhau
            $code_booking_trim = convert_booking_code_trim($code_booking);
            $code[$j] = convert_booking_code_trim($code[$j]);
            if ($code_booking_trim === $code[$j]):
                $has_error = true;
                $not_coincide = false; ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $code_booking ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><span  class="label label-info">Mã bị trùng trong danh sách nhập</span></td>
            </tr>
        <?php break; endif;endfor;?>

        <!-- có ma booking trong co do du lieu -->
        <?php if($not_coincide):
            foreach ($results as $data):
                if(convert_booking_code_trim($code_booking) === convert_booking_code_trim($data['booking_code'])):
                $has = true;
                $link = $cfg->getUrl() . "scp/report_commission.php?action=check_booking&id=".$data['report_id'];
                ?>
                <tr>
                    <td><?php echo $i++ ?></td>
                    <td><?php echo $code_booking ?></td>
                    <td><?php echo (isset($data['staff'])?$data['staff']:$allStaff[$data['user_id']]) ?></td>
                    <td><?php echo $data['total_retail_price'] ?></td>
                    <td><?php echo $data['total_quantity'] ?></td>
                    <td>
                        <?php  $note = 'note_'.$data['booking_code'];
                        if(isset($data['report_id']) && (int)$data['report_id'] <> $id_report): ?>
                        <input type="text" value="<?php echo $data['note']?>" readonly/>
                        <?php else: ?>
                        <input type="text" name="<?php echo $note ?>" value="<?php echo $data['note']?>"/>
                        <?php endif; ?>
                    </td>

                    <td>
                        <?php if(isset($data['report_id']) && ((int)$data['report_id'] <> $id_report)):
                            $report_available += 1?>

                        <a href="<?php echo $link; ?>" class="label label-danger">
                            <?php echo 'Đã có trong báo cáo tháng '.$timeReport[$data['report_id']][0]; ?>
                        </a>
                        <?php else:if(!isset($data['report_id'])):?>
                        <span class="label label-success">Báo cáo mới</span>
                        <?php endif;endif;?>
                    </td>
                </tr>
                <?php break;endif;
            endforeach; ?>

        <!-- ma booking khong ton tai trong co so du lieu-->
        <?php if($has === false):
           $has_error = true?>
        <tr>
            <td><?php echo $i++ ?></td>
            <td><?php echo $code_booking ?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><span  class="label label-default">Mã booking không tồn tại</span></td>
        </tr>
        <?php endif; endif ?>

    <?php endforeach; endif; ;?>
    <?php
    if($has_error or ($report_available >= 1 and ($report_available === count($code))) or (empty($results))):
        //$report_available >= 1 and ($report_available === count($code) : no booking new
        //delete booking_code in session
        unset($_SESSION['code']);
    else:?>
    <p class="centered">
        <button type="submit"  class="btn_sm btn-primary align-center" name="save" value="save">Save</button>
    </p>
    <?php endif;?>
    </tbody>
    <?php
//    if($res && $num): //Show options..
//        echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
//    endif;
    ?>
</table>
</form>
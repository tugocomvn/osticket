<?php
/**
 * Created by PhpStorm.
 * User: cosmos
 * Date: 7/20/16
 * Time: 9:41 AM
 */
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.download.php');

$path = trim(strval($_GET['path']));
$name = trim(strval($_GET['name']));
$hash = trim(strval($_GET['hash']));

if (Download::verifyPath($path, $name, $hash))
    Download::file($path, $name);
else
    Http::response(403, __('Access denied'));

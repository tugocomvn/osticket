<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('admin.inc.php');
require_once(INCLUDE_DIR.'class.facebook.php');       // For paper sizes
$fb_audience_file_name = null;
$fb_audience_file = null;
$count = 0;
if($_REQUEST && isset($_REQUEST['do'])){
    switch(strtolower($_REQUEST['do'])){
        case 'mass_process':
            $errors = [];
            $set = isset($_REQUEST['audience_set']) && $_REQUEST['audience_set'] ? $_REQUEST['audience_set'] : null;
            $source = isset($_REQUEST['source']) && $_REQUEST['source'] ? $_REQUEST['source'] : null;
            if ($set && $source)
                $count = OST_Facebook::PushDataToFB($source, $set, $errors);
            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
}

$page='facebook.inc.php';
$nav->setTabActive('facebook');
$ost->addExtraHeader('<meta name="tip-namespace" content="facebook.custom_audience" />',
    "$('#content').data('tipNamespace', 'facebook.custom_audience');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


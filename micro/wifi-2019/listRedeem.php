<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>List Redeem</title>
</head>
<body>
<div class="container bg-light" style="width: 80%">
    <?php include_once __DIR__.'/header.php'; ?>
    <table class="table table-striped">
        <h3 class="my-5 text-center text-info">DANH SÁCH KHÁCH ĐÃ KÍCH HOẠT</h3>
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Tên khách</th>
            <th scope="col">Số điện thoại</th>
            <th scope="col">Mã voucher</th>
            <th scope="col">Thời gian kích hoạt</th>
        </tr>
        </thead>
        <tbody>
        <?php if(isset($results)): foreach($results as $data):?>
            <tr>
                <td><?php echo ++$offset ?></td>
                <td><?php echo $data['customer_name'] ?></td>
                <td><?php echo $data['phone_number'] ?></td>
                <td><?php echo $data['code_voucher'] ?></td>
                <td><?php echo date('d/m/Y H:i:s',strtotime($data['date_redeem'])) ?></td>
            </tr>
        <?php endforeach; endif;?>
        </tbody>
    </table>
</div>
<?php $page = new Pagination($config);
echo $page->getPagination(); ?>

</body>
</html>

ALTER TABLE ost_groups ADD can_export_booking TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_change_booking_status TINYINT(1) DEFAULT 0 NULL;
<?php
/*********************************************************************
    visa-tour-list.php

    Emails

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');
require_once(INCLUDE_DIR.'class.list.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.visa-tour.php');
require_once(INCLUDE_DIR.'class.staff.php');
include_once INCLUDE_DIR.'data/country.list.php';
include_once INCLUDE_DIR.'data/roomtype.list.php';
include_once(INCLUDE_DIR.'class.qrcode.php');

$keyword_tour = $tour = $departure_flight_code = $return_flight_code = $departure_flight_code_b = $return_flight_code_b = NULL;
$booking_code = null;
$qs = [];
if (isset($_REQUEST['keyword_tour'])) {
    $keyword_tour = trim($_REQUEST['keyword_tour']);
    $qs += ['keyword_tour' => trim($_REQUEST['keyword_tour'])];
}
if (isset($_REQUEST['departure_flight_code'])) {
    $departure_flight_code = trim($_REQUEST['departure_flight_code']);
    $qs += ['departure_flight_code' => trim($_REQUEST['departure_flight_code'])];
}
if (isset($_REQUEST['tour']) && !empty($_REQUEST['tour'])) {
    $booking_type_id = $_REQUEST['tour'];
    $qs += ['tour' => $_REQUEST['tour']];
}
if (isset($_REQUEST['return_flight_code'])) {
    $return_flight_code = trim($_REQUEST['return_flight_code']);
    $qs += ['return_flight_code' => trim($_REQUEST['return_flight_code'])];
}
if (isset($_REQUEST['departure_flight_code_b'])) {
    $departure_flight_code_b = trim($_REQUEST['departure_flight_code_b']);
    $qs += ['departure_flight_code_b' => trim($_REQUEST['departure_flight_code_b'])];
}
if (isset($_REQUEST['return_flight_code_b'])) {
    $return_flight_code_b = trim($_REQUEST['return_flight_code_b']);
    $qs += ['return_flight_code_b' => trim($_REQUEST['return_flight_code_b'])];
}
if (isset($_REQUEST['visa_code'])) {
    $visa_code = trim($_REQUEST['visa_code']);
    $qs += ['visa_code' => trim($_REQUEST['visa_code'])];
}
$return_date = (isset($_REQUEST['return_date']) && (strlen($_REQUEST['return_date'])>=8))
    ? $_REQUEST['return_date'] : null;
if ($return_date) {
    $qs += [ 'return_date' => trim($_REQUEST['return_date']) ];
}
$gather_date  = (isset($_REQUEST['gather_date']) && (strlen($_REQUEST['gather_date'])>=8))
    ? $_REQUEST['gather_date'] : null;
if ($gather_date) {
    $qs += [ 'gather_date' => trim($_REQUEST['gather_date']) ];
}
$appointment = (isset($_REQUEST['appointment']) && (strlen($_REQUEST['appointment'])>=8))
    ? $_REQUEST['appointment'] : null;
if ($appointment) {
    $qs += [ 'appointment' => trim($_REQUEST['appointment']) ];
}
if(isset($_REQUEST['number_phone']) && trim($_REQUEST['number_phone'])) {
    $number_phone = trim($_REQUEST['number_phone']);
    $qs += ['number_phone' => trim($_REQUEST['number_phone'])];
}
if(isset($_REQUEST['passport_no']) && trim($_REQUEST['passport_no'])) {
    $passport_no = trim($_REQUEST['passport_no']);
    $qs += ['passport_no' => trim($_REQUEST['passport_no'])];
}
if(isset($_REQUEST['name_pax']) && trim($_REQUEST['name_pax'])) {
    $name_pax = trim($_REQUEST['name_pax']);
    $qs += ['name_pax' => trim($_REQUEST['name_pax'])];
}
if(isset($_REQUEST['staff']) && trim($_REQUEST['staff'])) {
    $staff = trim($_REQUEST['staff']);
    $qs += ['staff' => trim($_REQUEST['staff'])];
}
if(isset($_REQUEST['booking_code']) && trim($_REQUEST['booking_code'])){
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += ['booking_code' => $booking_code];
}


$listStaff = Staff::getAll();
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$searchData = array(
    'name_pax'                => $name_pax,
    'passport_no'             => $passport_no,
    'number_phone'            => $number_phone,
    'group_des'               => $group_des,
    'keyword_tour'            => $keyword_tour,
    'departure_flight_code'   => $departure_flight_code,
    'return_flight_code'      => $return_flight_code,
    'departure_flight_code_b' => $departure_flight_code_b,
    'return_flight_code_b'    => $return_flight_code_b,
    'return_date'             => $return_date,
    'gather_date'             => $gather_date,
    'destination'             => $booking_type_id,
    'visa_code'               => $visa_code,
    'appointment'             => $appointment,
    'staff'                   => $staff,
    'booking_code'            => $booking_code
);

$page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
$offset = ($page-1)*PAGE_LIMIT;
$count = DynamicList::objects()->count();
$results = VisaTour::getPaginationTourNew($searchData, $offset, $total, PAGE_LIMIT);
$arr_tour_id = [];
while ($results && ($row = db_fetch_array($results))){
    $arr_tour_id[] = $row['tour_id'];
}
$results->data_seek(0);
$count_status_wait = VisaTour::countVisaStatus($arr_tour_id,[\Tugo\VisaStatusList::WAITING_DOCUMENT]);
$count_status_hasVisa = VisaTour::countVisaStatus($arr_tour_id,[\Tugo\VisaStatusList::VISA_APPROVED, \Tugo\VisaStatusList::VISA_APPROVED_NOT_GO]);

$booking_type_list = TourDestination::loadList($group_des);

$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$qs = array_filter($qs);
$pageNav->setURL('visa-tour-list.php', $qs ?: null);
$showing=$pageNav->showing().' '._N('tours', 'tours', $total);

$page='visa-tour-list.inc.php';

$nav->setTabActive('visa');
//$nav->setActiveSubMenu(2,'visa');
$ost->addExtraHeader('<meta name="tip-namespace" content="visa.visa" />',
    "$('#content').data('tipNamespace', 'visa.visa_tour_list');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');

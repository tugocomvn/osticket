<?php

class TicketReturn
{
    public static function returnTicket($ticket_id, $user_id, $created)
    {
        if(!isset($user_id) || !is_numeric($user_id)) return null;
            $sql = "SELECT DISTINCT value as phone_number 
            FROM ".FORM_ANSWER_TABLE." v
            JOIN ".FORM_ENTRY_TABLE." e ON (e.id = v.entry_id) 
            JOIN ".USER_TABLE." u ON (u.id = e.object_id)
            WHERE e.object_type LIKE 'U' 
            AND v.field_id = ".USER_PHONE_NUMBER_FIELD."
             AND e.object_id =".db_input($user_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row || empty($row['phone_number'])) return null;
        $phone_number = $row['phone_number'];
        $_data = [
            'ticket_id'    => (int)$ticket_id,
            'user_id'      => (int)$user_id,
            'phone_number' => $phone_number,
            'created'      => $created,
        ];
        return self::getAmountTicket($_data);
    }

    public static function getAmountTicket($_data)
    {
        if(!is_array($_data)) return null;
        $totals = 0;
        $sql = "SELECT COUNT(DISTINCT ticket_id)
            FROM ".TICKET_TABLE." 
            WHERE status_id = ".db_input(TICKET_STATUS_SUCCESS)." 
            AND created <".db_input($_data['created'])."
            AND user_id =".db_input($_data['user_id'])."
            AND ticket_id <>".db_input($_data['ticket_id']);
        $total_from_ticket = db_count($sql);

        if ($total_from_ticket)
            echo " -- ". $_data['ticket_id']." -- ".$total_from_ticket." -- "."\r\n";

        if(!isset($_data['phone_number']) || !is_numeric($_data['phone_number']) || !_String::isMobileNumber($_data['phone_number'])) return $total_from_ticket;

        $_status = trim(json_encode(trim('Hủy')), '"');
        $_status = sprintf(" AND status <> ".ID_BOOKING_HUY."
        AND (  status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
        $sql = "SELECT COUNT(DISTINCT ticket_id)
                FROM ".BOOKING_TABLE."
                WHERE phone_number LIKE ".db_input('%'.$_data['phone_number'].'%')." 
                $_status
                AND created <".db_input($_data['created']);
        $total_from_booking = db_count($sql);

        if ($total_from_booking)
            echo " -- ". $_data['ticket_id']." -- ".$total_from_booking." -- "."\r\n";

        $sql = "SELECT COUNT(DISTINCT t.id)
                FROM ".PAX_TOUR_TABLE." ot 
                JOIN ".TOUR_NEW_TABLE." t ON t.id=ot.tour_id
                WHERE ot.phone_number LIKE ".db_input($_data['phone_number'])."
                AND gateway_present_time <".db_input($_data['created']);
        $total_from_op = db_count($sql);

        if ($total_from_op)
            echo " -- ". $_data['ticket_id']." -- ".$total_from_op." -- "."\r\n";

        $totals = $total_from_op_booking = $total_from_booking + $total_from_op;
        if(is_numeric($total_from_op_booking) || is_numeric($total_from_ticket)){
            $totals = max((int)$total_from_op_booking, (int)$total_from_ticket);
        }
        return self::insertValues($_data['ticket_id'], $totals);
    }

    public static function getValuesticket($list_30_ids)
    {
        if (!is_array($list_30_ids) || !count($list_30_ids)) return [];
        $sql = 'SELECT count_ticket, ticket_id FROM '.OST_TICKET_COUNT
            . " WHERE ticket_id IN (".implode(',', $list_30_ids).") ";
        $res = db_query($sql);
        $combined_array = [];
        while ($row = db_fetch_array($res)) {
            if (!isset($row['ticket_id']) || !$row['ticket_id']) continue;
            if (!isset($row['count_ticket']) || !$row['count_ticket']) continue;
            $combined_array[ $row['ticket_id'] ] = $row['count_ticket'];
        }
        return $combined_array;
    }

    public static function insertValues($ticket_id, $totals)
    {
        $sql = "INSERT INTO ".OST_TICKET_COUNT." (ticket_id, count_ticket) 
            values (".db_input($ticket_id).", ".db_input($totals).") 
            ON DUPLICATE KEY UPDATE count_ticket=".db_input($totals);
        return db_query($sql);
    }

    public static function get_user_id_from_phone_number($phone_number) 
    {
        if(!isset($phone_number) || !is_numeric($phone_number) || !_String::isMobileNumber($phone_number)) return null;
        $sql = "SELECT DISTINCT u.id 
            FROM ".FORM_ANSWER_TABLE." v
            JOIN ".FORM_ENTRY_TABLE." e ON (e.id = v.entry_id) 
            JOIN ".USER_TABLE." u ON (u.id = e.object_id)
            WHERE e.object_type LIKE 'U' 
            AND v.field_id = ".USER_PHONE_NUMBER_FIELD."
            AND v.value =".db_input($phone_number);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row['id'];
    }

    public static function get_ticket_id_from_user($user_id) {
        $ticket_id = [];
        if(empty($user_id)) return null;
        $sql = "SELECT DISTINCT ticket_id FROM ".TICKET_TABLE." WHERE user_id=".db_input($user_id);
        $res = db_query($sql);
        if(!$res) return null;
        while ($res && ($row = db_fetch_array($res))) {
            $ticket_id[] = $row['ticket_id'];
        }
        return $ticket_id;
    }

    public static function get_total_ticket_count_successful($arr_ticket_id) {
        if(empty($arr_ticket_id)) return 0;
        $sql = "SELECT DISTINCT count_ticket as total FROM ".OST_TICKET_COUNT." WHERE ";
        $sql .= 'ticket_id IN('.implode(',', db_input(array_filter($arr_ticket_id))).')';
        $res = db_query($sql);
        if(!$res) return null;
        while($res && ($row = db_fetch_array($res))){
            $arr_max_total[] = $row['total'];
        }
        $max_total = max($arr_max_total);
        if(!$max_total) return 0;
        return $max_total;
    }
}   
?>

<?php
if(!defined('OSTSCPINC') || !$thisstaff || !($thisstaff->canViewSettlementList() || $thisstaff->canViewExpectedSettlement())) die('Access Denied');
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);
$booking_type_list = TourDestination::loadList($group_des);
?>
<h2>Expense Forecast <small>base on bookings</small></h2>
<form action="<?php echo $cfg->getUrl() ?>scp/expenses-forecast.php" method="get">
    From: <input type="text" class="dp input-field" name="from" value="<?php if(isset($_REQUEST['from']) && $_REQUEST['from']) echo date('d/m/Y', strtotime($_REQUEST['from'])) ?>">
    To: <input type="text" class="dp input-field" name="to" value="<?php if(isset($_REQUEST['to']) && $_REQUEST['to']) echo date('d/m/Y', strtotime($_REQUEST['to'])) ?>">
    Booking Type: <select name="booking_type_id" id="" class="input-field">
        <option value=>- All -</option>
        <?php if(is_array($booking_type_list) && $booking_type_list): ?>
            <?php foreach($booking_type_list as  $item): ?>
                <option value="<?php echo $item['id'] ?>"
                        <?php if (isset($_REQUEST['booking_type_id']) && $_REQUEST['booking_type_id'] == $item['id']) echo 'selected' ?>
                ><?php echo $item['name'] ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>

    Group by: <select name="group" id="">
        <option value="day" <?php if(isset($_REQUEST['group']) && $_REQUEST['group'] === 'day') echo 'selected'  ?>>Day</option>
        <option value="week" <?php if(isset($_REQUEST['group']) && $_REQUEST['group'] === 'week') echo 'selected'  ?>>Week</option>
        <option value="month" <?php if(isset($_REQUEST['group']) && $_REQUEST['group'] === 'month') echo 'selected'  ?>>Month</option>
    </select>

    <button class="btn_sm btn-primary">Search</button>
</form>

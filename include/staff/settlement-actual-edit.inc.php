<?php
global $cfg;
?>

<style>
    form textarea {
        width: 95%;
        height: 5em;
        resize: none;
    }

    .spend_list {
        counter-reset: my-badass-counter;
    }

    .spend_list .stt:before {
        content: counter(my-badass-counter) '.';
        counter-increment: my-badass-counter;
        position: absolute;
        padding-left: 0.5em;
        margin-right: 1em;
        font-style: italic;
    }

    .spend_item {
        text-align: center;
    }

    .duplicate {
        border: 1px solid red;
    }
</style>
<?php
$visa_pax_count = PaxTour::countPax($_REQUEST['id'], false, true);
?>
<h2>Actual Settlement <small>Edit</small></h2>
<p><a href="<?php echo $referer ?>" class="btn_sm btn-default">Back</a></p>
<form action="" method="post">
    <input type="hidden" name="action" value="edit">
    <input type="hidden" name="referer" value="<?php echo $referer ?>">
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>">
    <?php csrf_token() ?>
    <table width="95%">
        <tr>
            <td width="65%">
                <table>
                    <tr>
                        <td><label for="">Rate: 1 USD =</label></td>
                        <td colspan="2"><input type="text" name="rate" required id="rate" class="input-field" step="1"
                            value="<?php if (isset($errors) && isset($_POST['rate'])) echo $_POST['rate'];
                            elseif (isset($settlement) && $settlement && isset($settlement->rate))
                                echo number_format($settlement->rate, 0, '.', ','); ?>"> VND<span><?php
                                if (isset($errors['rate'])) echo $errors['rate'] ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="">Payment Income</label></td>
                        <td><input type="text" name="income" class="input-field" id="income" readonly
                                   value="<?php
                                   $payment_income = PaxTour::calcIncomePayment($_REQUEST['id']);
                                   if (isset($settlement->rate))
                                       echo number_format($payment_income / $settlement->rate, 2, '.', ',');
                                   else echo '0';
                                   ?>"> $</td>

                        <td> = <span id="income_vnd"><?php echo number_format($payment_income, 0, '.', ','); ?></span> VND</td>
                    </tr>
                    <tr>
                        <td><label for="">Booking Income</label></td>
                        <td>
                            <?php
                            $booking_income = 0;
                            if (isset($settlement->rate)) {
                                $booking_income = PaxTour::calcIncome($_REQUEST['id']);
                                echo number_format($booking_income / $settlement->rate, 2, '.', ',');
                            }
                            else echo '0';
                            ?>
                            = <span id=""><?php echo number_format($booking_income, 0, '.', ','); ?></span> VND
                        </td>
                    </tr>
                    <tr>
                        <td><label for="">Total Outcome</label></td>
                        <td><input type="text" disabled id="outcome" class="input-field"
                            value="<?php if (isset($errors) && $errors && isset($_POST['outcome'])) echo $_POST['outcome'];
                            elseif (isset($settlement) && $settlement && isset($settlement->actual_outcome))
                                echo number_format($settlement->actual_outcome, 2, '.', ','); ?>"> $ <span class="error"><?php
                                if (isset($errors['outcome'])) echo $errors['outcome'] ?></span></td>

                        <td><span id="outcome_vnd"><?php if (isset($settlement) && $settlement && isset($settlement->actual_outcome) && isset($settlement->rate))
                            echo ' = '.number_format((float)($settlement->actual_outcome * $settlement->rate), 0, '.', ',').' VND'; ?></span></td>
                    </tr>
                    <tr>
                        <td>Actual Profit</td>
                        <td><input type="text" class="input-field" id="profit"
                                   value="<?php echo number_format($settlement->actual_profit, 2, '.', ',') ?>"
                                   disabled> $</td>
                        <td><span id="profit_vnd"><?php echo ' = '.number_format($settlement->actual_profit*$settlement->rate, 0, '.', ',').' VND' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="">Note</label></td>
                        <td colspan="2">
                            <textarea class="input-field" name="note"><?php
                                if (isset($errors) && $errors && isset($_POST['note'])) echo $_POST['note'];
                                elseif (isset($settlement) && $settlement && isset($settlement->note)) echo trim($settlement->note);
                                ?></textarea>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%">
                <table>
                    <caption>Tour Info</caption>
                    <tr>
                        <th>Tour Name</th>
                        <td><?php echo $tour->name ?></td>
                    </tr>
                    <tr>
                        <th>Destination</th>
                        <td><?php if (isset($destination_list[$tour->destination])) echo $destination_list[$tour->destination] ?></td>
                    </tr>
                    <tr>
                        <th>Departure Date</th>
                        <td><?php echo $tour->gateway_present_time ?></td>
                    </tr>
                    <tr>
                        <th>Airline</th>
                        <td><?php if (isset($airlines_list[$tour->airline_id])) echo $airlines_list[$tour->airline_id] ?></td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td><?php echo PaxTour::countPax($_REQUEST['id']); ?> + 1 TL</td>
                    </tr>
                    <tr>
                        <th>Visa Qty</th>
                        <td>
                            <?php foreach($visa_pax_count as $result => $_result): ?>
                                <span class="visa_result"><?php echo VisaResult::getResult( $result ) ?>: <?php echo $_result['total'] ?></span><br>
                            <?php endforeach; ?>
                            <span class="visa_result">+ 1</span>
                        </td>
                    </tr>
                    <tr>
                        <th>TL</th>
                        <?php
                        $tour_leader_item = DynamicListItem::lookup($tour->tour_leader_id);
                        ?>
                        <td><?php if ($tour_leader_item) echo $tour_leader_item->getValue() ?></td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <hr>
    <table width="95%">
        <caption>Spend Items</caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Value</th>
            <th>Total</th>
            <th>Total in VND</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody class="spend_list">
            <?php foreach($spend_details as $_index => $_item): ?>
                <tr class="spend_item new">
                    <td class="stt"></td>
                    <td>
                        <select name="item[]" id="" class="input-field item">
                            <option value=>- Select -</option>
                            <?php foreach($spend_items as $__index => $__item): ?>
                                <option value="<?php echo $__item['id'] ?>"
                                        data-qty="<?php echo $__item['default_quantity'] ?>"
                                        data-value="<?php echo $__item['default_value'] ?>"
                                        <?php if ($__item['id'] == $_item['account_item_id']) echo 'selected' ?>
                                ><?php echo $__item['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="quantity[]" class="input-field quantity" value="<?php echo number_format($_item['quantity'], 0, '.', ',') ?>">
                    </td>
                    <td>
                        <input type="text" name="value[]" id="" class="input-field value" value="<?php echo number_format($_item['value'], 2, '.', ',') ?>">
                    </td>
                    <td class="total"><?php echo number_format($_item['quantity']*$_item['value'], 2, '.', ',') ?></td>
                    <td class="total_vnd"><?php echo number_format($_item['quantity']*$_item['value']*$settlement->rate, 0, '.', ',') ?></td>
                    <td class="autohide-buttons">
                        <button class="btn_sm btn-xs btn-danger remove_item_btn new" type="button" data-title="Remove">
                            <i class="icon-remove"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tbody>
            <tr>
                <td colspan="7">
                    <p><button class="btn_sm btn-xs btn-primary" id="add_item_btn" type="button">add item</button></p>
                </td>
            </tr>
        </tbody>
    </table>
    <hr>
    <table width="95%">
        <tr>
            <td>
                <p class="centered">
                    <span class="notice error-banner" style="<?php if (!$error) echo "display: none;" ?>">
                        <?php if($error): ?>
                            <?php echo $error ?>
                        <?php endif; ?>
                    </span>
                </p>
                <p class="centered">
                    <a href="<?php echo $cfg->getBaseUrl().'/scp/actual_settlement.php' ?>" class="btn_sm btn-default">Cancel</a>
                    <button id="save_btn" class="btn_sm btn-primary">Save</button>
                </p>
            </td>
        </tr>
    </table>
</form>
<?php include STAFFINC_DIR.'tour-payment-list.inc.php' ?>
<?php include STAFFINC_DIR.'tour-payment-list-group.inc.php' ?>
<table class="list" width="1058" border="0" cellspacing="0" cellpadding="2">
    <caption>Tours this day</caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Departure Date</th>
        <th>Name</th>
        <th>Airline</th>
        <th>Pax Qty</th>
        <th>Rate</th>
        <th>Income</th>
        <th>Outcome</th>
        <th>Profit</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    <?php while($list && ($row = db_fetch_array($list))): if ($row['id'] == $_REQUEST['id']) continue; ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $row['gather_date'] ?></td>
            <td><?php if (isset($destination_list[$row['destination']])) echo $destination_list[$row['destination']] ?></td>
            <td><?php if (isset($airlines_list[$row['airline']])) echo $airlines_list[$row['airline']] ?></td>
            <td><?php echo PaxTour::countPax($row['id']); ?> + 1</td>
            <td><?php if($row['rate']) echo number_format($row['rate'], 0, '.', ',') ?></td>
            <td>
                <?php if($row['actual_income']) echo number_format($row['actual_income'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['actual_income'])
                            echo number_format($row['rate'] * $row['actual_income'], 0, '.', ',') . ' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if($row['actual_outcome']) echo number_format($row['actual_outcome'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['actual_outcome'])
                            echo number_format($row['rate'] * $row['actual_outcome'], 0, '.', ',').' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if((float)$row['actual_income'] - (float)$row['actual_outcome'])
                    echo number_format((float)$row['actual_income'] - (float)$row['actual_outcome'], 2, ',', '.' ).' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * ((float)$row['actual_income'] - (float)$row['actual_outcome']))
                            echo number_format($row['rate'] * ((float)$row['actual_income'] - (float)$row['actual_outcome']), 0, ',', '.' ).' VND' ?></i>
                </small>
            </td>
            <td>
                <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/actual_settlement.php?action=details&id=<?php echo $row['id'] ?>"
                   class="btn_sm btn-xs btn-primary" title="View Details">Details</a>
                <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/actual_settlement.php?action=edit&id=<?php echo $row['id'] ?>" class="btn_sm btn-xs btn-default">Edit</a>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
</table>

<script>
    (function($) {
        var spend_item_list = JSON.parse('<?php echo json_encode($spend_items) ?>');

        function add_item_elm(e) {
            elm = '<tr class="spend_item new">' +
                '                <td class="stt"></td>' +
                '                <td>' +
                '                    <select name="item[]" class="item input-field"><option value=>- Select -</option></select>' +
                '                </td>' +
                '                <td>' +
                '                    <input type="text" name="quantity[]" class="input-field quantity">' +
                '                </td>' +
                '                <td>' +
                '                    <input type="text" name="value[]" id="" class="input-field value">' +
                '                </td>' +
                '                <td class="total"></td>' +
                '                <td class="total_vnd"></td>' +
                '                <td class="autohide-buttons"> '+
                '                <button class="btn_sm btn-xs btn-danger remove_item_btn new" type="button" data-title="Remove"> '+
                '                <i class="icon-remove"></i> '+
                '               </button> '+
                '                </td>' +
                '            </tr>';
            $('.spend_list').append(elm);
            init_remove_btn($('.spend_item.new .remove_item_btn'));
            init_select_spend_item($('.spend_item.new .item'));
            $('.spend_item.new .item').on('change, blur, click, focusout', update_default_value);
            $('.spend_item.new .quantity, .spend_item.new .value').on('change, blur, click, focusout', calc);
            $('.spend_item.new').removeClass('new');
        }

        function duplicate_check(e) {
            var item_list = [];
            var duplicate = false;

            $('.spend_item select').removeClass('duplicate');
            $('.spend_item').each(function(index, elm) {
                var val = $(elm).find(':selected').val();
                console.log(val);
                if (item_list.length && item_list.indexOf(val) !== -1) {
                    duplicate = true;
                    duplicate_alert(val);
                }
                if (val) item_list.push(val);
            });

            if (duplicate) {
                $('form').bind('submit',function(e){e.preventDefault();});
                $('#save_btn').prop('disabled', true);
                $('.notice').text('Trùng cmnr').show();
            } else {
                $('form').unbind('submit');
                $('.notice').text('').hide();
                $('#save_btn').prop('disabled', false);
            }

            console.log(item_list);
        }

        function duplicate_alert(id) {
            $('.spend_item option:selected').filter(function() {
                console.log($(this).val());
                console.log(id);
                return (parseInt($(this).val()) === parseInt(id)); //To select Blue
            }).parents('select').addClass('duplicate');
        }

        function update_default_value(e) {
            elm = e.target;
            item = $(elm).parents('.spend_item');
            elm = $(elm).find(':selected');
            item.find('.quantity').val($(elm).data('qty'));
            item.find('.value').val($(elm).data('value'));

            item.find('.quantity').change();
            duplicate_check(e);
        }

        function remove_item_elm(e) {
            _self = e.target;
            if (!$(_self).hasClass('remove_item_btn') && !$(_self).parent().hasClass('remove_item_btn')) return;
            $(_self).parents('.spend_item').remove();
            calc_total();
            duplicate_check(e);
        }

        function init_select_spend_item(select) {
            string = '';
            for(elm in spend_item_list) {
                string = string+'<option value="'+spend_item_list[elm].id
                    +'" data-qty="'+spend_item_list[elm].default_quantity
                    +'" data-value="'+spend_item_list[elm].default_value+'">'+spend_item_list[elm].name+'</option>';
            }
            select.append(string);
        }

        function calc(e) {
            elm = e.target;
            item = $(elm).parents('.spend_item');
            calc_item(item);
            calc_total();
        }

        function calc_item(item) {
            var quantity = item.find('.quantity').val().length ? item.find('.quantity').val().removeDot() : 0;
            var value = item.find('.value').val().length ? item.find('.value').val().removeDot() : 0;
            var rate = $('#rate').val().length ? $('#rate').val().removeDot() : 0;
            if (!isNaN(quantity) && !isNaN(value)) {
                item.find('.total').text((quantity*value).formatMoney(2, '.', ','));
                item.find('.total_vnd').text((quantity*value*rate).formatMoney(0, '.', ','));
            }
        }

        function calc_total() {
            var sum_total = 0;
            var rate = $('#rate').val().length ? $('#rate').val().removeDot() : 0;
            $('.total').each(function(index, elm) {
                var tmp_total = $(elm).text().length ? parseFloat($(elm).text().removeDot()) : 0;
                sum_total = sum_total+parseFloat(tmp_total);
            });

            $('#outcome').val(sum_total.formatMoney(2, '.', ','));
            $('#outcome_vnd').text(' = ' + (sum_total*rate).formatMoney(0, '.', ',') + ' VND');

            var profit = $('#income').val().removeDot() - $('#outcome').val().removeDot();
            $('#profit').val(profit.formatMoney(2, '.', ','));
            $('#profit_vnd').text(' = ' + (profit*rate).formatMoney(0, '.', ',') + ' VND');
        }

        function init_remove_btn(btn) {
            btn.off('click').on('click', remove_item_elm);
        }

        function update_income_vnd() {
            var rate = $('#rate').val().length ? $('#rate').val().removeDot() : 0;
            if (!rate) return false;
            var income_vnd = $('#income_vnd').text().length ? $('#income_vnd').text().removeDot() : 0;
            if (!income_vnd) income_vnd = 0;
            $('#income').val(((income_vnd/rate) + '').removeDot().formatMoney(2, '.', ','));
            calc_total();
        }

        $('#add_item_btn').off('click').on('click', add_item_elm);
        $('#rate').on('change, blur, click, focusout', on_rate_change);

        function on_rate_change(e) {
            update_income_vnd();
            $('.spend_item').each(function(index, elm) {
                calc_item($(elm));
            });
        }

        $('.spend_item.new .remove_item_btn').off('click').on('click', remove_item_elm);
        $('.spend_item.new .item').on('change, blur, click, focusout', update_default_value);
        $('.spend_item.new .quantity, .spend_item.new .value').on('change, blur, click, focusout', calc);
    })(jQuery);
</script>

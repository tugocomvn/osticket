
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <meta charset="utf8">

    <style>
    @media all {
        #nav,
        #sub_nav,
        #header p {
            display: none;
            background-color: transparent;
        }

        #footer p {
            display: none;
            background-color: transparent;
        }

        #header {
            display: none;
        }

        body,
        #header,
        #content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }

        button,
        .btn_sm {
            display: none;
        }

        .col-md-6 {
            width: 50%;
            float: left;
        }

        .col-md-4 {
            width: 30%;
            float: left;
        }
    }

    @media screen {
        .btn_print,
        .btn_back {
            display: inline-block;
        }
    }

    .logo {
        height: 180px;
        display: flex;
        align-items: center;
    }
    </style>
</head>
<body>
    <div>
        <div class="pull-right">
            <button class="btn_sm btn-danger btn_print" onclick="window.print()"><i class="icon-print"></i> Print</button>
            <a class="no-pjax btn_sm btn-default btn_back" onclick="window.close()">Close</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-4 logo">
            <img src="<?php echo $cfg->getUrl() ?>images/tugo-logo-2024.png" width="200" height="100">
        </div>
        <div class="col-md-8">
            <p><b>CÔNG TY CỔ PHẦN DỊCH VỤ DU LỊCH VÀ THƯƠNG MẠI TUGO</b></p>
            <p>Địa chỉ:</p>
            <p>&circledast; 219 Lê Thánh Tôn, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh</p>
            <p>&circledast; 121 Lý Tự Trọng, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh</p>
            <p>Giấy phép lữ hành số: 79-726/2017/TCDL-GP LHQT</p>
            <p>Điện thoại: 028 7106 5543. Email: <a>support@tugo.com.vn</a> Website: <a>www.tugo.com.vn</a></p>
        </div>
    </div>
    <hr>
    <div>
        <div class="row" style="margin-left: 10px;">
            <div class="col-md-12" style="text-align: center; margin-bottom: 25px;">
                <h3><b>PHIẾU XÁC NHẬN ĐẶT TOUR</b></h3>
            </div>
            <div class="col-md-12">
                <p>Mã phiếu thu: <b><?php echo $results->receipt_code ?></b></p>
                <p>Mã Booking: <b><?php echo $results->booking_code ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Lộ trình: <b><?php echo $results->tour_name ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Ngày khởi hành:
                    <b><?php if (isset($results->departure_date) && strtotime($results->departure_date)) echo date('d/m/Y', strtotime($results->departure_date)) ?></b>
                </p>
            </div>
            <div class="col-md-6">
                <p>Giá tiền 1 khách (NL): <b><?php echo number_format($results->unit_price_nl) . 'đ' ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Số lượng: <b><?php if (isset($results->quantity_nl)) echo $results->quantity_nl ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Giá tiền 1 khách (TE):
                    <b><?php if (isset($results->unit_price_te)) echo number_format($results->unit_price_te) . 'đ' ?></b>
                </p>
            </div>
            <div class="col-md-6">
                <p>Số lượng: <b><?php if (isset($results->quantity_te)) echo $results->quantity_te ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Phí phụ thu:
                    <b><?php if (isset($results->surcharge)) echo number_format($results->surcharge) . 'đ' ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Nội dung phụ thu: <b><?php if (isset($results->surcharge_note)) echo $results->surcharge_note ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Tổng số tiền khách cần đóng: <b><?php echo number_format($results->amount_to_be_received) . 'đ' ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Số tiền khách đóng: <b><?php echo number_format($results->amount_received) . 'đ' ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Số tiền còn lại: <b><?php if (!empty($results->balance_due)) { echo number_format($results->balance_due) . 'đ'; } else { echo "0 đồng"; } ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Hình thức thanh toán: <b><?php if(isset($results->payment_method)) echo PaymentMethod::caseTitleName($results->payment_method) ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Tên khách đại diện: <b><?php echo $results->customer_name ?></b></p>
            </div>
            <div class="col-md-6">
                <p>Số điện thoại: <b><?php echo $results->customer_phone_number ?></b></p>
            </div>
            <div class="col-md-6">
                <p><i>Tên nhân viên kinh doanh: <b><?php echo $results->staff_name ?></b></i></p>
            </div>
            <div class="col-md-6">
                <p><i>Số điện thoại nhân viên kinh doanh: <b><?php echo $results->staff_phone_number ?></b></i></p>
            </div>
            <div class="col-md-6">
                <p><i>Tên nhân viên thu tiền: <b><?php echo $results->staff_cashier_name ?></b></i></p>
            </div>
            <div class="col-md-6">
                <p><i>Số điện thoại nhân viên thu tiền: <b><?php echo $results->staff_cashier_phone_number ?></b></i></p>
            </div>
            <div class="col-md-6">
                <p><i>Ngày hoàn tất thanh toán số tiền còn lại: <b>
                <?php if(!empty($results->due_date) && empty($results->due_date_text)): ?>
                    <?php if (strtotime($results->due_date)) echo date('d/m/Y', strtotime($results->due_date)) ?>
                <?php endif; ?>
                <?php if(!empty($results->due_date_text) && empty($results->due_date)): ?>
                    <?php echo trim($results->due_date_text) ?>
                <?php endif; ?>
                </b></i></p>
            </div>
            <div class="col-md-6">
                <p><b><i>Hotline hỗ trợ: 1900 555 543 (24/24)</i></b></p>
            </div>
            <div class="col-md-6">
                <p><i>Số lần in: <b><?php echo (int)$results->print_count ?></b></i></p>
            </div>
        </div>
    </div>
    <hr>
    <div class="row" style="margin-left: 30px;">
        <div class="col-md-12">
            <p><i><b><u>** Lưu ý:</u></b></i></p>
            <p><?php if (isset($results->public_note)) echo $results->public_note ?></p>
        </div>
        <div class="col-md-12">
            <p><b>*</b> Quý khách vui lòng thanh toán phí còn lại đúng hạn (nếu có), trường hợp quý khách thanh toán trễ hạn,
                Tugo sẽ không thể giữ dịch vụ cho quý khách và bắt buộc phải hủy tour mất phí theo quy định.</p>
        </div>
        <div class="col-md-4">
            <p><b>*</b><b> Ngày tập
                    trung: <?php if (isset($results->time_to_get_in) && strtotime($results->time_to_get_in)) echo date('d/m/Y', strtotime($results->time_to_get_in)) ?></b>
            </p>
        </div>
        <div class="col-md-4">
            <p><b>Địa điểm: <?php echo $results->place_to_get_in ?></b></p>
        </div>
        <div class="col-md-12">
            <p><b>*</b> Thời gian cần hoàn tất hồ sơ (nếu có):
                <b><?php if (isset($results->document_due) && strtotime($results->document_due)) echo date('d/m/Y', strtotime($results->document_due)) ?></b>
            </p>
            <p><b>*</b> Thời gian có kết quả visa (đối với các tour cần xin visa):
                <b><?php if (isset($results->visa_result_estimate_due) && strtotime($results->visa_result_estimate_due)) echo date('d/m/Y', strtotime($results->visa_result_estimate_due)) ?></b>
            </p>
            <p><b>*</b> Trước 1 ngày khởi hành (buổi chiều), HDV sẽ liên lạc với khách để xác nhận lại toàn bộ thông tin cần
                thiết.</p>
        </div>
    </div>
    <div class="row" style="margin-left: 30px;">
        <div>
            <p><i><b><u>CHÍNH SÁCH HỦY ĐỔI TOUR:</u></b></i></p>
        </div>
        <div>
            <p><i>Tour khuyến mãi tại Tugo không áp dụng chính sách hủy tour, dời ngày hoặc đổi người đi. Khách sẽ mất 100%
                    phí tour trong các trường hợp trên.</i></p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4" style="text-align:center; margin-top: 30px;">
            <p><b>Khách xác nhận</b></p>
            <?php if ($results->status === Receipt::WAITING_VERIFYING || $results->status === Receipt::APPROVED): ?>
                <span>Đã xác nhận</span>
            <?php endif; ?>
        </div>
        <div class="col-md-4" style="text-align:center; margin-top: 30px;">
            <p><b>Kế toán xác nhận</b></p>
            <?php if ($results->status === Receipt::APPROVED): ?>
                <span>Đã thu tiền</span>
            <?php endif; ?>
        </div>
        <div class="col-md-4" style="text-align:center;">
            <p><?php echo $weekday = Receipt::getWeekdays() ?></p>
            <p><b>Nhân viên xác nhận</b></p>
            <?php if ($results->status === Receipt::APPROVED || $results->status === Receipt::WAITING_VERIFYING || $results->status === Receipt::MONEY_RECEIVED || $results->status === Receipt::SEND_CODE): ?>
                <span>Đã xác nhận</span>
            <?php endif; ?>
        </div>
    </div>
</body>
<script>
    $(document).ready(function () {
        window.print();
    });
</script>
</html>
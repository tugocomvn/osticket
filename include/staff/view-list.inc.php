<?php
global $thisstaff;

$info=array();
if ($list) {
    $title = __('Update custom list');
    $action = 'update';
    $submit_text = __('Save Changes');
    $info = $list->getInfo();
    $newcount=2;
} else {
    $title = __('Add New Custom List');
    $action = 'add';
    $submit_text = __('Add List');
    $newcount=4;
}

$info=Format::htmlchars(($errors && $_POST) ? array_merge($info,$_POST) : $info);

?>
<h2><?php echo __('List'); ?>
    <?php echo $list ? $list->getName() : __('Add new list'); ?></h2>

<form action="" method="get" id="search">
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>">
    <table>
        <?php if(TOUR_PAX_LIST_ID != $_REQUEST['id']): ?>
            <tr>
                <td>Search Info</td>
                <td>
                    <input type="text" name="item_info" value="<?php
                    if (isset($_REQUEST['item_info']) && !empty($_REQUEST['item_info'])) echo trim($_REQUEST['item_info']);
                    ?>" class="input-field" placeholder="Name/Info" />
                </td>

            </tr>
        <?php endif; ?>
        <?php if(TOUR_PAX_LIST_ID == $_REQUEST['id']): ?>
            <tr>
                <td>Tour Info</td>
                <td>
                    <input type="text" name="keyword" value="<?php
                    if (isset($_REQUEST['keyword']) && !empty($_REQUEST['keyword'])) echo trim($_REQUEST['keyword']);
                    ?>" class="input-field" placeholder="Name/Info" />
                </td>

                <td>Pax Name</td>
                <td>
                    <input type="text" name="pax_name" value="<?php
                    if (isset($_REQUEST['pax_name']) && !empty($_REQUEST['pax_name'])) echo trim($_REQUEST['pax_name']);
                    ?>" class="input-field" placeholder="Pax name" />
                </td>

                <td>Phone number</td>
                <td>
                    <input type="text" name="pax_phone_number" value="<?php
                    if (isset($_REQUEST['pax_phone_number']) && !empty($_REQUEST['pax_phone_number'])) echo trim($_REQUEST['pax_phone_number']);
                    ?>" class="input-field" placeholder="Pax Phone number" />
                </td>
            </tr>
            <tr>

                <td>Điểm Đến</td>
                <td>
                    <select name="tour[]" class="input-field" multiple style="height: 12em;">
                        <option value=><?php echo __('-- All --') ?></option>
                        <?php foreach($destination_list as $id => $name): ?>
                            <option value="<?php echo $id ?>"
                                <?php if (isset($_REQUEST['tour']) && ((is_array($_REQUEST['tour']) && in_array($id, $_REQUEST['tour'])) || ($_REQUEST['tour']==$id && is_numeric($_REQUEST['tour'])))) echo 'selected' ?>
                            ><?php echo $name ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>

                <td>Passport number</td>
                <td>
                    <input type="text" name="pax_passport_no" value="<?php
                    if (isset($_REQUEST['pax_passport_no']) && !empty($_REQUEST['pax_passport_no'])) echo trim($_REQUEST['pax_passport_no']);
                    ?>" class="input-field" placeholder="Pax passport number" />
                </td>

                <td>Booking code</td>
                <td>
                    <input type="text" name="pax_booking_code" value="<?php
                    if (isset($_REQUEST['pax_booking_code']) && !empty($_REQUEST['pax_booking_code'])) echo trim($_REQUEST['pax_booking_code']);
                    ?>" class="input-field" placeholder="Pax Booking code" />
                </td>

            </tr>

            <tr>
                <td></td>
                <td>Ngày khởi hành</td>
                <td>Từ</td>
                <td>
                    <input type="text" class="input-field dp" name="from" value="<?php if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from'])) echo date('d/m/Y', strtotime($_REQUEST['from'])) ?>">
                </td>
                <td>Đến</td>
                <td>
                    <input type="text" class="input-field dp" name="to" value="<?php if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to'])) echo date('d/m/Y', strtotime($_REQUEST['to'])) ?>">
                </td>
            </tr>
        <?php endif; ?>

        <tr>
            <td>
                <button class="btn_sm btn-default" name="reset" value="reset" type="reset">Reset</button>
                <button class="btn_sm btn-primary" name="action" value="search">Search</button>
            </td>
        </tr>
    </table>
</form>
<form action="" method="post" id="save">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="<?php echo $action; ?>">
    <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
    <input type="hidden" name="id" value="<?php echo $info['id']; ?>">

    <table class="form_table" width="1058" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <?php if ($list) {
            $page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
            $count = $list->getNumItems($item_info ?: $keyword, $item_ids);
            $pageNav = new Pagenate($count, $page, PAGE_LIMIT);
            $pageNav->setURL('operator.php', $qs);
            $showing=$pageNav->showing().' '.__('list items');
            ?>
        <?php }
        else $showing = __('Add a few initial items to the list');
        ?>
        <tr>
            <th colspan="7">
                <em><?php echo $showing; ?></em>
            </th>
        </tr>
        <tr>
            <th width="3%"></th>
            <th width="18%"><?php echo __('Item'); ?></th>
            <th><?php echo __('Information'); ?></th>
            <th width="13%"><?php echo __('Action'); ?></th>
            <?php if(TOUR_PAX_LIST_ID == $_REQUEST['id']): ?>
                <th>Pax List</th>
            <?php endif; ?>
        </tr>
        </thead>

        <tbody <?php if ($info['sort_mode'] == 'SortCol') { ?>
            class="sortable-rows" data-sort="sort-"<?php } ?>>
        <?php
        if ($list && !empty($group_des) && isset($group_des)) {
            $icon = ($info['sort_mode'] == 'SortCol')
                ? '<i class="icon-sort"></i>&nbsp;' : '';
            foreach ($list->getItems($pageNav->getLimit(), $pageNav->getStart(), $item_info ?: $keyword, $item_ids) as $i) {
                $id = $i->getId();
                $count_pax = Tour::countPax($id);
            ?>
                <tr class="<?php if (!$i->isEnabled()) echo 'disabled'; ?>">
                    <td><?php echo $icon;?>
                        <input type="hidden" name="sort-<?php echo $id; ?>"
                               value="<?php echo $i->getSortOrder(); ?>"/></td>
                    <td><input type="text" size="20" <?php if(!$thisstaff->canCreateOperatorListItem()): ?>
                        readonly="readonly"
                    <?php endif; ?> name="value-<?php echo $id; ?>"
                               value="<?php echo $i->getValue(); ?>"/>
                        <?php

                        if ($errors["value-$id"])
                            echo sprintf('<br><span class="error">%s</span>',
                                         $errors["value-$id"]);
                        ?>
                    </td>
                    <td class="view-mode"><?php
                        $item = $i;
                        $config = $item->getConfiguration();
                        $internal = $item->isInternal();
                        $form = $item->getConfigurationForm();
                        foreach ($form->getFields() as $f) {

                            ?>
                            <div class="custom-field" id="field<?php
                            echo $f->getWidget()->id; ?>"
                                <?php
                                if (!$f->isVisible()) echo 'style="display:none;"'; ?>>
                                <div class="field-label">
                                    <label for="<?php echo $f->getWidget()->name; ?>"
                                           style="vertical-align:top;padding-top:0.2em">
                                        <?php echo Format::htmlchars($f->get('label')); ?>:</label>
                                    <?php
                                    if (!$internal && $f->isEditable() && $f->get('hint')) { ?>
                                        <br /><em style="color:gray;display:inline-block"><?php
                                            echo Format::htmlchars($f->get('hint')); ?></em>
                                        <?php
                                    } ?>

                                    <?php
                                    if ($internal && !$f->isEditable())
                                        $f->render('view');
                                    else {
                                        $f->render('text');
                                        if ($f->get('required')) { ?>
                                            <font class="error">*</font>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                                foreach ($f->errors() as $e) { ?>
                                    <div class="error"><?php echo $e; ?></div>
                                <?php } ?>
                            </div>
                            <?php
                        }
                        ?>
                    </td>

                    <?php if(TOUR_PAX_LIST_ID == $_REQUEST['id']): ?>
                        <td>
                            <?php
                            $group_des = $thisstaff->getGroup()->getDestinations();
                            ?>
                            <?php
                            $tour_arr = [];
                            $sql = 'SELECT DISTINCT IFNULL(destination,-1) from ost_tour WHERE id= '.$id.' LIMIT 1';
                            $res = db_query($sql);
                            if ($res && ($row = db_fetch_row($res)))
                                $tour_arr = $row;
                            ?>
                            <?php if ($list->hasProperties() && (array_intersect($tour_arr,$group_des) || in_array(-1,$tour_arr))) { ?>
                                <?php if($thisstaff->canOpEditPax()): ?>
                                    <p>
                                        <a class="btn_sm btn-primary btn-xs action-button field-config"
                                           style="overflow:inherit"
                                           href="#list/<?php
                                           echo $list->getId(); ?>/item/<?php
                                           echo $id ?>/properties"
                                           id="item-<?php echo $id; ?>"
                                        ><?php
                                            echo sprintf('<i class="icon-edit" %s></i> ',
                                                         $i->getConfiguration()
                                                             ? '': 'style="color:red; font-weight:bold;"');
                                            echo __('Edit');
                                            ?></a>
                                    </p>
                                <?php endif; ?>
                                <?php
                                if(array_intersect($tour_arr,$group_des) && !$count_pax): ?>
                                    <?php if($thisstaff->canOpEditPax()): ?>
                                        <p>
                                            <a href="/scp/tour-guest.php?layout=upload_booking&tour=<?php echo $id ?>"  target="_blank" class="no-pjax btn_sm btn-success btn-xs"><i class="icon-file-text"></i> Upload Booking</a>
                                        </p>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php
                            } ?>
                        </td>

                        <td>
                            <?php if($thisstaff->canViewTourList() && $count_pax) {?>
                                <p>
                                    <a href="/scp/tour-guest.php?tour=<?php echo $id ?>&layout=list" target="_blank"  class="no-pjax btn_sm btn-warning btn-xs"><i class="icon-list"></i> View List</a>
                                </p>

                                <p>
                                    <a href="/scp/tour-guest.php?tour=<?php echo $id ?>&layout=print" target="_blank" class="no-pjax btn_sm btn-default btn-xs no-pjax"><i class="icon-print"></i> Print</a>
                                </p>

                            <?php } else {
                                ?><p>[ No Data ]</p><?php
                            }?>
                            <?php
                                if(array_intersect($tour_arr,$group_des)): ?>
                                    <?php if($thisstaff->canOpEditPax()): ?>
                                        <p>
                                            <a href="/scp/tour-guest.php?tour=<?php echo $id ?>" target="_blank" class="no-pjax btn_sm btn-success btn-xs"><i class="icon-user"></i> Add/Edit</a>
                                        </p>
                                    <?php endif; ?>
                                    <?php if($count_pax): ?>
                                        <p>
                                            <a href="/scp/tour-guest.php?tour=<?php echo $id ?>&action=export" target="_blank" class="no-pjax btn_sm btn-danger btn-xs no-pjax"><i class="icon-file-text"></i> Export</a>
                                        </p>
                                    <?php endif; ?>
                                    <p>
                                        <a href="/scp/tour-guest.php?tour=<?php echo $id ?>&layout=import" target="_blank"  class="no-pjax btn_sm btn-info btn-xs"><i class="icon-list-alt"></i> Import</a>
                                    </p>
                                    <p>
                                        <a href="/scp/tour-guest.php?tour=<?php echo $id ?>&layout=passport_upload" target="_blank"  class="no-pjax btn_sm btn-default btn-xs"><i class="icon-camera"></i> Passport</a>
                                    </p>
                                    <p>
                                        <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $id ?>&layout=import_fast" target="_blank" class="no-pjax btn_sm btn-info btn-xs"><i class="icon-list-alt"></i> Fast Create</a>
                                    </p>

                                <?php endif; ?>
                                <?php
                            ?>
                        </td>

                    <?php else: ?>
                        <td>
                            <?php if ($list->hasProperties()) { ?>
                                <?php if($thisstaff->canOpEditPax()): ?>
                                    <p>
                                        <a class="btn_sm btn-primary btn-xs action-button field-config"
                                           style="overflow:inherit"
                                           href="#list/<?php
                                           echo $list->getId(); ?>/item/<?php
                                           echo $id ?>/properties"
                                           id="item-<?php echo $id; ?>"
                                        ><?php
                                            echo sprintf('<i class="icon-edit" %s></i> ',
                                                         $i->getConfiguration()
                                                             ? '': 'style="color:red; font-weight:bold;"');
                                            echo __('Edit');
                                            ?></a>
                                    </p>
                                <?php endif; ?>
                                <?php
                            } ?>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php }
        }

        if ((!$list || $list->allowAdd()) && $thisstaff->canCreateOperatorListItem()) {
            for ($i=0; $i<$newcount; $i++) { ?>
                <tr>
                    <td><?php echo $icon; ?> <em>+</em>
                        <input type="hidden" name="sort-new-<?php echo $i; ?>"/></td>
                    <td><input type="text" size="20" name="value-new-<?php echo $i; ?>"/></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <?php
            }
        }?>
        </tbody>
    </table>
    <?php
    if ($count) //Show options..
        echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
    ?>
    <?php if($thisstaff->canCreateOperatorListItem()): ?>
        <p class="centered">
            <input type="submit" class="btn_sm btn-primary" name="submit" value="<?php echo $submit_text; ?>">
            <input type="button" class="btn_sm btn-default" name="cancel" value="<?php echo __('Cancel'); ?>"
                   onclick='window.location.href="?"'>
        </p>
    <?php endif; ?>
</form>

<script type="text/javascript">
    var OPERATOR_LIST = 1;
    $(function() {
        $('a.field-config').click( function(e) {
            e.preventDefault();
            var $id = $(this).attr('id');
            var url = 'ajax.php/'+$(this).attr('href').substr(1);
            $.dialog(url, [201], function (xhr) {
                $('a#'+$id+' i').removeAttr('style');
            });
            return false;
        });
    });
</script>

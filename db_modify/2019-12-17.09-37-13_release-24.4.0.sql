
CREATE TABLE `tour_destination`
(
    `id`             int(10) UNSIGNED NOT NULL,
    `name`           varchar(64)      NOT NULL,
    `status`         tinyint(1)       NOT NULL DEFAULT '1' COMMENT 'ENABLE=1, DISABLE=0',
    `created_at`     datetime                  DEFAULT NULL,
    `created_by`     int(11)                   DEFAULT NULL,
    `updated_at`     datetime                  DEFAULT NULL,
    `updated_by`     int(11)                   DEFAULT NULL,
    `land_rate`      decimal(12, 0)            DEFAULT NULL,
    `flight_rate`    decimal(12, 0)            DEFAULT NULL,
    `land_date`      mediumint(3)              DEFAULT NULL,
    `flight_date`    mediumint(3)              DEFAULT NULL,
    `tour_market_id` int(11)                   DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `tour_destination`
--

INSERT INTO `tour_destination` (`id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`,
                                `land_rate`, `flight_rate`, `land_date`, `flight_date`, `tour_market_id`)
VALUES (51, 'Hàn Quốc (cũ)', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
       (52, 'Đà Lạt', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (58, 'Đài Loan', 1, NULL, NULL, NULL, NULL, '170', '154', 30, -10, 948),
       (59, 'Úc Sydney', 1, NULL, NULL, NULL, NULL, '289', '525', 30, -10, 946),
       (60, 'Thái Lan', 1, NULL, NULL, NULL, NULL, '60', '136', 30, -10, 945),
       (61, 'Campuchia', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 949),
       (62, 'Phú Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (63, 'Đà Nẵng', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (64, 'Nam Du', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (65, 'Bình Ba', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (66, 'Phòng khách sạn', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (67, 'Vé Máy Bay', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (68, 'Hong Kong', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (69, 'Sing Malay Indo', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (70, 'Hong Kong - Trung Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 951),
       (71, 'Sing Malay', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (72, 'Vũng Tàu', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (73, 'Miền Tây', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (74, 'Singapore', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 950),
       (75, 'Sổ tiết kiệm', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (76, 'Khác (nhớ ghi vào note)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (125, 'Nhật Bản (cũ)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 944),
       (126, 'Châu Âu 5 nước', 1, NULL, NULL, NULL, NULL, '789', '896', 30, -10, 940),
       (127, 'Mỹ', 1, NULL, NULL, NULL, NULL, '476', '555', 30, -10, 942),
       (128, 'Côn Đảo', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (129, 'Trung Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 951),
       (130, 'Bali', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (131, 'Nha Trang', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (132, 'Dubai', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (133, 'Sapa', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (134, 'Điệp Sơn', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (135, 'Phú Yên', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (139, 'Canada', 1, NULL, NULL, NULL, NULL, '752', '614', 30, -10, 939),
       (150, 'Pháp Mono', 1, NULL, NULL, NULL, NULL, '498', '523', 30, -10, 940),
       (167, 'Nhật Bản Osaka VNA', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 944),
       (168, 'Nhật Bản Narita', 1, NULL, NULL, NULL, NULL, '425', '362', 30, -10, 944),
       (169, 'Nhật Bản Nagoya', 1, NULL, NULL, NULL, NULL, '425', '415', 30, -10, 944),
       (170, 'Hàn Quốc VJ', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
       (171, 'Hàn Quốc VNA', 1, NULL, NULL, NULL, NULL, '90', '332', 90, -10, 941),
       (172, 'Hàn Quốc Busan (VNA)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
       (173, 'Myanmar', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (176, 'Nhật Bản HN Nagoya 4N', 1, NULL, NULL, NULL, NULL, '425', '415', 30, -10, 944),
       (211, 'Hàn Quốc CA', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
       (215, 'Hàn Quốc Busan (CA)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
       (226, 'Hàn Quốc T''way', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
       (234, 'Thái Lan Phuket', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 945),
       (242, 'Nhật Bản (SQ)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 944),
       (258, 'Úc Melbourne', 1, NULL, NULL, NULL, NULL, '276', '525', 30, -10, 946),
       (259, 'Châu Âu 7 nước', 1, NULL, NULL, NULL, NULL, '1073', '554', 30, -10, 940),
       (260, 'Châu Âu 5 nước luxury', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 940),
       (313, 'Mỹ (Free & Easy)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 942),
       (314, 'Canada (Free & Easy)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 939),
       (342, 'Anh Quốc (UK-VNA)', 1, NULL, NULL, NULL, NULL, '630', '765', 30, -10, 938),
       (343, 'Mỹ-Canada', 1, NULL, NULL, NULL, NULL, '1085', '752', 30, -10, 942),
       (488, 'Nhật Bản Fukuoka', 1, NULL, NULL, NULL, NULL, '425', '397', 30, -10, 944),
       (510, 'Bà Lụa-Trà Sư', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (511, 'Châu Âu 4 nước', 1, NULL, NULL, NULL, NULL, '945', '492', 30, -10, 940),
       (554, 'Maldives', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (569, 'Cần Giờ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
       (599, 'Hàn Quốc JJ', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
       (653, 'Nhật Bản HN Nagoya 5N', 1, NULL, NULL, NULL, NULL, '630', '362', 30, -10, 944),
       (654, 'Nhật Bản HN Osaka 5N', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 944),
       (655, 'Nhật Bản Narita 3N2D', 1, NULL, NULL, NULL, NULL, '425', '397', 30, -10, 944),
       (729, 'Châu Âu 3 nước', 1, NULL, NULL, NULL, NULL, '945', '492', 30, -10, 940),
       (873, 'New Zealand-Úc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 946),
       (882, 'Hàn Quốc-Nhật Bản', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
       (961, 'Visa Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1014, 'Nhật Bản Nikko', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 0),
       (1030, 'Úc VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1031, 'Mỹ VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1032, 'Canada VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1033, 'Châu Âu VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1039, 'Hàn Quốc VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1061, 'Úc Sydney-Mel', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1066, 'Anh Quốc (UK-CA)', 1, NULL, NULL, NULL, NULL, '664', '572', 30, -10, 938),
       (1067, 'Nhật Bản Osaka VJ', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 0),
       (1068, 'Nhật Bản Narita VJ', 1, NULL, NULL, NULL, NULL, '429', '397', 30, -10, 0),
       (1069, 'Anh Quốc (UK) VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1080, 'Thẻ Premium', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
       (1081, 'Miền Bắc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tour_destination`
--
ALTER TABLE `tour_destination`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `name_UNIQUE` (`name`),
    ADD KEY `index3` (`status`),
    ADD KEY `index4` (`land_rate`),
    ADD KEY `index5` (`flight_rate`),
    ADD KEY `index6` (`land_date`),
    ADD KEY `index7` (`flight_date`),
    ADD KEY `index8` (`tour_market_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tour_destination`
--
ALTER TABLE `tour_destination`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1082;

CREATE TABLE `tour_market`
(
    `id`         int(10) UNSIGNED NOT NULL,
    `name`       varchar(64)      NOT NULL,
    `status`     tinyint(1)       NOT NULL DEFAULT '1' COMMENT 'ENABLE=1, DISABLE=0',
    `created_at` datetime                  DEFAULT NULL,
    `created_by` int(11)                   DEFAULT NULL,
    `updated_at` datetime                  DEFAULT NULL,
    `updated_by` int(11)                   DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `tour_market`
--

INSERT INTO `tour_market` (`id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`)
VALUES (938, 'Anh Quốc', 1, NULL, NULL, NULL, NULL),
       (939, 'Canada', 1, NULL, NULL, NULL, NULL),
       (940, 'Châu Âu', 1, NULL, NULL, NULL, NULL),
       (941, 'Hàn Quốc', 1, NULL, NULL, NULL, NULL),
       (942, 'Mỹ', 1, NULL, NULL, NULL, NULL),
       (943, 'New Zealand', 1, NULL, NULL, NULL, NULL),
       (944, 'Nhật Bản', 1, NULL, NULL, NULL, NULL),
       (945, 'Thái Lan', 1, NULL, NULL, NULL, NULL),
       (946, 'Úc', 1, NULL, NULL, NULL, NULL),
       (947, 'Việt Nam', 1, NULL, NULL, NULL, NULL),
       (948, 'Đài Loan', 1, NULL, NULL, NULL, NULL),
       (949, 'Cambodia', 1, NULL, NULL, NULL, NULL),
       (950, 'Singapore', 1, NULL, NULL, NULL, NULL),
       (951, 'Trung Quốc', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tour_market`
--
ALTER TABLE `tour_market`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `name_UNIQUE` (`name`),
    ADD KEY `index3` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tour_market`
--
ALTER TABLE `tour_market`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 952;

CREATE TABLE IF NOT EXISTS `price_rule`
(
    `id`                        INT UNSIGNED   NOT NULL,
    `name`                      VARCHAR(64)    NOT NULL,
    `tour_size_greater_eq_than` MEDIUMINT(3)   NOT NULL DEFAULT 0,
    `net_price`                 DECIMAL(12, 0) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `index2` (`name` ASC)
)
    ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `tour_price_rule`
(
    `id`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `tour_id`       INT          NOT NULL,
    `price_rule_id` INT          NOT NULL,
    `updated_at`    DATETIME     NOT NULL,
    `updated_by`    INT          NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unique` (`tour_id` ASC, `price_rule_id` ASC),
    INDEX `index3` (`tour_id` ASC),
    INDEX `index4` (`price_rule_id` ASC)
)
    ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `tour`
(
    `id`                        INT            NOT NULL AUTO_INCREMENT,
    `name`                      VARCHAR(45)    NULL,
    `destination`               INT            NULL,
    `tour_leader_id`            INT            NULL,
    `gateway_id`                INT            NOT NULL,
    `gateway_present_time`      DATETIME       NOT NULL,
    `return_time`               DATETIME       NOT NULL,
    `pax_quantity`              SMALLINT(3)    NOT NULL DEFAULT 0,
    `tl_quantity`               SMALLINT(2)    NULL,
    `net_price`                 DECIMAL(10, 0) NOT NULL DEFAULT 0,
    `retail_price`              DECIMAL(10, 0) NOT NULL DEFAULT 0,
    `maximum_discount`          DECIMAL(10, 0) NOT NULL DEFAULT 0,
    `stimulus_tour`             TINYINT(1)     NULL     DEFAULT 0,
    `note`                      TEXT(65000)    NULL,
    `transit_airport_id`        INT            NOT NULL DEFAULT 0,
    `airline_id`                INT            NOT NULL,
    `country_id`                INT            NOT NULL,
    `sure_quantity`             SMALLINT(3)    NOT NULL DEFAULT 0,
    `hold_quantity`             SMALLINT(3)    NOT NULL DEFAULT 0,
    `approved_booking_quantity` SMALLINT(3)    NOT NULL DEFAULT 0,
    `status`                    TINYINT(1)     NOT NULL DEFAULT 1,
    `color_code`                VARCHAR(6)     NULL,
    `created_by`                INT(11)        NOT NULL,
    `created_at`                DATETIME       NOT NULL,
    `updated_at`                DATETIME       NULL,
    `updated_by`                INT(11)        NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`name` ASC),
    INDEX `index3` (`destination` ASC),
    INDEX `index4` (`tour_leader_id` ASC),
    INDEX `index5` (`gateway_id` ASC),
    INDEX `index6` (`gateway_present_time` ASC),
    INDEX `index7` (`return_time` ASC),
    INDEX `index8` (`pax_quantity` ASC),
    INDEX `index9` (`tl_quantity` ASC),
    INDEX `index10` (`net_price` ASC),
    INDEX `index11` (`retail_price` ASC),
    INDEX `index12` (`maximum_discount` ASC),
    INDEX `index13` (`stimulus_tour` ASC),
    INDEX `index15` (`transit_airport_id` ASC),
    INDEX `index16` (`airline_id` ASC),
    INDEX `index17` (`country_id` ASC),
    INDEX `index18` (`sure_quantity` ASC),
    INDEX `index19` (`hold_quantity` ASC),
    INDEX `index20` (`approved_booking_quantity` ASC),
    INDEX `index21` (`status` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `tour_log`
(
    `id`         INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `tour_id`    INT          NULL,
    `content`    LONGTEXT     NULL,
    `created_at` DATETIME     NULL,
    `created_by` INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`tour_id` ASC),
    INDEX `index3` (`created_at` ASC),
    INDEX `index4` (`created_by` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `flight_ticket`
(
    `id`                 INT UNSIGNED   NOT NULL AUTO_INCREMENT,
    `flight_ticket_code` VARCHAR(16)    NULL,
    `net_price`          DECIMAL(12, 0) NULL,
    `retail_price`       DECIMAL(12, 0) NULL,
    `total_quantity`     MEDIUMINT(3)   NULL,
    `adult_quantity`     MEDIUMINT(3)   NULL,
    `children_quantity`  MEDIUMINT(3)   NULL,
    `infant_quantity`    MEDIUMINT(3)   NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_code` ASC),
    INDEX `index3` (`net_price` ASC),
    INDEX `index4` (`retail_price` ASC),
    INDEX `index5` (`total_quantity` ASC),
    INDEX `index6` (`adult_quantity` ASC),
    INDEX `index7` (`children_quantity` ASC),
    INDEX `index8` (`infant_quantity` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `flight_ticket_log`
(
    `id`               INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_ticket_id` INT          NULL,
    `content`          LONGTEXT     NULL,
    `created_at`       DATETIME     NULL,
    `created_by`       INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_id` ASC),
    INDEX `index3` (`created_at` ASC),
    INDEX `index4` (`created_by` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `flight_ticket_tour`
(
    `id`               INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_ticket_id` INT          NULL,
    `tour_id`          INT          NULL,
    `updated_at`       DATETIME     NULL,
    `updated_by`       INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_id` ASC),
    INDEX `index3` (`tour_id` ASC),
    UNIQUE INDEX `index4` (`flight_ticket_id` ASC, `tour_id` ASC)
)
    ENGINE = InnoDB;

-- auto-generated definition
create table staff_signin_token
(
    id           int auto_increment
        primary key,
    user_name    varchar(32)  not null,
    staff_id     int unsigned null,
    signin_token varchar(64)  null,
    create_at    datetime     null,
    expired_at   datetime     null
);

/*branch feature/tour-market*/
alter table ost_groups
    add can_edit_net_price tinyint(1) unsigned default 0 not null;

alter table ost_groups
    add can_edit_retail_price tinyint(1) unsigned default 0 not null;


ALTER TABLE receipt
    ADD payment_method INT NOT NULL DEFAULT 0;

CREATE TABLE IF NOT EXISTS `fnd_flight`
(
    `id`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_code`       VARCHAR(10)  NULL,
    `airline_id`        INT          NULL,
    `airport_code_from` VARCHAR(6)   NULL,
    `airport_code_to`   VARCHAR(6)   NULL,
    `departure_at`      DATETIME     NULL,
    `arrival_at`        DATETIME     NULL,
    `created_at`        DATETIME     NULL,
    `created_by`        INT          NULL,
    `updated_at`        DATETIME     NULL,
    `updated_by`        INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_code` ASC),
    INDEX `index3` (`airline_id` ASC),
    INDEX `index4` (`airport_code_from` ASC),
    INDEX `index5` (`airport_code_to` ASC),
    INDEX `index6` (`departure_at` ASC),
    INDEX `index7` (`arrival_at` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_ticket`
(
    `id`                 INT UNSIGNED   NOT NULL AUTO_INCREMENT,
    `flight_ticket_code` VARCHAR(16)    NULL,
    `net_price`          DECIMAL(12, 0) NULL,
    `retail_price`       DECIMAL(12, 0) NULL,
    `total_quantity`     MEDIUMINT(3)   NULL,
    `adult_quantity`     MEDIUMINT(3)   NULL,
    `children_quantity`  MEDIUMINT(3)   NULL,
    `infant_quantity`    MEDIUMINT(3)   NULL,
    `flight_id`          INT            NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_code` ASC),
    INDEX `index3` (`net_price` ASC),
    INDEX `index4` (`retail_price` ASC),
    INDEX `index5` (`total_quantity` ASC),
    INDEX `index6` (`adult_quantity` ASC),
    INDEX `index7` (`children_quantity` ASC),
    INDEX `index8` (`infant_quantity` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_ticket_log`
(
    `id`               INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_ticket_id` INT          NULL,
    `content`          LONGTEXT     NULL,
    `created_at`       DATETIME     NULL,
    `created_by`       INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_id` ASC),
    INDEX `index3` (`created_at` ASC),
    INDEX `index4` (`created_by` ASC)
)
    ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_log`
(
    `id`         INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_id`  INT          NULL,
    `content`    LONGTEXT     NULL,
    `created_at` DATETIME     NULL,
    `created_by` INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_id` ASC),
    INDEX `index3` (`created_at` ASC),
    INDEX `index4` (`created_by` ASC)
)
    ENGINE = InnoDB;


DROP TABLE IF EXISTS `flight`;
CREATE TABLE IF NOT EXISTS `flight`
(
    `id`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_code`       VARCHAR(10)  NULL,
    `airline_id`        INT          NULL,
    `airport_code_from` VARCHAR(6)   NULL,
    `airport_code_to`   VARCHAR(6)   NULL,
    `departure_at`      DATETIME     NULL,
    `arrival_at`        DATETIME     NULL,
    `created_at`        DATETIME     NULL,
    `created_by`        INT          NULL,
    `updated_at`        DATETIME     NULL,
    `updated_by`        INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_code` ASC),
    INDEX `index3` (`airline_id` ASC),
    INDEX `index4` (`airport_code_from` ASC),
    INDEX `index5` (`airport_code_to` ASC),
    INDEX `index6` (`departure_at` ASC),
    INDEX `index7` (`arrival_at` ASC)
)
    ENGINE = InnoDB;



DROP TABLE IF EXISTS `flight_ticket`;
CREATE TABLE IF NOT EXISTS `flight_ticket`
(
    `id`                 INT UNSIGNED   NOT NULL AUTO_INCREMENT,
    `flight_ticket_code` VARCHAR(16)    NULL,
    `net_price`          DECIMAL(12, 0) NULL,
    `retail_price`       DECIMAL(12, 0) NULL,
    `total_quantity`     MEDIUMINT(3)   NULL,
    `adult_quantity`     MEDIUMINT(3)   NULL,
    `children_quantity`  MEDIUMINT(3)   NULL,
    `infant_quantity`    MEDIUMINT(3)   NULL,
    `flight_id`          INT            NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_ticket_code` ASC),
    INDEX `index3` (`net_price` ASC),
    INDEX `index4` (`retail_price` ASC),
    INDEX `index5` (`total_quantity` ASC),
    INDEX `index6` (`adult_quantity` ASC),
    INDEX `index7` (`children_quantity` ASC),
    INDEX `index8` (`infant_quantity` ASC)
)
    ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `flight_ticket_flight`
(
    `flight_ticket_id` INT NOT NULL,
    `flight_id`        INT NOT NULL,
    PRIMARY KEY (flight_ticket_id, flight_id),
    INDEX `index1` (`flight_ticket_id` ASC),
    INDEX `index2` (`flight_id` ASC)
)
    ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `fnd_flight_ticket_flight`
(
    `flight_ticket_id` INT NOT NULL,
    `flight_id`        INT NOT NULL,
    PRIMARY KEY (flight_ticket_id, flight_id),
    INDEX `index1` (`flight_ticket_id` ASC),
    INDEX `index2` (`flight_id` ASC)
)
    ENGINE = InnoDB;


ALTER TABLE `fnd_flight_ticket`
    DROP COLUMN flight_id;
ALTER TABLE `flight_ticket`
    DROP COLUMN flight_id;
ALTER TABLE receipt
    ADD bank_transfer_note VARCHAR(255) NULL;
ALTER TABLE ost_groups
    ADD can_reject_receipt TINYINT(1) DEFAULT 0 NULL;

CREATE TABLE IF NOT EXISTS `flight_log`
(
    `id`         INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `flight_id`  INT          NULL,
    `content`    LONGTEXT     NULL,
    `created_at` DATETIME     NULL,
    `created_by` INT          NULL,
    PRIMARY KEY (`id`),
    INDEX `index2` (`flight_id` ASC),
    INDEX `index3` (`created_at` ASC),
    INDEX `index4` (`created_by` ASC)
)
    ENGINE = InnoDB;

ALTER TABLE tour
    ADD log_id_ost_tour int(10);
ALTER TABLE tour
    ADD departure_date DATETIME NOT NULL;

alter table booking_tmp
    add twin_room tinyint default 0 null;

alter table booking_tmp
    add double_room tinyint default 0 null;

alter table booking_tmp
    add triple_room tinyint default 0 null;

alter table booking_tmp
    add single_room tinyint default 0 null;

alter table booking_tmp
    add other_room varchar(50) null;

alter table booking_tmp
    add referral_code int unsigned null;

create table booking_tmp_log
(
    id        int unsigned auto_increment,
    ticket_id int unsigned null,
    staff_id  int unsigned null,
    content   longtext     null,
    time      datetime     not null,
    constraint booking_tmp_log_pk
        primary key (id)
);
/*branch: booking-form*/

alter table booking_reservation_history
    add visa_review_at datetime null;

alter table booking_reservation_history
    add visa_review_by int null;

alter table tugo_osticket_customer_age
    add destination varchar(32) null;

alter table tugo_osticket_customer_age
    add market varchar(32) null;

alter table tugo_osticket_customer_age
    add tour_name int null after amount_pax;

alter table tugo_osticket_customer_age
    change destination tour_destination varchar(32) null;

alter table tugo_osticket_customer_age
    change market tour_market varchar(32) null;

alter table tugo_osticket_customer_age
    modify tour_name varchar(32) null;

alter table tugo_osticket_customer_age
    add tour_id int null;

alter table tugo_osticket_customer_age
    add tour_destination_id int null;

alter table tugo_osticket_customer_age
    add tour_market_id int null;

alter table tugo_osticket_customer_age
    drop key index2;

alter table tugo_osticket_customer_age
    add constraint index2
        unique (day_of_week_name, year, month_of_year, month_of_the_year, week_of_year, week_of_the_year, day_of_week,
                day_of_month, date, age_pax, tour_id, tour_destination_id, tour_market_id);

alter table tugo_osticket_customer_age
    modify age_pax varchar(7) null;

alter table tugo_osticket_call
    add duration int null;

alter table tugo_osticket_call
    change duration billduration int null;

alter table tugo_osticket_call
    add direction varchar(8) null;

alter table tugo_osticket_call
    change phone_number callernumber varchar(12) null;

alter table tugo_osticket_call
    add destinationnumber int null;

alter table tugo_osticket_call
    add disposition varchar(12) null;

alter table tugo_osticket_call
    drop key index2;

alter table tugo_osticket_call
    add constraint index2
        unique (day_of_week_name, year, month_of_year, month_of_the_year, minute, week_of_year, week_of_the_year, hour,
                day_of_week, day_of_month, hour_of_day, date, callernumber, direction, destinationnumber, disposition);

alter table tugo_osticket_sms
    add type varchar(64) null;

alter table tugo_osticket_sms
    drop key index2;

alter table tugo_osticket_sms
    add constraint index2
        unique (day_of_week_name, year, month_of_year, month_of_the_year, minute, week_of_year, week_of_the_year, hour,
                day_of_week, day_of_month, hour_of_day, date, phone_number, type);

alter table tugo_osticket_booking
    add tour_market_id int null;

alter table tugo_osticket_booking
    add tour_destination_id int null;

alter table tugo_osticket_booking
    add tour_destination varchar(64) null;

alter table tugo_osticket_booking
    add tour_market varchar(64) null;

alter table tugo_osticket_booking
    drop key index2;

alter table tugo_osticket_booking
    add constraint index2
        unique (day_of_week_name, year, month_of_year, month_of_the_year, minute, week_of_year, week_of_the_year, hour,
                day_of_week, day_of_month, hour_of_day, date, minute_of_day, staff_id, tour_market_id,
                tour_destination_id);


drop table if exists tugo_osticket_customer;
-- auto-generated definition
create table tugo_osticket_customer
(
    id                  bigint auto_increment
        primary key,
    date                varchar(8)           null comment 'YYYYMMDD',
    day_of_month        varchar(2)           null comment 'DD',
    day_of_week         varchar(1)           null comment 'D DAY_OF_WEEK',
    week_of_the_year    varchar(2)           null comment 'WEEK',
    week_of_year        varchar(6)           null comment 'YEAR_WEEK',
    month_of_the_year   varchar(2)           null comment 'MONTH',
    month_of_year       varchar(6)           null comment 'YEAR_MONTH',
    year                varchar(4)           null comment 'YEAR',
    day_of_week_name    varchar(10)          null comment 'DAY_OF_WEEK_NAME',
    passport_no         varchar(12)          null,
    full_name           varchar(32)          null,
    total_retail_price  decimal(12) unsigned null comment 'total money của số điện thoại và booking này',
    total_quantity      mediumint(3)         null comment 'tổng số khách của số điện thoại và booking này',
    phone_number        varchar(10)          null,
    booking_code        varchar(16)          null comment 'TG123-123321 (bỏ số 0 đầu)',
    tour_destination_id int                  null,
    tour_market_id      int                  null,
    tour_id             int                  null,
    dob                 int                  null,
    tour_name           varchar(64)          null,
    tour_destination    varchar(64)          null,
    tour_market         varchar(64)          null,
    constraint tugo_osticket_xxid_uindex
        unique (phone_number, booking_code, tour_destination_id, tour_market_id, tour_id, passport_no, date)
);

alter table tugo_osticket_call
    modify id bigint auto_increment;

alter table tugo_osticket_booking
    modify id bigint auto_increment;
alter table tugo_osticket_customer_age
    modify id bigint auto_increment;
alter table tugo_osticket_sms
    modify id bigint auto_increment;
alter table tugo_osticket_ticket
    modify id bigint auto_increment;

alter table tugo_osticket_customer
    change dob dob_year int null;

alter table tugo_osticket_customer
    add dob_string varchar(10) null;

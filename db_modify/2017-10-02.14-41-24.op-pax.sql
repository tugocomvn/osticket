ALTER TABLE ost_pax_info ADD nationality TINYINT NULL COMMENT '0: VN, 1: other';
CREATE INDEX ost_pax_info_nationality_index ON ost_pax_info (nationality);
ALTER TABLE ost_pax_info
  MODIFY COLUMN nationality TINYINT COMMENT '0: VN, 1: other' AFTER doe;
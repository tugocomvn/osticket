create table tour_export_log
(
	id int auto_increment,
	tour_id int null,
	content longtext null,
	filename text null,
	created_by int null,
	created_at datetime null,
	constraint tour_export_log_pk
		primary key (id)
);


create table booking_reservation_import_log
(
    id int unsigned auto_increment,
    tour_id int null,
    content longtext null,
    created_at datetime null,
    created_by int null,
    constraint table_name_pk
        primary key (id)
);


drop index ost_ticket_count_ticket_id_index on ost_ticket_count;
alter table ost_ticket_count drop  primary key;
alter table ost_ticket_count drop column id;

create unique index ost_ticket_count_ticket_id_uindex
	on ost_ticket_count (ticket_id);

alter table ost_ticket_count
	add constraint ost_ticket_count_pk
		primary key (ticket_id);

alter table ost_groups
	add can_all_receipt tinyint(1) unsigned default 0 null;

/*branch: 24.4.5*/
alter table ost_groups change can_all_receipt can_manage_all_receipt tinyint(1) unsigned default 0 null;


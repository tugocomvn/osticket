ALTER TABLE tour_pax_visa_profile_history CHANGE `tour_pax_visa_profile_history_id` `tour_pax_visa_profile_id` INT NULL;
ALTER TABLE tour_pax_visa_profile_history
ADD `created_by` INT NULL;

ALTER TABLE visa_history_action_log
ADD `created_by` INT NULL;

ALTER TABLE ost_pax_tour_history
ADD `created_by` INT NULL;

ALTER TABLE visa_information_action_log
ADD `created_by` INT NULL;

ALTER TABLE visa_profile_appointment_action_log
ADD `created_by` INT NULL;

DROP TABLE IF EXISTS `visa_profile_sms_log`;
CREATE TABLE IF NOT EXISTS `visa_profile_sms_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` INT NULL,
  `content` LONGTEXT NULL,
  `phone_number` VARCHAR(12) NULL,
  `created_by` INT NULL,
  `send_time` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`profile_id` ASC),
  INDEX `index4` (`phone_number` ASC),
  INDEX `index5` (`created_by` ASC),
  INDEX `index6` (`send_time` ASC))
ENGINE = InnoDB;
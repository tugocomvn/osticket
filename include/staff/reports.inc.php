<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
?>

<h2><?php echo __('System Reports');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>
<div id='filter' >
    <form action="reports.php" method="post">
        <div style="padding-left:2px;">
            <input type="submit" Value="<?php echo __('Export');?>" />
            <?php csrf_token(); ?>

            <select name='do'>
                <option value="ticket_history" selected><?php echo __('Ticket History');?></option>
            </select>

            For

            <input type="number" name="days" step="1" value="7" size="2"> days
        </div>
    </form>
</div>
create table tour_net_price
(
    id           int auto_increment
        primary key,
    tour_id      int      not null,
    quantity     int      not null,
    retail_price decimal(12,0)  null,
    created_by   int      not null,
    created_at   datetime not null
);
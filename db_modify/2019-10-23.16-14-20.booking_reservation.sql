ALTER TABLE `ost_groups` ADD `can_cancel_member_reservation` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_cancel_leader_reservation` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_cancel_super_reservation` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

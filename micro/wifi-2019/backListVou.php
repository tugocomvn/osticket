<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__."/Pagination.php";
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';

$code = "";
$total = 0;
$page = 1;
$offset = 0;

if (isset($_REQUEST['code'])) {
    $code = $_REQUEST['code'];
    $code = strtoupper($code);
}

if (isset($_REQUEST['page'])){
    $page = $_REQUEST['page'];
}

$total = Awing::getTotalData($code);

//PHAN TRANG
$config = [
    'code'   => $code,
    'total' => $total,
    'limit' => PAGE_LIMIT,
    'full' => false,
    'querystring' => 'page'
];
$offset = ($page - 1)*PAGE_LIMIT;
$results = Awing::getListLimit($code,$offset);

require_once ("listVouGroup.php");

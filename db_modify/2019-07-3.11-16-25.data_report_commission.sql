--
-- Đang đổ dữ liệu cho bảng `commission_checking`
--

INSERT INTO `commission_checking` (`id`, `staff_create_id`, `staff_id_of_report`, `month_of_report`, `year_of_report`, `date_create`, `note`) VALUES
(14, 4, 3, 1, 2018, '2019-06-21 14:04:33', 'asd'),
(23, 13, 65, 1, 2019, '2019-06-24 10:25:21', 'Note 1025'),
(24, 13, 1, 1, 2018, '2019-06-24 10:28:49', 'Note 1028'),
(25, 13, 88, 1, 2018, '2019-06-30 21:52:58', '30-Jun'),
(26, 13, 55, 1, 2019, '2019-07-01 09:49:09', 'note 1 Jul'),
(27, 13, 55, 1, 2019, '2019-07-01 09:51:20', '1 Jul 9:51'),
(28, 13, 37, 1, 2019, '2019-07-01 09:58:07', 'aaaa'),
(29, 13, 72, 1, 2019, '2019-07-01 14:17:19', 'asVDdb'),
(30, 13, 68, 1, 2019, '2019-07-01 14:37:23', 'enremnz'),
(31, 13, 1, 1, 2019, '2019-07-01 15:10:19', 'ABESWBEWb\r\nsaLBKVOWE\r\nNDBNV'),
(32, 13, 102, 1, 2019, '2019-07-01 16:40:05', 'evreb'),
(33, 13, 55, 1, 2019, '2019-07-02 13:44:39', 'ewve'),
(34, 13, 18, 1, 2019, '2019-07-02 16:53:58', 'dwqfewg'),
(35, 13, 13, 1, 2019, '2019-07-02 17:00:30', 'savesbv'),
(36, 13, 55, 1, 2019, '2019-07-02 22:08:31', 'wvBW'),
(37, 13, 55, 5, 2019, '2019-07-03 09:25:20', 'w4h5'),
(38, 13, 55, 6, 2019, '2019-07-03 10:01:54', 'ewberb'),
(39, 13, 55, 4, 2019, '2019-07-03 10:23:01', 'EWHBWHb'),
(40, 13, 55, 4, 2019, '2019-07-03 10:23:22', 'EWHBWHb'),
(41, 13, 55, 3, 2019, '2019-07-03 10:24:46', 'vwqvEW'),
(42, 13, 72, 4, 2019, '2019-07-03 10:25:20', 'vewvewb'),
(43, 13, 55, 1, 2019, '2019-07-03 10:25:53', 'ebareb'),
(44, 13, 55, 4, 2019, '2019-07-03 10:27:14', 'ewgHEHre'),
(45, 13, 55, 3, 2018, '2019-07-03 10:27:34', 'ebwewb'),
(46, 13, 55, 4, 2018, '2019-07-03 10:51:52', 'ewvewb');

--
-- Đang đổ dữ liệu cho bảng `commission_checking_booking`
--

INSERT INTO `commission_checking_booking` (`booking_id`, `booking_code`, `total_retail_price`, `total_quantity`, `user_id`, `report_id`, `note`) VALUES
(2, 'TG123-0019090', 37500000, 3, 3, 23, 'wbciiewv weverv rhrtjrt'),
(3, 'TG123-0019092', 67500000, 5, 4, 23, 'Ccwsa ervrev ewvewv'),
(6, 'TG123-0019088', 2000000, 1, 68, 24, 'ewvvew'),
(29, 'TG123-0019087', 5000000, 1, 68, 24, 'tuấn'),
(31, 'TG123-0019081', 28000000, 2, 32, 24, ' tuấn'),
(32, 'TG123-0019082', 25000000, 2, 5, 24, 'Tuấn'),
(33, 'TG123-0019086', 22500000, 1, 68, 24, '19086'),
(41, 'TG123-0019078', 67500000, 5, 51, 25, 'aaaaa'),
(45, 'TG123-0019080', 15750000, 4, 80, 25, 'SVebewbew'),
(83, 'TG123-0019076', 0, 4, 2, 26, 'AAAAA drntrnj'),
(84, 'TG123-0019073', 42000000, 3, 32, 27, 'AAAda bsd '),
(108, 'TG123-0019071', 7100000, 2, 66, 28, 'dawfregewrg'),
(109, 'TG123-0019072', 3600000, 3, 11, 28, 'egreb'),
(112, 'TG123-0019067', 74000000, 2, 68, 29, 'wLQOIViogwqvphq3hvps 1`73t8`t9488489`648915'),
(113, 'TG123-0019070', 54000000, 2, 68, 29, 'rhrtntrj'),
(124, 'TG123-0019064', 47600000, 2, 34, 30, 'wvgw ewvewv'),
(125, 'TG123-0019065', 24000000, 4, 66, 30, 'wqbqeb'),
(154, 'TG123-0019061', 47070000, 3, 10, 31, 'wvqqfew ewgvew'),
(155, 'TG123-0019062', 67500000, 3, 21, 31, 'bewufugviwg'),
(156, 'TG123-0019058', 2000000, 1, 19, 32, 'qwfwqvwvwev ewgrweg'),
(157, 'TG123-0019059', 166200000, 6, 34, 32, 'eBGGeb'),
(158, 'TG123-0019057', 5000000, 1, 19, 14, 'avebwbew dsv eragber'),
(159, 'TG123-0009995', 13200000, 2, 11, 36, 'wvqv'),
(160, 'TG123-0009996', 37500000, 3, 7, 36, ''),
(161, 'TG123-0010002', 12000000, 2, 32, 36, ''),
(162, 'TG123-0019053', 25800000, 1, 34, 38, '^ADAD'),
(163, 'TG123-0019051', 19400000, 2, 10, 45, 'ry,ldt'),
(164, 'TG123-0019052', 84000000, 4, 10, 45, 'rkytdlkt');

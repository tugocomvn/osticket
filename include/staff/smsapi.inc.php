<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
global $cfg;
$qs = array();
$qwhere =' WHERE 1  ';
$type = null;
//dates
if (!isset($_REQUEST['startDate']) || !$_REQUEST['startDate'])
    $_REQUEST['startDate'] = date('Y-m-d', time()-30*24*3600);

if (!isset($_REQUEST['endDate']) || !$_REQUEST['endDate'])
    $_REQUEST['endDate'] = date('Y-m-d', time());

$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere.=' AND DATE(send_time)>='.db_input(date('Y-m-d', $startTime));
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere.=' AND DATE(send_time)<='.db_input(date('Y-m-d', $endTime));
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}

$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
if ($type) {
    $qs['type'] = $type;

    if ($type == 'Ticket') {
        $qwhere .= ' AND (`type` like \'\' OR `type` = '.db_input($type).') ';
    } else {
        $qwhere .= ' AND `type` = '.db_input($type);
    }
}

$sortOptions=array(
    'id'=>'id',
    'content'=>'content',
    'phone_number'=>'phone_number',
    'send_time'=>'send_time',
    'type'=>'type',
);
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'id';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'log.send_time';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$qselect = 'SELECT * ';
$qfrom=' FROM '.SMS_LOG_TABLE.' log ';

$total=db_count("SELECT count(*) $qfrom $qwhere");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
//pagenate
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('sms-api.php',$qs);
$qs += array('order' => ($order=='DESC' ? 'ASC' : 'DESC'));
$qstr = '&amp;'. Http::build_query($qs);
$query="$qselect $qfrom $join $qwhere ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' '.$title;
else
    $showing=__('No logs found!');
?>

<h2><?php echo __('SMS API Logs');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>

<div id='filter' >
    <form action="sms-api.php" method="get">
        <div style="padding-left:2px;">
            <b><?php echo __('Date Span'); ?></b>&nbsp;<i class="help-tip icon-question-sign" href="#date_span"></i>
            <?php echo __('Between'); ?>:
            <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
            &nbsp;&nbsp;
            <input class="dp input-field" id="ed" size=15 name="endDate" value="<?php echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>&nbsp;&nbsp;

            <select name="type" id="type" class="input-field">
                <option value=>-- All --</option>
                <?php
                $_sql = sprintf("select distinct if(type is null or type like '', 'Ticket', `type`) as type from %s ", SMS_LOG_TABLE);
                $_res = db_query($_sql);
                if ($_res) {
                    while (($row = db_fetch_array($_res))) {
                        ?>
                        <option value="<?php echo $row['type'] ?>"
                        <?php if ($type && $type == $row['type']) echo 'selected' ?>
                        ><?php echo $row['type'] ?></option>
                        <?php
                    }
                }
                ?>
            </select>

            <input type="submit" class="btn_sm btn-default" Value="<?php echo __('Go!');?>" />
        </div>
    </form>
</div>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1050">
    <caption><?php echo $showing; ?></caption>
    <thead>
    <tr>
        <th>#</th>
        <th nowrap><a <?php echo $phone_number_sort; ?> href="sms-api.php?<?php echo $qstr; ?>&sort=phone_number"><?php echo __('Phone Number');?></a></th>
        <th nowrap><a <?php echo $content_sort; ?> href="sms-api.php?<?php echo $qstr; ?>&sort=content"><?php echo __('Content');?></a></th>
        <th><a  <?php echo $send_time_sort; ?> href="sms-api.php?<?php echo $qstr; ?>&sort=send_time"><?php echo __('Send Time');?></a></th>
        <th><a  <?php echo $type_sort; ?>href="sms-api.php?<?php echo $qstr; ?>&sort=type"><?php echo __('Type');?></a></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $num = 0;
    if($res && db_num_rows($res)):
        while ($row = db_fetch_array($res)) {?>
            <tr id="<?php echo $row['id']; ?>">
                <td>
                    <?php echo ($pageNav->getStart() + $num + 1); $num++;   ?>
                    <form action="tickets.php" method="get" target="_blank">
                        <?php csrf_token(); ?>
                        <input type="hidden" name="a" value="search">
                        <input type="hidden" name="query" value="<?php echo $row['phone_number'] ?>">
                        <input type="submit" value="&rarr;">
                    </form>
                </td>
                <td>
                    <span><?php echo $row['phone_number'] ?></span>

                    <?php if(isset($row['type']) && 'Ticket'===$row['type'] && isset($row['ticket_id']) && $row['ticket_id']): ?>
                        <a target="_blank" class="no-pjax" href="/scp/tickets.php?id=<?php echo $row['ticket_id'] ?>">Ticket ID: <?php echo $row['ticket_id'] ?></a>
                    <?php endif; ?>

                    <?php if(isset($row['type']) && strpos($row['type'], 'AutoAction') !== false && isset($row['ticket_id']) && $row['ticket_id']): ?>
                        <a target="_blank" class="no-pjax" href="#">AutoContent LogID: <?php echo $row['ticket_id'] ?></a>
                    <?php endif; ?>
                </td>
                <td><?php echo $row['content'] ?></td>
                <td><?php echo Format::date($cfg->getDateTimeFormat(), strtotime($row['send_time'])+($cfg->getDBTZoffset()*3600)); ?></td>
                <td><?php echo $row['type']; ?></td>
            </tr>
            <?php
        } //end of while.
    endif; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="9">
            <?php if(!$res || !$num){
                echo __('No logs found');
            } ?>
        </td>
    </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>

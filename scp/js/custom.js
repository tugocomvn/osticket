function ost_get_parent_iframe_name() {
    var ifs = window.top.document.getElementsByTagName("iframe");
    for(var i = 0, len = ifs.length; i < len; i++)  {
        var f = ifs[i];
        var fDoc = f.contentDocument || f.contentWindow.document;
        if(fDoc === document)   {
            return f.getAttribute('name');
        }
    }
}

function ost_set_parent_iframe_height() {
    h = ost_get_document_content_height();
    h = h*1.1;
    parent.$('[name='+ost_get_parent_iframe_name()+']').css('height', h+'px');
}

function ost_get_document_content_height() {
    if ($('body'))
        return $('body').height();
    return 0;
}
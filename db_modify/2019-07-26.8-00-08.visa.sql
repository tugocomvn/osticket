CREATE TABLE `osticket`.`visa_profile_appointment` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `tour_pax_visa_profile_id` INT(10) NOT NULL , `date_event` DATETIME NOT NULL , `visa_appointment_id` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `visa_appointment` (
  `id` int(10) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `staff_id_create` int(10) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staff_id_edit` int(10) NOT NULL,
  `date_edit` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

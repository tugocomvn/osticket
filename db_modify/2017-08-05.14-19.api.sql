CREATE TABLE api_user
(
  uuid VARCHAR(16) PRIMARY KEY,
  customer_number VARCHAR(10),
  customer_name VARCHAR(32),
  phone_number VARCHAR(16),
  register_at DATETIME,
  is_logged_in TINYINT(1) DEFAULT 0,
  last_active DATETIME
);
CREATE UNIQUE INDEX api_user_customer_number_uindex ON api_user (customer_number);
CREATE UNIQUE INDEX api_user_phone_number_uindex ON api_user (phone_number);
CREATE INDEX api_user_register_at_index ON api_user (register_at);
CREATE INDEX api_user_last_active_index ON api_user (last_active);
CREATE INDEX api_user_is_logged_in_index ON api_user (is_logged_in);
ALTER TABLE api_user DROP is_logged_in;

CREATE TABLE api_otp
(
  phone_number VARCHAR(16) PRIMARY KEY,
  otp VARCHAR(6),
  expires DATETIME
);
CREATE INDEX api_otp_otp_index ON api_otp (otp);
CREATE INDEX api_otp_expires_index ON api_otp (expires);

CREATE TABLE api_device
(
  user_uuid VARCHAR(16),
  device_id VARCHAR(16) COMMENT 'send notification',
  is_logged_in TINYINT(1),
  CONSTRAINT api_device_user_uuid_device_id_pk PRIMARY KEY (user_uuid, device_id)
);
CREATE INDEX api_device_is_logged_in_index ON api_device (is_logged_in);
ALTER TABLE api_device ALTER COLUMN is_logged_in SET DEFAULT 0;

CREATE TABLE api_device_log
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  device_id VARCHAR(16),
  user_uuid VARCHAR(16),
  action_name VARCHAR(64),
  action_data TEXT,
  log_time DATETIME
);

ALTER TABLE api_device CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE api_device_log CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE api_otp CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE api_user CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

CREATE TABLE api_refresh_token
(
  user_uuid VARCHAR(16),
  device_id VARCHAR(16),
  refresh_token VARCHAR(32),
  issued_at INT,
  expired_at INT,
  CONSTRAINT api_refresh_token_user_uuid_device_id_pk PRIMARY KEY (user_uuid, device_id)
);
CREATE UNIQUE INDEX api_refresh_token_refresh_token_uindex ON api_refresh_token (refresh_token);
CREATE INDEX api_refresh_token_issued_at_index ON api_refresh_token (issued_at);
CREATE INDEX api_refresh_token_expired_at_index ON api_refresh_token (expired_at);

ALTER TABLE api_refresh_token CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE api_user ADD user_point INT NULL;
ALTER TABLE api_user ALTER COLUMN user_point SET DEFAULT 0;
ALTER TABLE api_user MODIFY user_point INT(11) NOT NULL DEFAULT 0;
CREATE INDEX api_user_user_point_index ON api_user (user_point);


CREATE TABLE api_user_point_history
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  time DATETIME,
  updater VARCHAR(16),
  updater_id INT,
  user_uuid VARCHAR(16),
  subject VARCHAR(255),
  change_point INT,
  note TEXT
);

ALTER TABLE api_user_point_history CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
CREATE INDEX api_user_point_history_user_uuid_index ON api_user_point_history (user_uuid);
CREATE INDEX api_user_point_history_updater_id_updater_index ON api_user_point_history (updater_id, updater);
CREATE INDEX api_user_point_history_change_point_index ON api_user_point_history (change_point);
CREATE INDEX api_user_point_history_time_index ON api_user_point_history (time);
ALTER TABLE api_refresh_token MODIFY issued_at DATETIME;
ALTER TABLE api_refresh_token MODIFY expired_at DATETIME;

ALTER TABLE ost_sms_log ADD status TINYINT DEFAULT 1 NOT NULL;

DROP INDEX api_otp_otp_index ON api_otp;
CREATE INDEX api_otp_otp_phone_number_index ON api_otp (otp, phone_number);
ALTER TABLE api_otp DROP PRIMARY KEY;
ALTER TABLE api_otp ADD id INT NULL PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE api_otp
  MODIFY COLUMN id INT AUTO_INCREMENT FIRST;
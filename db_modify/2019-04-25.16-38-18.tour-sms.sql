alter table ost_tour
    add country_id int null;

create index ost_tour_country_id_index
    on ost_tour (country_id);


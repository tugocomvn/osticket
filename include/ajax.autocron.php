<?php  ?>

<script async>
(function($) {
    fetch_mail_cron();

    function fetch_mail_cron() {
        $.get(
            '/scp/autocron.php',
            function(response) {
                if (response) {
                    a=document.createElement('script');
                    m=document.getElementsByTagName('script')[0];
                    a.type = "text/javascript";
                    a.text = response;
                    m.parentNode.insertBefore(a,m);
                }

                setTimeout(fetch_mail_cron, 3*60*1000);
            }
        );
    }

})(jQuery);
</script>

<?php  ?>
<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/reservation.css">

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs"  id="myTab">
        <li class="active">
            <a href="#opreview" data-toggle="tab">Operator Review
                <?php if($opReview && ($rows = db_num_rows($opReview))): ?>
                    <span class="label label-warning"><?php echo $rows ?></span>
                <?php endif; ?>
            </a>
        </li>
        <li class="">
            <a href="#opreviewed" data-toggle="tab">Operator Reviewed</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="opreview">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.opreview.inc.php' ?>
            </div>
        </div>
        <div class="tab-pane" id="opreviewed">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.opreviewed.inc.php' ?>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        $(document).ready(function () {
            if (window.location.hash === '')
                activeTab('opreview');
            else
                activeTab(window.location.hash.replace('#', ''));
        });

        function activeTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        }

        $('#myTab a').click(function (e) {
            e.preventDefault();
            window.location.hash = $(e.target).attr("href");
            $(this).tab('show');
        });
    })(jQuery);
</script>


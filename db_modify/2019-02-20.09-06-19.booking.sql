alter table booking_tmp
  add booking_type_id int null;

create index booking_tmp_booking_type_id_index
  on booking_tmp (booking_type_id);

alter table booking_tmp modify booking_type_id int null after booking_type;


ALTER TABLE ost_groups ADD can_view_offlate TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_create_offlate TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_edit_offlate TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_accept_offlate TINYINT(1) DEFAULT 0 NULL;
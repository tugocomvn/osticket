ALTER TABLE api_user ADD rank TINYINT NULL;
CREATE INDEX api_user_rank_index ON api_user (rank);

CREATE TABLE api_user_rank
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(64),
  description TEXT
);
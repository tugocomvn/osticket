alter table mkt_group_voucher modify name varchar(128) collate utf8_unicode_ci not null;

alter table mkt_group_voucher modify content text collate utf8_unicode_ci not null;

ALTER TABLE `mkt_user` CHANGE `customer_name` `customer_name` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
<?php
namespace Booking;

require_once(INCLUDE_DIR . 'class.orm.php');

class StatusModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_STATUS_TABLE,
        'pk' => [
            'booking_ticket_id'
        ],
    ];
}

class StatusListModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_STATUS_LIST_TABLE,
        'pk' => ['id'],
        'ordering' => ['name'],
    ];
}

class StatusLogModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_STATUS_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class Status extends StatusModel {

}
class StatusLog extends StatusLogModel {}

class StatusList extends StatusListModel {
    public static function getAll(&$default, &$finished) {
        $sql = 'SELECT * FROM '.BOOKING_STATUS_LIST_TABLE.' ORDER BY `name`';
        $res = db_query($sql);
        $data = [];
        while ($res && ($row = db_fetch_array($res))) {
            if (isset($row['is_default']) && $row['is_default'])
                $default = $row['id'];
            if (isset($row['is_finished']) && $row['is_finished'])
                $finished = $row['id'];

            $data[ $row['id'] ] = $row;
        }

        return $data;
    }
}

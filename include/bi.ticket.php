<?php
namespace BI;
require_once(INCLUDE_DIR . 'class.orm.php');

class TicketFollowModel extends \VerySimpleModel {
    static $meta = [
        'table' => TICKET_FOLLOW_TABLE,
        'pk' => ['ticket_id'],
    ];
}

class TicketFollow extends TicketFollowModel {

}

class TicketFollowActionModel extends \VerySimpleModel {
    static $meta = [
        'table' => TICKET_FOLLOW_ACTION_TABLE,
        'pk' => ['id'],
    ];
}

class TicketFollowAction extends TicketFollowActionModel {

}

class TicketInfoModel extends \VerySimpleModel {
    static $meta = [
        'table' => TICKET_INFO_TABLE,
        'pk' => ['ticket_id', 'object_id', 'object_type'],
    ];
}

class TicketInfo extends TicketInfoModel {

}

class TicketAnalyticsModel extends \VerySimpleModel {
    static $meta = [
        'table' => TICKET_ANALYTICS_TABLE,
        'pk' => ['date', 'source', 'tag', 'status'],
    ];
}

class TicketAnalytics extends TicketAnalyticsModel {
    public static $source_name = [
        1 => 'Facebook',
        2 => 'Website',
        3 => 'Other',
        4 => 'Email',
        5 => 'Phone',
    ];

    public static function analytics($from, $to) {

    }

    public static function overview($from, $to) {

    }

    public static function tag($from, $to) {

    }

    public static function source($from, $to) {

    }

    public static function status($from, $to) {

    }
}

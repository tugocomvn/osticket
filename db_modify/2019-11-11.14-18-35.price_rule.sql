CREATE TABLE IF NOT EXISTS `price_rule` (
  `id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(64) NOT NULL,
  `tour_size_greater_eq_than` MEDIUMINT(3) NOT NULL DEFAULT 0,
  `net_price` DECIMAL(12,0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `index2` (`name` ASC))
ENGINE = InnoDB;
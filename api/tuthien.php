<?php
require_once __DIR__.'/tuthien.cnf.php';

echo "START: Read tuthien.csv file ".date("Y-m-d H:i:s")."\r\n";

$file_name = __DIR__.'/../public_file/tuthien-web.csv';
$remote_file = 'http://support.tugo.com.vn/public_file/tuthien.csv';
getRemoteFile($remote_file, $file_name);

///
$fp = @fopen($file_name, 'r');
if (!$fp) return false;

if (!feof($fp))
    $header = fgetcsv($fp);
else return;

$header_ = [];
foreach ($header as $key => $value) {
    $header_[ preg_replace('/[^a-z_]/', '', $value) ] = trim( $key );
}

// ini_set('display_errors', 0);
// error_reporting(~E_ALL);

// Make the connnection and then select the database.
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT);

/* check connection */
if (!$mysqli || $mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$mysqli->query("SET NAMES 'utf8'");
$mysqli->query("USE " . DB_NAME);

$sql_insert = "REPLACE INTO mkt_tuthien(
    pax_id,
    tour_id,
    full_name,
    dob,
    tour_name,
    gather_date
    ) VALUES ";

$flag = false;
while(!feof($fp)) {
    $row = fgetcsv($fp);

    if (!date_create_from_format('Y-m-d', $row[ $header_['dob'] ])) continue;
    if (!date_create_from_format('Y-m-d H:i:s', $row[ $header_['gather_date'] ])) continue;
    $name = substr(trim($row[ $header_['full_name'] ]), 0, 64);
    $tour = substr(trim($row[ $header_['tour_name'] ]), 0, 64);

    $sql_insert .= sprintf(
        "(
                       %d, 
                       %d, 
                       %s, 
                       %s, 
                       %s, 
                       %s
                    ),",
        intval($row[ $header_['pax_id'] ]),
        intval($row[ $header_['tour_id'] ]),
        "'".$name."'",
        "'".$row[ $header_['dob'] ]."'",
        "'".$tour."'",
        "'".$row[ $header_['gather_date'] ]."'"
    );

    $flag = true;
}

fclose($fp);

if ($flag) {
    $sql_insert = trim($sql_insert, ',');
//    echo $sql_insert."\r\n";
    $mysqli->query($sql_insert);
}

echo "DONE: Read tuthien.csv file ".date("Y-m-d H:i:s")."\r\n";

function getRemoteFile($url, $file_name)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);

    if($result !== FALSE)
    {
        try {
            file_put_contents($file_name, $result);
        } catch (Exception $ex) { echo $ex->getMessage()."\r\n"; }

        return true;
    }
    else
    {
        return false;
    }
}


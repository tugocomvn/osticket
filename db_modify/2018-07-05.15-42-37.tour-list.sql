create table ost_tour
(
  id int auto_increment
    primary key,
  name varchar(255) null,
  destination int null,
  notes text null,
  status tinyint(2) null,
  created_at datetime null,
  created_by int null,
  last_updated_at datetime null,
  last_updated_by int null,
  retail_price int null,
  net_price int null,
  transit_airport int null,
  gateway int null,
  departure_flight_code varchar(10) null,
  departure_flight_code_b varchar(10) null,
  return_flight_code varchar(10) null,
  return_flight_code_b varchar(10) null,
  arrival_time datetime null,
  arrival_time_b datetime null,
  departure_date datetime null,
  gather_date datetime null,
  return_date datetime null,
  airline int null,
  tour_leader int null
)
;

create index ost_tour_airline_index
  on ost_tour (airline)
;

create index ost_tour_arrival_time_b_index
  on ost_tour (arrival_time_b)
;

create index ost_tour_arrival_time_index
  on ost_tour (arrival_time)
;

create index ost_tour_departure_date_index
  on ost_tour (departure_date)
;

create index ost_tour_departure_flight_code_b_index
  on ost_tour (departure_flight_code_b)
;

create index ost_tour_departure_flight_code_index
  on ost_tour (departure_flight_code)
;

create index ost_tour_destination_index
  on ost_tour (destination)
;

create index ost_tour_gateway_index
  on ost_tour (gateway)
;

create index ost_tour_gather_date_index
  on ost_tour (gather_date)
;

create index ost_tour_name_index
  on ost_tour (name)
;

create index ost_tour_return_date_index
  on ost_tour (return_date)
;

create index ost_tour_return_flight_code_b_index
  on ost_tour (return_flight_code_b)
;

create index ost_tour_return_flight_code_index
  on ost_tour (return_flight_code)
;

create index ost_tour_tour_leader_index
  on ost_tour (tour_leader)
;

ALTER TABLE ost_tour ADD quantity tinyint NULL;

ALTER TABLE ost_tour CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

CREATE INDEX ost_tour_quantity_index ON ost_tour (quantity);
ALTER TABLE ost_tour ADD hold_quantity tinyint(4) NULL;
CREATE INDEX ost_tour_hold_quantity_index ON ost_tour (hold_quantity);
ALTER TABLE ost_tour ADD sure_quantity tinyint(4) NULL;
CREATE INDEX ost_tour_sure_quantity_index ON ost_tour (sure_quantity);
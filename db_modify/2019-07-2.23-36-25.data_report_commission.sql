-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 02, 2019 lúc 01:28 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `osticket`
--

-- --------------------------------------------------------

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `staff_create_id` int(11) NOT NULL,
  `staff_id_of_report` int(11) NOT NULL,
  `month_of_report` tinyint(4) NOT NULL,
  `year_of_report` int(4) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `report`
--

INSERT INTO `report` (`id`, `staff_create_id`, `staff_id_of_report`, `month_of_report`, `year_of_report`, `date_create`, `note`) VALUES
(14, 4, 3, 1, 2018, '2019-06-21 14:04:33', 'asd'),
(23, 13, 65, 1, 2019, '2019-06-24 10:25:21', 'Note 1025'),
(24, 13, 1, 1, 2018, '2019-06-24 10:28:49', 'Note 1028'),
(25, 13, 88, 1, 2018, '2019-06-30 21:52:58', '30-Jun'),
(26, 13, 55, 1, 2019, '2019-07-01 09:49:09', 'note 1 Jul'),
(27, 13, 55, 1, 2019, '2019-07-01 09:51:20', '1 Jul 9:51'),
(28, 13, 37, 1, 2019, '2019-07-01 09:58:07', 'aaaa'),
(29, 13, 72, 1, 2019, '2019-07-01 14:17:19', 'asVDdb'),
(30, 13, 68, 1, 2019, '2019-07-01 14:37:23', 'enremnz'),
(31, 13, 1, 1, 2019, '2019-07-01 15:10:19', 'ABESWBEWb\r\nsaLBKVOWE\r\nNDBNV'),
(32, 13, 102, 1, 2019, '2019-07-01 16:40:05', 'evreb');


--
-- Đang đổ dữ liệu cho bảng `report_booking`
--

INSERT INTO `report_booking` (`booking_id`, `booking_code`, `total_retail_price`, `total_quantity`, `user_id`, `report_id`, `note`) VALUES
(2, 'TG123-0019090', 37500000, 3, 3, 23, ''),
(3, 'TG123-0019092', 67500000, 5, 4, 23, ''),
(6, 'TG123-0019088', 2000000, 1, 68, 24, 'ewvvew'),
(29, 'TG123-0019087', 5000000, 1, 68, 24, 'tuấn'),
(31, 'TG123-0019081', 28000000, 2, 32, 24, ' tuấn'),
(32, 'TG123-0019082', 25000000, 2, 5, 24, 'Tuấn'),
(33, 'TG123-0019086', 22500000, 1, 68, 24, '19086'),
(41, 'TG123-0019078', 67500000, 5, 51, 25, 'aaaaa'),
(45, 'TG123-0019080', 15750000, 4, 80, 25, 'SVebewbew'),
(83, 'TG123-0019076', 0, 4, 2, 26, 'AAAAA'),
(84, 'TG123-0019073', 42000000, 3, 32, 27, 'AAAda bsd '),
(108, 'TG123-0019071', 7100000, 2, 66, 28, 'dawfregewrg'),
(109, 'TG123-0019072', 3600000, 3, 11, 28, 'egreb'),
(112, 'TG123-0019067', 74000000, 2, 68, 29, 'wLQOIViogwqvphq3hvps 1`73t8`t9488489`648915'),
(113, 'TG123-0019070', 54000000, 2, 68, 29, 'rhrtntrj'),
(124, 'TG123-0019064', 47600000, 2, 34, 30, 'wvgw ewvewv'),
(125, 'TG123-0019065', 24000000, 4, 66, 30, 'wqbqeb'),
(154, 'TG123-0019061', 47070000, 3, 10, 31, 'wvqqfew ewgvew'),
(155, 'TG123-0019062', 67500000, 3, 21, 31, 'bewufugviwg'),
(156, 'TG123-0019058', 2000000, 1, 19, 32, 'qwfwqvwvwev'),
(157, 'TG123-0019059', 166200000, 6, 34, 32, 'eBGGeb'),
(158, 'TG123-0019057', 5000000, 1, 19, 14, 'avebwbew');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

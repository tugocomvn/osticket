<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canApproveLeaderReservation())
    die('Access Denied');

$all_staff_list = Staff::getAll();
$all_staff = [];
while ($all_staff_list && ($row = db_fetch_array($all_staff_list))) {
    $all_staff[ $row['staff_id'] ] = $row['username'];
}
?>
<style>
    .border-right{
        border-right: 1px solid;
    }
    a {
        color: #0254EB
    }
    a:visited {
        color: #0254EB
    }
    a.morelink {
        text-decoration:none;
        outline: none;
    }
    .morecontent span {
        display: none;
    }
    .comment {
        width: 400px;
        margin: 10px;
    }
</style>
<table>
    <caption>Sale Leader Review</caption>
    <thead>
    <tr>
        <th class="border-right" colspan="6">Booking</th>
        <th colspan="8">Reservation</th>
    </tr>
    <tr>
        <th>Qty</th>
        <th>Customer/<br>Phone number</th>
        <th>Booking Type</th>
        <th>Sales/Booking Code/Tour</th>
        <th>Note</th>
        <th class="border-right">Price</th>
        <th>Qty</th>
        <th>Customer / <br>Phone number</th>
        <th>Sales</th>
        <th>Note/Update Time</th>
        <th>#</th>
    </tr>
    </thead>
    <tbody>
    <?php $count_item = 0; $i=1; ?>
    <?php while($leaderReview && ($history = db_fetch_array($leaderReview))): $count_item++; ?>
        <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue' ?>">
            <td><?php echo $arr_booking_review[$history['booking_code']]['total_quantity'] ?></td>
            <td><?php echo $arr_booking_review[$history['booking_code']]['customer'] ?>
                <hr>
                <?php echo $arr_booking_review[$history['booking_code']]['phone_number'] ?>
            </td>
            <td>
                <?php echo $arr_booking_review[$history['booking_code']]['booking_type'] ?>
            </td>
            <td>
                <?php if (isset($all_staff[ $arr_booking_review[$history['booking_code']]['staff_id'] ]))
                    echo $all_staff[ $arr_booking_review[$history['booking_code']]['staff_id'] ]; else echo '- Tugo -'; ?>
                <hr>
                <a href="<?php echo $cfg->getUrl().'scp/booking.php?booking_code='.$arr_booking_review[$history['booking_code']]['booking_code'].'#allbooking' ?>"
                   target="_blank" class="no-pjax">
                    <?php echo $arr_booking_review[$history['booking_code']]['booking_code'] ?>
                </a>
                <hr>
                <?php
                    if (isset($history['tour_id']) && $history['tour_id']) {
                        $tour = TourNew::lookup($history['tour_id']);
                        if ($tour) echo $tour->name;
                    } elseif (isset($history['country']) && $history['country']) {
                        $country = TourMarket::lookup($history['country']);
                        if($country)
                            echo 'Visa '.$country->name;
                    }
                ?>
            </td>
            <td class="comment _more">
                <?php echo $arr_booking_review[$history['booking_code']]['note'] ?>
            </td>
            <td  class="border-right">
                <?php echo number_format( $arr_booking_review[$history['booking_code']]['total_retail_price'], 0, '.', '.').'đ' ?>
            </td>
            <td><?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?></td>
            <td><?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
            <br><?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?></td>
            <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
            <td>
                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                    <label class="label label-default label-small" for="">Visa Only</label>
                <?php endif; ?>
                <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                    <label class="label label-default label-small" for="">Security Deposit</label>
                <?php endif; ?>
                <?php if (isset($history['fe']) && $history['fe']): ?>
                    <label class="label label-default label-small" for="">FE</label>
                <?php endif; ?>
                <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                    <label class="label label-default label-small" for="">Visa Ready</label>
                <?php endif; ?>
                <?php if (isset($history['one_way']) && $history['one_way']): ?>
                    <label class="label label-default label-small" for="">One Way</label>
                <?php endif; ?>

                <?php if(isset($history['infant']) && $history['infant']): ?>
                    [ Infant: <?php echo $history['infant'] ?> ]
                <?php endif; ?>
                <hr>
                <?php if (isset($history['updated_at']) && strtotime($history['updated_at'])) echo date('d/m/y H:i', strtotime($history['updated_at']));
                    elseif (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?>
            </td>
            <td class="goto" onclick="window.location='<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id=<?php echo $history['id'] ?>'">
                <?php
                if (isset($history['tour_id']) && $history['tour_id']) {
                    $tour = TourNew::lookup($history['tour_id']);
                    if ($tour) echo $tour->name;
                } elseif (isset($history['country']) && $history['country']) {
                    $country = TourMarket::lookup($history['country']);
                    if($country)
                        echo 'Visa '.$country->name;
                }

                ?>
            </td>
            <td>
                <form action="<?php echo $cfg->getUrl() ?>/scp/reservation.php" target="leader_confirm_<?php echo $history['id'] ?>" method="post">
                    <input type="hidden" name="action" value="leaderreview">
                    <input type="hidden" name="reservation_id" value="<?php echo $history['id'] ?>">
                    <?php csrf_token() ?>
                    <?php if ($thisstaff->canApproveLeaderReservation()): ?>
                        <button class="btn_sm btn-info btn-xs" onclick="confirm('Confirm booking <?php echo $history['booking_code'] ?>?')">Confirm</button>
                    <?php endif;?>
                </form>
                <iframe name="leader_confirm_<?php echo $history['id'] ?>" src="" class="confirm_frame" frameborder="0"></iframe>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$count_item): ?>
        <tr><td>No Item</td></tr>
    <?php endif; ?>
    </tbody>
</table>

<script>
    $(document).ready(function() {
        var showChar = 150;
        var ellipsestext = "...";
        var moretext = "Bấm để đọc tiếp";
        var lesstext = "Thu gọn";
        $('._more').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>
/*branch: visa */

create table visa_follow_action
(
    id        int auto_increment
        primary key,
    profile_id int          null,
    action_id int          null,
    time      datetime     null,
    subject   varchar(255) null,
    content   mediumtext   null
)
    collate = utf8_unicode_ci;

create index visa_follow_action_action_id_index
    on visa_follow_action (action_id);

create index visa_follow_action_profile_id_index
    on visa_follow_action (profile_id);

create index visa_follow_action_time_index
    on visa_follow_action (time);


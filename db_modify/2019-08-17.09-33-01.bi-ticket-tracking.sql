create table booking_count
(
  booking_code_trim int not null,
  count int null,
  constraint booking_count_pk
    primary key (booking_code_trim)
);

create table booking_return
(
  date date not null,
  booking_type int not null,
  count int null,
  constraint booking_return_pk
    primary key (date, booking_type)
);

create table ticket_follow
(
  ticket_id int not null,
  start_date datetime null,
  end_date datetime null,
  status tinyint null,
  constraint ticket_follow_pk
    primary key (ticket_id)
);

create index ticket_follow_end_date_index
  on ticket_follow (end_date);

create index ticket_follow_start_date_end_date_index
  on ticket_follow (start_date, end_date);

create index ticket_follow_start_date_index
  on ticket_follow (start_date);

create index ticket_follow_status_index
  on ticket_follow (status);

create table ticket_follow_action
(
  id int auto_increment,
  ticket_id int null,
  action_id int null,
  time datetime null,
  subject varchar(255) null,
  content text null,
  constraint ticket_follow_action_pk
    primary key (id)
);

create index ticket_follow_action_action_id_index
  on ticket_follow_action (action_id);

create index ticket_follow_action_ticket_id_index
  on ticket_follow_action (ticket_id);

create index ticket_follow_action_time_index
  on ticket_follow_action (time);



create table booking_follow
(
  booking_number int not null,
  start_date datetime null,
  end_date datetime null,
  status tinyint null,
  constraint booking_number_follow_pk
    primary key (booking_number)
);

create index booking_follow_end_date_index
  on booking_follow (end_date);

create index booking_follow_start_date_end_date_index
  on booking_follow (start_date, end_date);

create index booking_follow_start_date_index
  on booking_follow (start_date);

create index booking_follow_status_index
  on booking_follow (status);

create table booking_follow_action
(
  id int auto_increment,
  booking_number int null,
  action_id int null,
  time datetime null,
  subject varchar(255) null,
  content text null,
  constraint booking_number_follow_action_pk
    primary key (id)
);

create index booking_follow_action_action_id_index
  on booking_follow_action (action_id);

create index booking_follow_action_booking_number_index
  on booking_follow_action (booking_number);

create index booking_follow_action_time_index
  on booking_follow_action (time);




create table ticket_analytics
(
  date date not null,
  source int not null,
  tag int not null,
  status int not null comment 'create/success/close',
  quantity int default 0 not null,
  constraint ticket_analytics_pk
    primary key (date, source, tag, status)
);

create index ticket_analytics_date_index
  on ticket_analytics (date);

create index ticket_analytics_source_index
  on ticket_analytics (source);

create index ticket_analytics_status_index
  on ticket_analytics (status);

create index ticket_analytics_tag_index
  on ticket_analytics (tag);


create table booking_analytics
(
  date date not null,
  source int not null,
  type int not null,
  status int not null comment 'create/success/close',
  quantity int default 0 not null,
  constraint booking_analytics_pk
    primary key (date, source, type, status)
);

create index booking_analytics_date_index
  on booking_analytics (date);

create index booking_analytics_source_index
  on booking_analytics (source);

create index booking_analytics_status_index
  on booking_analytics (status);

create index booking_analytics_type_index
  on booking_analytics (type);

create table growth_commission
(
  id int auto_increment,
  date date not null,
  amount decimal(12) default 0 not null,
  constraint growth_commission_pk
    primary key (id)
);

create unique index growth_commission_date_uindex
  on growth_commission (date);

create table growth_commission_detail
(
  id int auto_increment,
  growth_commission_id int null,
  type enum('ticket', 'booking') default 'ticket' not null,
  object_number int null,
  success_date datetime null,
  commission decimal(12) default 0 not null,
  source int null,
  note varchar(255) null,
  constraint growth_commission_detail_pk
    primary key (id)
);

create index growth_commission_detail_growth_commission_id_index
  on growth_commission_detail (growth_commission_id);

create unique index growth_commission_detail_object_number_type_uindex
  on growth_commission_detail (object_number, type);

create index growth_commission_detail_source_index
  on growth_commission_detail (source);

create index growth_commission_detail_success_date_index
  on growth_commission_detail (success_date);

alter table ticket_follow
  add source int null;

create index ticket_follow_source_index
  on ticket_follow (source);

alter table booking_follow
  add source int null;

create index booking_follow_source_index
  on booking_follow (source);

-- auto-generated definition
create table tracking_cookie_urlparam_event
(
  id                          int auto_increment
    primary key,
  tracking_cookie_urlparam_id int      null,
  time                        datetime null
);

create index tracking_cookie_urlparam_event_time_index
  on tracking_cookie_urlparam_event (time);

create index tracking_cookie_urlparam_event_tracking_cookie_urlparam_id_index
  on tracking_cookie_urlparam_event (tracking_cookie_urlparam_id);

-- auto-generated definition
create table tracking_urlparam
(
  id          int auto_increment
    primary key,
  object_id   int                     null,
  object_type enum ('phone', 'email') null,
  time        datetime                null,
  code        varchar(20)             null,
  constraint tracking_code_uindex
    unique (code)
);

-- auto-generated definition
create table tracking_cookie_urlparam
(
  id           int auto_increment
    primary key,
  cookie_id    int null,
  url_param_id int null,
  constraint tracking_cookie_urlparam_cookie_id_url_param_id_uindex
    unique (cookie_id, url_param_id)
);

-- auto-generated definition
create table tracking_cookie
(
  id         int auto_increment
    primary key,
  cookie     varchar(32) null,
  browser    int         null,
  os         int         null,
  created_at datetime    null,
  constraint tracking_cookie_cookie_uindex
    unique (cookie)
);

create index tracking_cookie_browser_index
  on tracking_cookie (browser);

create index tracking_cookie_created_at_index
  on tracking_cookie (created_at);

create index tracking_cookie_os_index
  on tracking_cookie (os);

-- auto-generated definition
create table phone_number_list
(
  id           int auto_increment
    primary key,
  phone_number varchar(10) null,
  constraint phone_number_list_phone_number_uindex
    unique (phone_number)
);

-- auto-generated definition
create table email_list
(
  id            int auto_increment
    primary key,
  email_address varchar(128) null,
  constraint email_list_email_address_uindex
    unique (email_address)
);

create table ticket_info
(
  ticket_id int not null,
  object_id int not null,
  object_type enum('phone', 'email') not null,
  constraint ticket_info_pk
    primary key (ticket_id, object_id, object_type)
);

create index ticket_info_object_type_object_id_index
  on ticket_info (object_type, object_id);

create index ticket_info_ticket_id_index
  on ticket_info (ticket_id);

alter table ticket_follow
  add succeeded_ticket_id int null;

alter table ticket_follow
  add booking_code int null;

create unique index ticket_follow_booking_code_uindex
  on ticket_follow (booking_code);

create unique index ticket_follow_succeeded_ticket_id_uindex
  on ticket_follow (succeeded_ticket_id);

create table source_list
(
  id int auto_increment,
  name varchar(64) null,
  note varchar(255) null,
  constraint source_list_pk
    primary key (id)
);

create unique index source_list_name_uindex
  on source_list (name);

alter table booking_count change booking_code_trim booking_code int not null;

alter table booking_follow change booking_number booking_code int not null;

create table object_event
(
  id int auto_increment,
  object_id int null,
  object_type enum('phone', 'email') null,
  time datetime null,
  event_id int null,
  note varchar(255) null,
  constraint object_event_pk
    primary key (id)
);

create index object_event_event_id_index
  on object_event (event_id);

create index object_event_object_id_index
  on object_event (object_id);

create index object_event_object_id_object_type_index
  on object_event (object_id, object_type);

create index object_event_object_type_index
  on object_event (object_type);

create index object_event_time_index
  on object_event (time);

create table auto_action_content
(
  id int auto_increment,
  name varchar(64) not null,
  description varchar(255) null,
  sms_content varchar(512) null,
  sms_send_time varchar(255) null,
  notification_title varchar(64) null,
  notification_content varchar(255) null,
  notification_url varchar(512) null,
  notification_image_name varchar(64) null,
  notification_send_time varchar(255) null,
  email_subject varchar(255) null,
  email_content text null,
  email_send_time varchar(255) null,
  created_at datetime null,
  created_by int null,
  updated_at datetime null,
  updated_by int null,
  last_log_id int null,
  sms_status tinyint default 0 not null,
  notification_status tinyint default 0 not null,
  email_status tinyint default 0 not null,
  constraint auto_action_content_pk
    primary key (id)
);

create index auto_action_content_description_index
  on auto_action_content (description);

create index auto_action_content_email_send_time_index
  on auto_action_content (email_send_time);

create index auto_action_content_email_status_index
  on auto_action_content (email_status);

create index auto_action_content_name_index
  on auto_action_content (name);

create unique index auto_action_content_name_uindex
  on auto_action_content (name);

create index auto_action_content_notification_send_time_index
  on auto_action_content (notification_send_time);

create index auto_action_content_notification_status_index
  on auto_action_content (notification_status);

create index auto_action_content_sms_send_time_index
  on auto_action_content (sms_send_time);

create index auto_action_content_sms_status_index
  on auto_action_content (sms_status);

create table auto_action_content_update_log
(
  id int auto_increment,
  auto_action_content_id int null,
  snapshot longtext null,
  time datetime null,
  constraint auto_action_content_update_log_pk
    primary key (id)
);

create index auto_action_content_update_log_auto_action_content_id_index
  on auto_action_content_update_log (auto_action_content_id);

create index auto_action_content_update_log_time_index
  on auto_action_content_update_log (time);

alter table object_event comment 'sự kiện tạo ticket hỏi tour, booking, hủy...';

alter table auto_action_content
  add sms_trigger_id int null;

alter table auto_action_content
  add email_trigger_id int null;

alter table auto_action_content
  add notification_trigger_id int null;

create index auto_action_content_email_trigger_id_index
  on auto_action_content (email_trigger_id);

create index auto_action_content_notification_trigger_id_index
  on auto_action_content (notification_trigger_id);

create index auto_action_content_sms_trigger_id_index
  on auto_action_content (sms_trigger_id);

create table auto_action_waiting
(
  id int auto_increment,
  object_id int null,
  object_type enum('phone', 'email') null,
  add_time datetime null,
  trigger_id int null,
  constraint auto_action_waiting_pk
    primary key (id)
);

create index auto_action_waiting_add_time_index
  on auto_action_waiting (add_time);

create index auto_action_waiting_object_id_object_type_index
  on auto_action_waiting (object_id, object_type);

create index auto_action_waiting_trigger_id_index
  on auto_action_waiting (trigger_id);

create table notification_sub_id
(
  id int auto_increment,
  sub_id varchar(512) null,
  constraint notification_sub_id_pk
    primary key (id)
);

create unique index notification_sub_id_sub_id_uindex
  on notification_sub_id (sub_id);

create unique index notification_sub_id_sub_id_uindex_2
  on notification_sub_id (sub_id);

rename table notification_sub_id to notification_sub_id_list;
alter table auto_action_waiting modify object_type enum('phone', 'email', 'sub') null;
alter table tracking_urlparam modify object_type enum('phone', 'email', 'sub') null;

alter table auto_action_content
  add sms_send_after int null comment 'seconds';

alter table auto_action_content
  add email_send_after int null comment 'seconds';

alter table auto_action_content
  add notification_send_after int null comment 'seconds';

alter table auto_action_content change sms_send_time sms_send_one_time datetime null;

alter table auto_action_content change notification_send_time notification_send_one_time datetime null;

alter table auto_action_content change email_send_time email_send_one_time datetime null;

alter table auto_action_content
  add sms_send_recursion varchar(9) null;

alter table auto_action_content
  add email_send_recursion varchar(9) null;

alter table auto_action_content
  add notification_send_recursion varchar(9) null;

create index auto_action_content_email_send_recursion_index
  on auto_action_content (email_send_recursion);

create index auto_action_content_notification_send_recursion_index
  on auto_action_content (notification_send_recursion);

create index auto_action_content_sms_send_recursion_index
  on auto_action_content (sms_send_recursion);

drop index auto_action_content_email_trigger_id_index on auto_action_content;
drop index auto_action_content_notification_trigger_id_index on auto_action_content;
drop index auto_action_content_sms_trigger_id_index on auto_action_content;
alter table auto_action_content change sms_trigger_id trigger_id int null after description;
alter table auto_action_content drop column email_trigger_id;
alter table auto_action_content drop column notification_trigger_id;
create index auto_action_content_trigger_id_index
  on auto_action_content (trigger_id);

alter table auto_action_content change sms_send_after send_after int null comment 'seconds';

alter table auto_action_content drop column email_send_after;

alter table auto_action_content drop column notification_send_after;

alter table booking_count CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table booking_return CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table ticket_follow CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table ticket_follow_action CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table booking_follow CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table booking_follow_action CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table ticket_analytics CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table booking_analytics CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table growth_commission CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table growth_commission_detail CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table tracking_cookie_urlparam_event CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table tracking_urlparam CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table tracking_cookie_urlparam CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table tracking_cookie CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table phone_number_list CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table email_list CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table ticket_info CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table source_list CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table object_event CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table auto_action_content CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table auto_action_content_update_log CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table auto_action_waiting CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table notification_sub_id_list CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
alter table auto_action_content modify sms_send_recursion varchar(10) null;

alter table auto_action_content modify email_send_recursion varchar(10) null;

alter table auto_action_content modify notification_send_recursion varchar(10) null;



alter table auto_action_content change sms_send_recursion sms_send_recursion_hour varchar(72) null;

alter table auto_action_content
  add sms_send_recursion_minute varchar(180) null;

alter table auto_action_content
  add sms_send_recursion_day_of_week varchar(21) null;

alter table auto_action_content
  add sms_send_recursion_date_in_month varchar(93) null;

alter table auto_action_content
  add sms_send_recursion_month varchar(36) null;


alter table auto_action_content change email_send_recursion email_send_recursion_hour varchar(72) null;

alter table auto_action_content
  add email_send_recursion_minute varchar(180) null;

alter table auto_action_content
  add email_send_recursion_day_of_week varchar(21) null;

alter table auto_action_content
  add email_send_recursion_date_in_month varchar(93) null;

alter table auto_action_content
  add email_send_recursion_month varchar(36) null;


alter table auto_action_content change notification_send_recursion notification_send_recursion_hour varchar(72) null;

alter table auto_action_content
  add notification_send_recursion_minute varchar(180) null;

alter table auto_action_content
  add notification_send_recursion_day_of_week varchar(21) null;

alter table auto_action_content
  add notification_send_recursion_date_in_month varchar(93) null;

alter table auto_action_content
  add notification_send_recursion_month varchar(36) null;


create index aac_sms_send_recursion_hour_index
  on auto_action_content (sms_send_recursion_hour);
create index aac_sms_send_recursion_minute_index
  on auto_action_content (sms_send_recursion_minute);
create index aac_sms_send_recursion_day_of_week_index
  on auto_action_content (sms_send_recursion_day_of_week);
create index aac_sms_send_recursion_date_in_month_index
  on auto_action_content (sms_send_recursion_date_in_month);
create index aac_sms_send_recursion_month_index
  on auto_action_content (sms_send_recursion_month);

create index aac_notification_send_recursion_hour_index
  on auto_action_content (notification_send_recursion_hour);
create index aac_notification_send_recursion_minute_index
  on auto_action_content (notification_send_recursion_minute);
create index aac_notification_send_recursion_dow_index
  on auto_action_content (notification_send_recursion_day_of_week);
create index aac_notification_send_recursion_date_in_month_index
  on auto_action_content (notification_send_recursion_date_in_month);
create index aac_notification_send_recursion_month_index
  on auto_action_content (notification_send_recursion_month);

create index aac_email_send_recursion_hour_index
  on auto_action_content (email_send_recursion_hour);
create index aac_email_send_recursion_minute_index
  on auto_action_content (email_send_recursion_minute);
create index aac_email_send_recursion_day_of_week_index
  on auto_action_content (email_send_recursion_day_of_week);
create index aac_email_send_recursion_date_in_month_index
  on auto_action_content (email_send_recursion_date_in_month);
create index aac_email_send_recursion_month_index
  on auto_action_content (email_send_recursion_month);
alter table auto_action_content
  add sms_run_at tinyint(1) default 0 not null comment '0=one time';

alter table auto_action_content
  add email_run_at tinyint(1) default 0 not null comment '0=one time';

alter table auto_action_content
  add notification_run_at tinyint(1) default 0 not null comment '0=one time';

create index auto_action_content_email_run_at_index
  on auto_action_content (email_run_at);

create index auto_action_content_notification_run_at_index
  on auto_action_content (notification_run_at);

create index auto_action_content_sms_run_at_index
  on auto_action_content (sms_run_at);

alter table tracking_urlparam modify code varchar(32) null;

alter table auto_action_waiting
  add status tinyint default 0 not null;

alter table auto_action_waiting
  add ref_ticket_id int null;

create index auto_action_waiting_ref_ticket_id_index
  on auto_action_waiting (ref_ticket_id);

create index auto_action_waiting_status_index
  on auto_action_waiting (status);

alter table auto_action_waiting change ref_ticket_id ticket_id int null;
alter table tracking_urlparam
  add auto_action_waiting_id int null;

create index tracking_urlparam_auto_action_waiting_id_index
  on tracking_urlparam (auto_action_waiting_id);

alter table auto_action_waiting
  add auto_action_content_log_id int null;

create index auto_action_waiting_auto_action_content_log_id_index
  on auto_action_waiting (auto_action_content_log_id);

drop index tracking_urlparam_auto_action_waiting_id_index on tracking_urlparam;

alter table tracking_urlparam drop column auto_action_waiting_id;


<h3><i class="icon-copy"></i> <?php echo __('Merge Ticket'); ?></i></h3>
<b><a class="close" href="#"><i class="icon-remove-circle"></i></a></b>
<hr/><?php echo __(
    'Nhập mã ticket. Ticket hiện đang mở sẽ là ticket gốc. 
    Các thông tin cơ bản của ticket gốc như booking code, giá... sẽ được giữ nguyên. 
    Ticket mà bạn nhập mã vào ô bên dưới chỉ lấy các thread, không lấy các thông tin cơ bản.
    Thứ tự các thread trong ticket sau khi gộp được xếp theo thời gian của từng thread.'
); ?>
<br/>
<br/>
<form method="post" action="<?php echo $info['action']; ?>">
    <div>
        <input type="text" name="ticket_number" id="ticket_number">
    </div>
    <div id="msg_error" style="display:none;">

    </div>
    <hr>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="reset" value="<?php echo __('Reset'); ?>">
            <input type="button" name="cancel" class="<?php
            echo $user ? 'cancel' : 'close' ?>" value="<?php echo __('Cancel'); ?>">
        </span>
        <span class="buttons pull-right">
            <input type="submit" value="<?php echo __('Save Changes'); ?>">
        </span>

        <span style="display: none;" class="merge_ticket_hacking_span"></span>
    </p>
</form>
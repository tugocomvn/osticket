<?php
require_once(INCLUDE_DIR . 'class.general-object.php');
require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'class.info_list.php');

class TriggerList extends GeneralObject {
    const TICKET_CANCEL = 101;
    const TICKET_CANCEL_REMIND_2WEEK = 1010;
    const TICKET_SUCCESS = 102;
    const TICKET_OVERDUE = 103;
    const TICKET_NEW = 104;
    const BOOKING_NEW = 201;
    const BOOKING_CANCEL = 202;
    const BOOKING_FULL_PAID = 203;
    const VISA_APPROVE = 301;
    const VISA_REJECT = 302;
    const CALL_IN = 401;
    const CALL_OUT = 402;
    const SMS = 403;
    const LONG_TIME_NO_SEE = 501;
}

interface TriggerInterface {
    static function filter($ticket, $data = null);
    static function sendObjectToWaitingList();
    static function _fire($object, $trigger, $data = null);
}

class Trigger implements TriggerInterface {
    protected static $object_id;
    protected static $object_type;
    protected static $trigger_type;
    protected static $time;
    protected static $ticket_id;

    public function init($object_id, $object_type, $trigger_type, $ticket_id) {
        static::$object_id = $object_id;
        static::$object_type = $object_type;
        static::$trigger_type = $trigger_type;
        static::$ticket_id = $ticket_id;
        static::$time = date('Y-m-d H:i:s');
    }

    static function _fire($ticket, $trigger, $status_id = null) {
        if (is_null($status_id)) { return false; }

        $phone_number = $ticket->getFormatedPhoneNumber();

        $data = [
            'status_id' => $status_id,
            'phone_number' => $phone_number
        ];

        if (!$phone_number) return false;
        if (!static::filter($ticket, $data)) return false;

        try {
            $phone_id = PhoneNumberList::push($phone_number);
            static::init(
                $phone_id,
                'phone',
                $trigger,
                $ticket->getId()
            );
            static::sendObjectToWaitingList();
        } catch (Exception $ex) {
            if (defined('DEV_ENV') && DEV_ENV == 1) {
                var_dump($ex->getMessage());
            }
        }
    }

    static function filter($ticket, $data = null) {
        if (defined('TICKET_STATUS_SUCCESS') && TICKET_STATUS_SUCCESS == $data['status_id'])
            return false;

        if (Ticket::countOpenByPhoneNumber($data['phone_number'], $ticket->getId()))
            return false;

        return true;
    }

    static function sendObjectToWaitingList() {
        $data = [
            'object_id'   => static::$object_id,
            'object_type' => static::$object_type,
            'add_time'    => static::$time,
            'trigger_id'  => static::$trigger_type,
            'ticket_id'   => static::$ticket_id,
            'status'      => 0,
        ];
        $wating = AutoActionWaiting::create($data);
        $wating->save();
    }
}

class TicketCloseTrigger extends Trigger {
    static function fire($ticket, $status_id = null) {
        parent::_fire($ticket, TriggerList::TICKET_CANCEL, $status_id);
    }
}

class TwoWeekRemindTrigger extends Trigger {
    static function fire($ticket, $status_id = null) {
        parent::_fire($ticket, TriggerList::TICKET_CANCEL_REMIND_2WEEK, $status_id);
    }
}

class NewBookingTrigger extends Trigger {
    static function fire($booking) { // TODO: lấy số điện thoại từ booking
        $booking = Booking::get_booking_view($booking->getId());
        $phone_numbers = $booking['phone_number'];
        $phone_numbers = _String::getPhoneNumbers($phone_numbers);

        if (!$phone_numbers) return false;
        $phone_numbers = static::filter($booking, $phone_numbers);
        if (!$phone_numbers || !is_array($phone_numbers)) return false;

        foreach($phone_numbers as $phone_number) {
            try {
                $phone_id = PhoneNumberList::push($phone_number);
                static::init(
                    $phone_id,
                    'phone',
                    TriggerList::BOOKING_NEW,
                    $booking['ticket_id']
                );
                static::sendObjectToWaitingList();
            } catch (Exception $ex) {
                if (defined('DEV_ENV') && DEV_ENV == 1) {
                    var_dump($ex->getMessage());exit;
                }
            }
        }
    }

    static function filter($booking, $data = null) {
        if (!isset($booking['status']) || empty($booking['status']))
            return false;

        $status = _String::json_decode($booking['status']);
        if (!$status || $status === "Hủy") return false;

        if ($data && is_array($data)) {
            $data = array_filter($data);

            foreach ($data as $index => $number) {
                $phone = PhoneNumberList::lookup(['phone_number' => $number]);
                $filter = [
                    'object_type' => 'phone',
                    'object_id'   => $phone->id,
                    'ticket_id'   => $booking['ticket_id'],
                ];
                $check = AutoActionWaiting::lookup($filter);
                if ($check) unset($data[$index]);
            }
        }

        return $data;
    }
}

class BookingFullPaidTrigger extends Trigger {
    static function fire($ticket_id, $phone_number) {
        if (empty($phone_number)) { return false; }

        $phone_id = PhoneNumberList::push($phone_number);
        if (!$phone_id) return false;

        $data = [
            'ticket_id' => $ticket_id,
            'phone_number' => $phone_number,
            'phone_id' => $phone_id,
        ];

        if (!static::filter($ticket_id, $data)) return false;

        try {
            static::init(
                $phone_id,
                'phone',
                TriggerList::BOOKING_FULL_PAID,
                $ticket_id
            );
            static::sendObjectToWaitingList();
        } catch (Exception $ex) {
            if (defined('DEV_ENV') && DEV_ENV == 1) {
                var_dump($ex->getMessage());
            }
        }
    }

    static function filter($ticket_id, $data = null) {
        $where = [
            'object_id' => $data['phone_id'],
            'object_type' => 'phone',
            'trigger_id' => TriggerList::BOOKING_FULL_PAID,
            'ticket_id' =>$ticket_id,
        ];
        $check = AutoActionWaiting::lookup($where);
        if ($check) return false;
        return true;
    }
}

class LongTimeNoSeeTrigger extends Trigger {
    static function fire($phone_number, $date_from) {
        if (empty($phone_number)) { return false; }

        $phone_id = PhoneNumberList::push($phone_number);
        if (!$phone_id) return false;

        $data = [
            'phone_number' => $phone_number,
            'phone_id' => $phone_id,
            'date_from' => $date_from,
        ];

        if (!static::filter(0, $data)) return false;

        try {
            static::init(
                $phone_id,
                'phone',
                TriggerList::LONG_TIME_NO_SEE,
                0
            );
            echo "$phone_number\r\n";
            static::sendObjectToWaitingList();
        } catch (Exception $ex) {
            if (defined('DEV_ENV') && DEV_ENV == 1) {
                var_dump($ex->getMessage());
            }
        }
    }

    static function filter($ticket_id, $data = null) {
        if ($data === null) return false;
        $where = [
            'object_id' => $data['phone_id'],
            'object_type' => 'phone',
            'trigger_id' => TriggerList::LONG_TIME_NO_SEE,
            'status' => AutoActionWaiting::STATUS_WAITING,
        ];
        $check = AutoActionWaiting::lookup($where);
        if ($check) return false;

        $sql = "select COUNT(*) as total from booking_tmp
            where phone_number like '%".$data['phone_number']."%'
            and date(created) >= ".db_input($data['date_from']);

        $check = db_count($sql);
        if (!(int)$check) return false;

        return true;
    }
}

class AutoActionWaitingModel extends VerySimpleModel {
    static $meta = [
        'table' => AUTO_ACTION_WAITING_TABLE,
        'pk' => ['id'],
    ];
}

class AutoActionWaiting extends AutoActionWaitingModel {
    const STATUS_WAITING = 0;
    const STATUS_SENT = 1;
    const STATUS_NOACTION = 2;

    public static function getAll($trigger_id, $send_after, $object_type, $status = 0) {
        $sql = "SELECT * FROM ".static::$meta['table']." WHERE 1
            AND object_type LIKE '$object_type'
            AND status = '$status'
            AND trigger_id=$trigger_id AND add_time <= NOW()-INTERVAL $send_after MINUTE";
        return db_query($sql);
    }
}


Signal::connect('TriggerList.TICKET_CANCEL', ['TicketCloseTrigger', 'fire']);
Signal::connect('TriggerList.TICKET_CANCEL_REMIND_2WEEK', ['TwoWeekRemindTrigger', 'fire']);
Signal::connect('TriggerList.BOOKING_NEW', ['NewBookingTrigger', 'fire']);

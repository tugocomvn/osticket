<?php
/**
 * Created by PhpStorm.
 * User: minhjoko
 * Date: 6/13/18
 * Time: 9:31 AM
 */

if(isset($_POST['submit'])){
    $name_temp = $_POST['nametemp'];
    $template = $_POST['template'];
    if(!empty($template)){
        $apiKey = '7090ec095d3256042a961b4b799c7895-us16';
        $template_id = '64479';

        $data_center = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/templates/'. $template_id .'';

        $json = json_encode([
            'nametemp' => $name_temp,
            'template' => $template
        ]);
        RequestUrl::CurlPatch($url,$apiKey,$json);
    }
}
?>
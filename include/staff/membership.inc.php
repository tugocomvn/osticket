<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canViewMemberList()) exit('Access Denied');

$select = "SELECT * ";
$from = " FROM api_user ";
$where = " WHERE 1";
$sort = " ORDER BY register_at DESC";
if(isset($_REQUEST['query'])){
    $phone_number = trim($_REQUEST['query']);
    if(strlen($phone_number) == 11)
        $phone_number = _String::convertMobilePhoneNumber($phone_number);
    $check_isMobile = (_String::simpleCheckMobilePhone($phone_number));
}
if(isset($_REQUEST['query']) && !empty($_REQUEST['query']))
{
    $where .= " AND customer_name LIKE ".db_input('%'.trim($_REQUEST['query']).'%')
        .($check_isMobile ? 'OR phone_number LIKE '.db_input('%'.$phone_number.'%'):' ');
}
$total=db_count('SELECT count(DISTINCT uuid) '.$from.$where);
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total,$page,PAGE_LIMIT);
$qs = array('query' => $_REQUEST['query']);
$pageNav->setURL('membership.php', $qs);

$query="$select $from $where $sort LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

$res = db_query($query);
?>
<h2>Membership <small>List</small></h2>
<div class="pull-left">
    <?php
    $showing = $search ? __('Search Results').': ' : '';
    $res = db_query($query);
    if($res && ($num=db_num_rows($res)))
        $showing .= $pageNav->showing();
    else
        $showing .= __('No guests found!');
    ?>
    <form action="<?php echo $cfg->getUrl(); ?>scp/membership.php" method="get">
        <input type="text" id="basic-user-search" name="query"
               size=30 value="<?php echo Format::htmlchars($_REQUEST['query']); ?>"
               autocomplete="off" class="input-field" autocapitalize="off">
        <button type="submit" class="btn_sm btn-default " value="Search">Search</button>
    </form>
</div>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <thead>
    <tr>
        <th><?php echo __('Name'); ?></th>
        <th><?php echo __('Phone Number'); ?></th>
        <th><?php echo __('Total Point')?></th>
        <th><?php echo __('Used Point')?></th>
        <th><?php echo __('Date of birth'); ?></th>
        <th><?php echo __('Created'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    while ($res && ($row = db_fetch_array($res))): ?>
    <tr>
        <td>
            <a class=""
               href="<?php echo $cfg->getUrl();?>scp/membership.php?id=<?php echo $row['customer_number']; ?>">
                <?php echo $row['customer_name']; ?>
            </a>
        </td>
        <td>
            <?php  echo substr_replace($row['phone_number'],"xxxx",3,4); ?>
        </td>
        <td>
            <?php echo $row['user_point']; ?>
        </td>
        <td>
            <?php
            $results = UserPoint::forUserUuid($row['uuid']);
            $point = 0;
            foreach ($results as $data){
                if($data->change_point < 0)
                    $point += abs($data->change_point);
            }
            echo $point; ?>
        </td>
        <td>
            <?php if(isset($row['dob'])):
                echo date('d/m/Y',strtotime($row['dob']));
            endif;?>
        </td>
        <td>
            <?php echo date('d/m/Y',strtotime($row['register_at'])); ?>
        </td>
    </tr>
    <?php endwhile; ?>
    </tbody>
</table>
<?php
if($res && $num): //Show options..
    echo sprintf('<div>&nbsp;'.__('Page').': %s &nbsp; </div>',
        $pageNav->getPageLinks(),
        $qhash);
endif;
?>



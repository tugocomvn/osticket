<?php
require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'class.receipt.php');
require_once INCLUDE_DIR."../vendor/autoload.php";
use \koolreport\processes\Group;
use \koolreport\processes\Sort;
use \koolreport\processes\Limit;

class PaymentModel extends \VerySimpleModel {
    static $meta = [
        'table' => TICKET_PAYMENT,
        'pk' => ['ticket_id'],
    ];
}

class Payment extends PaymentModel {
    const KEY = 'DLNwROkurGhgMEqKvbjn';
    public static $payment_type_exclude_cache;

    public static function get_inner_sql($type) {
        return " (SELECT DISTINCT
             vv.value
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', unserialize(IO_FORM)) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '{$type}' AND tt.ticket_id=t.ticket_id
          ) AS {$type} ";
    }
    public static function get_inner_sql_id($type, $as) {
        return " (SELECT DISTINCT
             vv.value_id
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', unserialize(IO_FORM)) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '{$type}' AND tt.ticket_id=t.ticket_id
          ) AS {$as} ";
    }

    public static function get_inner_sql_trim($type, $as) {
        return " (SELECT DISTINCT
             trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM vv.value))
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', unserialize(IO_FORM)) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '{$type}' AND tt.ticket_id=t.ticket_id
          ) AS {$as} ";
    }

    public static function get_select_sql($ticket_id = 0) {
        $ticket_id = intval($ticket_id);
        $ticket_id = db_input($ticket_id);
        $booking_code = self::get_inner_sql('booking_code');
        $booking_code_trim = self::get_inner_sql_trim('booking_code', 'booking_code_trim');
        $time = self::get_inner_sql('time');
        $receipt_code = self::get_inner_sql('receipt_code');
        $amount = self::get_inner_sql('amount');
        $quantity = self::get_inner_sql('quantity');
        $income_type = self::get_inner_sql('income_type');
        $outcome_type = self::get_inner_sql('outcome_type');
        $income_type_id = self::get_inner_sql_id('income_type', 'income_type_id');
        $outcome_type_id = self::get_inner_sql_id('outcome_type', 'outcome_type_id');
        $method = self::get_inner_sql('method');
        $agent = self::get_inner_sql('agent');
        $note = self::get_inner_sql('note');
        $form = implode(',', unserialize(IO_FORM));
        $where = $ticket_id ? "  WHERE t.ticket_id = {$ticket_id}  " : " ";
        return " SELECT DISTINCT
              t.ticket_id,
              t.number,
              t.topic_id,
              t.dept_id,
              {$booking_code},
              {$booking_code_trim},
              {$time},
              {$receipt_code},
              {$amount},
              {$quantity},
              {$income_type},
              {$income_type_id},
              {$outcome_type},
              {$outcome_type_id},
              0 as exclude,
              {$method},
              {$agent},
              {$note},
              0 as booking_ticket_id
            FROM ost_ticket t
              JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T' AND e.form_id IN ({$form})
              JOIN ost_form_entry_values v ON e.id = v.entry_id
              JOIN ost_form_field f ON f.id = v.field_id
             {$where} ";
    }

    public static function get_create_sql() {
        $select = self::get_select_sql();
        $sql = "CREATE TABLE IF NOT EXISTS payment_tmp
            (UNIQUE INDEX ticket_id_ix(ticket_id), INDEX time_ix (`time`(20)),
                INDEX booking_code_ix (booking_code(20)),
                 INDEX topic_ix (topic_id),
                 INDEX income_type_id_ix (income_type_id),
                 INDEX outcome_type_id_ix (outcome_type_id),
                INDEX booking_ticket_id_ix (booking_ticket_id))
            {$select} ";
        return $sql;
    }

    public static function create_table() {
        $sql = self::get_create_sql();
        return db_query($sql);
    }

    public static function push($ticket_id = 0) {
        $select = self::get_select_sql($ticket_id);
        $sql_tmp_table = " REPLACE INTO payment_tmp {$select} LIMIT 1";
        db_query($sql_tmp_table);
        return self::get_hash($ticket_id);
    }

    public static function get_hash($ticket_id) {
        $select = self::get_select_sql($ticket_id);
        $sql = " ${select} LIMIT 1 ";
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if (!$row) return null;
        $hash = self::_hash($row);
        $string = trim($row['ticket_id'])
            .'<>'.(self::_get_string_to_hash($row));
        $data = strval($string);
        self::save_hash($hash, $data);
        return $hash;
    }

    private static function _hash($row) {
        return md5((self::_get_string_to_hash($row)).self::KEY);
    }

    private static function _get_string_to_hash($row) {
        return trim($row['booking_code']).trim($row['receipt_code']).trim($row['amount']);
    }

    public static function save_hash($hash, $data) {
        $sql = "INSERT INTO ost_hash_data SET
            `hash` = '{$hash}',
            `type` = 'payment',
            `data` = '{$data}'";
        try {
            return db_query($sql);
        } catch (Exception $ex) { return false; }
    }

    public static function export($params) {
        $sql = self::_get_payment_list_sql($params);
        return self::_payment_export_excel($sql);
    }

    private static function _payment_export_excel($sql) {
        require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
        $dir = INCLUDE_DIR.'../public_file/';
        $file_name = 'Payment_list_' . Format::userdate("Y-m-d_H-i-s", time()).".xlsx";

        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
        $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
        $objPHPExcel = new PHPExcel();
        $properties = $objPHPExcel->getProperties();
        $properties->setCreator(NOTIFICATION_TITLE);
        $properties->setLastModifiedBy("System");
        $properties->setTitle(NOTIFICATION_TITLE." - Payment Daily Report");
        $properties->setSubject(NOTIFICATION_TITLE." - Payment Daily Report");
        $properties->setDescription(NOTIFICATION_TITLE." - Payment Daily Report - ".Format::userdate("Y-m-d H:i:s", time()));
        $properties->setKeywords(NOTIFICATION_TITLE." - Payment Daily Report");
        $properties->setCategory(NOTIFICATION_TITLE." - Payment Daily Report");

        $sheet = $objPHPExcel->getActiveSheet();

        $header = [
            'Ngày',
            'Booking Code',
            'Receipt Code',
            'Loại',
            'Số lượng',
            'Tổng tiền',
            'Phương thức',
            'Note/Agent',
        ];

        $excel_row = 1;
        $col = 'A';
        foreach ($header as $title) {
            $sheet->setCellValue($col++.$excel_row, $title);
        }
        // Set cell A1 with a string value
        $excel_row = 2;
        $res = db_query($sql);
        if ($res) {
            while (($row = db_fetch_array($res))) {
                $data = [
                    Format::userdate('d/m/Y', $row['time']),
                    $row['booking_code'],
                    $row['receipt_code'],
                    _String::json_decode($row['income_type'] ?: $row['outcome_type']),
                    $row['quantity'],
                    ($row['income_type'] || $row['topic_id']==THU_TOPIC) ? number_format($row['amount']) : number_format(-1 * $row['amount']),
                    _String::json_decode($row['method']),
                    $row['note'] . $row['agent'],
                ];
                $sheet->fromArray($data, NULL, 'A'.$excel_row++);
            } //end of while.
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($dir.$file_name);

        $objPHPExcel->disconnectWorksheets();
        unset($objWriter);
        unset($objPHPExcel);
        return $dir.$file_name;
    }

    private static function _get_payment_list_sql($params) {
        global $thisstaff;
        $type = $booking_code = $amount = $receipt_code = $method = $dept_id = null;
        if ($params['type']) {
            switch (strtolower($params['type'])) {
                case 'in':
                    $type = $params['type'];
                    break;
                case 'out':
                    $type = $params['type'];
                    break;
                default:
                    $type = null;
            }
        }

        if (isset($params['booking_code']))
            $booking_code = trim($params['booking_code']);

        if (isset($params['receipt_code']))
            $receipt_code = trim($params['receipt_code']);

        if (isset($params['amount']))
            $amount = preg_replace('/[^0-9]/', '', trim($params['amount']));

        if (isset($params['method']))
            $method = trim(json_encode(trim($params['method'])), '"');

        if (isset($params['dept_id']))
            $dept_id = trim($params['dept_id']);

        $qwhere = ' WHERE 1 ';
        //Type
        if ($type)
            $qwhere .= ' AND topic_id=' . db_input($type == 'in' ? THU_TOPIC : CHI_TOPIC);

        if ($dept_id)
            $qwhere .= sprintf(" AND (  dept_id = %s ) ", db_input("$dept_id"));

        if ($booking_code)
            $qwhere .= sprintf(" AND (  booking_code LIKE %s ) ", db_input("%$booking_code%"));

        if ($receipt_code)
            $qwhere .= sprintf(" AND (  receipt_code LIKE %s ) ", db_input("%$receipt_code%"));

        if ($method)
            $qwhere .= sprintf(" AND (  method LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $method)."%'");

        if (!empty($amount))
            $qwhere .= sprintf(" AND (  amount = %s ) ", db_input("$amount"));

        //dates
        $startTime = ($params['startDate'] && (strlen($params['startDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($params['startDate'])) : 0;
        $endTime = ($params['endDate'] && (strlen($params['endDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($params['endDate'])) : 0;
        if ($startTime > $endTime && $endTime > 0) {
            $errors['err'] = __('Entered date span is invalid. Selection ignored.');
            $startTime = $endTime = 0;
        } else {
            if ($startTime)
                $qwhere .= " AND DATE(FROM_UNIXTIME(`time`)) >= date(" . (db_input($startTime)) . ")";

            if ($endTime)
                $qwhere .= " AND DATE(FROM_UNIXTIME(`time`)) <= date(" . (db_input($endTime)) . ")";
        }

        $where_dept = ' ';
        if ($thisstaff) {
            $staff_dept = $thisstaff->getGroup()->getDepartments();
            if ($staff_dept)
                $where_dept = ' AND dept_id IN ('.implode(',', $staff_dept).') ';
            else
                $where_dept = '  AND 1 = 0 ';
        }

        $qwhere .= $where_dept;

//        self::create_table();
        $sql = " SELECT * FROM payment_tmp $qwhere ORDER BY `time`, ticket_id DESC ";
        return $sql;
    }

    public static function updateTypeId($ticket_id = 0) {
        if (!$ticket_id) return false;
        $payment = static::lookup($ticket_id);
        if (!$payment) return false;
        if (!isset($payment->topic_id) && !$payment->topic_id) return false;

        if ($payment->topic_id == THU_TOPIC)
            $column_name = 'income_type';
        elseif ($payment->topic_id == CHI_TOPIC)
            $column_name = 'outcome_type';
        else
            return false;

        $type_id = static::getTypeId($payment->$column_name);
        if (!$type_id || !is_numeric($type_id)) return false;
        $exclude = 0;
        if (isset(static::$payment_type_exclude_cache[ $column_name ][ $type_id ])) {
            if (intval(trim(static::$payment_type_exclude_cache[ $column_name ][ $type_id ]))) {
                $exclude = 1;
            }
        } else {
            $exclude = static::$payment_type_exclude_cache[ $column_name ][ $type_id ] = static::isExclude( $type_id );
        }

        $payment->setAll(
            [
                $column_name.'_id' => db_input($type_id),
                'exclude' => db_input($exclude),
            ]
        );

        $payment->save();

        return true;
    }

    public static function isExclude($payment_type_id) {
        $list_item = DynamicListItem::lookup($payment_type_id);
        $config = $list_item->getConfiguration();
        if (isset($config[ INCOME_EXCLUDE ]) && $config[ INCOME_EXCLUDE ])
            return 1;
        if (isset($config[ OUTCOME_EXCLUDE ]) && $config[ OUTCOME_EXCLUDE ])
            return 1;

        return 0;
    }

    public static function getTypeId($type_json) {
        $tmp = explode('":"', $type_json);
        if (!is_array($tmp) || !isset($tmp[0])) return null;
        $id = preg_replace('/[^0-9]/', '', $tmp[0]);
        return $id;
    }
}

class BookingViewModel extends \VerySimpleModel {
    static $meta = [
        'table' => 'booking_view',
        'pk' => ['ticket_id']
    ];
}
class BookingView extends BookingViewModel {

}

class BookingModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_TABLE,
        'pk' => ['ticket_id'],
    ];
}

class BookingLogModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_LOG_TABLE,
        'pk' => ['id']
    ];
}

class BookingLog extends BookingLogModel{

}

class Booking extends BookingModel {
    const VIEW_NAME = 'booking_view';
    const TABLE_NAME = 'booking_tmp';

    public static function getTableName() {
        return static::TABLE_NAME;
    }

    public static function getViewName() {
        return static::VIEW_NAME;
    }

    public static function getIdFromBookingCode($booking_code) {
        $check = self::lookup(['booking_code' => $booking_code]);
        if (!$check) return null;
        if (!$check->ticket_id) return null;
        return $check->ticket_id;
    }

    public static function getBookingsByPhoneNumber($phone_number) {
        $phone_number = db_input(_String::formatPhoneNumber($phone_number), true);
        $sql = 'SELECT booking_code, ticket_id, total_retail_price FROM '
            .self::VIEW_NAME.' WHERE phone_number LIKE '.$phone_number;
        $res = db_query($sql);
        if (!$res) return null;
        return $res;
    }

    public static function get_booking_sql($booking_code){

        global $thisstaff;
        if(!isset($booking_code)) return null;
        $booking_code_trim = \_String::trimBookingCode($booking_code);
        if(!isset($booking_code_trim)) return null;
        $data = Booking::lookup(['booking_code_trim' => $booking_code_trim]);
        if($data){
            $ticket_id = self::getIdFromBookingCode($data->booking_code);
            $phone = Staff::lookup($data->staff_id)->getMobilePhone();
            if($ticket_id){
                $data_booking_view = self::get_booking_view($ticket_id);
                if($data_booking_view){
                    $_data = [
                        'total_retail_price' => $data_booking_view['total_retail_price'],
                        'amount_to_be_received' => $data_booking_view['total_retail_price'],
                    ];
                }
            }
            $results = array_merge($_data, [
                'tour_name'                 => _String::json_decode($data->booking_type),
                'booking_type_id'           => $data->booking_type_id,
                'ticket_id'                 => $data->ticket_id,
                'time_to_get_in_'           => date('H:i'),
                'place_to_get_in'           => PLACE_TO_GET_IN,
                'time_to_get_in'            => date('d/m/Y',($data->dept_date)),
                'departure_date'            => (isset($data->dept_date))?date('d/m/Y',$data->dept_date):NULL,
                'customer_name'             => strip_tags($data->customer),
                'customer_phone_number'     => _String::formatPhoneNumber($data->phone_number),
                'internal_notes'            => strip_tags($data->note,' '),
                'quantity_nl'               => (!empty($data->quantity1))?$data->quantity1:0,
                'unit_price_nl'             => (!empty($data->retailprice1))?$data->retailprice1:0,
                'quantity_te'               => (!empty($data->quantity2))?$data->quantity2:0,
                'unit_price_te'             => (!empty($data->retailprice2))?$data->retailprice2:0,
                'surcharge'                 => (!empty($data->retailprice3))?abs($data->retailprice3):0,
                'surcharge_note'            => (!empty($data->note3))?$data->note3:'',
                'staff_id'                  => $data->staff_id,
                'staff_phone_number'        => _String::formatPhoneNumber($phone),
                'staff_cashier_id'          => $data->staff_id,
                'staff_cashier_phone_number'=> _String::formatPhoneNumber($phone)
            ]);
        }
        if(!$results) return null;
        return $results;
    }

    public static function get_inner_sql() {
        return " (SELECT DISTINCT
             vv.value
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', [BOOKING_FORM]) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }

    public static function get_inner_sql_trim() {
        return " (SELECT DISTINCT
             trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM vv.value))
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', [BOOKING_FORM]) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }

    public static function get_inner_sql_id() {
        return " (SELECT DISTINCT
             vv.value_id
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', [BOOKING_FORM]) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }

    public static function get_inner_sql_null() {
        return " (SELECT DISTINCT
             IFNULL(vv.value, 0)
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (".implode(',', [BOOKING_FORM]).")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }

    public static function get_replace_sql($ticket_id) {
        $select = self::get_select_sql();
        $ticket_id = db_input($ticket_id);
        $table_name = self::TABLE_NAME;
        return " REPLACE INTO $table_name $select WHERE t.ticket_id = $ticket_id LIMIT 1";
    }

    public static function get_create_table_sql() {
        $select = self::get_select_sql();
        $table_name = self::TABLE_NAME;
        $sql = " CREATE TABLE IF NOT EXISTS $table_name
            (UNIQUE INDEX ticket_id_ix(`ticket_id`), INDEX time_ix (`dept_date`(20)), INDEX booking_code_ix (booking_code(20)), INDEX ticket_number_ix (ticket_number(20)))
            $select ";
        return $sql;
    }

    public static function get_create_view_sql() {
        $table_name = self::TABLE_NAME;
        $view_name = self::VIEW_NAME;
        return "CREATE OR REPLACE  VIEW $view_name AS SELECT *,
            (quantity1*netprice1 + quantity2*netprice2 + quantity3*netprice3 + quantity4*netprice4)
                AS total_net_price,
            ((quantity1*retailprice1 + quantity2*retailprice2 + quantity3*retailprice3 + quantity4*retailprice4)
             - (discountamount1 + discountamount2 + discountamount3))
                AS total_retail_price,
            ((quantity1*retailprice1 + quantity2*retailprice2 + quantity3*retailprice3 + quantity4*retailprice4)
                - (quantity1*netprice1 + quantity2*netprice2 + quantity3*netprice3 + quantity4*netprice4)
                - (discountamount1 + discountamount2 + discountamount3))
                AS total_profit,
            (quantity1 + quantity2 + quantity3 + quantity4)
                AS total_quantity
            FROM $table_name";
    }

    public static function get_booking_view($ticket_id) {
        $view_name = self::VIEW_NAME;
        $sql = " SELECT * FROM $view_name WHERE ticket_id = $ticket_id LIMIT 1 ";
        $res = db_query($sql);
        if (!$res) return null;
        return db_fetch_array($res);
    }

    public static function select_ticket_from_booking_view($ticket_number) {
        $view_name = self::VIEW_NAME;
        return db_query(" SELECT * FROM $view_name WHERE ticket_number LIKE '$ticket_number' ");
    }

    public static function create_view() {
        $sql = self::get_create_view_sql();
        return db_query($sql);
    }

    public static function create_table() {
        $sql = self::get_create_table_sql();
        return db_query($sql);
    }

    public static function get_select_sql() {
        $_inner_sql = self::get_inner_sql();
        $_inner_sql_id = self::get_inner_sql_id();
        $_inner_sql_trim = self::get_inner_sql_trim();
        $_inner_sql_null = self::get_inner_sql_null();

        $str_ticket_number = sprintf($_inner_sql, 'ticket_number', 'ticket_number');
        $str_booking_code = sprintf($_inner_sql, 'booking_code', 'booking_code');
        $str_booking_code_trim = sprintf($_inner_sql_trim, 'booking_code', 'booking_code_trim');
        $str_receipt_code = sprintf($_inner_sql, 'receipt_code', 'receipt_code');
        $str_customer = sprintf($_inner_sql, 'customer', 'customer');
        $str_phone_number = sprintf($_inner_sql, 'phone_number', 'phone_number');
        $str_booking_type = sprintf($_inner_sql, 'booking_type', 'booking_type');

        $str_quantity1 = sprintf($_inner_sql_null, 'quantity1', 'quantity1');
        $str_retailprice1 = sprintf($_inner_sql_null, 'retailprice1', 'retailprice1');
        $str_netprice1 = sprintf($_inner_sql_null, 'netprice1', 'netprice1');
        $str_note1 = sprintf($_inner_sql, 'note1', 'note1');

        $str_quantity2 = sprintf($_inner_sql_null, 'quantity2', 'quantity2');
        $str_retailprice2 = sprintf($_inner_sql_null, 'retailprice2', 'retailprice2');
        $str_netprice2 = sprintf($_inner_sql_null, 'netprice2', 'netprice2');
        $str_note2 = sprintf($_inner_sql, 'note2', 'note2');

        $str_quantity3 = sprintf($_inner_sql_null, 'quantity3', 'quantity3');
        $str_retailprice3 = sprintf($_inner_sql_null, 'retailprice3', 'retailprice3');
        $str_netprice3 = sprintf($_inner_sql_null, 'netprice3', 'netprice3');
        $str_note3 = sprintf($_inner_sql, 'note3', 'note3');

        $str_quantity4 = sprintf($_inner_sql_null, 'quantity4', 'quantity4');
        $str_retailprice4 = sprintf($_inner_sql_null, 'retailprice4', 'retailprice4');
        $str_netprice4 = sprintf($_inner_sql_null, 'netprice4', 'netprice4');
        $str_note4 = sprintf($_inner_sql, 'note4', 'note4');

        $str_partner = sprintf($_inner_sql, 'partner', 'partner');
        $str_staff = sprintf($_inner_sql, 'staff', 'staff');
        $str_staff_id = sprintf($_inner_sql_id, 'staff', 'staff_id');
        $str_dept_date = sprintf($_inner_sql, 'dept_date', 'dept_date');
        $str_status = sprintf($_inner_sql, 'status', 'status');
        $str_note = sprintf($_inner_sql, 'note', 'note');
        $str_email = sprintf($_inner_sql, 'email', 'email');
        $str_fullpay = sprintf($_inner_sql, 'fullpay_date', 'fullpay_date');
        $str_fullcoc = sprintf($_inner_sql, 'fullcoc_date', 'fullcoc_date');

        $booking_form = implode(',', [BOOKING_FORM]);

        return " SELECT DISTINCT
              t.ticket_id,
              t.number,
              t.topic_id,
              t.created,
              $str_ticket_number,
              $str_booking_code,
              $str_booking_code_trim,
              $str_receipt_code,
              $str_customer,
              $str_phone_number,
              $str_booking_type,
              0,
              $str_quantity1,
              $str_retailprice1,
              $str_netprice1,
              $str_note1,
              $str_quantity2,
              $str_retailprice2,
              $str_netprice2,
              $str_note2,
              $str_quantity3,
              $str_retailprice3,
              $str_netprice3,
              $str_note3,
              $str_quantity4,
              $str_retailprice4,
              $str_netprice4,
              $str_note4,
              $str_partner,
              $str_staff,
              $str_staff_id,
              $str_dept_date,
              $str_status,
              $str_note,
              $str_email,
              $str_fullpay,
              $str_fullcoc
            FROM ost_ticket t
                JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T'
                    AND e.form_id IN ($booking_form)
                JOIN ost_form_entry_values v ON e.id = v.entry_id
                JOIN ost_form_field f ON f.id = v.field_id ";
    }

    public static function push($ticket_id = 0) {
        $ticket_id = intval($ticket_id);
        $sql_tmp_table = self::get_replace_sql($ticket_id);
        db_query($sql_tmp_table);
        static::_calendar($ticket_id);
    }

    public static function createBooking($vars)
    {
        global $cfg,$thisstaff;
        if(!$cfg || !$thisstaff || !$thisstaff->canEditBookings())
            return false;
        $name_type_booking = TourDestination::lookup((int)$vars['booking_type']);
        $name_staff = new Staff((int)$vars['staff']);
        $name_staff = $name_staff->getFirstName().' '.$name_staff->getLastName();

        $data = [
            'booking_code'      => $vars['booking_code'],
            'booking_code_trim' => _String::trimBookingCode($vars['booking_code']),
            'ticket_id'         => $vars['ticket_id'],
            'ticket_number'     => $vars['ticket_number'],
            'number'            => $vars['number'],
            'topic_id'          => BOOKING_FORM,
            'created'           => new SqlFunction('NOW'),
            'receipt_code'      => $vars['receipt_code'],
            'customer'          => $vars['customer'],
            'phone_number'      => $vars['phone_number'],
            'email'             => $vars['email'],
            'booking_type_id'   => $vars['booking_type'],
            'staff_id'          => $vars['staff'],
            'quantity1'         => $vars['quantity1'],
            'retailprice1'      => $vars['retailprice1'],
            'netprice1'         => $vars['netprice1'],
            'note1'             => $vars['note1'],
            'quantity2'         => $vars['quantity2'],
            'retailprice2'      => $vars['retailprice2'],
            'netprice2'         => $vars['netprice2'],
            'note2'             => $vars['note2'],
            'quantity3'         => $vars['quantity3'],
            'retailprice3'      => $vars['retailprice3'],
            'netprice3'         => $vars['netprice3'],
            'note3'             => $vars['note3'],
            'quantity4'         => $vars['quantity4'],
            'retailprice4'      => $vars['retailprice4'],
            'netprice4'         => $vars['netprice4'],
            'note4'             => $vars['note4'],
            'note'              => $vars['note'],
            'status'            => $vars['status'],
            'booking_type'      => ($name_type_booking && $name_type_booking->name)? $name_type_booking->name : '',
            'staff'             => $name_staff,
            'partner'           => $vars['partner'],
            'dept_date'         => $vars['dept_date'],
            'fullcoc_date'      => $vars['fullcoc_date'],
            'fullpay_date'      => $vars['fullpay_date'],
            'twin_room'         => (int)$vars['room_twin'],
            'double_room'       => (int)$vars['room_double'],
            'triple_room'       => (int)$vars['room_triple'],
            'single_room'       => (int)$vars['room_single'],
            'other_room'        => $vars['room_other'],
            'referral_code'     => $vars['referral_code'],
            'discounttype1'     => $vars['discounttype1'],
            'discountamount1'   => (int)$vars['discountamount1']?:'0',
            'discounttype2'     => $vars['discounttype2'],
            'discountamount2'   => (int)$vars['discountamount2']?:'0',
            'discounttype3'     => $vars['discounttype3'],
            'discountamount3'   => (int)$vars['discountamount3']?:'0',
        ];

        $new = Booking::create($data);
        $new->save(true);

        //log
        $log = BookingLog::create([
            'staff_id' => $thisstaff->getId(),
            'content'=> json_encode($data),
            'ticket_id'=> $vars['ticket_id'],
            'time' => date('Y-m-d H:i:s',time()),
        ]);
        $id_log = $log->save();
    }

    public static function upadteBooking($vars){
        global $cfg,$thisstaff;
        if(!$cfg || !$thisstaff || !$thisstaff->canEditBookings())
            return false;
        $booking_tmp = Booking::lookup((int)$vars['id']);

        if(!$booking_tmp) return false;
        $name_type_booking = TourDestination::lookup((int)$vars['booking_type']);
        $name_staff = new Staff((int)$vars['staff']);
        $name_staff = $name_staff->getFirstName().' '.$name_staff->getLastName();

        $data = [
            'booking_code'      => $vars['booking_code'],
            'booking_code_trim' => _String::trimBookingCode($vars['booking_code']),
            'ticket_number'     => $vars['ticket_number'],
            'receipt_code'      => $vars['receipt_code'],
            'customer'          => $vars['customer'],
            'phone_number'      => $vars['phone_number'],
            'email'             => $vars['email'],
            'booking_type_id'   => $vars['booking_type'],
            'staff_id'          => $vars['staff'],
            'quantity1'         => $vars['quantity1'],
            'retailprice1'      => $vars['retailprice1'],
            'netprice1'         => $vars['netprice1'],
            'note1'             => $vars['note1'],
            'quantity2'         => $vars['quantity2'],
            'retailprice2'      => $vars['retailprice2'],
            'netprice2'         => $vars['netprice2'],
            'note2'             => $vars['note2'],
            'quantity3'         => $vars['quantity3'],
            'retailprice3'      => $vars['retailprice3'],
            'netprice3'         => $vars['netprice3'],
            'note3'             => $vars['note3'],
            'quantity4'         => $vars['quantity4'],
            'retailprice4'      => $vars['retailprice4'],
            'netprice4'         => $vars['netprice4'],
            'note4'             => $vars['note4'],
            'note'              => $vars['note'],
            'status'            => $vars['status'],
            'booking_type'      => ($name_type_booking && $name_type_booking->name)? $name_type_booking->name : '',
            'staff'             => $name_staff,
            'partner'           => $vars['partner'],
            'dept_date'         => $vars['dept_date'],
            'fullcoc_date'      => $vars['fullcoc_date'],
            'fullpay_date'      => $vars['fullpay_date'],
            'twin_room'         => (int)$vars['room_twin'],
            'double_room'       => (int)$vars['room_double'],
            'triple_room'       => (int)$vars['room_triple'],
            'single_room'       => (int)$vars['room_single'],
            'other_room'        => $vars['room_other'],
            'referral_code'     => $vars['referral_code'],
            'discounttype1'     => $vars['discounttype1'],
            'discountamount1'   => (int)$vars['discountamount1']?:'0',
            'discounttype2'     => $vars['discounttype2'],
            'discountamount2'   => (int)$vars['discountamount2']?:'0',
            'discounttype3'     => $vars['discounttype3'],
            'discountamount3'   => (int)$vars['discountamount3']?:'0',
        ];

        $booking_tmp->setAll($data);
        $booking_tmp->save();

        $log = BookingLog::create([
            'staff_id' => $thisstaff->getId(),
            'content'=> json_encode($data),
            'ticket_id'=> $vars['id'],
            'time' => date('Y-m-d H:i:s',time()),
        ]);
        $id_log = $log->save();

        return (int)$vars['id'];
    }

    public static function _calendar($ticket_id) {
        $ticket_id = intval($ticket_id);
        if (!$ticket_id) return false;

        $table = static::TABLE_NAME;
        $sql = "SELECT * FROM $table WHERE ticket_id = $ticket_id";
        $res = db_query($sql);
        if (!$res) return false;
        $row = db_fetch_array($res);
        if (!$row) return false;

        $check_sql = "DELETE FROM ost_ticket_calendar WHERE ticket_id=$ticket_id";
        db_query($check_sql);

        $sql = "INSERT INTO ost_ticket_calendar SET
          ticket_id = $ticket_id,
          start_date = %s,
          end_date = %s,
          title = %s,
          created_at = NOW(),
          created_by = %s
          ";

        if (isset($row['fullpay_date']) && $row['fullpay_date']) {
            $date = date('Y-m-d 08:00:00', $row['fullpay_date']);
            $_sql = sprintf(
                $sql,
                db_input($date),
                db_input($date),
                db_input('Full pay Bk '.$row['booking_code']),
                db_input($row['staff_id'])
            );

            db_query($_sql);
        }

        if (isset($row['fullcoc_date']) && $row['fullcoc_date']) {
            $date = date('Y-m-d 08:00:00', $row['fullpay_date']);
            $_sql = sprintf(
                $sql,
                db_input($date),
                db_input($date),
                db_input('Full coc Bk '.$row['booking_code']),
                db_input($row['staff_id'])
            );

            db_query($_sql);
        }
    }

    public static function fromTicketId($ticket_id) {
        $ticket_id = intval($ticket_id);
        $ticket_id = db_input($ticket_id);
        $table = static::TABLE_NAME;
        $sql = "SELECT booking_code FROM $table WHERE ticket_id = $ticket_id LIMIT 1";
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['booking_code']) || empty($row['booking_code'])) return null;
        return $row['booking_code'];
    }

    public static function getSummaryForecastFlight($from) {
        return static::getSummaryForecast($from, 'flight');
    }

    public static function getSummaryForecastLand($from) {
        return static::getSummaryForecast($from, 'land');
    }

    public static function getSummaryForecast($from, $type) {
        $sql = "SELECT A.qty,
                   A.booking_type_id,
                   DATE_ADD(A.dept, INTERVAL B.".$type."_date DAY) as ".$type."_date, B.".$type."_rate*A.qty as $type
            FROM (select sum(total_quantity) as qty, date(from_unixtime(dept_date)) as dept, booking_type_id
                  from booking_view
                  where date(from_unixtime(dept_date)) >= ".db_input($from)."
                  group by date(from_unixtime(dept_date)), booking_type_id) AS A
                     JOIN (
                SELECT *
                FROM booking_type_list
                ORDER BY name ASC
            ) AS B ON A.booking_type_id = B.id";

        return db_query($sql);
    }
    public static function getBookingLackDocument($from = null, $to = null, $status = 0){
        $sql = 'Select * from booking_tmp where created is not null ';
        if($from !== null)
            $sql .= ' and created >= '.db_input($from);
        else $sql .= ' and created >= '.db_input(date('Y-m-d'));
        if($to !== null )
            $sql .= ' and created <= '.db_input($to);

        $sql .= ' and ticket_id not in
        (select ticket_id from '. BOOKING_DOCUMENT_TABLE.' where action_id = '.$status.') ';
        return db_query($sql);
    }
}

class BookingAction extends VerySimpleModel {
    static $meta = [
        'table' => 'booking_action',
        'pk' => ['booking_ticket_id'],
    ];
}

class BookingReservationModel extends VerySimpleModel {
    static $meta = array(
        'table' => BOOKING_RESERVATION_TABLE,
        'pk' => array('id'),
    );
}

class BookingReservation extends BookingReservationModel {
    const BOOKING_RESERVATION_DISABLE = 0;
    const BOOKING_RESERVATION_ENABLE = 1;
    const APPROVAL_REQUEST = 2;
    const APPROVAL_LEADER = 3;
    const APPROVAL_VISA = 4;
    const APPROVAL_OPERATOR = 5;

    public static function getByTour($tour_id = 0, $visa = null) {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_TABLE
            ." WHERE 1 "
            . ($tour_id ? " AND tour_id=" .db_input(intval($tour_id))." " : " ")
            . ( is_null($visa) ? " " : ( $visa ? " AND visa_only=1 AND country=".db_input($visa)." " : " AND (visa_only=0 OR visa_only IS NULL) " ) )
            ." AND status <> ".self::BOOKING_RESERVATION_DISABLE
            . " ORDER BY created_at DESC ";
        return db_query($sql);
    }

    public static function tourWithVisa($from, $to, $params = []) {
        $sql = "SELECT DISTINCT country, year(created_at) as year, month(created_at) as month FROM ".BOOKING_RESERVATION_TABLE
            ." WHERE 1 "
            ." AND status <> ".self::BOOKING_RESERVATION_DISABLE
            . " AND visa_only=1 ";

        if(isset($params['group_market']) && is_array($params['group_market']))
            if(count($params['group_des']) > 0)
                $sql .= " AND country IN (" . implode(",", $params['group_market']) . ") ";
            else
                $sql .= " AND 1 = 0 ";

        if (isset($params['reservation_id']) && is_numeric($params['reservation_id']) && $params['reservation_id']) {
            $item = BookingReservation::lookup($params['reservation_id']);
            if (!$item || !( isset($item->country) && $item->country && !(isset($item->tour_id) && $item->tour_id) )) {
                $sql .= ' AND 1=0 ';
            } else {
                $sql .= ' AND country=' . db_input($item->country) . ' ';
            }
        }

        if (isset($params['from']) && strtotime($params['from'])) {
            $from = trim($params['from']);
            $sql .= " AND date(created_at) >= ".db_input($from)." ";
        }

        if (isset($params['to']) && strtotime($params['to'])) {
            $to = trim($params['to']);
            $sql .= " AND date(created_at) <= ".db_input($to)." ";
        }

        if (isset($params['customer_name']) && !empty(trim($params['customer_name']))) {
            $sql .= " AND customer_name LIKE '%".trim($params['customer_name'])."%' ";
        }

        if (isset($params['customer_phonenumber']) && !empty(trim($params['customer_phonenumber']))) {
            $sql .= " AND phone_number LIKE '%".trim($params['customer_phonenumber'])."%' ";
        }

        if (isset($params['country']) && $params['country']) {
            $sql .= " AND country=".(int)($params['country'])." ";
        }

        if(isset($params['staff_id']) && (int)$params['staff_id']){
            $sql .= " AND staff_id=".(int)($params['staff_id']);
        }
        //result 0 when search tour_name, air_line with VISA only
        if($params['tour_name'] || $params['airline'] )
            $sql .= ' AND 1=0 ';

        $sql .= " GROUP BY country, year, month "
            . " ORDER BY created_at DESC ";
        return db_query($sql);
    }

    public static function upcoming() {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_TABLE." WHERE 1
        AND (hold_qty IS NOT NULL AND hold_qty <> 0 AND hold_qty <> '')
        AND TIMESTAMPDIFF(HOUR, due_at, NOW()) >= -12
        AND status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY due_at DESC ";
        return db_query($sql);
    }

    public static function getDueFlowTime($hour_diff_now) {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_TABLE."
        WHERE hold_qty IS NOT NULL AND hold_qty <> 0 AND hold_qty <> ''
        AND status <> ".self::BOOKING_RESERVATION_DISABLE;
        if($hour_diff_now <= 0 )
            $sql .= " AND TIMESTAMPDIFF(HOUR, due_at, NOW()) >= ". $hour_diff_now. " AND TIMESTAMPDIFF(HOUR, due_at, NOW()) < 0";
        else
            $sql .= " AND TIMESTAMPDIFF(HOUR, due_at, NOW()) <= ". $hour_diff_now. " AND TIMESTAMPDIFF(HOUR, due_at, NOW()) > 0";

        $sql .= " ORDER BY staff_id,due_at DESC";

        return db_query($sql);
    }

    public static function staffReservation($id_staffReservation, $tours, $params) {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_TABLE." WHERE 1
        AND (hold_qty IS NOT NULL AND hold_qty <> 0 AND hold_qty <> '')
        AND status <> ".self::BOOKING_RESERVATION_DISABLE."
        AND staff_id = ".(int)$id_staffReservation;

        if($tours)
            $sql .= " AND tour_id IN "."('".implode("','",$tours)."')";
        else
            $sql .= " AND 1 = 0 ";

        if(isset($params['customer_name']) && $params['customer_name'])
            $sql .= " AND customer_name LIKE ".db_input('%'.$params['customer_name'].'%');

        if(isset($params['customer_phonenumber']) && $params['customer_phonenumber'])
            $sql .= " AND phone_number = ".$params['customer_phonenumber'];

        $sql .= " ORDER BY due_at DESC ";

        return db_query($sql);
    }

    public static function leaderReview() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status= ".self::APPROVAL_REQUEST.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.updated_at ASC ";
        return db_query($sql);
    }

    public static function leaderReviewed() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status > ".self::APPROVAL_REQUEST.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.leader_review_at DESC ";
        return db_query($sql);
    }

    public static function visaReview() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status=".self::APPROVAL_LEADER.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.leader_review_at ASC ";
        return db_query($sql);
    }

    public static function visaReviewed() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status > ".self::APPROVAL_LEADER.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.visa_review_at DESC ";
        return db_query($sql);
    }

    public static function opReview() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status=".self::APPROVAL_VISA.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.visa_review_at ASC ";
        return db_query($sql);
    }

    public static function opReviewed() {
        $sql = "SELECT r.*, b.total_retail_price FROM ".BOOKING_RESERVATION_TABLE." r
        LEFT JOIN ".Booking::getViewName()." b
        ON r.booking_code_trim LIKE b.booking_code_trim
        WHERE 1
        AND r.booking_code IS NOT NULL
        AND r.booking_code <> ''
        AND (r.status > ".self::APPROVAL_VISA.")
        AND (r.hold_qty = 0 OR r.hold_qty IS NULL OR r.hold_qty LIKE '')
        AND (r.sure_qty <> 0 AND r.sure_qty IS NOT NULL AND r.sure_qty <> '')
        AND r.status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY r.op_review_at DESC ";
        return db_query($sql);
    }

    public static function recently() {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_HISTORY_TABLE." WHERE 1
        ORDER BY created_at DESC LIMIT ".PAGE_LIMIT;
        return db_query($sql);
    }

    public static function overdue() {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_TABLE." WHERE 1
        AND (hold_qty IS NOT NULL AND hold_qty <> 0 AND hold_qty <> '')
        AND status <> ".self::BOOKING_RESERVATION_DISABLE."
        ORDER BY due_at DESC AND isoverdue=1";
        return db_query($sql);
    }

    public static function setOverdue() {
        $sql = "UPDATE ".BOOKING_RESERVATION_TABLE." SET isoverdue=1
        WHERE 1
        AND (hold_qty IS NOT NULL AND hold_qty <> 0 AND hold_qty <> '')
        AND due_at IS NOT NULL AND due_at <> '' AND due_at <> 0
        AND due_at <= NOW()
        AND (isoverdue=0 OR isoverdue IS NULL)
        AND status <> ".self::BOOKING_RESERVATION_DISABLE;
        return db_query($sql);
    }

    public static function deleteOverdue() {
        db_start_transaction();
        try {
            $sql = "SELECT id, tour_id FROM ".BOOKING_RESERVATION_TABLE."
            WHERE 1
            AND isoverdue=1
            AND due_at IS NOT NULL AND due_at <> '' AND due_at <> 0
            AND DATEDIFF(due_at, NOW()) < -1
            AND status <> ".self::BOOKING_RESERVATION_DISABLE;
            $res = db_query($sql);
            while ($res && ($row = db_fetch_array($res))) {
                if (isset($row['tour_id']) && intval($row['tour_id']))
                    if (!TourNew::updateQuantity($row['tour_id'])) throw new Exception('DB Error');

                $sql = "INSERT INTO ".BOOKING_RESERVATION_HISTORY_TABLE." (
                booking_reservation_id,
                tour_id,
                staff_id,
                customer_name,
                phone_number,
                booking_code,
                created_at,
                created_by,
                hold_qty,
                sure_qty,
                infant,
                visa_only,
                fe,
                one_way,
                visa_ready,
                security_deposit,
                country,
                note
            )
                SELECT
                id,
                tour_id,
                staff_id,
                customer_name,
                phone_number,
                booking_code,
                NOW(),
                0,
                0,
                0,
                0,
                visa_only,
                fe,
                one_way,
                visa_ready,
                security_deposit,
                country,
                'SYSTEM'
                FROM ".BOOKING_RESERVATION_TABLE." WHERE id=".db_input($row['id']) . " LIMIT 1 ";

                if (!db_query($sql)) throw new Exception('DB Error');
            }

            $sql = " UPDATE ".BOOKING_RESERVATION_TABLE."
                SET status = ".self::BOOKING_RESERVATION_DISABLE."
                WHERE 1
                AND isoverdue=1
                AND due_at IS NOT NULL AND due_at <> '' AND due_at <> 0
                AND DATEDIFF(due_at, NOW()) < -1 ";
            if (!db_query($sql)) throw new Exception('DB Error');

            db_commit();
        } catch(Exception $ex) {
            db_rollback();
        }
    }

    public static function hold($data) {
        global $thisstaff;

        $data['created_by'] = $thisstaff->getId();
        $data['booking_code_trim'] = _String::trimBookingCode($data['booking_code']);
        $data['status']  = self::BOOKING_RESERVATION_ENABLE;
        $reservation = BookingReservation::create($data);
        $_id = $reservation->save();

        if (!$_id || !BookingReservation::lookup($_id)) return false;

        $data['booking_reservation_id'] = $_id;
        unset($data['booking_code_trim']);
        unset($data['status']);
        $history = BookingReservationHistory::create($data);
        $id = $history->save();

        if (!$id || !BookingReservationHistory::lookup($id)) return false;

        if (isset($data['tour_id']) && $data['tour_id'])
            TourNew::updateQuantity($data['tour_id']);

        return $_id;
    }

    public static function update($data) {
        global $thisstaff;

        $data['updated_by'] = $thisstaff->getId();
        $data['updated_at'] = new SqlFunction('NOW');
        $data['booking_code_trim'] = _String::trimBookingCode($data['booking_code']);
        $reservation = BookingReservation::lookup(['id' => $data['id']]);
        if (!$reservation) return false;
        $reservation->setAll($data);
        $_id = $reservation->save();

        if (!$_id || !BookingReservation::lookup($_id)) return false;

        unset($data['id']);
        unset($data['updated_by']);
        unset($data['updated_at']);
        unset($data['booking_code_trim']);
        unset($data['status']);
        $data['created_at'] = new SqlFunction('NOW');
        $data['booking_reservation_id'] = $_id;
        $data['created_by'] = $thisstaff->getId();
        $history = BookingReservationHistory::create($data);
        $id = $history->save();

        if (!$id || !BookingReservationHistory::lookup($id)) return false;

        if (isset($data['tour_id']) && $data['tour_id'])
            TourNew::updateQuantity($data['tour_id']);

        return $_id;
    }

    public static function cancel($data, $type = 'all') {
        global $thisstaff;
        $reason = $data['reason'];
        $reservation = BookingReservation::lookup(['id' => $data['id']]);
        if (!$reservation) return false;
        $data = $reservation->ht;

        if($type === 'hold' && $reservation->sure_qty){
            if(!$reservation->hold_qty) return false;
            $reservation->set('hold_qty', 0);
            $data['hold_qty'] = 0;
            $reservation->save();
        }

        if($type === 'sure' && $reservation->hold_qty){
            if(!$reservation->sure_qty) return false;
            $reservation->set('sure_qty', 0);
            $data['sure_qty'] = 0;
            $reservation->save();
        }

        if(($type === 'hold' && !$reservation->sure_qty) || ($type === 'sure' && !$reservation->hold_qty) || $type === 'all'){
            if(!$reservation->delete())
                return false;
            $data['hold_qty'] = 0;
            $data['sure_qty'] = 0;
            $data['infant'] = 0;
        }

        $data['booking_reservation_id'] = $data['id'];
        $data['created_at'] = new SqlFunction('NOW');
        unset($data['id']);
        unset($data['created_by']);
        unset($data['updated_by']);
        unset($data['status']);
        unset($data['updated_at']);
        unset($data['status']);
        unset($data['booking_code_trim']);
        $data['created_by'] = $thisstaff->getId();
        $data['note'] .= ' reason:'.$reason;
        $history = BookingReservationHistory::create($data);
        $id = $history->save();

        if (!$id || !BookingReservationHistory::lookup($id)) return false;

        if (isset($data['tour_id']) && $data['tour_id']){
            TourNew::updateQuantity($data['tour_id']);
            self::resetOver($data['tour_id'],$data['booking_reservation_id']); // do not over after cancel
        }
        return true;
    }

    public static function countOverHold($id){
        $sql = "SELECT count(*) as over FROM ".static::$meta['table']." where tour_id = ".(int)$id." && `over` = 1
        AND status <> ".self::BOOKING_RESERVATION_DISABLE;
        $result = db_query($sql);

        return db_fetch_array($result)['over'];
    }

    public static function resetOver($tour_id = 0, $reservation_id = 0){
        $tour = TourNew::lookup((int)$tour_id);
        if( !$tour) return;

        $tour_available = $tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity;
        if($tour_available >= 0) {
            $reservation = BookingReservation::lookup((int)$reservation_id);
            if(!$reservation) return;
            $reservation->set('over',0);
            $reservation->save();
        }
    }
    public static function set_content($row){
        global $cfg;
        $content = '';
        $info = '';
        $link_tab = '';
        if ($row['tour_id'] !== null) {
            $tour = TourNew::lookup((int)$row['tour_id']);
            if ($tour) {
                $info= $tour->name;
                $link_tab = '#tourlist';
            } else {
                $info = 'Không tìm thấy tour';
            }

        } elseif($row['country']) {
            $info = 'VISA ONLY';
            $country = TourMarket::lookup((int)$row['country']);
            if($country){
                $info = $country->name;
                $link_tab = '#visareservation';
            }
        }

        $temp = $row['customer_name'].' - '.$row['phone_number'].' - (Hold: '.(int)$row['hold_qty']
            .' Sure: '.(int)$row['sure_qty'].') - '.$info;

        //$content .= '<p><a href="http://localhost/osticket/scp/reservation.php?reservation_id='.$row['id'] .'">'.$count.'. </a>';
        $content .= $temp. '  ';
        $content .= '<a href="'.$cfg->getUrl().'scp/reservation.php?reservation_id='.$row['id'].$link_tab.'" target="_blank"> Link </a>';
        return $content;
    }
}

class BookingReservationHistoryModel extends VerySimpleModel {
    static $meta = array(
        'table' => BOOKING_RESERVATION_HISTORY_TABLE,
        'pk' => array('id'),
    );
}

class BookingReservationHistory extends BookingReservationHistoryModel {
    public static function getByTour($tour_id = 0, $visa = null) {
        $sql = "SELECT * FROM ".BOOKING_RESERVATION_HISTORY_TABLE
            ." WHERE 1 "
            . ($tour_id ? " AND tour_id=" .db_input(intval($tour_id))." " : " ")
            . ( is_null($visa) ? " " : ( $visa ? " AND visa_only=1 AND country=".db_input($visa)." " : " AND (visa_only=0 OR visa_only IS NULL) " ) )
            . " ORDER BY created_at DESC ";
        return db_query($sql);
    }
    public static function search_staff_pax($params = []){
        $sql = "SELECT bk.id as `current_id`, history_bk.* FROM ".BOOKING_RESERVATION_HISTORY_TABLE
            ." as history_bk
            LEFT JOIN ".BOOKING_RESERVATION_TABLE." as bk ON history_bk.booking_reservation_id = bk.id
            WHERE 1 ";

        if (isset($params['from']) && strtotime($params['from'])) {
            $from = trim($params['from']);
            $sql .= " AND date(history_bk.created_at) >= ".db_input($from)." ";
        }

        if (isset($params['to']) && strtotime($params['to'])) {
            $to = trim($params['to']);
            $sql .= " AND date(history_bk.created_at) <= ".db_input($to)." ";
        }

        if (isset($params['customer_name']) && !empty(trim($params['customer_name']))) {
            $sql .= " AND history_bk.customer_name LIKE '%".trim($params['customer_name'])."%' ";
        }

        if (isset($params['customer_phonenumber']) && !empty(trim($params['customer_phonenumber']))) {
            $sql .= " AND history_bk.phone_number LIKE '%".trim($params['customer_phonenumber'])."%' ";
        }

        if (isset($params['country']) && $params['country']) {
            $sql .= " AND history_bk.country= ".(int)($params['country'])." ";
        }

        if(isset($params['staff_id']) && (int)$params['staff_id']){
            $sql .= " AND history_bk.staff_id= ".(int)($params['staff_id']);
        }

        $where_tour = '';
        if (isset($params['airline']) && is_numeric($params['airline']) && $params['airline']) {
            $where_tour .= " AND airline = ".(int)$params['airline'];
        }

        if(isset($params['tour_name']) && $params['tour_name'])
            $where_tour .= " AND name LIKE ". db_input('%'.trim($params['tour_name']).'%');

        //search info tour
        if($where_tour !== '' && ($params['airline'] || $params['tour_name'])){
            $sql_tour = 'SELECT id FROM ost_tour WHERE 1 '.$where_tour;

            $res = db_query($sql_tour);
            if(db_num_rows($res) > 0){
                $arr_tour_id = [];
                while ($res && ($row = db_fetch_array($res))){
                    $arr_tour_id[] = $row['id'];
                }
                $sql .= " AND history_bk.tour_id IN "."('".implode("','",$arr_tour_id)."')";
            }else
                $sql .= " AND 1 = 0";
        }

        $sql .= " ORDER BY bk.id DESC, history_bk.created_at DESC ";
        return db_query($sql);
    }
}

class PaymentAnalytics {
    public static function getPagination($params, $offset, &$total, $limit) {
        $sql = " select booking_view.staff_id, booking_view.total_quantity,
        payment_tmp.receipt_code,
        booking_view.booking_type_id, booking_view.booking_code,
            SUM( CASE WHEN payment_tmp.topic_id=".THU_TOPIC." THEN payment_tmp.amount ELSE (-1*payment_tmp.amount) END ) as total
            from booking_view  join payment_tmp  ON
               booking_view.booking_code IS NOT NULL AND booking_view.booking_code<>''
              AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''
              AND booking_view.booking_code_trim = payment_tmp.booking_code_trim
            ";

        $sql .= " WHERE 1 ";

        $limit = " LIMIT $limit OFFSET $offset ";

        static::sql($sql, $params);

        $sql .= " GROUP BY booking_view.staff_id, booking_view.booking_code ";

        $res = db_query("$sql ORDER BY total DESC $limit");
        $total = db_num_rows(db_query($sql));

        return $res;
    }

    public static function getTotalByStaff($params) {
        $sql = " select
            SUM( CASE WHEN payment_tmp.topic_id=".THU_TOPIC." THEN payment_tmp.amount ELSE (-1*payment_tmp.amount) END ) as total
            from booking_view  join payment_tmp  ON
               booking_view.booking_code IS NOT NULL AND booking_view.booking_code<>''
              AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''
              AND booking_view.booking_code_trim = payment_tmp.booking_code_trim
            ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $sql .= " GROUP BY booking_view.staff_id ";

        $res = db_query("$sql");
        if (!$res) return 0;
        $data = db_fetch_array($res);
        if (!$data || !isset($data['total'])) return 0;

        return $data['total'];
    }

    public static function getSummaryTable($params) {
        $sql = " select booking_view.staff_id,
            SUM( CASE WHEN payment_tmp.topic_id=".THU_TOPIC." THEN payment_tmp.amount ELSE (-1*payment_tmp.amount) END ) as total
            from booking_view  join payment_tmp  ON
               booking_view.booking_code IS NOT NULL AND booking_view.booking_code<>''
              AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''
              AND booking_view.booking_code_trim = payment_tmp.booking_code_trim
            ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $sql .= " GROUP BY booking_view.staff_id ";

        $res = db_query("$sql ORDER BY total DESC ");

        return $res;
    }

    public static function getChart($params) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(FROM_UNIXTIME(payment_tmp.time), 5), year(FROM_UNIXTIME(payment_tmp.time))';
                $select = 'week(FROM_UNIXTIME(payment_tmp.time), 5) as week, year(FROM_UNIXTIME(payment_tmp.time)) as year';
                break;
            case 'month':
                $group = 'month(FROM_UNIXTIME(payment_tmp.time)), year(FROM_UNIXTIME(payment_tmp.time))';
                $select = 'month(FROM_UNIXTIME(payment_tmp.time)) as month, year(FROM_UNIXTIME(payment_tmp.time)) as year';
                break;
            case 'day':
            default:
                $group = 'date(FROM_UNIXTIME(payment_tmp.time))';
                $select = 'date(FROM_UNIXTIME(payment_tmp.time)) as date';
                break;
        }

        $sql = " select booking_view.staff_id, $select,
            SUM( CASE WHEN payment_tmp.topic_id=".THU_TOPIC." THEN payment_tmp.amount ELSE (-1*payment_tmp.amount) END ) as total
            from booking_view  join payment_tmp  ON
               booking_view.booking_code IS NOT NULL AND booking_view.booking_code<>''
              AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''
              AND booking_view.booking_code_trim = payment_tmp.booking_code_trim
            ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $sql .= " GROUP BY booking_view.staff_id, $group ";

        $res = db_query($sql);

        return $res;
    }

    private static function sql(&$sql, $params) {
        $_status = trim(json_encode(trim('Hủy')), '"');
        $sql .= sprintf(" AND (  booking_view.status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%' ");

        $sql .= " AND (
        (payment_tmp.income_type NOT LIKE '%\"".PAYMENT_NOT_IN."\"%' AND payment_tmp.outcome_type IS NULL) ";
        $sql .= " OR
        (payment_tmp.outcome_type NOT LIKE '%\"".PAYMENT_NOT_OUT."\"%' ";
        $sql .= " AND payment_tmp.outcome_type NOT LIKE '%\"".PAYMENT_NOT_OUT1."\"%' ";
        $sql .= " AND payment_tmp.outcome_type NOT LIKE '%\"".PAYMENT_NOT_OUT2."\"%' AND payment_tmp.income_type IS NULL)
        )";
        if (isset($params['staff_id']) && $params['staff_id']) {
            $sql .= " AND booking_view.staff_id = ".db_input(trim($params['staff_id']))." ";
        }

        if (isset($params['from']) && $params['from']) {
            $sql .= " AND DATE(from_unixtime(payment_tmp.time)) >= ".db_input(trim($params['from']))." ";
            $sql .= " AND DATE(booking_view.created ) >= ".db_input(date('Y-m-d', strtotime(trim($params['from']))-24*3600*365))." ";
        }

        if (isset($params['to']) && $params['to']) {
            $sql .= " AND DATE(from_unixtime(payment_tmp.time)) <= ".db_input(trim($params['to']))." ";
            $sql .= " AND DATE(booking_view.created) <= ".db_input(date('Y-m-d', strtotime(trim($params['to']))+24*3600*365))." ";
        }

        return $sql;
    }
}

class BookingTypeList {
    public static function refresh() {
        $data = static::_load_booking_type_list();
        static::_insert_booking_type_list_to_table($data);
    }

    private static function _load_booking_type_list() {
        $list = DynamicList::lookup(BOOKING_TYPE_LIST);
        $items = $list->getItems();
        $data = [];
        foreach ($items as $item) {
            $config = $item->getConfiguration();
            $country = $land_date = $flight_rate = $land_rate = $flight_date = null;
            foreach ($config as $_field_id => $_data) {
                $formfield = DynamicFormField::lookup($_field_id);
                if (!$formfield) continue;

                switch ($formfield->get('name')) {
                    case 'country':
                        if (!$_data) {
                            $country = null;
                            break;
                        }

                        reset($_data);
                        $_field_value = array_keys($_data);

                        if (!isset($_field_value[0])) {
                            $country = null;
                            break;
                        }
                        $country = $_field_value[0];
                        break;
                    case 'land_date': // for land
                        $land_date = $_data;
                        break;
                    case 'flight_date':
                        $flight_date = $_data;
                        break;
                    case 'flight_rate':
                        $flight_rate = $_data;
                        break;
                    case 'land_rate':
                        $land_rate = $_data;
                        break;
                    default:
                        continue 2;
                } // end switch
            } // end foreach

            $data[] = [
                'id' => $item->getId(),
                'name' => $item->getValue(),
                'country' => $country,
                'land_rate' => $land_rate,
                'flight_rate' => $flight_rate,
                'land_date' => $land_date, // for land
                'flight_date' => $flight_date,
            ];
        } // end foreach

        return $data;
    }

    private static function _insert_booking_type_list_to_table(array $data) {
        if (!is_array($data) || !count($data)) return false;
        $sql = "REPLACE INTO booking_type_list(
            id,
            name,
            country,
            land_rate,
            flight_rate,
            land_date,
            flight_date
            ) VALUES ";

        $flag = false;

        foreach($data as $item) {
            $sql .= sprintf(
                "(%s, %s, %s, %s, %s, %s, %s),",
                db_input($item['id']),
                db_input($item['name']),
                db_input($item['country']),
                db_input($item['land_rate']),
                db_input($item['flight_rate']),
                db_input($item['land_date']),
                db_input($item['flight_date'])
            );
            $flag = true;
        }

        if (!$flag) return false;

        $sql = trim($sql, ',');
        return db_query($sql);
    }

    public static function loadList() {
        $sql = "SELECT * FROM booking_type_list ORDER BY name ASC";
        $res = db_query($sql);
        $data = [];
        while($res && ($row = db_fetch_array($res))) {
            $data[ $row['id'] ] = $row;
        }
        return $data;
    }
}

class BookingExpenseForecast extends \koolreport\KoolReport {
    public static function analytics() {
        BookingTypeList::refresh();
        $from = date('Y-m-d', time()-24*3600*30);
        $flight_summary = Booking::getSummaryForecastFlight($from);
        $land_summary = Booking::getSummaryForecastLand($from);
        static::_remove_data($from);
        static::_forecast_process($land_summary, $flight_summary);
    }

    public function settings()
    {
        return array(
            "dataSources"=>array(
                "booking"=>array(
                    "connectionString"=>sprintf("mysql:host=%s;dbname=%s", DBHOST, DBNAME),
                    "username"=>DBUSER,
                    "password"=>DBPASS,
                    "charset"=>"utf8"
                ),
            )
        );
    }

    public function setup()
    {
        switch ($this->params['group']) {
            case 'week':
                $select = 'CONCAT(week(pay_date, 5) , \'-\', year(pay_date)) as pay_date';
                $group = 'week(pay_date, 5), year(pay_date) ';
                break;
            case 'month':
                $select = 'CONCAT(month(pay_date), \'/\', year(pay_date)) as pay_date';
                $group = 'month(pay_date) , year(pay_date) ';
                break;
            case 'day':
            default:
                $select = 'pay_date';
                $group = 'pay_date';
                break;
        }

        $sql = "SELECT $select, sum(land) as land, sum(flight) flight, SUM(IFNULL(land, 0) + IFNULL(flight, 0)) as total, sum(qty) qty FROM booking_paid_forecast WHERE 1 ";

        if (isset($this->params['from']) && date_create_from_format('Y-m-d', $this->params['from']))
            $sql .= " AND pay_date >= ".db_input($this->params['from'])." ";

        if (isset($this->params['to']) && date_create_from_format('Y-m-d', $this->params['to']))
            $sql .= " AND pay_date <= ".db_input($this->params['to'])." ";

        if (isset($this->params['booking_type_id']) && intval($this->params['booking_type_id']))
            $sql .= " AND booking_type_id = ".db_input(intval($this->params['booking_type_id']))." ";

        $sql .= " GROUP BY $group ORDER BY $group ASC ";

        $this->src('booking')
            ->query($sql)
            ->pipe($this->dataStore('booking_pay_forecast'));
    }

    private static function _forecast_process($land_summary, $flight_summary) {
        static::_forecast_process_update($land_summary, 'land');
        static::_forecast_process_update($flight_summary, 'flight');
    }

    private static function _forecast_process_update($data, $type) {
        $sql = "REPLACE INTO booking_paid_forecast(pay_date, booking_type_id, $type, qty) VALUES ";

        $flag = false;
        while ($data && ($row = db_fetch_array($data))) {
            $sql .= sprintf(
                "(%s, %s, %s, %s),",
                db_input($row[$type.'_date']),
                db_input($row['booking_type_id']),
                db_input(round(floatval($row[$type]), 2)),
                db_input($row['qty'])
            );

            $flag = true;
        } // end while

        if (!$flag) return false;
        $sql = trim($sql, ',');
        if ($sql) db_query($sql);
    }

    private static function _remove_data($from) {
        $sql = "DELETE FROM booking_paid_forecast WHERE pay_date>=".db_input($from);
        return db_query($sql);
    }
}

class BookingReservationImportLogModel extends VerySimpleModel {
    static $meta = array(
        'table' => BOOKING_RESERVATION_IMPORT_LOG_TABLE,
        'pk' => array('id'),
    );
}

class BookingReservationImportLog extends BookingReservationImportLogModel
{

}

class BookingDocumentModel extends VerySimpleModel{
    static $meta = array(
        'table' => BOOKING_DOCUMENT_TABLE,
        'pk' => array('id'),
    );
}
class BookingDocument extends BookingDocumentModel{
    const COMMITMENT_FILE_UPLOAD = 1;
    const CONTRACT_FILE_UPLOAD = 2;
}

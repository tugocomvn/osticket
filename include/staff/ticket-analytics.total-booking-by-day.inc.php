<?php
$booking_by_day = TicketAnalytics::bookingAnalytics($_REQUEST['from'], $_REQUEST['to'], 'booking_by_day', $_REQUEST);
$total_booking_arr = [];
while($booking_by_day && ($row = db_fetch_array($booking_by_day))) {
    switch ($_REQUEST['group']) {
        case 'week':
            $total_booking_arr[ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $total_booking_arr[ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $total_booking_arr[ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}
?>
<td colspan="">
    <h2>Total Booking by day</h2>
    <canvas id="total_booking" width="500" height="300"></canvas>
</td>
<script>
    var total_booking_elm = document.getElementById("total_booking").getContext('2d');
    i = 0;
    var total_booking = new Chart(total_booking_elm, {
        type: 'line',
        data: {
            datasets: [
                {
                    borderColor: '#c55',
                    label: 'Booking By Day',
                    data: $.parseJSON('<?php echo json_encode(array_values($total_booking_arr)) ?>')
                }
            ],
            labels: $.parseJSON('<?php echo json_encode(array_keys($total_booking_arr)) ?>'),
        },
        options: {
            responsive: false,
            maintainAspectRatio: false
        }
    });
</script>
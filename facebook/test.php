<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
// Add to header of your file
use FacebookAds\Api;
// Add to header of your file
use FacebookAds\Object\AdUser;

// Init PHP Sessions
session_start();

$fb = new Facebook([
    'app_id' => '1707934562753534',
    'app_secret' => '301d0b445aea8a2e2398031b98476c57',
]);

$helper = $fb->getRedirectLoginHelper();

if (!isset($_SESSION['facebook_access_token'])) {
    $_SESSION['facebook_access_token'] = null;
}

if (!$_SESSION['facebook_access_token']) {
    $helper = $fb->getRedirectLoginHelper();
    try {
        $_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
    } catch(FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
}

if ($_SESSION['facebook_access_token']) {
    echo "You are logged in!";

    // Add after echo "You are logged in "

    // Initialize a new Session and instantiate an Api object
    Api::init(
        '1707934562753534', // App ID
        '301d0b445aea8a2e2398031b98476c57',
        $_SESSION['facebook_access_token'] // Your user access token
    );

    // Add after Api::init()
    $me = new AdUser('me');
    $my_adaccount = $me->getAdAccounts()->current();

} else {
    $permissions = ['ads_management'];
    $loginUrl = $helper->getLoginUrl('http://localhost:889/test/facebook/test.php', $permissions);
    echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
}


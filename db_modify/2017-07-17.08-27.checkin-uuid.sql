ALTER TABLE ost_checkin ADD uuid VARCHAR(32) NULL;
CREATE INDEX ost_checkin_uuid_index ON ost_checkin (uuid);
CREATE INDEX ost_checkin_staff_id_index ON ost_checkin (staff_id);
CREATE INDEX ost_checkin_checkin_time_index ON ost_checkin (checkin_time);
CREATE INDEX ost_checkin_ip_address_index ON ost_checkin (ip_address);

ALTER TABLE osticket_dev.ost_user_email MODIFY user_id INT(10) unsigned;
CREATE INDEX ost_config_namespace_index ON osticket_dev.ost_config (namespace);
CREATE INDEX ost_staff_group_id_index ON osticket_dev.ost_staff (group_id);
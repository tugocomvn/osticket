<?php

/**
 * Created by PhpStorm.
 * User: cosmos
 * Date: 7/20/16
 * Time: 5:25 PM
 */

require_once(dirname(__FILE__).'/lib/nusoap.php');

try {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        throw new Exception('POST method is required.', 405);

    if (!isset($_POST['code']) || empty($_POST['code']) || $_POST['code'] !== '*&CH*a(21*h&A^4%34pSU$v^&s*()')
        throw new Exception('Goodbye my love.', 403);

    if (!isset($_POST['phone_number']) || empty($_POST['phone_number']))
        throw new Exception('Phone number is required.', 400);

    if (!isset($_POST['message']) || empty($_POST['message']))
        throw new Exception('Message is required.', 400);

    if (!isset($_POST['checking']) || empty($_POST['checking'])
        || $_POST['checking'] !== hash('sha256', ($_POST['phone_number'].$_POST['message'].$_POST['phone_number'])))
        throw new Exception('I love you.', 403);

    $errors = [];
    if (! ( $msgId = _SMS::send($_POST['phone_number'], $_POST['message'], $errors) ) )
        throw new Exception(isset($errors['sending_failed']) ? $errors['sending_failed']
            : (isset($errors['error']) ? $errors['error'] : 'Unknown Error'), 500);

    http_response_code(201);
    echo json_encode(['result' => 'Success', 'msgId' => $msgId]);
    exit;
} catch(Exception $e) {
    http_response_code($e->getCode() ?: 500);
    echo json_encode(['error' => $e->getMessage(), 'dump' => serialize($errors)]);
    exit;
}

class _SMS
{
    public static function send($phone_number, $message, &$error)
    {
        $client = new nusoap_client("http://brandsms.vn:8018/VMGAPI.asmx?wsdl", 'wsdl', '', '', '', '');
        $err = $client->getError();
        if ($err) {
            $error[] = $err;
            return false;
        }

        $phone_number = trim($phone_number);
        $phone_number = trim($phone_number, '+');

        if (!preg_match('/^84[^0][0-9]{8,9}/', $phone_number)) { // ! 841692998182
            if (preg_match('/^0[0-9]{8,9}/', $phone_number)) // 0909817306
                $phone_number = preg_replace('/^0/', '84', $phone_number);
            elseif (preg_match('/^[^0][0-9]{8,9}/', $phone_number)) // 909817306
                $phone_number = '84'.$phone_number;
            else {
                $error['phonenumber_format'] = 'Wrong phone number format';
                return false;
            }
        }

        $message = trim($message);

        $result = $client->call('BulkSendSms',
            array(
                'msisdn' => $phone_number,
                'alias'=> 'Tugo',
                'message' => $message,
                'sendTime'=>'',
                'authenticateUser'=>'tugo',
                'authenticatePass'=>'vmg123456'
            ),'','',''
        );
        $result = $result['BulkSendSmsResult'];

        if (isset($result['error_code']) && $result['error_code'] == 0 && isset($result['messageId']))
            return $result['messageId'];

        $result_code = [
            '-2' => 'Invalid input parameters',
            '-1' => 'Sending error (Message content unicode character)',
            '0' => 'Success',
            '100' => 'Authentication failure',
            '101' => 'Authentication User is deactived',
            '102' => 'Authentication User is expired',
            '103' => 'Authentication User is locked',
            '104' => 'Template not actived',
            '105' => 'Template does not existed',
            '108' => 'Msisdn in blackList',
            '304' => 'Send the same content in short time',
            '400' => 'Not enough money',
            '900' => 'System is error',
            '901' => 'Length of message is 612 with noneUnicode message and 266 with Unicode message',
            '902' => 'Number of msisdn must be > 0',
            '904' => 'Brandname is inactive',
        ];

        if (isset($result_code[ $result['error_code'] ]) && $result_code[ $result['error_code'] ])
            $error['sending_failed'] = $result_code[ $result['error_code'] ];

        if ($err = $client->getError())
            $error['error'] = $err;

        return false;
    }
}

class _SMS_NEW {
    public static function send($phone_number, $message, &$error) {

        $client = new nusoap_client("http://wcf.worldsms.vn/apisms.svc", 'wsdl', '', '', '', '');
        $err = $client->getError();
        if ($err) {
            $error[] = $err;
            return false;
        }

        $phone_number = trim($phone_number);
        $phone_number = trim($phone_number, '+');

        if (!preg_match('/^84[^0][0-9]{8,9}/', $phone_number)) { // ! 841692998182
            if (preg_match('/^0[0-9]{8,9}/', $phone_number)) // 0909817306
                $phone_number = preg_replace('/^0/', '84', $phone_number);
            elseif (preg_match('/^[^0][0-9]{8,9}/', $phone_number)) // 909817306
                $phone_number = '84'.$phone_number;
            else {
                $error['phonenumber_format'] = 'Wrong phone number format';
                return false;
            }
        }

        $message = trim($message);

        $result = $client->call('PushMsg2Phone',
            array(
                'Phone' => $phone_number,
                'Sender'=> 'VienThongMN',
                'Msg' => $message,
                'Username'=>'togo',
                'Password'=>'smstogo@stel'
            ),'','',''
        );

        $result_code = [
            '1' => 'Success',
            '0' => 'Authentication failure',
            '3' => 'Invalid Phone number',
            '4' => 'Message is too long',
            '9' => 'Invalid brandname',
            '16' => 'Spam cmnr',
        ];

        if (isset($result_code[ $result ]) && $result_code[ $result ])
            $error['sending_failed'] = $result_code[ $result ];

        if ($err = $client->getError())
            $error['error'] = $err;

        return false;
    }
}

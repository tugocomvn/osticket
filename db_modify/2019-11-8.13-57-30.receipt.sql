ALTER TABLE `ost_groups` ADD `can_view_receipt` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_create_receipt` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_edit_receipt` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_view_receipt_list` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_approve_receipt` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

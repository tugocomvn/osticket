<?php
/*************************************************************************
statistics.php

thong ke nhung ticket agent duoc assign & trang thai hien tai cua ticket trong 1 khoang thoi gian xac dinh

Minh Bao <beckerbao29@gmail.com>
 **********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.log.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');

if(!defined('OSTSCPINC') || !$thisstaff || !@$thisstaff->isStaff()) die('Access Denied');
//Navigation
$nav->setTabActive('analytics');
$total = 0;
$page = isset($_REQUEST['p']) && is_numeric($_REQUEST['p']) ? intval($_REQUEST['p']) : 1;
$pagelimit = PAGE_LIMIT;
$offset = ($page-1)*$pagelimit;
$params = [];

if (!isset($_REQUEST['group']) || !in_array($_REQUEST['group'], ['day', 'week', 'month']))
    $_REQUEST['group'] = 'day';

$params['group'] = trim($_REQUEST['group']);

if (isset($_REQUEST['staff_id']) && is_numeric($_REQUEST['staff_id'])) {
    $params['staff_id'] = intval(trim($_REQUEST['staff_id']));
}

if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from'])) {
    $params['from'] = trim($_REQUEST['from']);
} else {
    $params['from'] = $_REQUEST['from'] = date('Y-m-d', time()-31*3600*24);
}

if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to'])) {
    $params['to'] = trim($_REQUEST['to']);
} else {
    $params['to'] = $_REQUEST['to'] = date('Y-m-d', time());
}

$year_from = explode('-', $_REQUEST['from']);
$year_from = $year_from[0];
$year_to = explode('-', $_REQUEST['to']);
$year_to = $year_to[0];

$two_year = false;
if ($year_from != $year_to) {
    $two_year = true;
}

switch ($_REQUEST['group']) {
    case 'week':
        define('DATE_FORMAT', 'W-Y');
        break;
    case 'month':
        define('DATE_FORMAT', 'm-Y');
        break;
    case 'day':
    default:
        if ($two_year)
            define('DATE_FORMAT', 'Y-M-d D');
        else
            define('DATE_FORMAT', 'M-d D');
        break;
}

switch ($_REQUEST['group']) {
    case 'week':
    case 'month':
        function ___date_compare($a, $b) {
            $a = explode('-', $a);
            $b = explode('-', $b);

            if ($a[1] > $b[1]) return 1;
            if ($a[1] < $b[1]) return -1;
            return $a[0] - $b[0];
        }
        break;
    case 'day':
    default:
        function ___date_compare($a, $b) {
            $_a = date_create_from_format(DATE_FORMAT, $a)->getTimestamp();
            $_b = date_create_from_format(DATE_FORMAT, $b)->getTimestamp();

            return $_a - $_b;
        }
        break;
}

$total_by_staff = 0;
if (isset($_REQUEST['staff_id']) && $_REQUEST['staff_id']) {
    $all_res = PaymentAnalytics::getPagination($params, $offset, $total, $pagelimit);
    $total_by_staff = PaymentAnalytics::getTotalByStaff($params);
}
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$booking_type_items = TourDestination::getList($group_des);
$all_staff = Staff::getAll();
$all_staff_list = [];
while ($all_staff && ($row = db_fetch_array($all_staff))) {
    $all_staff_list[ $row['staff_id'] ] = $row['username'];
}

$incomeSummaryTable = PaymentAnalytics::getSummaryTable($params);
$incomeChart = PaymentAnalytics::getChart($params);

// ------------------------------------------------------ //
// ------------------------------------------------------ //

$source_by_day_chart = [];
$dates_source = [];

while($incomeChart && ($row = db_fetch_array($incomeChart))) {
    if (!isset($all_staff_list[ $row['staff_id'] ])) continue;

    $staff_username = $all_staff_list[ $row['staff_id'] ];
    if (!isset($source_by_day_chart[ $staff_username ]))
        $source_by_day_chart[ $staff_username ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $dates_source[] = $row['week'].'-'.$row['year'];
            $source_by_day_chart[ $staff_username ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $dates_source[] = $row['month'].'-'.$row['year'];
            $source_by_day_chart[ $staff_username ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $source_by_day_chart[ $staff_username ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}


$dates_source = array_filter(array_unique($dates_source));

foreach ($source_by_day_chart as $_staff => $_date_data) {
    foreach ($dates_source as $_date) {
        if (!isset($source_by_day_chart[ $_staff ][ $_date ]))
            $source_by_day_chart[ $_staff ][ $_date ] = 0;
    }

    uksort($source_by_day_chart[ $_staff ], "___date_compare");
}

// ------------------------------------------------------ //
// ------------------------------------------------------ //

$pageNav = new Pagenate($total,$page,$pagelimit);
$pageNav->setURL('sales-income.php', $params);

$showing = $total ? ' &mdash; '.$pageNav->showing():"";

$list_staff = Staff::getAll(1);
$ACTION = LogAction::ACTIONS;
ksort($ACTION);

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.'sales-income.inc.php');
require_once(STAFFINC_DIR.'footer.inc.php');
?>

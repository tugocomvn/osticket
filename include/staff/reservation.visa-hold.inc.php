<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<form action="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php" method="post" id="hold__visa" target="hold_frame__visa">
    <?php csrf_token() ?>
    <input type="hidden" name="action" value="hold">
    <table>
        <caption><span class="label label-default">Visa</span> Reservation ONLY</caption>
        <tr>
            <td><strong>Quantity *</strong></td>
            <td>
                Hold <input type="number" class="input-field" max="40" min="0" step="1" name="hold_qty" value="0">
                - Sure <input type="number" class="input-field" max="40" min="0" step="1" name="sure_qty" value="0">
            </td>

            <td><strong>Staff *</strong></td>
            <td>
                <select name="staff_id" id="">
                    <option value="0">- TUGO -</option>
                    <?php
                    if($users) {
                        foreach($users as $id => $name) {
                            $s = " ";
                            if ($id == $thisstaff->getId())
                                $s="selected";
                            echo sprintf('<option value="%s" %s>%s</option>', $id, $s, $name);
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td><strong>Customer Name *</strong></td>
            <td>
                <input type="text" class="input-field" name="customer_name">
            </td>
            <td><strong>Phone number *</strong></td>
            <td>
                <input type="text" class="input-field" name="phone_number">
            </td>
        </tr>

        <tr>
            <td><strong>Due date *</strong></td>
            <td>
                <input type="text" class="input-field" readonly
                       name="due" value="<?php echo date('d/m/Y', time()+24*3600*2) ?>">
            </td>
            <td><strong>Country *</strong></td>
            <td>
                <?php
                if ($country_list) {
                    ?>
                    <select name="country" id="country" class="input-field">
                        <option value>- Select -</option>
                        <?php foreach($country_list as $_item): ?>
                            <option value="<?php echo $_item['id'] ?>"><?php echo $_item['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td>Other</td>
            <td>
                <label class="label label-default" for=""><input name="visa_only" value="1" checked onclick="return false;" type="checkbox"> Visa Only</label>
            </td>
            <td>Infant</td>
            <td>
                <input type="number" step="1" min="0" max="10" name="infant" class="input-field" value="0">
            </td>
        </tr>
        <tr>
            <td>Note</td>
            <td>
                <textarea name="note" id="" cols="30" rows="10" class="input-field"></textarea>
            </td>
            <td><strong>Booking Code *</strong></td>
            <td>
                <input type="text" name="booking_code" value="" class="input-field">
            </td>

        </tr>
        <tr>
            <td></td>
            <td><em>Các mục có dấu * là bắt buộc</em></td>
            <td colspan="2">
                <button class="btn_sm btn-primary submit_btn" name="action" value="hold" type="submit">Book</button>

            </td>
        </tr>
    </table>
</form>
<iframe class="hold_frame" src="" frameborder="0" name="hold_frame__visa">

</iframe>
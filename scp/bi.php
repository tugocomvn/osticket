<?php
require('staff.inc.php');

$nav->setTabActive('bi');
$ost->addExtraHeader('<meta name="tip-namespace" content="bi.dashboard" />',
                     "$('#content').data('tipNamespace', 'bi.dashboard');");
$ost->setPageTitle('Business Intelligence');

$page='bi.inc.php';

require(STAFFINC_DIR.'header.bi.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.bi.inc.php');
?>

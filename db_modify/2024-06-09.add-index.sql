ALTER TABLE `default`.`ost_list_items`
ADD INDEX `extra` (`extra` ASC) VISIBLE;
;
ALTER TABLE `default`.`ost_form_entry_values`
ADD INDEX `value_id` (`value_id` ASC) VISIBLE;
;

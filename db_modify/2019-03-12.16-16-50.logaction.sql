create table log_action
(
  id int auto_increment,
  staff_id int null,
  created_at datetime null,
  action_id int null,
  ticket_id int null,
  thread_id int null,
  user_id int null,
  constraint log_action_pk
    primary key (id)
);

create index log_action_action_id_index
  on log_action (action_id);

create index log_action_created_at_index
  on log_action (created_at);

create index log_action_staff_id_index
  on log_action (staff_id);

create index log_action_thread_id_index
  on log_action (thread_id);

create index log_action_ticket_id_index
  on log_action (ticket_id);

create index log_action_user_id_index
  on log_action (user_id);


<?php
/*********************************************************************
    ajax.php

    Ajax utils interface.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
# Override staffLoginPage() defined in staff.inc.php to return an
# HTTP/Forbidden status rather than the actual login page.
# XXX: This should be moved to the AjaxController class
function staffLoginPage($msg='Unauthorized') {
    Http::response(403,'Must login: '.Format::htmlchars($msg));
    exit;
}

define('AJAX_REQUEST', 1);
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.auto_filter.php');

//Clean house...don't let the world see your crap.
ini_set('display_errors','0'); //Disable error display
ini_set('display_startup_errors','0');

Signal::connect('ticket.changeStatus',
    function($data) {
        $ticket_filter = new TicketAutoFilter('Any', $data->ht);
        $ticket_filter->apply($data);
    },
    'Ticket');

//TODO: disable direct access via the browser? i,e All request must have REFER?
if(!defined('INCLUDE_DIR'))	Http::response(500, 'Server configuration error');

require_once INCLUDE_DIR.'/class.dispatcher.php';
require_once INCLUDE_DIR.'/class.ajax.php';
$dispatcher = patterns('',
    url('^/kb/', patterns('ajax.kbase.php:KbaseAjaxAPI',
        # Send ticket-id as a query arg => canned-response/33?ticket=83
        url_get('^canned-response/(?P<id>\d+).(?P<format>json|txt)', 'cannedResp'),
        url_get('^faq/(?P<id>\d+)', 'faq')
    )),
    url('^/content/', patterns('ajax.content.php:ContentAjaxAPI',
        url_get('^log/(?P<id>\d+)', 'log'),
        url_get('^ticket_variables', 'ticket_variables'),
        url_get('^signature/(?P<type>\w+)(?:/(?P<id>\d+))?$', 'getSignature'),
        url_get('^(?P<id>\d+)/(?:(?P<lang>\w+)/)?manage$', 'manageContent'),
        url_get('^(?P<id>[\w-]+)/(?:(?P<lang>\w+)/)?manage$', 'manageNamedContent'),
        url_post('^(?P<id>\d+)(?:/(?P<lang>\w+))?$', 'updateContent')
    )),
    url('^/config/', patterns('ajax.config.php:ConfigAjaxAPI',
        url_get('^scp', 'scp'),
        url_get('^links', 'templateLinks')
    )),
    url('^/form/', patterns('ajax.forms.php:DynamicFormsAjaxAPI',
        url_get('^help-topic/(?P<id>\d+)$', 'getFormsForHelpTopic'),
        url_get('^field-config/(?P<id>\d+)$', 'getFieldConfiguration'),
        url_post('^field-config/(?P<id>\d+)$', 'saveFieldConfiguration'),
        url_delete('^answer/(?P<entry>\d+)/(?P<field>\d+)$', 'deleteAnswer'),
        url_post('^upload/(\d+)?$', 'upload'),
        url_post('^upload/(\w+)?$', 'attach')
    )),
    url('^/list/', patterns('ajax.forms.php:DynamicFormsAjaxAPI',
        url_get('^(?P<list>\w+)/item/(?P<id>\d+)/properties$', 'getListItemProperties'),
        url_post('^(?P<list>\w+)/item/(?P<id>\d+)/properties$', 'saveListItemProperties')
    )),
    url('^/report/overview/', patterns('ajax.reports.php:OverviewReportAjaxAPI',
        # Send
        url_get('^graph$', 'getPlotData'),
        url_get('^chart$','getChartData'),
        url_get('^table/groups$', 'enumTabularGroups'),
        url_get('^table/export$', 'downloadTabularData'),
        url_get('^table$', 'getTabularData')
    )),
    url('^/users', patterns('ajax.users.php:UsersAjaxAPI',
        url_get('^$', 'search'),
        url_get('^/local$', 'search', array('local')),
        url_get('^/remote$', 'search', array('remote')),
        url_get('^/(?P<id>\d+)$', 'getUser'),
        url_post('^/(?P<id>\d+)$', 'updateUser'),
        url_get('^/(?P<id>\d+)/preview$', 'preview'),
        url_get('^/(?P<id>\d+)/edit$', 'editUser'),
        url('^/lookup$', 'getUser'),
        url_get('^/lookup/form$', 'lookup'),
        url_post('^/lookup/form$', 'addUser'),

        url_get('^/add$', 'addUser'),
        url('^/import$', 'importUsers'),
        url_get('^/select$', 'selectUser'),
        url_get('^/select/(?P<id>\d+)$', 'selectUser'),
        url_get('^/select/auth:(?P<bk>\w+):(?P<id>.+)$', 'addRemoteUser'),
        url_get('^/(?P<id>\d+)/register$', 'register'),
        url_post('^/(?P<id>\d+)/register$', 'register'),
        url_get('^/(?P<id>\d+)/delete$', 'delete'),
        url_post('^/(?P<id>\d+)/delete$', 'delete'),
        url_get('^/(?P<id>\d+)/manage(?:/(?P<target>\w+))?$', 'manage'),
        url_post('^/(?P<id>\d+)/manage(?:/(?P<target>\w+))?$', 'manage'),
        url_get('^/(?P<id>\d+)/org(?:/(?P<orgid>\d+))?$', 'updateOrg'),
        url_post('^/(?P<id>\d+)/org$', 'updateOrg'),
        url_get('^/staff$', 'searchStaff'),
        url_post('^/(?P<id>\d+)/note$', 'createNote'),
        url_post('^/(?P<id>\d+)/point$', 'updatePoint'),
        url_post('^/(?P<id>\d+)/updatePointCustomer$', 'updatePointCustomer'),
        url_get('^/(?P<id>\d+)/forms/manage$', 'manageForms'),
        url_post('^/(?P<id>\d+)/forms/manage$', 'updateForms'),
        url_get('^/(?P<id>\d+)/rank$', 'getRank'),
        url_get('^/(?P<id>\d+)/getInfo$', 'getUserFromNumber')
    )),

    url('^/pax', patterns('ajax.pax.php:PaxAjaxAPI',
        url_get('^$', 'search'),
        url_post('^/passport$', 'savePassportPhoto'),
        url_post('^/passport_re_upload$', 'reUploadPassportPhoto'),
        url_get('^/booking$', 'getBookingQuantity'),
        url_post('^/passportPhoto$', 'savePassportPhotoHasPassport')
    )),
    url('^/orgs', patterns('ajax.orgs.php:OrgsAjaxAPI',
        url_get('^$', 'search'),
        url_get('^/search$', 'search'),
        url_get('^/(?P<id>\d+)$', 'getOrg'),
        url_post('^/(?P<id>\d+)$', 'updateOrg'),
        url_post('^/(?P<id>\d+)/profile$', 'updateOrg', array(true)),
        url_get('^/(?P<id>\d+)/edit$', 'editOrg'),
        url_get('^/lookup/form$', 'lookup'),
        url_post('^/lookup$', 'lookup'),
        url_get('^/add$', 'addOrg'),
        url_post('^/add$', 'addOrg'),
        url_get('^/select$', 'selectOrg'),
        url_get('^/select/(?P<id>\d+)$', 'selectOrg'),
        url_get('^/(?P<id>\d+)/add-user(?:/(?P<userid>\d+))?$', 'addUser'),
        url_get('^/(?P<id>\d+)/add-user(?:/auth:(?P<userid>.+))?$', 'addUser', array(true)),
        url_post('^/(?P<id>\d+)/add-user$', 'addUser'),
        url('^/(?P<id>\d+)/import-users$', 'importUsers'),
        url_get('^/(?P<id>\d+)/delete$', 'delete'),
        url_delete('^/(?P<id>\d+)/delete$', 'delete'),
        url_post('^/(?P<id>\d+)/note$', 'createNote'),
        url_get('^/(?P<id>\d+)/forms/manage$', 'manageForms'),
        url_post('^/(?P<id>\d+)/forms/manage$', 'updateForms')
    )),
    url('^/tickets/', patterns('ajax.tickets.php:TicketsAjaxAPI',
        url_get('^(?P<tid>\d+)/change-user$', 'changeUserForm'),
        url_post('^(?P<tid>\d+)/change-user$', 'changeUser'),
        url_get('^(?P<tid>\d+)/contract$', 'getContractForm'),
        url_post('^(?P<tid>\d+)/tags$', 'updateTags'),
        url_get('^tags(?:/(?P<keyword>(.)+))?$', 'getTags'),
        url_get('^(?P<tid>\d+)/user$', 'viewUser'),
        url_post('^(?P<tid>\d+)/user$', 'updateUser'),
        url_get('^(?P<tid>\d+)/preview', 'previewTicket'),
        url_post('^(?P<tid>\d+)/lock$', 'acquireLock'),
        url_post('^(?P<tid>\d+)/lock/(?P<id>\d+)/renew', 'renewLock'),
        url_post('^(?P<tid>\d+)/lock/(?P<id>\d+)/release', 'releaseLock'),
        url_get('^(?P<tid>\d+)/collaborators/preview$', 'previewCollaborators'),
        url_get('^(?P<tid>\d+)/collaborators$', 'showCollaborators'),
        url_post('^(?P<tid>\d+)/collaborators$', 'updateCollaborators'),
        url_get('^(?P<tid>\d+)/add-collaborator/(?P<uid>\d+)$', 'addCollaborator'),
        url_get('^(?P<tid>\d+)/add-collaborator/auth:(?P<bk>\w+):(?P<id>.+)$', 'addRemoteCollaborator'),
        url('^(?P<tid>\d+)/add-collaborator$', 'addCollaborator'),
        url_get('^(?P<tid>\d+)/forms/manage$', 'manageForms'),
        url_get('^(?P<tid>\d+)/merge', 'merge'),
        url_post('^(?P<tid>\d+)/merge', 'mergeTicket'),
        url_post('^(\d+)\/(.)+/save-calendar', 'saveCalendar'),
        url_post('^(\d+)\/(.)+/delete-calendar', 'deleteCalendar'),
        url_post('^(?P<tid>\d+)/forms/manage$', 'updateForms'),
        url_get('^(?P<tid>\d+)/canned-resp/(?P<cid>\w+).(?P<format>json|txt)', 'cannedResponse'),
        url_get('^(?P<tid>\d+)/status/(?P<status>\w+)(?:/(?P<sid>\d+))?$', 'changeTicketStatus'),
        url_post('^(?P<tid>\d+)/status$', 'setTicketStatus'),
        url_get('^status/(?P<status>\w+)(?:/(?P<sid>\d+))?$', 'changeSelectedTicketsStatus'),
        url_post('^status/(?P<state>\w+)$', 'setSelectedTicketsStatus'),
        url_get('^lookup', 'lookup'),
        url_get('^search', 'search')
    )),
    url('^/bookings/', patterns('ajax.tickets.php:BookingsAjaxAPI',
        url_get('^(?P<booking_id>\d+)/note$', 'getBookingNoteForm'),
        url_get('^(?P<booking_id>\d+)/finish$', 'getBookingFinishedForm'),
        url_post('^(?P<booking_id>\d+)/note$', 'saveBookingNote'),
        url_post('^(?P<booking_id>\d+)/finish$', 'finishBooking'),
        url_post('^addToPayment', 'paymentAddBookingCode')
    )),   
    url('^/receipt/', patterns('ajax.tickets.php:BookingsAjaxAPI',
        url_get('^booking', 'getBookingInfo'),
        url_post('^receipt', 'checkAvailableCode'),
        url_post('^confirm', 'confirmImage'),
        url_post('^load', 'get_phone_number')
    )),
    url('^/collaborators/', patterns('ajax.tickets.php:TicketsAjaxAPI',
        url_get('^(?P<cid>\d+)/view$', 'viewCollaborator'),
        url_post('^(?P<cid>\d+)$', 'updateCollaborator')
    )),
    url('^/draft/', patterns('ajax.draft.php:DraftAjaxAPI',
        url_post('^(?P<id>\d+)$', 'updateDraft'),
        url_delete('^(?P<id>\d+)$', 'deleteDraft'),
        url_post('^(?P<id>\d+)/attach$', 'uploadInlineImage'),
        url_get('^(?P<namespace>[\w.]+)$', 'getDraft'),
        url_post('^(?P<namespace>[\w.]+)$', 'createDraft'),
        url_get('^images/browse$', 'getFileList')
    )),
    url('^/note/', patterns('ajax.note.php:NoteAjaxAPI',
        url_get('^(?P<id>\d+)$', 'getNote'),
        url_post('^(?P<id>\d+)$', 'updateNote'),
        url_delete('^(?P<id>\d+)$', 'deleteNote'),
        url_post('^attach/(?P<ext_id>\w\d+)$', 'createNote')
    )),
    url('^/sequence/', patterns('ajax.sequence.php:SequenceAjaxAPI',
        url_get('^(?P<id>\d+)$', 'current'),
        url_get('^manage$', 'manage'),
        url_post('^manage$', 'manage')
    )),
    url('^/tour/', patterns('ajax.tour.php:TourAjaxAPI',
        url_post('^load', 'loadDataFormat'),
        url_post('^save', 'saveTourFlightTicket'),
        url_post('^edit', 'editTour'),
        url_post('^request_edit_tour', 'requestEditTour'),
        url_post('^approval_edit_tour', 'approvalEditTour')
    )),
    url('^/reservation/', patterns('ajax.reservation.php:ReservationAjaxAPI',
        url_post('^save', 'save_reservation'),
        url_post('^load', 'format_values')
    )),
    url('^/flight-ticket/', patterns('ajax.flight-ticket.php:FlightTicketAjaxAPI',
        url_post('^load', 'format_values'),
        url_post('^save', 'save_flight_ticket')
    )),
    url_post('^/upgrader', array('ajax.upgrader.php:UpgraderAjaxAPI', 'upgrade')),
    url('^/help/', patterns('ajax.tips.php:HelpTipAjaxAPI',
        url_get('^tips/(?P<namespace>[\w_.]+)$', 'getTipsJson'),
        url_get('^(?P<lang>[\w_]+)?/tips/(?P<namespace>[\w_.]+)$', 'getTipsJsonForLang')
    )),
    url('^/i18n/(?P<lang>[\w_]+)/', patterns('ajax.i18n.php:i18nAjaxAPI',
        url_get('(?P<tag>\w+)$', 'getLanguageFile')
    ))
);

Signal::send('ajax.scp', $dispatcher);

# Call the respective function
print $dispatcher->resolve($ost->get_path_info());

try {
    @db_close();
} catch(Exception $ex) {}
?>

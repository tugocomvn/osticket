<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include STAFFINC_DIR.'brand.aside.inc.php' ?>

    <!-- Sidebar -->
    <div class="sidebar">
        <?php include STAFFINC_DIR.'staff.aside.inc.php' ?>
        <?php include STAFFINC_DIR.'menu.aside.inc.php' ?>
    </div>
    <!-- /.sidebar -->
</aside>
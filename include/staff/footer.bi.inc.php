</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php // include STAFFINC_DIR.'bi.sidebar.inc.php' ?>

<?php include ROOT_DIR.'version.php' ?>
<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Business Intelligence
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y') ?>
        <a href="http://support.tugo.com.vn">Tugo System</a>.
    </strong>
    Version <?php echo TUGO_OST_VERSION ?>
</footer>
</div>
<!-- ./wrapper -->

<script>
    (function($) {
        $('#datetime_main').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            "locale": {
                "format": "DD MMM YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "showCustomRangeLabel": true,
            "alwaysShowCalendars": true,
            "parentEl": "body",
            "opens": "left"
        }, function(start, end, label) {
            //do something, like clearing an input
            main_start = $('#datetime_main').data('daterangepicker').startDate.clone();
            xstart = $('#datetime_main').data('daterangepicker').startDate.clone();
            main_end = $('#datetime_main').data('daterangepicker').endDate.clone();

            $('#datetime_compare').data('daterangepicker')
                .setStartDate(main_start.subtract((main_end - xstart)/1000, 'seconds').format('DD MMM YYYY'));
            $('#datetime_compare').data('daterangepicker')
                .setEndDate(xstart.subtract(1, 'days').format('DD MMM YYYY'));
        });

        $('#datetime_main').on('apply.daterangepicker', function(ev, picker) {

        });


        $('#datetime_compare').daterangepicker({
            ranges: {
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            "locale": {
                "format": "DD MMM YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "showCustomRangeLabel": true,
            "alwaysShowCalendars": true,
            "parentEl": "body",
            "opens": "left"
        }, function(start, end, label) {
        });

        $('#compare_to').on('change', function() {
            if ($('#compare_to').prop('checked'))
                $('#datetime_compare').show();
            else
                $('#datetime_compare').hide();
        })

        $('#example').DataTable( {
            columnDefs: [ {
                targets: [ 0 ],
                orderData: [ 0, 1 ]
            }, {
                targets: [ 1 ],
                orderData: [ 1, 0 ]
            }, {
                targets: [ 4 ],
                orderData: [ 4, 0 ]
            } ]
        } );
    })(jQuery);
</script>
</body>
</html>
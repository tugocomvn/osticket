/*branch: visa */

create table tugo_osticket_visa
(
    id                int unsigned auto_increment
        primary key,
    year              varchar(4)  null comment 'YEAR',
    month_of_year     varchar(6)  null comment 'YEAR_MONTH',
    month_of_the_year varchar(2)  null comment 'MONTH',
    week_of_year      varchar(2)  null comment 'YEAR_WEEK',
    week_of_the_year  varchar(6)  null comment 'WEEK',
    date              varchar(8)  null comment 'YYYYMMDD',
    day_of_month      varchar(2)  null comment 'DD',
    day_of_week       varchar(1)  null comment 'D DAY_OF_WEEK',
    day_of_week_name  varchar(10) null comment 'DAY_OF_WEEK_NAME',
    hour_of_day       varchar(10) null comment 'YYYYMMDDhh',
    hour              varchar(2)  null comment 'hh',
    minute_of_day     varchar(12) null comment 'date_hour_minute',
    minute            varchar(2)  null comment 'mm',
    second_of_day     varchar(14) null comment 'YYYYMMDDhhmmss',
    second            varchar(2)  null comment 'ss',
    staff_id          int unsigned,
    staff_username    varchar(45) null,
    action_id         int,
    amount_profile       int unsigned default 0 comment 'amount_profile of staff ',

    constraint index2
        unique (day_of_week_name, year, month_of_year, month_of_the_year, minute, week_of_year, week_of_the_year, hour,
                day_of_week, day_of_month, hour_of_day, date, minute_of_day, staff_id, second_of_day, second, action_id)
);

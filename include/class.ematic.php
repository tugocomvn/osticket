<?php
require_once(INCLUDE_DIR.'class.curl.php');

class Ematic {
    public static function mailchimpMembers($email, $fields, &$error) {
        $url = "https://".EMATIC_MC_DATA_CENTER.".api.mailchimp.com/3.0/lists/".EMATIC_MC_LIST_ID."/members/";

        $curl = new _curl($url, true, 60);
        $curl->useAuth(1);
        $curl->setName('tugo');
        $curl->setPass(EMATIC_MC_API_KEY);

        $curl->setPost(json_encode(
            [
                'email_address' => $email,
                'status' => "subscribed",
                'merge_fields' => [
                    "FULLNAME" => $fields['fullname'],
                    "ADDRESS" => $fields['address'],
                ],
                'interests' => [ "50286b5f1f" => true ]
            ]
        ));

        $curl->createCurl();
        $string = json_decode($curl->__tostring());

        if ($curl->getHttpStatus() != 201 && $curl->getHttpStatus() != 200) {
            $error[] = isset($string->error) ? $string->error : ( isset($string->title) ? $string->title : 'Error');
            return false;
        }
    }
}

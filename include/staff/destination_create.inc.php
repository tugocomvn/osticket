<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

?>

<form action="<?php echo $cfg->getUrl().'scp/destination_manage_item.php' ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $error ?></div>
    <?php endif;?>

    <input type="hidden" name="action" value="create">
    <input type="hidden" name="market" value="<?php echo $market_id ?>">

    <h2 class="centered"><?php echo $title_create.': '.$name_market ?></h2>

    <table style="margin-left: 25%; margin-top: 10px">
        <tr class="required">
            <td width="150">Tên<span class="error">*&nbsp;</span></td>
            <td >
                <input class="input-field" type="text" size="28" name="name" required
                       value="<?php if($_REQUEST['name']) echo $_REQUEST['name'] ?>">
                <br><em style="color:gray;display:inline-block">Tên điểm điến là duy nhất</em>
            </td>
        </tr>
        <tr>
            <td>Land rate</td>
            <td>
                <input class="input-field" type="number" size="28" name="land_rate" data-name="land_rate"
                       value="<?php if($_REQUEST['land_rate']) echo $_REQUEST['land_rate'] ?>">
                <span class="amount_text_helper" id="land_rate">đ</span>
            </td>
        </tr>
        <tr>
            <td>Land date</td>
            <td >
                <input class="input-field" type="number" size="20" name="land_date"
                       value="<?php if($_REQUEST['land_date']) echo $_REQUEST['land_date'] ?>"> <i>(days)<i>
            </td>
        </tr>
        <tr>
            <td>Flight rate</td>
            <td >
                <input class="input-field" type="number" size="28" name="flight_rate" data-name="flight_rate"
                       value="<?php if($_REQUEST['flight_rate']) echo $_REQUEST['flight_rate'] ?>">
                <span class="amount_text_helper" id="flight_rate">đ</span>
            </td>
        </tr>
        <tr>
            <td>Flight date</td>
            <td >
                <input class="input-field" type="number" size="20" name="flight_date"
                       value="<?php if($_REQUEST['flight_date']) echo $_REQUEST['flight_date'] ?>"> <i>(days)<i>
            </td>
        </tr>
        <tr>
            <td>Trạng thái</td>
            <td>
                <select name="status">
                    <option value="1" selected>Show</option>
                    <option value="0" >Hide</option>
                </select>
            </td>
        </tr>
    </table>
    <p class="centered">
        <a class="btn_sm btn-default" href="<?php echo $cfg->getUrl().'scp/destination_manage_item.php?market='.(int)$_REQUEST['market'] ?>">Cancel</a>
        <button class="btn_sm btn-primary" name="create" value="create">Save</button>
    </p>
</form>

<script>

    $(document).ready(function() {
        money = $('[data-name^="land_rate"]').val().trim();
        money = parseInt(money);
        money = money.formatMoney(0, '.', ',');
        $('#land_rate').text(money + ' đ');

        money = $('[data-name^="flight_rate"]').val().trim();
        money = parseInt(money);
        money = money.formatMoney(0, '.', ',');
        $('#flight_rate').text(money + ' đ');
    });

    $('[data-name="land_rate"]')
        .off('change, keyup')
        .on('change, keyup', function () {
            money = $('[data-name^="land_rate"]').val().trim();
            money = parseInt(money);
            money = money.formatMoney(0, '.', ',');
            $('#land_rate').text(money + ' đ');
        });

    $('[data-name="flight_rate"]')
        .off('change, keyup')
        .on('change, keyup', function () {
            money = $('[data-name^="flight_rate"]').val().trim();
            money = parseInt(money);
            money = money.formatMoney(0, '.', ',');
            $('#flight_rate').text(money + ' đ');
        });
</script>
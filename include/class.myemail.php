<?php
require_once __DIR__.'/../vendor/autoload.php';
class MyEmail {
    public static function send($to, $to_name, $subject, $content, $from = false, $from_name = false, $files = null) {
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = SYSTEM_EMAIL_SERVER;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = SYSTEM_EMAIL_USERNAME;                 // SMTP username
        $mail->Password = SYSTEM_EMAIL_PASSWORD;                           // SMTP password
        $mail->SMTPSecure = SYSTEM_EMAIL_SMTPSecure;                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SYSTEM_EMAIL_SMTPPort;                                    // TCP port to connect to
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        if (isset($files) && count ($files)) {
            foreach ($files as $file)
                $mail->addAttachment($file['path'], $file['name']);         // Add attachments
        }

        $mail->setFrom($from ?: SYSTEM_EMAIL_USERNAME, $from_name ?: SYSTEM_EMAIL_NAME);
        if (is_array($to) && is_array($to_name)) {
            foreach ($to as $key => $value) {
                $mail->addAddress($value, $to_name[$key]);     // Add a recipient
            }
        } elseif (is_string($to) && is_string($to_name)) {
            $mail->addAddress($to, $to_name);     // Add a recipient
        } else {
            echo "Invalid email address\r\n";
            return false;
        }

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $content;
        $mail->AltBody = substr(strip_tags($content), 32);

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
}

<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

?>
<div class="pull-left" style="width:700;padding-top:5px;">
    <h2><?php echo __('App Notification'); ?></h2>
</div>
<div class="pull-right flush-right" style="padding-top:5px;padding-right:5px;">
    <b><a href="app_notification.php?a=add" class="Icon list-add"><?php
            echo __('Send Notification'); ?></a></b></div>
<div class="clear"></div>

<?php
$qs = array();

$select = 'SELECT * ';

$from = ' FROM '.NOTIFICATION_TABLE.'';

$where=' WHERE 1 ';

if (isset($_REQUEST['query'])) {
    $_REQUEST['query'] = trim($_REQUEST['query']);
}
if (isset($_REQUEST['startDate'])) {
    $_REQUEST['startDate'] = trim($_REQUEST['startDate']);
}
if (isset($_REQUEST['endDate'])) {
    $_REQUEST['endDate'] = trim($_REQUEST['endDate']);
}
if($_REQUEST['query']) {
    $search = db_input(($_REQUEST['query']), false);
        $where .= ' AND (
                    phone_number LIKE \'%' .$search. '%\'
                    OR user_uuid LIKE \'%' .$search. '%\'
                    OR content LIKE \'%' .$search. '%\'
                    OR cloud_message_token LIKE \'%' .$search. '%\'
                    OR title LIKE \'%' .$search. '%\'
                )';
    $qs += array('query' => $_REQUEST['query']);
}
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):null;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):null;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $where .= " AND DATE(`sent_at`) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $where .=" AND DATE(`sent_at`) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
$sortOptions=array();
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'sent_at';
//Sorting options...
$order_column = $order = null;
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column ?: '`sent_at`';

if(isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order ?: 'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by=" $order_column $order ";

$qstr = '&amp;' . Http::build_query($qs);
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$total = db_count('SELECT count(id)'.$from.' '.$where);
$offset = ($page-1) * PAGE_LIMIT;
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('app_notification.php',array('query' => $_REQUEST['query'],'startDate' => $_REQUEST['startDate'],'endDate' => $_REQUEST['endDate']));
$showing=$pageNav->showing().' '._N('notification', 'notifications', $total);
$query="$select $from $where ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
?>
<?php
$res = db_query($query);
if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '."All Notification";
else:
    $showing=__('No Notification found!');
endif;?>
<div class="pull-left">
    <form action="app_notification.php" method="get">
        <?php csrf_token(); ?>
        <table>
            <tr>
                <td><span>Date offer <strong>Between</strong></span></td>
                <td style="display: block"><input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF></td>
                <td style="display: block"><input class="dp input-field" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF></td>
                <td><span>All</strong></span></td>
                <td><input type="text" id="basic-user-search" name="query" size=30 value="<?php echo Format::htmlchars($_REQUEST['query']); ?>"
                           autocomplete="off" autocorrect="off" autocapitalize="off"></td>
                <td><input type="submit" name="basic_search" class="button" value="<?php echo __('Search'); ?>"></td>
                <!-- <td>&nbsp;&nbsp;<a href="" id="advanced-user-search">[advanced]</a></td> -->
            </tr>
        </table>
    </form>
</div>
<form action="app_notification.php" method="POST" name="lists">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="mass_process" >
    <input type="hidden" id="action" name="a" value="" >
    <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
        <caption>
                <?php
                    echo "".$showing. "";
                ?>
            </caption>
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th><?php echo __('Title'); ?></th>
            <th><?php echo __('Content') ?></th>
            <th><?php echo __('Users') ?></th>
            <th><?php echo __('Sound On') ?></th>
            <th><?php echo __('Sent At'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($res):?>
            <?php $i = 1;?>
            <?php while(($row = db_fetch_array($res))): ?>
                <tr>
                    <td><?php echo ($offset + $i++) ?></td>
                    <td><?php echo $row['title'] ?></td>
                    <td title="<?php echo $row['content'] ?>"><?php echo substr($row['content'], 0, 100).(strlen($row['content']) > 100?' [...]':'') ?></td>
                    <td>
                    <?php
                        if (isset($row['user_uuid']) && $row['user_uuid'])
                            echo '<span class="label label-default">User UUID</span> '.$row['user_uuid'];
                        elseif (isset($row['phone_number']) && $row['phone_number'])
                            echo '<span class="label label-success">Phone Number</span> '.$row['phone_number'];
                        elseif (isset($row['cloud_message_token']) && $row['cloud_message_token'])
                            echo '<span class="label label-primary">Cloud Message Token</span> '.substr($row['cloud_message_token'], 0, 30);
                        else
                            echo "_All Users_";
                    ?>
                    </td>
                    <td><?php if (isset($row['sound_on']) && $row['sound_on'] == 1) echo '√' ?></td>
                    <td><?php if ($row['sent_at']) echo date('d/m/Y H:i:s', strtotime($row['sent_at'])) ?></td>
                </tr>
            <?php endwhile; ?>
        <?php endif; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6">
                <?php
                ?>
                <?php if($res && $num){ ?>

                <?php }else{
                    echo __('No Notification found');
                } ?>
            </td>
        </tr>
        </tfoot>
    </table>
    <?php
    if ($res && $num) //Show options..
        echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
    ?>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm'); ?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo sprintf(
                    __('Are you sure you want to DELETE %s?'),
                    _N('selected custom list', 'selected custom lists', 2)); ?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered.'); ?>
    </p>
    <div><?php echo __('Please confirm to continue.'); ?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" value="No, Cancel" class="btn_sm btn-default close">
        </span>
        <span class="buttons pull-right">
            <input type="button" value="Yes, Do it!" class="btn_sm btn-danger confirm">
        </span>
    </p>
    <div class="clear"></div>
</div>

<h2>Sales And CS Activities</h2>

<form action="<?php echo $cfg->getUrl() ?>scp/statistics.php" method="get">
<table>
    <tr>
        <td>Staff</td>
        <td>
            <select name="staff_id" class="input-field" id="">
                <option value=>-- All --</option>
                <?php while($list_staff && ($_staff=db_fetch_array($list_staff))): ?>
                <?php $all_staff[ $_staff['staff_id'] ] = $_staff['username'] ?>
                    <option value="<?php echo $_staff['staff_id'] ?>"
                            <?php if (isset($_REQUEST['staff_id']) && $_REQUEST['staff_id'] == $_staff['staff_id']) echo 'selected' ?>
                    ><?php echo $_staff['username'] ?></option>
                <?php endwhile; ?>
            </select>
        </td>
        <td>Activity</td>
        <td>
            <select name="action_id" class="input-field" id="">
                <option value=>-- All --</option>
                <?php foreach($ACTION as $_name => $_id): ?>
                    <option value="<?php echo $_id ?>"
                            <?php if (isset($_REQUEST['action_id']) && $_REQUEST['action_id'] == $_id) echo 'selected' ?>
                    ><?php echo ucwords(strtolower(implode(' ', explode('_', $_name)))) ?></option>
                <?php endforeach; ?>
            </select>
        </td>

        <td>From</td>
        <td>
            <input type="text" class="dp input-field" name="from"
                   value="<?php echo date_create_from_format('Y-m-d', $_REQUEST['from'])->format('d/m/Y') ?>">
        </td>
        <td>To</td>
        <td>
            <input type="text" class="dp input-field" name="to"
                   value="<?php echo date_create_from_format('Y-m-d', $_REQUEST['to'])->format('d/m/Y') ?>">
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Customer Name</td>
        <td>
            <input type="text" class=" input-field" name="customer_name"
                   value="<?php if (isset($_REQUEST['customer_name'])) echo trim($_REQUEST['customer_name']) ?>">
        </td>
        <td>Customer PhoneNo.</td>
        <td>
            <input type="text" class=" input-field" name="phone_number"
                   value="<?php if (isset($_REQUEST['phone_number'])) echo trim($_REQUEST['phone_number']) ?>">
        </td>
    </tr>

    <tr>
        <td colspan="8">
            <p class="jqx-center-align">
                <button class="btn_sm btn-primary">Search</button>
                <a href="<?php echo $cfg->getUrl() ?>scp/statistics.php" class="reset btn_sm btn-default">Reset</a>
            </p>
        </td>
    </tr>
</table>
</form>

<p><?php echo $showing ?></p>
<table class="list" width="1050">
    <caption>All Activities</caption>
    <thead>
    <tr>
        <th>No.</th>
        <th>Staff</th>
        <th>Ticket Number</th>
        <th>Customer Name</th>
        <th>Customer PhoneNo.</th>
        <th>Activity</th>
        <th>Note</th>
        <th>Time</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = ($page-1)*$pagelimit; foreach ($all_res as $row): ?>
    <tr>
        <td><?php echo ++$i; ?></td>
        <td><a href="<?php echo $cfg->getUrl() ?>scp/statistics.php?staff_id=<?php
            echo $row['staff_id'] ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php
            echo $_REQUEST['to'] ?>"><?php echo $all_staff[ $row['staff_id']] ?></a></td>
        <td><a href="<?php echo $cfg->getUrl() ?>scp/tickets.php?id=<?php echo $row['ticket_id'] ?>"
               target="_blank" class="no-pjax"
            ><?php echo $ticket_numbers[$row['ticket_id']] ?></a></td>
        <td><?php echo $users[ $row['user_id'] ]['name'] ?></td>
        <td><a href="<?php echo $cfg->getUrl() ?>scp/statistics.php?phone_number=<?php
            echo $users[ $row['user_id'] ]['phone_number'] ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php
            echo $_REQUEST['to'] ?>"><?php echo $users[ $row['user_id'] ]['phone_number'] ?: '' ?></a></td>
        <td><?php echo LogAction::getName($row['action_id']) ?></td>
        <td><?php if(isset($threads[ $row['thread_id'] ])) echo $threads[ $row['thread_id'] ] ?></td>
        <td><?php echo Format::db_datetime($row['created_at']) ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
if ($total>0) { //if we actually had any tickets returned.
    echo '<div>'.__('Page').':'.$pageNav->getPageLinks();
} ?>

<table>
    <tr>

        <td>
            <table>
                <caption>Staff Summary</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Staff</th>
                    <th>Number of Ticket</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; while($StaffNumberTicketSummaryTable && ($row = db_fetch_array($StaffNumberTicketSummaryTable))): ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td>
                        <a href="<?php echo $cfg->getUrl() ?>scp/statistics.php?staff_id=<?php
                        echo $row['staff_id'] ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php
                        echo $_REQUEST['to'] ?>"><?php echo $all_staff[ $row['staff_id']] ?></a>
                    </td>
                    <td><?php echo $row['total'] ?></td>
                </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </td>

        <td>
            <div class="staff_summary">
                <canvas id="staff_summary" width="650" height="500"></canvas>
                <button class="btn_sm btn-xs btn-default" onclick="hide_all('staff_summary_chart')">Hide All</button>
                <script>
                    var staff_summary = document.getElementById("staff_summary").getContext('2d');
                    <?php $i = 0; $dates = []; ?>
                    var colors = poolColors(<?php echo count($StaffNumberTicketChart_data) ?>);
                    var staff_summary_chart = new Chart(staff_summary, {
                        type: 'bar',
                        data: {
                            datasets: [
                                <?php foreach($StaffNumberTicketChart_data as $_type => $_date): ?>
                                {
                                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                                    backgroundColor:  colors[<?php echo $i++ ?>],
                                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                                    fill: true
                                    <?php $dates = array_merge($dates, array_keys($_date)) ?>
                                },
                                <?php endforeach; ?>
                            ],
                            labels: $.parseJSON('<?php echo json_encode(array_unique($dates)) ?>'),
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Staff Summary By Date'
                            },
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            },
                            responsive: false,
                            maintainAspectRatio: false,
                            showLines: false, // disable for all datasets
                        }
                    });
                </script>
            </div>
        </td>
    </tr>
</table>

<?php if(isset($_REQUEST['staff_id']) && $_REQUEST['staff_id']): ?>
<table>
    <tr>
        <td>
            <table>
                <caption>Staff Activity Summary</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Staff</th>
                    <th>Activity</th>
                    <th>Number of Ticket</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; while($StaffActivitySummaryTable && ($row = db_fetch_array($StaffActivitySummaryTable))): ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td>
                            <a href="<?php echo $cfg->getUrl() ?>scp/statistics.php?staff_id=<?php
                            echo $row['staff_id'] ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php
                            echo $_REQUEST['to'] ?>"><?php echo $all_staff[ $row['staff_id']] ?></a>
                        </td>
                        <td><?php echo LogAction::getName($row['action_id']) ?></td>
                        <td><?php echo $row['total'] ?></td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </td>

        <td>
             <div class="staff_activity_summary">
                <canvas id="staff_activity_summary" width="650" height="500"></canvas>
                <button class="btn_sm btn-xs btn-default" onclick="hide_all('staff_activity_summary_chart')">Hide All</button>
                <script>
                    var staff_activity_summary = document.getElementById("staff_activity_summary").getContext('2d');
                    <?php $i = 0; $dates = []; ?>
                    var colors = poolColors(<?php echo count($StaffActivityChart_data) ?>);
                    var staff_activity_summary_chart = new Chart(staff_activity_summary, {
                        type: 'line',
                        data: {
                            datasets: [
                                <?php foreach($StaffActivityChart_data as $_type => $_date): ?>
                                {
                                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                                    borderColor:  colors[<?php echo $i++ ?>],
                                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                                    fill: false
                                },
                                <?php endforeach; ?>
                            ],
                            labels: $.parseJSON('<?php echo json_encode($StaffActivityChart_date) ?>'),
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Staff Activity By Date'
                            },
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                }],
                                yAxes: [{
                                    stacked: false
                                }]
                            },
                            responsive: false,
                            maintainAspectRatio: false,
                            showLines: true, // disable for all datasets
                        }
                    });
                </script>
            </div>
        </td>
    </tr>
</table>
<?php endif; ?>
<table>
    <tr>

        <td>
            <table>
                <caption>Activity Summary</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Activity</th>
                    <th>Number of Ticket</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; while($ActivitySummaryTable && ($row = db_fetch_array($ActivitySummaryTable))): ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo LogAction::getName($row['action_id']) ?></td>
                        <td><?php echo $row['total'] ?></td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </td>

        <td>
            <div class="activity_summary">
                <canvas id="activity_summary" width="650" height="500"></canvas>
                <button class="btn_sm btn-xs btn-default" onclick="hide_all('activity_summary_chart')">Hide All</button>
                <script>
                    var activity_summary = document.getElementById("activity_summary").getContext('2d');
                    <?php $i = 0; $dates = []; ?>
                    var colors = poolColors(<?php echo count($ActivityChart_data) ?>);
                    var activity_summary_chart = new Chart(activity_summary, {
                        type: 'line',
                        data: {
                            datasets: [
                                <?php foreach($ActivityChart_data as $_type => $_date): ?>
                                {
                                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                                    borderColor:  colors[<?php echo $i++ ?>],
                                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                                    fill: false
                                },
                                <?php endforeach; ?>
                            ],
                            labels: $.parseJSON('<?php echo json_encode($ActivityChart_date) ?>'),
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Activity Summary By Date'
                            },
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                }],
                                yAxes: [{
                                    stacked: false
                                }]
                            },
                            responsive: false,
                            maintainAspectRatio: false,
                            showLines: true, // disable for all datasets
                        }
                    });
                </script>
            </div>
        </td>
    </tr>
</table>

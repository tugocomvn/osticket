<?php
require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'class.ticket.php');
require_once(INCLUDE_DIR . 'class.general-object.php');
require_once(INCLUDE_DIR . 'class.staff.php');
include_once INCLUDE_DIR.'../vendor/autoload.php';

use Endroid\QrCode\QrCode;

class ReceiptModel extends VerySimpleModel {
    static $meta = array(
        'table' => RECEIPT_TABLE,
        'pk' => ['id'],
    );
}
class Receipt extends ReceiptModel{
    const MONEY_RECEIVED = 1; // staff send sms atfer receiving money
    const SEND_CODE = 2; // send sms receipt & code
    const WAITING_VERIFYING = 3; // confirm code
    const UPLOAD_IMAGE = 4; //upload image
    const APPROVED = 5; // accountant approve
    const REJECTED = 6;   // accountant reject
    const CONFIRM_IMAGE = 7; // confirm image
    const PAYMENT_CREATED_FROM_RECEIPT = 8; // create payment form receipt

    public static function getPagination(array $params, $offset, &$total, $limit = PAGE_LIMIT)
    {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY r.created_at DESC";
        $where = "  ";
        $total = 0;

        if (isset($params['receipt_code']) && !empty($params['receipt_code']))
            $where .= " AND receipt_code LIKE ".db_input($params['receipt_code']);

        if(isset($params['booking_code_trim']) && !empty($params['booking_code_trim']))
            $where .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) LIKE '.db_input(strval($params['booking_code_trim']));

        if(isset($params['customer_name']) && !empty($params['customer_name']))
            $where .= " AND customer_name LIKE " . db_input('%' . ($params['customer_name']) . '%') . " ";

        if(isset($params['customer_phone_number']) && !empty($params['customer_phone_number']))
            $where .= " AND customer_phone_number = ".db_input($params['customer_phone_number']);

        if(isset($params['tour_name']) && !empty($params['tour_name']))
            $where .= " AND tour_name LIKE " . db_input('%' . ($params['tour_name']) . '%') . " ";

        $from  =($params['from'] && (strlen($params['from'])>=8))?date_create_from_format('Y-m-d', $params['from'])->format('Y-m-d'):null;
        $to    =($params['to'] && (strlen($params['to'])>=8))?date_create_from_format('Y-m-d', $params['to'])->format('Y-m-d'):null;
        if($from)
            $where .= " AND DATE(`created_at`) >= date(".(db_input($from)).")";
        if($to)
            $where .= " AND DATE(`created_at`) <= date(".(db_input($to)).")";

        if(isset($params['staff_cashier']) && !empty($params['staff_cashier']))
            $where .= " AND staff_cashier_id = ".db_input($params['staff_cashier']);

        if(isset($params['staff_cashier_phone_number']) && !empty($params['staff_cashier_phone_number']))
            $where .= " AND staff_cashier_phone_number = ".db_input($params['staff_cashier_phone_number']);

        if(isset($params['payment_method']) && !empty($params['payment_method']))
            $where .= " AND payment_method = ".db_input($params['payment_method']);

        if(isset($params['receipt_status']) && !empty($params['receipt_status'])) 
            $where .= " AND status = ".db_input($params['receipt_status']);

        if(isset($params['staff_business']) && !empty($params['staff_business'])) 
            $where .= " AND staff_id = ".db_input($params['staff_business']);

        $sql = " SELECT * FROM " .static::$meta['table']." r WHERE 1 $where $order";
 
        $sql_count = "SELECT COUNT(*) as total FROM ($sql) as A";
        $res_total = db_query($sql_count);

        if ($res_total) {
            $total_row = db_fetch_array($res_total);
            if ($total_row && isset($total_row['total'])) {
                $total = $total_row['total'];
            }
        }
        $res = db_query("$sql $limit");
        return $res;
    }

    public static function Rand_md5($length) {
        $max = ceil($length / 32);
        $random = '';
        for ($i = 0; $i < $max; $i ++) {
          $random .= md5(microtime(true).mt_Rand(10000,90000));
        }
        return substr($random, 0, $length);
    }

    public static function generateMixedCode($length) {
        $base = 'abcdefghijklmnopqrstuvwxyz';
        $baseD = '0123456789';
        $r = array();

        for ($i = 0; $i < $length; $i += 2) {
            $r[] = substr($base, Rand(0, strlen($base) - 1), 1);
        }
        for ($i = 0; $i < $length; $i += 2) {
            $r[] = substr($baseD, Rand(0, strlen($baseD) - 1), 1);
        }
        shuffle($r);

        return implode('', $r);
    }

    public static function __save_receipt_sms_content($receipt)
    {
        global $thisstaff;
        if(empty($receipt)) return false;
        $receipt_sms = ReceiptConfirmationCode::lookup(['receipt_id' => $receipt->id]);
        if(!$receipt_sms){
            $__data_new = [
                'receipt_id'    => $receipt->id,
                'code'          => self::Rand_md5(RECEIPT_CODE_LENGHT),
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => $thisstaff->getId(),
                'status'        => self::SEND_CODE,
                'expired_at'    => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) + (RECEIPT_CONFIRM_CODE_CUSTOMER_EXPIRE_AFTER)),
                'phone_number'  => $receipt->customer_phone_number
            ];
            $receipt_sms = ReceiptConfirmationCode::create($__data_new);
            $receipt_sms->save(true);
        }else {
            $__data_update = [
                'code'          => self::Rand_md5(RECEIPT_CODE_LENGHT),
                'created_at'    => date('Y-m-d H:i:s'),
                'created_by'    => $thisstaff->getId(),
                'expired_at'    => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) + (RECEIPT_CONFIRM_CODE_CUSTOMER_EXPIRE_AFTER)),
           ];
           $receipt_sms->setAll($__data_update);
           $receipt_sms->save(true);
        }
        return $receipt_sms;
    }

    public static function SendConfirmationCodeToCustomer($vars, &$errors, $thisstaff)
    {
        if(!empty($vars) && !isset($vars)) return null;

        try {
            $receipt = Receipt::lookup($vars['receipt_id']);
            if(!$receipt){
                throw new Exception('Require receipt.');
            }

            $receipt_sms = self::__save_receipt_sms_content($receipt);
            if(!$receipt_sms){
                throw new Exception('Require receipt sms.');
            }

            if($vars['customer_phone_number'] !== $receipt->customer_phone_number){
                throw new Exception('Customer phone number not available.');
            }
            $content  = SMS_SEND_CODE_RECEIPT.$receipt_sms->code;
            $content .= SMS_SEND_LINK_RECEIPT.WEBSITE_URL_SUPPORT;
            $content .= URL_RECEIPT.$receipt->hash_url;
            $content .= EXPIRED_CODE_RECEIPT;
            $error = [];
            $send = \_SMS::send($receipt->customer_phone_number, $content, $error, 'Receipt Code');
            if(!$send){
                throw new Exception('Sending failed.');
            }
            $receipt->set('status', Receipt::SEND_CODE);
            $log = self::log($receipt,$thisstaff);
            $receipt->set('log_id', $log);
            $receipt->save();
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
            return false;
        }
        return true;
    }

    public static function SendFinanceApprovalSMS($vars, &$errors, $thisstaff)
    {
        if(!empty($vars) && !isset($vars)) return null;

        try {
            $receipt = Receipt::lookup($vars['receipt_id']);

            if(!$receipt){
                throw new Exception('Require receipt.');
            }

            $content = SMS_SEND_APPROVAL_RECEIPT.$receipt->receipt_code." số tiền ".number_format($receipt->amount_received).CURRENCY_UNIT;
            $error = [];
            
            $send = \_SMS::send($receipt->customer_phone_number, $content, $error, 'Receipt Approved');
            if(!$send){
                throw new Exception('Sending failed.');
            }
            $__data =[
                'status'                     => Receipt::APPROVED,
                'finance_staff_id_confirmed' => $thisstaff->getId(),
                'finance_staff_confirmed_at' => date('Y-m-d H:i:s')
            ];
            $receipt->setAll($__data);
            $log = self::log($receipt, $thisstaff);
            $receipt->set('log_id', $log);
            $receipt->save();
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
            return false;
        }
        return true;
    }

    public static function SendFinanceRejectedSendSMS($vars, &$errors, $thisstaff)
    {
        if(!empty($vars) && !isset($vars)) return null;

        try {
            $receipt = Receipt::lookup($vars['receipt_id']);
            if(!$receipt){
                throw new Exception('Require receipt.');
            }
            $content = SMS_SEND_REJECTED_RECEIPT.$receipt->receipt_code." số tiền ".number_format($receipt->amount_received).CURRENCY_UNIT;
            $error = [];
            $send = \_SMS::send($receipt->customer_phone_number, $content, $error, 'Receipt Rejected');
            if(!$send){
                throw new Exception('Sending failed.');
            }
            $__data =[
                'status'                     => Receipt::REJECTED,
                'finance_staff_id_confirmed' => $thisstaff->getId(),
                'finance_staff_confirmed_at' => date('Y-m-d H:i:s')
            ];
            $receipt->setAll($__data);
            $log = self::log($receipt, $thisstaff);
            $receipt->set('log_id', $log);
            $receipt->save();
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
            return false;
        }
        return true;
    }

    public static function SendAfterReceivingMoneySMS($vars, &$errors, $thisstaff)
    {
        if(!empty($vars) && !isset($vars)) return null;
        
        try {
            $receipt = Receipt::lookup($vars['receipt_id']);
            if(!$receipt){
                throw new Exception('Require receipt.');
            }
            
            $name_staff = Staff::getNameStaff($thisstaff->getId());
            if($name_staff) {
                $phone_number = Staff::lookup($thisstaff->getId())->getMobilePhone();
            }
            $content = "Nhan vien Tugo: ".$name_staff;
            $content .= ", SDT: *****".substr($phone_number, 5);
            $content .= " đã thu số tiền ".$vars['amount_received']."VND của phiếu thu ".$vars['receipt_code'];
            $content .= " vào lúc ".date('H:i');
            $content .= " ngày ".date('d-m-Y');
            $error = [];
            $send = \_SMS::send($receipt->customer_phone_number, $content, $error, 'Receipt Money');
            if(!$send){
                throw new Exception('Sending failed.');
            }
            $__data =[
                'status'                     => Receipt::MONEY_RECEIVED,
                'sales_cashier_id_confirmed' => $thisstaff->getId(),
                'sales_cashier_confirmed_at' => date('Y-m-d H:i:s')
            ];
            $receipt->setAll($__data);
            $log = self::log($receipt, $thisstaff);
            $receipt->set('log_id', $log);
            $receipt->save();
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
            return false;
        }
        return true;
    }

    public static function log($receipt, $thisstaff) {
        $data = [
            'receipt_id' => $receipt->id,
            'created_by'  => $thisstaff->getId(),
            'content'    => json_encode($receipt->ht),
            'created_at'  => date('Y-m-d H:i:s'),
        ];

        try {
            $log = ReceiptLog::create($data);
            if($log) $log->save();
            return $log->id;
        } catch (Exception $ex) {  }
        return 0 ;
    }

    public static function qrCodeReceipt($vars)
    {
        $vars = (array) $vars;
        if (!$vars) return null;
        $id = (int)$vars['receipt_id'];
        $results_log = ReceiptLog::lookup(['receipt_id' => $id]);
        //log content
        $qrcode_string_ = $results_log->content;
        $qrcode_string = md5($qrcode_string_);
        $file_name = md5($qrcode_string).'.png';
        $receipt = Receipt::lookup($id);
        $receipt->set('qr_code', $file_name);
        $receipt->save();
        $path = '/public_file/qrcode_receipt/';
        if (!file_exists(__DIR__.'/..'.$path))
        @mkdir(__DIR__.'/..'.$path, 0777);
        if (!mkdir($concurrentDirectory = __DIR__ . '/..' . $path, 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        $file_path = __DIR__.'/..'.$path.$file_name;
        $qrCode = new QrCode();
        $qrCode->setText($qrcode_string)
            ->setSize(150)
            ->setPadding(20)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setExtension('png')
            ->save($file_path);
        return true;
    }

    public static function send_new_code($id, &$errors) {
        if(empty($id)) return false;

        try {
            $receipt = Receipt::lookup($id);

            if(!$receipt){
                throw new Exception('Require receipt.');
            }

            $receipt_sms = self::__save_receipt_sms_content($receipt);
            if(!$receipt_sms){
                throw new Exception('Require receipt sms.');
            }
            $content = 'DL Tugo gui Mã code phiếu thu của bạn là '.$receipt_sms->code;
            $content .= EXPIRED_CODE_RECEIPT;
            $error = [];
            $send = \_SMS::send($receipt->customer_phone_number, $content, $error);
            if(!$send){
                throw new Exception('Sending failed.');
            }
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
            return false;
        }
        return true;
    }

    public static function generateReceiptCode(){
        global $cfg;
        $topicId = 1; // general inquiry
        $topic = Topic::lookup($topicId);
        $number = $topic ? $topic->getNewReceiptNumber() : $cfg->getNewReceiptNumber();
        return $number;
    }

    static function isReceiptNumberUnique($receipt_code) {
        return 0 == db_num_rows(db_query(
                'SELECT ticket_id FROM '.self::$meta['table'].' WHERE `receipt_code`='.db_input($receipt_code)));
    }

    public static function getWeekdays()
    {
        $weekday = date("l");
        $weekday = strtolower($weekday);
        switch($weekday) {
            case 'monday':
                $weekday = 'Thứ hai';
                break;
            case 'tuesday':
                $weekday = 'Thứ ba';
                break;
            case 'wednesday':
                $weekday = 'Thứ tư';
                break;
            case 'thursday':
                $weekday = 'Thứ năm';
                break;
            case 'friday':
                $weekday = 'Thứ sáu';
                break;
            case 'saturday':
                $weekday = 'Thứ bảy';
                break;
            default:
                $weekday = 'Chủ nhật';
                break;
        }
        return $weekday.', '.' Ngày '.date('d').' Tháng '.date('m').' Năm '.date('Y');
    }

    public static function sum_amount_received($params) {
        $where = "  ";
        if (isset($params['receipt_code']) && !empty($params['receipt_code']))
            $where .= " AND receipt_code LIKE ".db_input($params['receipt_code']);

        if(isset($params['booking_code_trim']) && !empty($params['booking_code_trim']))
            $where .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) LIKE '.db_input(strval($params['booking_code_trim']));

        if(isset($params['customer_name']) && !empty($params['customer_name']))
            $where .= " AND customer_name LIKE " . db_input('%' . ($params['customer_name']) . '%') . " ";

        if(isset($params['customer_phone_number']) && !empty($params['customer_phone_number']))
            $where .= " AND customer_phone_number = ".db_input($params['customer_phone_number']);

        if(isset($params['tour_name']) && !empty($params['tour_name']))
            $where .= " AND tour_name LIKE " . db_input('%' . ($params['tour_name']) . '%') . " ";

        $from  =($params['from'] && (strlen($params['from'])>=8))?date_create_from_format('Y-m-d', $params['from'])->format('Y-m-d'):null;
        $to    =($params['to'] && (strlen($params['to'])>=8))?date_create_from_format('Y-m-d', $params['to'])->format('Y-m-d'):null;
        if($from)
            $where .= " AND DATE(`created_at`) >= date(".(db_input($from)).")";
        if($to)
            $where .= " AND DATE(`created_at`) <= date(".(db_input($to)).")";

        if(isset($params['staff_cashier']) && !empty($params['staff_cashier']))
            $where .= " AND staff_cashier_id = ".db_input($params['staff_cashier']);

        if(isset($params['staff_cashier_phone_number']) && !empty($params['staff_cashier_phone_number']))
            $where .= " AND staff_cashier_phone_number = ".db_input($params['staff_cashier_phone_number']);

        if(isset($params['payment_method']) && !empty($params['payment_method']))
            $where .= " AND payment_method = ".db_input($params['payment_method']);

        if(isset($params['receipt_status']) && !empty($params['receipt_status'])) 
            $where .= " AND status = ".db_input($params['receipt_status']);
            
        if(isset($params['staff_business']) && !empty($params['staff_business'])) 
            $where .= " AND staff_id = ".db_input($params['staff_business']);

        $sql = " SELECT SUM(amount_received) as total FROM " .static::$meta['table']." r WHERE 1 $where";
        $res_total = db_query($sql);
        $row = db_fetch_array($res_total);
        return $row['total'];
    }
}

class DueStatus extends GeneralObject {
    const DUE_DATE = 'due_date'; 
    const DUE_DATE_TEXT = 'due_date_text';
}

class PaymentMethod extends GeneralObject {
    const Tiền_Mặt = 1; 
    const Quẹt_Thẻ_Atm = 2; 
    const Thẻ_Visa_Thường = 3;
    const Quẹt_Thẻ_Trả_Góp = 4;
    const Chuyển_Khoản = 5;
}

class ReceiptStatus extends GeneralObject {
    const MONEY_RECEIVED = 1; // staff send sms atfer receiving money
    const SEND_CODE = 2; // send sms receipt & code
    const WAITING_VERIFYING = 3; // confirm code
    const UPLOAD_IMAGE = 4; //upload image
    const APPROVED = 5; // accountant approve
    const REJECTED = 6;   // accountant reject
    const CONFIRM_IMAGE = 7; // confirm image
}

class ReceiptConfirmationCodeModel extends VerySimpleModel{
    static $meta = array(
        'table' => RECEIPT_CONFIRMATION_CODE_TABLE,
        'pk' => ['id'],
    );
}

class ReceiptConfirmationCode extends ReceiptConfirmationCodeModel{

}
class ReceiptLogModel extends VerySimpleModel{
    static $meta = array(
        'table' => RECEIPT_LOG_TABLE,
        'pk' => ['id'],
    );
}

class ReceiptLog extends ReceiptLogModel{
    public static function getAllLog($id) {
        $sql = "SELECT * FROM " .static::$meta['table']." l  WHERE receipt_id =".db_input($id)." ORDER BY created_at DESC";
        return db_query($sql);
    }
    public static function get_hash_url_log($receipt_id)
    {
        if(!isset($receipt_id)) return null;
        $receipt = Receipt::lookup($receipt_id);
        $sql = "SELECT content FROM ".static::$meta['table']." WHERE receipt_id=".db_input($receipt_id)."";
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        while ($res && ($row = db_fetch_array($res))) {
            $content = json_decode($row['content'], true);
            if($content['hash_url'] != $receipt->hash_url) {
                $_sql = "SELECT content, MIN(created_at) FROM ".static::$meta['table']." WHERE receipt_id=".db_input($receipt_id)."";
                $_res = db_query($_sql);
                $_row = db_fetch_array($_res);
                $_content = json_decode($_row['content'], true);
                $hash_url = $_content['hash_url'];
                $receipt->set('hash_url', $hash_url);
                return $receipt->save();
            }
        }
    }

}

class ReceiptPrintModel extends VerySimpleModel{
    static $meta = array(
        'table' => RECEIPT_PRINT_TABLE,
        'pk' => ['id'],
    );
}

class ReceiptPrint extends  ReceiptPrintModel{

}

<?php
/*********************************************************************
    receiptlist.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
include_once(INCLUDE_DIR.'class.ticket.php');
include_once INCLUDE_DIR.'class.sms.php';
include_once INCLUDE_DIR.'class.staff.php';
include_once INCLUDE_DIR.'class.receipt.php';

$finance_staff_list= Staff::getAllNameFinance();
$payment_method_list = PaymentMethod::caseTitleName();
$receipt_status_list = ReceiptStatus::caseTitleName();
$staff_business_list = Staff::getAllName();
$params = $qs = [];
$total = 0;

if(isset($_REQUEST['receipt_code']) && trim($_REQUEST['receipt_code'])) {
    $params['receipt_code'] = trim($_REQUEST['receipt_code']);
    $qs += ['receipt_code' => trim($_REQUEST['receipt_code'])];
}

if (isset($_REQUEST['booking_code_trim']) && trim($_REQUEST['booking_code_trim'])) {
    $booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $_REQUEST['booking_code_trim']);
    $booking_code_trim = ltrim($booking_code_trim, '0');
    $params['booking_code_trim'] = trim($booking_code_trim);
    $qs += ['booking_code_trim' => trim($_REQUEST['booking_code_trim'])];
}

if (isset($_REQUEST['customer_name']) && trim($_REQUEST['customer_name'])) {
    $params['customer_name'] = trim($_REQUEST['customer_name']);
    $qs += ['customer_name' => trim($_REQUEST['customer_name'])];
}

if(isset($_REQUEST['customer_phone_number']) && trim($_REQUEST['customer_phone_number'])) {
    $params['customer_phone_number'] = trim($_REQUEST['customer_phone_number']);
    $qs += ['customer_phone_number' => trim($_REQUEST['customer_phone_number'])];
}

if(isset($_REQUEST['tour_name']) && trim($_REQUEST['tour_name'])) {
    $params['tour_name'] = trim($_REQUEST['tour_name']);
    $qs += ['tour_name' => trim($_REQUEST['tour_name'])];
}

if (isset($_REQUEST['staff_cashier'])&& (int)($_REQUEST['staff_cashier'])) {
    $params['staff_cashier'] = (int)($_REQUEST['staff_cashier']);
    $qs += ['staff_cashier' => trim($_REQUEST['staff_cashier'])];
}

if (isset($_REQUEST['staff_cashier_phone_number'])&& trim($_REQUEST['staff_cashier_phone_number'])) {
    $params['staff_cashier_phone_number'] = trim($_REQUEST['staff_cashier_phone_number']);
    $qs += ['staff_cashier_phone_number' => trim($_REQUEST['staff_cashier_phone_number'])];
}

if (isset($_REQUEST['from']) && trim($_REQUEST['from'])) {
    $params['from'] = trim($_REQUEST['from']);
    $qs += ['from' => trim($_REQUEST['from'])];
}
if (isset($_REQUEST['to']) && trim($_REQUEST['to'])) {
    $params['to'] = trim($_REQUEST['to']);
    $qs += ['to' => trim($_REQUEST['to'])];
}

if (isset($_REQUEST['payment_method']) && (int)$_REQUEST['payment_method']) {
    $params['payment_method'] = (int)$_REQUEST['payment_method'];
    $qs += ['payment_method' => (int)$_REQUEST['payment_method']];
}
if (isset($_REQUEST['receipt_status']) && (int)$_REQUEST['receipt_status']) {
    $params['receipt_status'] = (int)$_REQUEST['receipt_status'];
    $qs += ['receipt_status' => (int)$_REQUEST['receipt_status']];
}

if (isset($_REQUEST['staff_business']) && (int)$_REQUEST['staff_business']) {
    $params['staff_business'] = (int)$_REQUEST['staff_business'];
    $qs += ['staff_business' => (int)$_REQUEST['staff_business']];
}
$sum_amount_received = 0;
$sum_amount_received = Receipt::sum_amount_received($params);
$page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
$offset = ($page - 1) * PAGE_LIMIT;
$results = Receipt::getPagination($params, $offset, $total, PAGE_LIMIT);
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('receiptlist.php', $qs ?: null);
$page_inc = "receiptlist.inc.php";

$nav->setTabActive('booking', 'booking');
$ost->addExtraHeader('<meta name="tip-namespace" content="booking.booking" />',
    "$('#content').data('tipNamespace', 'booking.booking');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page_inc);
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canManageVisaItems()) die('Access Denied');
?>
<style>
    input,textarea{
        margin-bottom: 5px;
        padding-left: 8px;
        border-radius: 1em;
    }
    input{
        height: 25px;
    }
    span{
        color: darkblue ;
        font-weight: bold;
    }
</style>
<form action="<?php echo $url;?>" method="get">
    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php if(isset($mess)) echo $mess?></div>
    <?php endif;?>
    <a class="label label-info" href="<?php $cfg->getUrl()?>visa_manage_item.php">Trở về</a>
    <input type="hidden" name="id" value="<?php  echo $_REQUEST['id']; ?>">
    <h2><?php echo $title_list;?></h2>
    <div class="pull-right flush-right" style="padding-right:5px;height: 20px"><b><a href="<?php echo $url?>?action=create" class="Icon newstaff">Add new</a></b></div>
    <table class="list" border="0" cellspacing="1" cellpadding="0" width="1050">
        <thead>
        <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Mô tả</th>
            <th>Chỉnh sửa</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1; if(!is_null($results)): ?>
            <?php while($results && ($row = db_fetch_array($results))):?>
            <tr class="row-item">
                <td><?php echo $i++ ?>.</td>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['description'] ?></td>
                <td><a class="no-pjax btn_sm btn-default btn-xs" href="<?php echo $url;?>?action=edit&id=<?php echo $row['id'] ?>">Edit</a></td>
            </tr>
            <?php endwhile;?>
        <?php endif; ?>
        </tbody>
    </table>
</form>

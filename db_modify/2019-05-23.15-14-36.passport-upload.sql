create table pax_passport_photo
(
  id int auto_increment,
  filename varchar(64) null,
  booking_code varchar(16) null,
  upload_at datetime null,
  upload_by int null,
  status tinyint null,
  constraint pax_passport_photo_pk
    primary key (id)
);

create index pax_passport_photo_booking_code_index
  on pax_passport_photo (booking_code);

create unique index pax_passport_photo_filename_uindex
  on pax_passport_photo (filename);

create index pax_passport_photo_status_index
  on pax_passport_photo (status);

create index pax_passport_photo_upload_at_index
  on pax_passport_photo (upload_at);

create index pax_passport_photo_upload_by_index
  on pax_passport_photo (upload_by);

alter table pax_passport_photo
  add result varchar(255) null;

alter table pax_passport_photo
  add upload_filename varchar(255) null;

alter table pax_passport_photo
  add read_at datetime null;

alter table pax_passport_photo
  add tour_id int null after booking_code;

create index pax_passport_photo_tour_id_index
  on pax_passport_photo (tour_id);


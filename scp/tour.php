<?php
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');
require_once(INCLUDE_DIR.'class.tour-market.php');

$leaders_list = TourNew::getAllLeaders();
$transits_airport_list = TourNew::getAllTransitAirport();
$departure_flight_list = TourNew::getAllDepartureFlight();
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);
$market_list = TourMarket::loadList($group_market);
$booking_type_list = TourDestination::loadList($group_des);
$list_colors = ListOfColors::caseTitleName();
$airlines_list = TourNew::getAllAirline();

$page='tour.inc.php';
$nav->setActiveTab('operator', 1);
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

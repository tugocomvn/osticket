<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canCreateBookings()) die('Access Denied');
$type = DynamicListItem::objects()->filter(['list_id' => BOOKING_TYPE_LIST])->order_by('value');
$status = DynamicListItem::objects()->filter(['list_id' => BOOKING_STATUS_LIST])->order_by('value');
$partner = DynamicListItem::objects()->filter(['list_id' => PARTNER_LIST])->order_by('value');
$code = DynamicListItem::objects()->filter(['list_id' => NO_ANSWER_1ST_STATUS])->order_by('value');
$allStaff = Staff::getAllName();
//$result = Booking::get_booking_view(453166);
$result = Booking::lookup((int)$_REQUEST['id']);
//$partner_choose =  json_decode($result['partner'], true);
//$booking_type_choose = json_decode($result['booking_type'], true);
//$status_choose = json_decode($result['status'], true);


?>
<form action="<?php echo $cfg->getUrl(); ?>scp/new_booking.php?id=<?php echo $result->ticket_id ?>&action=edit" method="post" id="save"  enctype="multipart/form-data" class="repeater">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="update">
    <input type="hidden" name="a" value="edit-booking">
    <input type="hidden" name="id" value="<?php echo $result->ticket_id; ?>">
    <h2><?php echo 'Update Ticket #'.$result->number ?></h2>
    <?php if(isset($errors) && $errors ): ?>
        <?php foreach ($mess as $value): ?>
            <div id="msg_warning"><?php echo $value ?></div>
        <?php endforeach; ?>
    <?php endif;?>
    <table class="form_table fixed" width="940" border="0" cellspacing="0" cellpadding="2">
        <thead>
            <tr><td></td><td></td></tr>
            <tr>
                <th colspan="2">
                    <h4>Update Booking</h4>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:150px;"></td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">
                    <em><strong>Booking</strong>:
                    </em>
                </th>
            </tr>
            <tr>
                <td>
                    Mã Booking:
                </td>
                <td>
                    <input type="text" name="booking_code" readonly value="<?php echo $result->booking_code ?>">
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Mã Ticket:
                </td>
                <td>
                    <input type="text" name="ticket_number" required value="<?php echo $result->ticket_number ?>">
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Mã Receipt:
                </td>
                <td>
                    <input type="text" name="receipt_code" value="<?php echo $result->receipt_code ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Tên khách:
                </td>
                <td>
                    <textarea class="input-field" cols="100" rows="4" type="text" required name="customer"><?php echo $result->customer ?></textarea>
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Số điện thoại:
                </td>
                <td>
                    <input class="input-field" cols="12" width="12" type="text" required name="phone_number"
                        pattern="^0[0-9]{9}$" aria-label="Điền một Số điện thoại"
                        value="<?php echo $result->phone_number ?>" />
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <input type="text" name="email" value="<?php echo $result->email ?>">
                </td>
            </tr>

            <tr>
                <td>
                    Loại Booking:
                </td>
                <td>
                    <div style="position:relative">
                        <select name="booking_type" required>
                            <option value="">— Select —</option>
                            <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                                <?php foreach($booking_type_list as  $item): ?>
                                    <option value="<?php echo $item['id'] ?>"
                                        <?php if(isset($result->booking_type_id) && $result->booking_type_id == $item['id']) echo 'selected' ?>
                                    ><?php echo $item['name'] ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <font class="error">*</font>
                    </div>
                </td>
            </tr>
            <tr>
                <td >
                    Ngày khởi hành:
                </td>
                <td>
                    <input class="input-field dp" name="dept_date"  required type="text" value="<?php if($result->dept_date) echo date('d/m/Y', $result->dept_date) ?>">
                </td>

            </tr>
            <tr>
                <td >
                    Ngày full CỌC:
                </td>
                <td>
                    <input class="input-field dp" name="fullcoc_date"  required type="text" value="<?php if($result->fullcoc_date) echo date('d/m/Y', $result->fullcoc_date) ?>">
                </td>

            </tr>
            <tr>
                <td >
                    Ngày full PAY:
                </td>
                <td>
                    <input class="input-field dp" name="fullpay_date"  required type="text" value="<?php if($result->fullpay_date) echo date('d/m/Y', $result->fullpay_date) ?>">
                </td>

            </tr>

            <tr>
                <td>
                    Đối tác:
                </td>
                <td>
                    <?php
                    $partner_id = 0;
                    if(is_numeric($result->partner))
                        $partner_id  = $result->partner;
                    elseif(strlen((_String::json_decode($result->partner))))
                        $partner_id = (int)key(json_decode($result->partner, true));
                    ?>
                    <div style="position:relative">
                        <select name="partner">
                            <option value="">— Select —</option>
                            <?php foreach ($partner as $data):?>
                                <option value="<?php echo $data->id ?>" <?php if($partner_id == $data->id) echo 'selected' ?>><?php echo $data->value ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Nhân viên phụ trách:
                </td>
                <td>
                    <div style="position:relative" required>
                        <select name="staff">
                            <option value="">— Select —</option>
                            <?php while ($allStaff && ($row = db_fetch_array($allStaff))):?>
                                <option value="<?php echo $row['staff_id'] ?>" <?php if($row['staff_id'] == $result->staff_id) echo 'selected' ?>><?php echo $row['name'] ?></option>
                            <?php endwhile;?>
                        </select>
                        <font class="error">*</font>
                    </div>
                </td>
            </tr>

            <tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Người Lớn __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input name="quantity1"  required type="number" data-name="quantity1" value="<?php echo $result->quantity1 ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice1" data-type='currency' data-name="retailprice1" required  value="<?php echo $result->retailprice1 ?>">
                        <font class="error">*</font>
                        <span id="retailprice1">đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice1" data-type='currency' required data-name="netprice1" value="<?php echo $result->netprice1?>">
                        <font class="error">*</font>
                        <span id="netprice1">đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note1" rows="2"><?php echo $result->note1 ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Trẻ Em __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input name="quantity2"  required type="number" data-name="quantity2" value="<?php echo $result->quantity2?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice2" data-type='currency' data-name="retailprice2" required value="<?php echo $result->retailprice2 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice2"  data-type='currency' data-name="netprice2" value="<?php echo $result->netprice2 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note2" rows="2"><?php echo $result->note2 ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Phụ Thu __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input name="quantity3"  required type="number" data-name="quantity3" value="<?php echo $result->quantity3 ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice3" data-type='currency' required data-name="retailprice3" value="<?php echo $result->retailprice3 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice3" data-type='currency' required data-name="netprice3" value="<?php echo $result->netprice3 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note3" rows="2"><?php echo $result->note3 ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Khác __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input name="quantity4"  required type=number data-name="quantity4" value="<?php echo $result->quantity4 ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice4" data-type='currency' data-name="retailprice4" required value="<?php echo $result->retailprice4 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice4" data-type='currency' data-name="netprice4" required value="<?php echo $result->netprice4 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note4" rows="2"><?php echo $result->note4 ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 1__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype1" class="input-field" data-name="discounttype1"
                               value="<?php echo $result->discounttype1 ?>">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount1"  class="input-field" data-type='currency' data-name="discountamount1"
                               value="<?php echo $result->discountamount1?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 2__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype2" class="input-field" data-name="discounttype2"
                               value="<?php echo $result->discounttype2 ?>">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount2" class="input-field" data-type='currency' data-name="discountamount2"
                               value="<?php echo $result->discountamount2 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 3__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype3" class="input-field" data-name="discounttype3"
                               value="<?php echo $result->discounttype3 ?>">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount3" class="input-field" data-type='currency' data-name="discountamount3"
                               value="<?php echo $result->discountamount3 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>Xếp Phòng</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Twin:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="room_twin" value="<?php echo $result->twin_room ?>">
                        <font class="error">*</font>
                        <br><em style="color:gray;display:inline-block">Điền số lượng khách xếp vào loại phòng này (điền
                            "00" nếu không có)</em>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Double:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="room_double" value="<?php echo $result->double_room ?>" >
                        <font class="error">*</font>
                        <br><em style="color:gray;display:inline-block">Điền số lượng khách xếp vào loại phòng này (điền
                            "00" nếu không có)</em>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Triple:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="room_triple" value="<?php echo $result->triple_room ?>">
                        <br><em style="color:gray;display:inline-block">Điền số lượng khách xếp vào loại phòng này (để trống nếu không có)</em>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Single:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="room_single" value="<?php echo $result->single_room ?>">
                        <br><em style="color:gray;display:inline-block">Điền số lượng khách (để trống nếu không có) CÓ PHỤ THU</em>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Khác:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="room_other" placeholder="Ma booking" value="<?php echo $result->other_room ?>">
                        <br><em style="color:gray;display:inline-block">Lẻ nam hay lẻ nữ</em>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>Tải lên tài liệu</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>Cam kết</td>
                <td width="150px" class="form_group">
                    <?php if(isset($fileNameCommitment) && $fileNameCommitment): ?>
                        <a class="no-pjax btn_sm btn-success btn-xs" target="_blank"
                           href="<?php echo $cfg->getUrl().'scp/new_booking.php?id='.$result->ticket_id.'&action=down_commitment'?>">Download</a>
                    <?php else:?>
                        <input type="file" name="commitment_file" class="input-field"/>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <td>Hợp đồng</td>
                <td width="150px" class="form_group">
                    <?php if(isset($fileNameContract) && $fileNameContract): ?>
                    <a class="no-pjax btn_sm btn-success btn-xs" target="_blank"
                       href="<?php echo $cfg->getUrl().'scp/new_booking.php?id='.$result->ticket_id.'&action=down_contract'?>">Download</a>
                    <?php else: ?>
                        <input type="file" name="contract_file" class="input-field"/>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>____</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Trạng thái:
                </td>
                <td>
                    <?php
                    $status_id = 0;
                    if(is_numeric($result->status))
                        $status_id  = $result->status;
                    elseif(strlen((_String::json_decode($result->status))))
                        $status_id = (int)key(json_decode($result->status, true));
                    ?>
                    <select name="status">
                        <option value="">— Select —</option>
                        <?php foreach ($status as $data):?>
                            <option value="<?php echo $data->id ?>" <?php if($status_id == $data->id) echo 'selected'; ?>><?php echo $data->value ?></option>
                        <?php endforeach;?>
                    </select>

                </td>
            </tr>
            <tr>
                <td>
                    Ghi chú:
                </td>
                <td>
                    <textarea style="resize: none" rows="2" cols="50" name="note"><?php echo $result->note ?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Mã giới thiệu:
                </td>
                <td>
                    <select name="referral_code">
                        <option value="">— Select —</option>
                        <?php foreach ($code as $data):?>
                            <option value="<?php echo $data->id ?>" <?php if($result->referral_code == $data->id) echo 'selected'; ?>><?php echo $data->value ?></option>
                        <?php endforeach;?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><strong>Tổng giá bán: </strong></td>
                <td><span class="total_retailprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng giá net: </strong></td>
                <td><span class="total_netprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng lợi nhuận: </strong></td>
                <td><span class="total_profit"></span></td>
            </tr>
        </tbody>
    </table>
    <p style="padding-left:250px;">
        <input type="submit" name="submit" class="btn_sm btn-primary" value="<?php echo __('Save');?>">
        <input type="reset"  name="reset"  class="btn_sm btn-default" value="<?php echo __('Reset');?>">
        <input type="button" name="cancel" class="btn_sm btn-danger" value="<?php echo __('Cancel');?>">
    </p>
</form>
<script>

    $(document).ready(function () {
        $('#'+$(this).attr("name")).text(formatNumber($(this).val()));
        booking_calc_action();

        $('.repeater').repeater({
            show: function () {
                $(this).slideDown('fast');
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp('fast', deleteElement);
                }
            },
            isFirstItemUndeletable: false
        });

        $('#submit_btn').off('click').on('click', function(e) {
            e.preventDefault();

            if (!$('#save')[0].checkValidity()) {
                $('#check_submit').click();
                return false;
            } else {
                $('input[type="button"], input[type="submit"], button').not('#check_submit').prop('disabled', true);
                $('#check_submit').click();
            }
        });


    });

</script>

<script>
    (function($) {
        setTimeout(function() {
            $('[name="topicId"]').change();
        }, 1000)
    })(jQuery);

    $("input[data-type='currency']").on({
        keyup: function() {
            $('#'+$(this).attr("name")).text(formatNumber($(this).val()));
            booking_calc_action();
        },
    });

    function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }


    function formatCurrency(input) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        var original_len = input_val.length;

        // initial caret position
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);

            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>
<script>
$(document).ready(function() {
    $('input[name="phone_number"]').on('input blur change keyup', function() {
        var phone_number = $(this).val();
        if(phone_number.match(/^0[0-9]{9}$/)) {
            $(this).css('border-color', 'green');
            this.setCustomValidity("");
        } else {
            $(this).css('border-color', 'red');
            this.setCustomValidity("Điền 01 số điện thoại đi bạn!");
        }
    });
});
</script>
<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

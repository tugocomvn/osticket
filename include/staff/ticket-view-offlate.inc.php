<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');

//Make sure the staff is allowed to access the page.
if(!@$thisstaff->isStaff()) die('Access Denied');
if(!$thisstaff->canViewOfflates()) die('Access Denied');

require_once INCLUDE_DIR.'class.offlate.php';

//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info=($_POST && $errors)?Format::input($_POST):array();

//Get the goodies.
$dept  = $ticket->getDept();  //Dept
$staff = $ticket->getStaff(); //Assigned or closed by..
$team  = $ticket->getTeam();  //Assigned team.
$offlate = Offlate::lookup($ticket->getId());

//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = sprintf(
            __('Current ticket status (%s) does not allow the end guest to reply.'),
            $ticket->getStatus());
elseif ($ticket->isAssigned()
        && (($staff && $staff->getId()!=$thisstaff->getId())
            || ($team && !$team->hasMember($thisstaff))
        ))
    $warn.= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
            sprintf(__('Người duyệt hiện tại là %s'),
                implode('/', $ticket->getAssignees())
                ));
?>
<?php
// get ticket form
if (!isset($forms) || !$forms) $forms=DynamicFormEntry::forTicket($ticket->getId());

// set ticket as read
if ($forms && $ticket->getStaffId() && $ticket->getStaffId() == $thisstaff->getId()) {
    $break = false;
    $ticket->markAsRead();
    $ticket->reload();
}

?>

<table width="1058" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td width="50%" class="">
             <h2><a href="tickets.php?id=<?php echo $ticket->getId(); ?>"
             title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
             <?php echo sprintf(__('Request for Absence #%s'), $ticket->getNumber()); ?></a></h2>
        </td>
        <td width="auto" class="flush-right">
            <?php
            if (in_array($ticket->getTopicId(), [OL_TOPIC]) && $thisstaff->getId() == $offlate->staff_offer && $offlate->ol_status < 2) { ?>
                <a class="btn_sm btn-default action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-ol"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php } ?>
        </td>
    </tr>
</table>

<hr>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Status');?>:</th>
                    <td>
                        <?php echo Offlate::status($offlate->ol_status) ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Create Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCreateDate()); ?></td>
                </tr>
            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <?php
                if($ticket->isOpen()) { ?>
                    <tr>
                        <th width="100"><?php echo __('Assigned To');?>:</th>
                        <td>
                            <?php
                            if($ticket->isAssigned()) {
                                echo Format::htmlchars(implode('/', $ticket->getAssignees()));
                                if ($staff && $staff->getMobilePhone())
                                    echo ' ('.$staff->getMobilePhone().')';
                            }
                            else
                                echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                            ?>
                        </td>
                    </tr>
                    <?php
                } else { ?>
                    <tr>
                        <th width="100"><?php echo __('Closed By');?>:</th>
                        <td>
                            <?php
                            if(($staff = $ticket->getStaff()))
                                echo Format::htmlchars($staff->getName());
                            else
                                echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                            ?>
                        </td>
                    </tr>
                    <?php
                } ?>

                <tr>
                    <th>Number of day:</th>
                    <td><?php echo Offlate::calc($offlate->time_offer, $offlate->time_end); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<div class="clear"></div>
<h2 style="padding:10px 0 5px 0; font-size:11pt;"><?php echo Format::htmlchars($ticket->getSubject()); ?></h2>
<?php
$tcount = $ticket->getThreadCount();
$tcount+= $ticket->getNumNotes();
?>
<ul id="threads">
    <li><a class="active" id="toggle_ticket_thread" href="#"><?php echo sprintf(__('Ticket Thread (%d)'), $tcount); ?></a></li>
</ul>

<div id="ticket_thread">
    <?php
    $count_reply = 0;
    $threadTypes=array('M'=>'message','R'=>'response', 'N'=>'note', 'S'=>'sms', 'AE'=>'automatic-email', 'AS'=>'automatic-sms', 'Overdue' => 'automatic-sms');
    /* -------- Messages & Responses & Notes (if inline)-------------*/
    $types = array('M', 'R', 'N', 'S', 'AE', 'AS', 'Overdue');
    if(($thread=$ticket->getThreadEntries($types))) {
       foreach($thread as $entry) { ?>
               <?php if('R' == $entry['thread_type']) $count_reply++; ?>
        <table class="thread-entry <?php echo $threadTypes[$entry['thread_type']]; ?>" cellspacing="0" cellpadding="1" width="1058" border="0">
            <tr>
                <th colspan="4" width="100%">
                <div>
                    <span class="pull-left">
                        <span style="display:inline-block"><?php echo _Format::db_short_datetime($entry['created']);?></span>
                        <span class="time-elapsed"><?php if (isset($entry['created']) && $entry['created']) echo (' - '. _String::time_elapsed_string(Misc::db2gmtime($entry['created']))); ?></span>
                        <span style="display:inline-block;padding:0 1em" class="faded title"><?php echo Format::truncate($entry['title'], 100); ?></span>
                    </span>
                    <span class="pull-right" style="white-space:no-wrap;display:inline-block">
                        <span style="vertical-align:middle;" class="textra"></span>
                        <span style="vertical-align:middle;"
                            class="tmeta faded title"><?php
                            $poster = $entry['name'] ?: $entry['poster'];
                            $name = $ticket->getName();
                            if (isset($name->name)) $name = $name->name;
                            if ($name === $poster) {
                                ?><span class="label label-danger"><?php echo Format::htmlchars($poster); ?></span><?php
                            } else
                                echo Format::htmlchars($poster);
                            ?></span>
                    </span>
                </div>
                </th>
            </tr>
            <tr><td colspan="4" class="thread-body" id="thread-id-<?php
                echo $entry['id']; ?>"><div><?php
                echo $entry['body']->toHtml(); ?></div></td></tr>
            <?php
            if($entry['attachments']
                    && ($tentry = $ticket->getThreadEntry($entry['id']))
                    && ($urls = $tentry->getAttachmentUrls())
                    && ($links = $tentry->getAttachmentsLinks())) {?>
            <tr>
                <td class="info" colspan="4"><?php echo $tentry->getAttachmentsLinks(); ?></td>
            </tr> <?php
            }
            if ($urls) { ?>
                <script type="text/javascript">
                    $('#thread-id-<?php echo $entry['id']; ?>')
                        .data('urls', <?php
                            echo JsonDataEncoder::encode($urls); ?>)
                        .data('id', <?php echo $entry['id']; ?>);
                </script>
<?php
            } ?>
        </table>
        <?php
        if($entry['thread_type']=='M')
            $msgId=$entry['id'];
       }
    } else {
        echo '<p>'.__('Error fetching ticket thread - get technical help.').'</p>';
    }?>
</div>
<div class="clear" style="padding-bottom:10px;"></div>
<?php if($errors['err']) { ?>
    <div id="msg_error"><?php echo $errors['err']; ?></div>
<?php }elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php }elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php } else ?>

<?php if ($errors && !$errors['err']) {?>
    <?php foreach($errors as $e) { ?><div class="error-banner"><?php echo $e ?></div><?php } ?>
    <?php
} ?>
<div id="response_options">
    <ul class="tabs">
        <?php
        if(($thisstaff->canEditOfflates() || $thisstaff->canAcceptOfflates()) && $thisstaff->getId() != $offlate->staff_offer) { ?>
        <li><a id="assign_tab" class="active" href="#assign_ol">Approve/Deny</a></li>
        <?php } ?>
        <li><a id="note_absence" href="#note_absence"><?php echo __('Note for Absence request');?></a></li>
    </ul>

    <?php
    if(($thisstaff->canEditOfflates() || $thisstaff->canAcceptOfflates()) && $thisstaff->getId() != $offlate->staff_offer) { ?>
    <form id="assign_ol" class="active" action="tickets.php?id=<?php echo $ticket->getId(); ?>#assign_ol" name="assign_ol" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="assign_ol">
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">

            <?php
            if($errors['assign']) {
                ?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['assign']; ?></td>
            </tr>
            <?php
            } ?>

                <tr>
                    <td width="120" style="vertical-align:top">
                        <label for="assignId"><strong><?php echo __('Người tiếp theo sẽ duyệt');?>:</strong></label>
                    </td>
                    <td>
                        <select id="assignId" name="assignId" required>
                            <option value= >&mdash; <?php echo __('Người tiếp theo sẽ duyệt');?> &mdash;</option>
                            <option value="-2">&mdash; <?php echo __('Tôi là trùm cuối');?> &mdash;</option>
                            <option value="-3">&mdash; <?php echo __('Không duyệt');?> &mdash;</option>
                            <?php
                            $sid=$tid=0;

                            if ($dept->assignMembersOnly())
                                $users = $dept->getAvailableMembers();
                            else
                                $users = Staff::getAvailableStaffMembers();

                            if ($users) {
                                $staffId=$ticket->isAssigned()?$ticket->getStaffId():0;
                                foreach($users as $id => $name) {
                                    if($staffId && $staffId==$id)
                                        continue;

                                    if (!is_object($name))
                                        $name = new PersonsName($name);

                                    $k="s$id";
                                    echo sprintf('<option value="%s" %s>%s</option>',
                                                 $k,(($info['assignId']==$k)?'selected="selected"':''), $name);
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            <?php if($offlate->ol_status == 2): ?>
            <tr>
                <td width="120" style="vertical-align:top">
                <td><span id="msg_info">Đề nghị nghỉ phép đã được duyệt lần cuối</span></td>
            </tr>
            <?php endif; ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Comments');?>:</strong><span class='error'>&nbsp;</span></label>
                </td>
                <td>
                    <textarea name="assign_comments" id="assign_comments"
                        cols="80" rows="7" wrap="soft"
                        placeholder="<?php echo __('Ghi chú'); ?>"
                        class="richtext ifhtml no-bar"><?php echo $info['assign_comments']; ?></textarea>
                    <span class="error"><?php echo $errors['assign_comments']; ?></span><br>
                </td>
            </tr>
        </table>
        <p>
            <?php if ($ticket->isAssigned()
                && (($staff && $staff->getId()!=$thisstaff->getId())
                    || ($team && !$team->hasMember($thisstaff))
                )) {
                ?><div id="msg_warning"><?php echo sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
                                                           sprintf(__('Người duyệt hiện tại là %s'),
                                                                   implode('/', $ticket->getAssignees())
                                                           )); ?></div><?php
            }
            ?>
        </p>
        <p  style="padding-left:165px;">
            <button class="btn_sm btn-primary" value="1" name="approve">Approve</button>
            <button class="btn_sm btn-danger" value="-1" name="approve">Deny</button>
        </p>
    </form>
    <?php
    } ?>

    <form id="note_absence" action="tickets.php?id=<?php echo $ticket->getId(); ?>#note_absence" name="note_absence" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime(); ?>">
        <input type="hidden" name="a" value="postnote">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['postnote']) {?>
                <tr>
                    <td width="120">&nbsp;</td>
                    <td class="error"><?php echo $errors['postnote']; ?></td>
                </tr>
                <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Note'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <div>
                        <div class="faded" style="padding-left:0.15em"><?php
                            echo __('Note title - summary of the note (optional)'); ?></div>
                        <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                        <br/>
                        <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                    </div>
                    <br/>
                    <div class="error"><?php echo $errors['note']; ?></div>
                    <textarea name="note" id="internal_note" cols="80"
                              placeholder="<?php echo __('Note details'); ?>"
                              rows="9" wrap="soft" data-draft-namespace="ticket.note"
                              data-draft-object-id="<?php echo $ticket->getId(); ?>"
                              class="richtext ifhtml draft draft-delete"><?php echo $info['note'];
                        ?></textarea>
                    <div class="attachments">
                        <?php
                        print $note_form->getField('attachments')->render();
                        ?>
                    </div>
                </td>
            </tr>
        </table>

        <p  style="padding-left:165px;">
            <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Post Note');?>">
            <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
</div>

<script type="text/javascript">
(function($) {
    $('.thread-body>div').readmore({
        speed: 20,
        collapsedHeight: 200,
        moreLink: '<a href="#" class="read_more_link">Bấm để đọc tiếp</a>',
        lessLink: '<a href="#" class="read_more_link">Close</a>',
        blockCSS: 'display: block; width: 100%; margin-bottom: 1em !important;'
    });
})(jQuery);
</script>

<?php
namespace Tugo\Data;

class RoomType {
    private static $list = [
        'TW',
        'DBL',
        'TRIP',
        'SGL',
    ];

    public static function getList() {
        return self::$list;
    }
}

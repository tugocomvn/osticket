<?php
if(!defined('OSTSCPINC') || !$thisstaff ) die('Invalid path');
if (!$thisstaff->canViewMemberList()) exit('Access Denied');

require_once INCLUDE_DIR . 'tugo.user.php';
require_once INCLUDE_DIR. 'class.pax.php';

$customer_number =  $_REQUEST['id'];
if (!empty($customer_number)) {
    $user = \Tugo\User::get(['customer_number' => $customer_number]);
}
?>

<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="150"><?php echo __('Name'); ?>:</th>
                    <td><?php echo $user['customer_name'] ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Phone Number'); ?>:</th>
                    <td>
                        <?php echo substr_replace($user['phone_number'],"xxxx",3,4); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Date of birth'); ?>:</th>
                    <td>
                        <?php if(isset($user['dob'])):
                            echo date('d/m/Y',strtotime($user['dob']));
                        endif;?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Customer Number'); ?>:</th>
                    <td>
                        <?php $customer_number = $user['customer_number']; ?>
                        <?php if($customer_number): ?>
                            <span class="label label-primary"><?php echo $customer_number ?></span>
                        <?php else: ?>
                            <span class="label label-primary">-- chưa cài app --</span>
                        <?php endif; ?>
                    </td>
                </tr>


            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th><?php echo __('Point'); ?>:</th>
                    <td>
                        <?php $user_point = $user['user_point']; ?>
                        <?php if($user_point): ?>
                            <span class="label label-primary"><?php echo $user_point ?></span>
                        <?php else: ?>
                            <span>0</span>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th width="150"><?php echo __('Rank'); ?>:</th>
                    <td> <?php echo $user['rank'] ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?>:</th>
                    <td><?php echo date('d/m/Y H:i:s',strtotime($user['register_at'])); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<div class="clear"></div>
<ul class="tabs">
    <li>
        <a id="points_tab" href="#points"><i class="icon-flag-alt"></i>&nbsp;
            <?php echo __('Loyalty'); ?>
        </a>
    </li>
</ul>
<div class="tab_content" id="points" >
    <?php
    $points = UserPoint::forUserUuid($user['uuid']);
    $create_point_url = 'users/'.$user['customer_number'].'/updatePointCustomer';
    include STAFFINC_DIR . 'templates/points.tmpl.php';
    ?>
</div>
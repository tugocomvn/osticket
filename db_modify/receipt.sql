-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2019 at 12:10 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(11) NOT NULL,
  `receipt_code` varchar(64) NOT NULL,
  `booking_code` varchar(64) NOT NULL,
  `booking_type_id` int(11) NOT NULL,
  `departure_day` datetime NOT NULL,
  `price_nl` int(255) NOT NULL,
  `quantity_nl` mediumint(2) NOT NULL,
  `price_te` int(255) NOT NULL,
  `quantity_te` mediumint(2) NOT NULL,
  `surcharge` int(255) NOT NULL,
  `note_surcharge` varchar(255) NOT NULL,
  `total_retail_price` int(255) NOT NULL,
  `staked` int(255) NOT NULL,
  `remaining_charge` int(255) NOT NULL,
  `completed_date` date NOT NULL,
  `customer` varchar(255) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `staff` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `note` varchar(255) NOT NULL,
  `created_receipt` datetime DEFAULT CURRENT_TIMESTAMP,
  `middle_date` date DEFAULT NULL,
  `place_air` varchar(64) DEFAULT NULL,
  `file_date` date DEFAULT NULL,
  `visa_date` date DEFAULT NULL,
  `hash_id` varchar(7) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `code_` varchar(11) DEFAULT NULL,
  `confirm_date` datetime DEFAULT NULL,
  `code_hidden` varchar(11) DEFAULT NULL,
  `tracking` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `receipt_code`, `booking_code`, `booking_type_id`, `departure_day`, `price_nl`, `quantity_nl`, `price_te`, `quantity_te`, `surcharge`, `note_surcharge`, `total_retail_price`, `staked`, `remaining_charge`, `completed_date`, `customer`, `phone_number`, `staff`, `phone`, `note`, `created_receipt`, `middle_date`, `place_air`, `file_date`, `visa_date`, `hash_id`, `ticket_id`, `code_`, `confirm_date`, `code_hidden`, `tracking`) VALUES
(797, 'E19-639', 'TG123-0026805', 0, '2019-09-11 11:20:00', 5020000, 1, 0, 0, 0, '', 5020000, 5020000, 500000, '2019-07-25', 'Cô Vân', '0933819768', 'Âu Nguyễn', '0938974807', '', '2019-07-25 15:09:02', '2019-06-21', 'Tân Sơn Nhất', '2019-07-26', '2019-08-27', '811bd6', 388013, 'af761d', NULL, '57e1a2ec', 0),
(798, 'H17- 348h', 'TG123-0010934', 342, '2019-10-18 11:20:00', 5000000, 1, 2000000, 1, 500000, 'abc', 7500000, 7500000, 0, '2019-07-19', 'NGUYỄN BÁ NHẪN ', '0906644081', 'Hàn Phạm', '0972468328', 'abc', '2019-07-25 15:17:59', '2017-08-31', 'Tân Sơn Nhất', '2019-07-25', '2019-08-28', 'da5809', 105562, '7bc3c3', NULL, 'd28dbbdd', 0),
(804, 'D19-39', 'TG123-0026727', 342, '2019-09-19 11:20:00', 5000000, 1, 0, 0, 0, 'abc', 5000000, 5000000, 0, '2019-08-22', 'ĐÀO THỊ NGỌC MAI', '0936200469', 'Thúy An Phan', '0366900918', '', '2019-07-25 16:52:17', '2019-05-22', 'Tân Sơn Nhất', '2019-07-05', '2019-08-06', '8ac8b6', 386662, '6dddaa', NULL, 'd0c45ce0', 0),
(805, 'F17-8596', 'TG123-0010007', 0, '2019-08-22 11:20:00', 5000000, 1, 2000000, 3, 50000, 'abc', 7000000, 7000000, 0, '2019-07-27', 'Đào Thanh Sơn', '0933196486', 'Trang Nguyen', '0938974807', '', '2019-07-25 16:56:16', '2017-06-29', 'Tân Sơn Nhất', '2019-07-18', '2019-07-27', 'a036a2', 89799, 'b7f502', NULL, '7edc068a', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=806;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

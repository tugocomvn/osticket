<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Access Denied');

$qs = array();

$select = 'SELECT DISTINCT user.*, email.address as email, value.value as phone_number, a.customer_number ';

$from = ' FROM '.USER_TABLE.' user '
    . ' LEFT JOIN '.USER_EMAIL_TABLE.' email ON (user.id = email.user_id) '
    . ' LEFT JOIN '.USER_ACCOUNT_TABLE.' account ON (user.id = account.user_id) '
    . ' LEFT JOIN api_user a ON user.uuid = a.uuid ';

$from .=' LEFT JOIN '.FORM_ENTRY_TABLE.' entry
            ON (entry.object_type=\'U\' AND entry.object_id = user.id)
          LEFT JOIN '.FORM_ANSWER_TABLE.' value
            ON (value.entry_id=entry.id AND value.field_id = '.USER_PHONE_NUMBER_FIELD.') ';

$where = ' WHERE 1 ';

if (isset($_REQUEST['query'])) {
    $_REQUEST['query'] = trim($_REQUEST['query']);
}

if (preg_match('/^[0-9]{9}$/', $_REQUEST['query'])) {
    $where .= ' AND a.customer_number LIKE  '.db_input($_REQUEST['query']). ' ';


} elseif (preg_match('/^[0-9]{10,11}$/', $_REQUEST['query'])) {
    $search = db_input(strtolower($_REQUEST['query']), false);
    $where .= ' AND (
            value.value LIKE \''.$search.'\'
            OR
            value.value LIKE \''._String::convertMobilePhoneNumber($search).'\'
        )';
}  elseif (preg_match('/@/', $_REQUEST['query'])) {
    $search = db_input(strtolower($_REQUEST['query']), false);
    $where .= ' AND (
            email.address LIKE \''.$search.'\'
        )';
} elseif ($_REQUEST['query']) {
    $search = db_input(strtolower($_REQUEST['query']), false);
    $where .= ' AND (
            user.name LIKE \'%'.$search.'%\'
        )';
}

if ((empty($_REQUEST['gold']) || $_REQUEST['gold'] !== 'gold')
    && (empty($_REQUEST['diamond']) || $_REQUEST['diamond'] !== 'diamond')
    && (empty($_REQUEST['platinum']) || $_REQUEST['platinum'] !== 'platinum')) {
    $_REQUEST['all'] = 'all';
} else {
    $_REQUEST['all'] = null;
}

if (empty($_REQUEST['all']) || $_REQUEST['all'] !== 'all') {
    $rank_id_list = [];
    if (!empty($_REQUEST['gold']) && $_REQUEST['gold'] === 'gold') {
        $_sql = 'SELECT id FROM '.LIST_ITEM_TABLE.' WHERE
            list_id='.db_input(APP_USER_RANK_LIST).' AND extra='.db_input($_REQUEST['gold'], true);
        $rank_id_list[] = db_result(db_query($_sql));
    }

    if (!empty($_REQUEST['diamond']) && $_REQUEST['diamond'] === 'diamond') {
        $_sql = 'SELECT id FROM '.LIST_ITEM_TABLE.' WHERE
            list_id='.db_input(APP_USER_RANK_LIST).' AND extra='.db_input($_REQUEST['diamond'], true);
        $rank_id_list[] = db_result(db_query($_sql));
    }

    if (!empty($_REQUEST['platinum']) && $_REQUEST['platinum'] === 'platinum') {
        $_sql = 'SELECT id FROM '.LIST_ITEM_TABLE.' WHERE
            list_id='.db_input(APP_USER_RANK_LIST).' AND extra='.db_input($_REQUEST['platinum'], true);
        $rank_id_list[] = db_result(db_query($_sql));
    }

    $rank_id_list = array_filter($rank_id_list);
    $from .=' LEFT JOIN '.FORM_ENTRY_TABLE.' rank_entry
            ON (rank_entry.object_type=\'U\' AND rank_entry.object_id = user.id)
           JOIN '.FORM_ANSWER_TABLE.' rank_value
            ON (rank_entry.id = rank_value.entry_id AND rank_value.field_id = '.APP_USER_RANK_FIELD_ID.'
            and '. ((is_array($rank_id_list) && count($rank_id_list))
                ? 'rank_value.value_id IN ('.implode(',', $rank_id_list).') '
                : ' 1=0 ');
    $from .= ' ) ';

    if (!empty($_REQUEST['gold']) && $_REQUEST['gold'] === 'gold') {
        // $where .= ' AND rank_item.extra='.db_input($_REQUEST['gold'], true).' ';
        $qs += ['gold' => 'gold'];
    } else {
        $qs['gold'] = null;
    }

    if (!empty($_REQUEST['diamond']) && $_REQUEST['diamond'] === 'diamond') {
        // $where .= ' AND rank_item.extra='.db_input($_REQUEST['diamond'], true).' ';
        $qs += ['diamond' => 'diamond'];
    } else {
        $qs['diamond'] = null;
    }

    if (!empty($_REQUEST['platinum']) && $_REQUEST['platinum'] === 'platinum') {
        // $where .= ' AND rank_item.extra='.db_input($_REQUEST['platinum'], true).' ';
        $qs += ['platinum' => 'platinum'];
    } else {
        $qs['platinum'] = null;
    }
} else {

}

$qs += array('query' => $_REQUEST['query']);

$sortOptions = array('name' => 'user.name',
                     'email' => 'email.address',
                     'create' => 'user.created',
                     'update' => 'user.updated');
$orderWays = array('DESC'=>'DESC','ASC'=>'ASC');
$sort= ($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])]) ? strtolower($_REQUEST['sort']) : 'name';
//Sorting options...
if ($sort && $sortOptions[$sort])
    $order_column =$sortOptions[$sort];

$order_column = $order_column ?: 'user.name';

if ($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])])
    $order = $orderWays[strtoupper($_REQUEST['order'])];

$order=$order ?: 'ASC';
if ($order_column && strpos($order_column,','))
    $order_column = str_replace(','," $order,",$order_column);

$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$total=db_count('SELECT count(DISTINCT user.id) '.$from.' '.$where);
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total,$page,PAGE_LIMIT);
$qstr = '&amp;'. Http::build_query($qs);
$qs += array('sort' => $_REQUEST['sort'], 'order' => $_REQUEST['order']);
$pageNav->setURL('users.php', $qs);
$qstr.='&amp;order='.($order=='DESC' ? 'ASC' : 'DESC');

$query="$select $from $where  LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

$qhash = md5($query);
$_SESSION['users_qs_'.$qhash] = $query;
?>
<style>
    .ranking.label input {
        vertical-align: middle;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background: grey;
        width: 1em;
        height: 1em;
        border-radius: 2px;
        margin: 0;
    }

    .ranking.label input:checked {
        background: blue;
    }

    .ranking.label input[type="checkbox"]:checked::after {
        content: "✔";
        /* position: absolute; */
        /* left: 50%;
        top: 50%;
        transform: translate(-50%, -50%); */
        color: white;
        font-size: 16px;
    }

    .ranking.label {
        vertical-align: middle;
        display: inline-block;
        padding: 4px 8px;
        cursor: pointer;
        border: 2px solid transparent;
    }

    .ranking.label:hover {
        border: 2px solid grey;
    }

    .ranking.label:hover span {
        text-decoration: underline;
    }

    .ranking.label span {
        vertical-align: middle;
    }
</style>
<h2><?php echo __('Guest Directory'); ?></h2>
<div class="pull-left">
    <form action="users.php" method="get">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="search">
        <table>
            <tr>
                <td><input type="text" id="basic-user-search" name="query" size=30 value="<?php echo Format::htmlchars($_REQUEST['query']); ?>"
                autocomplete="off" autocorrect="off" autocapitalize="off"></td>
                <td><input type="submit" name="basic_search" class="button" value="<?php echo __('Search'); ?>"></td>
                <td>
                    <label class="ranking label label-default" for="all">
                        <input name="all" id="all" value="all" <?php if ((!empty($_REQUEST['all']) && $_REQUEST['all'] === 'all')
                            || ( empty($_REQUEST['gold']) && empty($_REQUEST['platinum']) && empty($_REQUEST['diamond']))
                        ): ?>checked<?php endif; ?> type="checkbox">
                            <span>Show All</span>
                    </label>
                    <label class="ranking label label-warning" for="gold">
                        <input name="gold" id="gold" value="gold" <?php if (!empty($_REQUEST['gold']) && $_REQUEST['gold'] === 'gold'): ?>checked<?php endif; ?> type="checkbox">
                            <span>Gold</span>
                    </label>
                    <label class="ranking label label-success" for="platinum">
                        <input name="platinum" id="platinum" value="platinum" <?php if (!empty($_REQUEST['platinum']) && $_REQUEST['platinum'] === 'platinum'): ?>checked<?php endif; ?> type="checkbox">
                            <span>Platinum</span>
                    </label>
                    <label class="ranking label label-primary" for="diamond">
                        <input name="diamond" id="diamond" value="diamond" <?php if (!empty($_REQUEST['diamond']) && $_REQUEST['diamond'] === 'diamond'): ?>checked<?php endif; ?> type="checkbox">
                            <span>Diamond</span>
                    </label>
                </td>
            </tr>
        </table>
    </form>
 </div>

<div class="pull-right">
    <a class="action-button popup-dialog"
        href="#users/add">
        <i class="icon-plus-sign"></i>
        <?php echo __('Add Guest'); ?>
    </a>
    <a class="action-button popup-dialog"
        href="#users/import">
        <i class="icon-upload"></i>
        <?php echo __('Import'); ?>
    </a>
    <span class="action-button" data-dropdown="#action-dropdown-more"
        style="/*DELME*/ vertical-align:top; margin-bottom:0">
        <i class="icon-caret-down pull-right"></i>
        <span ><i class="icon-cog"></i> <?php echo __('More');?></span>
    </span>
    <div id="action-dropdown-more" class="action-dropdown anchor-right">
        <ul>
            <li><a class="users-action" href="#delete">
                <i class="icon-trash icon-fixed-width"></i>
                <?php echo __('Delete'); ?></a></li>
            <li><a href="#orgs/lookup/form" onclick="javascript:
$.dialog('ajax.php/orgs/lookup/form', 201);
return false;">
                <i class="icon-group icon-fixed-width"></i>
                <?php echo __('Add to Organization'); ?></a></li>
<?php
if ('disabled' != $cfg->getClientRegistrationMode()) { ?>
            <li><a class="users-action" href="#reset">
                <i class="icon-envelope icon-fixed-width"></i>
                <?php echo __('Send Password Reset Email'); ?></a></li>
            <li><a class="users-action" href="#register">
                <i class="icon-smile icon-fixed-width"></i>
                <?php echo __('Register'); ?></a></li>
            <li><a class="users-action" href="#lock">
                <i class="icon-lock icon-fixed-width"></i>
                <?php echo __('Lock'); ?></a></li>
            <li><a class="users-action" href="#unlock">
                <i class="icon-unlock icon-fixed-width"></i>
                <?php echo __('Unlock'); ?></a></li>
<?php } # end of registration-enabled? ?>
        </ul>
    </div>
</div>

<div class="clear"></div>
<?php
$showing = $search ? __('Search Results').': ' : '';
$res = db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing .= $pageNav->showing();
else
    $showing .= __('No guests found!');
?>
<form id="users-list" action="users.php" method="POST" name="staff" >
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process" >
 <input type="hidden" id="action" name="a" value="" >
 <input type="hidden" id="selected-count" name="count" value="" >
 <input type="hidden" id="org_id" name="org_id" value="" >
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th nowrap width="12"> </th>
            <th width="350"><a <?php echo $name_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=name"><?php echo __('Name'); ?></a></th>
            <th width="150"><a <?php echo $name_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=name"><?php echo __('Phone Number'); ?></a></th>
            <th width="150"><a <?php echo $name_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=name"><?php echo __('Email'); ?></a></th>
            <th width="150"><a <?php echo $name_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=booking_code"><?php echo __('Booking Code'); ?></a></th>
            <th width="100"><a <?php echo $create_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=create"><?php echo __('Created'); ?></a></th>
            <th width="165"><a <?php echo $update_sort; ?> href="users.php?<?php
                echo $qstr; ?>&sort=update"><?php echo __('Updated'); ?></a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        if($res && db_num_rows($res)):
            $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
            while ($row = db_fetch_array($res)) {
                // Default to email address mailbox if no name specified
                if (!$row['name'])
                    list($name) = explode('@', $row['email']);
                else
                    $name = new PersonsName($row['name']);

                $sel=false;
                if($ids && in_array($row['id'], $ids))
                    $sel=true;
                ?>
               <tr id="<?php echo $row['id']; ?>">
                <td nowrap>
                    <input type="checkbox" value="<?php echo $row['id']; ?>" class="ckb mass nowarn"/>
                </td>
                <td>&nbsp;
                    <a class="userPreview" href="users.php?id=<?php echo $row['id']; ?>"><?php
                        echo Format::htmlchars($name); ?></a>
                </td>
                <td>
                    <a class="userPreview" href="users.php?id=<?php echo $row['id']; ?>"><?php
                        echo Format::htmlchars($row['phone_number']); ?></a>
                </td>
                <td>
                    <a class="userPreview" href="users.php?id=<?php echo $row['id']; ?>"><?php
                        echo Format::htmlchars($row['email']); ?></a>
                </td>
                <td>
                <?php if(!empty($row['phone_number'])):
                    $booking_total_retail_price = 0;
                    $bookings = Booking::getBookingsByPhoneNumber($row['phone_number']);
                    while ($booking = db_fetch_array($bookings)):
                        $booking_total_retail_price += @intval($booking['total_retail_price']);
                ?>
                    <a href="/scp/booking.php?booking_code=<?php echo $booking['booking_code']; ?>"
                        target="_blank" class="no-pjax" title="<?php echo number_format($booking['total_retail_price'], 0).'đ'; ?>"
                    >
                        <?php echo $booking['booking_code']; ?>
                    </a>
                    <br>
                <?php endwhile; ?>
                    <?php if ($booking_total_retail_price): ?>
                    <p style="font-weight: bold;"><?php echo number_format($booking_total_retail_price, 0).'đ'; ?></p>
                    <?php endif; ?>
                <?php endif; ?>
                </td>
                <td><?php echo Format::db_date($row['created']); ?></td>
                <td><?php echo Format::db_datetime($row['updated']); ?>&nbsp;</td>
               </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
    <tfoot>
     <tr>
        <td colspan="7">
            <?php if ($res && $num) { ?>
            <?php echo __('Select');?>:&nbsp;
            <a id="selectAll" href="#ckb"><?php echo __('All');?></a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb"><?php echo __('None');?></a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb"><?php echo __('Toggle');?></a>&nbsp;&nbsp;
            <?php }else{
                echo '<i>';
                echo __('Query returned 0 results.');
                echo '</i>';
            } ?>
        </td>
     </tr>
    </tfoot>
</table>
<?php
if($res && $num && $thisstaff->isAdmin()): //Show options..
    echo sprintf('<div>&nbsp;'.__('Page').': %s &nbsp; <a class="no-pjax"
            href="users.php?a=export&qh=%s">'.__('Export').'</a></div>',
            $pageNav->getPageLinks(),
            $qhash);
endif;
?>
</form>

<script type="text/javascript">
$(function() {
    var last_req;
    $('input#basic-user-search').typeahead({
        source: function (typeahead, query) {
            if (!query.trim().length) return false;

            $('input#basic-user-search').val(query.trim());

            if (last_req) {
                window.clearTimeout(last_req);
                last_req = undefined;
                console.log('clearTimeout');
            }
            last_req = setTimeout(function() {
                $.ajax({
                    url: "ajax.php/users/local?q="+query,
                    dataType: 'json',
                    success: function (data) {
                        typeahead.process(data);
                    }
                });
            }, 600);
        },
        onselect: function (obj) {
            window.location.href = 'users.php?id='+obj.id;
        },
        property: "/bin/true"
    });

    $(document).on('click', 'a.popup-dialog', function(e) {
        e.preventDefault();
        $.userLookup('ajax.php/' + $(this).attr('href').substr(1), function (user) {
            var url = window.location.href;
            if (user && user.id)
                url = 'users.php?id='+user.id;
            $.pjax({url: url, container: '#pjax-container'})
            return false;
         });

        return false;
    });
    var goBaby = function(action, confirmed) {
        var ids = [],
            $form = $('form#users-list');
        $(':checkbox.mass:checked', $form).each(function() {
            ids.push($(this).val());
        });
        if (ids.length) {
          var submit = function() {
            $form.find('#action').val(action);
            $.each(ids, function() { $form.append($('<input type="hidden" name="ids[]">').val(this)); });
            $form.find('#selected-count').val(ids.length);
            $form.submit();
          };
          if (!confirmed)
              $.confirm(__('You sure?')).then(submit);
          else
              submit();
        }
        else {
            $.sysAlert(__('Oops'),
                __('You need to select at least one item'));
        }
    };
    $(document).on('click', 'a.users-action', function(e) {
        e.preventDefault();
        goBaby($(this).attr('href').substr(1));
        return false;
    });
    $(document).on('dialog:close', function(e, json) {
        $form = $('form#users-list');
        try {
            var json = $.parseJSON(json),
                org_id = $form.find('#org_id');
            if (json.id) {
                org_id.val(json.id);
                goBaby('setorg', true);
            }
        }
        catch (e) { }
    });
});

document.addEventListener('DOMContentLoaded', function() {
    var allCheckbox = document.getElementById('all');
    var checkboxes = [document.getElementById('gold'), document.getElementById('platinum'), document.getElementById('diamond')];

    allCheckbox.addEventListener('change', function() {
        if (!allCheckbox.checked) return;

        checkboxes.forEach(function(checkbox) {
            checkbox.checked = false;
        });
    });

    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            if (checkbox.checked) {
                allCheckbox.checked = false;
            }
        });
    });
});
</script>

<h2>Sales And CS Activities</h2>

<form action="<?php echo $cfg->getUrl() ?>scp/sales-income.php" method="get">
<table>
    <tr>
        <td>Staff</td>
        <td>
            <select name="staff_id" class="input-field" id="">
                <option value=>-- All --</option>
                <?php while($list_staff && ($_staff=db_fetch_array($list_staff))): ?>
                    <option value="<?php echo $_staff['staff_id'] ?>"
                            <?php if (isset($_REQUEST['staff_id']) && $_REQUEST['staff_id'] == $_staff['staff_id']) echo 'selected' ?>
                    ><?php echo $_staff['username'] ?></option>
                <?php endwhile; ?>
            </select>
        </td>
        <td>From</td>
        <td>
            <input type="text" class="dp input-field" name="from"
                   value="<?php echo date_create_from_format('Y-m-d', $_REQUEST['from'])->format('d/m/Y') ?>">
        </td>
        <td>To</td>
        <td>
            <input type="text" class="dp input-field" name="to"
                   value="<?php echo date_create_from_format('Y-m-d', $_REQUEST['to'])->format('d/m/Y') ?>">
        </td>

        <td>Group: </td>
        <td>
            <select name="group" id="group">
                <option value="day" <?php
                if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'day') echo 'selected';
                ?>>Day</option>
                <option value="week" <?php
                if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'week') echo 'selected';
                ?>>Week</option>
                <option value="month" <?php
                if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'month') echo 'selected';
                ?>>Month</option>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="8">
            <p class="jqx-center-align">
                <button class="btn_sm btn-primary">Search</button>
                <button class="reset btn_sm btn-default">Reset</button>
            </p>
        </td>
    </tr>
</table>
</form>

<?php if(isset($_REQUEST['staff_id']) && $_REQUEST['staff_id']): ?>
<h2>Total: <?php if (isset($total_by_staff)) echo number_format($total_by_staff, 0, ',', '.').' đ' ?></h2>

    <p><?php echo $showing ?></p>
<table class="list" width="1050">
    <caption>Sales Income</caption>
    <thead>
    <tr>
        <th>No.</th>
        <th>Staff</th>
        <th>Tour</th>
        <th>Booking Code</th>
        <th>Receipt Code</th>
        <th>Quantity</th>
        <th>Total</th>
        <th>Average Price</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = ($page-1)*$pagelimit; while($all_res && ($row = db_fetch_array($all_res))): ?>
    <tr>
        <td><?php echo ++$i; ?></td>
        <td><?php if (isset($all_staff_list[ $row['staff_id'] ])) echo $all_staff_list[ $row['staff_id'] ] ?></td>
        <td><?php if (isset($booking_type_items[ $row['booking_type_id'] ])) echo $booking_type_items[ $row['booking_type_id'] ] ?></td>
        <td><a href="<?php echo $cfg->getUrl() ?>scp/booking.php?booking_code=<?php echo $row['booking_code'] ?>" class="no-pjax" target="_blank"><?php echo $row['booking_code'] ?></a></td>
        <td><?php echo $row['receipt_code'] ?></td>
        <td><?php echo $row['total_quantity'] ?></td>
        <td><?php if (isset($row['total'])) echo number_format($row['total'], 0, ',', '.').' đ' ?></td>
        <td><?php if (isset($row['total']) && $row['total_quantity'] != 0) echo number_format($row['total']/$row['total_quantity'], 0, ',', '.').' đ' ?></td>
    </tr>
    <?php endwhile; ?>
    </tbody>
</table>
<?php
if ($total>0) { //if we actually had any tickets returned.
    echo '<div>'.__('Page').':'.$pageNav->getPageLinks();
} ?>

<?php endif; ?>

<table>
    <tr>

        <td>
            <table>
                <caption>Staff Summary</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Staff</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; while($incomeSummaryTable && ($row = db_fetch_array($incomeSummaryTable))): ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td>
                        <a href="<?php echo $cfg->getUrl() ?>scp/sales-income.php?staff_id=<?php
                        echo $row['staff_id'] ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php
                        echo $_REQUEST['to'] ?>"><?php if (isset($all_staff_list[ $row['staff_id'] ])) echo $all_staff_list[ $row['staff_id'] ] ?>
                        </a>
                    </td>
                    <td><?php if (isset($row['total'])) echo number_format($row['total'], 0, ',', '.').' đ' ?></td>
                </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </td>

        <td>
            <div class="staff_summary">
                <canvas id="staff_summary" width="700" height="700"></canvas>
                <button class="btn_sm btn-xs btn-default" onclick="hide_all('staff_summary_chart')">Hide All</button>
                <script>
                    var staff_summary = document.getElementById("staff_summary").getContext('2d');
                    <?php $i = 0; $dates = []; ?>
                    var colors = poolColors(<?php echo count($source_by_day_chart) ?>);
                    var staff_summary_chart = new Chart(staff_summary, {
                        type: 'bar',
                        data: {
                            datasets: [
                                <?php foreach($source_by_day_chart as $_type => $_date): ?>
                                {
                                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                                    backgroundColor:  colors[<?php echo $i++ ?>],
                                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                                    fill: true
                                    <?php $dates = array_merge($dates, array_keys($_date)) ?>
                                },
                                <?php endforeach; ?>
                            ],
                            labels: $.parseJSON('<?php echo json_encode(array_unique($dates)) ?>'),
                        },
                        options: {
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                        if (label) {
                                            label += ': ';
                                        }

                                        label += parseInt(tooltipItem.yLabel).formatMoney(0, ',',  '.')+' đ';
                                        return label;
                                    }
                                }
                            },

                            title: {
                                display: true,
                                text: 'Staff Summary By Date'
                            },
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            },
                            responsive: false,
                            maintainAspectRatio: false,
                            showLines: false // disable for all datasets
                        }
                    });
                </script>
            </div>
        </td>
    </tr>
</table>


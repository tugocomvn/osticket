
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vouchermx4m42k1t6`
--

-- --------------------------------------------------------

--
-- Table structure for table `group_voucher`
--

CREATE TABLE `mkt_group_voucher` (
                                   `group_id` char(50) NOT NULL,
                                   `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                   `content` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                   `date_start` date NOT NULL,
                                   `date_end` date NOT NULL,
                                   `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `mkt_user` (
                          `user_id` int(50) NOT NULL,
                          `username` varchar(100) NOT NULL,
                          `sdt` text NOT NULL,
                          `email` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                          `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `mkt_voucher` (
                             `id` int(50) NOT NULL,
                             `code_voucher` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                             `group_id` char(50) NOT NULL,
                             `status` tinyint(4) NOT NULL,
                             `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_redeem`
--

CREATE TABLE `mkt_voucher_redeem` (
                                    `booking_code` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                    `code_voucher` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                    `user_id` int(50) NOT NULL,
                                    `date_redeem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group_voucher`
--
ALTER TABLE `mkt_group_voucher`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `mkt_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `mkt_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `code_voucher` (`code_voucher`);

--
-- Indexes for table `voucher_redeem`
--
ALTER TABLE `mkt_voucher_redeem`
  ADD PRIMARY KEY (`booking_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `mkt_user`
  MODIFY `user_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `mkt_voucher`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

alter table mkt_user change username customer_name varchar(64) not null;

alter table mkt_user change sdt phone_number varchar(16) not null;

alter table mkt_user modify email varchar(64) collate utf8_unicode_ci not null;

create index mkt_user_user_id_index
  on mkt_user (user_id);

drop index user_id on mkt_user;

alter table mkt_voucher modify code_voucher varchar(32) collate utf8_unicode_ci not null;

alter table mkt_voucher modify group_id int not null;

create index mkt_voucher_status_index
  on mkt_voucher (status);


alter table mkt_voucher_redeem drop primary key;

alter table mkt_voucher_redeem
  add id int auto_increment primary key first;

alter table mkt_voucher_redeem modify booking_code varchar(16) collate utf8_unicode_ci not null;

alter table mkt_voucher_redeem modify code_voucher varchar(32) collate utf8_unicode_ci not null;

alter table mkt_voucher_redeem modify user_id int null;

create index mkt_voucher_redeem_booking_code_index
  on mkt_voucher_redeem (booking_code);

create index mkt_voucher_redeem_code_voucher_index
  on mkt_voucher_redeem (code_voucher);

create index mkt_voucher_redeem_user_id_index
  on mkt_voucher_redeem (user_id);

create table mkt_wifi_eventobject_voucher
(
  wifi_eventobject_id int not null,
  voucher_id int not null,
  send_date datetime null,
  last_check datetime null,
  redeem_date datetime null,
  constraint mkt_wifi_eventobject_voucher_pk
    primary key (voucher_id, wifi_eventobject_id)
);

alter table mkt_wifi_eventobject_voucher
  add reddem_booking_code varchar(16) null;

create unique index mkt_wifi_eventobject_voucher_reddem_booking_code_uindex
  on mkt_wifi_eventobject_voucher (reddem_booking_code);

create index mkt_wifi_eventobject_voucher_redeem_date_index
  on mkt_wifi_eventobject_voucher (redeem_date);

create index mkt_wifi_eventobject_voucher_send_date_index
  on mkt_wifi_eventobject_voucher (send_date);

alter table mkt_wifi_eventobject
  add campaignId varchar(32) null;


alter table mkt_group_voucher modify group_id int auto_increment;

drop index mkt_wifi_eventobject_voucher_reddem_booking_code_uindex on mkt_wifi_eventobject_voucher;

alter table mkt_group_voucher
  add campaignId varchar(20) null;

create unique index mkt_group_voucher_campaignId_uindex
  on mkt_group_voucher (campaignId);


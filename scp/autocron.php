<?php
/*********************************************************************
    cron.php

    Auto-cron handle.
    File requested as 1X1 image on the footer of every staff's page

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
define('AJAX_REQUEST', 1);
require('staff.inc.php');
ignore_user_abort(1);//Leave me a lone bro!
@set_time_limit(0); //useless when safe_mode is on

$_notify = '';
$_number_list = [];

//Terminate the request
//if (function_exists('fastcgi_finish_request'))
//    fastcgi_finish_request();

//Keep the image output clean. Hide our dirt.
//TODO: Make cron DB based to allow for better time limits. Direct calls for now sucks big time.
//We DON'T want to spawn cron on every page load...we record the lastcroncall on the session per user
$caller = $thisstaff->getUserName();
$__thisstaff = $thisstaff;
// Agent can call cron once every 3 minutes. //tmp disabled

$now = time();
$time = 0;
$log_dir = __DIR__.'/../local_data';
$file_timelog = $log_dir.'/lastfetch.txt';

if (!file_exists($log_dir))
    mkdir($log_dir, 0777);

if (file_exists($file_timelog)) {
    $time = file_get_contents($file_timelog);
    $time = intval(trim($time));
}

if ($now - $time < config('mailfetch_time'))
    exit;
// Release the session to prevent locking a future request while this is
// running
file_put_contents($file_timelog, $now);

require_once(INCLUDE_DIR.'class.cron.php');

// Clear staff obj to avoid false credit internal notes & auto-assignment
$thisstaff = null;

session_write_close();

// Age tickets: We're going to age tickets regardless of cron settings.
Cron::TicketMonitor();

if($cfg && $cfg->isAutoCronEnabled()) { //ONLY fetch tickets if autocron is enabled!
    Cron::MailFetcher();  //Fetch mail.
    $ost->logDebug(_S('Auto Cron'), sprintf(_S('Mail fetcher cron call [%s]'), $caller));
}

$data = array('autocron'=>true);
Signal::send('cron', $data);

@header('Content-type:  text/javascript');
@header('Cache-Control: no-cache, must-revalidate');
@header('Content-Length: '.strlen($_notify));
while(@ob_end_flush());
flush();
ob_start();
if ($_notify) echo $_notify;
else echo "console.log('Nothing');";

ob_end_flush();

try {
    @db_close();
} catch(Exception $ex) {}

?>

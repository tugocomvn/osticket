alter table payment_tmp modify time bigint null;

alter table payment_tmp modify receipt_code varchar(255) null;

alter table payment_tmp modify amount bigint null;

alter table payment_tmp modify quantity int null;

alter table payment_tmp modify income_type varchar(255) null;

alter table payment_tmp modify outcome_type varchar(255) null;

create index payment_tmp_amount_index
    on payment_tmp (amount);

alter table payment_tmp
    add income_type_id int null after income_type;

alter table payment_tmp
    add outcome_type_id int null after outcome_type;

create index payment_tmp_income_type_id_index
    on payment_tmp (income_type_id);

create index payment_tmp_outcome_type_id_index
    on payment_tmp (outcome_type_id);


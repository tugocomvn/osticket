<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canEditPayments() || !$ticket) die('Access Denied');
if(!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
    function staffLoginPage($msg) {
        global $ost, $cfg;
        $_SESSION['_staff']['auth']['dest'] =
            '/' . ltrim($_SERVER['REQUEST_URI'], '/');
        $_SESSION['_staff']['auth']['msg']=$msg;
        $_SESSION['_staff']['auth']['finance']=true;
        require(SCP_DIR.'login.php');
        exit;
    }
}

if (!isset($_SESSION['check_session']) || !$_SESSION['check_session']
    || !isset($_SESSION['check_session_time'])
    || (isset($_SESSION['check_session_time']) && time() - $_SESSION['check_session_time'] > 1000)) {
    $_SESSION['check_session_time'] = -10000;
    $_SESSION['check_session'] = false;
    unset($_SESSION['check_session_time']);
    unset($_SESSION['check_session']);
    staffLoginPage(__('Enter password for access'));
    exit;
}

$__log = AllAction::create([
    'source_id' => $thisstaff->getId(),
    'source_name' => 'staff',
    'action_name' => 'edit ticket',
    'info' => $ticket->getId(),
]);
Signal::send('log.new_action', $__log);

$info=Format::htmlchars(($errors && $_POST)?$_POST:$ticket->getUpdateInfo());
if ($_POST)
    $info['duedate'] = Format::date($cfg->getDateFormat(),
       strtotime($info['duedate']));
?>
<form action="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-io" method="post" id="save"  enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="update">
 <input type="hidden" name="a" value="edit-io">
 <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
 <h2><?php echo sprintf(__('Update Ticket #%s'),$ticket->getNumber());?></h2>
 <table class="form_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <tbody>
        <tr>
            <th colspan="2">
            <em><strong><?php echo __('Ticket Information'); ?></strong>: <?php echo __("Due date overrides SLA's grace period."); ?></em>
            </th>
        </tr>
        <tr>
            <td width="160" class="required">
                <?php echo __('Help Topic');?>:
            </td>
            <td>
                <select disabled="disabled"">
                    <option value="" selected >&mdash; <?php echo __('Select Help Topic');?> &mdash;</option>
                    <?php
                    $this_help_id = null;
                    if($topics=Topic::getHelpTopics()) {
                        foreach($topics as $id =>$name) {
                            if (!in_array($id, unserialize(IO_TOPIC))) continue;
                            if ($info['topicId']==$id) $this_help_id = $id;
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['topicId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                <input type="hidden" name="topicId" value="<?php echo $this_help_id ?>">
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>
        </tr>
    </tbody>
</table>
<table class="form_table dynamic-forms" width="940" border="0" cellspacing="0" cellpadding="2">
        <?php
        if ($forms) {
            $this_topic = $ticket->getTopic();
            foreach ($forms as $form) {
                if (!$form || !$form->getForm()) continue;

                if ($this_topic->getFormId() == $form->getForm()->get('id'))
                    $form->render(true, false, array('mode'=>'edit','width'=>160,'entry'=>$form));
            }
        } ?>
</table>

<p style="padding-left:250px;">
    <input type="submit" name="submit" class="btn_sm btn-primary" value="<?php echo __('Save');?>">
    <input type="reset"  name="reset"  class="btn_sm btn-default" value="<?php echo __('Reset');?>">
    <input type="button" name="cancel" class="btn_sm btn-danger" value="<?php echo __('Cancel');?>" onclick='window.location.href="tickets.php?id=<?php echo $ticket->getId(); ?>"'>
</p>
</form>
<div style="display:none;" class="dialog draggable" id="user-lookup">
    <div class="body"></div>
</div>
<script type="text/javascript">
$('table.dynamic-forms').sortable({
  items: 'tbody',
  handle: 'th',
  helper: function(e, ui) {
    ui.children().each(function() {
      $(this).children().each(function() {
        $(this).width($(this).width());
      });
    });
    ui=ui.clone().css({'background-color':'white', 'opacity':0.8});
    return ui;
  }
});

<?php if (isset($_GET['booking_code']) && !empty($_GET['booking_code'])):
$booking_code = $_GET['booking_code'];
$booking_code = str_replace('TG123-', '', $booking_code);
?>
(function($) {
    $('[data-name=booking_code]').val('<?php echo trim('TG123-'.$booking_code) ?>');
    $('[data-name=booking_code]').after(' <span id="msg_error">Đây là mã booking vừa được sales cập nhật, hãy kiểm tra và bấm SAVE</span>');
})(jQuery);
<?php endif; ?>
</script>

<style>
    .pax_list {
        counter-reset: my-badass-counter;
    }

    .pax_list .stt:before {
        content: '#' counter(my-badass-counter);
        counter-increment: my-badass-counter;
        position: absolute;
        padding-left: 1em;
    }

    .pax_list .stt button {
        margin-left: 3.5em;
    }

    .pax_list textarea {
        width: 90%;
        height: 4em;
        resize: none;
    }

    .outter_table>tbody>tr>td {
        border: none;
        border-bottom: 1px solid #eee;
    }

    .outter_table>tbody:last-child>tr>td {
        border: none;
        border-bottom: 1px solid #eee;
        padding-top: 1em;
        padding-bottom: 1em;
    }

    .inner_table td {
        border: none;
    }

    table.fixed td.stt {
        width: 5em;
    }

    .inner_table td label {
        margin-left: 1em;
    }

    iframe {
        height: 50px;
        width: 100%;
    }

    .pax_item:nth-child(2n) td {
        background: #fff6ff;
    }

    .pax_item:nth-child(2n+1) td {
        background: white;
    }

    .passport_no {
        width: 100px;
    }

    input.dp, .dp, .dp_dob, .dp_doe, .dp_visa {
        width: 90px;
    }

    table.fixed td {
        width: auto;
    }

    .full_name {
        width: 200px;
    }

    .passport_photo_preview img {
        max-width: 300px;
    }
</style>
<p>
    <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"
        class="btn_sm btn-default">Back</a>
</p>

<h2><?php echo __('Pax List'); ?> | <small>Tour: <?php echo $tour->name ?></small></h2>

<div>
    <div class="pull-right">
        Layout:
        <button type="button" class="btn_sm btn-info" onclick="$('.inner_table tr').not('.simple_sort').hide()">Simple</button>
        <button type="button" class="btn_sm btn-default" onclick="$('.inner_table tr').not('.simple_sort').show()">All</button>
    </div>
    <div class="clearfix"></div>
</div>
<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" id="save"  enctype="multipart/form-data" class="repeater" target="save_frame">
    <?php csrf_token(); ?>
    <input type="hidden" name="tour_id" value="<?php echo $tour->id ?>" >
    <input type="hidden" name="referer" value="<?php echo $referer ?>" >
    <table class="form_table fixed outter_table" width="1058" border="0" cellspacing="0" cellpadding="2">
        <tbody data-repeater-list="group-loyalty" class="pax_list sortable-rows ui-sortable">
        <?php if (!$tour_pax_list->num_rows): ?>
            <tr data-repeater-item class="pax_item">
                <td class="stt">
                </td>
                <td>
                    <p class="pax_description"></p>
                    <table border="0" cellspacing="0" cellpadding="2" class="inner_table">
                        <tr>
                            <td><label for="passport_no">Passport No.</label></td>
                            <td><input class="input-field passport_no new" name="passport_no" required type="text"> <button class="btn_sm btn-xs btn-primary" type="button"><i class="icon-search"></i></button></td>
                            <td><label for="doe">DOE</label></td>
                            <td><input class="dp_doe doe new" name="doe"  min="<?php echo date('Y-m-d', time()+3600*24*30*6) ?>" required type="text"></td>
                            <td><label for="nationality">Nationality</label></td>
                            <td>
                                <select class="input-field nationality" required name="nationality">
                                    <?php
                                    if (is_array($country_list) && $country_list):
                                    foreach($country_list as $_code => $_name): ?>
                                        <option value="<?php echo $_code ?>"><?php echo $_name ?></option>
                                    <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="simple_sort">
                            <td><label for="full_name">Passenger Name</label></td>
                            <td><input class="input-field full_name" name="full_name" required type="text"></td>

                            <td><label for="room">Room No.</label></td>
                            <td>
                                <select class="input-field room" name="room">
                                    <option value=>-- Select --</option>
                                    <?php for($_room = 1; $_room <= 40; $_room++): ?>
                                        <option value="<?php echo $_room ?>"><?php echo $_room ?></option>
                                    <?php endfor; ?>
                                </select>
                            </td>

                            <td><label for="room_type">Room Type</label></td>
                            <td>
                                <select class="input-field room_type" name="room_type">
                                    <option value=>-- Select --</option>
                                    <?php foreach($roomtype_list as $_type): ?>
                                        <option value="<?php echo $_type ?>"><?php echo $_type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="gender">Gender</label></td>
                            <td>
                                <select class="input-field gender" required name="gender">
                                    <option value=>-- Select --</option>
                                    <option value="0">Female</option>
                                    <option value="1">Male</option>
                                </select>
                            </td>
                            <td><label for="dob">DOB</label></td>
                            <td><input class="dp_dob dob new" name="dob"  required type="text"></td>

                            <td><label for="phone_number">Phone number</label></td>
                            <td>
                                <input class="input-field phone_number" required name="phone_number" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Receiving date</label></td>
                            <td><input class="dp_visa receiving_date new" name="receiving_date"   type="text"></td>
                            <td>
                                <label for="">Visa apply date</label>
                                <button type="button" class="btn_sm btn-xs btn-default"
                                        onclick="$(this).parents('td').next('td').find('.apply_date')
                                            .datepicker( 'setDate', null );"><i class="icon-remove"></i></button>
                            </td>
                            <td><input class="dp_visa apply_date new" name="apply_date"   type="text"></td>
                            <td>
                                <label for="">Est. due date</label>
                                <button type="button" class="btn_sm btn-xs btn-default"
                                        onclick="$(this).parents('td').next('td').find('.visa_estimated_due_date')
                                            .datepicker( 'setDate', null );"><i class="icon-remove"></i></button>
                            </td>
                            <td><input class="dp_visa visa_estimated_due_date new"  name="visa_estimated_due_date" type="text"></td>
                        </tr>
                        <tr>
                            <td><label for="booking_code">Booking Code</label></td>
                            <td><input class="input-field booking_code" name="booking_code" required type="text"></td>
                            <td><label for="visa_result">Visa result</label></td>
                            <td>
                                <select class="input-field visa_result" required name="visa_result">
                                    <?php foreach(VisaResult::getAll() as $_id => $_name): ?>
                                        <option value="<?php echo $_id ?>"><?php echo $_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td><label for="tl">Ghép vớiTL *</label></td>
                            <td>
                                <input type="radio" class="input-field tl" value="1"  name="tl" />
                            </td>
                        </tr>
                        <tr>
                            <td><label for="note">Note</label></td>
                            <td colspan="5">
                                <textarea name="note" id="note" cols="30" rows="5"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><button data-repeater-delete class="btn_sm btn-xs btn-danger" type="button" value="Minus"><i class="icon-minus-sign"></i> Remove</button></td>
                            <td colspan="5">
                                <p><em>Ghi chú: Nếu một khách đại diện đặt tour cho nhiều khách, sử dụng số điện thoại người đại diện</em></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php else: ?>
            <?php while(($row = db_fetch_array($tour_pax_list))): ?>
                <tr data-repeater-item class="pax_item">
                    <input type="hidden" name="pax_id" value="<?php echo $row['id'] ?>">
                    <td class="stt">
                    </td>
                    <td>
                        <p class="pax_description"></p>
                        <table border="0" cellspacing="0" cellpadding="2" class="inner_table">
                            <tr>
                                <td><label for="passport_no">Passport No.</label></td>
                                <td>
                                    <input class="input-field passport_no new" name="passport_no" required type="text" value="<?php echo $row['passport_no'] ?>">
                                    <button class="btn_sm btn-xs btn-primary" type="button"><i class="icon-search"></i></button>
                                </td>
                                <td><label for="doe">DOE</label></td>
                                <td><input class="dp_doe doe new" name="doe"  min="<?php echo date('Y-m-d', time()+3600*24*30*6) ?>"
                                           required type="text" value="<?php if(isset($row['doe']) && $row['doe'])  echo date('d/m/Y', strtotime($row['doe'])) ?>"></td>
                                <td><label for="nationality">Nationality</label></td>
                                <td>
                                    <select class="input-field nationality" required name="nationality">
                                        <?php
                                        if (is_array($country_list) && $country_list):
                                            foreach($country_list as $_code => $_name): ?>
                                                <option value="<?php echo $_code ?>"
                                                        <?php if(isset($row['nationality']) && $row['nationality'] == $_code) echo 'selected' ?>
                                                ><?php echo $_name ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr class="simple_sort">
                                <td><label for="full_name">Passenger Name</label></td>
                                <td><input class="input-field full_name" name="full_name" required type="text"
                                           value="<?php echo $row['full_name'] ?>"></td>

                                <td><label for="room">Room No.</label></td>
                                <td>
                                    <select class="input-field room" name="room">
                                        <option value=>-- Select --</option>
                                        <?php for($_room = 1; $_room <= 40; $_room++): ?>
                                            <option value="<?php echo $_room ?>"
                                                <?php if(isset($row['room']) && $_room == $row['room']) echo "selected" ?>><?php echo $_room ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>

                                <td><label for="room_type">Room Type</label></td>
                                <td>
                                    <select class="input-field room_type" name="room_type">
                                        <option value=>-- Select --</option>
                                        <?php foreach($roomtype_list as $_type): ?>
                                            <option value="<?php echo $_type ?>"
                                                <?php if(isset($row['room_type']) && $row['room_type'] == $_type) echo "selected" ?>><?php echo $_type ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="gender">Gender</label></td>
                                <td>
                                    <select class="input-field gender" required name="gender">
                                        <option value=>-- Select --</option>
                                        <option value="0" <?php if($row['gender'] == 0) echo "selected" ?>>Female</option>
                                        <option value="1" <?php if($row['gender'] == 1) echo "selected" ?>>Male</option>
                                    </select>
                                </td>

                                <td><label for="dob">DOB</label></td>
                                <td><input class="dp_dob dob new" name="dob"  required type="text"
                                           value="<?php if(isset($row['dob']) && $row['dob']) echo date('d/m/Y', strtotime($row['dob'])) ?>"></td>

                                <td><label for="phone_number">Phone number</label></td>
                                <td><input class="input-field phone_number" required name="phone_number" type="text"
                                           value="<?php echo $row['phone_number'] ?>"></td>
                            </tr>

                            <tr>
                                <td><label for="">Receiving date</label></td>
                                <td><input class="dp_visa receiving_date new" name="receiving_date"   type="text" value="<?php
                                    if(isset($row['receiving_date']) && $row['receiving_date'] && $row['receiving_date'] !== '0000-00-00 00:00:00')
                                        echo date('d/m/Y', strtotime($row['receiving_date'])) ?>"></td>
                                <td>
                                    <label for="">Visa apply date</label>
                                    <button type="button" class="btn_sm btn-xs btn-default"
                                            onclick="$(this).parents('td').next('td').find('.apply_date')
                                            .datepicker( 'setDate', null );"><i class="icon-remove"></i></button>
                                </td>
                                <td><input class="dp_visa apply_date new" name="apply_date"   type="text" value="<?php
                                    if(isset($row['apply_date']) && $row['apply_date'] && $row['apply_date'] !== '0000-00-00 00:00:00')
                                        echo date('d/m/Y', strtotime($row['apply_date'])) ?>"></td>
                                <td>
                                    <label for="">Est. due date</label>
                                    <button type="button" class="btn_sm btn-xs btn-default"
                                            onclick="$(this).parents('td').next('td').find('.visa_estimated_due_date')
                                            .datepicker( 'setDate', null );"><i class="icon-remove"></i></button>
                                </td>
                                <td><input class="dp_visa visa_estimated_due_date new"  name="visa_estimated_due_date" value="<?php
                                    if(isset($row['visa_estimated_due_date']) && $row['visa_estimated_due_date'] && $row['visa_estimated_due_date'] !== '0000-00-00 00:00:00')
                                        echo date('d/m/Y', strtotime($row['visa_estimated_due_date'])) ?>"  type="text"></td>
                            </tr>

                            <tr>
                                <td><label for="booking_code">Booking Code</label></td>
                                <td><input class="input-field booking_code" name="booking_code" required type="text"
                                           value="<?php echo $row['booking_code'] ?>"></td>
                                <td><label for="visa_result">Visa result</label></td>
                                <td>
                                    <select class="input-field visa_result" required name="visa_result">
                                        <?php foreach(VisaResult::getAll() as $_id => $_name): ?>
                                            <option value="<?php echo $_id ?>"
                                                <?php if(isset($row['visa_result']) && $row['visa_result'] == $_id) echo 'selected' ?>
                                            ><?php echo $_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td><label for="tl">Ghép với TL *</label></td>
                                <td>
                                    <input type="radio" class="input-field tl" value="1"  name="tl" <?php if (isset($row['tl']) && $row['tl']) echo 'checked'; ?> />
                                </td>
                            </tr>

                            <tr>
                                <td><label for="note">Note</label></td>
                                <td colspan="5">
                                    <textarea name="note" id="note" cols="30" rows="5"><?php echo trim($row['note']) ?></textarea>
                                </td>

                            </tr>
                            <tr>
                                <td><button data-repeater-delete class="btn_sm btn-xs btn-danger" type="button" value="Minus"><i class="icon-minus-sign"></i> Remove</button></td>
                                <td colspan="5">
                                    <p><em>Ghi chú: Nếu một khách đại diện đặt tour cho nhiều khách, sử dụng số điện thoại người đại diện</em></p>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="5">
                                    <table class="passport">
                                        <tr>
                                            <td>Upload Passport Photo: <br> <input type="file" name="passport_photo" data-passport-no="<?php echo $row['passport_no'] ?>" class="passport_photo"></td>
                                            <td>Status:</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="passport_photo_preview">
                                                    <img src alt="">
                                                </div>
                                            </td>
                                            <td><span class="passport_photo_status"></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php endwhile; ?>
        <?php endif; ?>
        </tbody>
        <tbody>
        <tr>
            <td></td>
            <td>
                <label for="tl"><input type="radio" class="input-field tl dbl_room" value="1"  name="tl" <?php if(!$tl_room) echo 'checked' ?>  /> TL phòng DBL</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><button data-repeater-create class="btn_sm btn-primary" type="button" value="Add"><i class="icon-plus-sign"></i> Add New Pax</button></td>
        </tr>
        </tbody>

        <tbody></tbody>
    </table>
    <hr>
    <table class="form_table " width="1058" border="0" cellspacing="0" cellpadding="2">
        <tbody>
        <tr>
            <td><label for="">Receiving date</label></td>
            <td><input class="dp_visa apply_receiving_date new" name="receiving_date"   type="text"></td>
            <td>
                <label for="">Visa apply date</label>
            </td>
            <td><input class="dp_visa apply_apply_date new" name="apply_date"   type="text"></td>
            <td>
                <label for="">Est. due date</label>
            </td>
            <td><input class="dp_visa apply_visa_estimated_due_date new"  name="visa_estimated_due_date" type="text"></td>

            <td colspan="6"><button type="button" class="apply_date_btn btn_sm btn-success btn-xs">Apply to All</button></td>
        </tr>
        </tbody>
    </table>
    <hr>
    <table class="form_table " width="1058" border="0" cellspacing="0" cellpadding="2">
        <tbody>
        <tr>
            <td><label for="visa_result">Visa result</label></td>
            <td>
                <select class="input-field apply_visa_result" required name="visa_result">
                    <option value="0">N/A</option>
                    <option value="1">Passed</option>
                    <option value="2">Failed</option>
                </select>
            </td>
            <td><button type="button" class="apply_visa_btn btn_sm btn-success btn-xs">Apply to All</button></td>
        </tr>
        </tbody>
    </table>
    <hr>
    <div>
        <p>
            <iframe src="" name="save_frame" frameborder="0"></iframe>
        </p>
    </div>

    <div>
        <p style="text-align:center;">
            <input type="submit" style="display:none" name="submitButton" id="submitButton">
            <button type="button" class="btn_sm btn-primary" id="save_btn" onclick="form_submit(this);return false;">Save</button> <a class="no-pjax btn_sm btn-default" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">Cancel</a>
        </p>
    </div>
</form>
<script>
    var this_config;
    var last_req;
    var tl_dbl = false;

    function form_submit() {
        if (!$('#save')[0].checkValidity()) {
            $('#submitButton').click();
            return false;
        }

        $('#save').submit();
    }

    function datePicker() {
        $('.dp_doe.new').datepicker({
            changeYear: true,
            yearRange: "2015:2035",
            shortYearCutoff: 50,
            showButtonPanel: true,
            selectOtherMonths: false,
            numberOfMonths: 1,
            buttonImage: './images/cal.png',
            showOn:'both',
            minDate: new Date(<?php echo (time() + 6*30*24*3600)*1000 ?>),
            dateFormat: $.translate_format(this_config.date_format||'dd/mm/yy')
        });

        $('.dp_dob.new').datepicker({
            changeYear: true,
            yearRange: "1925:2022",
            shortYearCutoff: 50,
            showButtonPanel: true,
            selectOtherMonths: false,
            numberOfMonths: 1,
            buttonImage: './images/cal.png',
            showOn:'both',
            maxDate: new Date(),
            dateFormat: $.translate_format(this_config.date_format||'dd/mm/yy')
        });
        $('.dp_doe.new').removeClass('new');
        $('.dp_dob.new').removeClass('new');

        $('.dp_visa.new').datepicker({
            changeYear: true,
            yearRange: "2017:2022",
            shortYearCutoff: 50,
            showButtonPanel: true,
            selectOtherMonths: false,
            numberOfMonths: 1,
            buttonImage: './images/cal.png',
            showOn:'both',
            maxDate: new Date(<?php echo (time() + 6*30*24*3600)*1000 ?>),
            dateFormat: $.translate_format(this_config.date_format||'dd/mm/yy')
        });
        $('.dp_visa.new').removeClass('new');
    }

    function passport_no_search() {
        $('.passport_no').typeahead({
            source: function (typeahead, query) {
                _passport_no_elm = $(typeahead.$element);
                console.log(_passport_no_elm)
                _parent = _passport_no_elm.parents('.pax_item');
                _pax_description = _parent.find('.pax_description');
                _pax_description.text('');

                if (!query.trim().length || query.trim().length < 5) return false;

                _passport_no_elm.val(query.trim());
                _pax_description.text('Đang tìm...');

                if (last_req) {
                    window.clearTimeout(last_req);
                    last_req = undefined;
                    console.log('clearTimeout');
                }
                last_req = setTimeout(function() {
                    console.log('setTimeout')
                    $.ajax({
                        url: "ajax.php/pax?passport_no="+query,
                        dataType: 'json',
                        success: function (data) {
                            if (!data.length) {
                                _pax_description.text('Không tìm thấy kết quả');
                            } else {
                                _pax_description.text('Có '+data.length+' kết quả');
                            }
                            typeahead.process(data);
                        },
                        error: function (data) {
                            _pax_description.text(data.responseText);
                        }
                    });
                }, 600);
            },
            onselect: function (obj) {
                _passport_no_elm = $(this.$element);
                _parent = _passport_no_elm.parents('.pax_item');

                _pax_description = _parent.find('.pax_description');
                _pax_description.text('');

                _parent.find('.doe').val($.datepicker.formatDate('dd/mm/yy', new Date(Date.parse(obj.doe.replace('-','/','g')))));
                _parent.find('.dob').val($.datepicker.formatDate('dd/mm/yy', new Date(Date.parse(obj.dob.replace('-','/','g')))));
                _parent.find('.passport_no').val(obj.passport_no);
                _parent.find('.full_name').val(obj.full_name);
                _parent.find('.phone_number').val(obj.phone_number);
                _parent.find('.gender').val(obj.gender);
            },
            property: "/bin/true"
        });
    }

    function tl_check(e) {
        _self = $(e.target);
        if (!_self.hasClass('tl'))
            _self = _self.find('.tl');
        if (!_self.hasClass('tl')) return false;

        $('.tl').prop('checked', false);
        _self.prop('checked', true);
        if (_self.hasClass('dbl_room'))
            tl_dbl = true;
        else
            tl_dbl = false;
    }

    function tl_radio_check() {
        $('.tl').off('click').on('click', tl_check);
        if (tl_dbl)
            $('.tl.dbl_room').prop('checked', true);
    }

    function apply_date_to_all() {
        if ($('.apply_receiving_date').val())
            $('.receiving_date').val( $('.apply_receiving_date').val() );

        if ($('.apply_apply_date').val())
            $('.apply_date').val( $('.apply_apply_date').val() );

        if ($('.apply_visa_estimated_due_date').val())
            $('.visa_estimated_due_date').val( $('.apply_visa_estimated_due_date').val() );
    }

    function apply_visa_to_all() {
        $('.visa_result').val( $('.apply_visa_result').val() );
    }

    $(document).ready(function () {
        getConfig().then(function(c) { this_config = c; datePicker(); });
        passport_no_search();

        tl_radio_check();

        $('.repeater').repeater({
            show: function () {
                $(this).slideDown('fast');
                setTimeout(function() {
                    datePicker();
                    passport_no_search();
                    tl_radio_check();
                }, 500);
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp('fast', deleteElement);
                }
            },
            ready: function (setIndexes) {
                $('.sortable-rows').on('sortstop', setIndexes);
            },
            isFirstItemUndeletable: false
        });

        $('.apply_date_btn').off('click').on('click', apply_date_to_all);
        $('.apply_visa_btn').off('click').on('click', apply_visa_to_all);

        $('.dp_visa, .dp_dob, .dp_doe').click(function(e) {
            e.target.select();
        });

        $(".passport_photo").change(function() {
            readURL(this);
        });
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                passport_no = $(input).data('passport-no').trim();

                parent = $(input).parents('table.passport');
                parent.find('.passport_photo_preview img').attr('src', e.target.result);
                _status = parent.find('.passport_photo_status');
                _status.text('Uploading...');

                var jqxhr = $.post(
                    '<?php echo $cfg->getUrl()?>scp/ajax.php/pax/passportPhoto',
                    {
                        data: e.target.result,
                        name: input.files[0].name,
                        passport_no: passport_no,
                        __CSRFToken__: $('[name=__CSRFToken__]').val()
                    },function() {})
                    .done(function(data) {
                        var response = jQuery.parseJSON(data);
                        if ('1' === response.result+'') {
                            parent.find('.passport_photo_preview img').attr('src', e.target.result);
                        }

                            _status.text(response.message);
                        })
                    .fail(function(err) {
                        _status.text(err);
                    })
                    .always(function() {
                        parent.find('.progress').hide();
                        $('[type=file]').val('').prop('disabled', true);
                        $('.custom-file-label').text('Choose file');
                        $('.booking_code').val('TG123-');
                        $('.booking_quantity').text('');
                    });
                };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

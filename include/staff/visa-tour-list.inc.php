<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if( !$thisstaff->canViewVisaDocuments()) die('Access Denied');
?>
<style>
    table.list tbody td,
    table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }
    .actions a {
        visibility: hidden;
    }

    tr:hover .actions a {
        visibility: visible;
    }

    table.list {
        margin-left: -100px;
    }
</style>
<h2><?php echo __('Tours List'); ?></h2>
<form action="<?php echo $cfg->getUrl();?>scp/visa-tour-list.php" method="get">
    <?php csrf_token(); ?>
    <table>
        <tr>
            <td>Tên Tour</td>
            <td>
                <input type="text" name="keyword_tour" value="<?php
                    if (isset($_REQUEST['keyword_tour']) && !empty($_REQUEST['keyword_tour']))
                        echo trim($_REQUEST['keyword_tour']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Điểm đến</td>
            <td>
                <select name="tour[]" class="input-field" multiple style="height: 12em;">
                    <option value=><?php echo __('-- All --') ?></option>
                    <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                        <?php foreach($booking_type_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                <?php if (isset($_REQUEST['tour']) && in_array($item['id'],$_REQUEST['tour'])) echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
            <td>Mã xin visa</td>
            <td>
                <input type="text" name="visa_code" value="<?php
                if (isset($_REQUEST['visa_code']) && !empty($_REQUEST['visa_code']))
                    echo trim($_REQUEST['visa_code']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Ngày tập trung</td>
            <td>
                <input style="width: 100px" type="text" class="input-field dp"
                       autocomplete="off" name="gather_date" placeholder="Tập trung"
                       value="<?php
                       if (isset($_REQUEST['gather_date']) && date_create_from_format('Y-m-d', $_REQUEST['gather_date']))
                           echo date('d/m/Y', strtotime($_REQUEST['gather_date'])) ?>">
            </td>
        </tr>
        <tr>
            <td>Tên khách</td>
            <td>
                <input type="text" name="name_pax" value="<?php
                if (isset($_REQUEST['name_pax']) && !empty($_REQUEST['name_pax']))
                    echo trim($_REQUEST['name_pax']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Số hộ chiếu</td>
            <td>
                <input type="text" name="passport_no" value="<?php
                    if (isset($_REQUEST['passport_no']) && !empty($_REQUEST['passport_no']))
                        echo trim($_REQUEST['passport_no']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Number Phone</td>
            <td>
                <input type="text" name="number_phone" value="<?php
                if (isset($_REQUEST['number_phone']) && !empty($_REQUEST['number_phone']))
                    echo trim($_REQUEST['number_phone']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Ngày Về</td>
            <td>
                <input placeholder="Đến" style="width: 100px" type="text"
                       class="input-field dp" name="return_date" autocomplete="off"
                       value="<?php
                       if (isset($_REQUEST['return_date']) && date_create_from_format('Y-m-d', $_REQUEST['return_date']))
                           echo date('d/m/Y', strtotime($_REQUEST['return_date'])) ?>">
            </td>
        </tr>
        <tr>
            <td>Nhân viên phụ trách</td>
            <td>
                <select name='staff' class="staff">
                    <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                    <?php while ($listStaff && ($row = db_fetch_array($listStaff))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['staff_id'],($_REQUEST['staff'] == $row['staff_id']?'selected="selected"':''),$row['username']);
                    }?>
                </select>
            </td>
            <td>Booking code</td>
            <td>
                <input type="text" name="booking_code" class="input-field" placeholder="" value="<?php
                if(isset($_REQUEST['booking_code']) && $_REQUEST['booking_code']) echo $_REQUEST['booking_code']
                ?>">
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="6">
                <input type="submit" class="btn_sm btn-primary"
                       value="<?php echo __('Search');?>" />
                <input type="reset" class="btn_sm btn-default"
                       value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
            </td>
        </tr>
        <script>
            function reset_form(event) {
                console.log(event);
                form = $(event).parents('form');
                form.find('input[type="text"], select, .hasDatepicker').val('');
                form.submit();
            }
        </script>
    </table>
 </form>

<table class="list" border="0" cellspacing="1" cellpadding="2" width="1258">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th>No.</th>
            <th>Tên Tour</th>
            <th>Điểm Đến</th>
            <th>Ngày Tập Trung/Ngày Về</th>
            <th>Tổng</th>
            <th>Deadline</th>
            <th>Thiếu hồ sơ</th>
            <th>Chưa có visa</th>
            <th>#</th>
        </tr>
    </thead>
    <tbody>
    <?php $time = time(); $i = $offset; ?>
    <?php while($results && ($row = db_fetch_array($results))): ?>
    <?php
        $sum_pax = 0;
        if (isset($row['id'])) {
            $sum_pax = TourNew::countPax($row['id']);
        }
        ?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td>
                    <input type="hidden" name="id_tour" value="<?php echo ($row['id']); ?>"/>
                    <?php echo $row['name']?>
                </td>
                <td><?php echo $row['name_booking'];?></td>
                <td>
                    <?php if(!empty($row['gather_date']) && !empty($row['return_date'])): ?>
                    <?php _Format::dateInSameYear($row['gather_date'], $row['return_date']) ?>
                    <?php endif; ?>
                </td>
                <td><?php echo $sum_pax ?></td>
                <td>
                    <?php $deadline = VisaTourGuest::getMinDeadlineTour($row['id']) ?>
                    <?php if($deadline): ?>
                        <?php if($time < strtotime($row['gather_date'])): ?>
                            <?php if ($time >  strtotime($row['gather_date']) - VISA_DEADLINE_DANGER): ?>
                                <span class="label label-danger">
                                    <?php echo date('d/m', strtotime($deadline)) ?>
                                </span>
                            <?php elseif ($time >  strtotime($row['gather_date']) - VISA_DEADLINE_WARNING): ?>
                                <span class="label label-warning">
                                    <?php echo date('d/m', strtotime($deadline)) ?>
                                </span>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo date('d/m', strtotime($deadline)) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td><?php if(isset($count_status_wait[$row['id']])) echo $count_status_wait[$row['id']]; else echo 0; ?></td>
                <td><?php if(isset($count_status_hasVisa[$row['id']])) echo ($sum_pax - (int)$count_status_hasVisa[$row['id']]); else echo $sum_pax; ?></td>
                <td class="autohide-buttons">
                    <a href="<?php echo $cfg->getUrl();
                    ?>scp/visa-tour-guest.php?tour=<?php echo $row['id'] ?>&layout=list"
                        target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                            data-title="View List" >
                            <i class="icon-list"></i></a>
                                    
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                    <a href="<?php echo $cfg->getUrl();
                    ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>"
                        target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                            data-title="Manual Add/Edit" >
                            <i class="icon-user"></i></a>
                    <?php endif;?>

                    <a href="<?php echo $cfg->getUrl();
                    ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>&layout=passport_upload"
                        target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                            data-title="Scan Passport" >
                                <i class="icon-camera"></i></a>

                    <a href="<?php echo $cfg->getUrl();
                    ?>scp/list-passport-upload.php?tour_id=<?php echo $row['id'] ?>"
                        target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                            data-title="Passport chờ quét" >
                                <i class="icon-list"></i></a>
                </td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>
<?php
if($count): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>

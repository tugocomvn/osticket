CREATE TABLE `ost_tour_history` (
  `id` int AUTO_INCREMENT primary key,
  `ost_tour_history_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ost_tour ADD log_id_ost_tour int(10);
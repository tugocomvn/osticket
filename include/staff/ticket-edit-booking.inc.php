<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canEditBookings() || !$ticket) die('Access Denied');
$info=Format::htmlchars(($errors && $_POST)?$_POST:$ticket->getUpdateInfo());
if ($_POST)
    $info['duedate'] = Format::date($cfg->getDateFormat(),
       strtotime($info['duedate']));
?>

<?php
$booking_code = Booking::fromTicketId($ticket->getId());
$points = UserPoint::fromBookingCode($booking_code);
?>

<form action="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-booking" method="post" id="save"  enctype="multipart/form-data" class="repeater">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="update">
 <input type="hidden" name="a" value="edit-booking">
 <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
 <h2><?php echo sprintf(__('Update Ticket #%s'),$ticket->getNumber());?></h2>
 <table class="form_table" width="1058" border="0" cellspacing="0" cellpadding="2">
    <tbody>
        <tr>
            <th colspan="2">
            <em><strong><?php echo __('Ticket Information'); ?></strong>: <?php echo __("Due date overrides SLA's grace period."); ?></em>
            </th>
        </tr>
        <tr>
            <td width="160" class="required">
                <?php echo __('Help Topic');?>:
            </td>
            <td>
                <select disabled="disabled">
                    <option value="" selected >&mdash; <?php echo __('Select Help Topic');?> &mdash;</option>
                    <?php
                    $this_help_id = null;
                    if($topics=Topic::getHelpTopics()) {
                        foreach($topics as $id =>$name) {
                            if (!in_array($id, [BOOKING_TOPIC])) continue;
                            if ($info['topicId']==$id) $this_help_id = $id;
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['topicId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                <input type="hidden" name="topicId" value="<?php echo $this_help_id ?>">
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>
        </tr>
    </tbody>
</table>
<table class="form_table dynamic-forms" width="1058" border="0" cellspacing="0" cellpadding="2">
        <?php
        if ($forms) {
            $this_topic = $ticket->getTopic();
            foreach ($forms as $form) {
                if (!$form || !$form->getForm()) continue;

                if ($this_topic->getFormId() == $form->getForm()->get('id'))
                    $form->render(
                        true,
                        false,
                        array('mode'=>'edit','width'=>160,'entry'=>$form, 'disabled' => ['booking_code'], 'booking' => 1)
                    );
            }
        } ?>

    <tbody data-repeater-list="group-loyalty">
        <?php if (isset($points) && $points): ?>
            <?php foreach ($points as $point): ?>
            <?php
                $customer_numner = '';
                $loyalty_user = \Tugo\User::get(['uuid' => $point->user_uuid]);
                if ($loyalty_user)
                    $customer_numner = $loyalty_user['customer_number'];
            ?>
            <tr data-repeater-item>
                <td></td>
                <td>
                    <label for="">Mã số khách hàng</label> <input type="text" readonly="readonly" class="input-field" name="customer-number" value="<?php echo $customer_numner ?>"/>
                    <label for="">Loại</label> <select name="type" id="type" disabled>
                        <option value="+" <?php if ($point->change_point > 0) echo "selected" ?>>Tích điểm</option>
                        <option value="-" <?php if ($point->change_point < 0) echo "selected" ?>>Sử dụng điểm</option>
                    </select>
                    <label for="">Số điểm</label> <input type="text" readonly="readonly" class="input-field" name="point" value="<?php echo $point->change_point ?>"/>
                    <label for="">Ghi Chú</label> <input type="text" readonly="readonly" class="input-field" name="note" value="<?php echo $point->note ?>"/>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>

    <tbody>
    <tr>
        <td><strong>Tổng giá bán: </strong></td>
        <td><span class="total_retailprice"></span></td>
    </tr>
    <tr>
        <td><strong>Tổng giá net: </strong></td>
        <td><span class="total_netprice"></span></td>
    </tr>
    <tr>
        <td><strong>Tổng lợi nhuận: </strong></td>
        <td><span class="total_profit"></span></td>
    </tr>
    </tbody>
</table>

<p style="padding-left:250px;">
    <input type="submit" name="submit" class="btn_sm btn-primary" value="<?php echo __('Save');?>">
    <input type="reset"  name="reset"  class="btn_sm btn-default" value="<?php echo __('Reset');?>">
    <input type="button" name="cancel" class="btn_sm btn-danger" value="<?php echo __('Cancel');?>" onclick='window.location.href="tickets.php?id=<?php echo $ticket->getId(); ?>"'>
</p>
</form>
<div style="display:none;" class="dialog draggable" id="user-lookup">
    <div class="body"></div>
</div>

<script type="text/javascript">
(function($) {
    booking_calc_action();
    setTimeout(booking_calc, 1000);
})(jQuery);
</script>

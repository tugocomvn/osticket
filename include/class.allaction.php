<?php

class AllAction {

    protected $id;
    protected $source_id, $source_name, $action_name, $info;

    function AllAction($id = 0){
        $this->id=0;
        return $this->load($id);
    }

    function load($id = 0){

        $sql='SELECT * FROM '.ALL_ACTION_TABLE.' WHERE id='.db_input($id);
        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->info=db_fetch_array($res);
        $this->id=$this->info['id'];

        return $this->id;
    }

    function log($source_id, $source_name, $action_name, $info) {

        $ip = tugo_get_ip_address();

        $_info = array(
            'source_id' => $source_id,
            'source_name' => $source_name,
            'action_name' => $action_name,
            'info' => $info,
            'ip' => $ip,
        );
        Signal::send('log.all_action', null, $_info);

        //Save log based on system log level settings.
        $sql = sprintf('INSERT INTO %s SET
                source_id = %s,
                source_name = %s,
                action_name = %s,
                info = %s,
                ip = %s,
                action_time = NOW(),
                action_time_unix = UNIX_TIMESTAMP(NOW())',
            ALL_ACTION_TABLE,
            db_input($source_id),
            db_input($source_name),
            db_input($action_name),
            db_input($info),
            db_input($ip)
        );
        db_query($sql, false);

        return true;
    }

    function autoLog($obj) {
        $obj->save();
    }

    function create($data) {
        $action = new AllAction();
        $action->source_id = $data['source_id'];
        $action->source_name = $data['source_name'];
        $action->action_name = $data['action_name'];
        $action->info = $data['info'];
        return $action;
    }

    function save() {
        $this->log($this->source_id, $this->source_name, $this->action_name, $this->info);
    }

    function reload(){
        return $this->load($this->getId());
    }

    function getId(){
        return $this->id;
    }

    /*** static function ***/
    function lookup($id){
        return ($id && is_numeric($id) && ($l= new AllAction($id)) && $l->getId()==$id)?$l:null;
    }
}
?>

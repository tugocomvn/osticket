<?php
require('staff.inc.php');
require_once INCLUDE_DIR.'class.passport.php';
require_once INCLUDE_DIR.'class.pax.php';

function getNamePax($string_passport){
    if($string_passport === null || $string_passport === '')
        return '';

    $fullname_start = 5;
    $block_text_no_space = str_replace(' ', '', $string_passport);
    $block_text_no_space = str_replace('«', '<<', $block_text_no_space);

    //echo $block_text_no_space;
    $_check = '<<<<';
    $fullname_end = strpos($block_text_no_space, $_check);
    $firtname_start = strpos($block_text_no_space, '<<');
    $firstname_string = substr($block_text_no_space, $firtname_start, $fullname_end-$firtname_start);
    $firstname_string = trim(str_replace('<', ' ', $firstname_string));
    //echo $firtname_string;exit;

    $last_name_string = substr($block_text_no_space, $fullname_start, $firtname_start-$fullname_start);
    $last_name_string = trim(str_replace('<', ' ', $last_name_string));
    //echo $first_name_string;exit;
    return $last_name_string.' '.$firstname_string;
}

function getPassportNo($string_passport){
    if($string_passport === null || $string_passport === '')
        return '';

    $block_text_no_space = str_replace(' ', '', $string_passport);
    $block_text_no_space = str_replace('«', '<<', $block_text_no_space);

    $check_ = '<<<<';
    $passport_start = strrpos($block_text_no_space, $check_, -20);
    $passport_end = strpos($block_text_no_space, '<', $passport_start+strlen($check_));
    $passport_string = substr($block_text_no_space, $passport_start+strlen($check_), $passport_end-$passport_start-strlen($check_));
    $passport_string = trim(str_replace('<', ' ', $passport_string));
    $passport_string = str_replace('O', '0', $passport_string);
    return $passport_string;
}

function convertFileToBas64($path){
    if(!file_exists($path))
        return null;

    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

$tour_name = $passport_code = $booking_code = $staff  =  null;
$qs = [];
$mobile = false;
if(isset($_REQUEST['tour_name'])){
    $tour_name = trim($_REQUEST['tour_name']);
    $qs += [ 'tour_name' => trim($_REQUEST['tour_name']) ];
}

if(isset($_REQUEST['passport_code'])){
    $passport_code = trim($_REQUEST['passport_code']);
    $qs += [ 'passport_code' => trim($_REQUEST['passport_code']) ];
}

if(isset($_REQUEST['booking_code'])){
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += [ 'booking_code' => trim($_REQUEST['booking_code']) ];
}

if(isset($_REQUEST['staff']) && $_REQUEST['staff']){
    $staff = trim($_REQUEST['staff']);
    $qs += [ 'staff' => trim($_REQUEST['staff']) ];
}

if(isset($_REQUEST['tour_id']) && $_REQUEST['tour_id']){
    $tour_id = (int)$_REQUEST['tour_id'];
    $qs += [ 'tour_id' => (int)$_REQUEST['tour_id'] ];
}

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):null;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):null;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere .= " AND DATE(`created`) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere .=" AND DATE(`created`) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}

$searchPassportUpload = array(
    'startTime'     => $startTime,
    'endTime'       => $endTime,
    'tour_name'     => $tour_name,
    'booking_code'  => $booking_code,
    'passport_code' => $passport_code,
    'staff'         => $staff,
    'tour_id'       => $tour_id,
);
$allStaff = Staff::getAll();
$listStaff = [];
while ($allStaff && ($row = db_fetch_array($allStaff)))
{
    $listStaff[$row['staff_id']] = $row['username'];
}

if(isset($_REQUEST['action']) && $_REQUEST['action'] === 'view_image' && isset($_REQUEST['passport_id']) && $_REQUEST['passport_id']){
    $searchPassportUpload['passport_id'] = (int)$_REQUEST['passport_id'];
    $total = 0;
    $result = \Tugo\PassportPhoto::getPassportUpload($searchPassportUpload,0,1,$total);
    $result = db_fetch_array($result);
    $dataImage = null;
    if($result['filename'] !== null) {
        $name_image = $result['filename'];
        $path = '../passport_upload/' . $name_image;
        $dataImage = convertFileToBas64($path);
    }
    $page = 'view-passport-upload.inc.php';

}elseif ($_REQUEST['action'] === 'reupload_passport_photo' && $_REQUEST['tour_id'] && $_REQUEST['passport_id']){
    $tour = TourNew::lookup(((int)$_REQUEST['tour_id']));
    $passport = \Tugo\PassportPhoto::lookup((int)$_REQUEST['passport_id']);
    $mobile = true;
    $page = 'passport-reupload.inc.php';
}
else
{
    $total = 0;
    $p = isset($_REQUEST['p']) && intval($_REQUEST['p']) ? intval($_REQUEST['p']) : 1;
    $offset = ($p - 1) * PAGE_LIMIT;
    $results = \Tugo\PassportPhoto::getPassportUpload($searchPassportUpload, $offset, PAGE_LIMIT, $total);
    $pageNav = new Pagenate($total, $p, PAGE_LIMIT);
    $pageNav->setURL('list-passport-upload.php', $qs ?: null);
    $showing = $pageNav->showing() . ' ' . _N('guest', 'Passport', $total);
    $page = "list-passport-upload.inc.php";

}

$nav->setTabActive('visa');
//$nav->setActiveSubMenu(5,'visa');
$ost->addExtraHeader('<meta name="tip-namespace" content="visa.visa" />',
    "$('#content').data('tipNamespace', 'visa.list-passport-upload');");
if (!$mobile)
    require(STAFFINC_DIR.'header.inc.php');
else
    require(STAFFINC_DIR.'header.mobile.inc.php');

require(STAFFINC_DIR.$page);

if (!$mobile)
    include(STAFFINC_DIR.'footer.inc.php');
else
    include(STAFFINC_DIR.'footer.mobile.inc.php');
//require(STAFFINC_DIR.'header.inc.php');
//require(STAFFINC_DIR.$page);
//include(STAFFINC_DIR.'footer.inc.php');

UPDATE ost_pax_info set nationality = 0 WHERE nationality IS NULL OR nationality LIKE '';

ALTER TABLE ost_pax_info MODIFY nationality TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0: VN, 1: other';
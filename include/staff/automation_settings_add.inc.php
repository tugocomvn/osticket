<link rel="stylesheet" type="text/css" href="<?php echo $cfg->getUrl() ?>scp/css/selectize.css" />
<style>
    ._row {
        width: 100%;
        display: block;
        clear: both;
    }

    ._row:before,
    ._row:after {
        clear: both;
        display: block;
    }

    ._col {
        display: inline-block;
        float: left;
        padding: 2%;
        width: 48%;
    }

    ._row ._col:first-child {
        padding-left: 0;
    }

    ._row ._col:last-child {
        padding-right: 0;
    }

    .outer_table,
    .inner_table,
    .inner_table table {
        width: 100%;
    }

    .inner_table textarea {
        width: 95%;
    }

    input.dp {
        width: 6em;
    }

    textarea {
        resize: none;
    }

    input[name*="title"] {
        width: 95%;
    }

    .outer_table > tbody input,
    .outer_table > tbody select {
        width: 50%;
    }


    .outer_table > tbody textarea {
        width: 80%;
        height: 6em;
    }

    .outer_table > tbody input,
    .outer_table > tbody select,
    .outer_table > tbody textarea {
        margin-left: 12.5%;
    }

    .outer_table > tbody tr td {
        padding-top: 1em;
    }

    .outer_table tr td:first-child {
        text-align: right;
        width: 20%;
    }

    .tab-pane input,
    .tab-pane textarea {
        width: 95%;
    }

    .inner_table table td {
        text-align: center;
    }

    .inner_table table input,
    .inner_table table select {
        margin-right: 0.5em;
        margin-left: 0.5em;
    }

    .inner_table table caption {
        font-weight: bold;
        font-size: 14px;
    }

    th {
        border-top: 1px solid #ccc;
        padding: 0.5em;
        font-size: 16px;
    }

    iframe {
        border: none;
        width: 95%;
        height: 150px;
    }

    select[multiple] {
        height: 10em;
    }

    .tab-pane table {
        width: 100%;
    }
</style>

<a href="<?php $cfg->getUrl() ?>scp/automation_settings.php"
   class="btn_sm btn-default">Back</a>
<div class="clearfix"></div>
<h2>Automation Config <small>Add New</small></h2>
<div class="clearfix"></div>
<form action="<?php echo $cfg->getUrl() ?>scp/automation_settings.php" method="post">
    <?php csrf_token() ?>
    <input type="hidden" name="id"
           value="<?php if (isset($auto) && $auto && isset($auto->id))
               echo (int)trim($auto->id) ?>">
    <input type="hidden" name="action" value="save">
    <div class="_row">
        <div class="_col">
            <table class="outer_table">
                <tr>
                    <td><strong>Name</strong></td>
                    <td>
                        <input type="text" name="name" class="input-field" required placeholder="Name"
                               value="<?php if (isset($auto) && $auto && isset($auto->name))
                                   echo trim($auto->name) ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Description</strong></td>
                    <td>
                <textarea name="description" class="input-field"
                          placeholder="Description" required
                ><?php
                    if (isset($auto) && $auto && isset($auto->description))
                        echo trim($auto->description) ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td><strong>Send after (minutes)</strong></td>
                    <td>
                        <input type="number" min="2" step="1" name="send_after" placeholder="Send after (minutes)"
                               class="input-field" required value="<?php
                        if (isset($auto) && $auto && isset($auto->send_after))
                            echo (int)trim($auto->send_after) ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Trigger</strong></td>
                    <td>
                        <select name="trigger_id" id="" class="input-field" required>
                            <option value=>- Select -</option>
                            <?php foreach ($trigger_list as $key => $value): ?>
                                <option value="<?php echo $key ?>"
                                    <?php if (isset($auto) && $auto
                                        && isset($auto->trigger_id) && $auto->trigger_id == $key)
                                        echo "selected" ?>
                                ><?php echo $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td><strong>Status</strong></td>
                    <td>
                        <select name="status" id="status" class="input-field type" required>
                            <option <?php if ($auto && isset($auto->status) && $auto->status == "1") echo "selected" ?>
                                    value="1">Enable</option>
                            <option <?php if ($auto && isset($auto->status) && $auto->status == "0") echo "selected" ?>
                                    value="0">Disable</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>Type</strong></td>
                    <td>
                        <select name="type" id="type" class="input-field type" required>
                            <option value=>- Select -</option>
                            <option
                                <?php if ($auto && isset($auto->type) && $auto->type === "sms") echo "selected" ?>
                                    value="sms">SMS</option>
                            <option
                                <?php if ($auto && isset($auto->type) && $auto->type === "email") echo "selected" ?>
                                    value="email">Email</option>
                            <option
                                <?php if ($auto && isset($auto->type) && $auto->type === "notification") echo "selected" ?>
                                    value="notification">Notification</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th colspan="2">UTM Tags</th>
                </tr>
                <tr>
                    <td><strong>Campaign</strong></td>
                    <td>
                        <select name="campaign"  required>
                            <?php if(isset($utm) && is_array($utm['campaign'])): ?>
                                <?php foreach($utm['campaign'] as $_id => $value): ?>
                                    <option
                                        <?php if ($auto && isset($auto->aac_campaign_id) && $auto->aac_campaign_id === $_id)
                                            echo "selected" ?>
                                            value="<?php echo trim($value) ?>"><?php echo trim($value) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td><strong>Source</strong></td>
                    <td>
                        <select name="utm_source"  required>
                            <?php if(isset($utm) && is_array($utm['source'])): ?>
                                <?php foreach($utm['source'] as $_id => $value): ?>
                                    <option
                                        <?php if ($auto && isset($auto->aac_utm_source_id) && $auto->aac_utm_source_id === $_id)
                                            echo "selected" ?>
                                            value="<?php echo trim($value) ?>"><?php echo trim($value) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>Medium</strong></td>
                    <td>
                        <select name="utm_medium" required>
                            <?php if (isset($utm) && is_array($utm['medium'])): ?>
                                <?php foreach ($utm['medium'] as $_id => $value): ?>
                                    <option
                                        <?php if ($auto && isset($auto->aac_utm_medium_id) && $auto->aac_utm_medium_id === $_id)
                                            echo "selected" ?>
                                            value="<?php echo trim($value) ?>"><?php echo trim($value) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Term</td>
                    <td>
                        <select name="utm_term">
                            <?php if (isset($utm) && is_array($utm['term'])): ?>
                                <?php foreach ($utm['term'] as $_id => $value): ?>
                                    <option
                                        <?php if ($auto && isset($auto->aac_utm_term_id) && $auto->aac_utm_term_id === $_id)
                                            echo "selected" ?>
                                            value="<?php echo trim($value) ?>"><?php echo trim($value) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><strong>Content</strong></td>
                    <td>
                        <select name="utm_content" required>
                            <?php if (isset($utm) && is_array($utm['content'])): ?>
                                <?php foreach ($utm['content'] as $_id => $value): ?>
                                    <option
                                        <?php if ($auto && isset($auto->aac_utm_content_id) && $auto->aac_utm_content_id === $_id)
                                            echo "selected" ?>
                                            value="<?php echo trim($value) ?>"><?php echo trim($value) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th colspan="2"><strong>Schedule</strong></th>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <select name="schedule" id="schedule" class="input-field schedule">
                            <option <?php if (isset($auto) && $auto && isset($auto->schedule)
                                && $auto->schedule === "now") echo "selected" ?>
                                    value="now">Now
                            </option>
                            <option <?php if (isset($auto) && $auto && isset($auto->schedule)
                                && $auto->schedule === "one_time") echo "selected" ?>
                                    value="one_time">One Time
                            </option>
                            <option <?php if (isset($auto) && $auto && isset($auto->schedule)
                                && $auto->schedule === "recursion") echo "selected" ?>
                                    value="recursion">Recursion
                            </option>
                        </select>
                    </td>
                </tr>


            </table>
            <table class="inner_table">
                <tbody>

                <tr class="schedule_row">
                    <td <?php if($auto && isset($auto->schedule) && $auto->schedule !== 'one_time'): ?>
                        style="display: none;"
                    <?php endif; ?> class="one_time">
                        <input type="text" class="dp input-field" name="run_one_time_date"
                               value="<?php if (isset($auto) && $auto
                                   && isset($auto->send_one_time_at)
                                   && ($one_time_date = date_create_from_format('Y-m-d H:i:s', $auto->send_one_time_at))
                               ) echo $one_time_date->format('d/m/Y')
                               ?>"> @
                        <select name="run_one_time_hour" id="">
                            <?php for ($i = 0; $i <= 23; $i++): ?>
                                <option value="<?php echo $i ?>"
                                    <?php if (isset($one_time_date) && $one_time_date
                                        && $one_time_date->format('H') == $i) echo "selected" ?>
                                ><?php echo sprintf("%02d", $i) ?></option>
                            <?php endfor; ?>
                        </select>
                        :
                        <select name="run_one_time_minute" id="">
                            <?php for ($i = 0; $i <= 59; $i++): ?>
                                <option value="<?php echo $i ?>"
                                    <?php if (isset($one_time_date) && $one_time_date
                                        && $one_time_date->format('i') == $i) echo "selected" ?>
                                ><?php echo sprintf("%02d", $i) ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>
                    <td <?php if($auto && isset($auto->schedule) && $auto->schedule !== 'recursion'): ?>
                        style="display: none;"
                    <?php endif; ?> class="recursion">
                        <table>
                            <tr>
                                <td>Hours</td>
                                <td>Mins</td>
                                <td>Dates</td>
                                <td>Months</td>
                                <td>Days of Week</td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="recursion_hour[]" id="" multiple>
                                        <option value="-1"
                                            <?php if (isset($auto) && $auto
                                                && isset($auto->recursion_hour)
                                                && ($tmp = explode(',', $auto->recursion_hour))
                                                && ($tmp[0] == '-1' || empty($tmp[0]))) echo "selected" ?>
                                        >- Any -
                                        </option>
                                        <?php for ($i = 0; $i <= 23; $i++): ?>
                                            <option value="<?php echo $i ?>"
                                                <?php if (isset($auto) && $auto
                                                    && isset($auto->recursion_hour)
                                                    && ($tmp = explode(',', $auto->recursion_hour))
                                                    && in_array($i, $tmp)) echo "selected" ?>
                                            ><?php echo sprintf("%02d", $i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="recursion_minute[]" id="" multiple>
                                        <?php for ($i = 0; $i <= 59; $i += 5): ?>
                                            <option value="<?php echo $i ?>"
                                                <?php if (isset($auto) && $auto
                                                    && isset($auto->recursion_minute)
                                                    && ($tmp = explode(',', $auto->recursion_minute))
                                                    && ($tmp[0] == '-1' || empty($tmp[0]) || in_array($i, $tmp))
                                                ) echo "selected" ?>
                                            ><?php echo sprintf("%02d", $i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="recursion_date_in_month[]" id="" multiple>
                                        <option value="-1"
                                            <?php if (isset($auto) && $auto
                                                && isset($auto->recursion_date_in_month)
                                                && ($tmp = explode(',', $auto->recursion_date_in_month))
                                                && ($tmp[0] == '-1' || empty($tmp[0]))) echo "selected" ?>
                                        >- Any -
                                        </option>
                                        <?php for ($i = 1; $i <= 31; $i++): ?>
                                            <option value="<?php echo $i ?>"
                                                <?php if (isset($auto) && $auto
                                                    && isset($auto->recursion_date_in_month)
                                                    && ($tmp = explode(',', $auto->recursion_date_in_month))
                                                    && in_array($i, $tmp)) echo "selected" ?>
                                            ><?php echo sprintf("%02d", $i) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="recursion_month[]" id="" multiple>
                                        <option value="-1"
                                            <?php if (isset($auto) && $auto
                                                && isset($auto->recursion_month)
                                                && ($tmp = explode(',', $auto->recursion_month))
                                                && ($tmp[0] == '-1' || empty($tmp[0]))) echo "selected" ?>
                                        >- Any -
                                        </option>
                                        <?php for ($i = 1; $i <= 12; $i++): ?>
                                            <option value="<?php echo $i ?>"
                                                <?php if (isset($auto) && $auto
                                                    && isset($auto->recursion_month)
                                                    && ($tmp = explode(',', $auto->recursion_month))
                                                    && in_array($i, $tmp)) echo "selected" ?>
                                            ><?php echo date("M", strtotime("January +{$i} months")) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="recursion_day_of_week[]" id="" multiple>
                                        <option value="-1"
                                            <?php if (isset($auto) && $auto
                                                && isset($auto->recursion_day_of_week)
                                                && ($tmp = explode(',', $auto->recursion_day_of_week))
                                                && ($tmp[0] == '-1' || empty($tmp[0]))) echo "selected" ?>
                                        >- Any -
                                        </option>
                                        <?php for ($i = 0; $i <= 6; $i++): ?>
                                            <option value="<?php echo $i ?>"
                                                <?php if (isset($auto) && $auto
                                                    && isset($auto->recursion_day_of_week)
                                                    && ($tmp = explode(',', $auto->recursion_day_of_week))
                                                    && in_array($i, $tmp)) echo "selected" ?>
                                            ><?php echo date('D', strtotime("Sunday +{$i} days")) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="5"><span class="recursion_text"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="_col">

            <h2>Content</h2>
            <div <?php if($auto && isset($auto->type) && $auto->type !== 'sms'): ?>
                style="display: none;"
            <?php endif; ?> class="tab-pane active" id="sms_pane">
                <table class="inner_table">
                    <tbody>
                    <tr class="">
                        <th>SMS</th>
                    </tr>
                    <?php $_automation_name = 'sms' ?>
                    <tr>
                        <td>
                    <textarea name="sms_content" id="sms_content" class="input-field"
                              cols="30" rows="3" placeholder="SMS Content"
                    ><?php if (isset($auto) && $auto && isset($auto->sms_content))
                            echo trim($auto->sms_content) ?></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div <?php if($auto && isset($auto->type) && $auto->type !== 'email'): ?>
                style="display: none;"
            <?php endif; ?> class="tab-pane" id="email_pane">
                <table>
                    <tbody>
                    <tr class="">
                        <th>EMAIL</th>
                    </tr>
                    <?php $_automation_name = 'email' ?>
                    <tr>
                        <td>
                            <input name="email_subject" id="email_subject" class="input-field"
                                   value="<?php if (isset($auto) && $auto && isset($auto->email_subject))
                                       echo trim($auto->email_subject) ?>"
                                   placeholder="Email Subject"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                    <textarea name="email_content" id="email_content" class="input-field richtext"
                              cols="30" rows="10" placeholder="Email Content"
                    ><?php if (isset($auto) && $auto && isset($auto->email_content))
                            echo trim($auto->email_content) ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Attachment:
                            <input name="email_attachment" id="email_attachment" class=""
                                   type="file" placeholder="Email Attachment"
                            />
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div <?php if($auto && isset($auto->type) && $auto->type !== 'notification'): ?>
                style="display: none;"
            <?php endif; ?> class="tab-pane" id="notification_pane">
                <table>
                    <tbody>
                    <tr class="">
                        <th>NOTIFICATION</th>
                    </tr>
                    <?php $_automation_name = 'notification' ?>
                    <tr>
                        <td>
                            <input name="notification_title" id="notification_title" class="input-field"
                                   value="<?php if (isset($auto) && $auto && isset($auto->notification_title))
                                       echo trim($auto->notification_title) ?>"
                                   placeholder="Title"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="notification_content" id="notification_content" class="input-field"
                                      cols="30" rows="3" placeholder="Content"
                            ><?php if (isset($auto) && $auto && isset($auto->notification_content))
                                    echo trim($auto->notification_content) ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="notification_url" id="notification_url" class="input-field"
                                      cols="30" rows="3" placeholder="URL"
                            ><?php if (isset($auto) && $auto && isset($auto->notification_url))
                                    echo trim($auto->notification_url) ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="notification_topic" id="notification_topic" class="input-field"
                                      cols="30" rows="3" placeholder="Topic"
                            ><?php if (isset($auto) && $auto && isset($auto->notification_topic))
                                    echo trim($auto->notification_topic) ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Photo:
                            <input name="notification_photo" id="notification_photo" class=""
                                   type="file" placeholder="Notification Photo"
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div>
                <p>Available PARAMS</p>
                <ul>
                    <li>STAFF_NAME_ASSIGN: nhân viên xử lý ticket</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <p class="centered">
        <a href="<?php $cfg->getUrl() ?>scp/automation_settings.php" class="btn_sm btn-default">Cancel</a>
        <button class="btn_sm btn-primary">Save</button>
    </p>
    <iframe src="" name="save_frame" frameborder="0"></iframe>
</form>

<script>
    function _getSelectedText(i, element) {
        return jQuery(element).text();
    }

    function _showText(e) {
        _self = $(e.target).parents('.recursion');

        text = "";
        text += ' Hrs: ';

        hrs = _self.find('[name*="_send_recursion_hour"] :selected').map(_getSelectedText).get();
        text += (hrs ? hrs.join(',') : " * ")
            + ' <br> Mins: ';

        mins = _self.find('[name*="_send_recursion_minute"] :selected').map(_getSelectedText).get();
        text += (mins ? mins.join(',') : " * ")
            + ' <br> Dates on Month: ';

        dom = _self.find('[name*="_send_recursion_date_in_month"] :selected').map(_getSelectedText).get();
        text += (dom ? dom.join(',') : " * ")
            + ' <br> Months: ';

        mon = _self.find('[name*="_send_recursion_month"] :selected').map(_getSelectedText).get();
        text += (mon ? mon.join(',') : " * ")
            + ' <br> Days of Week: ';

        dow = _self.find('[name*="_send_recursion_day_of_week"] :selected').map(_getSelectedText).get();
        text += (dow ? dow.join(',') : " * ");

        _self.parents('.inner_table').find('.recursion_text').html(text);
    }

    function show_schedule(e) {
        _self = $(e.target);
        if (!_self.hasClass('schedule')) return false;
        schedule = _self.find('option:selected').val();
        if (!schedule) return false;
        $('.schedule_row>td').hide();
        $('.schedule_row>td.' + schedule).show();
    }

    function show_content_type(e) {
        _self = $(e.target);
        if (!_self.hasClass('type')) return false;
        type = _self.find('option:selected').val();
        if (!type) return false;
        $('[id*="_pane"]').hide();
        $('#' + type + '_pane').show();
    }

    (function ($) {
        $('.recursion select').off('click, change, blur').on('change', _showText);
        $('#schedule').off('change').on('change', show_schedule);
        $('#type').off('change').on('change', show_content_type);
    })(jQuery);


</script>
<script type="text/javascript" src="<?php echo $cfg->getUrl() ?>scp/js/standalone/selectize.min.js"></script>
<script>
    $(function() {
        options = {
            create: true,
            sortField: 'text'
        };
        $('[name*="utm"], [name="campaign"]').selectize(options);
    });
</script>

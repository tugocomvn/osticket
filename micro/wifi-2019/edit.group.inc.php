<html lang="vi">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" minimum-scale="600px">
    <meta charset="utf8">
    <title>Group</title>
</head>
<style type="text/css" media="screen">
    td.title {
        width: 180px;
    }

    tr {
        height: 45px;
    }

    .error {
        color: red;
    }
    .center-parent {
        text-align: center;
    }
</style>
<body>
<div class="container bg-light rounded" style="width: 80%">
    <?php include_once __DIR__ . '/header.php'; ?>
    <?php if (isset($mess) && isset($error)): ?>
        <?php if (!$error): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo $mess ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php else: ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo $mess ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

    <?php endif; ?>
    <form action="edit.group.php?id=<?php echo $_REQUEST['id']; ?>" method="post">
        <?php if (!empty($group)): ?>
            <h3 class="mt-3 text-center text-info">Chỉnh sửa nhóm <?php echo $group['name'] ?></h3>
            <table style="margin-left: 25%; margin-top: 10px">
                <tr>
                    <td class="title">Tên nhóm<span class="error">*&nbsp;</span></td>
                    <td>
                        <input type="text" size="40" class="form-control col-9 border border-info rounded"
                               name="name" value="<?php echo $group['name'] ?>" required="true"/>
                    </td>
                </tr>

                <tr>
                    <td>Nội dung</td>
                    <td>
                        <input type="text" size="40" class="form-control col-9 border border-info rounded"
                               name="content" value="<?php echo $group['content'] ?>" >
                    </td>
                </tr>

                <tr>
                    <td width="180">Bắt đầu<span class="error">*&nbsp;</span></td>
                    <td>
                        <input class="form-control col-9" type="date" name="date_start"
                               value="<?php if($group['date_start']) echo $group['date_start'] ?>" required="true">
                    </td>
                </tr>

                <tr>
                    <td>Kết thúc<span class="error">*&nbsp;</span></td>
                    <td>
                        <input class="form-control col-9" type="date" size="28" name="date_end"
                               value="<?php if($group['date_end']) echo $group['date_end'] ?>" required="true">

                    </td>
                </tr>
                <tr>
                    <td>Ngày sửa</td>
                    <td>
                        <span><?php if(isset($group['date_edit']) && $group['date_edit']) echo date("d/m/Y H:i:s", strtotime($group['date_edit'])) ?></span>
                    </td>
                </tr>

                <tr>
                    <td>Ngày tạo</td>
                    <td>
                        <span><?php if(isset($group['date_create']) && $group['date_create']) echo date("d/m/Y H:i:s", strtotime($group['date_create'])) ?></span>
                    </td>
                </tr>

                <tr>
                    <td>Campaign ID</td>
                    <td>
                        <span><?php echo $group['campaignId'] ?></span>
                    </td>
                </tr>
            </table>
            <div class="center-parent">
                <a href="groups.list.php" class="btn btn-primary centered">Cancel</a>
                <button class="btn btn-primary centered" name="edit" value="edit">Save</button>
            </div>
        <?php endif; ?>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
<?php
namespace BI;
require_once(INCLUDE_DIR . 'class.orm.php');

class BookingCountModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_COUNT_TABLE,
        'pk' => ['booking_code'],
    ];
}

class BookingCount extends BookingCountModel {

}

class BookingReturnModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_RETURN_TABLE,
        'pk' => ['booking_code'],
    ];
}

class BookingReturn extends BookingReturnModel {

}

class BookingFollowModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_FOLLOW_TABLE,
        'pk' => ['booking_code'],
    ];
}

class BookingFollow extends BookingFollowModel {

}

class BookingFollowActionModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_FOLLOW_ACTION_TABLE,
        'pk' => ['id'],
    ];
}

class BookingFollowAction extends BookingFollowActionModel {

}


class BookingAnalyticsModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_ANALYTICS_TABLE,
        'pk' => ['date', 'source', 'type', 'status'],
    ];
}

class BookingAnalytics extends BookingAnalyticsModel {
    public static function analytics($from, $to) {

    }

    public static function overview($from, $to) {

    }

    public static function type($from, $to) { // destination

    }

    public static function source($from, $to) {

    }

    public static function status($from, $to) {

    }
}

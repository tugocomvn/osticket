<form action="#bookings/<?php echo $booking_id ?>/note" method="post">
    <?php csrf_token(); ?>
    <input type="hidden" name="booking_ticket_id" value="<?php echo $booking_id ?>">
    <h3>Note for booking</h3>
    <p>
        <textarea class="booking-note input-field" name="note_content" id="note_content" cols="30" rows="10"></textarea>
    </p>
    <p class="centered"><button class="btn_sm btn-primary">Save</button> <button type="button" class="close btn_sm btn-default">Cancel</button></p>
</form>
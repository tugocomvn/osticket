alter table tugo_osticket_customer
	add date_value date null;

create index tugo_osticket_customer_date_value_index
	on tugo_osticket_customer (date_value);

update tugo_osticket_customer
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;
alter table tugo_osticket_customer_age
	add date_value date null;

create index tugo_osticket_customer_age_date_value_index
	on tugo_osticket_customer_age (date_value);

update tugo_osticket_customer_age
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;
alter table tugo_osticket_customer_age
	add year_of_birth int null;

alter table tugo_osticket_customer_age change year_of_birth dob_year int null;
alter table tugo_osticket_booking
	add date_value date null;

update tugo_osticket_booking
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;
alter table tugo_osticket_ticket
	add date_value date null;

create index tugo_osticket_ticket_date_value_index
	on tugo_osticket_ticket (date_value);

create index tugo_osticket_booking_date_value_index
	on tugo_osticket_booking (date_value);

update tugo_osticket_ticket
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

alter table tugo_osticket_call drop key index2;

alter table tugo_osticket_call
	add date_value date null;

create index tugo_osticket_call_date_value_index
	on tugo_osticket_call (date_value);

update tugo_osticket_call
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;


alter table tugo_osticket_sms
	add date_value date null;

create index tugo_osticket_sms_date_value_index
	on tugo_osticket_sms (date_value);

alter table tugo_osticket_sms drop key index2;

update tugo_osticket_sms
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

alter table tugo_osticket_customer drop key tugo_osticket_xxid_uindex;

alter table tugo_osticket_customer_age drop key index2;

alter table tugo_osticket_booking drop key index2;

alter table tugo_osticket_sms modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_sms modify week_of_year varchar(6) null comment 'YEAR_WEEK';


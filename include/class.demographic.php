<?php
namespace Tugo;
require_once(INCLUDE_DIR . 'class.orm.php');
require_once INCLUDE_DIR."../vendor/autoload.php";

//----------class AgeGenderPax-------------
class AgeGenderPaxModel extends \VerySimpleModel
{
    static $meta = [
        'table' => OST_PAX_TABLE,
        'pk' => ['id'],
    ];
}
class AgeGenderPax extends AgeGenderPaxModel
{
    public static function countPaxAgeGroupInTour(){
        $sql = "SELECT 
                    SUM(CASE WHEN DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365 BETWEEN 0 AND 25 THEN 1 ELSE 0 END) as `0_25`, 
                    SUM(CASE WHEN DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365 BETWEEN 25 AND 35 THEN 1 ELSE 0 END) as `25_35`, 
                    SUM(CASE WHEN DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365 BETWEEN 35 AND 55 THEN 1 ELSE 0 END) as `35_55`, 
                    SUM(CASE WHEN DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365 > 55 THEN 1 ELSE 0 END) as `55+`, 
                    DATE(ost_tour.gather_date) as `gather_date` 
                FROM ost_pax_tour JOIN ost_tour ON ost_tour.id = ost_pax_tour.tour_id 
                JOIN ost_pax ON ost_pax_tour.pax_id = ost_pax.id 
                WHERE ost_tour.gather_date IS NOT NULL and DATEDIFF(CURRENT_DATE(),ost_tour.gather_date) < ".DAY_LIMIT." 
                GROUP BY DATE(ost_tour.gather_date)";
        return db_query($sql);
    }
    public static function countPaxAgeInTour(){
        $sql = "SELECT floor(DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365) as `age`, 
                    COUNT(ost_pax.dob) as `amount_age`, DATE(ost_tour.gather_date) as `gather_date` 
                FROM ost_pax_tour JOIN ost_tour ON ost_tour.id = ost_pax_tour.tour_id 
                JOIN ost_pax ON ost_pax_tour.pax_id = ost_pax.id 
                WHERE ost_tour.gather_date IS NOT NULL and DATEDIFF(CURRENT_DATE(),ost_tour.gather_date) < ".DAY_LIMIT."
                GROUP BY DATE(ost_tour.gather_date), floor(DATEDIFF(CURRENT_DATE(),ost_pax.dob)/365)";
        return db_query($sql);
    }
    public static function countPaxGenderInTour(){
        $sql = "SELECT 
                    COUNT(ost_pax.gender) as`amount_gender`,
                    ost_pax.gender as `gender`,
                    DATE(ost_tour.gather_date) as `gather_date` 
                FROM ost_pax_tour JOIN ost_tour ON ost_tour.id = ost_pax_tour.tour_id 
                JOIN ost_pax ON ost_pax_tour.pax_id = ost_pax.id 
                WHERE ost_pax.gender IS NOT NULL  and ost_tour.gather_date IS NOT NULL and DATEDIFF(CURRENT_DATE(),ost_tour.gather_date) < ".DAY_LIMIT."
                GROUP BY DATE(ost_tour.gather_date), ost_pax.gender";
        return db_query($sql);
    }


}

// ----------------class GraphicAgeGroup-----------------
class GraphicAgeGroupModel extends \VerySimpleModel
{
    static $meta = [
        'table' => DEMOGRAPHIC_AGE_GROUP_TABLE,
        'pk' => ['gather_date'],
    ];
}
class GraphicAgeGroup extends  GraphicAgeGroupModel
{

}

//-----------------class demographic_age--------------
class GraphicAgeModel extends \VerySimpleModel
{
    static $meta = [
        'table' => DEMOGRAPHIC_AGE_TABLE,
        'pk' => ['gather_date','age'],
    ];
}
class GraphicAge extends  GraphicAgeModel
{

}

//-----------------class GraphicGender--------------
class GraphicGenderModel extends \VerySimpleModel
{
    static $meta = [
        'table' => DEMOGRAPHIC_GENDER_TABLE,
        'pk' => ['gather_date','gender'],
    ];
}
class GraphicGender extends  GraphicGenderModel
{

}


class StatisticGender extends \koolreport\KoolReport
{
    public function settings()
    {
        return array(
            "dataSources"=>array(
                "age_gender"=>array(
                    "connectionString"=>sprintf("mysql:host=%s;dbname=%s", DBHOST, DBNAME),
                    "username"=>DBUSER,
                    "password"=>DBPASS,
                    "charset"=>"utf8"
                ),
            )
        );
    }

    public function setup()
    {
        $from = ($_REQUEST['from'] && (strlen($_REQUEST['from']) >= 8)) ? $_REQUEST['from'] : date('Y-m-d',time()-365*3600*24);

        $to = ($_REQUEST['to'] && (strlen($_REQUEST['to']) >= 8)) ? $_REQUEST['to'] : date('Y-m-d');

        if($to >= $from) {
            $sql = "SELECT SUM(amount) as `amount`, 
                        CASE WHEN gender = 1 THEN 'male' ELSE 'female' END as `gender` 
                    FROM " . DEMOGRAPHIC_GENDER_TABLE . " 
                    WHERE DATE(gather_date) >= ".db_input($from). " and DATE(gather_date) <= ".db_input($to)." 
                    GROUP BY gender";

            $this->src('age_gender')
                ->query($sql)
                ->pipe($this->dataStore('statistic_gender_pax'));
        }
    }
}
class StatisticAge extends \koolreport\KoolReport
{
    public function settings()
    {
        return array(
            "dataSources"=>array(
                "age_gender"=>array(
                    "connectionString"=>sprintf("mysql:host=%s;dbname=%s", DBHOST, DBNAME),
                    "username"=>DBUSER,
                    "password"=>DBPASS,
                    "charset"=>"utf8"
                ),
            )
        );
    }

    public function setup()
    {
        $from = ($_REQUEST['from'] && (strlen($_REQUEST['from']) >= 8)) ? $_REQUEST['from'] : date('Y-m-d',time()-365*3600*24);

        $to = ($_REQUEST['to'] && (strlen($_REQUEST['to']) >= 8)) ? $_REQUEST['to'] : date('Y-m-d');

        if($to >= $from && !is_null($from) && !is_null($to)) {
            $sql = "SELECT SUM(0_25) as `0-25`,
                        SUM(25_35) as `25-35`,
                        SUM(35_55) as `35-55`,
                        SUM(older_55) as `55+`
                    FROM " . DEMOGRAPHIC_AGE_GROUP_TABLE . "
                     WHERE DATE(gather_date) >= " . db_input($from) . " and DATE(gather_date) <= " . db_input($to);
            $this->src('age_gender')
                ->query($sql)
                ->pipe($this->dataStore('statistic_age_pax'));
        }
    }
}
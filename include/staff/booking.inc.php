<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');

if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '.$title;
else:
    $showing=__('No booking found!');
endif;
?>
<link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/reservation.css">
<style>
    .tab-content {
        overflow: visible;
    }
    table.list {
        table-layout: fixed;
        width: 1258px;
        margin: auto;
        margin-left: -100px;
    }

    table.list .quantity_cell {
        text-align: center;
        vertical-align: middle;
    }

    table.list tbody td {
        overflow: hidden;
        vertical-align: middle;
    }

    table.list .payment_log {
        padding-left: 2em;
    }

    td p:first-child {
        margin-top: 3px;
    }

    td p:last-child {
        margin-bottom: 3px;
    }

    br {
        content: "";
        display: block;
        clear: both;
        margin-bottom: 5px;
    }

    .td {
        word-break: break-all;
    }
</style>
<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs"  id="myTab">
        <li class="active">
            <a href="#allbooking" data-toggle="tab">All Booking</a>
        </li>
        <?php if ($thisstaff->canApproveLeaderReservation()):?>
            <li>
                <a href="#leaderreview" data-toggle="tab">Leader Review
                    <?php if($leaderReview && ($rows = db_num_rows($leaderReview))): ?>
                        <span class="label label-warning"><?php echo $rows ?></span>
                    <?php endif; ?>
                </a>
            </li>
        <?php endif;?>
        <?php if ($thisstaff->canViewApproveLeader() || $thisstaff->canApproveLeaderReservation()):?>
            <li>
                <a href="#leaderreviewed" data-toggle="tab">Leader Reviewed</a>
            </li>
        <?php endif;?>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="allbooking">
            <div class="reservation_list">
                <h2><?php echo __('Booking List');?>
                    &nbsp;<i class="help-tip icon-question-sign" href="#booking"></i>
                </h2>
                <div id='filter' >
                    <form action="booking.php" method="get">
                        <div style="padding-left:2px;">
                            <table>
                                <tr>
                                    <td><b><?php echo __('Created Date'); ?></b> <?php echo __('Between'); ?></td>
                                    <td>
                                        <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
                                        <input class="dp input-field" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>
                                    </td>
                                    <td><?php echo __('Agent'); ?></td>
                                    <td>
                                        <select name='agent' class="input-field">
                                            <option value><?php echo __('All');?></option>
                                            <?php
                                            $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                                                .' FROM '.STAFF_TABLE.' staff '
                                                .' ORDER by name';
                                            if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                                                while(list($id,$name)=db_fetch_row($agent_res)){
                                                    $selected=($_REQUEST['agent'] && $id==$_REQUEST['agent'])?'selected="selected"':'';
                                                    echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td><?php echo __('Info'); ?></td>
                                    <td><input type="text" class="input-field" name="info" value="<?php echo $_REQUEST['info'] ?>"></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Mã Booking'); ?></td>
                                    <td><input type="text" class="input-field" name="booking_code" value="<?php echo $_REQUEST['booking_code'] ?>"></td>

                                    <td>Trạng Thái</td>
                                    <td>
                                        <select name="status" class="input-field">
                                            <option value=><?php echo __('-- All --') ?></option>
                                            <?php
                                            $sql = "SELECT DISTINCT `value` as name FROM ".LIST_ITEM_TABLE." li WHERE li.list_id IN (".BOOKING_STATUS_LIST.")";
                                            $res_list = db_query($sql);
                                            if ($res_list) {
                                                while(($row = db_fetch_array($res_list))) {
                                                    ?>
                                                    <option value="<?php echo $row['name'] ?>"
                                                        <?php if (isset($_REQUEST['status']) && $_REQUEST['status']==$row['name']) echo 'selected' ?>
                                                    ><?php echo $row['name'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>Đối tác</td>
                                    <td>
                                        <select name="partner" class="input-field">
                                            <option value=><?php echo __('-- All --') ?></option>
                                            <?php
                                            $sql = "SELECT DISTINCT `value` as name, id FROM ".LIST_ITEM_TABLE." li WHERE li.list_id IN (".PARTNER_LIST.")";
                                            $res_list = db_query($sql);
                                            if ($res_list) {
                                                while(($row = db_fetch_array($res_list))) {
                                                    ?>
                                                    <option value="<?php echo $row['id'] ?>"
                                                        <?php if (isset($_REQUEST['partner']) && $_REQUEST['partner']==$row['id']) echo 'selected' ?>
                                                    ><?php echo $row['name'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Loại Booking</td>
                                    <td>
                                        <select name="tour[]" class="input-field" multiple style="height: 12em;">
                                            <option value=>- Select -</option>
                                            <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                                                <?php foreach($booking_type_list as  $item): ?>
                                                    <option value="<?php echo $item['id'] ?>"
                                                        <?php if (isset($_REQUEST['tour']) && in_array($item['id'],$_REQUEST['tour'])) echo 'selected' ?>
                                                    ><?php echo $item['name'] ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>

                                    <td><?php echo __('Số khách hàng'); ?></td>
                                    <td><input type="text" class="input-field" name="customer_number" value="<?php echo $_REQUEST['customer_number'] ?>"></td>
                                </tr>
                                <tr>

                                    <td colspan="6">
                                        <input type="submit" class="btn_sm btn-primary" Value="<?php echo __('Search');?>" />
                                        <input type="reset" class="btn_sm btn-default" Value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
                                        <?php if($thisstaff->canExportBookings()): ?>
                                            <button type="submit" class="btn_sm btn-success" value="1" name="export">Export</button>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <script>
                                    function reset_form(event) {
                                        console.log(event);
                                        form = $(event).parents('form');
                                        form.find('input[type="text"], select, .hasDatepicker').val('');
                                        form.submit();
                                    }
                                </script>
                            </table>
                        </div>
                    </form>
                </div>
                <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
                    <caption style="margin-bottom: 0em"><?php echo $showing; ?></caption>
                    <thead>
                    <tr>
                        <th><?php echo __('Mã Booking');?></th>
                        <th>Loại Booking</th>
                        <th style="width:115px;"><?php echo __('Phiếu Thu');?></th>
                        <th style="width:180px;"><?php echo __('Khách hàng');?></th>
                        <th style="width:100px;"><?php echo __('Nhân viên');?></th>
                        <th style="width: 70px"><?php echo __('Số lượng');?></th>
                        <th><?php echo __('Khởi hành');?></th>
                        <th><?php echo __('Giá Tổng');?></th>
                        <th style="width:180px;"><?php echo __('Ghi chú');?></th>
                        <th style="width:105px;"><?php echo __('Ngày tạo');?></th>
                    </tr>
                    <?php if($is_mobile): ?>
                        <tr>
                            <th colspan="10" style="text-align:center; padding: 0.5em"><?= number_format($total_amount, 0).'đ' ?></th>
                        </tr>
                    <?php endif; ?>
                    </thead>
                    <tbody>
                    <?php
                    $total=0;
                    if($res):
                        while ($row = db_fetch_array($res)) {
                            ?>
                            <tr id="<?php echo $row['ticket_id']; ?>" class="<?php if ($row['amount'] < 0) echo 'type_out' ?>">
                                <td>
                                    <a class="no-pjax" target="_blank" title="Logs"
                                       href="<?php echo $cfg->getUrl()?>scp/new_booking.php?id=<?php echo $row['ticket_id']; ?>"
                                    ><?php echo Format::htmlchars($row['booking_code']); ?></a>
                                    <br>
                                    <a class="btn_sm btn-xs btn-default no-pjax"
                                       href="<?php echo $cfg->getUrl()?>scp/new_booking.php?id=<?php echo $row['ticket_id']; ?>&action=edit"
                                       data-title="Edit">
                                        <i class="icon-edit"></i>
                                    </a>
                                    <?php if($thisstaff->canCreateReceipt()): ?>
                                        <a class="btn_sm btn-xs btn-primary no-pjax"
                                           href="<?php echo $cfg->getUrl() ?>scp/receipt.php?booking_code=<?php echo $row['booking_code'] ?>&from_view=booking"
                                           data-title="Create Receipt">
                                            <i class="icon-list"></i>
                                        </a>
                                    <?php endif;  ?>
                                </td>
                                <td><?php echo $booking_type_list[$row['booking_type_id']]['name']?:''; ?></td>
                                <td>
                                    <em><?php echo $row['receipt_code']; ?></em>
                                </td>
                                <td>
                                    <div class="more">
                                        <p><?php echo $row['customer'] ?></p>
                                        <?php if($row['phone_number']): ?>
                                            <p><i class="icon-phone"></i> <?php echo $row['phone_number'] ?></p>
                                        <?php endif; ?>
                                    </div>
                                </td>
                                <td>
                                    <p><em><?php echo _String::json_decode($row['staff']); ?></em></p>
                                </td>
                                <td class="quantity_cell"><?php echo $row['total_quantity']; ?></td>
                                <td>
                                    <p><?php echo Format::userdate('d/m/Y H:i', $row['dept_date']); ?></p>
                                    <?php $_partner='';
                                        if(is_numeric($row['partner'])){
                                            $result = DynamicListItem::lookup($row['partner']);
                                            if ($result && $result->value)
                                                $_partner = $result->value;
                                        }
                                        elseif(strlen(($_partner=_String::json_decode($row['partner']))))
                                            $_partner=_String::json_decode($row['partner']);
                                    ?>
                                    <p><span class="label label-<?php if ($_partner === 'Tugo') echo 'success'; else echo 'primary'; ?>"><?php echo $_partner; ?></span></p>
                                </td>
                                <td>
                                    <p>Bán: <?php echo number_format($row['total_retail_price']).'đ'; ?></p>
                                    <p>Net: <?php echo number_format($row['total_net_price']).'đ'; ?></p>
                                    <?php if($row['total_profit']): ?>
                                        <p>Lợi nhuận: <?php echo number_format($row['total_profit']).'đ'; ?></p>
                                        <?php endif; ?>
                                        <?php if($booking_total_amount[ $row['ticket_id'] ]): ?>
                                            <p style="font-weight: bold;">
                                            <?= 'Đã thu: ' . number_format($booking_total_amount[ $row['ticket_id'] ], 0).'đ' ?>
                                            <?php if($booking_total_amount[ $row['ticket_id'] ] == $row['total_retail_price']): ?>
                                                <i class="icon-ok"></i>
                                            <?php endif; ?>
                                            </p>
                                        <?php endif; ?>
                                </td>
                                <td>
                                    <?php
                                    $len = 100;
                                    $note = mb_substr($row['note'], 0, $len, 'UTF-8').(strlen($row['note']) > $len ? "[...]":"");
                                    ?>
                                    <span <?php if(strlen($row['note']) > $len):?>title="<?php echo strip_tags($row['note']) ?><?php endif; ?>"><?php echo strip_tags($row['note']); ?></span>
                                    <?php if($note && isset($payemnt_logs[$row['ticket_id']])): ?>
                                        <hr>
                                    <?php endif; ?>
                                    <?php if(isset($payemnt_logs[$row['ticket_id']]) && is_array($payemnt_logs[$row['ticket_id']])): ?>
                                        <ol class="payment_log">
                                            <?php foreach($payemnt_logs[$row['ticket_id']] as $_ticket_id => $_row): ?>
                                                <li><?php echo $_row['receipt_code'] ?>: <?php echo number_format($_row['amount']).'đ' ?></li>
                                            <?php endforeach; ?>
                                        </ol>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if(strtotime($row['created'])): ?>
                                        <p><?php echo Format::date('d/m/Y H:i', strtotime($row['created'])); ?></p>
                                    <?php endif; ?>
                                    <?php $status = '';
                                    if(is_numeric($row['status'])) {
                                        $result = DynamicListItem::lookup($row['status']);
                                        if($result && $result->value)
                                            $status = $result->value;
                                    }
                                    elseif(strlen(($status = _String::json_decode($row['status']))))
                                        $status = _String::json_decode($row['status'])
                                    ?>
                                    <p><span class="label label-small label-<?php if ($status === 'Thành công')
                                            echo 'success'; else echo 'default'; ?>"><?php echo $status; ?></span></p>
                                </td>
                            </tr>
                            <?php
                        } //end of while.
                    endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8">
                            <?php if($res && $num){ ?>
                            <?php }else{
                                echo __('No booking found');
                            } ?>
                        </td>
                    </tr>
                    </tfoot>
                </table>
                <?php
                if($res && $num): //Show options..
                    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
                endif;
                ?>
            </div>
        </div>
        <?php if ($thisstaff->canApproveLeaderReservation()):?>
            <div class="tab-pane" id="leaderreview">
                <div class="upcoming_list">
                    <?php include STAFFINC_DIR.'reservation.leaderreview.inc.php' ?>
                </div>
            </div>
        <?php endif;?>
        <?php if ($thisstaff->canViewApproveLeader() || $thisstaff->canApproveLeaderReservation()):?>
            <div class="tab-pane" id="leaderreviewed">
                <div class="upcoming_list">
                    <?php include STAFFINC_DIR.'reservation.leaderreviewed.inc.php' ?>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>


<script>
    (function($) {
        $(document).ready(function(){
            activeTab(window.location.hash.replace('#',''));
        });

        function activeTab(tab){
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        }

        $('#myTab a').click(function (e) {
            e.preventDefault();
            window.location.hash = $(e.target).attr("href");
            $(this).tab('show');
        });

        $('.more').readmore({
            speed: 20,
            collapsedHeight: 100,
            moreLink: '<a href="#" class="read_more_link">Read more</a>',
            lessLink: '<a href="#" class="read_more_link">Close</a>',
            blockCSS: 'display: block; width: 100%; margin-bottom: 1em !important;'
        });
    })(jQuery);
</script>

<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

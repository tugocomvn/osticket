<?php
require_once(INCLUDE_DIR . 'class.orm.php');

// date of week: 1:Sun -> 7: Fri
// week: start 1
// time: 24h
class Analysis{
    public static function countCall($from = null, $to = null){
        $start = microtime(1);
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');

        $date_from = date_create_from_format('Y-m-d H:i:s', "$from 00:00:00");
        $date_to = date_create_from_format('Y-m-d H:i:s', "$to 00:00:00");

        $date_check = $date_from;
        while($date_check <= $date_to) {
            $date_string = $date_check->format('Y-m-d');
            echo "$date_string \r\n";

            $sql = "DELETE FROM ".ANALYSIS_CALL_TABLE." WHERE 
            date_value = ".db_input($date_string);
            echo date('Y-m-d H:i:s')." DELETE $date_string ";
            db_query($sql);
            echo date('Y-m-d H:i:s')."  OK\r\n";

            $sql = 'insert into '.ANALYSIS_CALL_TABLE.'(
                `date`,
                `date_value`,
                `hour`,
                `hour_of_day`,
                `day_of_month`,
                `day_of_week`,
                `day_of_week_name`,
                `week_of_year`,
                `week_of_the_year`,
                `minute`,
                `month_of_the_year`,
                `month_of_year`,
                `year`,
                `minute_of_day`,
                `amount`,
                `billduration`,
                `callernumber`,
                `destinationnumber`,
                `disposition`,
                `direction`
                )
            select DISTINCT 
                date_format(starttime, "%Y%m%d") as `date`
                , date_format(starttime, "%Y-%m-%d") as `date_value`
                , date_format(starttime, "%H")as `hour`
                , date_format(starttime, "%Y%m%d%H") as `hour_of_day`
                , date_format(starttime, "%d") as `day_of_month`
                , dayofweek(starttime) as `day_of_week`
                , date_format(starttime, "%W") as `day_of_week_name`
                , date_format(starttime, "%Y%v") as `week_of_year`
                , date_format(starttime, "%v") as `week_of_the_year`
                , date_format(starttime, "%i") as `minute`
                , date_format(starttime, "%m") as `month_of_the_year`
                , date_format(starttime, "%Y%m") as `month_of_year`
                , date_format(starttime, "%Y") as `year`
                , date_format(starttime, "%Y%m%d%H%i") as `minute_of_day`
                , count(id) as `amount`
                , sum(billduration) as `billduration`
                , `callernumber`
                , `destinationnumber` 
                , `disposition`
                , direction
                from ost_call_log
                where 
                    date(starttime) = '.db_input($date_string).'
                    and length(destinationnumber) <= 12
                    and length(callernumber) <= 12
                group by
                    date(starttime)
                    ,hour(starttime)
                    ,minute(starttime)
                    , `callernumber` 
                    , `destinationnumber` 
                    , `disposition` 
                    , direction';

            echo date('Y-m-d H:i:s')." INSERT ";
            db_query($sql);
            echo date('Y-m-d H:i:s')."  OK\r\n";

            $date_check = $date_check->add(new DateInterval('P1D'));
        }

        echo "Execute time: ".(microtime(1) - $start)."\r\n";
    }
    public static function countSms($from = null, $to = null){
        $start = microtime(1);
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');

        $date_from = date_create_from_format('Y-m-d H:i:s', "$from 00:00:00");
        $date_to = date_create_from_format('Y-m-d H:i:s', "$to 00:00:00");

        $date_check = $date_from;
        while($date_check <= $date_to) {
            $date_string = $date_check->format('Y-m-d');
            echo "$date_string \r\n";

            $sql = "DELETE FROM ".ANALYSIS_SMS_TABLE." WHERE 
            date_value = ".db_input($date_string);
            echo date('Y-m-d H:i:s')." DELETE $date_string ";
            db_query($sql);
            echo date('Y-m-d H:i:s')."  OK\r\n";

            $sql = 'insert into '.ANALYSIS_SMS_TABLE.'
                (`date`,
                 `date_value`,
                 `hour`,
                 `hour_of_day`,
                 `day_of_month`,
                 `day_of_week`,
                 `day_of_week_name`,
                 `week_of_year`,
                 `week_of_the_year`,
                 `minute`,
                 `month_of_the_year`,
                 `month_of_year`,
                 `year`,
                 `minute_of_day`,
                 `amount`,
                 `phone_number`,
                 `type`) select DISTINCT 
                date_format(send_time, "%Y%m%d") as `date`
                , date_format(send_time, "%Y-%m-%d") as `date_value`
                , date_format(send_time, "%H")as `hour`
                , date_format(send_time, "%Y%m%d%H") as `hour_of_day`
                , date_format(send_time, "%d") as `day_of_month`
                , dayofweek(send_time) as `day_of_week`
                , date_format(send_time, "%W") as `day_of_week_name`
                , date_format(send_time, "%Y%v") as `week_of_year`
                , date_format(send_time, "%v") as `week_of_the_year`
                , date_format(send_time, "%i") as `minute`
                , date_format(send_time, "%m") as `month_of_the_year`
                , date_format(send_time, "%Y%m") as `month_of_year`
                , date_format(send_time, "%Y") as `year`
                , date_format(send_time, "%Y%m%d%H%i") as `minute_of_day`
                , count(id) as `amount`
                , `phone_number`
                , `type`
                from ost_sms_log
                where 
                    date(send_time) = '.db_input($date_string).'
                    and length(phone_number) <= 12
                group by
                    date(send_time),
                    hour(send_time),
                    minute(send_time)
                    , `type`
                    , `phone_number` ';

            echo date('Y-m-d H:i:s')." INSERT ";
            db_query($sql);
            echo date('Y-m-d H:i:s')."  OK\r\n";

            $date_check = $date_check->add(new DateInterval('P1D'));
        }

        echo "Execute time: ".(microtime(1) - $start)."\r\n";
    }

    public static function countAge($from = null, $to = null){
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');

        $sql = "DELETE FROM ".ANALYSIS_AGE_TABLE." WHERE 
            date_value >= ".db_input($from)."
            AND date_value <= ".db_input($to);
        echo "$sql\r\n";
        db_query($sql);

        $_status = trim(json_encode(trim('Hủy')), '"');
        $_status = sprintf(" AND (  b.status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
        $sql = 'select  DISTINCT
                    date_format(b.created, "%Y%m%d") as `date`
                    , date_format(b.created, "%Y-%m-%d") as `date_value`
                    , date_format(b.created, "%d") as `day_of_month`
                    , dayofweek(b.created) as `day_of_week`
                    , date_format(b.created, "%W") as `day_of_week_name`
                    , date_format(b.created, "%Y%v") as `week_of_year`
                    , date_format(b.created, "%v") as `week_of_the_year`
                    , date_format(b.created, "%m") as `month_of_the_year`
                    , date_format(b.created, "%Y%m") as `month_of_year`
                    , date_format(b.created, "%Y") as `year`
                    , floor(DATEDIFF(b.created , p.dob) / 365) as `age_pax`
                    , date_format(p.dob, "%Y") as `dob_year`
                    , count(p.dob) as `amount_pax`
                    , t.name as `tour_name`
                    , td.name as `tour_destination`
                    , tm.name as `tour_market`
                    , t.id as `tour_id`
                    , td.id as `tour_destination_id`
                    , tm.id as `tour_market_id`
                from 
                    ost_pax_tour pt 
                join booking_tmp b 
                    on trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM pt.booking_code)) = b.booking_code_trim
                join ost_pax p 
                    on p.id = pt.pax_id
                join tour t ON t.id = pt.tour_id
                join tour_destination td ON td.id = t.destination
                join tour_market tm ON tm.id = td.tour_market_id
                where 
	                b.created is not null and pt.booking_code is not null
	                and date(b.created) >= '.db_input($from).' and date(b.created) <= '.db_input($to).'
	                and t.name not like \'%delete%\'
	                and t.name not like \'%rot visa%\'
	                and t.name not like \'%rớt visa%\'
	                '.$_status.'
                group by
                    date_format(b.created, "%Y%m%d")
                    , floor(DATEDIFF(b.created, p.dob)/365)
                    , td.id
                    , tm.id
        ';
        echo "$sql\r\n";
        return db_query($sql);
    }

    public static function countPax($from = null, $to = null){
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');

        $sql = "DELETE FROM ".ANALYSIS_PAX_TABLE." WHERE 
            date_value >= ".db_input($from)."
            AND date_value <= ".db_input($to);
        echo "$sql\r\n";
        db_query($sql);

        $_status = trim(json_encode(trim('Hủy')), '"');
        $_status = sprintf(" AND (  b.status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
        $sql = 'select DISTINCT
                    FROM_UNIXTIME(b.dept_date, "%Y%m%d") as `date`
                 , FROM_UNIXTIME(b.dept_date, "%Y-%m-%d") as `date_value`
                 , FROM_UNIXTIME(b.dept_date, "%d")     as `day_of_month`
                 , FROM_UNIXTIME(b.dept_date, "%w") + 1 as `day_of_week`
                 , FROM_UNIXTIME(b.dept_date, "%W")     as `day_of_week_name`
                 , FROM_UNIXTIME(b.dept_date, "%Y%v")   as `week_of_year`
                 , FROM_UNIXTIME(b.dept_date, "%v")     as `week_of_the_year`
                 , FROM_UNIXTIME(b.dept_date, "%m")     as `month_of_the_year`
                 , FROM_UNIXTIME(b.dept_date, "%Y%m")   as `month_of_year`
                 , FROM_UNIXTIME(b.dept_date, "%Y")     as `year`
                 , pi.passport_no
                 , p.full_name
                 , pt.phone_number
                 , date_format(p.dob, "%Y%m%d") as dob_string
                 , YEAR(p.dob) as dob_year
                 , pt.booking_code
                 , b.total_retail_price
                 , b.total_quantity
                 , t.name as tour_name
                 , td.name as tour_destination
                 , tm.name as tour_market
                 , t.id as tour_id
                 , td.id as tour_destination_id
                 , tm.id as tour_market_id
            from booking_view b
                     join ost_pax_tour pt on
                    trim(LEADING \'0\' FROM trim(LEADING \'TG123-\' FROM trim(LEADING \'TG-\' FROM b.booking_code))) LIKE
                    trim(LEADING \'0\' FROM trim(LEADING \'TG123-\' FROM trim(LEADING \'TG-\' FROM pt.booking_code)))
                     join tour t ON t.id = pt.tour_id
                     join tour_destination td ON td.id = t.destination
                     join tour_market tm ON tm.id = td.tour_market_id
                     join ost_pax_info pi on pi.id = pt.pax_info
                     join ost_pax p on p.id = pt.pax_id
            WHERE 1
             and FROM_UNIXTIME(b.dept_date) >= '.db_input($from).' 
             and date(FROM_UNIXTIME(b.dept_date)) <= '.db_input($to).'
              and t.name not like \'%delete%\'
              and t.name not like \'%rot visa%\'
              and t.name not like \'%rớt visa%\'
              '.$_status.'
            group by FROM_UNIXTIME(b.dept_date, "%Y%m%d")
                   , pi.passport_no
                   , pt.phone_number
                   , b.booking_code
                   , t.id
                   , td.id
                   , tm.id

                ';
        echo "$sql\r\n";
        return db_query($sql);
    }

    public static function countBooking($from = null, $to = null){
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');


        $sql = "DELETE FROM ".ANALYSIS_BOOKING_TABLE." WHERE 
            date_value >= ".db_input($from)."
            AND date_value <= ".db_input($to);
        echo "$sql\r\n";
        db_query($sql);

        $_status = trim(json_encode(trim('Hủy')), '"');
        $_status = sprintf(" AND (  b.status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
        $sql = 'select DISTINCT
                    date_format(b.created, "%Y%m%d") as `date`
                   , date_format(b.created, "%Y-%m-%d") as `date_value`
                    , date_format(b.created, "%H")as `hour`
                    , date_format(b.created, "%Y%m%d%H") as `hour_of_day`
                    , date_format(b.created, "%d") as `day_of_month`
                    , dayofweek(b.created) as `day_of_week`
                    , date_format(b.created, "%W") as `day_of_week_name`
                    , date_format(b.created, "%Y%v") as `week_of_year`
                    , date_format(b.created, "%v") as `week_of_the_year`
                    , date_format(b.created, "%i") as `minute`
                    , date_format(b.created, "%m") as `month_of_the_year`
                    , date_format(b.created, "%Y%m") as `month_of_year`
                    , date_format(b.created, "%Y") as `year`
                    , date_format(b.created, "%Y%m%d%H%i") as `minute_of_day`
                    , count(ticket_id) as `new_booking`
                    , sum(total_quantity) as `booking_quantity`
                    , sum(total_retail_price) as `booking_value`
                    , s.staff_id as `staff_id`
                    , s.username as `staff_username`
                    , td.name as `tour_destination`
                    , tm.name as `tour_market`
                    , td.id as `tour_destination_id`
                    , tm.id as `tour_market_id`
                from 
                    booking_view b
                    join tour_destination td ON td.id = b.booking_type_id
                    join tour_market tm ON tm.id = td.tour_market_id
                    join ost_staff s ON s.staff_id=b.staff_id
                where
                    date(b.created) >= '.db_input($from).' and date(b.created) <= '.db_input($to).'
                    and b.created is not null and b.created <> 0
                    and s.staff_id is not null
                    and booking_code is not null and booking_code != \'\'
                    '.$_status.'
                group by
                     date_format(b.created, "%Y%m%d%H%i")
                    , s.staff_id
                    , td.id
                    , tm.id
                    ';
        echo $sql;
        return db_query($sql);
    }

    public static function countTicket($from = null, $to = null){
        if($from === null)
            $from = date('Y-m-d', time() - DAY_LIMIT_COUNT * 24 * 60 * 60);
        if($to === null)
            $to = date('Y-m-d');

        $date_from = date_create_from_format('Y-m-d H:i:s', "$from 00:00:00");
        $date_to = date_create_from_format('Y-m-d H:i:s', "$to 00:00:00");

        $date_check = $date_from;
        while($date_check <= $date_to) {
            $date_string = $date_check->format('Y-m-d');
            echo "$date_string \r\n";

            $sql = "DELETE FROM ".ANALYSIS_TICKET_TABLE." WHERE 
            date_value = ".db_input($date_string);
            echo date('Y-m-d H:i:s')." DELETE $date_string \r\n";
            db_query($sql);
            echo date('Y-m-d H:i:s')."  OK\r\n";

            echo date('Y-m-d H:i:s')." INSERT ";
            $sql = ' insert into '.ANALYSIS_TICKET_TABLE.' (
             `date`, 
            `date_value`, 
            `hour`, 
            `hour_of_day`, 
            `day_of_month`, 
            `day_of_week`, 
            `day_of_week_name`, 
            `week_of_year`, 
            `week_of_the_year`, 
            `minute`, 
            `month_of_the_year`, 
            `month_of_year`, 
            `year`, 
            `minute_of_day`, 
            `new_ticket`, 
            `staff_id` 
        )
            select DISTINCT 
                    date_format(created, "%Y%m%d") as `date`
                    , date_format(created, "%Y-%m-%d") as `date_value`
                    , date_format(created, "%H")as `hour`
                    , date_format(created, "%Y%m%d%H") as `hour_of_day`
                    , date_format(created, "%d") as `day_of_month`
                    , dayofweek(created) as `day_of_week`
                    , date_format(created, "%W") as `day_of_week_name`
                    , date_format(created, "%Y%v") as `week_of_year`
                    , date_format(created, "%v") as `week_of_the_year`
                    , date_format(created, "%i") as `minute`
                    , date_format(created, "%m") as `month_of_the_year`
                    , date_format(created, "%Y%m") as `month_of_year`
                    , date_format(created, "%Y") as `year`
                    , date_format(created, "%Y%m%d%H%i") as `minute_of_day`
                    , count(ticket_id) as `new_ticket`
                    , staff_id as `staff_id`
                from
                    ost_ticket	
                where
                    date(created) = '.db_input($date_string).'
                    and dept_id IN ('.implode(',', unserialize(SALE_DEPT)).')
                group by
                    date(created),
                    hour(created),
                    minute(created)
                    , staff_id';
            db_query($sql);
            echo date('Y-m-d H:i:s')." OK\r\n ";

            $date_check = $date_check->add(new DateInterval('P1D'));
        }
    }
}

class AnalysisCallModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_CALL_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisCall extends  AnalysisCallModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        foreach ($data as $key=>$value){
            $fields[] = "`$key` = ".db_input($value);
        }

        $sql .= ' SET '.implode(', ', $fields);

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}

class AnalysisSmsModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_SMS_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisSms extends  AnalysisSmsModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        foreach ($data as $key=>$value){
            $fields[] = "`$key` = ".db_input($value);
        }

        $sql .= ' SET '.implode(', ', $fields);

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }

}

class AnalysisAgeModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_AGE_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisAge extends  AnalysisAgeModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        foreach ($data as $key=>$value){
            switch($key) {
                case 'tour_name':
                case 'tour_destination':
                case 'tour_market':
                    $fields[] = "`$key` = ".db_input(_String::khongdau($value));
                    break;
                default:
                    $fields[] = "`$key` = ".db_input($value);
                    break;
            } // end switch
        }
        $sql .= ' SET '.implode(', ', $fields);

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}

class AnalysisPaxModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_PAX_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisPax extends  AnalysisPaxModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        foreach ($data as $key=>$value){
            switch($key) {
                case 'tour_name':
                case 'tour_destination':
                case 'tour_market':
                    $fields[] = "`$key` = ".db_input(_String::khongdau($value));
                    break;
                default:
                    $fields[] = "`$key` = ".db_input($value);
                    break;
            } // end switch
        } // end foreach

        $sql .= ' SET '.implode(', ', $fields);

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}

class AnalysisBookingModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_BOOKING_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisBooking extends  AnalysisBookingModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        foreach ($data as $key=>$value){
            switch ($key){
                case 'tour_name':
                case 'tour_destination':
                case 'tour_market':
                    $fields[] = " `$key` = ".db_input(_String::khongdau($value));
                    break;
                default:
                    $fields[] = " `$key` = ".db_input($value);
                    break;
            } // end switch
        } // foreach

        $sql .= ' SET '.implode(', ', $fields);

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}

class AnalysisTicketModel extends VerySimpleModel {
    static $meta = array(
        'table' => ANALYSIS_TICKET_TABLE,
        'pk' => array('id')
    );
}

class  AnalysisTicket extends  AnalysisTicketModel{
    public static function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        $update_value = '';
        foreach ($data as $key=>$value){
            $fields[] = "`$key` = ".db_input($value);

            if($key === 'new_ticket')
                $update_value = 'new_ticket = '.$value;
        }

        $sql .= ' SET '.implode(', ', $fields);
        $sql .= ' ON DUPLICATE KEY UPDATE '.$update_value;

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}

CREATE TABLE `ost_email_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_type` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'H, O, T, U',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ost_email_index_email_object_id_object_type_uindex` (`email`,`object_id`,`object_type`),
  KEY `ost_email_index_email_index` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29726108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci


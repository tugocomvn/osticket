<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canEditReceipt() ||
    !($thisstaff->getId() == $result->created_by
        || $thisstaff->getId() == $result->staff_cashier_id
        || $thisstaff->getId() == $result->staff_id
        || $thisstaff->canRejectReceipt()
    )) exit('Access Denied');
?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>">Back</a>
<h2 class="centered">Tải lên hình ảnh phiếu thu online</h2>
<h3 class="centered"><font class="error">*</font>Chú ý: Chỉ upload phiếu thu có chữ ký xác nhận của khách và sales. Upload ảnh lấy toàn bộ thông tin trên phiếu thu</h3>
<form action="<?php echo $cfg->getUrl() ?>scp/receipt.php" method="POST" enctype = "multipart/form-data">
    <?php csrf_token(); ?>
    <?php if(isset($errors) && $errors): ?>
        <div id="msg_error"><?php echo $errors ?></div>
    <?php endif; ?>
    <input type="hidden" name="id" value="<?php echo $result->id ?>">
    <p class="centered">Mã phiếu thu: <b><?php echo $result->receipt_code ?></b></p>
    <p class="centered">Mã booking code: <b><?php echo $result->booking_code ?></b></p>

    <div class="centered form-group">
        <input type="file" id="profile-img" name ="image" class="input-field" />
        <?php if($thisstaff->canEditReceipt() &&
            ($thisstaff->getId() == $result->created_by
            || $thisstaff->getId() == $result->staff_cashier_id
            || $thisstaff->getId() == $result->staff_id
            || $thisstaff->canRejectReceipt())
        ): ?>
        <span class="centered">
            <button class="btn_sm btn-success" type="submit" name="action" value="receipt_upload">Upload</button>
        </span>
        <?php endif; ?>
    </div>
    <br>
    <div class="centered">
        <img src="" id="profile-img-tag" width="30%"/>
        <p><em id="notify_image"></em></p>
    </div>
</form>
<div>
    <?php if (isset($dataImage) && $dataImage !== null) :?>
        <p>Quét mã bên dưới để upload ảnh trực tiếp từ điện thoại</p>
        <img src="<?php echo $dataImage ?>" width="25%">
    <?php endif; ?>
</div>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
                $('#notify_image').text('Ảnh hiển thị trước khi tải lên')
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>

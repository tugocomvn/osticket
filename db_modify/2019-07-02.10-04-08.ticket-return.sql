
CREATE TABLE `ost_ticket_count` (
                                  `id` int(11) NOT NULL,
                                  `ticket_id` int(11) NOT NULL,
                                  `count_ticket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


alter table ost_ticket_count
  add constraint ost_ticket_count_pk
    primary key (id);
alter table ost_ticket_count modify id int auto_increment;

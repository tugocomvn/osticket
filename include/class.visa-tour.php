<?php
require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.sms.php');
require_once INCLUDE_DIR.'class.visa.php';


class VisaTourModel extends VerySimpleModel {
    static $meta = [
        'table' => TOUR_NEW_TABLE,
        'pk' => ['id'],
        'ordering' => ['gateway_present_time'],
    ];
}
class VisaTour extends VisaTourModel {
    public static function getPaginationTour(array $params,$offset,$limit = 30,&$total) {
        $sql = "SELECT 
                    case when  date(t.gather_date) >= ".db_input(date('Y-m-d'))." then 1 else 0 end as `greater_date`,
                    (SELECT count(ost_pax_tour.pax_id) FROM ost_pax_tour WHERE ost_pax_tour.tour_id = t.id) as sum_pax,
                       t.*,a.name as name_booking, t.id as tour_id
        FROM ".TOUR_TABLE." t ";

        $sql .= " LEFT JOIN ".TOUR_DESTINATION_TABLE." a ON t.destination=a.id";

        $sql .= " WHERE 1 ";

        $sql .= " AND t.status != ".VisaStatus::Disable."";
        $group_des = $params['destination'];

        if(isset($group_des) && is_array($group_des) && count(array_unique(array_filter($group_des)))) {
            $sql .= " AND destination IN (" . implode(",", $group_des) . ") ";
        }else{
            $sql .= " AND 1 = 0";
        }

        $limit = " LIMIT $limit OFFSET $offset";

        if($params['return_date']){
               $sql .=" AND DATE(`return_date`) = date(".(db_input($params['return_date'])).")";
        }
        if($params['gather_date']){
            $sql .=" AND DATE(`gather_date`) = date(".(db_input($params['gather_date'])).")";
        }
        if($params['keyword_tour']){
            $sql .= " AND t.name LIKE ".db_input('%'.($params['keyword_tour']).'%')." ";
        }
        if($params['tour'] && ( $params['tour'] = array_unique(array_filter($params['tour'])))){
            $sql .= " AND destination IN "."('".implode("','",$params['tour'])."')";
        }
        if($params['departure_flight_code']){
            $sql .= " AND departure_flight_code LIKE ".db_input($params['departure_flight_code']);
        }
        if($params['visa_code']){
           $tour_id = self::searchVisaCode($params['visa_code']);
           $sql .= " AND t.id = ".db_input($tour_id);
        }

        if($params['number_phone']){
           $tour_id = self::searchNumberPhone($params['number_phone']);
           if($tour_id)
               $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
           else
               $sql .= " AND 1 = 0 ";
        }
        if($params['passport_no']){
           $tour_id = self::searchPassport($params['passport_no']);
           if($tour_id)
               $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
           else
               $sql .= " AND 1 = 0 ";
        }
        if($params['name_pax']){
           $tour_id = self::searchNamePax($params['name_pax']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['staff']){
            $tour_id = self::sreachStaff($params['staff']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['return_flight_code']){
            $sql .= " AND return_flight_code LIKE ".db_input($params['return_flight_code']);
        }
        if($params['departure_flight_code_b']){
            $sql .= " AND departure_flight_code_b LIKE ".db_input($params['departure_flight_code_b']);
        }
        if($params['return_flight_code_b']){
            $sql .= " AND return_flight_code_b LIKE ".db_input($params['return_flight_code_b']);
        }

        $order = ' order by `greater_date` desc, 
                    case when `greater_date` = 1 then t.gather_date end asc, 
                    case when `greater_date` = 0 then t.gather_date end desc';

        $res = db_query("$sql $order $limit");

        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function getPaginationTourNew(array $params, $offset, &$total, $limit = 30) {
        $sql = "SELECT
                    t.name, t.departure_date, t.id,
                       a.name as name_booking, t.id as tour_id
                       ,t.gateway_present_time as `gather_date`, t.return_time as `return_date`
        FROM ".TOUR_NEW_TABLE." t ";

        $sql .= " JOIN ".TOUR_DESTINATION_TABLE." a ON t.destination=a.id";

        $sql .= " WHERE 1 ";

        $sql .= " AND t.status != ".VisaStatus::Disable."";

        if($params['destination'] && is_array($params['destination']) && !empty(array_filter($params['destination']))){
            $sql .= " AND destination IN (" . implode(",", $params['destination']) . ") ";
        }

        if(isset($params['group_des']) && is_array($params['group_des']))
            if(count($params['group_des']) > 0)
                if(isset($params['tour_not_des']) && $params['tour_not_des'] === 1)
                    $sql .= " AND ( destination IN (" . implode(",", $params['group_des']) . ")  OR destination is null ) "; // allow view tour NOT destination
                else
                    $sql .= " AND destination IN (" . implode(",", $params['group_des']) . ") ";
            else
                $sql .= " AND 1 = 0 ";

        $limit = " LIMIT $limit OFFSET $offset";

        if($params['return_date']){
            $sql .=" AND DATE(`return_time`) = date(".(db_input($params['return_date'])).")";
        }
        if($params['gather_date']){
            $sql .=" AND DATE(`gateway_present_time`) = date(".(db_input($params['gather_date'])).")";
        }
        if($params['keyword_tour']){
            $sql .= " AND t.name LIKE ".db_input('%'.($params['keyword_tour']).'%')." ";
        }
        if($params['booking_type_id']){
            $sql .= " AND destination = ".db_input($params['tour'])." ";
        }

        if($params['visa_code']){
            $tour_id = self::searchVisaCode($params['visa_code']);
            $sql .= " AND t.id = ".db_input($tour_id);
        }

        if($params['number_phone']){
            $tour_id = self::searchNumberPhone($params['number_phone']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }
        if($params['passport_no']){
            $tour_id = self::searchPassport($params['passport_no']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }
        if($params['name_pax']){
            $tour_id = self::searchNamePax($params['name_pax']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['staff']){
            $tour_id = self::sreachStaff($params['staff']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if ($params['booking_code']) {
            $sql .= " AND (trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM pt.booking_code))) 
                    LIKE " . db_input(_String::trimBookingCode($params['booking_code']));
        }

        $sql .= " GROUP BY t.id ";
        $order = " ORDER BY case when date(gateway_present_time) > NOW() then 0 when date(gateway_present_time) <= NOW() then 1 end,
         case when date(gateway_present_time) > NOW() then gateway_present_time end asc,
         case when date(gateway_present_time) <= NOW() then gateway_present_time end desc ";

        $res = db_query("$sql $order $limit");

        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function searchVisaCode($visa_code) {

        $sql = "SELECT DISTINCT t.tour_id
                FROM ost_pax p JOIN ost_pax_info i ON p.id=i.pax_id 
                JOIN ost_pax_tour t ON p.id=t.pax_id AND i.id=t.pax_info 
                JOIN ".VISA_TOUR_PAX_PROFILE." vtp ON vtp.id=t.tour_pax_visa_profile_id";

        if (isset($visa_code) && $visa_code) {
            $sql .= " AND vtp.visa_code LIKE '%".$visa_code."%' ";
        }
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        $list = $row['tour_id'];
        return $list ? $list : null;
    }

    public static function getIdProfileVisa($tour_id)
    {
        $sql = "SELECT p.id as id_guest
        FROM ".PAX_TABLE." p 
        JOIN ".PAX_INFO_TABLE." i ON p.id=i.pax_id
        JOIN ".PAX_TOUR_TABLE." t ON p.id=t.pax_id AND i.id=t.pax_info
        JOIN ".TOUR_NEW_TABLE." tb ON tb.id=t.tour_id
        WHERE t.tour_id =".db_input($tour_id);
        return db_query($sql);
    }

    public static function searchNumberPhone($phone_number) {
        $sql = "SELECT DISTINCT t.tour_id
                FROM ost_pax_tour t
                WHERE t.phone_number = ".db_input(trim($phone_number));

        $res = db_query($sql);
        $list = [];
        while ($res && ($row = db_fetch_array($res))){
            $list[] = $row['tour_id'];
        }
        return $list;
    }

    public static function searchPassport($passport_no) {

        $sql = "SELECT DISTINCT t.tour_id
                FROM ost_pax_info i
                JOIN ost_pax_tour t ON i.id=t.pax_info
                WHERE i.passport_no = ".db_input(trim($passport_no));

        $res = db_query($sql);
        $list = [];
        while ($res && $row = db_fetch_array($res)) {
            $list[] = $row['tour_id'];
        }
        return $list;
    }

    public static function searchNamePax($name_pax){
        $sql = "SELECT DISTINCT opt.tour_id FROM ost_pax as op JOIN ost_pax_tour as opt
                ON op.id = opt.pax_id WHERE op.full_name LIKE '%".$name_pax."%' ";

        $res = db_query($sql);
        $list = [];
        while ($res && $row = db_fetch_array($res)) {
            $list[] = $row['tour_id'];
        }
        return $list;
    }

    public static function sreachStaff($staff_id){
        $sql = "SELECT DISTINCT opt.tour_id FROM ".VISA_TOUR_PAX_PROFILE." as vtp JOIN ost_pax_tour as opt
                ON vtp.id = opt.tour_pax_visa_profile_id WHERE vtp.staff_id =".db_input($staff_id);
        $res = db_query($sql);
        $list = [];
        while ($res && $row = db_fetch_array($res)) {
            $list[] = $row['tour_id'];
        }
        return $list;
    }

    public static function countVisaStatus($arr_id_tour, $arr_filter_status){
        if(empty($arr_id_tour)) return [];
        $sql = "SELECT opt.tour_id, count(opt.pax_id) as num FROM ost_pax_tour opt 
                    LEFT JOIN tour_pax_visa_profile pf ON opt.tour_pax_visa_profile_id = pf.id
                WHERE opt.tour_id IN (".implode(',', db_input($arr_id_tour)).")";
        if(!empty($arr_filter_status))
            $sql .= " AND pf.visa_status_id IN (".implode(',', db_input($arr_filter_status)).")";
        $sql .= " GROUP BY opt.tour_id";

        $res = db_query($sql);
        $data = [];
        while ($res && ($row = db_fetch_array($res))){
            $data[$row['tour_id']] = $row['num'];
        }
        return $data;
    }
}
class VisaTourGuestModel extends VerySimpleModel {
    static $meta = [
        'table' => PAX_TOUR_TABLE,
        'pk' => ['id'],
    ];
}
class VisaTourGuest extends VisaTourGuestModel
{
    public static function getPaginationTourGuest(array $params, $offset, $limit = 30, &$total)
    {
        $sql = "SELECT DISTINCT p.id, p.full_name, p.dob, p.gender, i.passport_no, i.doe, i.nationality, t.booking_code, t.phone_number, t.room, t.note, tl,
            t.visa_estimated_due_date, t.apply_date, t.receiving_date,t.tour_id, t.visa_result, v.staff,v.ticket_id,
            DATEDIFF(i.doe, DATE_ADD(ot.return_time, INTERVAL 61 DAY)) as doe_after, ot.gateway_present_time,
            t.tour_pax_visa_profile_id as `profile_id`
            FROM ost_pax_tour t
                 JOIN ost_pax p ON p.id = t.pax_id
                 JOIN ost_pax_info i ON i.id = t.pax_info
                 JOIN booking_view v ON v.booking_code_trim = t.booking_code_trim
                 JOIN tour ot ON ot.id = t.tour_id
                 JOIN tour_pax_visa_profile pr ON pr.id=t.tour_pax_visa_profile_id
                 WHERE 1 ";

        $limit = " LIMIT $limit OFFSET $offset";

        if ($params['name']) {
            $sql .= " AND p.full_name LIKE " . db_input('%' . ($params['name']) . '%') . " ";
        }
        if ($params['phone_number']) {
            $sql .= " AND t.phone_number = " . db_input($params['phone_number']);
        }
        if ($params['booking_code']) {
            $sql .= " AND (trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM t.booking_code))) LIKE " . db_input(_String::trimBookingCode($params['booking_code']));
        }
        if ($params['passport_code']) {
            $sql .= " AND i.passport_no = " . db_input($params['passport_code']);
        }
        if ($params['dob']) {
            $sql .= " AND dob = " . db_input($params['dob']);
        }
        if ($params['doe']) {
            $sql .= " AND doe = " . db_input($params['doe']);
        }
        if($params['visa_apply_date']){
            $sql .= " AND apply_date = ".db_input($params['visa_apply_date']);
        }
        if($params['est_due_date']){
            $sql .= " AND visa_estimated_due_date = ".db_input($params['est_due_date']);
        }
        if($params['visa_result']){
            $arr_id_profile = VisaTourGuest::searchVisaStatus(trim($params['visa_result']));
            if(!empty($arr_id_profile))
                $sql .= " AND t.tour_pax_visa_profile_id IN "."('".implode("','",$arr_id_profile)."')";
            else
                $sql .= " AND 1=0 ";
        }
        if($params['keyword_tour']) {
            $sql .= " AND name LIKE " . db_input('%' . ($params['keyword_tour']) . '%') . " ";
        }

        if(isset($params['destination'])){
            $group_des = $params['destination'];
            if(isset($group_des) && is_array($group_des) && count(array_unique(array_filter($group_des)))) {
                $sql .= " AND destination IN (" . implode(",", $group_des) . ") ";
            }else{
                $sql .= " AND 1 = 0";
            }
        }

        if($params['staff']){
            $pax_id = self::getListPaxidOfSatff($params['staff']);
            if($pax_id)
                $sql .= " AND t.pax_id IN "."('".implode("','",$pax_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        $order = " ORDER BY pr.updated_at ";

        $res = db_query("$sql $order $limit");
        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function getListPaxidOfSatff($staff_id)
    {
        $sql = "SELECT DISTINCT opt.pax_id FROM ".VISA_TOUR_PAX_PROFILE." as vtp JOIN ost_pax_tour as opt
                ON vtp.id = opt.tour_pax_visa_profile_id WHERE vtp.staff_id =".db_input($staff_id);
        $res = db_query($sql);
        $list = [];
        while ($res && $row = db_fetch_array($res)) {
            $list[] = $row['pax_id'];
        }
        return $list;
    }

    public static function getPaxFromId($id,$pax_tour)
    {
        $sql = "SELECT * FROM " . OST_PAX_TABLE . " as pax 
        JOIN " . OST_PAX_TOUR_TABLE . " as pax_tour ON  pax.id = pax_tour.pax_id 
        JOIN ost_pax_info as pax_info ON pax_tour.pax_info = pax_info.id 
        WHERE pax_tour.pax_id = " . $id . " and pax_tour.tour_id = " . $pax_tour;
        return db_query($sql);
    }

    public static function insert_visa_profile($id,$pax_tour,$id_profile)
    {
        $sql = "UPDATE ".OST_PAX_TOUR_TABLE." as pax_tour SET pax_tour.tour_pax_visa_profile_id = ".$id_profile." WHERE pax_tour.pax_id = ".$id." and pax_tour.tour_id = ".$pax_tour;
        return db_query($sql);
    }

    public static function getStatusProfile($guest_id_status)
    {
        if(!isset($guest_id_status) && empty($guest_id_status)) return null;

        $sql = "SELECT st.visa_status_id as visa_status FROM ".VISA_TOUR_PAX_PROFILE." st
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = st.id
        WHERE opt.pax_id =".db_input($guest_id_status);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row;
    }

    public static function getPassportLocation($guest_id_location)
    {
        if(!isset($guest_id_location) && empty($guest_id_location)) return null;

        $sql = "SELECT pl.name,pl.id,tp.id FROM ".PASSPORT_LOCATION_TABLE." pl
            JOIN ".VISA_TOUR_PAX_PROFILE." tp ON pl.id = tp.passport_location_id
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = tp.id
        WHERE opt.pax_id =".db_input($guest_id_location);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row;
    }

    public static function getAppointment($guest_id_appointment, $future = false)
    {
        $res = static::getAllAppointments($guest_id_appointment, $future);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row;
    }

    public static function getAllAppointments($guest_id_appointment, $future = false)
    {
        if(!isset($guest_id_appointment) && empty($guest_id_appointment)) return null;

        $sql = "SELECT va.name,vpa.date_event FROM ".VISA_APPOINTMENT." va
            JOIN ".VISA_PROFILE_APPOINTMENT." vpa ON vpa.visa_appointment_id = va.id
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = vpa.tour_pax_visa_profile_id 
        WHERE opt.tour_pax_visa_profile_id =".db_input($guest_id_appointment)
            . ($future ? " AND vpa.date_event >= NOW() " : " ")
            . " ORDER BY vpa.date_event DESC ";
        return db_query($sql);
    }

    public static function countAppointments($guest_id_appointment, $future = false)
    {
        $res = static::getAllAppointments($guest_id_appointment, $future);
        if (!$res) return 0;
        return db_num_rows($res);
    }

    public static function getDeadline($profile_id)
    {
        if(!isset($profile_id) && empty($profile_id)) return null;

        $sql = "SELECT deadline FROM ".VISA_TOUR_PAX_PROFILE." 
        WHERE id = ".db_input($profile_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row || !isset($row['deadline']) || !$row['deadline']) return null;
        return $row['deadline'];
    }

    public static function getVisaCode($tour_id)
    {
        if(!isset($tour_id) && empty($tour_id)) return null;

        $sql = "SELECT vpa.visa_code as deadline FROM ".VISA_TOUR_PAX_PROFILE." vpa 
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = vpa.id 
            JOIN ".PAX_INFO_TABLE." opf ON opf.id = opt.pax_info
            JOIN ".OST_PAX_TABLE." op ON op.id = opf.pax_id
        WHERE opt.tour_id =".db_input($tour_id);
        return db_query($sql);
    }

    public static function getNoteAppointment($guest_id)
    {
        if(!isset($guest_id) && empty($guest_id)) return null;

        $sql = "SELECT vpa.note as note FROM ".VISA_PROFILE_APPOINTMENT." vpa 
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = vpa.tour_pax_visa_profile_id 
            JOIN ".PAX_INFO_TABLE." opf ON opf.id = opt.pax_info
            JOIN ".OST_PAX_TABLE." op ON op.id = opf.pax_id
            WHERE op.id = ".db_input($guest_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row;

    }

    public static function getMinDeadlineTour($tour_id)
    {
        if(!isset($tour_id) && empty($tour_id)) return null;

        $sql = "SELECT MIN(vpa.deadline) as mindeadline FROM ".VISA_TOUR_PAX_PROFILE." vpa 
            JOIN ".OST_PAX_TOUR_TABLE." opt ON opt.tour_pax_visa_profile_id = vpa.id 
        WHERE opt.tour_id = ".db_input($tour_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row['mindeadline'];
    }

    public static function getValuesSendGroupSms($datas_guest_id, $datas_tour_id)
    {
        if(!isset($datas_guest_id) && !$datas_guest_id) return NULL;
        if(!isset($datas_tour_id) && !$datas_tour_id) return NULL;

        $sql = 'SELECT DISTINCT p.id,p.full_name,t.booking_code,t.phone_number,tt.name, tt.id as `tour_id` FROM '.PAX_TABLE.' p 
            JOIN '.PAX_INFO_TABLE.' i ON p.id=i.pax_id
            JOIN '.PAX_TOUR_TABLE.' t ON p.id=t.pax_id AND i.id=t.pax_info
            JOIN '.TOUR_NEW_TABLE.' tt ON t.tour_id=tt.id';

        $sql .=' WHERE t.pax_id IN ('.implode(',', db_input($datas_guest_id)).')';
        $sql .=' AND t.tour_id IN ('.implode(',', db_input($datas_tour_id)).')';

        $res = db_query($sql);
        if(!$res) return NULL;
        return $res;
    }

    public static function _sendSMSVisa($vars,&$errors,$profile_id)
    {
        global $thisstaff;

        if (!$thisstaff) return false;

        $phone_number = $vars['phone_number'];

        if (!_String::simpleCheckMobilePhone($phone_number)) return false;

        $vars['sms_content'] = trim($vars['sms_content']);

        $sms_content = _String::khongdau($vars['sms_content']);
        $errors = [];
        $send_sms = _SMS::send($phone_number, $sms_content, $errors, 'Visa Dept');
        if($send_sms){
            $datas_profile_sms = [
                'profile_id' => $profile_id,
                'content' => $sms_content,
                'phone_number' => $phone_number,
                'created_by' => $thisstaff->getId(),
                'send_time' => date('Y-m-d H:i:s')
            ];
            $news = ProfileSMS::create($datas_profile_sms);
            $news->save(true);
            //add log action send sms
            if(isset($vars['time_action_log']) && $vars['time_action_log'] !== '')
                $time_action_log = $vars['time_action_log'];
            else $time_action_log = date('Y-m-d H:i:s');
            \Tugo\VisaFollowAction::addLogAction((int)$vars['tour_pax_visa_profile_id'], $news->ht,
                                    \Tugo\VisaFollowAction::VISA_SENT_SMS, $time_action_log);
        }
        return true;
    }

    public static function _sendSMSProfileGroup($vars, &$errors, &$mess)
    {
        global $thisstaff;

        if (!isset($vars) && empty($vars)) {
            return false;
        }

        $vars['sms_content_group'] = trim($vars['sms_content_group']);
        $content_group = _String::khongdau($vars['sms_content_group']);

        $vars['sms_phone_number_group'] = array_unique($vars['sms_phone_number_group']);

        foreach ($vars['sms_phone_number_group'] as $key => $phone_number) {
            if (!_String::simpleCheckMobilePhone($phone_number)) {
                $errors[] = true;
                $mess[$phone_number] = 'Không thể gửi tin nhắn';
                continue;
            }
            $send_sms = _SMS::send($phone_number, $content_group, $errors);
            if (!$send_sms) {
                $errors[] = true;
                $mess[] = $phone_number.' -> Gửi không thành công';
            }
            $data_profile_sms = [
                'profile_id'  => intval(trim($vars['id'][$key])),
                'content'     => $content_group,
                'phone_number'=> $phone_number,
                'created_by'  => $thisstaff->getId(),
                'send_time'   => date('Y-m-d H:i:s')
            ];
            $news = ProfileSMS::create($data_profile_sms);
            $news->save(true);
        }
    }

    public static function getProfileVisaPax($pax_id, $tour_id){
        if(!isset($pax_id) || !$pax_id || !isset($tour_id) || !$tour_id)
            return false;

        $profile = PaxTour::lookup(['pax_id' => $pax_id, 'tour_id' => $tour_id]);
        if($profile && $profile->tour_pax_visa_profile_id)
        {
            $id_profile = $profile->tour_pax_visa_profile_id;
            $profile_pax = \Tugo\VisaTourPaxProfile::lookup($id_profile);
            if($profile_pax)
                return $profile_pax;
        }
        return false;
    }

    public static function searchVisaStatus($status){
        $arr_id_proflie = [];
        $sql = "SELECT id from tour_pax_visa_profile where visa_status_id = ". $status;
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))){
            $arr_id_proflie[] = $row['id'];
        }
        return $arr_id_proflie;
    }
}

class VisaStatus extends GeneralObject {
    const Enable = 1;
    const Disable = 2;
}

class ProfileSMSModel extends VerySimpleModel {
    static $meta = [
        'table' => VISA_PROFILE_SMS_LOG_TABLE,
        'pk' => ['id']
    ];
}
class ProfileSMS extends ProfileSMSModel{
    public static function getValuesProfileSms($profile_id)
    {
        $sql = "SELECT * FROM ". self::$meta['table']." WHERE profile_id = ".$profile_id. " ORDER BY `send_time` DESC";
        return db_query($sql);
    }
}

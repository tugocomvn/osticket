<h2>Operator <small>Lists</small></h2>
<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

?>
<style>
    table.list tbody td {
        padding: 0.75em;
    }
</style>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <tbody>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/operator.php?id=".AIRLINE_LIST ?>">Airlines</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/tour-list.php" ?>">Tours</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/operator.php?id=".TOUR_LEADER_LIST ?>">Tour Leaders</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/operator.php?id=".TRANSIT_AIRPORT ?>">Transit Airport</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/market_manage_item.php"?>">Market List</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/list-tour-review.php" ?>">List Tour Review</a></td></tr>
    </tbody>
</table>
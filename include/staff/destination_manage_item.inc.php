<?php

if (!defined('OSTSCPINC') || !$thisstaff) {
    die('Invalid path');
}


?>
<style>
    table.list tbody td {
        padding: 0.75em;
    }
</style>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl().'scp/market_manage_item.php' ?>">Back</a>
<h2>Quản lí nơi đến của thị trường: <?php echo $name_market ?></h2>
<div class="pull-right flush-right" style="padding-right:5px;height: 20px"><b><a href="<?php echo $cfg->getUrl()?>scp/destination_manage_item.php?action=create&market=<?php echo (int)$_REQUEST['market'] ?>" class="Icon newstaff">Add new</a></b></div>

<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <thead>
        <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Chỉnh sửa</th>
        </tr>
    </thead>
    <tbody>
    <?php $i = 0; foreach ($tour_destination as $destination):?>
        <tr>
            <td style="width: 50px" class="centered"><?php echo ++$i ?></td>
            <td>
                <?php echo $destination->name ?>
                <?php if(!$destination->status):?>
                    <span class="label label-default label-small">Hide</span>
                <?php endif;?>
            </td>
            <td class="autohide-buttons">
                <a href="<?php echo $cfg->getUrl().'scp/destination_manage_item.php?action=edit&market='.(int)$_REQUEST['market'].'&destination='.(int)$destination->id?>"
                   class="btn_sm btn-xs btn-default no-pjax" data-title="Edit destination" ><i class="icon-edit"></i></a>
            </td>
        </tr>
    <?php endforeach;?>
    <?php if($i === 0):?>
        <tr>No Item</tr>
    <?php endif;?>
    </tbody>
</table>



<?php
/*********************************************************************
 * class.nav.php
 *
 * Navigation helper classes. Pointless BUT helps keep navigation clean and free from errors.
 *
 * Peter Rotich <peter@osticket.com>
 * Copyright (c)  2006-2013 osTicket
 * http://www.osticket.com
 *
 * Released under the GNU General Public License WITHOUT ANY WARRANTY.
 * See LICENSE.TXT for details.
 *
 * vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require_once(INCLUDE_DIR . 'class.app.php');
include_once INCLUDE_DIR . 'class.offlate.php';

class StaffNav {

    var $activetab;
    var $activeMenu;
    var $panel;

    var $staff;

    function StaffNav($staff, $panel = 'staff') {
        $this->staff = $staff;
        $this->panel = strtolower($panel);
    }

    function __get($what) {
        // Lazily initialize the tabbing system
        switch ($what) {
            case 'tabs':
                $this->tabs = $this->getTabs();
                break;
            case 'submenus':
                $this->submenus = $this->getSubMenus();
                break;
            default:
                throw new Exception($what . ': No such attribute');
        }
        return $this->{$what};
    }

    function getPanel() {
        return $this->panel;
    }

    function isAdminPanel() {
        return (!strcasecmp($this->getPanel(), 'admin'));
    }

    function isStaffPanel() {
        return (!$this->isAdminPanel());
    }

    function getRegisteredApps() {
        return Application::getStaffApps();
    }

    function setTabActive($tab, $menu = '') {

        if ($this->tabs[$tab]) {
            $this->tabs[$tab]['active'] = true;
            if ($this->activetab && $this->activetab != $tab && $this->tabs[$this->activetab])
                $this->tabs[$this->activetab]['active'] = false;

            $this->activetab = $tab;
            if ($menu) $this->setActiveSubMenu($menu, $tab);

            return true;
        }

        return false;
    }

    function setActiveTab($tab, $menu = '') {
        return $this->setTabActive($tab, $menu);
    }

    function getActiveTab() {
        return $this->activetab;
    }

    function setActiveSubMenu($mid, $tab = '') {
        if (is_numeric($mid))
            $this->activeMenu = $mid;
        elseif ($mid && $tab && ($subNav = $this->getSubNav($tab))) {
            foreach ($subNav as $k => $menu) {
                if (strcasecmp($mid, $menu['href'])) continue;

                $this->activeMenu = $k + 1;
                break;
            }
        }
    }

    function getActiveMenu() {
        return $this->activeMenu;
    }

    function addSubMenu($item, $active = false) {

        // Triger lazy loading if submenus haven't been initialized
        isset($this->submenus[$this->getPanel() . '.' . $this->activetab]);
        $this->submenus[$this->getPanel() . '.' . $this->activetab][] = $item;
        if ($active)
            $this->activeMenu = sizeof($this->submenus[$this->getPanel() . '.' . $this->activetab]);
    }


    function getTabs() {
        $staff = $this->staff;
        if (!$staff) return [];
        if (!$this->tabs) {
            $this->tabs = array();
            if (
                $staff->can_view_agent_statistics()
                || $staff->can_view_all_checkin()
                || $staff->can_view_personal_checkin()
                || $staff->can_view_ticket_activity()
                || $staff->can_view_general_statistics()
                || $staff->can_view_agent_directory()
            ) {
                $this->tabs['dashboard'] = array('desc' => __('Dashboard'), 'href' => 'dashboard.php', 'title' => __('Agent Dashboard'), "class" => "no-pjax");
            }

            if ($staff->can_view_guest_directory()
                || $staff->can_edit_guest_info()) {
                $this->tabs['users'] = array('desc' => __('Guests'), 'href' => 'users.php', 'title' => __('Guest Directory'));
            }

            if ($staff->canEditTickets()
                || $staff->canCreateTickets()
                || $staff->canManageTickets()) {
                $this->tabs['tickets'] = array('desc' => __('Tickets'), 'href' => 'tickets.php', 'title' => __('Ticket Queue'));
                $this->tabs['todo'] = array('desc' => __('To do'), 'href' => 'todo.php', 'title' => __('To do'));
            }

            if ($staff->canViewOperatorLists() || $staff->canEditOparetorLists() || $staff->canCreateOperatorListItem())
                $this->tabs['operator'] = array('desc' => __('Operator'), 'href' => 'operator.php', 'title' => __('Operator'));

            if ($staff->canViewPayments() || $staff->canCreatePayments())
                $this->tabs['finance'] = array('desc' => __('Finance'), 'href' => 'io.php', 'title' => __('Finance'));

            if ($staff->canViewBookings() || $staff->canCreateBookings())
                $this->tabs['booking'] = array('desc' => __('Booking'), 'href' => 'booking.php', 'title' => __('Booking'));

            if ($staff->canViewVisaDocuments() || $staff->canManageVisaItems() || $staff->canMangeVisaPaxDocuments())
                $this->tabs['visa'] = array('desc' => __('Visa'), 'href' => 'visa-tour-list.php', 'title' => __('Visa'));

            $this->tabs['flight_ticket'] = array('desc' => __('Flight Ticket'), 'href' => 'flight-ticket.php', 'title' => __('Flight Ticket'));

            $this->tabs['analytics'] = array('desc' => __('Analytics'), 'href' => 'ticket-analytics.php', 'title' => __('Analytics'));

            if (count($this->getRegisteredApps()))
                $this->tabs['apps'] = array('desc' => __('Applications'), 'href' => 'apps.php', 'title' => __('Applications'));
        }

        return $this->tabs;
    }

    function getSubMenus() { //Private.
        global $cfg;

        $staff = $this->staff;
        $submenus = array();
        foreach ($this->getTabs() as $k => $tab) {
            $subnav = array();
            switch (strtolower($k)) {
                case 'tickets':
                    $subnav[] = array('desc' => __('Tickets'), 'href' => 'tickets.php', 'iconclass' => 'Ticket', 'droponly' => true);
                    if ($staff) {
                        $subnav[] = array(
                            'desc'      => __('My&nbsp;Tickets'),
                            'href'      => 'tickets.php?status=assigned',
                            'iconclass' => 'assignedTickets',
                            'droponly'  => true,
                        );

                        if ($staff->canCreateTickets()) {
                            $subnav[] = array(
                                'desc'      => __('New Ticket'),
                                'title'     => __('Open a New Ticket'),
                                'href'      => 'tickets.php?a=open',
                                'iconclass' => 'newTicket',
                                'id'        => 'new-ticket',
                                'droponly'  => true,
                            );
                        }
                    }
                    break;
                case 'dashboard': //
                    if ($staff) {
                        if (
                            $staff->can_view_agent_statistics()
                            || $staff->can_view_all_checkin()
                            || $staff->can_view_personal_checkin()
                            || $staff->can_view_general_statistics()
                        ) {
                            $subnav[] = array('desc' => __('Dashboard'), 'href' => 'dashboard.php', 'iconclass' => 'logs');
                        }

                        $subnav[] = array('desc' => __('Google drive'), 'href' => 'listDrive.php', 'iconclass' => 'logs');

                        if ($staff->can_view_agent_directory()) {
                            $subnav[] = array('desc' => __('Agents'), 'href' => 'directory.php', 'iconclass' => 'teams');
                        }

                        if ($staff->can_view_agent_statistics()) {
                            $subnav[] = array('desc' => __('Sales&CS Activities'), 'href' => 'statistics.php', 'iconclass' => 'users');
                        }

                        $subnav[] = array('desc' => __('Phòng Họp'), 'href' => 'phonghop.php', 'iconclass' => 'logs');
                    }
                    break;
                case 'users':
                    if ($staff) {
                        if ($staff->can_view_organizations()
                            || $staff->can_edit_organizations()
                            || $staff->can_create_organizations()) {
                            $subnav[] = array('desc' => __('Organizations'), 'href' => 'orgs.php', 'iconclass' => 'departments');
                        }
                        if ($staff->can_view_guest_directory()
                            || $staff->can_edit_guest_info()) {
                            $subnav[] = array('desc' => __('Guest Directory'), 'href' => 'users.php', 'iconclass' => 'teams');
                        }
                    }
                    break;
                case 'kbase':
                    $subnav[] = array('desc' => __('FAQs'), 'href' => 'kb.php', 'urls' => array('faq.php'), 'iconclass' => 'kb');
                    if ($staff) {
                        if ($staff->canManageFAQ())
                            $subnav[] = array('desc' => __('Categories'), 'href' => 'categories.php', 'iconclass' => 'faq-categories');
                        if ($cfg->isCannedResponseEnabled() && $staff->canManageCannedResponses())
                            $subnav[] = array('desc' => __('Canned Responses'), 'href' => 'canned.php', 'iconclass' => 'canned');
                    }
                    break;
                case 'apps':
                    foreach ($this->getRegisteredApps() as $app)
                        $subnav[] = $app;
                    break;
                case 'finance':
                    if ($staff->canCreatePayments())
                        $subnav[] = array(
                            'desc'      => __('New Payment'),
                            'title'     => __('Create a new payment'),
                            'href'      => 'tickets.php?a=io',
                            'iconclass' => 'newTicket',
                            'id'        => 'new_io',
                        );

                    if ($staff->canViewPayments())
                        $subnav[] = array(
                            'desc'      => __('Payment List'),
                            'title'     => __('Payment List'),
                            'href'      => 'io.php',
                            'iconclass' => 'logs',
                            'id'        => 'io',
                        );

                    if ($staff->canViewPayments())
                        $subnav[] = array(
                            'desc'      => __('Booking Balance'),
                            'title'     => __('Booking Balance'),
                            'href'      => 'io-booking.php',
                            'iconclass' => 'logs',
                            'id'        => 'iobooking',
                        );
                    if ($staff->canViewPaymentsChart()) {
                        $subnav[] = array(
                            'desc'      => __('Payment Chart'),
                            'title'     => __('Payment Chart'),
                            'href'      => 'io-chart.php',
                            'iconclass' => 'logs',
                            'id'        => 'iochart',
                        );

                        $subnav[] = array(
                            'desc'      => __('Expense Forecasts'),
                            'title'     => __('Expense Forecasts'),
                            'href'      => 'expenses-forecast.php',
                            'iconclass' => 'logs',
                            'id'        => 'expenses_forecast',
                        );
                    }
                    if ($staff->canCreatePayments()) {
                        //add report
                        $subnav[] = array(
                            'desc'      => __('Booking Check'),
                            'href'      => 'report_commission.php?action=list_report',
                            'iconclass' => 'logs',
                        );
                    }
                    break;

                case 'booking':
                    if ($staff->canCreateBookings())
                        $subnav[] = array(
                            'desc'      => __('New Booking'),
                            'title'     => __('Create a new booking'),
                            'href'      => 'new_booking.php?action=create',
                            'iconclass' => 'newTicket',
                            'id'        => 'new_booking',
                        );

                    if ($staff->canViewBookings()) {
                        $subnav[] = array(
                            'desc'      => __('Booking List'),
                            'title'     => __('Booking List'),
                            'href'      => 'booking.php',
                            'iconclass' => 'logs',
                            'id'        => 'booking',
                        );
                        $subnav[] = array(
                            'desc'      => __('Unfinished Bookings'),
                            'title'     => __('Unfinished Bookings'),
                            'href'      => 'unfinished.php',
                            'iconclass' => 'log-alert',
                            'id'        => 'unfinished_booking',
                        );
                        $subnav[] = array(
                            'desc'      => __('Reservation'),
                            'title'     => __('Reservation'),
                            'href'      => 'reservation.php',
                            'iconclass' => 'api',
                            'id'        => 'reservation',
                        );
                    }

                    if ($staff->canViewBookings()) {
                        $subnav[] = array(
                            'desc'      => __('Booking Reminder'),
                            'title'     => __('Booking Reminder'),
                            'href'      => 'booking_reminder.php',
                            'iconclass' => 'log-alert',
                            'id'        => 'booking_reminder',
                        );
                    }

                    if ($staff->canCreateBookings() || $staff->canCreatePayments())
                        $subnav[] = array(
                            'desc'      => __('New Receipt'),
                            'title'     => __('Create a new receipt'),
                            'href'      => 'receipt.php',
                            'iconclass' => 'newTicket',
                            'id'        => 'new_receipt',
                        );

                    if ($staff->canViewBookings() || $staff->canViewPayments())
                        $subnav[] = array(
                            'desc'      => __('Receipt List'),
                            'title'     => __('Receipt List'),
                            'href'      => 'receiptlist.php',
                            'iconclass' => 'logs',
                            'id'        => 'receipt',
                        );

                    break;

                case 'operator':
                    if ($staff->canViewOperatorLists() || $staff->canEditOparetorLists())
                        $subnav[] = array(
                            'desc'      => __('Operator Lists'),
                            'title'     => __('View All Lists'),
                            'href'      => 'operator.php',
                            'iconclass' => 'newTicket',
                            'id'        => 'new_booking',
                        );

                    if($staff->canViewApproveOperator() || $staff->canApproveOperatorReservation())
                        $subnav[] = array(
                            'desc' => __('Review Booking'),
                            'title' => 'view and approve booking',
                            'href' => 'operator_review_booking.php',
                            'iconclass' => 'users',
                            'id'=> 'op_review_booking'
                        );

                    if($staff->canEditSettlement() || $staff->canViewSettlementList())
                        $subnav[] = array(
                            'desc'      => __('Settlement Setup'),
                            'title'     => __('Settlement Setup'),
                            'href'      => 'settlement.php',
                            'iconclass' => 'logs',
                            'id'        => 'settlement_item',
                        );

                    if ($staff->canEditSettlement() || $staff->canViewExpectedSettlement())
                        $subnav[] = array(
                            'desc'      => __('Expected Settlement'),
                            'title'     => __('Expected Settlement'),
                            'href'      => 'expected_settlement.php',
                            'iconclass' => 'log-alert',
                            'id'        => 'expected_settlement',
                        );

                    if ($staff->canEditSettlement() || $staff->canViewActualSettlement())
                        $subnav[] = array(
                            'desc'      => __('Actual Settlement'),
                            'title'     => __('Actual Settlement'),
                            'href'      => 'actual_settlement.php',
                            'iconclass' => 'lists',
                            'id'        => 'actual_settlement',
                        );

                    break;
                case 'visa':
                    if ($staff->canManageVisaItems()) {
                        $subnav[] = array('desc' => __('Document Items'), 'href' => 'visa_manage_item.php', 'iconclass' => 'users', 'id' => 'visa_tour_list');
                    }
                    if ($staff->canViewVisaDocuments() || $staff->canMangeVisaPaxDocuments()) {
                        $subnav[] = array('desc' => __('Tours'), 'href' => 'visa-tour-list.php', 'iconclass' => 'users');
                        $subnav[] = array('desc' => __('Review Booking'), 'href' => 'review_booking.php', 'iconclass' => 'users');
                        $subnav[] = array('desc' => __('Guest Documents'), 'href' => 'visa-tour-guest.php', 'iconclass' => 'users');
                        $subnav[] = array('desc' => __('Passport Upload Result'), 'href' => 'list-passport-upload.php', 'iconclass' => 'users');
                    }
                    break;
                case 'flight_ticket':
                    $subnav[] = array(
                        'desc'      => __('Import'),
                        'title'     => __('Import Flight Ticket'),
                        'href'      => 'flight-ticket.php',
                        'iconclass' => 'newTicket',
                    );
                    $subnav[] = array(
                        'desc'      => __('List'),
                        'title'     => __('List Flight Ticket'),
                        'href'      => 'flight-ticket-list.php',
                        'iconclass' => 'lists',
                    );
                    break;

                case 'analytics':
                    if ($staff->can_view_agent_statistics()) {
                        $subnav[] = array('desc' => __('Sales Income'), 'href' => 'sales-income.php', 'iconclass' => 'users');
                    }

                    if ($staff->can_view_ticket_analytics()) {
                        $subnav[] = array('desc' => __('Tickets'), 'href' => 'ticket-analytics.php', 'iconclass' => 'logs');
                    }

                    if ($staff->can_view_ticket_analytics()) {
                        $subnav[] = array('desc' => __('Booking'), 'href' => 'booking-analytics.php', 'iconclass' => 'logs');
                    }

                    if ($staff->can_view_ticket_analytics()) {
                        $subnav[] = array('desc' => __('Call & SMS'), 'href' => 'call-sms-analytics.php', 'iconclass' => 'logs');
                    }

                    if ($staff->can_view_ticket_analytics() || $staff->can_view_booking_returning()) {
                        $subnav[] = array('desc' => __('Booking Returning'), 'href' => 'booking-returning.php', 'iconclass' => 'logs');
                    }
                    if ($staff->can_view_ticket_analytics() || $staff->can_view_booking_returning()) {
                        $subnav[] = array('desc' => __('Demographic'), 'href' => 'demographic.php', 'iconclass' => 'logs');
                    }
                    if ($staff->can_view_ticket_analytics()) {
                        $subnav[] = array('desc' => __('Top Customer List'), 'href' => 'top-customer-list.php', 'iconclass' => 'logs');
                    }
                    if ($staff->can_view_ticket_analytics()) {
                        $subnav[] = array('desc' => __('Staff Booking'), 'href' => 'staff-booking.php', 'iconclass' => 'logs');
                    }

                    break;
            }


            if ($subnav)
                $submenus[$this->getPanel() . '.' . strtolower($k)] = $subnav;
        }

        return $submenus;
    }

    function getSubMenu($tab = null) {
        $tab = $tab ? $tab : $this->activetab;
        return $this->submenus[$this->getPanel() . '.' . $tab];
    }

    function getSubNav($tab = null) {
        return $this->getSubMenu($tab);
    }

}

class AdminNav extends StaffNav {

    function AdminNav($staff) {
        parent::StaffNav($staff, 'admin');
    }

    function getRegisteredApps() {
        return Application::getAdminApps();
    }

    function getTabs() {

        if (!$this->tabs) {

            $tabs = array();
            $tabs['dashboard'] = array('desc' => __('Dashboard'), 'href' => 'logs.php', 'title' => __('Admin Dashboard'));
            $tabs['settings'] = array('desc' => __('Settings'), 'href' => 'settings.php', 'title' => __('System Settings'));
            $tabs['manage'] = array('desc' => __('Manage'), 'href' => 'helptopics.php', 'title' => __('Manage Options'));
            $tabs['emails'] = array('desc' => __('Emails'), 'href' => 'emails.php', 'title' => __('Email Settings'));
            $tabs['staff'] = array('desc' => __('Agents'), 'href' => 'staff.php', 'title' => __('Manage Agents'));
            $tabs['facebook'] = array('desc' => __('Facebook Tools'), 'href' => 'facebook.php', 'title' => __('Facebook Tools'));
            $tabs['tugo_app'] = array('desc' => __('Loyalty App'), 'href' => 'tugo_app.php', 'title' => __('Loyalty App'));
            if (count($this->getRegisteredApps()))
                $tabs['apps'] = array('desc' => __('Applications'), 'href' => 'apps.php', 'title' => __('Applications'));
            $this->tabs = $tabs;
        }

        return $this->tabs;
    }

    function getSubMenus() {

        $submenus = array();
        foreach ($this->getTabs() as $k => $tab) {
            $subnav = array();
            switch (strtolower($k)) {
                case 'dashboard':
                    $subnav[] = array('desc' => __('System Logs'), 'href' => 'logs.php', 'iconclass' => 'logs');
                    $subnav[] = array('desc' => __('Information'), 'href' => 'system.php', 'iconclass' => 'preferences');
                    $subnav[] = array('desc' => __('SMS'), 'href' => 'sms-api.php', 'iconclass' => 'emails');
                    $subnav[] = array('desc' => __('Call'), 'href' => 'call.php', 'iconclass' => 'emails');
                    $subnav[] = array('desc' => __('Reports'), 'href' => 'reports.php', 'iconclass' => 'Ticket');
                    $subnav[] = array('desc' => __('Agent Logs'), 'href' => 'agent.php', 'iconclass' => 'users');
                    break;
                case 'settings':
                    $subnav[] = array('desc' => __('Company'), 'href' => 'settings.php?t=pages', 'iconclass' => 'pages');
                    $subnav[] = array('desc' => __('System'), 'href' => 'settings.php?t=system', 'iconclass' => 'preferences');
                    $subnav[] = array('desc' => __('Tickets'), 'href' => 'settings.php?t=tickets', 'iconclass' => 'ticket-settings');
                    $subnav[] = array('desc' => __('Emails'), 'href' => 'settings.php?t=emails', 'iconclass' => 'email-settings');
                    $subnav[] = array('desc' => __('Access'), 'href' => 'settings.php?t=access', 'iconclass' => 'users');
                    $subnav[] = array('desc' => __('Alerts and Notices'), 'href' => 'settings.php?t=alerts', 'iconclass' => 'alert-settings');
                    $subnav[] = array('desc' => __('Call &amp; SMS'), 'href' => 'settings.php?t=sms', 'iconclass' => 'emails');
                    $subnav[] = array('desc' => __('Automation'), 'href' => 'automation_settings.php', 'iconclass' => 'email-autoresponders');
                    break;
                case 'manage':
                    $subnav[] = array('desc' => __('Help Topics'), 'href' => 'helptopics.php', 'iconclass' => 'helpTopics');
                    $subnav[] = array('desc'  => __('Ticket Filters'), 'href' => 'filters.php',
                                      'title' => __('Ticket Filters'), 'iconclass' => 'ticketFilters');
                    $subnav[] = array('desc'  => __('Auto Filters'), 'href' => 'auto-filters.php',
                                      'title' => __('Ticket Auto Filters'), 'iconclass' => 'ticketFilters');
                    $subnav[] = array('desc' => __('SLA Plans'), 'href' => 'slas.php', 'iconclass' => 'sla');
                    $subnav[] = array('desc' => __('API Keys'), 'href' => 'apikeys.php', 'iconclass' => 'api');
//                    $subnav[]=array('desc'=>__('Pages'), 'href'=>'pages.php','title'=>'Pages','iconclass'=>'pages');
                    $subnav[] = array('desc' => __('Forms'), 'href' => 'forms.php', 'iconclass' => 'forms');
                    $subnav[] = array('desc' => __('Lists'), 'href' => 'lists.php', 'iconclass' => 'lists');
                    $subnav[] = array('desc' => __('Plugins'), 'href' => 'plugins.php', 'iconclass' => 'api');
                    $subnav[] = array('desc' => __('Import Tickets'), 'href' => 'ticket-import.php', 'iconclass' => 'lists');
                    break;
                case 'emails':
                    $subnav[] = array('desc' => __('Emails'), 'href' => 'emails.php', 'title' => __('Email Addresses'), 'iconclass' => 'emailSettings');
                    $subnav[] = array('desc'  => __('Banlist'), 'href' => 'banlist.php',
                                      'title' => __('Banned Emails'), 'iconclass' => 'emailDiagnostic');
                    $subnav[] = array('desc' => __('Templates'), 'href' => 'templates.php', 'title' => __('Email Templates'), 'iconclass' => 'emailTemplates');
                    $subnav[] = array('desc' => __('Diagnostic'), 'href' => 'emailtest.php', 'title' => __('Email Diagnostic'), 'iconclass' => 'emailDiagnostic');
                    $subnav[] = array('desc' => __('Contracts'), 'href' => 'contracts.php', 'title' => __('Contract Templates'), 'iconclass' => 'emailTemplates');
                    break;
                case 'staff':
                    $subnav[] = array('desc' => __('Agents'), 'href' => 'staff.php', 'iconclass' => 'users');
                    $subnav[] = array('desc' => __('Teams'), 'href' => 'teams.php', 'iconclass' => 'teams');
                    $subnav[] = array('desc' => __('Groups'), 'href' => 'groups.php', 'iconclass' => 'groups');
                    $subnav[] = array('desc' => __('Departments'), 'href' => 'departments.php', 'iconclass' => 'departments');
                    break;
                case 'facebook':
                    $subnav[] = array('desc' => __('Custom Audiences'), 'href' => 'facebook.php', 'iconclass' => 'users');
                    break;
                case 'tugo_app':
                    $subnav[] = array('desc' => __('Users'), 'href' => 'app_users.php', 'iconclass' => 'users');
                    $subnav[] = array('desc' => __('News'), 'href' => 'app_news.php', 'iconclass' => 'users');
                    $subnav[] = array('desc' => __('Notification'), 'href' => 'app_notification.php', 'iconclass' => 'users');
                    break;
                case 'apps':
                    foreach ($this->getRegisteredApps() as $app)
                        $subnav[] = $app;
                    break;
            }
            if ($subnav)
                $submenus[$this->getPanel() . '.' . strtolower($k)] = $subnav;
        }

        return $submenus;
    }
}

class UserNav {

    var $navs = array();
    var $activenav;

    var $user;

    function UserNav($user = null, $active = '') {

        $this->user = $user;
        $this->navs = $this->getNavs();
        if ($active)
            $this->setActiveNav($active);
    }

    function getRegisteredApps() {
        return Application::getClientApps();
    }

    function setActiveNav($nav) {

        if ($nav && $this->navs[$nav]) {
            $this->navs[$nav]['active'] = true;
            if ($this->activenav && $this->activenav != $nav && $this->navs[$this->activenav])
                $this->navs[$this->activenav]['active'] = false;

            $this->activenav = $nav;

            return true;
        }

        return false;
    }

    function getNavLinks() {
        global $cfg;

        //Paths are based on the root dir.
        if (!$this->navs) {

            $navs = array();
            $user = $this->user;
            $navs['home'] = array('desc' => __('Trang chủ'), 'href' => 'index.php', 'title' => '');
            if ($cfg && $cfg->isKnowledgebaseEnabled())
                $navs['kb'] = array('desc' => __('Knowledgebase'), 'href' => 'kb/index.php', 'title' => '');

            // Show the "Open New Ticket" link unless BOTH client
            // registration is disabled and client login is required for new
            // tickets. In such a case, creating a ticket would not be
            // possible for web clients.
            if ($cfg->getClientRegistrationMode() != 'disabled'
                || !$cfg->isClientLoginRequired())
                $navs['new'] = array('desc' => __('Tạo phiếu hỗ trợ'), 'href' => 'open.php', 'title' => '');
            if ($user && $user->isValid()) {
                if (!$user->isGuest()) {
                    $navs['tickets'] = array('desc'  => sprintf(__('Tất cả hỗ trợ (%d)'), $user->getNumTickets(true)),
                                             'href'  => 'tickets.php',
                                             'title' => __('Tất cả hỗ trợ'));
                } else {
                    $navs['tickets'] = array('desc'  => __('View Ticket Thread'),
                                             'href'  => sprintf('tickets.php?id=%d', $user->getTicketId()),
                                             'title' => __('View ticket status'));
                }
            } else {
                $navs['status'] = array('desc' => __('Kiểm tra'), 'href' => 'tickets.php', 'title' => '');
            }
            $this->navs = $navs;
        }

        return $this->navs;
    }

    function getNavs() {
        return $this->getNavLinks();
    }

}

?>

CREATE INDEX ost_ticket_lastmessage_index ON osticket_dev.ost_ticket (lastmessage);
CREATE INDEX ost_ticket_lastresponse_index ON osticket_dev.ost_ticket (lastresponse);
CREATE INDEX ost_ticket_updated_index ON osticket_dev.ost_ticket (updated);
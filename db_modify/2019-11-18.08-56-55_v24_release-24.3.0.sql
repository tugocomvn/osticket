alter table tugo_osticket_call
	add phone_number varchar(12) null;

ALTER TABLE `tugo_osticket_call`
    DROP INDEX `index2`;

CREATE  UNIQUE INDEX `index2`on `tugo_osticket_call` (`day_of_week_name` ASC, `year` ASC, `month_of_year` ASC, `month_of_the_year` ASC, `minute`
    ASC, `week_of_year` ASC, `week_of_the_year` ASC, `hour` ASC, `day_of_week` ASC,
    `day_of_month` ASC, `hour_of_day` ASC, `date` ASC, `phone_number` ASC);

alter table tugo_osticket_sms
    add phone_number varchar(12) null;

ALTER TABLE `tugo_osticket_sms`
    DROP INDEX `index2`;

CREATE  UNIQUE INDEX `index2`on `tugo_osticket_sms` (`day_of_week_name` ASC, `year` ASC, `month_of_year` ASC, `month_of_the_year` ASC, `minute`
    ASC, `week_of_year` ASC, `week_of_the_year` ASC, `hour` ASC, `day_of_week` ASC,
    `day_of_month` ASC, `hour_of_day` ASC, `date` ASC, `phone_number` ASC);

ALTER TABLE receipt
    ADD `sales_cashier_id_confirmed` INT NULL,
    ADD `sales_cashier_confirmed_at` DATETIME NULL;


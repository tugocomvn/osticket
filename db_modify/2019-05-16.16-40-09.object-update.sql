create table object_update
(
  object_id int not null,
  object_type_id int not null,
  update_value varchar(128) null,
  created_at datetime null,
  created_by int null,
  approved_at datetime null,
  approved_by int null,
  constraint object_update_pk
    primary key (object_id, object_type_id)
);


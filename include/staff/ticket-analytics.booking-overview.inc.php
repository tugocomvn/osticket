<?php
$table_data = TicketAnalytics::bookingAnalytics($_REQUEST['from'], $_REQUEST['to'], 'all_booking', $_REQUEST);
$booking_percent = $booking_overview = [];
$booking_count = 0;
$total_booking = 0;
while($table_data && ($row = db_fetch_array($table_data))) {
    $booking_count++;

    if ($booking_count >= 10) {
        if (!isset($booking_overview[ 'Other' ]))
            $booking_overview[ 'Other' ] = 0;

        $booking_overview[ 'Other' ] = $booking_overview[ 'Other' ] + $row['total'];
    } else {
        $booking_overview[ _String::json_decode($row['booking_type']) ] = $row['total'];
    }
    $total_booking += $row['total'];
}

foreach ($booking_overview as $_key => $_value) {
    $booking_percent[$_key] = round($_value * 100 / $total_booking);
}
?>
<td>
    <h2>Booking Overview</h2>
    <p>Unit: percent %</p>
    <canvas id="bookings" width="300" height="300"></canvas>
</td>
<td>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Thị trường</th>
            <th>Số lượng</th>
        </tr>
        </thead>
        <?php foreach($booking_overview as $_name => $_quantity): ?>
            <tr>
                <td><?php echo $_name ?></td>
                <td><?php echo $_quantity ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</td>
<script>
    var bookings_elm = document.getElementById("bookings").getContext('2d');
    var bookings_chart = new Chart(bookings_elm, {
        type: 'pie',
        data: {
            labels: $.parseJSON('<?php echo json_encode(array_keys($booking_percent), JSON_HEX_APOS) ?>'),
            datasets: [
                {
                    backgroundColor: poolColors(<?php echo count($booking_percent) ?>),
                    data: $.parseJSON('<?php echo json_encode(array_values($booking_percent)) ?>')
                }
            ]
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
            cutoutPercentage: 30
        }
    });
</script>

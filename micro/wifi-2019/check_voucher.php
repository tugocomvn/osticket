
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf8">
    <title>Kiểm tra mã voucher | Du lịch Tugo Travel</title>

    <style>
        .container {
            padding-top: 0.5rem;
            margin-top: 1rem;
            padding-bottom: 20px;
        }
    </style>
</head>
<body>
<div class="container bg-light rounded" style="width: 70%">
    <h3 class="mt-5 text-center text-info">Kiểm tra mã voucher</h3>
    <form  action="back_check_voucher.php" method="get" >
        <div class="input-group mb-3 mt-3 ">
            <input type="text" name="code" class="form-control border border-primary" placeholder="Nhập mã voucher của quý khách" aria-label="Recipient's username" aria-describedby="button-addon2">
            <div class="input-group-append">
                <button class="btn btn btn-info" type="submit" id="check">Kiểm tra</button>
            </div>
        </div>
    </form>

    <?php
    if(!is_null($str_result)): ?>
    <h4 class="text-primary">Kết quả</h4>
    <div class="alert alert-info" role="alert">
        <p><?php echo $str_result; ?></p>
    </div>
    <?php endif; ?>
</div>
</body>
<footer></footer>
</html>
ALTER TABLE ost_groups ADD can_view_booking TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_create_booking TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_groups ADD can_edit_booking TINYINT(1) DEFAULT 0 NULL;

<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');

$id = intval(trim($_REQUEST['id']));
$booking = Booking::get_booking_view($id);

$sql = sprintf(
    "SELECT B.*, s.username, s.firstname, s.lastname FROM (
    SELECT created_at, created_by, content, 'note' as type 
        FROM %s WHERE booking_ticket_id = %s 
        UNION 
        SELECT sl.updated_at as created_at, sl.updated_by as created_by, li.name as content, 'status' as type 
            FROM %s sl JOIN %s li ON li.id=sl.booking_status_id  WHERE booking_ticket_id = %s
    ) as B LEFT JOIN %s s ON s.staff_id=B.created_by ORDER BY created_at DESC",
    BOOKING_NOTE_TABLE,
    db_input($id),
    BOOKING_STATUS_LOG_TABLE,
    BOOKING_STATUS_LIST_TABLE,
    db_input($id),
    STAFF_TABLE
);
$res = db_query($sql);
?>
<p>
    <a href="<?php echo $referer ?>" class="btn_sm btn-xs btn-default">Back</a>
</p>

<div>
    <h2 class="pull-left"><?php echo __('Unfinished Bookings');?> <small>Logs</small></h2>
    <h3 class="pull-right">Booking: <?php echo $booking['booking_code'] ?></h3>
    <div class="clearfix"></div>
</div>

 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th><?php echo __('STT');?></th>
            <th><?php echo __('Ngày');?></th>
            <th><?php echo __('Người cập nhật');?></th>
            <th><?php echo __('Nội dung');?></th>
            <th><?php echo __('Loại');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $total=0;
        if($res):
            $i = 1;
            while ($row = db_fetch_array($res)) {
                ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row['created_at'] ?></td>
                <td><?php echo $row['username'] ?> (<?php echo $row['firstname'] ?> <?php echo $row['lastname'] ?>)</td>
                <td><?php echo $row['content'] ?></td>
                <td><?php echo $row['type'] ?></td>
            </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
</table>
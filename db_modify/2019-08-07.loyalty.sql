ALTER TABLE `ost_groups` ADD `can_view_member_list` TINYINT(1) NOT NULL DEFAULT '0', 
ADD `can_edit_point_loyalty` TINYINT(1) NOT NULL DEFAULT '0' AFTER `can_view_member_list`;

ALTER TABLE `api_user` ADD `dob` DATE NULL AFTER `rank`;

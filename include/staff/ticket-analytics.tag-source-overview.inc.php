<style>
    .list_2d {
        border-collapse: collapse;
    }

    .list_2d td {
        padding: 1em;
        border: 1px solid #eee;
    }
</style>
<?php
$chart_data_sources = [];
$table_data_sources = [];
$source_by_day_chart = [];

TicketAnalytics::analytics($_REQUEST['from'], $_REQUEST['to'], 'tag_and_source_by_day', $_REQUEST, $chart_data_sources, $table_data_sources);
//
//
$table_data_sources_sort = [];
$source_list = [];
$total_ticket_source_tag = 0;
$max_ticket_source_tag = 0;
while($table_data_sources && ($row = db_fetch_array($table_data_sources))) {
    $source_list[ $row['sourcex'] ] = $row['sourcex'];

    if (isset($tags[ $row['tag_id'] ])) {
        $table_data_sources_sort[ $tags[ $row['tag_id'] ] ][ $row['sourcex'] ] = $row['total'];
    } else {
        if (!isset($table_data_sources_sort[ 'Other' ][ $row['sourcex'] ]))
            $table_data_sources_sort[ 'Other' ][ $row['sourcex'] ] = 0;

        $table_data_sources_sort[ 'Other' ][ $row['sourcex'] ] += $row['total'];
    }

    $total_ticket_source_tag += $row['total'];
    if ($row['total'] > $max_ticket_source_tag)
        $max_ticket_source_tag = $row['total'];
}

$source_list = array_unique(array_filter($source_list));
//
//

$source_total = $source_by_day_chart = $date_list = [];
while($chart_data_sources && ($row = db_fetch_array($chart_data_sources))) {
    if (!$row['sourcex']) $row['sourcex'] = 3;

    $_index = $_name = '';

    if (!isset($source_by_day_chart[ $row['sourcex'] ]))
        $source_by_day_chart[ $row['sourcex'] ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $_index = $row['week'].'-'.$row['year'];
            break;
        case 'month':
            $_index = $row['month'].'-'.$row['year'];
            break;
        case 'day':
        default:
            $_index = date(DATE_FORMAT, strtotime($row['date']));
            break;
    }

    $date_list[] = $_index;

    if (!isset($source_total[ $_name ])) $source_total[ $_name ] = 0;

    if (isset($tags[ $row['tag_id'] ])) {
        $_name = ( isset(TicketAnalytics::$source_name[ $row['sourcex'] ])
                ? TicketAnalytics::$source_name[ $row['sourcex'] ] : 'Other' )
            .' / '.$tags[ $row['tag_id'] ];
        $source_by_day_chart[ $_name ][ $_index ] = $row['total'];
        $source_total[ $_name ] += $row['total'];
    } else {
        $_name = ( isset(TicketAnalytics::$source_name[ $row['sourcex'] ])
                ? TicketAnalytics::$source_name[ $row['sourcex'] ] : 'Other' )
            .' / Other';
        if (!isset($source_by_day_chart[ 'Other' ][ $row['sourcex'] ]))
            $source_by_day_chart[ $_name ][ $_index ] = 0;

        $source_by_day_chart[ $_name ][ $_index ] += $row['total'];
        $source_total[ $_name ] += $row['total'];
    }
}

$date_list = array_filter(array_unique($date_list));

foreach ($source_by_day_chart as $_source => $_date_data) {
    foreach ($date_list as $_date) {
        if (!isset($source_by_day_chart[ $_source ][ $_date ]))
            $source_by_day_chart[ $_source ][ $_date ] = 0;
    }

    uksort($source_by_day_chart[ $_source ], "___date_compare");
}

arsort($source_total);

//$source_total = array_slice($source_total, 0, 15, true);
?>
<tr>
    <td>
        <h2>Ticket source and tag Overview</h2>
        <table class="list_2d">
            <thead>
            <tr>
                <th>#</th>
                <?php foreach($source_list as $source): ?>
                    <th><?php if (isset(TicketAnalytics::$source_name[ $source ])) echo TicketAnalytics::$source_name[ $source ]; ?></th>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach($table_data_sources_sort as $_tag => $_sources): ?>
                <tr>
                    <th><?php echo $_tag ?></th>
                    <?php foreach($source_list as $source): ?>
                        <td style="background: rgba(0, 174, 237, <?php echo ($_sources[ $source ] / $max_ticket_source_tag) ?>)"><?php echo $_sources[ $source ] ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2">
        <h2>Source / Tag by Day  <button class="btn_sm btn-xs btn-default" onclick="hide_all('source_tag_by_day')">Hide All</button></h2>
        <canvas id="source_tag_by_day" width="1000" height="800"></canvas>
    </td>
</tr>
<script>
    var colors = poolColors(<?php echo count($source_by_day_chart) ?>);
    var source_tag_by_day_elm = document.getElementById("source_tag_by_day").getContext('2d');
    i = 0;
    var source_tag_by_day = new Chart(source_tag_by_day_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($source_by_day_chart as $_source => $_date_data): if (!isset($source_total[ $_source ])) continue; ?>
                {
                    fill: true,
                    backgroundColor:  colors[<?php echo $i++ ?>],
                    label: '<?php echo htmlspecialchars($_source, ENT_QUOTES) ?>',
                    data: $.parseJSON('<?php echo json_encode(array_values($_date_data)) ?>')
                    <?php $dates = array_keys($_date_data) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },

        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>
<?php
define('ZALO_API_VERSION', 'v1');
define('OAID', '46466149071017173');
define('SECRET_KEY', '4VUVID6QO8tS8wy61PZ8');
define('OPENAPI_ZALOAPP', 'https://openapi.zaloapp.com/oa/');

class Zalo {

    public static function sendTextToZalo($message, $uid, &$error) {
        return self::_send_message_zalo($message, $uid, $error);
    }

    public static function sendLinkToZalo($links, $uid, &$error) {
        return self::_send_link_zalo($links, $uid, $error);
    }

    public static function sendTextToPhone($message, $phone_number, &$error) {
        return self::_send_message_phone($message, $phone_number, $error);
    }

    public static function sendAction($actions, $uid, &$error) {
        return self::_send_action($actions, $uid, $error);
    }

    public static function getProfile($uid, &$error) {
        return self::_get_profile($uid, $error);
    }

    public static function pushArticle($title, $author, $cover, $desc, $status, $actionLink, $relatedMedias, $body, $trackingLink, &$error) {
        return self::_push_article($title, $author, $cover, $desc, $status, $actionLink, $relatedMedias, $body, $trackingLink, $error);
    }

    private static function _send_action($actions, $uid, &$error) {
        $uid = trim($uid);
        $url = self::api_url("/sendmessage/actionlist");

        $data = [
            "uid" => $uid,
            "actionlist" => $actions,
        ];

        $output = self::curl($url, true, self::data($data));

        return self::_return($output, $error);
    }

    private static function _push_article($title, $author, $cover, $desc, $status, $actionLink, $relatedMedias, $body, $trackingLink, &$error) {
        $url = self::api_url("/media/create");

        $data = [
            'title' => $title,
            'author' => $author,
            'cover' => $cover,
            'desc' => $desc,
            'status' => $status,
            'actionLink' => $actionLink,
            'relatedMedias' => $relatedMedias,
            'body' => $body,
            'trackingLink' => $trackingLink,
        ];

        $output = self::curl($url, true, self::media($data));

        return self::_return($output, $error);
    }

    private static function _get_profile($uid, &$error) {
        $uid = trim($uid);
        $url = self::api_url("/getprofile");

        $timestamp = self::timestamp();
        $string = OAID . $uid . $timestamp . SECRET_KEY;
        $mac = hash('sha256', $string);
        $params = [
            'oaid' => OAID,
            'uid' => $uid,
            'timestamp' => $timestamp,
            'mac' => $mac,
        ];

        $output = self::curl($url, false, $params);

        $output = json_decode($output);
        if (
            $output
            && isset($output->data)
            && $output->data
            && isset($output->errorCode)
            && $output->errorCode == 1
        ) return $output->data;

        if (
            $output
            && isset($output->errorMsg)
            && $output->errorMsg
            && isset($output->errorCode)
            && $output->errorCode
        ) {
            $error[$output->errorCode] = $output->errorMsg;
        } // if
        return false;
    }

    private static function _send_message_zalo($message, $uid, &$error) {
        $uid = trim($uid);
        $url = self::api_url("/sendmessage/text");

        $data = [
            "uid" => $uid,
            "message" => $message,
        ];

        $output = self::curl($url, true, self::data($data));

        return self::_return($output, $error);
    }

    private static function _send_link_zalo($links, $uid, &$error) {
        $uid = trim($uid);
        $url = self::api_url("/sendmessage/links");

        $data = [
            "uid" => $uid,
            "links" => $links,
        ];

        $output = self::curl($url, true, self::data($data));

        return self::_return($output, $error);
    }

    private static function _send_message_phone($message, $phone_number, &$error) {
        $phone_number = trim($phone_number);
        $phone_number = trim($phone_number, '+');

        if (preg_match('/^0[^2][0-9]{8}/', $phone_number)) // 0909817306
            $phone_number = preg_replace('/^0/', '84', $phone_number);
        elseif (preg_match('/^[^02][0-9]{8}/', $phone_number)) // 909817306
            $phone_number = '84'.$phone_number;
        else {
            $error['phonenumber_format'] = 'Wrong phone number format';
            return false;
        }

        $url = self::api_url("/sendmessage/phone/text");

        $data = [
            "phone" => $phone_number,
            "message" => $message,
        ];

        $output = self::curl($url, true, self::data($data));

        return self::_return($output, $error);
    }

    private static function _return($output, &$error) {
        $output = json_decode($output);
        if (
            $output
            && isset($output->data)
            && isset($output->data->msgId)
            && $output->data->msgId
            && isset($output->errorCode)
            && $output->errorCode == 1
        ) return $output->data->msgId;

        if (
            $output
            && isset($output->errorMsg)
            && $output->errorMsg
            && isset($output->errorCode)
            && $output->errorCode
        ) {
            $error[$output->errorCode] = $output->errorMsg;
        } // if
        return false;
    }

    private static function data($data) {
        $timestamp = self::timestamp();
        $params = [
            'oaid' => OAID,
            'data' => json_encode($data),
            'timestamp' => $timestamp,
            'mac' => self::mac($data, $timestamp),
        ];

        return $params;
    }

    private static function media($data) {
        $timestamp = self::timestamp();
        $params = [
            'oaid' => OAID,
            'media' => json_encode($data),
            'timestamp' => $timestamp,
            'mac' => self::mac($data, $timestamp),
        ];

        return $params;
    }

    private static function mac($data, $timestamp) {
        $string = OAID . json_encode($data) . $timestamp . SECRET_KEY;
        $mac = hash('sha256', $string);
        return $mac;
    }

    private static function timestamp() {
        return floor(microtime(1)*1000);
    }

    private static function api_url($end_point) {
        return OPENAPI_ZALOAPP.ZALO_API_VERSION.$end_point;
    }

    /**
     * @param $url
     * @param bool $post POST if true
     */
    private static function curl($url, $post = false, $params = []) {
        $ch = curl_init();
        if (!$url) return false;

        $request_string = '?1=1';
        if (is_array($params) && count($params)) {
            foreach($params as $name => $value) {
                $request_string .= "&$name=".urlencode(is_array($value) ? json_encode($value, JSON_HEX_QUOT) : $value);
            }
        }

        // set url
        curl_setopt($ch, CURLOPT_URL, $url.$request_string);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POST, $post);
        if ($post && $params) curl_setopt($ch, CURLOPT_POSTFIELDS, $request_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch) ;
        $header  = curl_getinfo($ch);

//        file_put_contents(__DIR__.'/../../debug/zalo.content.txt', $content);
//        file_put_contents(__DIR__.'/../../debug/zalo.err.txt', $err    );
//        file_put_contents(__DIR__.'/../../debug/zalo.errmsg.txt', $errmsg );
//        file_put_contents(__DIR__.'/../../debug/zalo.header.txt', json_encode($header, JSON_HEX_QUOT) );
//        file_put_contents(__DIR__.'/../../debug/zalo.data.txt', json_encode($params, JSON_HEX_QUOT));

        // close curl resource to free up system resources
        curl_close($ch);

        return $content;
    }
}

class ZaloArticle {
    const STATUS_SHOW = 'show';
    const STATUS_HIDE = 'hide';

    public static function init($title, $author, $cover, $desc, $status, $actionLink, $relatedMedias, $body, $trackingLink) {
        return [
            'title' => $title,
            'author' => $author,
            'cover' => $cover,
            'desc' => $desc,
            'status' => $status,
            'actionLink' => $actionLink,
            'relatedMedias' => $relatedMedias,
            'body' => $body,
            'trackingLink' => $trackingLink,
        ];
    }
}

class ZaloArticleCover {
    const TYPE_PHOTO = 0;
    const TYPE_VIDEO = 1;
    const VIEW_WIDE = 1;
    const VIEW_PORTRAIT = 2;
    const VIEW_SQUARE = 3;
    public static function init($coverType, $coverView, $videoId, $photoUrl) {
        return [
            'coverType' => $coverType,
            'coverView' => $coverView,
            'videoId' => $videoId,
            'photoUrl' => $photoUrl,
        ];
    }
}

class ZaloActionLink {
    const TYPE_WEB = 0;
    const TYPE_PHOTO = 1;
    const TYPE_VIDDEO = 2;
    const TYPE_AUDIO = 3;
    public static function init($type, $label, $url) {
        return [
            'type' => $type,
            'label' => $label,
            'url' => $url,
        ];
    }
}

class ZaloRelatedMedias {
    public static function init(array $list) {
        return $list;
    }
}

class ZaloArticleBody {
    public static function init(array $list) {
        return $list;
    }
}

class ZaloBodyObject {
    const TYPE_TEXT = 0;
    const TYPE_PHOTO = 1;
    const TYPE_VIDEO = 3;
    const TYPE_PRODUCT = 4;

    public static function init($type, $object) {
        return array_merge(['type' => $type], $object);
    }
}

class ZaloBodyTextObject {
    public static function init($content) {
        return [
            'content' => $content
        ];
    }
}

class ZaloBodyPhotoObject {
    public static function init($url, $caption, $width = null, $height = null) {
        return [
            'url' => $url,
            'caption' => $caption,
            'width' => $width,
            'height' => $height,
        ];
    }
}

class ZaloAction {
    const OA_OPEN_INAPP = "oa.open.inapp";
    const OA_OPEN_OUTAPP = "oa.open.outapp";
    const OA_QUERY_SHOW = "oa.query.show";
    const OA_QUERY_HIDE = "oa.query.hide";
    const OA_OPEN_SMS = "oa.open.sms";
    const OA_OPEN_PHONE = "oa.open.phone";

    public static function init($action, $title, $description, $thumb, $href, $data, $popup) {
        return [
            "action" => $action,
            "title" => $title,
            "description" => $description,
            "thumb" => $thumb,
            "href" => $href,
            "data" => $data,
            "popup" => $popup,
        ];
    }
}

class ZaloPopup {
    public static function init($title, $desc, $ok, $cancel) {
        return [
            'title' => $title,
            'desc' => $desc,
            'ok' => $ok,
            'cancel' => $cancel,
        ];
    }
}

class ZaloLink {
    public static function init($link, $linktitle, $linkdes, $linkthumb) {
        return [
            'link' => $link,
            'linktitle' => $linktitle,
            'linkdes' => $linkdes,
            'linkthumb' => $linkthumb,
        ];
    }
}

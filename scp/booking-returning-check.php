<?php
require('staff.inc.php');
if(!$thisstaff || !$thisstaff->getId() || !$thisstaff->can_view_booking_returning()) exit;

$phone_number = null;
if (isset($_POST['phone_number']) && !empty(trim($_POST['phone_number']))) {
    $phone_number = trim($_POST['phone_number']);
}

if (!$phone_number) exit('Phone number is required');

include_once INCLUDE_DIR.'class.ticket-analytics.php';

$res = TicketAnalytics::bookingReturingPhoneNumberCheck($phone_number);
if (!$res) exit('No result');

$row = db_fetch_array($res);
$codes = explode(',', $row['booking_code']);
$dates = explode(',', $row['date']);
$new_date = [];
if (is_array($codes) && is_array($dates)) {
    foreach ($dates as $i => $date) {
        $new_date[ $date ] = $codes[$i];
    }
}
?>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Booking Code</th>
        <th>Date</th>
    </tr>
    </thead>
<tbody>
<?php
$count = 0;
if (is_array($codes) && is_array($new_date)) {
    foreach ($new_date as $_date => $_code) {
        ?>
        <tr>
            <td><?php echo (++$count) ?></td>
            <td><?php echo $_code ?></td>
            <td><?php echo $_date ?></td>
        </tr>
        <?php
    }
}
?>
</tbody>
</table>

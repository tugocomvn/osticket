<?php
/*********************************************************************
    logs.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
global $thisstaff;

if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canViewBookings()) exit('Access Denied');

include_once INCLUDE_DIR.'class.object.php';
include_once INCLUDE_DIR.'class.tour-destination.php';

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$booking_type_list = TourDestination::loadList($group_des);

$sql = $title = $type;
$pageNav = null;
get_payment_list($sql, $pageNav, $title, $type);

function get_payment_list(&$sql, &$pageNav, &$title, &$type) {
    global $thisstaff;
    $qs = array();
    $agent = $type = $booking_code = $amount = $receipt_code = $method = $dept_id = $tour = $note = $outcome_type_id = $income_type_id = null;
    if ($_REQUEST['type']) {
        $qs += array('type' => $_REQUEST['type']);
        switch (strtolower($_REQUEST['type'])) {
            case 'in':
                $title = __('Thu');
                $type = $_REQUEST['type'];
                break;
            case 'out':
                $title = __('Chi');
                $type = $_REQUEST['type'];
                break;
            default:
                $type = null;
                $title = __('All bookings');
        }
    }

    if (isset($_REQUEST['agent'])) {
        $agent = trim($_REQUEST['agent']);
        $qs += ['agent' => $_REQUEST['agent']];
    }

    if (isset($_REQUEST['receipt_code'])) {
        $receipt_code = trim($_REQUEST['receipt_code']);
        $qs += ['receipt_code' => $_REQUEST['receipt_code']];
    }
    if (isset($_REQUEST['amount'])) {
        $amount = preg_replace('/[^0-9]/', '', trim($_REQUEST['amount']));
        $qs += ['amount' => $_REQUEST['amount']];
    }
    if (isset($_REQUEST['method'])) {
        $method = trim(json_encode(trim($_REQUEST['method'])), '"');
        $qs += ['method' => $_REQUEST['method']];
    }

    if (isset($_REQUEST['dept_id'])) {
        $dept_id = trim($_REQUEST['dept_id']);
        $qs += ['dept_id' => $dept_id];
    }

    if (isset($_REQUEST['income_type_id'])) {
        $income_type_id = trim($_REQUEST['income_type_id']);
        $qs += ['income_type_id' => $income_type_id];
    }

    if (isset($_REQUEST['note'])) {
        $note = trim($_REQUEST['note']);
        $qs += ['note' => $note];
    }

    if (isset($_REQUEST['tour'])) {
        $tour = trim(json_encode(trim($_REQUEST['tour'])), '"');
        $qs += ['tour' => $tour];
    }

    $qwhere = ' WHERE 1 ';
    $qwhere .= ' AND (payment_tmp.receipt_code IS NOT NULL AND payment_tmp.receipt_code NOT LIKE \'\') ';
    $qwhere .= ' AND (payment_tmp.booking_code IS NULL OR payment_tmp.booking_code LIKE \'\') ';
    $qwhere .= ' AND payment_tmp.topic_id=' . db_input(THU_TOPIC);
    $qwhere .= " AND DATE(FROM_UNIXTIME(`payment_tmp`.`time`)) >= date(" . (db_input(PAYMENT_BOOKING_CODE_MISSING_FROM)) . ")";

    if ($agent) {
        $qwhere .= " AND agent = ".db_input($agent);
    }

    if ($dept_id)
        $qwhere .= sprintf(" AND (  payment_tmp.dept_id = %s ) ", db_input("$dept_id"));

    if ($income_type_id)
        $qwhere .= sprintf(" AND (  payment_tmp.income_type_id = %s ) ", db_input("$income_type_id"));

    if ($tour) {
        $join = " JOIN booking_tmp b ON trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM b.booking_code))
               LIKE trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM payment_tmp.booking_code)) ";
        $qwhere .= sprintf(" AND (  b.booking_type LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $tour)."%'");
    }

    if ($receipt_code)
        $qwhere .= sprintf(" AND (  payment_tmp.receipt_code LIKE %s ) ", db_input("%$receipt_code%"));

    if ($method)
        $qwhere .= sprintf(" AND (  payment_tmp.method LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $method)."%'");

    if (!empty($amount))
        $qwhere .= sprintf(" AND (  payment_tmp.amount = %s ) ", db_input("$amount"));

    if ($note && !empty($note)) {
        if ( _String::isMobileNumber($note) ) {
            $qwhere .= sprintf(" AND (  payment_tmp.note LIKE %s OR payment_tmp.note LIKE %s ) "
                , db_input("%$note%"), db_input("%"._String::convertMobilePhoneNumber($note)."%"));
        } else {
            $qwhere .= sprintf(" AND (  payment_tmp.note LIKE %s ) ", db_input("%$note%"));
        }
    }

//dates
    $startTime = ($_REQUEST['startDate'] && (strlen($_REQUEST['startDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])) : 0;
    $endTime = ($_REQUEST['endDate'] && (strlen($_REQUEST['endDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])) : 0;
    if ($startTime > $endTime && $endTime > 0) {
        $errors['err'] = __('Entered date span is invalid. Selection ignored.');
        $startTime = $endTime = 0;
    } else {
        if ($startTime) {
            $qwhere .= " AND DATE(FROM_UNIXTIME(`payment_tmp`.`time`)) >= date(" . (db_input($startTime)) . ")";
            $qs += array('startDate' => $_REQUEST['startDate']);
        }
        if ($endTime) {
            $qwhere .= " AND DATE(FROM_UNIXTIME(`payment_tmp`.`time`)) <= date(" . (db_input($endTime)) . ")";
            $qs += array('endDate' => $_REQUEST['endDate']);
        }
    }

    $sortOptions = array();
    $orderWays = array('DESC' => 'DESC', 'ASC' => 'ASC');
    $sort = ($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])]) ? strtolower($_REQUEST['sort']) : 'time';
//Sorting options...
    $order_column = $order = null;
    if ($sort && $sortOptions[$sort]) {
        $order_column = $sortOptions[$sort];
    }
    $order_column = $order_column ?: '`time`';

    if (isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
        $order = $orderWays[strtoupper($_REQUEST['order'])];
    }
    $order = $order ?: 'DESC';

    if ($order_column && strpos($order_column, ',')) {
        $order_column = str_replace(',', " $order,", $order_column);
    }
    $x = $sort . '_sort';
    $$x = ' class="' . strtolower($order) . '" ';
    $order_by = " payment_tmp.$order_column $order ";
//pagenate

    $qs += array('order' => $order);
    $sql = " SELECT payment_tmp.* FROM payment_tmp " .( $join ?: " " ) . " $qwhere ORDER BY $order_by, payment_tmp.ticket_id DESC ";

    $total = db_count("SELECT COUNT(*) FROM ($sql) AS A");
    $page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
    $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
    $pageNav->setURL('booking_reminder.php', $qs);

    if (isset($_REQUEST['export']) && $_REQUEST['export']) {
        switch ($_REQUEST['export']) {
            case 'list':
                payment_export_list($sql);
                break;
            default:
                break;
        }

        exit;
    }
}

$res = db_query($sql . " LIMIT ".$pageNav->getStart().",".$pageNav->getLimit());


$page='booking_reminder.inc.php';
$nav->setTabActive('booking', 'booking_reminder');
$ost->addExtraHeader('<meta name="tip-namespace" content="booking.booking_reminder" />',
    "$('#content').data('tipNamespace', 'booking.booking_reminder');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('admin.inc.php');

if($_REQUEST && isset($_REQUEST['do'])){
    switch(strtolower($_REQUEST['do'])){
        case 'mass_process':
            $errors = [];
            $phone_list = OST_Facebook::PushDataToFB($errors);
            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
}

$page='tugo_app.inc.php';
$nav->setTabActive('tugo_app');
$ost->addExtraHeader('<meta name="tip-namespace" content="tugo_app.tugo_app" />',
                     "$('#content').data('tipNamespace', 'tugo_app.tugo_app');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


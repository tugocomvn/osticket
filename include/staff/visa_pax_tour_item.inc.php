<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if(!$thisstaff->canViewVisaDocuments()) die('Access Denied');
?>
    <style>
        .item table td,
        .form_table td {
            border: none;
            padding: 0.5em 0.5em;
        }

        .item table td:nth-child(2n) {
            padding-right: 1em;
        }

        .basic_info select {
            width: 170px;
        }
    </style>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/visa-tour-guest.php"?>">Back</a></p>
<?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
<h2 class="">Cập Nhật Hồ Sơ Khách</h2>
<?php else: ?>
<h2 class="">Chi Tiết Hồ Sơ Khách</h2>
<?php endif; ?>
<div class="pull-right">
    <p >
        <span class="label label-info">TG123-<?php echo _String::trimBookingCode($paxInfo['booking_code']) ?></span>
        &bull; <?php echo $tour->name; ?>
    </p>
    <p><?php
        echo $booking_type_list[(int)$tour->destination]['name']?:'';
        echo ' - ';
        if($tour->gateway_present_time) echo date('d/m/Y', strtotime($tour->gateway_present_time)) ?>
    </p>
</div>
<p><big><?php echo $paxInfo['full_name'] ?></big></p>
<p><?php if(isset($paxInfo['passport_no']) && $paxInfo['passport_no']) echo $paxInfo['passport_no'] ?></p>
<p><?php if(isset($paxInfo['dob']) && $paxInfo['dob']) echo date('d/m/Y', strtotime($paxInfo['dob']));  ?></p>
<div class="clearfix"></div>
<div class="centered">
    <?php if(isset($dataPassportPhoto) && $dataPassportPhoto !== null) : ?>
        <div class="pull-left" style="width: 50%">
            <p>Passport của khách</p>
            <img src="<?php echo $dataPassportPhoto ?>" width="60%">
        </div>
    <?php endif; ?>

    <?php if(isset($visa_image_filename) && $visa_image_filename): ?>
        <div class="clearfix pull-right" style="width: 50%">
            <p>Visa khách được cấp</p>
            <img src="<?php if ($visa_image_filename !== '') echo '../visa_upload/' . $visa_image_filename ?>"
                 alt="No find image" width="60%"/>
        </div>
    <?php endif; ?>
</div>
<?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
<form action="<?php echo $cfg->getUrl();?>scp/visa_pax_tour_item.php" method="POST" name="tenants" enctype="multipart/form-data">
    <?php csrf_token(); ?>
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
    <input type="hidden" name="tour" value="<?php echo $_REQUEST['tour']; ?>">
    <input type="hidden" id="host_request" value="<?php echo $cfg->getUrl() ?>">
    <input type="hidden" name="passport_no" value="<?php if(isset($paxInfo['passport_no']) && $paxInfo['passport_no'])
                        echo $paxInfo['passport_no'];
                    else echo '';?>">
<?php endif; ?>
    <?php if(isset($error) && $error ): ?>
        <?php foreach ($mess as $value): ?>
            <div id="msg_warning"><?php echo $value ?></div>
        <?php endforeach; ?>
    <?php endif;?>
    <table class="form_table basic_info" style="width: 1058px; margin: 0 auto">
        <tbody>
        <tr>
            <th colspan="4">
                <em><strong><?php echo __('Thông tin quản lí hồ sơ'); ?></strong></em>
            </th>
        </tr>
        <tr>
            <td>
                <?php echo __('Nhân viên phụ trách'); ?>:
            </td>
            <td>
                <select name="staff_id" <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                    <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($listStaff && ($row = db_fetch_array($listStaff))) {
                        echo sprintf('<option value="%d" %s>%s</option>',
                                     $row['staff_id'], ($visaProfile->staff_id == $row['staff_id'] ? 'selected="selected"' : ''),
                                     $row['username']);
                    } ?>
                </select>
            </td>
            <td>
                <?php echo __('Vị trí passport'); ?>
            </td>
            <td>
                <select name="location_passport" <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                    <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($listPassportLocation && ($row = db_fetch_array($listPassportLocation))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                                     $row['id'],($visaProfile->passport_location_id == $row['id']?'selected="selected"':''),$row['name']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo __('Deadline hồ sơ'); ?>:
            </td>
            <td>
                <input autocomplete="off" type="text"
                       name="date-deadline" <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                       class="dp input-field" value ="<?php if($visaProfile->deadline != 0) echo date('d/m/Y',strtotime($visaProfile->deadline));?>">
            </td>
            <td>
                <strong>Tình trạng Hồ sơ Visa</strong>
            </td>
            <td>
                <select name="status_visa" <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                    <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php foreach(\Tugo\VisaStatusList::caseTitleName() as $_id => $_name): ?>
                        <option value="<?php echo $_id ?>"
                                <?php if($visaProfile->visa_status_id == $_id) echo "selected" ?>
                        ><?php echo $_name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo __('Loại visa'); ?>:
            </td>
            <td>
                <select name="visa_type" <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                    <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($listVisaType && ($row = db_fetch_array($listVisaType))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                                     $row['id'],($visaProfile->visa_type_id == $row['id']?'selected="selected"':''),$row['name']);
                    }?>
                </select>
            </td>
            <td>
                <?php echo __('Mã xin visa'); ?>:
            </td>
            <td>
                <input style="width: 20em;" maxlength="32"  <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                       placeholder="Mã xin visa" type="text" name="visa-code"
                       value="<?php if(!is_null($visaProfile->visa_code)) echo $visaProfile->visa_code; else echo ''?>"/>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="form_table basic_info" style="width: 1058px; margin: 0 auto">
        <tbody>
        <tr>
            <th colspan="2">
                <em>
                    <strong><?php echo __('Các giấy tờ bắt buộc:');?></strong>
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                        <a class="selectAll" href="#paper_require"><?php echo __('Select All');?></a>
                        <a class="selectNone" href="#paper_require"><?php echo __('Select None');?></a>
                    <?php endif; ?>
                </em>
            </th>
        </tr>
        <?php $check = false; ?>
        <?php $i=0;while ($listRequiredPapers && ($row = db_fetch_array($listRequiredPapers))):
            if($row['status'] === '1'):?>

                <tr>
                    <td colspan="2">
                        <input type="checkbox" class="paper_require"
                            name="paper_require[]"
                            <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                            value="<?php echo $i ?>"
                            <?php if('1' === $listCheckRequiredPapers[$i]) echo 'checked' ?>
                        ><?php echo $row['name'] ?>
                    </td>
                </tr>
            <?php $check = true; ?>
            <?php  endif; $i++;?>
        <?php  endwhile; ?>
        <?php if (!$check): ?>
        <tr><td colspan="2">Chưa có dữ liệu. Bấm vào mục <a class="no-pjax" target="_blank"
                        href="<?php echo $cfg->getUrl() ?>scp/visa_paper_require.php">Paper require</a>
                để tạo thêm.</td></tr>
        <?php endif; ?>        <tr>
            <th colspan="2">
                <em>
                    <strong><?php echo __('Tình trạng tài chính');?></strong>
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                        <a class="selectAll" href="#status_finance"><?php echo __('Select All');?></a>
                        <a class="selectNone" href="#status_finance"><?php echo __('Select None');?></a>
                    <?php endif; ?>
                </em>
            </th>
        </tr>
        <?php $check = false; ?>
        <?php $i=0;while ($listStatusFinance && ($row = db_fetch_array($listStatusFinance))):
            if($row['status'] === '1'):?>

                <tr>
                    <td colspan="2">
                        <input type="checkbox" class="status_finance"
                            name="status_finance[]"
                            <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                            value="<?php echo $i ?>"
                            <?php if('1' === $listCheckStatusFinance[$i]) echo 'checked' ?>
                        ><?php echo $row['name'] ?>
                    </td>
                </tr>
            <?php $check = true; ?>
            <?php  endif; $i++;?>
        <?php  endwhile; ?>
        <?php if (!$check): ?>
        <tr><td colspan="2">Chưa có dữ liệu. Bấm vào mục <a class="no-pjax" target="_blank"
                        href="<?php echo $cfg->getUrl() ?>scp/status_finance.php">Status Finance</a>
                để tạo thêm.</td></tr>
        <?php endif; ?>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Các thông tin cá nhân:');?></strong></em>
            </th>
        </tr>
        <tr>
            <input type="hidden" name="address" id="address" value="">
            <td width="180">
                <?php echo __('Địa chỉ hộ khẩu'); ?>:
            </td>
            <td>
                <p>
                    <select name="city" id="city" onchange="getDistrict(0)"
                        <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                        <option value="0">&mdash; <?php echo __('Tỉnh/Thành phố'); ?> &mdash;</option>
                        <?php while ($listCityAddress && ($row = db_fetch_array($listCityAddress))) {
                            $select = '';
                            if(isset($visaProfile->city_id) && $visaProfile->city_id && ($visaProfile->city_id  == $row['id']))
                                $select = 'selected';
                            echo sprintf('<option value="%d" %s>%s</option>',
                                         $row['id'],$select,$row['name']);
                        } ?>
                    </select> -
                    <select name="district" id="district" onchange="getWard(0)"
                        <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                        <option value="0">&mdash; <?php echo __('Quận/Huyện'); ?> &mdash;</option>
                        <?php if(isset($visaProfile->district_id) && $visaProfile->district_id) {
                            echo sprintf('<option value="%d" selected></option>',
                                         $visaProfile->district_id);
                        } ?>
                    </select> -
                    <select name="ward" id="ward"
                        <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                        <option value="0"><?php echo __('Phường/Xã/Thị trấn'); ?> &mdash;</option>
                        <?php if(isset($visaProfile->ward_id) && $visaProfile->ward_id) {
                            echo sprintf('<option value="%d" selected></option>',
                                         $visaProfile->ward_id);
                        } ?>
                    </select>
                </p>
                <br>
                <textarea style="width: 60%;resize:none" cols="20" rows="4" type="text" name="street" value=""
                          placeholder="số nhà, đường" <?php if (!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                            ><?php if(isset($visaProfile) && !empty($visaProfile->street_address)) echo $visaProfile->street_address ?></textarea>
            </td>
        </tr>
        <tr>
            <td width="180">
                <?php echo __('Tình trạng công việc'); ?>:
            </td>
            <td>
                <select name="status_job" <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                    <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($listJobStatus && ($row = db_fetch_array($listJobStatus))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                            $row['id'],($visaProfile->job_status_id == $row['id']?'selected="selected"':'') ,$row['name']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
        <td width="180">
            <?php echo __('Tình trạng hôn nhân'); ?>:
        </td>
        <td>
            <select name="status_marital" <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>>
                <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                <?php while ($listMarital && ($row = db_fetch_array($listMarital))){
                    echo sprintf('<option value="%d" %s >%s</option>',
                        $row['id'],($visaProfile->marital_status_id == $row['id']?'selected="selected"':''),$row['name']);
                }?>
            </select>
        </td>
        </tr>
        </tbody>
    </table>
    <table class="form_table" style="width: 1058px; margin: 0 auto">
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Thông tin visa');?></strong></em>
            </th>
        </tr>
        <tr>
            <td>
                <?php echo __('Lịch sử visa'); ?>:
            </td>
            <td class="centered">
                <div class="repeater">
                    <div data-repeater-list="group-loyalty" class="pax_list sortable-rows ui-sortable">
                        <?php if (!$history_pax_list->num_rows && $thisstaff->canMangeVisaPaxDocuments()): ?>
                            <div data-repeater-item class="item">
                                <table>
                                    <tr>
                                        <td>Nội dung:</td>
                                        <td>
                                            <textarea style="overflow:auto;resize:none" cols="40" rows="3" type="text" name="text-input" value=""></textarea>
                                        </td>
                                        <td>Ngày:</td>
                                        <td><input autocomplete="off" type="text" name="date-input" class="dp_visa_h dob new" value =""/></td>
                                        <td><input data-repeater-delete class="btn_sm btn-xs btn-danger" type="button" value="Delete"/></td>
                                    </tr>
                                </table>
                            </div>
                        <?php else: ?>
                            <?php while($history_pax_list && ($row = db_fetch_array($history_pax_list))): ?>
                                <div data-repeater-item class="item">
                                    <input autocomplete="false" type="hidden" name="history_id" value="<?php echo $row['id']?>" >
                                    <table>
                                        <tr>
                                            <td>Nội dung:</td>
                                            <td>
                                                <textarea style="overflow:auto;resize:none" cols="40" rows="3"
                                                          type="text" name="text-input"
                                                   <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                                                  value=""><?php echo trim($row['visa_content']) ?></textarea>
                                            </td>
                                            <td>Ngày:</td>
                                            <td>
                                                <input autocomplete="off"
                                                       type="text" name="date-input"
                                                       class="dp_visa_h dob new"
                                                    <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                                                       value ="<?php if(isset($row['date_event']))
                                                           echo date('d/m/Y',strtotime($row['date_event'])); ?>"/>
                                            </td>
                                            <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                                            <td>
                                                <input data-repeater-delete class="btn_sm btn-xs btn-danger"  type="button" value="Delete"/>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    </table>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                        <p class="centered"><input data-repeater-create type="button" class="btn_sm btn-xs btn-default" value="&plus; Add"/></p>
                    <?php endif; ?>
                </div>
            </td>
        </tr>

    </table>
    <table class="form_table" style="width: 1058px; margin: 0 auto">
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Thông tin bổ sung:');?></strong></em>
            </th>
        </tr>
        <tr>

            <td class="centered">
                <div class="repeater">
                    <div data-repeater-list="group-loyalty-info" class="pax_list sortable-rows ui-sortable">
                        <?php if (!$info_pax_list->num_rows && $thisstaff->canMangeVisaPaxDocuments()): ?>
                            <div data-repeater-item class="item">
                                <table>
                                    <tr>
                                        <td>Nội dung:</td>
                                        <td>
                                            <textarea style="overflow:auto;resize:none" cols="50" rows="3" type="text" name="text-input-info" value=""></textarea>
                                        </td>
                                        <td>
                                            <input data-repeater-delete class="btn_sm btn-xs btn-danger" type="button" value="Delete"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <?php else: ?>
                            <?php while($info_pax_list && ($row = db_fetch_array($info_pax_list))): ?>
                                <div data-repeater-item class="item">
                                    <input  type="hidden" name="info_id" value="<?php echo $row['id']?>" >
                                    <table>
                                        <tr>
                                            <td>Nội dung:</td>
                                            <td>
                                                <textarea style="overflow:auto;resize:none" cols="50" rows="3"
                                                          type="text" name="text-input-info"
                                                   <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>
                                                  value=""><?php echo trim($row['visa_content']) ?></textarea>
                                            </td>
                                            <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                                            <td>
                                                <input data-repeater-delete class="btn_sm btn-xs btn-danger"  type="button" value="Delete"/>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    </table>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                        <p class="centered"><input data-repeater-create type="button" class="btn_sm btn-xs btn-default" value="&plus; Add"/></p>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
    </table>
    <table class="form_table" style="width: 1058px; margin: 0 auto">
        <tr>
            <th colspan="3">
                <em><strong>Tải lên hình ảnh visa có sẵn trên hộ chiếu</strong></em>
            </th>
        </tr>
        <tr>
            <td width="100px">Tải ảnh lên</td>
            <td width="150px" class="form_group">
                <input type="file" id="profile-img" name="image" class="input-field"
                    <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>/>
            </td>
            <td>
                <img src="" id="profile-img-tag" width="300px"/>
                <p><em id="notify_image"></em></p>
            </td>
        </tr>
    </table>
    <table class="form_table" style="width: 1058px; margin: 0 auto">
        <tr>
            <th colspan="3">
                <em><strong>Tải lên hình ảnh passport của khách</strong></em>
            </th>
        </tr>
        <tr>
            <td width="100px">Tải ảnh lên</td>
            <td width="150px" class="form_group">
                <input type="file" id="passport-img" name="image_passport" class="input-field"
                    <?php if(!$thisstaff->canMangeVisaPaxDocuments()) echo 'disabled' ?>/>
            </td>
            <td>
                <img src="" id="profile-img-passport-tag" width="300px"/>
                <p><em id="notify_image_passport"></em></p>
            </td>
        </tr>
    </table>

<?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
    <p class="centered">
        <button class="btn_sm btn-primary" name="edit" value="edit">Save</button>
    </p>
<?php endif; ?>
</form>

<script type="text/javascript">
    function readURLVisa(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
                $('#notify_image').text('*Bấm Save để lưu lại ảnh này');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLPassport(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-passport-tag').attr('src', e.target.result);
                $('#notify_image_passport').text('*Bấm Save để lưu lại ảnh này');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURLVisa(this);
    });
    $("#passport-img").change(function(){
        readURLPassport(this);
    });
</script>

<script>
     var this_config;
     var host = $("#host_request").val();
     $(document).ready(function() {
         var val_ward = $('#ward option:selected').val();
         var val_district = $('#district option:selected').val();
         if($('#city option:selected').val() > 0) {
             getDistrict(val_district);
             getWard(val_ward);
         }
     });

     function getDistrict(select_district) {
         var id_city = $("select#city option:checked").val();
         var url = host + "api/http.php/v1/app/address?type=district&id="+id_city;
         $.get(url, function(){

         }).done(function(res) {
             var data = res.data;
             $("#district option").remove();
             $("#district").append(new Option('Quận/Huyện',0));
             for (i in data) {
                 var option=  new Option(data[i].name, data[i].id);
                 $("#district").append(option);
             }
             var select = "select#district option[value='" + select_district + "']";
             $(select).attr("selected","selected");

             if(select_district === 0) {//reset value in ward
                 $("#ward option").remove();
                 $('#ward').append(new Option('Phường/Xã',0));
                 $("select#ward option[value='0']").attr("selected","selected");
             }
         })
     }
     function getWard(select_ward) {
         var id_district = $("select#district option:checked").val();

         var url = host + "api/http.php/v1/app/address?type=ward&id="+id_district;
         $.get(url, function(){
         }).done(function(res) {
             var data = res.data;
             $("#ward option").remove();
             $('#ward').append(new Option('Phường/Xã',0));
             for (i in data) {
                 var option=  new Option(data[i].name, data[i].id);
                 $("#ward").append(option);
             }
             var select = "select#ward option[value='" + select_ward + "']";
             $(select).attr("selected","selected");
         })
     }

     function datePicker() {
        $('.dp_visa_h.new').datepicker({
            changeYear: true,
            yearRange: "2000:2033",
            shortYearCutoff: 50,
            showButtonPanel: true,
            selectOtherMonths: false,
            numberOfMonths: 1,
            buttonImage: './images/cal.png',
            showOn:'both',
            minDate: new Date(<?php echo (time()) ?>),
            dateFormat: $.translate_format(this_config.date_format||'dd/mm/yy')
    });
    $('.dp_visa_h.new').removeClass('new');
    }
        $(document).ready(function () {
            getConfig().then(function(c) { this_config = c; datePicker(); });
            $('.repeater').repeater({
                visible:false,
                show: function () {
                    $(this).slideDown();
                    datePicker();
                    },
                hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                    }
                },
                ready: function (setIndexes) {
                    $('.sortable-rows').on('sortstop', setIndexes);
                },
                isFirstItemUndeletable: false
        });
    });
</script>

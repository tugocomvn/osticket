<?php
/*********************************************************************
    receipt.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

require('staff.inc.php'); 
require_once(INCLUDE_DIR.'class.receipt.php');
require_once(INCLUDE_DIR.'class.booking.php');
include_once INCLUDE_DIR.'class.staff.php';
require_once INCLUDE_DIR.'class.qrcode.php';

$staff_list = Staff::getAllName();
$finance_staff_list = Staff::getAllNameFinance();
$newReceipt = Receipt::generateReceiptCode();

function convertFileToBas64($path){
    if(!file_exists($path))
        return null;

    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

function __validation_datetime($data){
    if (!isset($data['departure_date']) || empty($data['departure_date']))
        throw new Exception('Vui lòng nhập ngày khởi hành !!!');
        
    if (!isset($data['time_to_get_in']) || empty($data['time_to_get_in']))
        throw new Exception('Vui lòng nhập ngày tập trung !!!');

    if(isset($data['document_due']) && !empty($data['departure_date'])){
        if(strtotime($data['document_due']) > strtotime($data['departure_date']))
        throw new Exception('Thời gian cần hoàn tất hồ sơ không được lớn hơn ngày khởi hành !!!'); 
    }   

    if(isset($data['visa_result_estimate_due']) && !empty($data['departure_date'])){
        if(strtotime($data['visa_result_estimate_due']) > strtotime($data['departure_date']))
        throw new Exception('Thời gian có kết quả visa không được lớn hơn ngày khởi hành !!!'); 
    }
}

function __validation_number($data){
    if (!isset($data['unit_price_nl']) || empty($data['unit_price_nl']))
    throw new Exception('Vui lòng nhập giá tiền 1 khách (NL) !!!');

    if(strlen($data['unit_price_nl']) > 11)
    throw new Exception('Giá tiền vượt mức định dạng cho phép !!!');

    if (!isset($data['quantity_nl']) || empty($data['quantity_nl']))
    throw new Exception('Vui lòng nhập số lượng (NL) !!!');

    if(strlen($data['unit_price_te']) > 11)
    throw new Exception('Giá tiền vượt mức định dạng cho phép !!!');

    if(strlen($data['surcharge']) > 11)
    throw new Exception('Phí phụ thu vượt mức định dạng cho phép !!!');  

    if (!isset($data['amount_to_be_received']) || empty($data['amount_to_be_received']))
    throw new Exception('Vui lòng nhập tổng số tiền khách cần đóng!!!');

    if (!isset($data['amount_received']) || empty($data['amount_received']))
    throw new Exception('Vui lòng nhập số tiền khách đóng !!!');

    if (strlen($data['amount_received']) > 11)
    throw new Exception('Số tiền khách đóng vượt mức định dạng cho phép !!!');

    if (strlen($data['balance_due']) > 11)
    throw new Exception('Số tiền còn lại vượt mức định dạng cho phép !!!');

    
}

function __validation($data){

    $receipt = null;

    if (isset($data['receipt_id']) && (int)(trim($data['receipt_id']))) {
        $data['receipt_id'] = (int)(trim($data['receipt_id']));
        $receipt = Receipt::lookup(['id' => $data['receipt_id']]);
        if (!$receipt) throw new \Exception('Invalid receipt content. Wrong ID');   
    }
    
    if (!isset($data['booking_code']) || empty($data['booking_code']))
        throw new Exception('Vui lòng nhập mã booking !!!');

    if(isset($data['booking_code'])){
        $booking_code_trim = _String::trimBookingCode($data['booking_code']);
        $_booking = Booking::lookup(['booking_code_trim' => $booking_code_trim ]);
        if(!$_booking)
        throw new Exception('Mã booking không tồn tại !!!');
    }

    if (!isset($data['tour_name']) || empty($data['tour_name']))
        throw new Exception('Vui lòng nhập lộ trình phiếu thu !!!');

    if (isset($data['departure_date'])) {
        __validation_datetime($data);
    }

    if (isset($data['time_to_get_in'])) {
        __validation_datetime($data);
    }
   
    if (isset($data['time_to_get_in_'])) {
        __validation_datetime($data);
    }

    if (!isset($data['place_to_get_in']) || empty($data['place_to_get_in']))
        throw new Exception('Vui lòng nhập địa điểm !!!');

    if (isset($data['unit_price_nl'])) {
        __validation_number($data);
    }

    if (isset($data['quantity_nl'])) {
        __validation_number($data);
    }

    if(isset($data['unit_price_te'])){
        __validation_number($data);
    }
    
    if(isset($data['amount_to_be_received'])){
        __validation_number($data);
    }
   
    if(isset($data['surcharge'])){
        __validation_number($data);
    }

    if(isset($data['amount_received'])){
        __validation_number($data);
    }

    if (!isset($data['payment_method']) || empty($data['payment_method']))
        throw new Exception('Vui lòng chọn hình thức thanh toán !!!');
    
    if($data['payment_method'] != 1) {
        if(!isset($data['bank_transfer_note']) || empty($data['bank_transfer_note']))
        throw new Exception('Bổ sung ghi chú là bắt buộc khi thanh toán bằng tất cả các loại thẻ !!!');
    }
    
    if(isset($data['balance_due'])){
        __validation_number($data);
    }
    
    if(empty($data['due_date']) && empty($data['due_date_text']))
        throw new Exception('Vui lòng nhập ngày hoàn tất thanh toán số tiền còn lại !!!');

    if (!isset($data['customer_name']) || empty($data['customer_name']))
        throw new Exception('Vui lòng nhập tên khách đại diện !!!');
        
    if (!isset($data['customer_phone_number']) || empty($data['customer_phone_number']))
        throw new Exception('Vui lòng nhập số điện thoại của khách !!!');

    if (!\_String::isMobileNumber($data['customer_phone_number']) || !preg_match('/^0[3-9][0-9]{8}$/', $data['customer_phone_number']))
        throw new Exception('Số điện thoại khách không đúng định dạng !!!');
    
    if (!isset($data['staff_id']) || empty($data['staff_id']))
        throw new Exception('Vui lòng chọn tên nhân viên kinh doanh !!!');    

    if (!isset($data['staff_phone_number']) || empty($data['staff_phone_number']))
        throw new Exception('Vui lòng nhập số điện thoại của nhân viên kinh doanh !!!'); 
        
    if (!\_String::isMobileNumber($data['staff_phone_number']) || !preg_match('/^0[3-9][0-9]{8}$/', $data['staff_phone_number']))
        throw new Exception('Số điện nhân viên kinh doanh không đúng định dạng !!!');

    if(!isset($data['staff_cashier_id']) || empty($data['staff_cashier_id']))
        throw new Exception('Vui lòng chọn nhân viên thu tiền !!!'); 

    if (!isset($data['staff_cashier_phone_number']) || empty($data['staff_cashier_phone_number']))
        throw new Exception('Vui lòng nhập số điện thoại của nhân viên thu tiền !!!');
        
    if (!\_String::isMobileNumber($data['staff_cashier_phone_number']) || !preg_match('/^0[3-9][0-9]{8}$/', $data['staff_cashier_phone_number']))
        throw new Exception('Số điện thoại nhân viên thu tiền không đúng định dạng !!!');  
        
    if(isset($data['document_due'])){
        __validation_datetime($data);
    }

    if(isset($data['visa_result_estimate_due'])){
        __validation_datetime($data);
    }

    if ($receipt) return $receipt;
    return null;
}

function __save_receipt_content($data){
    global $thisstaff;  
    $receipt = __validation($data);
    $_data = [
        'receipt_code'              => trim($data['receipt_code']),
        'booking_code'              => trim($data['booking_code']),
        'booking_type_id'           => (int)($data['booking_type_id']),
        'ticket_id'                 => (int)$data['ticket_id'],
        'booking_code_trim'         => (int)_String::trimBookingCode($data['booking_code']),
        'departure_date'            => date_create_from_format('Y-m-d',$data['departure_date'])->format('Y-m-d'),
        'time_to_get_in'            => date_create_from_format('Y-m-d',$data['time_to_get_in'])->format('Y-m-d'),
        'unit_price_nl'             => (int)trim($data['unit_price_nl']),
        'quantity_nl'               => (int)trim($data['quantity_nl']),
        'unit_price_te'             => (int)trim($data['unit_price_te']),
        'quantity_te'               => (int)trim($data['quantity_te']),
        'surcharge'                 => (int)trim($data['surcharge']),
        'surcharge_note'            => trim($data['surcharge_note']),
        'amount_to_be_received'     => (int)trim($data['amount_to_be_received']),
        'amount_received'           => (int)trim($data['amount_received']),
        'balance_due'               => trim($data['balance_due']),
        'customer_name'             => trim($data['customer_name']),
        'customer_phone_number'     => trim($data['customer_phone_number']),
        'public_note'               => trim($data['public_note']),
        'staff_id'                  => (int)$data['staff_id'],
        'staff_name'                => $data['staff_id'] && ($_staff = Staff::lookup($data['staff_id']))? $_staff->getName()->name:'',
        'staff_phone_number'        => trim($data['staff_phone_number']),
        'internal_note'             => trim($data['internal_note']), 
        'document_due'              => (!empty($data['document_due']))?date_create_from_format('Y-m-d',$data['document_due'])->format('Y-m-d'):null,
        'visa_result_estimate_due'  => (!empty($data['visa_result_estimate_due']))?date_create_from_format('Y-m-d',$data['visa_result_estimate_due'])->format('Y-m-d'):null,
        'tour_name'                 => trim($data['tour_name']),
        'payment_method'            => (int)$data['payment_method'],
        'place_to_get_in'           => trim($data['place_to_get_in']),
        'staff_cashier_id'          => (int)$data['staff_cashier_id'],
        'staff_cashier_name'        => Staff::lookup($data['staff_cashier_id'])->getName()->name,
        'staff_cashier_phone_number'=> trim($data['staff_cashier_phone_number']),
        'due_date_text'             => (!empty($data['due_date_text']))?trim($data['due_date_text']):'',
        'due_date'                  => (!empty($data['due_date']))?date_create_from_format('Y-m-d',$data['due_date'])->format('Y-m-d'):'',
        'bank_transfer_note'        => (isset($data['payment_method']))?trim($data['bank_transfer_note']):'',
    ];
    if ($receipt) {
        $_data["updated_at"] = date('Y-m-d H:i:s');
        $_data["updated_by"] = $thisstaff->getId();
        $receipt->setAll($_data);
        $receipt->save(true);
    } else {
        $_data["created_at"] = date('Y-m-d H:i:s');
        $_data["created_by"] = $thisstaff->getId();
        $_data["hash_url"] = Receipt::Rand_md5(RECEIPT_HASH_URL_LENGHT);
        $receipt = Receipt::create($_data);
        if (!$receipt) throw new Exception('Create error. Please check data again.');
        $receipt->save(true);
        $cookie_duration = time() + (86400 * 30);
        setcookie('staff_cashier_id', (int)$_data['staff_cashier_id'], $cookie_duration);
        setcookie('staff_cashier_phone_number', $_data['staff_cashier_phone_number'], $cookie_duration);
    }
    $log = ReceiptLog::create(
        [
            'receipt_id' => $receipt->id,
            'created_by'  => $thisstaff->getId(),
            'content'    => json_encode($receipt->ht),
            'created_at'  => date('Y-m-d H:i:s'),
        ]
    );
    $log->save(true);
    $receipt->set('log_id', $log->id); //update log_id new
    return $receipt->save(true);
}
$cookie_staff_cashier_id = isset($_COOKIE['staff_cashier_id']) ? $_COOKIE['staff_cashier_id']: "";
$cookie_staff_cashier_phone_number = isset($_COOKIE['staff_cashier_phone_number']) ? $_COOKIE['staff_cashier_phone_number']: "";
$inc='receipt.inc.php';

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    if (isset($_REQUEST['action'])){
        switch ($_REQUEST['action']) {
            case 'save':
                try {
                    db_start_transaction();
                    $data = $_POST;
                    $errors = array();
                    if(!$thisstaff
                    || !$thisstaff->canCreateReceipt()
                    ) throw new Exception('Access denied'); 
                    if (__save_receipt_content($data)) {
                        db_commit();
                        FlashMsg::set('ticket_create', 'Save Successfully');
                        header('Location: '.$cfg->getUrl().'scp/receiptlist.php');
                        exit;
                    } else {
                        db_rollback();
                    }
                } catch (Exception $ex) {
                    db_rollback();
                    $errors[] = $ex->getMessage();
                }
            break;   
            case 'edit':
                $errors = array();
                try {
                    db_start_transaction();
                    $data = $_POST;
                    $id = $data['receipt_id'];
                    if(!$thisstaff
                    || !$thisstaff->canEditReceipt()
                    ) throw new Exception('Access denied'); 
                    if (__save_receipt_content($data)) {
                        db_commit();
                        $msg = 'Updated successfully';
                        FlashMsg::set('ticket_create', $msg);
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                            ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$id.'&action=edit' : $cfg->getBaseUrl() . '/scp/receipt.php';
                        header('Location: '. $referer);
                        exit();
                    } else {
                        db_rollback();
                    }
                } catch (Exception $ex) {
                    db_rollback();
                    $errors[] = $ex->getMessage();
                }
            break;
            case 'sms_cashier_money': /* Send a message after staff receiving the money */
                if(isset($_POST['receipt_id']) && (int)(trim($_POST['receipt_id']))){
                    $receipt = Receipt::lookup(['id' => $_POST['receipt_id']]);
                    if($receipt->status != 0
                    ) {
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                        ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                        header('Location: '. $referer);
                        exit();
                    }
                    elseif(!$thisstaff || !$thisstaff->canEditReceipt()
                        || !($thisstaff->getId() == $receipt->created_by
                            || $thisstaff->getId() == $receipt->staff_cashier_id
                            || $thisstaff->getId() == $receipt->staff_id
                            || $thisstaff->canRejectReceipt())
                    ) {
                        $errors['err']=__('Permission Denied.');
                    }
                    elseif(Receipt::SendAfterReceivingMoneySMS($_POST, $errors, $thisstaff)){
                        $msg=__('SMS receiving money sent successfully');
                        FlashMsg::set('ticket_create', $msg);
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                        ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$_REQUEST['receipt_id'].'&action=confirm' : $cfg->getBaseUrl() . '/scp/receipt.php';
                        header('Location: '. $referer);
                        exit();
                    }
                }
            break;
            case 'send_sms_receipt': /* Send a message receipt and code */
                if(isset($_POST['receipt_id']) && (int)(trim($_POST['receipt_id']))){
                    $receipt = Receipt::lookup(['id' => $_POST['receipt_id']]);
                    if($receipt->status != Receipt::MONEY_RECEIVED
                    ) {
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                        ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                        header('Location: '. $referer);
                        exit();
                    }
                    elseif(!$thisstaff
                    || !$thisstaff->canEditReceipt()
                    || !($thisstaff->getId() == $receipt->created_by
                            || $thisstaff->getId() == $receipt->staff_cashier_id
                            || $thisstaff->getId() == $receipt->staff_id
                            || $thisstaff->canRejectReceipt()
                        ))  {
                        $errors['err']=__('Permission Denied.');
                    }
                    elseif(Receipt::SendConfirmationCodeToCustomer($_POST, $errors, $thisstaff)){
                        $msg=__('SMS receipt & code sent successfully');
                        FlashMsg::set('ticket_create', $msg);
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                        ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$_REQUEST['receipt_id'].'&action=confirm_code' : $cfg->getBaseUrl() . '/scp/receipt.php';
                        header('Location: '. $referer);
                        exit();
                    }
                }
            break;
            case 'finance_approval': /* Send a message accountant approve */
                if(isset($_POST['receipt_id']) && (int)(trim($_POST['receipt_id']))){
                    $receipt = Receipt::lookup(['id' => $_POST['receipt_id']]);
                    if($receipt->status != Receipt::WAITING_VERIFYING
                    ) {
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                        ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                        header('Location: '. $referer);
                        exit();
                    }
                    elseif(!$thisstaff
                    || !$thisstaff->canApproveReceipt()
                    )  {
                        $errors['err']=__('Permission Denied.');
                    }
                    elseif(Receipt::SendFinanceApprovalSMS($_POST, $errors, $thisstaff)){
                        $qrcode_receipt = Receipt::qrCodeReceipt($_POST);
                        $msg=__('SMS finance approval sent successfully');
                        FlashMsg::set('ticket_create', $msg);
                        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                        ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$_REQUEST['receipt_id'].'&action=view' : $cfg->getBaseUrl() . '/scp/receipt.php';
                        header('Location: '. $referer);
                        exit(); 
                    }
                }
            break;
            case 'rejected': /* Send a message accountant reject */
                if(!$thisstaff
                || !$thisstaff->canRejectReceipt()
                ) $errors['err']=__('Permission Denied.');
                elseif(Receipt::SendFinanceRejectedSendSMS($_POST, $errors, $thisstaff)){
                    $msg=__('SMS finance rejected sent successfully');
                    FlashMsg::set('ticket_create', $msg);
                    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                    ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$_REQUEST['receipt_id'].'&action=view' : $cfg->getBaseUrl() . '/scp/receipt.php';
                    header('Location: '. $referer);
                    exit();
                }
            break;
            case 'new_internal_note':
                $vars = $_POST;
                $id = $vars['receipt_id'];
                $receipt = Receipt::lookup((int)$id);
                if(!empty($receipt)){
                    $new_internal_note = $vars['internal_note'];
                    $receipt->set('internal_note', $new_internal_note);
                    $log = ReceiptLog::create(
                        [
                            'receipt_id' => $receipt->id,
                            'created_by'  => $thisstaff->getId(),
                            'content'    => json_encode($receipt->ht),
                            'created_at'  => date('Y-m-d H:i:s'),
                        ]
                    );                  
                    $log->save(true);
                    $receipt->set('log_id', $log->id);
                    $receipt->save(true);
                    $msg=__('Update successfully');
                    FlashMsg::set('ticket_create', $msg);
                    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                    ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$id.'&action=view' : $cfg->getBaseUrl() . '/scp/receipt.php';
                    header('Location: '. $referer);
                    exit();
                }
            break;
            case 'add_internal_note':
                $vars = $_POST;
                $id = $vars['receipt_id'];
                $receipt = Receipt::lookup((int)$id);
                if(!empty($receipt)){
                    $new_internal_note = $vars['internal_note'];
                    $old_internal_note = $receipt->internal_note;
                    $internal_note = $old_internal_note." ".trim($new_internal_note);
                    $receipt->set('internal_note', $new_internal_note);
                    $log = ReceiptLog::create(
                        [
                            'receipt_id' => $receipt->id,
                            'created_by'  => $thisstaff->getId(),
                            'content'    => json_encode($receipt->ht),
                            'created_at'  => date('Y-m-d H:i:s'),
                        ]
                    );
                    $log->save(true);
                    $receipt->set('log_id', $log->id);
                    $receipt->save(true);
                    $msg=__('Update successfully');
                    FlashMsg::set('ticket_create', $msg);
                    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                    ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$id.'&action=view' : $cfg->getBaseUrl() . '/scp/receipt.php';
                    header('Location: '. $referer);
                    exit();
                }
            break;
            case 'receipt_upload':
                $receipt_id = (int)($_REQUEST['id']);
                $result = Receipt::lookup($receipt_id);
                if(!$thisstaff
                || !$thisstaff->canEditReceipt()
                || !($thisstaff->getId() == $result->created_by
                    || $thisstaff->getId() == $result->staff_cashier_id
                    || $thisstaff->getId() == $result->staff_id
                    || $thisstaff->canRejectReceipt()
                )
                ) $errors['err']=__('Permission Denied.');
                else{
                    if(isset($_FILES['image']) && $result) {
                        //$file_name = $_FILES['image']['name'];
                        $file_size = $_FILES['image']['size'];
                        $file_tmp = $_FILES['image']['tmp_name'];
                        $file_type = $_FILES['image']['type'];
                        $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
            
                        $extensions = array("jpeg", "jpg", "png");
            
                        if (in_array($file_ext, $extensions) === false) {
                            $errors = "Định dạng không cho phép, vui lòng chọn  JPEG or PNG file.";
                        }
            
                        if ($file_size > 2097152) {
                            $errors = 'Ảnh phải có kích thước không quá 2 MB';
                        }
            
                        if (empty($errors)) {
                            $file_name = md5(mt_rand(0,9999).$result->receipt_code.time());
                            $dir = '/public_file';
                            if (!file_exists(__DIR__.'/..'.$dir))
                            @mkdir(__DIR__.'/..'.$dir, 0777);
                            $path = '/public_file/receipt_upload/';
                            if (!file_exists(__DIR__.'/..'.$path))
                            @mkdir(__DIR__.'/..'.$path, 0777);
                            $destination = __DIR__.'/..'.$path.$file_name.'.png';
                            $res = move_uploaded_file($file_tmp, $destination);
                            if ($res) {
                                $__data = [
                                    'image_name' => $file_name,
                                    'status'     => Receipt::UPLOAD_IMAGE,
                                    'sales_cashier_id_confirmed' => $thisstaff->getId(),
                                    'sales_cashier_confirmed_at' => date('Y-m-d H:i:s'),
                                    'customer_confirmed_at' => date('Y-m-d H:i:s')
                                ];
                                $result->setAll($__data);
                                $log = Receipt::log($result, $thisstaff);
                                $results->set('log_id', $log);
                                $result->save(true); // reload after save
                                FlashMsg::set('ticket_create', 'Upload images successfully');
                                header('Location: ' . $cfg->getUrl() . 'scp/receiptlist.php');
                                exit;
                            }else
                                $errors = "Upload images failed!";
                        }
            
                        $inc = 'receipt-upload.inc.php';
                    }
                }
            
        }
    }
}

$_REQUEST['action'] = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
    if (isset($_REQUEST['action'])) {
        switch ($_REQUEST['action']) {
        case 'edit':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $receipt = Receipt::lookup(['id' => $_REQUEST['id']]);
                $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                if ($receipt->status != 0) {
                    header('Location: '.$referer);
                    exit;
                }
                if (!$receipt) {
                    $errors['id'] = 'Content not found';
                }
            } else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.edit.inc.php";
            break; 
        case 'view':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
            } else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.view.inc.php";
            break; 
        case 'confirm':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
                $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                if ($results != Receipt::MONEY_RECEIVED) {
                    header('Location: '.$referer);
                    exit;
                }
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.send-sms.inc.php";
            break;
        case 'detailed_information':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $receipt_log = ReceiptLog::lookup(['id' => $_REQUEST['id']]);
                $results = json_decode($receipt_log->content);
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.details.inc.php";
            break;
        case 'new_code':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $id = (int)(trim($_REQUEST['id']));
                $send_new_code = Receipt::send_new_code($id, $errors); 
                if(!empty($send_new_code)){
                    $msg=__('Sent new code successfully');
                    FlashMsg::set('ticket_create', $msg);
                    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/receipt.php') !== false
                    ? $cfg->getBaseUrl() . '/scp/receipt.php?id='.$id.'&action=confirm_code' : $cfg->getBaseUrl() . '/scp/receipt.php';
                    header('Location: '. $referer);
                    exit();
                }
            }            
            break;
        case 'sales_confirm':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
                $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $cfg->getUrl().'scp/receiptlist.php') !== false
                ? $_SERVER['HTTP_REFERER'] : $cfg->getUrl().'scp/receiptlist.php';
                if ($results->status != 0 || $results != Receipt::MONEY_RECEIVED) {
                    header('Location: '.$referer);
                    exit;
                }
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.sales-confirm.inc.php";
            break; 
        case 'rejected':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.reject.inc.php";
            break;   
        case 'confirm_financial':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = 'receipt.confirm-financial.inc.php';
            break;
        case 'print':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
                $receipt_print = ReceiptPrint::create([
                    'receipt_id' => (int)$results->id,
                    'log_id'     => (int)$results->log_id,
                    'created_by' => $thisstaff->getId(),
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                $receipt_print->save(true);
                $print_count = ReceiptPrint::objects()->filter(['receipt_id' => (int)$results->id])->count();
                $results->set('print_count', (int)$print_count);
                $results->save(true);
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.print.inc.php";
            break; 
        case 'confirm_code':
            if (isset($_REQUEST['id']) && (int)(trim($_REQUEST['id']))) {
                $_REQUEST['id'] = (int)(trim($_REQUEST['id']));
                $results = Receipt::lookup(['id' => $_REQUEST['id']]);
                $results_sms = ReceiptConfirmationCode::lookup(['receipt_id' => $results->id]);
            }else {
                $errors['id'] = 'No ID';
            }
            $inc = "receipt.confirm-code.inc.php";
            break; 
        case 'receipt_upload':
            $receipt_id = (int)($_REQUEST['id']);
            $result = Receipt::lookup($receipt_id);

            //create qr_code
            $url = $cfg->getUrl().'scp/receipt.php?id='.(int)$_REQUEST['id'].'&action=receipt_upload';
            $file_name = QrCodeToken::createAndSaveQrCode($url,$errors, $token);
            $path = __DIR__.'/../qr_code/'. $file_name;
            $dataImage = convertFileToBas64($path);

            if($result)
                $inc = 'receipt-upload.inc.php';
            break;
        case 'qr_code':
            $error = null;
            $url = $cfg->getUrl().'scp/receipt.php?id='.(int)$_REQUEST['id'].'&action=receipt_upload';
            $token = QrCodeToken::createToken();
            QrCodeToken::createQrCode($url,$error, $token);
            if($error) echo $error;
            exit();
            break;
        case 'confirm_image':
            $receipt_id = (int)($_REQUEST['id']);
            $result = Receipt::lookup($receipt_id);
            if($result) {
                $name_image = $result->image_name;
                if(!empty($name_image)) {
                    $name_image = 'receipt_upload/' . $name_image . '.png';
                    $inc = 'receipt-confirm-image.inc.php';
                }
                else $name_image = '';
            }
            break;
        }
    }
$nav->setTabActive('booking');
$nav->setActiveSubMenu(0,'dayoff');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.system_logs" />',
    "$('#content').data('tipNamespace', 'dashboard.system_logs');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$inc);
include(STAFFINC_DIR.'footer.inc.php');
?>

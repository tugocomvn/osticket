<?php
/*********************************************************************
    class.reservation.php

    Help topic helper

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

require_once(INCLUDE_DIR . 'class.orm.php');

class ReservationModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_RESERVATION_TABLE,
        'pk' => ['id'],
    ];
}
class Reservation extends ReservationModel{

} 
?>

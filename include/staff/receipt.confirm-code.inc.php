<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canEditReceipt()) die('Access Denied');

if ($thisstaff->getId() != $results->created_by
    && $thisstaff->getId() != $results->staff_cashier_id
    && $thisstaff->getId() != $results->staff_id
    && !$thisstaff->canRejectReceipt()) die('Access Denied');
?>
<style>
h2 {
  border-bottom: 1px solid black;
}
</style>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>">Back</a>
<h2><?php echo __('Verification code');?></h2>
<div>
    <input type="hidden" id="receipt_id" value="<?php echo $results->id ?>" >
	<div class="centered form-row">
        <p class="centered">Mã phiếu thu: <b><?php echo $results->receipt_code ?></b></p>
        <p class="centered">Mã booking code: <b><?php echo $results->booking_code ?></b></p>
        <p class="centered">Tên khách: <b><?php echo $results->customer_name ?></b></p>		
        <p class="centered">Số điện thoại: <b><?php echo $results->customer_phone_number ?></b></p>	
        <p class="centered">Nhân viên gửi: <b><?php echo $results->staff_name ?></b></p>	
        <p class="centered">Tin nhắn được gửi lúc: <b><?php if(strtotime($results_sms->created_at)) echo date("d-m-Y H:i",strtotime($results_sms->created_at)) ?></b></p>	
    </div>
    <div class="centered" style="margin-top: 20px;">
        <label><b>Verification code</b></label>
        <input type="text" id="confirm_code" maxlength="6" class="input-field" name="confirm_code" value="" placeholder="Enter verification code">
        <div id="message"></div>
        <p><font class="error">*</font>This code will be valid for 10 minutes after you request it.</p>
        <p>Note: If you have't received verification code or it is expired or already used,
            please <a class="new_code btn_sm btn-xs btn-info"
                      href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php
                      echo $results->id ?>&action=new_code"
                >request a new one</a>. </p>
    </div>
	<div class="centered row">
        <a href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>" class="btn_sm btn-default btn-sm">Cancel</a>
        <?php if($thisstaff->canEditReceipt()): ?>
            <input id="verify" class="btn_sm btn-primary btn-sm btnVerify" type="button" value="Verify Code">
        <?php endif; ?>		
    </div>
</div>
<script>
    (function($) {
        $("#verify").attr("disabled", true);
        $('#confirm_code').off('change, keyup').on('change, keyup', function() {
            var code = $('#confirm_code').val();
            if(code.length > 5){
                $("#verify").attr("disabled", false);
            }else{
                $("#verify").attr("disabled", true);
            }
        });
        $("#verify").click(function() {
            var confirm_code = $("#confirm_code").val();
            var message = $('#message');
            var receipt_id = $('#receipt_id').val();
            if(confirm_code != null || confirm_code != "") {
                var data = {
                    "verification_code" : confirm_code,
                    "receipt_id" : receipt_id,
                    "action" : "send_code"
                };
                $.ajax({
                    dataType: 'json',
                    type : 'POST',
                    url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/receipt/checkAvailableCode',
                    data: data, 
                    success : function(response) {
                        if(response.result === 1) {
                            messageSuccess();
                            message.text(response.message);
                            <?php $url = $cfg->getUrl().'scp/receipt.php?id='.$results->id.'&action=view'; ?>
                            parent.setTimeout(function() { parent.window.location = "<?php echo $url  ?>"; }, 3000);       
                        } else{
                            messageError();
                            message.text(response.message);
                        }
                    },
                });
            } else {
                $("#message").text('Please enter a valid code!')
            }
        });
        function messageError(){
            document.getElementById("message").style.color = "Red";
        }
        function messageSuccess(){
            document.getElementById("message").style.color = "Green";
        }
        $('.new_code').off('click').on('click', function(e) {
            if (!confirm('Bạn có muốn gửi lại mã xác nhận phiếu thu cho khách ?')) {
                e.preventDefault();
                return false;
            }
        });
    })(jQuery);
</script>

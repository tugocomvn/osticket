<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

$qs = array();
$agent=null;
$action=null;

if (isset($_REQUEST['agent']) && $_REQUEST['agent']) {
    $agent = $_REQUEST['agent'];
    $qs += array('agent' => $_REQUEST['agent']);
}

if (isset($_REQUEST['action']) && $_REQUEST['action']) {
    $action = $_REQUEST['action'];
    $qs += array('action' => $_REQUEST['action']);
}

$qwhere =' WHERE t.topic_id=1 ';
//Type
if($agent)
    $qwhere.=' AND s.staff_id='.db_input($agent);
if($action)
    $qwhere.=' AND tr.title LIKE '.db_input('%assigned%');

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere.=' AND tr.created>=FROM_UNIXTIME('.$startTime.')';
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere.=' AND tr.created<=FROM_UNIXTIME('.$endTime.')';
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
$sortOptions = array(
    'created'=>'tr.created', 'title'=>'tr.title',
    'username'=>'s.username','firstname'=>'s.firstname','lastname'=>'s.lastname'
);
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'id';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'tr.created';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$qselect = 'SELECT s.staff_id,
    s.firstname,
    s.lastname,
    s.username,
    t.ticket_id,
    t.number,
    tr.title,
    tr.thread_type,
    tr.id,
    tr.body,
    tr.created,
    tr.updated,
    st.name AS status';
$qfrom=sprintf(' FROM
    %s t
        LEFT JOIN
    %s tr ON t.ticket_id = tr.ticket_id
        JOIN
    %s s ON s.staff_id = t.`staff_id` 
        JOIN 
    %s st ON st.id=t.status_id ',
    TICKET_TABLE,
    TICKET_THREAD_TABLE,
    STAFF_TABLE,
    TICKET_STATUS_TABLE
);
$total=db_count("SELECT count(*) $qfrom $qwhere");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
//pagenate
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('agent.php',$qs);
$qs += array('order' => ($order=='DESC' ? 'ASC' : 'DESC'));
$qstr = '&amp;'. Http::build_query($qs);
$query="$qselect $qfrom $qwhere ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' '.$title;
else
    $showing=__('No logs found!');
?>

<h2><?php echo __('Agent Logs');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>
<div id='filter' >
    <form action="agent.php" method="get">
        <div style="padding-left:2px;">
            <b><?php echo __('Date Span'); ?></b>&nbsp;<i class="help-tip icon-question-sign" href="#date_span"></i>
            <?php echo __('Between'); ?>:
            <input class="dp" id="sd" size=15 name="startDate" value="<?php echo Format::htmlchars($_REQUEST['startDate']); ?>" autocomplete=OFF>
            &nbsp;&nbsp;
            <input class="dp" id="ed" size=15 name="endDate" value="<?php echo Format::htmlchars($_REQUEST['endDate']); ?>" autocomplete=OFF>
            &nbsp;<?php echo __('Agent'); ?>:&nbsp</i>
            <select name='agent'>
                <option value><?php echo __('All');?></option>
                <?php
                $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                    .' FROM '.STAFF_TABLE.' staff '
                    .' ORDER by name';
                if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                    while(list($id,$name)=db_fetch_row($agent_res)){
                        $selected=($_REQUEST['agent'] && $id==$_REQUEST['agent'])?'selected="selected"':'';
                        echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                    }
                }
                ?>
            </select>
            <?php echo __('Actions'); ?>:&nbsp</i>
            <select name='action'>
                <option value><?php echo __('All');?></option>
                <option value="assign">Assign</option>
            </select>
            &nbsp;&nbsp;
            <input type="submit" Value="<?php echo __('Go!');?>" />
        </div>
    </form>
</div>

<table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <caption><?php echo $showing; ?></caption>
    <thead>
    <tr>
        <th><?php echo __('Ticket Number');?></th>
        <th><?php echo __('Status');?></th>
        <th><a  <?php echo $username_sort; ?> href="agent.php?<?php echo $qstr; ?>&sort=username"><?php echo __('Agent');?></a></th>
        <th><?php echo __('Title');?></th>
        <th><?php echo __('Message');?></th>
        <th><a  <?php echo $created_sort; ?> href="agent.php?<?php echo $qstr; ?>&sort=created"><?php echo __('Created');?></a></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $total=0;
    $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
    if($res && db_num_rows($res)):
        while ($row = db_fetch_array($res)) {
            $sel=false;
            if($ids && in_array($row['log_id'],$ids))
                $sel=true;
            ?>
            <tr id="<?php echo $row['log_id']; ?>">
                <td>&nbsp;<a class="no-pjax" target="_blank" href="/scp/tickets.php?id=<?php echo $row['ticket_id'] ?>#thread-id-<?php echo $row['id'] ?>"><?php echo Format::htmlchars($row['number']); ?></a></td>
                <td><?php echo $row['status']; ?></td>
                <td><?php echo sprintf("%s %s", $row['firstname'], $row['lastname']); ?></td>
                <td><?php echo $row['title']; ?></td>
                <td><?php echo substr(strip_tags($row['body']), 0, 128); ?></td>
                <td>&nbsp;<?php echo Format::db_daydatetime($row['created']); ?></td>
            </tr>
            <?php
        } //end of while.
    endif; ?>
    </tbody>
    <tfoot>

    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>


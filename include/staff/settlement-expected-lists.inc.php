<?php
global $cfg;
?>
<style>
    .clone.from + div {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        width: 0;
        height: 0;
    }

    .clone.to + div  {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
        width: 0;
        height: 0;
    }

    .clone + div {
        display: inline-block;
        width: 9px;
        height: 9px;
    }

    .clone[disabled] {
        visibility: hidden;
    }

    tr .actions * {
        visibility: hidden;
    }

    tr:hover .actions * {
        visibility: visible;
    }

    tr .actions .from,
    tr .actions .to,
    tr .actions .from + div,
    tr .actions .to + div {
        visibility: visible;
    }

    .visa_result {
        white-space: nowrap;
    }

    table.list tbody td,
    table.list thead th,
    table.list tfoot th {
        padding-top: 0.5em;
        padding-bottom: 0.5em;
        vertical-align: middle;
    }
</style>

<script>
    function setView(view) {
        document.location.href = replaceUrlParam(document.location.href, 'view', view);;
    }
</script>

<h2>Expected Settlement <small>List</small></h2>

<div class="clearfix">
    <div class="pull-right">
        View:
        <select name="view" id="view" class="input-field" onchange="setView(this.value)">
            <option value=>Default</option>
            <?php while($view_list && ($view_item = db_fetch_array($view_list))): ?>
                <option <?php if (strtolower($view_item['name']) == strtolower($_REQUEST['view'])) echo 'selected' ?>
                        value="<?php echo $view_item['name'] ?>"><?php echo $view_item['name'] ?></option>
            <?php endwhile; ?>
        </select>
    </div>
</div>
<form action="" method="get" id="search">
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>">
    <table>
        <tr>
            <td>Điểm Đến</td>
            <td>
                <select name="tour[]" class="input-field" multiple style="height: 12em;">
                    <option value=><?php echo __('-- All --') ?></option>
                    <?php foreach($destination_list as $id => $name): ?>
                        <option value="<?php echo $id ?>"
                            <?php if (isset($_REQUEST['tour']) && ((is_array($_REQUEST['tour']) && in_array($id, $_REQUEST['tour'])) || ($_REQUEST['tour']==$id && is_numeric($_REQUEST['tour'])))) echo 'selected' ?>
                        ><?php echo $name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>Hãng bay</td>
            <td>
                <select name="airline" class="input-field">
                    <option value=><?php echo __('-- All --') ?></option>
                    <?php foreach($airlines_list as $id => $name): ?>
                        <option value="<?php echo $id ?>"
                            <?php if (isset($_REQUEST['airline']) && $_REQUEST['airline']==$id) echo 'selected' ?>
                        ><?php echo $name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>Ngày tập trung</td>
            <td>Từ</td>
            <td>
                <input type="text" class="input-field dp" name="from" value="<?php if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from'])) echo date('d/m/Y', strtotime($_REQUEST['from'])) ?>">
            </td>
            <td>Đến</td>
            <td>
                <input type="text" class="input-field dp" name="to" value="<?php if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to'])) echo date('d/m/Y', strtotime($_REQUEST['to'])) ?>">
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="5">
                <button class="btn_sm btn-default" name="reset" value="reset" type="reset">Reset</button>
                <button class="btn_sm btn-success" name="action" value="search">Search</button>
            </td>
        </tr>
    </table>
</form>
<p>
    <?php echo $showing ?>
</p>
<table class="list" width="1258" border="0" cellspacing="0" cellpadding="2" style="margin-left: -100px;">
<thead>
<tr>
    <th>#</th>
    <th>Departure Date</th>
    <th>Name</th>
    <th>Airline</th>
    <th>Pax Qty</th>
    <th>Visa Qty</th>
    <th>Rate</th>

    <?php if($view): ?>
        <?php foreach($spend_items as $_spend_item): ?>
            <th><?php echo $_spend_item['name'] ?></th>
        <?php endforeach; ?>
    <?php else: ?>
        <th>Income</th>
        <th>Outcome</th>
        <th>Profit</th>
    <?php endif; ?>

    <th>Note</th>
    <th>Action <button type="button" class="btn_sm btn-xs btn-info copy_btn"
                       data-title="Copy"
                       style="visibility: hidden;"><i class="icon-copy"></i></button></th>
</tr>
</thead>
<tbody>
<?php
    $i = 1+$offset;
    $count = 0;
    $rows = 0;
    $pax = 0;
    $rate_total = 0;
    $income = 0;
    $outcome = 0;
    $profit = 0;
?>
    <?php while($list && ($row = db_fetch_array($list))): ?>
    <tr>
    <?php
        $this_pax_count = PaxTour::countPax($row['id']);
        $visa_pax_count = PaxTour::countPax($row['id'], false, true);

        $total_spend_detail = 0;
        $spend_details = SettlementExpectedDetail::getPagination([ 'tour_id' => $row['id'] ], 0, 999, $total_spend_detail);
        $spend_in_table = [];

        foreach($spend_details as $_index => $_item) {
            if (isset($spend_items[ $_item['account_item_id'] ]))
            $spend_in_table[ $_item['account_item_id'] ] = $_item;
        }
    ?>
        <td><?php echo $i++; ?></td>
        <td class="gather_date"><?php if($row['gather_date']) echo date('Y-m-d H:i', strtotime($row['gather_date'])) ?></td>
        <td class="destination">
            <?php if (isset($destination_list[$row['destination']])): ?>
                <span class="label label-default label-small">
                    <?php echo $destination_list[$row['destination']] ?>
                 </span>
                <br>
            <?php endif; ?>
            <?php echo $row['name'] ?>
        </td>
        <td><?php if (isset($airlines_list[$row['airline']])) echo $airlines_list[$row['airline']] ?></td>
        <td><?php echo $this_pax_count; ?><?php if ($this_pax_count) echo '+1' ?></td>
        <td>
            <?php foreach($visa_pax_count as $result => $_result): ?>
            <span class="visa_result"><?php echo VisaResult::getResult( $result ) ?>: <?php echo $_result['total'] ?></span><br>
            <?php endforeach; ?>
            <span class="visa_result">+ 1</span>
        </td>
        <td><?php if($row['rate']) echo number_format($row['rate'], 0, '.', ',') ?></td>

        <?php if($view): ?>
            <?php foreach($spend_items as $_item): ?>
                <td>
                    <?php echo $spend_in_table[ $_item['id'] ]['quantity'] ?>
                    <br>
                    <em><?php if ($spend_in_table[ $_item['id'] ]['total'])
                        echo '= '.number_format($spend_in_table[ $_item['id'] ]['total'], 2, '.', ',') ?></em>
                </td>
            <?php endforeach; ?>
        <?php else: ?>
            <td>
                <?php if($row['expected_income']) echo number_format($row['expected_income'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['expected_income'])
                            echo number_format($row['rate'] * $row['expected_income'], 0, '.', ',') . ' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if($row['expected_outcome']) echo number_format($row['expected_outcome'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['expected_outcome'])
                            echo number_format($row['rate'] * $row['expected_outcome'], 0, '.', ',').' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if((float)$row['expected_income'] - (float)$row['expected_outcome'])
                    echo number_format((float)$row['expected_income'] - (float)$row['expected_outcome'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * ((float)$row['expected_income'] - (float)$row['expected_outcome']))
                            echo number_format($row['rate'] * ((float)$row['expected_income'] - (float)$row['expected_outcome']), 0, '.', ',').' VND' ?></i>
                </small>
            </td>

        <?php endif; ?>

        <td><?php echo trim($row['note']) ?></td>
        <td class="actions">
            <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/expected_settlement.php?action=details&id=<?php echo $row['id'] ?>"
               class="btn_sm btn-xs btn-success" title="View Details" data-title="View"><i class="icon-list"></i></a>
            <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/expected_settlement.php?action=edit&id=<?php echo $row['id'] ?>"
               class="btn_sm btn-xs btn-default" data-title="Edit"><i class="icon-edit"></i></a>
            <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/expected_settlement.php?action=clone_to_actual&id=<?php echo $row['id'] ?>"
               class="btn_sm btn-xs btn-primary clone_actual" data-title="Clone"><i class="icon-copy"></i></a>
            <input type="checkbox" class="input-field clone" value="<?php echo $row['id'] ?>"> <div></div>
        </td>
    </tr>
    <?php
        $rate_total += $row['rate'];
        if (isset($row['rate']) && $row['rate']) $count++;
        $pax += $this_pax_count;
        if ($this_pax_count) $rows++;
        $income += $row['expected_income'];
        $outcome += $row['expected_outcome'];
        $profit += $row['expected_income'] - $row['expected_outcome'];
    ?>
    <?php endwhile; ?>
<tr>
    <?php
        $avg_rate = 0;
        if ($count) $avg_rate = $rate_total/$count;
    ?>
    <th colspan="4">Total</th>
    <th><?php echo $pax; ?> + <?php echo $rows ?></th>
    <th></th>
    <th>
        ~ <?php echo number_format($avg_rate, 0, '.', ','); ?>
    </th>
    <th>
        <?php if ($income): ?>
            <?php  echo number_format($income, 2, '.', ',').' $'; ?>
            <br>
        <?php endif; ?>
        <?php if($avg_rate * $income): ?>
            <small>
                <i><?php  echo number_format($avg_rate * $income, 0, '.', ',').' VND' ?></i>
            </small>
            <br>
        <?php endif; ?>
        Income
    </th>
    <th>
        <?php if ($outcome): ?>
        <?php  echo number_format($outcome, 2, '.', ','). ' $'; ?>
        <br>
        <?php endif; ?>
        <?php if($avg_rate * $outcome): ?>
        <small>
            <i><?php  echo number_format($avg_rate * $outcome, 0, '.', ',').' VND' ?></i>
        </small>
        <br>
        <?php endif; ?>
        Outcome
    </th>
    <th>
        <?php if ($profit): ?>
            <?php  echo number_format($profit, 2, '.', ',').' $'; ?>
            <br>
        <?php endif; ?>
        <?php if($avg_rate * $profit): ?>
            <small>
                <i><?php echo number_format($avg_rate * $profit, 0, '.', ',').' VND' ?></i>
            </small>
            <br>
        <?php endif; ?>
        Profit
    </th>
    <th>
        <?php if ($profit): ?>
            <?php echo number_format($profit/$pax, 2, '.', ',').' $'; ?>
            <br>
        <?php endif; ?>
        <?php if($avg_rate * $profit/$pax): ?>
            <small>
                <i><?php echo number_format($avg_rate * $profit/$pax, 0, '.', ',').' VND' ?></i>
            </small>
            <br>
        <?php endif; ?>
        Profit/pax
    </th>
</tr>
</tbody>
</table>
<?php
if ($total) //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
?>

<script>
    jQuery.fn.visible = function() {
        return this.css('visibility', 'visible');
    };

    jQuery.fn.invisible = function() {
        return this.css('visibility', 'hidden');
    };

    jQuery.fn.visibilityToggle = function() {
        return this.css('visibility', function(i, visibility) {
            return (visibility == 'visible') ? 'hidden' : 'visible';
        });
    };

    function post(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }

    $('.clone_actual').on('click', function(e) {
        e.preventDefault();
        _self = $(e.target);
        from_parent = _self.parents('tr');
        from_text = from_parent.find('.destination span').text().trim() +' '+ from_parent.find('.gather_date').text();

        if (!confirm('Clone spend items from ['+from_text +'] to ' + ' actual settlement?')) return false;

        post('<?php echo $cfg->getBaseUrl() ?>/scp/expected_settlement.php', {
            action: 'clone_actual',
            from: from_parent.find('.clone').val(),
            referer: '<?php echo $referer; ?>',
            __CSRFToken__: '<?php echo $ost->getCSRFToken(); ?>'
        });
    });

    $('.clone').on('click', function(e) {
        _self = $(e.target);

        $('.clone:not(:checked)').prop('disabled', false);
        $('.copy_btn').invisible();
        if (1 === $('.clone:checked').length) {
            $('.clone:checked').addClass('from').removeClass('to');
            $('.clone:not(:checked)').removeClass('from').removeClass('to');

        } else if (2 === $('.clone:checked').length) {
            _self.addClass('to').removeClass('from');

            $('.clone:not(:checked)').prop('disabled', true);
            $('.copy_btn').visible();
        }

        $('.clone:not(:checked)').removeClass('from').removeClass('to');
    });

    $('.copy_btn').on('click', function() {
        from_parent = $('.clone.from').parents('tr');
        from_text = from_parent.find('.destination span').text().trim() +' '+ from_parent.find('.gather_date').text();

        to_parent = $('.clone.to').parents('tr');
        to_text = to_parent.find('.destination span').text().trim() +' '+ to_parent.find('.gather_date').text();
        if (!confirm('Clone spend items from ['+from_text +'] to ' + '['+to_text+']?')) return false;
<?php
global $ost;
?>
        post('<?php echo $cfg->getBaseUrl() ?>/scp/expected_settlement.php', {
            action: 'clone',
            from: $('.clone.from').val(),
            to: $('.clone.to').val(),
            referer: '<?php echo $referer; ?>',
            __CSRFToken__: '<?php echo $ost->getCSRFToken(); ?>'
        });
    });
</script>

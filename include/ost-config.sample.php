<?php
/*********************************************************************
ost-config.php

Static osTicket configuration file. Mainly useful for mysql login info.
Created during installation process and shouldn't change even on upgrades.

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2010 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
$Id: $
 **********************************************************************/

#Disable direct access.
if(!strcasecmp(basename($_SERVER['SCRIPT_NAME']),basename(__FILE__)) || !defined('INCLUDE_DIR'))
    die('kwaheri rafiki!');

#Install flag
define('OSTINSTALLED',TRUE);
if(OSTINSTALLED!=TRUE){
    if(!file_exists(ROOT_DIR.'setup/install.php')) die('Error: Contact system admin.'); //Something is really wrong!
    //Invoke the installer.
    header('Location: '.ROOT_PATH.'setup/install.php');
    exit;
}

# Encrypt/Decrypt secret key - randomly generated during installation.
define('SECRET_SALT','wbQYMuj1CSV90JMe9KkuylW1drEl0ngO');

#Default admin email. Used only on db connection issues and related alerts.
define('ADMIN_EMAIL','it@tugo.com.vn');
define('DEV_EMAIL','phamquocbuu@gmail.com');

# Database Options
# ---------------------------------------------------
# Mysql Login info
define('DBTYPE','mysql');
define('DBHOST','localhost');
define('DBNAME','');
define('DBUSER','');
define('DBPASS','');

# Table prefix
define('TABLE_PREFIX','ost_');

define('TODO_TOPIC', 12);
define('THU_TOPIC', 17);
define('CHI_TOPIC', 18);
define('CHI_FORM', 16);
define('THU_FORM', 12);
define('IO_TOPIC', serialize(array(THU_TOPIC,CHI_TOPIC)));
define('IO_FORM', serialize(array(CHI_FORM,THU_FORM)));
define('THU_METHOD_LIST', 7);
define('CHI_METHOD_LIST', 9);
define('BOOKING_STATUS_LIST', 12);
define('PARTNER_LIST', 13);
define('BOOKING_TYPE_LIST', 11);

define('BOOKING_TOPIC', 19);
define('BOOKING_DEPARTMENT', 9);
define('BOOKING_FORM', 19);
define('BOOKING_CODE_FIELD', 79);
define('BOOKING_NO_FIELD', 59);

define('BOOKING_BOOKING_CODE', 79);
define('BOOKING_STATUS', 90);
define('BOOKING_CUSTOMER', 81);
define('BOOKING_PHONE_NUMBER', 82);
define('BOOKING_QUANTITY', 84);
define('BOOKING_RECEIPT_CODE', 80);
define('BOOKING_REFERRAL_CODE', 98);

define('FEEDBACK_TOPIC', 2);
define('SALE_DEPT', serialize(array(1,7, 15)));
// define('SALE_GROUPIDs', serialize(array(2)));
define('SALE_TOPIC', serialize(array(1,13,20,12)));

define('APP_ID', '529351340785863');
define('APP_SECRET', '303c898e1837f9a2db67bc9f18f9d750');
define('AUDIENCE_SET', '6065926465709');

define('SMS_CHAR_LIMIT', 320);
define('MYSQL_ERROR_DUPLICATE_CODE', 1062);
define('MYSQL_ERROR_GONE_AWAY_CODE', 2006);

define('APP_USER_RANK_LIST', 19);
define('APP_USER_RANK_FIELD_ID', 135);

define('OFFLATE_FORM',30);
define('OL_FORM',21);

define('USER_PHONE_NUMBER_FIELD', 3);

define('TOUR_DESTINATION_LIST', 1);
define('TOUR_LIST_ID', 15);

define('TOUR_PROERTIES_FORM_ID', 24);
define('AIRLINE_LIST', 17);
define('OPERATOR_TOUR', 15);
define('COUNTRY_LIST', 21);

define('OL_TOPIC',21);
define('TICKET_NOT_INCLUDE_TOPIC', serialize(array(THU_TOPIC, CHI_TOPIC, OL_TOPIC, BOOKING_TOPIC)));

define('ABSENCE_REQUEST_DENIED_TITLE', 'Phiếu xin nghỉ phép %s bị từ chối bởi %s (%s)');
define('ABSENCE_REQUEST_APPROVED_TITLE', 'Phiếu xin nghỉ phép %s được chấp nhận bởi %s (%s)');
define('ABSENCE_REQUEST_TRANSFER_TITLE', 'Phiếu xin nghỉ phép %s được %s (%s) duyệt và chuyển đến %s (%s) duyệt lần 2');

define('EMATIC_MC_DATA_CENTER', 'us16');
define('EMATIC_MC_API_KEY', '7090ec095d3256042a961b4b799c7895-'.EMATIC_MC_DATA_CENTER);
define('EMATIC_MC_LIST_ID', '2140897d9d');

define('NO_EDIT_USERS', serialize([
                                      'phamquocbuu@gmail.com',
                                      'beckerbao29@gmail.com',
                                      'beckerbao@gmail.com',
                                      'it@tugo.com.vn',
                                      'support@tugo.com.vn',
                                      'support@tugo.vn',
                                      'alerts@tugo.com.vn',
                                      'tour@tugo.com.vn',
                                      'notifications@cognitoforms.com',
                                      '108hcm@gmail.com',
                                      'noreply@subiz.com',
                                      'yunaney1912@gmail.com',
                                      'thuynguyendg372@gmail.com',
                                      'micnguyen94@gmail.com',
                                      'htuyenthao13p@gmail.com',
                                      'namcool1307@gmail.com',
                                      'quynguyen140193@gmail.com',
                                      'dothithanhhai11493@gmail.com',
                                      'dulichvietnam3579@gmail.com',
                                      'thaodoan171093@gmail.com',
                                      'caominh171@gmail.com',
                                      'phanthuyan1993@gmail.com',
                                      'hothinhuhuyen.296@gmail.com',
                                      'dtvy94@gmail.com',
                                      'halu.pham90@gmail.com',
                                      'nguyenhuukhanh7979@gmail.com',
                                      'hai310394@gmail.com',
                                      'tratour82@gmail.com',
                                      'bangtuong210405@gmail.com',
                                      'tamnt2090@gmail.com',
                                      'linhtugohanoi@gmail.com',
                                      'codyau1993@gmail.com',
                                      'tuongtranthi95@gmail.com',
                                      'jintae1710@gmail.com',
                                      'letram208@gmail.com',
                                      'nguyenhongnhung1112@gmail.com',
                                      'jacqueline.nguyen1004@gmail.com',
                                      'maitrunghieu040488@gmail.com',
                                      'sangvienthong@gmail.com',
                                      'vantrangtn1123@gmail.com',
                                      'thienkimng97@gmail.com',
                                      'thucuyen0107@gmail.com',
                                      'm.tntt.97@gmail.com',
                                      'postmaster@m.buu.vn',
                                      'postmaster@m.buupq.com',
                                  ]));
#
# SSL Options
# ---------------------------------------------------
# SSL options for MySQL can be enabled by adding a certificate allowed by
# the database server here. To use SSL, you must have a client certificate
# signed by a CA (certificate authority). You can easily create this
# yourself with the EasyRSA suite. Give the public CA certificate, and both
# the public and private parts of your client certificate below.
#
# Once configured, you can ask MySQL to require the certificate for
# connections:
#
# > create user osticket;
# > grant all on osticket.* to osticket require subject '<subject>';
#
# More information (to-be) available in doc/security/hardening.md

# define('DBSSLCA','/path/to/ca.crt');
# define('DBSSLCERT','/path/to/client.crt');
# define('DBSSLKEY','/path/to/client.key');

#
# Mail Options
# ---------------------------------------------------
# Option: MAIL_EOL (default: \n)
#
# Some mail setups do not handle emails with \r\n (CRLF) line endings for
# headers and base64 and quoted-response encoded bodies. This is an error
# and a violation of the internet mail RFCs. However, because this is also
# outside the control of both osTicket development and many server
# administrators, this option can be adjusted for your setup. Many folks who
# experience blank or garbled email from osTicket can adjust this setting to
# use "\n" (LF) instead of the CRLF default.
#
# References:
# http://www.faqs.org/rfcs/rfc2822.html
# https://github.com/osTicket/osTicket-1.8/issues/202
# https://github.com/osTicket/osTicket-1.8/issues/700
# https://github.com/osTicket/osTicket-1.8/issues/759
# https://github.com/osTicket/osTicket-1.8/issues/1217

# define(MAIL_EOL, "\r\n");

#
# HTTP Server Options
# ---------------------------------------------------
# Option: ROOT_PATH (default: <auto detect>, fallback: /)
#
# If you have a strange HTTP server configuration and osTicket cannot
# discover the URL path of where your osTicket is installed, define
# ROOT_PATH here.
#
# The ROOT_PATH is the part of the URL used to access your osTicket
# helpdesk before the '/scp' part and after the hostname. For instance, for
# http://mycompany.com/support', the ROOT_PATH should be '/support/'
#
# ROOT_PATH *must* end with a forward-slash!

# define('ROOT_PATH', '/support/');

#
# Session Storage Options
# ---------------------------------------------------
# Option: SESSION_BACKEND (default: db)
#
# osTicket supports Memcache as a session storage backend if the `memcache`
# pecl extesion is installed. This also requires MEMCACHE_SERVERS to be
# configured as well.
#
# MEMCACHE_SERVERS can be defined as a comma-separated list of host:port
# specifications. If more than one server is listed, the session is written
# to all of the servers for redundancy.
#
# Values: 'db' (default)
#         'memcache' (Use Memcache servers)
#         'system' (use PHP settings as configured (not recommended!))
#
define('SESSION_BACKEND', 'system');
# define('MEMCACHE_SERVERS', 'server1:11211,server2:11211');

define('APPLICATION_NAME', 'Google Sheets API PHP Quickstart');
define('CLIENT_SECRET_PATH', __DIR__.'/../credentials/client_secret_849767727205-q3mmuplspla3o13drj8bevb08ujacl7m.apps.googleusercontent.com.json');

define('CREDENTIALS_PATH', __DIR__.'/../credentials/sheets.googleapis.com-php-quickstart.json');
define('GOOGLE_SHEET_ID_CALL_LOG', '1IEjUpG3jKDWBunOrc9ryyjtNzeH7yymbCuapvXdQgyA');

define('CREDENTIALS_AGENT_PATH', __DIR__.'/../credentials/sheets.googleapis.com-agent.json');
define('GOOGLE_SHEET_AGENT_LIST', '1NPQ8BSNOkkhS0nqQ3J6YpuYVgqN9T9C8aXTOPKsPPNo');

// define('GOOGLE_SHEET_BOOKING', '1OyUpDzzWLdMETRl2h1m5W-sW-XCytKFRBXe0nz4boFI');
define('GOOGLE_SHEET_BOOKING', '1rh_HZRgq-qScRL3VQYj_V8ODwfQTR9NjA2IOR9WTOhc');
define('CREDENTIALS_BOOKING_PATH', $_SERVER['DOCUMENT_ROOT'].'/credentials/sheets.googleapis.com-booking.json');
define('CLIENT_SECRET_PATH_BOOKING', $_SERVER['DOCUMENT_ROOT'].'/credentials/client_secret-booking.json');

define('ALERT', 'Đã có thể <a target="_blank" href="https://docs.google.com/spreadsheets/d/1IJYXBYLLxWvGyBKKIsyKVihxIjdH37u23pm7hxJoNPQ">Giữ chỗ <img src="https://cdn3.iconfinder.com/data/icons/iconano-web-stuff/512/109-External-16.png" /></a>, gia hạn và hủy chỗ ngay trong Ticket! Hãy bấm &gt;&gt;&gt;');

define('SYSTEM_EMAIL_USERNAME', '');
define('SYSTEM_EMAIL_PASSWORD', '');
define('SYSTEM_EMAIL_SERVER', 'mail.tugo.com.vn');
define('SYSTEM_EMAIL_SMTPPort', 465);
define('SYSTEM_EMAIL_SMTPSecure', true);

define('APP_FEEDBACK_EMAIL', 'khieunai@tugo.com.vn');
define('APP_REQUEST_EMAIL', 'support@tugo.com.vn');
define('BOOKING_EMAIL', 'buu@tugo.com.vn');


define('FIREBASE_SERVER_TOKEN', 'key=AAAA-7_h4Ms:APA91bEBrAyICTVMW4TmJzPPrIGUIM7eANjjhwAWXKCMPJXRqJz_JGUP30pvEa6wdRw91DzSvjcOgqoVa7fTR5iuN1t5lY4fZy_ciKpZXn5LScoMcgSBI7cwoULRppIIU5Shub2Ds6z4');

define('CHECKING_BOOKING_CODE', 1);
define('TOUR_PAX_LIST_ID', 15);

define('UPCOMING_OVERDUE', 30); // hours
define('REMIND_OVERDUE', 6); // days

define('CHECK_OTP_SPAM', 1);


define('TUTHIEN_TOURS', serialize([
                                      729, // Châu Âu 3 nước
                                      511, // Châu Âu 4 nước
                                      126, // Châu Âu 5 nước
                                      260, // Châu Âu 5 nước luxury
                                      259, // Châu Âu 7 nước
                                      139, // Canada
                                      342, // Anh Quốc (UK)
                                      127, // Mỹ
                                      313, // Mỹ (Free & Easy)
                                      343, // Mỹ-Canada
                                      873, // New Zealand-Úc
                                      150, // Pháp Mono
                                      258, // Úc Melbourne
                                      59, // Úc Sydney
                                  ]));

define('TUTHIEN_START_TIME', '2019-01-01');
define('TICKET_STATUS_SUCCESS', 10);
// define('SALE_CS__GROUPIDs', serialize(array(2, 25)));


define('BOOKING_CODE_PREFIX', 'TG123-');
define('BOOKING_CODE_PREFIX_SHORT', 'TG-');
define('WEBSITE_DOMAIN', 'www.tugo.com.vn');
define('WEBSITE_URL', 'https://'.WEBSITE_DOMAIN);
define('COMPANY_LOWER', 'tugo');
define('COMPANY_TITLE', 'Tugo');
define('NOTIFICATION_TITLE', COMPANY_TITLE.' System');
define('SYSTEM_EMAIL_NAME', COMPANY_TITLE.' System');

define('NO_OVERDUE_SLA_ID', 5);
define('NO_ANSWER_1ST_STATUS', 18);
define('OPENING_STATUS', 1);
define('CLOSED_STATUS', 7);
define('NO_ACTION_SMS', "Tugo da nhan duoc thong tin cua quy khach, cac ban Sale se goi lai trong thoi gian som nhat. De lien he gap, quy khach co the goi cho cac ban sau:
Tour Nhat Ban: Bang Tuong - 0984069625
Tour Han Quoc: Nguyen Au - 0938974807
Tour Chau Au, Anh Quoc: Bien Thuy - 0976474372
Uc, My, Canada: Hong Nhung - 0764559456
Tour khac (Dai Loan, Thai, Sing, Malay, v.v...): Cao Minh - 0966666171
");

define('DEV_ENV', 0);
define('OP_TOPIC', 15);
define('FINANCE_DEPT_ID', serialize([6,8,13,14,19]));
define('WELCOME_SUPPORT_EMAIL', 'support@welcome.vn');
define('UNASSIGN_CRON_LIMIT', 100);

define('SALE_TOPIC_NO_TODO', serialize(array(1,13,20)));
define('PAYMENT_NOT_IN', 16);
define('PAYMENT_NOT_OUT', 21);
define('PAYMENT_NOT_OUT1', 1021);
define('PAYMENT_NOT_OUT2', 1022);

define('EMAIL_DOMAIN', '@tugo.com.vn');


define('AFTER_SMS_COUNTRY_ID_HANQUOC', 941);
define('AFTER_SMS_COUNTRY_HANQUOC', 'Han Quoc');

define('AFTER_SMS_COUNTRY_ID_NHATBAN', 944);
define('AFTER_SMS_COUNTRY_NHATBAN', 'Nhat Ban');

define('AFTER_SMS_COUNTRY_ID_CHAUAU', 940);
define('AFTER_SMS_COUNTRY_CHAUAU', 'Chau Au');

define('AFTER_SMS_COUNTRY_ID_UC', 946);
define('AFTER_SMS_COUNTRY_UC', 'Uc');

define('AFTER_SMS_COUNTRY_ID_CANADA', 939);
define('AFTER_SMS_COUNTRY_CANADA', 'Canada');

define('AFTER_SMS_COUNTRY_ID_ANHQUOC', 938);
define('AFTER_SMS_COUNTRY_ANHQUOC', 'Anh Quoc');

define('AFTER_SMS_DATE_DIFF', 30);
define('AFTER_SMS_LIMIT', 20);

define('UNASSIGN_WELCOME_CRON_LIMIT', 10);
define('TODAY_SMS_500', 1556730000); // 2-5-2019 0:00:00

define('INCOME_TYPE_LIST_ID', 6);
define('OUTCOME_TYPE_LIST_ID', 8);

define('OLD_ATTACHMENT_CLEAN', 300);

define('PAYMENT_TYPE_TOURBOOKING', 15);

define('PAYMENT_BOOKING_CODE_MISSING_FROM', '2019-01-01');

define('FIX_AFTER_SALES_SMS', 0);

define('INCOME_EXCLUDE', 168);
define('OUTCOME_EXCLUDE', 169);


define('PROPERTIES1', 164);
define('PROPERTIES2', 165);
define('LIST_ID', 23);
define('DATE_TICKET_RETURN','2016-01-01');
define('DAY_LIMIT', 900);


define('TOUR_LEADER_LIST', 14);

define('CALLIN_ALERT_HOUR', 60);
define('CALLIN_ALERT_HOUR_LIMIT', 2);
define('CALLIN_ALERT_DAY', 1440);
define('CALLIN_ALERT_DAY_LIMIT', 3);

define('WORLDSMS_FROM_NAME', 'TugoTravel');
define('WORLDSMS_AUTHORIZATON_KEYS', 'dHVnbzozZTE0dnF6');
define('WORLDSMS_URL_SEND_SMS', "https://api-02.worldsms.vn/webapi/sendSMS");
//define('OSTICKET_URL_SEND_SMS', 'http://support.tugo.com.vn/sms/sms.php');
define('OSTICKET_URL_SEND_SMS', 'http://localhost:1180/sms/sms.php');
define('URL_SHORTENER', 'http://tugo.vn/');
define('TRACKING_UTM_CAMPAIGN', 'tgs_');
define('TRACKING_UTM_MEDIUM', 'tgs_');
define('TRACKING_UTM_SOURCE', 'tgs_');
define('TRACKING_UTM_CONTENT', 'tgs_');

define('AUTOMATION_CONTENT_DAILY_LIMIT', 100);

define('FORMAT_RECEIPT','TGF######');
define('OTP_EXPIRED',0.5); //OTP code expired
define('OTP_VERIFY','verify');

define('SALE_GROUPIDs', serialize(array(2, 10, 12, 23, 24)));
define('SALE_CS__GROUPIDs', serialize(array(2, 10, 12, 23, 24, 25)));

define('MAX_TIME_EXTEND_RESERVATION',48);
define('MAX_EXTEND_RESERVATION',2);
define('TIME_NEAR_DUE_RESERVATION', 24); //hour

define('TRANSIT_AIRPORT', 20);
define('VISA_DEADLINE_DANGER', 3600*24*28);
define('VISA_DEADLINE_WARNING', 3600*24*42);
define('OST_TOUR_HISTORY','ost_tour_history');

//receipt
define('PLACE_TO_GET_IN', 'Tân Sơn Nhất');
define('RECEIPT_CODE_LENGHT', 6);
define('RECEIPT_HASH_URL_LENGHT', 10);
//SMS phiếu thu và mã code
define('SMS_SEND_CODE_RECEIPT', 'Du lịch Tugo chào bạn, mã code phiếu thu của bạn là : ');
define('EXPIRED_CODE_RECEIPT', '. Mã code tồn tại trong 10 phút');
define('SMS_SEND_LINK_RECEIPT', '. Xem chi tiết phiếu thu của bạn tại đây ');
define('WEBSITE_DOMAIN_SUPPORT', 'support.tugo.com.vn');
define('WEBSITE_URL_SUPPORT', 'http://'.WEBSITE_DOMAIN_SUPPORT);
define('URL_RECEIPT', '/phieuthu/phieuthu.php?passkey=');
// SMS Kế toán xác nhận
define('SMS_SEND_APPROVAL_RECEIPT', 'Kế toán Tugo đã nhận tiền từ phiếu thu ');
define('SMS_SEND_REJECTED_RECEIPT', 'Kế toán Tugo đã hủy nhận tiền từ phiếu thu ');
define('CURRENCY_UNIT', ' VND của quý khách');
//
define('RECEIPT_CONFIRM_CODE_CUSTOMER_EXPIRE_AFTER', 10*60);

define('DAY_LIMIT_COUNT',90);
define('TIME_OVERDUE_GROUP1',-12);
define('TIME_OVERDUE_GROUP2',48);
define('TITLE_TIME_OVERDUE_GROUP1','GIỮ CHỖ GẦN ĐẾN HẠN TRONG VÒNG 12H');
define('TITLE_TIME_OVERDUE_GROUP2','GIỮ CHỖ QUÁ HẠN TRONG VÒNG 24H');
define('TITLE_BOOKING_LACK_COMMITMENT', 'BOOKING THIẾU TÀI LIỆU CAM KẾT');
define('TITLE_BOOKING_LACK_CONTRACT', 'BOOKING THIẾU TÀI LIỆU HỢP ĐỒNG');

define('PASSPORT_PHOTO_TABLE','pax_passport_photo');

define('EXPIRE_TOKEN_SECONDS', 60*3);

define('ID_BOOKING_HUY', 149);

define('RUN_BOOKING_FULLPAID', 1);
define('RUN_LONG_TIME_NO_SEE', 1);
define('BOOKING_FULLPAID_LIMIT', 1);
define('LONG_TIME_NO_SEE_LIMIT_1', 60);
define('LONG_TIME_NO_SEE_LIMIT_2', 180);
define('LONG_TIME_NO_SEE_LIMIT_3', 365);

define('GATEWAY_LIST', 16);
?>

<h2><?php echo __('Checkin Summary'); ?></h2>

<?php
include_once INCLUDE_DIR.'class.checkin.php';

if (!isset($_REQUEST['staff_from']) || empty($_REQUEST['staff_from']))
    $_REQUEST['staff_from'] = date('01/m/Y', time());

if (!isset($_REQUEST['staff_to']) || empty($_REQUEST['staff_to']))
    $_REQUEST['staff_to'] = date('d/m/Y', time());

$from = date_create_from_format('d/m/Y', $_REQUEST['staff_from'])
    ? date_create_from_format('d/m/Y', $_REQUEST['staff_from'])->format('Y-m-d') : date('Y-m-d');
$to = date_create_from_format('d/m/Y', $_REQUEST['staff_to'])
    ? date_create_from_format('d/m/Y', $_REQUEST['staff_to'])->format('Y-m-d') : date('Y-m-d');


$data = Checkin::getByStaff($thisstaff->getId(), $from, $to, 31, 0);
?>

<?php if(!$data): ?>
    <p>No data.</p>
<?php else: ?>
<div>
    <form action="" method="get" class="well form-inline">
        <?php csrf_token(); ?>
        <table class="">
            <tr>
                <td><b><?php echo __('Month'); ?></b></td>
                <td>
                    <input type="text" class="input-field dateq" name="staff_from" value="<?php echo $_REQUEST['staff_from'] ?>">
                </td>
                <td><b><?php echo __('To'); ?></b></td>
                <td>
                    <input type="text" class="input-field dateq" name="staff_to" value="<?php echo $_REQUEST['staff_to'] ?>">
                </td>
                <td>
                    <button class="btn_sm btn-default">Search</button>
                </td>
            </tr>
        </table>
    </form>
</div>

    <table class="dashbard_list list" border="0" cellspacing="1" cellpadding="2" width="1058">
    <thead>
    <tr>
        <th>#</th>
        <th>Staff</th>
        <th>Checkin time</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    while (($row = db_fetch_array($data))) {
        ?><tr>
            <td><?php echo $i++ ?></td>
            <td><?php echo $row['username'] ?></td>
            <td><?php echo $row['checkin_time'] ?></td>
        </tr><?php
    }
    ?>
    </tbody>
</table>
<?php endif; ?>
<script>
    var conf;
    /* Get config settings from the backend */
    getConfig().then(function(c) {
        conf = c;
        // Datepicker
        $('.dateq').datepicker({
            numberOfMonths: 1,
            showButtonPanel: true,
            buttonImage: './images/cal.png',
            showOn:'both',
            dateFormat: $.translate_format(c.date_format||'d/m/Y')
        });

    });
</script>
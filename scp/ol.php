<?php
$____start = microtime(1);
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');

require_once INCLUDE_DIR.'class.note.php';
require_once INCLUDE_DIR.'class.offlate.php';


if($ticket && $ticket->getId()) {
    $ticket_id = $ticket->getId();
    $errors=array();
    switch(strtolower($_POST['a'])):
        case 'postnote': /* Post Internal Note */
            $vars = $_POST;
            $note_form = new Form(array(
                'attachments' => new FileUploadField(array('id'=>'attach',
                'name'=>'attach:note',
                'configuration' => array('extensions'=>'')))
            ));
            $attachments = $note_form->getField('attachments')->getClean();
            $vars['cannedattachments'] = array_merge(
                $vars['cannedattachments'] ?: array(), $attachments);

            $wasOpen = ($ticket->isOpen());
            if(($note=$ticket->postNote($vars, $errors, $thisstaff))) {

                $msg=__('Internal note posted successfully');
                // Clear attachment list
                $note_form->setSource(array());
                $note_form->getField('attachments')->reset();

                if($wasOpen && $ticket->isClosed())
                    $ticket = null; //Going back to main listing.
                else
                    // Ticket is still open -- clear draft for the note
                    Draft::deleteForNamespace('ticket.note.'.$ticket->getId(),
                                              $thisstaff->getId());

            }
            break;
        default:
            $errors['err']=__('Unknown action');
    endswitch;
}

if(isset($_POST['delete']) && $_POST['delete'] && isset($_POST['id']) && $_POST['id']){
    $id = db_input($_POST['id']);
    $offlate = Offlate::lookup($_POST['id']);

    if($offlate && (isset($offlate->ol_status) && !$offlate->ol_status)
        && isset($offlate->staff_offer) && $thisstaff->getId() == $offlate->staff_offer) {
        $del_ol = "DELETE FROM offlate_tmp WHERE ticket_id=$id";
        $del_ol1 = "DELETE FROM ost_ticket WHERE ticket_id=$id";
        db_query($del_ol);
        db_query($del_ol1);

        $msg = 'Delete successfully.';
        FlashMsg::set('ticket_create', $msg);
        header('Location: /scp/ol.php?list=all');
    }
}

$page='ol.inc.php';
$nav->setTabActive('dayoff');
$ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.ol" />',
    "$('#content').data('tipNamespace', 'dayoff.ol');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

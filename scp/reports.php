<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('admin.inc.php');
require_once(INCLUDE_DIR.'class.export.php');

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'ticket_history':
            $days = isset($_REQUEST['days']) ? intval($_REQUEST['days']) : 7;
            $ts = strftime('%Y%m%d');
            $sql = sprintf('SELECT DISTINCT
                        t.number,
                        t.created,
                        s.username as assigned,
                        s2.username as updater,
                        tr.title,
                        tr.body,
                        tr.created as updated,
                        st.name,
                        t.isoverdue
                    FROM
                        %s t
                            JOIN
                        %s tr ON t.ticket_id = tr.ticket_id
                            JOIN
                        %s st ON st.id = t.status_id
                            JOIN
                        %s s ON s.staff_id = t.staff_id
                            JOIN
                        %s s2 ON s2.staff_id = tr.staff_id
                    WHERE
                        1
                            AND tr.created >= DATE_SUB(NOW(), INTERVAL %d DAY)
                    ORDER BY t.created',
                TICKET_TABLE,
                TICKET_THREAD_TABLE,
                TICKET_STATUS_TABLE,
                STAFF_TABLE,
                STAFF_TABLE,
                $days
            );

            if (!$thisstaff || !$thisstaff->isAdmin())
                $errors['err'] = __('Only Admins can export');
            elseif (!Export::saveTicketHistory($sql, "tickets-history-$ts.csv", 'csv'))
                $errors['err'] = __('Internal error: Unable to dump query results');

            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
}

$page='reports.inc.php';
$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.system_logs" />',
    "$('#content').data('tipNamespace', 'dashboard.system_logs');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

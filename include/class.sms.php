<?php

/**
 * Created by PhpStorm.
 * User: cosmos
 * Date: 7/20/16
 * Time: 5:25 PM
 */

//require_once(INCLUDE_DIR.'lib/nusoap.php');
require_once(INCLUDE_DIR.'class.curl.php');
require_once(INCLUDE_DIR.'/../lib/zalo/zalo.php');

class _SMS
{
    public static function send($phone_number, $message, &$error, $type = 'Ticket', $ticket_id = 0)
    {
        if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
            $phone_number = PHONE_SMS_DEV;
        }

        if (strlen($phone_number) == 11)
            $phone_number = _String::convertMobilePhoneNumber($phone_number);
        return MsgSMS::send($phone_number, $message, $error, $type, $ticket_id);
    }
}

class _Zalo extends Zalo
{
}

class MsgSMS {
    public static function send($phone_number, $message, &$error,
                                $type = 'Ticket', $ticket_id = 0) {
        return TicketMessage::send(
            $phone_number,
            $message,
            $error,
            $type,
            $ticket_id
        );
    }
}

class TicketMessage {
    public static function send($phone_number, $message, &$error,
                                $type = 'Ticket', $ticket_id = 0,
                                $msg_type = '')
    {
        $msgId = null;
        try {
            $msgId = _SMS_NEW::send($phone_number, $message, $error);

            if ($msgId) {
                $sql = sprintf(
                    "INSERT INTO %s SET `content` = %s, `send_time` = %s, 
                    `phone_number` = %s, `type` = %s, ticket_id = %s",
                    SMS_LOG_TABLE,
                    db_input($message),
                    db_input(date('Y-m-d H:i:s')),
                    db_input($phone_number),
                    db_input($msg_type . $type),
                    db_input($ticket_id)
                );

                db_query($sql);
            }

        } catch (Exception $ex) {
            if (!$error)
                $error['sending_failed'] = 'Sending failed';
            return false;
        }

        return isset($msgId) ? $msgId : true;
    }
}

class _SMS_NEW {
    public static function send($phone_number, $message, &$error) {


        $message = trim($message);//_String::khongdau(trim($message));
        $phone_number = _String::formatPhoneNumberSMS($phone_number);

        $res = static::api($phone_number, $message, $error);

        if ($res) return true;

        if (isset($error) && $error)
            $error['sending_failed'] = $error;

        return false;
    }

    static function api($phone_number, $message, &$error) {
        $curl = new _curl(WORLDSMS_URL_SEND_SMS, true, 60);

        $curl->setHeaders(
            [
                'Authorization: Basic ' . WORLDSMS_AUTHORIZATON_KEYS,
                'Content-Type: application/json',
                'Accept: application/json',
            ]
        );
        $curl->setRAWPostBody(true);
        $curl->setPost(
            [
                'to'   => $phone_number,
                'text' => $message,
                'from' => WORLDSMS_FROM_NAME,
                'unicode' => 1
            ]
        );

        $curl->createCurl();
        $string = json_decode($curl->__tostring(), true);

        if (($curl->getHttpStatus() != 201 && $curl->getHttpStatus() != 200)
            || (!isset($string['status']) || $string['status'] != 1)) {

            if (isset($string['errorcode']))
                $error = isset($string['description'])
                    ? $string['errorcode'].' : '.$string['description'] : 'Sending Error';
            return false;
        }

        return true;
    }
}

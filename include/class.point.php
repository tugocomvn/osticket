<?php
/*********************************************************************
    class.note.php

    Simple note interface for affixing notes to users and organizations

    Peter Rotich <peter@osticket.com>
    Jared Hancock <jared@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require_once(INCLUDE_DIR . 'class.orm.php');

class UserPointModel extends VerySimpleModel {
    static $meta = array(
        'table' => USER_POINT_TABLE,
        'pk' => array('id'),
        'ordering' => array('date')
    );
}

class UserPoint extends UserPointModel {
    static $types = array(
        '+' => /* @trans */ 'Tích điểm',
        '-' => /* @trans */ 'Sử dụng điểm',
    );

    function display() {
        return Format::display($this->subject);
    }

    function getStaff() {
        if (!isset($this->_staff) && $this->updater_id) {
            $this->_staff = Staff::lookup($this->updater_id);
        }
        return $this->_staff;
    }

    function getFormattedTime() {
        return Format::db_datetime($this->date);
    }

    function getExtType() {
        return static::$types[static::getSign()];
    }

    function getSign() {
        return $this->change_point > 0 ? '+' : '-';
    }

    function getExtIconClass() {
        switch (static::getSign()) {
        case '+':
            return 'plus';
        case '-':
            return 'minus';
        }
    }

    function getIconTitle() {
        return static::getExtType();
    }

    static function forUser($user) {
        return static::objects()->filter(array('user_uuid' => $user->get('uuid')));
    }
    static function forUserUuid($uuid) {
        return static::objects()->filter(['user_uuid'=> $uuid]);
    }

    static function fromBookingCode($booking_code) {
        return static::objects()->filter(array('booking_code' => $booking_code));
    }

    function save($refetch=false) {
        if (count($this->dirty))
            $this->date = new SQLFunction('NOW');
        return parent::save($refetch);
    }
}

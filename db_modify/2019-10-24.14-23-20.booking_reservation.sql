alter table ost_groups drop column can_approve;
ALTER TABLE `ost_groups` ADD `can_approve_leader` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_approve_visa` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_approve_operator` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
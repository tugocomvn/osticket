CREATE TABLE IF NOT EXISTS `receipt_tracking` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `receipt_id` INT NULL,
  `status` TINYINT NULL DEFAULT 0,
  `add_time` DATETIME NULL,
  `user_agent` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`receipt_id` ASC),
  INDEX `index3` (`status` ASC),
  INDEX `index4` (`add_time` ASC))
ENGINE = InnoDB;
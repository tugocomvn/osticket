<?php
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.flight-ticket.php');

$airlines_list = FlightTicket::getAllAirline();

$params = $qs = [];
$total = 0;
if(isset($_REQUEST['flight_ticket_code']) && trim($_REQUEST['flight_ticket_code'])) {
    $params['flight_ticket_code'] = trim($_REQUEST['flight_ticket_code']);
    $qs += ['flight_ticket_code' => trim($_REQUEST['flight_ticket_code'])];
}

if (isset($_REQUEST['flight_code']) && trim($_REQUEST['flight_code'])) {
    $params['flight_code'] = trim($_REQUEST['flight_code']);
    $qs += ['flight_code' => trim($_REQUEST['flight_code'])];
}

if (isset($_REQUEST['airline'])&& (int)($_REQUEST['airline'])) {
    $params['airline'] = (int)($_REQUEST['airline']);
    $qs += ['airline' => trim($_REQUEST['airline'])];
}

if (isset($_REQUEST['departure_at']) && trim($_REQUEST['departure_at'])) {
    $params['departure_at'] = trim($_REQUEST['departure_at']);
    $qs += ['departure_at' => trim($_REQUEST['departure_at'])];
}

if (isset($_REQUEST['arrival_at']) && trim($_REQUEST['arrival_at'])) {
    $params['arrival_at'] = trim($_REQUEST['arrival_at']);
    $qs += ['arrival_at' => trim($_REQUEST['arrival_at'])];
}

if (isset($_REQUEST['total_quantity']) && trim($_REQUEST['total_quantity'])) {
    $params['total_quantity'] = trim($_REQUEST['total_quantity']);
    $qs += ['total_quantity' => trim($_REQUEST['total_quantity'])];
}

$page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
$offset = ($page - 1) * PAGE_LIMIT;
$results = FlightTicket::getPagination($params, $offset, $total, PAGE_LIMIT);
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('flight-ticket-list.php', $qs ?: null);
$page_inc = "flight-ticket-list.inc.php";

$nav->setActiveTab('flight_ticket');
$ost->addExtraHeader('<meta name="tip-namespace" content="flight_ticket.flight_ticket" />',
    "$('#content').data('tipNamespace', 'flight_ticket.flight_ticket_list');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page_inc);
include(STAFFINC_DIR.'footer.inc.php');
?>
CREATE TABLE `ost_ticket_payment_history` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`ticket_id`  int(11) UNSIGNED NULL ,
`user_id`  int(11) UNSIGNED NULL ,
`staff_id`  int(11) UNSIGNED NULL ,
`pay_amount`  decimal(10,2) UNSIGNED NULL ,
`pay_time`  datetime NULL,
PRIMARY KEY (`id`),
INDEX (`ticket_id`) ,
INDEX (`user_id`) ,
INDEX (`staff_id`) ,
INDEX (`pay_time`)
)
;

ALTER TABLE `ost_ticket_payment_history`
MODIFY COLUMN `pay_amount`  decimal(12,0) UNSIGNED NULL DEFAULT NULL AFTER `staff_id`;

ALTER TABLE `ost_ticket_payment_history`
ADD COLUMN `pay_method`  enum('pay_at_office','banking','home_cash','other') NULL AFTER `pay_time`;

ALTER TABLE `ost_ticket_payment_history`
ADD COLUMN `note` text NULL AFTER `pay_method`;

ALTER TABLE `ost_ticket_payment_history`
ADD COLUMN `thread_id` int(11) after `note`;

ALTER TABLE `ost_ticket_payment_history`
MODIFY COLUMN `pay_method`  enum('pay_at_office','banking','home_cash','other') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'other' AFTER `pay_time`;


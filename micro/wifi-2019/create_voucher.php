<html lang="en">
<head>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Create voucher</title>
</head>
<body>

<div class="container bg-light rounded" style="width: 80%;">
    <?php require_once __DIR__.'/header.php'; ?>
    <div class="form-group">
        <form action="back_create_voucher.php" method="post">
            <h3 class="text-center mt-5 text-center text-info">Tạo mã voucher</h3>
            <div class="row my-3">
                <div class="col-3 pl-5">Nhóm </div>
                <select name="group_id" class="form-control col-9" id="">
                    <?php foreach(Awing::selectDataGroup() as $row): ?>
                        <option value="<?php echo $row['group_id'] ?>"
                            <?php if (isset($_REQUEST['group_id']) && $_REQUEST['group_id'] == $row['group_id'])
                                echo 'selected' ?>
                        ><?php echo $row['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="row my-3">
                <p class="col-3 pl-5 ">Số lượng kí tự</p>
                <input type="number"  class="form-control-plaintext col-9 border border-info rounded"
                       required min="5" max="15" step="1"
                       name="strlen" value=<?php if(isset($_REQUEST['strlen']) && $_REQUEST['strlen']) echo $_REQUEST['strlen']; else echo '5'; ?>>
            </div>
            <div class="row my-3">
                <p class="col-3 pl-5">Số lượng mã</p>
                <input type="number"  class="form-control-plaintext col-9 border border-info rounded"
                       required min="1" max="1000" step="1"
                       name="total" value=<?php if(isset($_REQUEST['total']) && $_REQUEST['total']) echo $_REQUEST['total']; else echo '1'; ?>>
            </div>
            <div class="row my-3">
                <p class="col-3 pl-5">Mã mẫu</p>
                <input type="text" placeholder="Mẫu ####"
                       class="form-control-plaintext col-9 border border-info rounded"
                       name="pattern" value=<?php if(isset($_REQUEST['pattern']) && $_REQUEST['pattern']) echo $_REQUEST['pattern'] ?>>
            </div>
            <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg rounded"
                    name="create" value="create">Tạo mã</button>
            </div>
            <div class="text-center mt-2" style="display:<?php echo $showAdd ?>">
                <button type="submit" class="btn btn-primary btn-lg rounded"
                        name="add" value="add" >Thêm vào database</button>
            </div>
            <div class="alert alert-success alert-dismissible mt-2"
                 style="display:<?php echo $showSuccess ?>">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <p class="text-center">
                    <strong>Thành công!</strong> Dữ liệu đã được thêm vào cơ sở dữ liệu.
                </p>
            </div>
        </form>
    </div>
    <?php if($strWarn != ""): ?>
    <div class="alert alert-danger" role="alert"><?php echo $strWarn ?><div>
    <?php endif;  ?>
    <?php if(isset($result) && !is_null($result) ):?>

        <h4 class="text-primary">Kết quả</h4>
        <?php foreach ($result as $res):?>
            <div class="<?php echo $class_col?> alert alert-info text-center" style="float: left">
                <?php echo $res ?>
            </div>
        <?php endforeach; ?>
    <?php elseif (isset($result)): ?>
        <div class="alert alert-danger" role="alert">Các mã như mẫu trên đã tồn tại. Hãy tăng số lượng ký tự cho phép để có thể tạo được mã mới.<div>
    <?php endif;?>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
<footer></footer>
</html>
<?php 
/** 
 * Created by PhpStorm. 
 * User: minhjoko 
 * Date: 6/1/18 
 * Time: 11:55 AM 
 */ 
session_start(); 
if(isset($_POST['submit'])){ 
    $fname = $_POST['fname']; 
    $email = $_POST['email']; 
    $address = $_POST['address']; 
    if(!empty($email) && !filter_var($email,FILTER_VALIDATE_EMAIL) === false){ 
        $apiKey = '7090ec095d3256042a961b4b799c7895-us16'; 
        $listID = '2140897d9d'; 
 
        $data_center = substr($apiKey,strpos($apiKey,'-')+1); 
        $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists/'. $listID .'/members'; 
 
        $json = json_encode([ 
           'email_address' => $email, 
            'status' => 'subscribed', 
            'merge_fields' => [ 
                'FNAME' => $fname, 
                'ADDRESS' => $address 
            ] 
        ]); 
        RequestUrl::CallCurl($url,$apiKey,$json); 
    } 
} 
?>
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2019 at 04:24 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `tour_market`
--

CREATE TABLE `tour_market` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ENABLE=1, DISABLE=0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_market`
--

INSERT INTO `tour_market` (`id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(938, 'Anh Quốc', 1, NULL, NULL, NULL, NULL),
(939, 'Canada', 1, NULL, NULL, NULL, NULL),
(940, 'Châu Âu', 1, NULL, NULL, NULL, NULL),
(941, 'Hàn Quốc', 1, NULL, NULL, NULL, NULL),
(942, 'Mỹ', 1, NULL, NULL, NULL, NULL),
(943, 'New Zealand', 1, NULL, NULL, NULL, NULL),
(944, 'Nhật Bản', 1, NULL, NULL, NULL, NULL),
(945, 'Thái Lan', 1, NULL, NULL, NULL, NULL),
(946, 'Úc', 1, NULL, NULL, NULL, NULL),
(947, 'Việt Nam', 1, NULL, NULL, NULL, NULL),
(948, 'Đài Loan', 1, NULL, NULL, NULL, NULL),
(949, 'Cambodia', 1, NULL, NULL, NULL, NULL),
(950, 'Singapore', 1, NULL, NULL, NULL, NULL),
(951, 'Trung Quốc', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tour_market`
--
ALTER TABLE `tour_market`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `index3` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tour_market`
--
ALTER TABLE `tour_market`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=952;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


<style>
    iframe{
        width: 100%;
        height: 100%;
    }
</style>
<?php
require_once(__DIR__.'/staff.inc.php');
require_once('../vendor/autoload.php');

function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');



    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }
    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);
            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

function getTypeFile($service, $fileId){
    try {
        $file = $service->files->get($fileId);

        return $file->getMimeType();
    } catch (Exception $e) {
        return false;
    }
}

$nav->setTabActive('dashboard');
$nav->setActiveSubMenu(1);
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.dashboard" />',
    "$('#content').data('tipNamespace', 'dashboard.dashboard');");
include(STAFFINC_DIR.'header.inc.php');
//BODY
$link = "";
$id_forder = "";
$data = DynamicListItem::lookup($_REQUEST['id']);
if ($data) {
    $link = $data->getConfiguration()[PROPERTIES2];
    $id_forder = $link;
}
$pos = strpos($link, '<iframe');
if ($pos === false) {
    $client = getClient();
    $service = new Google_Service_Drive($client);

    if ($id_forder == "") {
        $id_forder = $_REQUEST['id'];
    }
    $query = sprintf(' "%s" in parents and trashed = false', $id_forder);
    $optParams = array(
        'q' => $query,
        'fields' => 'files(id, name)'
    );
    $name_foder = $service->files->get($id_forder)->getName();
    $results = $service->files->listFiles($optParams);
     if (count($results->getFiles()) != 0):
         ?>
     <a href=<?php echo $_SERVER['HTTP_REFERER']  ?>> < Quay lại</a> </br>
     <h3><?php echo $name_foder ?></h3>
     <table class="list" border="0" cellspacing="1" cellpadding="2" style=" width:100%">
     <tbody>
     <?php foreach ($results->getFiles() as $file):?>
         <tr>
            <?php
            $fileId = $file->getId();
            $type = "";
            $type = getTypeFile($service,$fileId);
            if(strpos($type,".document")):
                
                $link = $cfg->getUrl().'scp/read_document.php?id='.$fileId;?>
                <td>
                    <img src="./images/document.png">
                    <a class="no-pjax" href="<?php echo $link ?>" target="_blank"><?php echo $file->getName() ?></a> </br>
                </td>
                <?php endif;?>
            <?php if (strpos($type,".spreadsheet")):
                $link =  $cfg->getUrl().'scp/read_spreadsheet.php?id=' .$fileId;?>
                <td>
                    <img src="./images/sheets.png">
                    <a class="no-pjax" href="<?php echo $link ?>" target="_blank"><?php echo $file->getName() ?></a> </br>
                </td>
            <?php endif;?>

            <?php if (strpos($type,".folder")):

                $link =  $cfg->getUrl().'scp/readDrive.php?id=' .$fileId; ?>
                <td>
                    <img src="./images/foder.png">
                    <a href="<?php echo $link ?>"><?php echo $file->getName()?></a> </br>
                </td>
            <?php endif;?>
         </tr>
     <?php endforeach; ?>
     </tbody>
     </table>
    <?php endif;
    }else{
        echo $link;
    }
include(STAFFINC_DIR.'footer.inc.php');

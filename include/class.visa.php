<?php

namespace Tugo;

require_once(INCLUDE_DIR . 'class.orm.php');
//----------class RequiredPapers-------------
class RequiredPapersModel extends \VerySimpleModel
{
    static $meta = [
        'table' => PAPER_REQUIRE_TABLE,
        'pk' => ['id'],
    ];
}

class RequiredPapers extends RequiredPapersModel
{
    public static function getTotalItem()
    {
        $sql = "SELECT count(id) as total FROM ". self::$meta['table'];
        $result = db_query($sql);
        return db_fetch_array($result)['total'];
    }
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY id DESC";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY id DESC";
        return db_query($sql);

    }
}

//----------class JobStatus-------------
class JobStatusModel extends \VerySimpleModel
{
    static $meta = [
        'table' => STATUS_JOB_TABLE,
        'pk' => ['id'],
    ];
}

class JobStatus extends JobStatusModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name`";
        return db_query($sql);
    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }
}

//----------class StatusMarital-------------
class StatusMaritalModel extends \VerySimpleModel
{
    static $meta = [
        'table' => STATUS_MARITAL_TABLE,
        'pk' => ['id'],
    ];
}

class StatusMarital extends StatusMaritalModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name`";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }

}

//----------class StatusVisa-------------
class StatusVisaModel extends \VerySimpleModel
{
    static $meta = [
        'table' => STATUS_VISA_TABLE,
        'pk' => ['id'],
    ];
}

class StatusVisa extends StatusVisaModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name`";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }
}

class VisaStatusList extends \GeneralObject {
    const WAITING_DOCUMENT = 4; // chờ khách nợp hồ sơ
    const DOCUMENT_PROCESSING = 5; // phòng visa đang nhận và xử lý hồ sơ
    const WAITING_APPLY = 8; // đủ hồ sơ, chờ nộp
    const APPLIED_TO_EMBASSY = 6; // đã nộp LSQ
    const EMBASSY_PROCESSING = 7; // chờ LSQ duyệt
    const VISA_APPROVED = 1; // đậu visa
    const VISA_APPROVED_NOT_GO = 3; // đậu visa mà đéo đi tour
    const VISA_REJECTED = 2; // bị từ chối
}

//----------class PassPortLocation-------------
class PassportLocationModel extends \VerySimpleModel
{
    static $meta = [
        'table' => PASSPORT_LOCATION_TABLE,
        'pk' => ['id'],
    ];
}

class PassportLocation extends PassportLocationModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name` ";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }

}

//----------class VisaType-------------
class VisaTypeModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_TYPE_TABLE,
        'pk' => ['id'],
    ];
}

class VisaType extends VisaTypeModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name`";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }
}

//---------------class Visa appointment-------------------
class VisaAppointmentModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_APPOINTMENT,
        'pk' => ['id'],
    ];
}

class VisaAppointment extends VisaAppointmentModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY `name`";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY `name`";
        return db_query($sql);

    }
}

//----------class VisaHistory-------------
class VisaHistoryModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_HISTORY_TABLE,
        'pk' => ['id'],
    ];
}

class VisaHistory extends VisaHistoryModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table'];
        return db_query($sql);

    }
    public  static  function deleteItem($id)
    {
        $new = self::lookup($id);
        if($new) $new ->delete();
    }
    public static function getHistoryPax($id_profile){
        $sql = "SELECT * FROM ". self::$meta['table']." WHERE tour_pax_visa_profile_id = ".$id_profile;
        return db_query($sql);
    }

}

//----------class VisaInformation-------------
class VisaInformationModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_INFORMATION_TABLE,
        'pk' => ['id'],
    ];
}

class VisaInformation extends VisaInformationModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table'];
        return db_query($sql);

    }
    public  static  function deleteItem($id)
    {
        $new = self::lookup($id);
        if($new) $new ->delete();
    }
    public static function getInfoPax($id_profile){
        $sql = "SELECT * FROM ". self::$meta['table']." WHERE tour_pax_visa_profile_id = ".$id_profile;
        return db_query($sql);
    }
}

//----------class VisaTourPaxProfile-------------
class VisaTourPaxProfileModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_TOUR_PAX_PROFILE,
        'pk' => ['id'],
    ];
}

class VisaTourPaxProfile extends VisaTourPaxProfileModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table'];
        return db_query($sql);

    }
    public  static  function deleteItem($id)
    {
        $new = self::lookup($id);
        if($new) $new ->delete();
    }

    public static function getAllVisaImage()
    {
        $sql = 'select p.full_name as customer_name ,t.name as tour_name, fp.staff_id, fp.visa_image_uploaded_at, fp.visa_image_uploaded_by, fp.visa_image_filename
                from tour_pax_visa_profile fp
                    join ost_pax_tour pt on pt.tour_pax_visa_profile_id = fp.id
                    join ost_pax p on pt.pax_id = p.id  
                    join ost_tour t on pt.tour_id = t.id
                where fp.visa_image_filename is not null and pt.tour_pax_visa_profile_id is not null
                order by fp.visa_image_uploaded_at desc ';
        return db_query($sql);
    }
}

//--------------class Visa profile appointment----------------
class VisaProfileAppointmentModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_PROFILE_APPOINTMENT,
        'pk' => ['id'],
    ];
}

class VisaProfileAppointment extends VisaProfileAppointmentModel
{
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table'];
        return db_query($sql);

    }
    public  static  function deleteItem($id)
    {
        $new = self::lookup($id);
        if($new) $new ->delete();
    }
    public static function getAppointmentPax($id_profile){
        if(!$id_profile) return false;
        $sql = "SELECT * FROM ". self::$meta['table']." WHERE tour_pax_visa_profile_id = ".$id_profile." ORDER BY id DESC";
        return db_query($sql);
    }
}

class StatusFinanceModel extends \VerySimpleModel
{
    static $meta = [
        'table' => STATUS_FINANCE_TABLE,
        'pk' => ['id'],
    ];
}

class StatusFinance extends StatusFinanceModel
{
    public static function getTotalItem()
    {
        $sql = "SELECT count(id) as total FROM ". self::$meta['table'];
        $result = db_query($sql);
        return db_fetch_array($result)['total'];
    }
    public static function getAllItem()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " ORDER BY id DESC";
        return db_query($sql);

    }
    public static function getItemExist()
    {
        $sql = "SELECT * FROM ". self::$meta['table']. " WHERE status = 1 ORDER BY id DESC";
        return db_query($sql);

    }
}

class TourPaxVisaProfileHistoryModel extends \VerySimpleModel
{
    static $meta = [
        'table' => TOUR_PAX_VISA_PROFILE_HISTORY_TABLE,
        'pk' => ['id'],
    ];
}

class TourPaxVisaProfileHistory extends TourPaxVisaProfileHistoryModel
{

}

class VisaHistoryActionLogModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_HISTORY_ACTION_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class VisaHistoryAction extends VisaHistoryActionLogModel
{

}

class VisaProfileAppointmentActionLogModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_PROFILE_APPOINTMENT_ACTION_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class VisaProfileAppointmentAction extends VisaProfileAppointmentActionLogModel
{

}

class VisaInformationActionLogModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_INFORMATION_ACTION_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class VisaInformationAction extends VisaInformationActionLogModel
{

}

class VisaFollowActionModel extends \VerySimpleModel
{
    static $meta = [
        'table' => VISA_FOLLOW_ACTION_TABLE,
        'pk' => ['id'],
    ];
}
class VisaFollowAction extends VisaFollowActionModel
{
    //status visa
    const VISA_STATUS_WAITING_DOCUMENT = 12; // chờ khách nợp hồ sơ
    const VISA_STATUS_DOCUMENT_PROCESSING = 13; // phòng visa đang nhận và xử lý hồ sơ
    const VISA_STATUS_WAITING_APPLY = 14; // đủ hồ sơ, chờ nộp
    const VISA_STATUS_APPLIED_TO_EMBASSY = 15; // đã nộp LSQ
    const VISA_STATUS_EMBASSY_PROCESSING = 16; // chờ LSQ duyệt
    const VISA_STATUS_APPROVED = 17; // đậu visa
    const VISA_STATUS_APPROVED_NOT_GO = 18; // đậu visa mà không đi tour
    const VISA_STATUS_REJECTED = 19; // bị từ chối

    //table tour_pax_visa_profile
    const VISA_AUTO_CREATE_PROFILE = 21;
    const VISA_CREATED_PROFILE = 22;
    const VISA_ASSIGNED_PROFILE = 1;
    const VISA_RECEIVED_PROFILE = 20;
    const VISA_EDITED_PROFILE = 2;
    const VISA_FINISHED_PROFILE = 11;

    //table visa history
    const VISA_CREATED_VISA_HISTORY = 3;
    const VISA_EDITED_VISA_HISTORY = 4;
    const VISA_DELETED_VISA_HISTORY = 5;

    //table visa information
    const VISA_CREATED_VISA_INFORMATION = 6;
    const VISA_EDITED_VISA_INFORMATION = 7;
    const VISA_DELETED_VISA_INFORMATION = 8;

    //table appointment
    const VISA_CREATED_VISA_APPOINTMENT = 9;

    //table visa sms
    const VISA_SENT_SMS = 10;

    //upload image
    const VISA_UPLOAD_VISA_IMAGE = 23;

    static public function addLogAction($profile_id, $content, $action_id, $time = null, $staff = null){
        global $thisstaff;

        $new = VisaFollowAction::create([
            'profile_id' => (int)$profile_id,
            'action_id'  => (int)$action_id,
            'time'       => $time ? : date('Y-m-d H:i:s'),
            'subject'    => $staff && $staff->getUserName() ? $staff->getUserName() : $thisstaff->getUserName(),
            'content'    => json_encode($content),
            'staff_id'   => $staff && $staff->getId() ? $staff->getId() : $thisstaff->getId()
        ]);
        $new->save();
    }

    static public function countProfileVisa($time){
        $sql = 'select
                      date_format(`time`, "%Y") as `year`
                    , date_format(`time`, "%Y%m") as `month_of_year`
                    , date_format(`time`, "%m") as `month_of_the_year`
                    , date_format(`time`, "%Y%v") as `week_of_year`
                    , date_format(`time`, "%v") as `week_of_the_year`
                    , date_format(`time`, "%Y%m%d") as `date`
                    , date_format(`time`, "%d") as `day_of_month`
                    , dayofweek(`time`) as `day_of_week`
                    , date_format(`time`, "%W") as `day_of_week_name`
                    , date_format(`time`, "%Y%m%d%H") as `hour_of_day`
                    , date_format(`time`, "%H")as `hour`                    
                    , date_format(`time`, "%Y%m%d%H%i") as `minute_of_day`
                    , date_format(`time`, "%i") as `minute`
                    , date_format(`time`, "%Y%m%d%H%i%s") as `second_of_day`
                    , date_format(`time`, "%s") as `second`
                    , staff_id as `staff_id`
                    , `subject` as `staff_username`
                    , action_id as `action_id`
                    , count(id) as `amount_profile`
                from
                    visa_follow_action 	
                where
                    `time` = '.db_input($time). '
                    and `time` is not null and `time` != \'\'
                group by
                    date_format(`time`, "%Y%m%d%H%i%s")
                    , staff_id
                    , action_id
                    ';
        return db_query($sql);
    }
}

class AnalysisVisaModel extends \VerySimpleModel{
    static $meta = [
        'table' => ANALYSIS_VISA_TABLE,
        'pk' => array('id')
    ];
}
class AnlysisVisa extends AnalysisVisaModel
{
    static public function insert($data){
        $sql = 'INSERT INTO '.static::$meta['table'];
        $fields = [];
        $update_value = '';

        foreach ($data as $key=>$value){
            $fields[] = "`$key` = ".db_input($value);
            if($key === 'amount_profile')
                $update_value = 'amount_profile = '.$value;
        }

        $sql .= ' SET '.implode(', ', $fields);
        $sql .= ' ON DUPLICATE KEY UPDATE '.$update_value;

        if (!db_query($sql))
            throw new Exception(db_error());
        return 1;
    }
}


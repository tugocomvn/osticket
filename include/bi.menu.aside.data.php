<?php
$bi_menu_aside_data = [
    'overview' => [
        'text' => 'Overview',
        'icon-class' => 'fas fa-chart-pie',
        'href' => 'bi.overview.php',
        'children' => [],
        'available' => false,
    ],
    'tickets' => [
        'text' => 'Tickets',
        'icon-class' => 'fas fa-ticket-alt',
        'href' => 'bi.tickets.php',
        'children' => [
            'overview' => [
                'text' => 'Overview',
                'icon-class' => 'fas fa-circle',
                'href' => 'bi.tickets.overview.php',
            ],
            'sources' => [
                'text' => 'Sources',
                'icon-class' => 'fas fa-random',
                'href' => 'bi.tickets.sources.php',
            ],
            'tags' => [
                'text' => 'Tags',
                'icon-class' => 'fas fa-tag',
                'href' => 'bi.tickets.tags.php',
            ],
            'sources_tags' => [
                'text' => 'Sources/Tags',
                'icon-class' => 'fas fa-tags',
                'href' => 'bi.tickets.sources_tags.php',
            ],
            'returning' => [
                'text' => 'Returning',
                'icon-class' => 'fas fa-retweet',
                'href' => 'bi.tickets.returning.php',
            ],
        ]
    ],
    'bookings' => [
        'text' => 'Bookings',
        'icon-class' => 'fas fa-calendar-check',
        'href' => 'bi.bookings.php',
        'children' => [
            'overview' => [
                'text' => 'Overview',
                'icon-class' => 'fas fa-circle',
                'href' => 'bi.bookings.overview.php',
            ],
            'destination' => [
                'text' => 'Destination',
                'icon-class' => 'fas fa-sign-in-alt',
                'href' => 'bi.bookings.destination.php',
            ],
            'tickets_bookings' => [
                'text' => 'Tickets/Bookings',
                'icon-class' => 'fas fa-clipboard-list',
                'href' => 'bi.bookings.tickets_bookings.php',
            ],
            'returning' => [
                'text' => 'Returning',
                'icon-class' => 'fas fa-retweet',
                'href' => 'bi.bookings.returning.php',
            ],
        ]
    ],
    'call_sms' => [
        'text' => 'Call &amp; SMS',
        'icon-class' => 'fas fa-phone-square-alt',
        'href' => 'bi.call_sms.php',
        'available' => false,
        'children' => [
            'call' => [
                'text' => 'Call',
                'icon-class' => 'fas fa-phone-volume',
                'href' => 'bi.call_sms.call.php',
            ],
            'sms' => [
                'text' => 'SMS',
                'icon-class' => 'fas fa-sms',
                'href' => 'bi.call_sms.sms.php',
            ],
        ]
    ],
    'customers' => [
        'text' => 'Customers',
        'icon-class' => 'fas fa-users',
        'href' => 'bi.customers.php',
        'available' => false,
        'children' => [
            'overview' => [
                'text' => 'Overview',
                'icon-class' => 'fas fa-circle',
                'href' => 'bi.customers.overview.php',
            ],
            'demographics' => [
                'text' => 'Demographics',
                'icon-class' => 'fas fa-phone-volume',
                'href' => 'bi.customers.demographics.php',
            ],
            'flow' => [
                'text' => 'Flow',
                'icon-class' => 'fas fa-map-marked-alt',
                'href' => 'bi.customers.flow.php',
            ],
            'returning' => [
                'text' => 'Returning',
                'icon-class' => 'fas fa-retweet',
                'href' => 'bi.customers.returning.php',
            ],
            'phone_number' => [
                'text' => 'Phone Number',
                'icon-class' => 'fas fa-phone-square-alt',
                'href' => 'bi.customers.phone_number.php',
            ],
            'email' => [
                'text' => 'Email',
                'icon-class' => 'fas fa-envelope',
                'href' => 'bi.customers.email.php',
            ],
            'notification' => [
                'text' => 'Notification',
                'icon-class' => 'fas fa-bell',
                'href' => 'bi.customers.notification.php',
            ],
        ]
    ],
    'finance' => [
        'text' => 'Finance',
        'icon-class' => 'fas fa-money-bill',
        'href' => 'bi.finance.php',
        'available' => false,
        'children' => [
            'balance' => [
                'text' => 'Balance',
                'icon-class' => 'fas fa-balance-scale-left',
                'href' => 'bi.finance.balance.php',
            ],
            'expected_settlement' => [
                'text' => 'Expected Settlement',
                'icon-class' => 'fas fa-money-check',
                'href' => 'bi.finance.expected_settlement.php',
            ],
            'actual_settlement' => [
                'text' => 'Actual Settlement',
                'icon-class' => 'fas fa-money-check-alt',
                'href' => 'bi.finance.actual_settlement.php',
            ],
            'forecast' => [
                'text' => 'Forecast',
                'icon-class' => 'fas fa-chart-line',
                'href' => 'bi.finance.forecast.php',
            ],
        ]
    ],
    'sales_cs' => [
        'text' => 'Sales &amp; CS',
        'icon-class' => 'fas fa-user-friends',
        'href' => '',
        'available' => false,
        'children' => [
            'revenues' => [
                'text' => 'Revenues',
                'icon-class' => 'fas fa-donate',
                'href' => 'bi.sales_cs.revenues.php',
            ],
            'commission' => [
                'text' => 'Commission',
                'icon-class' => 'fas fa-hand-holding-usd',
                'href' => 'bi.sales_cs.commission.php',
            ],
            'activities' => [
                'text' => 'Activities',
                'icon-class' => 'fas fa-cash-register',
                'href' => 'bi.sales_cs.activities.php',
            ],
            'tickets' => [
                'text' => 'Tickets',
                'icon-class' => 'fas fa-ticket-alt',
                'href' => 'bi.sales_cs.tickets.php',
            ],
            'bookings' => [
                'text' => 'Bookings',
                'icon-class' => 'fas fa-calendar-check',
                'href' => 'bi.sales_cs.bookings.php',
            ],
        ]
    ],
    'reservation' => [
        'text' => 'Reservation',
        'icon-class' => 'fas fa-bookmark',
        'href' => '',
        'children' => [],
        'available' => false,
    ],
    'visa' => [
        'text' => 'Visa',
        'icon-class' => 'fas fa-passport',
        'href' => '',
        'children' => [],
        'available' => false,
    ],
    'operator' => [
        'text' => 'Operator',
        'icon-class' => 'fas fa-hotel',
        'href' => '',
        'children' => [],
        'available' => false,
    ],
    'air_ticket' => [
        'text' => 'Air Ticket',
        'icon-class' => 'fas fa-plane',
        'href' => '',
        'children' => [],
        'available' => false,
    ],
];

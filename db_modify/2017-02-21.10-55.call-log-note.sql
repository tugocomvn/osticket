ALTER TABLE osticket_dev.ost_call_log ADD note TINYINT(2) NULL;
CREATE INDEX ost_call_log_note_index ON osticket_dev.ost_call_log (note);

ALTER TABLE osticket_dev.ost_call_log ADD ticket_id INT(11) NULL;
CREATE INDEX ost_call_log_ticket_id_index ON osticket_dev.ost_call_log (ticket_id);
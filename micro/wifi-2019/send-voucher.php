<?php
include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/config.php';
include_once __DIR__.'/functions.php';

date_default_timezone_set('Asia/Ho_Chi_Minh');

try {
    echo date('Y-m-d H:i:s').": Send Voucher\r\n";
    Awing::getDataToSendVoucher();
    echo date('Y-m-d H:i:s').": DONE Send Voucher\r\n";
} catch (Exception $ex) {
    echo $ex->getMessage();
}

<?php

include_once INCLUDE_DIR.'class.cron.php';

class CronApiController extends ApiController {

    function execute() {

        if(!($key=$this->requireApiKey()) || !$key->canExecuteCron())
            return $this->exerr(401, __('API key not authorized'));

        $this->run();
    }

    /* private */
    function run() {
        global $ost;

        Cron::run();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_automation() {
        global $ost;

        Cron::runAutomation();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_automation_daily() {
        global $ost;

        Cron::runAutomationDaily();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_vision() {
        global $ost;

        Cron::runVision();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function runAnalytics() {
        global $ost;
        echo date('Y-m-d H:i:s')." - Running Analytics cron...\r\n";

        Cron::runBookingExpenseForecast();

        echo date('Y-m-d H:i:s')." - DONE Analytics cron...\r\n";

        $ost->logDebug(__('Cron Job'),__('Cron job Analytics executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_no_action_sms() {
        global $ost;

        Cron::runNoActionSMS();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_passport_expiration_reminder() {
        global $ost;

        Cron::runPassportExpirationReminder();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_unassign() {
        global $ost;

        Cron::runUnassign();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_tu_thien() {
        global $ost;

        Cron::runTuThien();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function runBookingPhonenumber() {
        global $ost;

        Cron::runBookingPhonenumber();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function runPhone11() {
        global $ost;

        Cron::runPhone11();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_ematic_mc() {
        global $ost;

        Cron::runEmaticMC();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_tag() {
        global $ost;

        Cron::runTag();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_fb_offline_conversion() {
        global $ost;

        Cron::runFBOfflineConversion();

        $ost->logDebug(__('Cron Job'),__('Cron job run_fb_offline_conversion executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_birthday() {
        global $ost;

        if (FIX_AFTER_SALES_SMS) {
            Cron::run_pax_return_sms();
            return;
        }
        Cron::run_birthday();
        Cron::run_pax_return_sms();
        Cron::run_clean_attachment();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_migrate_files() {
        global $ost;

        Cron::runMigrateFiles();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_booking_ticket_id() {
        global $ost;

        Cron::run_booking_ticket_id();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_cron_every_minute() {
        global $ost;

        Cron::run_cron_every_minute();

        $ost->logDebug(__('Cron Job'),__('Cron job run_cron_every_minute executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_indexing_phone() {
        global $ost;

        Cron::call_indexing_phone();
        $this->response(200,'Completed');
    }

    /* private */
    function run_export_payment() {
        global $ost;

        Cron::run_export_payment();

        $ost->logDebug(__('Cron Job'),__('Cron export payment').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_update_agent_list() {
        global $ost;

        Cron::update_agent_list();

        $ost->logDebug(__('Cron Job'),__('Update Agent List').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_call_google_sheet_calllog() {
        global $ost;

        Cron::run_call_google_sheet_calllog();

        $ost->logDebug(__('Cron Job'),__('Cron job calendar executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_fetch() {
        global $ost;

        Cron::run_fetch();

        $ost->logDebug(__('Cron Job'),__('Cron job fetch email executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_booking_type_id() {
        global $ost;

        Cron::run_booking_type_id();

        $this->response(200,'Completed');
    }

    /* private */
    function run_default() {
        global $ost;

        Cron::run_default();

        $ost->logDebug(__('Cron Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_call_log() {
        global $ost;

        Cron::run_call_log();

        $ost->logDebug(__('Cron Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_call_log_no_answer() {
        global $ost;

        Cron::run_call_log_no_answer();

        $ost->logDebug(__('Cron Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_callin_alert() {
        global $ost;

        Cron::run_callin_alert();

        $ost->logDebug(__('Cron Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    /* private */
    function run_overdue() {
        global $ost;

        Cron::run_overdue();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    /* private */
    function run_ticket_return() {
        global $ost;

        Cron::run_ticket_return();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    function runStatistic() {
        global $ost;

        Cron::runStatistic();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runLoyalty(){
        global $ost;

        Cron::runLoyaltyMembership();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runSendEmailOverReservation(){
        global $ost;

        Cron::runSendEmailOverReservation();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountCall(){
        global  $ost;

        Cron::runCountCall();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountSms(){
        global  $ost;

        Cron::runCountSms();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountAge(){
        global  $ost;

        Cron::runCountAge();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountPax(){
        global  $ost;

        Cron::runCountPax();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountBooking(){
        global  $ost;

        Cron::runCountBooking();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runCountTicket(){
        global  $ost;

        Cron::runCountTicket();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runMigrateTour(){
        global $ost;

        Cron::runMigrateTour();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }

    function run_update_hash_url_receipt(){
        global $ost;

        Cron::runUpdateHashUrlReceipt();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
    function runSendEmailBookingLackDocument(){
        global  $ost;

        Cron::runSendEmailBookingLackDocument();

        $ost->logDebug(__('Cron Overdue tickets Job'),__('Cron job default executed').' ['.$_SERVER['REMOTE_ADDR'].']');
        $this->response(200,'Completed');
    }
}

class LocalCronApiController extends CronApiController {

    function response($code, $resp) {

        if($code == 200) //Success - exit silently.
            exit(0);

        //On error echo the response (error)
        echo $resp;
        exit(1);
    }

    function call() {
        $cron = new LocalCronApiController();
        $cron->run();
    }

    function callAutomation() {
        $cron = new LocalCronApiController();
        $cron->run_automation();
    }

    function callAutomationDaily() {
        $cron = new LocalCronApiController();
        $cron->run_automation_daily();
    }

    function callVision() {
        $cron = new LocalCronApiController();
        $cron->run_vision();
    }

    function callTicketReturn() {
        $cron = new LocalCronApiController();
        $cron->run_ticket_return();
    }

    function callAnalytics() {
        $cron = new LocalCronApiController();
        $cron->runAnalytics();
    }

    function callNoActionSMS() {
        $cron = new LocalCronApiController();
        $cron->run_no_action_sms();
    }

    function callPassportExpirationReminder() {
        $cron = new LocalCronApiController();
        $cron->run_passport_expiration_reminder();
    }

    function callUnassign() {
        $cron = new LocalCronApiController();
        $cron->run_unassign();
    }

    function callTuThien() {
        $cron = new LocalCronApiController();
        $cron->run_tu_thien();
    }

    function callBookingPhonenumber() {
        $cron = new LocalCronApiController();
        $cron->runBookingPhonenumber();
    }
    function callPhone11() {
        $cron = new LocalCronApiController();
        $cron->runPhone11();
    }

    function callEmaticMC() {
        $cron = new LocalCronApiController();
        $cron->run_ematic_mc();
    }

    function callTag() {
        $cron = new LocalCronApiController();
        $cron->run_tag();
    }

    function callFBOfflineConversion() {
        $cron = new LocalCronApiController();
        $cron->run_fb_offline_conversion();
    }

    function callBirthday() {
        $cron = new LocalCronApiController();
        $cron->run_birthday();
    }

    function call_migrate_files() {
        $cron = new LocalCronApiController();
        $cron->run_migrate_files();
    }

    function call_booking_ticket_id() {
        $cron = new LocalCronApiController();
        $cron->run_booking_ticket_id();
    }

    function call_cron_every_minute() {
        $cron = new LocalCronApiController();
        $cron->run_cron_every_minute();
    }

    function call_indexing_phone() {
        $cron = new LocalCronApiController();
        $cron->run_indexing_phone();
    }

    function call_export_payment() {
        $cron = new LocalCronApiController();
        $cron->run_export_payment();
    }

    function update_agent_list() {
        $cron = new LocalCronApiController();
        $cron->run_update_agent_list();
    }

    function call_google_sheet_calllog() {
        $cron = new LocalCronApiController();
        $cron->run_call_google_sheet_calllog();
    }

    function call_fetch() {
        $cron = new LocalCronApiController();
        $cron->run_fetch();
    }

    function call_booking_type_id() {
        $cron = new LocalCronApiController();
        $cron->run_booking_type_id();
    }

    function call_call_log() {
        $cron = new LocalCronApiController();
        $cron->run_call_log();
    }

    function call_log_no_answer() {
        $cron = new LocalCronApiController();
        $cron->run_call_log_no_answer();
    }

    function callin_alert() {
        $cron = new LocalCronApiController();
        $cron->run_callin_alert();
    }

    function call_default() {
        $cron = new LocalCronApiController();
        $cron->run_default();
    }

    function call_overdue() {
        $cron = new LocalCronApiController();
        $cron->run_overdue();
    }
    function callStatistic() {
        $cron = new LocalCronApiController();
        $cron->runStatistic();
    }
    function  callLoyalty(){
        $cron = new LocalCronApiController();
        $cron->runLoyalty();
    }
    function  callCountCall(){
        $cron = new LocalCronApiController();
        $cron->runCountCall();
    }
    function  callCountSms(){
        $cron = new LocalCronApiController();
        $cron->runCountSms();
    }
    function  callCountAge(){
        $cron = new LocalCronApiController();
        $cron->runCountAge();
    }
    function  callCountPax(){
        $cron = new LocalCronApiController();
        $cron->runCountPax();
    }
    function  callCountBooking(){
        $cron = new LocalCronApiController();
        $cron->runCountBooking();
    }
    function  callCountTicket(){
            $cron = new LocalCronApiController();
            $cron->runCountTicket();
        }
    function call_migrate_tour(){
        $cron = new LocalCronApiController();
        $cron->runMigrateTour();
    }
    function callUpdateHashReceipt(){
        $cron = new LocalCronApiController();
        $cron->run_update_hash_url_receipt();
    }
    function callSendEmailBookingLackDocument(){
        $cron = new LocalCronApiController();
        $cron->runSendEmailBookingLackDocument();
    }
}
?>

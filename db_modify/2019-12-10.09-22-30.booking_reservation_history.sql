/*branch: booking-form*/

alter table booking_reservation_history
    add visa_review_at datetime null;

alter table booking_reservation_history
    add visa_review_by int null;
-- auto-generated definition
create table staff_signin_token
(
    id           int auto_increment
        primary key,
    user_name    varchar(32)  not null,
    staff_id     int unsigned null,
    signin_token varchar(64)  null,
    create_at    datetime     null,
    expired_at   datetime     null
);
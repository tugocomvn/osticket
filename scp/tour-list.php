<?php
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.flight-ticket.php');
require_once(INCLUDE_DIR.'class.visa-tour.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');

$leaders_list = TourNew::getAllLeaders();
$bookings_type_list = TourNew::getAllBookingType();
$transits_airport_list = TourNew::getAllTransitAirport();
$departure_flight_list = TourNew::getAllDepartureFlight();
$list_colors = ListOfColors::caseTitleName();
$airlines_list = TourNew::getAllAirline();
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);
$booking_type_list = TourDestination::loadList($group_des);

function __validation($data)
{
    $tour = null;
    if (isset($data['tour_id']) && (int)trim($data['tour_id'])) {
        $data['tour_id'] = (int)trim($data['tour_id']);
        $tour = TourNew::lookup(['id' => $data['tour_id']]);
        if (!$tour) throw new \Exception('Invalid Tour. Wrong ID');
    }

    if(!isset($data['booking_type']) || empty($data['booking_type']))
        throw new Exception('Điểm đến không được để trống .');

    if(!isset($data['gateway_present_date']) || empty($data['gateway_present_date']))
        throw new Exception('Ngày tập trung không được để trống .');

    if(!isset($data['gateway_present_time']) || empty($data['gateway_present_time']))
        throw new Exception('Giờ tập trung không được để trống .');

    if(!isset($data['departure_date']) || empty($data['departure_date']))
        throw new Exception('Ngày khởi hành không được để trống .');

    if(!isset($data['return_date']) || empty($data['return_date']))
        throw new Exception('Ngày về không được để trống .');

    if(!isset($data['pax_quantity']) || empty($data['pax_quantity']))
        throw new Exception('Số khách tối đa không được để trống .');

    if(!isset($data['tour_name']) || empty($data['tour_name']))
        throw new Exception(('Tour name không được để trống .'));
    return $tour;
}
function edit_tour_net_price($data) {
    $tour_net_price = TourNetPrice::lookup((int)$data['id']);
    if(!empty($tour_net_price)){
        if($tour_net_price->quantity != $data['quantity'] || (int)$tour_net_price->retail_price != (int)$data['retail_price']){
            $_data = [
                'quantity' => (int)$data['quantity'],
                'retail_price' => (int)$data['retail_price']
            ];
            $tour_net_price->setAll($_data);
            return $tour_net_price->save(true);
        }
    }
}

function insert_flight_table($value, $tour_id) {
    global $thisstaff;
    $tour = TourNew::lookup($tour_id);
    $departure_at = (!empty($value['departure_at']))?date_create_from_format('d/m/Y', $value['departure_at'])->format('Y-m-d'):null;
    $arrival_at = (!empty($value['arrival_at']))?date_create_from_format('d/m/Y', $value['arrival_at'])->format('Y-m-d'):null;
    //Flight
    $flight = FlightOP::lookup($value['flight_id']);
    $_data_flight = [
        'airline_id' => $tour->airline_id,
        'airport_code_from' => $value['airport_code_from'],
        'airport_code_to' => $value['airport_code_to'],
        'flight_code' => $value['flight_code'],
        'departure_at' => $departure_at." ".$value['departure_time'],
        'arrival_at' => $arrival_at." ".$value['arrival_time']
    ]; 
    if(!empty($flight)) {
        $_data_flight['updated_at'] = date('Y-m-d H:i:s');
        $_data_flight['updated_by'] = $thisstaff->getId();
        $flight->setAll($_data_flight);
        $flight_id = $flight->save(true);
    } else {
        $_data_flight['created_at'] = date('Y-m-d H:i:s');
        $_data_flight['created_by'] = $thisstaff->getId();
        $flight = FlightOP::create($_data_flight);
        $flight_id = $flight->save(true);
    }
    $log = FlightLogOP::create(
        [
            'flight_id' => $flight_id,
            'content' => json_encode($flight->ht),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId()
        ]
    );
    $log->save(true);
    //Flight Ticket
    $flight_ticket = FlightTicketOP::lookup($value['flight_ticket_id']);
    $_data_flight_ticket = [
        'flight_ticket_code' => $value['flight_ticket_code'],
    ];
    if(!empty($flight_ticket)) {
        //$_data_flight_ticket['updated_at'] = date('Y-m-d H:i:s');
        //$_data_flight_ticket['updated_by'] = $thisstaff->getId();
        $flight_ticket->setAll($_data_flight_ticket);
        $flight_ticket_id = $flight_ticket->save(true);
    } else {
        //$_data_flight_ticket['created_at'] = date('Y-m-d H:i:s');
        //$_data_flight_ticket['created_by'] = $thisstaff->getId();
        $flight_ticket = FlightTicketOP::create($_data_flight_ticket);
        $flight_ticket_id = $flight_ticket->save(true);
    }
    $log = FlightTicketLogTR::create(
        [
            'flight_ticket_id' => $flight_ticket_id,
            'content' => json_encode($flight_ticket->ht),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId()
        ]
    );
    //Flight Ticket Flight
    $id_flight_ticket_flight = [
        'flight_ticket_id' => $flight_ticket_id,
        'flight_id' => $flight_id
    ];
    $flight_ticket_flight = FlightTicketFlightOP::lookup(['flight_ticket_id' => $flight_ticket_id]);
    $_flight_ticket_flight = FlightTicketFlightOP::lookup(['flight_id' => $flight_id]);
    if(empty($flight_ticket_flight) && empty($_flight_ticket_flight)){
        $flight_ticket_flight = FlightTicketFlightOP::create($id_flight_ticket_flight);
        $flight_ticket_flight->save(true);
    }
    //FLight Ticket Tour
    $data_flight_ticket_tour = [
        'flight_ticket_id' => $flight_ticket_id,
        'tour_id' => $tour_id
    ];
    $flight_ticket_tour = FlightTicketTourOP::lookup(['flight_ticket_id' => $flight_ticket_id]);
    if(empty($flight_ticket_tour)){
        $flight_ticket_tour = FlightTicketTourOP::create($data_flight_ticket_tour);
        $flight_ticket_tour->save('true');
    }
}

function __update_tourNew($data)
{
    global $thisstaff;
    $tour = __validation($data);
    $tour_id = (int)$_POST['tour_id'];
    if (!isset($_POST['group-loyalty']) || !$_POST['group-loyalty'] || !is_array($_POST['group-loyalty'])) {
        $_POST['group-loyalty'] = [];
    }else{
        $all_values_flight_ticket = $_POST['group-loyalty'];
        $tour_list_op = FlightTicketTourOP::getFlightTicketFLight($tour_id);
        $flight_ticket_id_list_current = [];
        while (($row = db_fetch_array($tour_list_op))) {
            $flight_ticket_id_list_current[] = $row['flight_ticket_id'];
        } 
        $all_values_flight_ticket = array_filter($all_values_flight_ticket, 'implode');;
        if(is_array($all_values_flight_ticket)){
            foreach ($all_values_flight_ticket as $value) {
                if(count($flight_ticket_id_list_current) == 0 && empty($flight_ticket_id_list_current)){
                    insert_flight_table($value, $tour_id);
                } else {
                    insert_flight_table($value, $tour_id);
                }
            } 
        }
    }
    if (!isset($_POST['group-loyalty-net-price']) || !$_POST['group-loyalty-net-price'] || !is_array($_POST['group-loyalty-net-price'])) {
        $_POST['group-loyalty-net-price'] = [];
    }else{
        $all_values_net_price = $_POST['group-loyalty-net-price'];
        $tour_id = (int)$_POST['tour_id'];
        $tour_net_price = TourNetPrice::lookup(['tour_id' => $tour_id]);
        if(empty($tour_net_price)){
            $new_val_net_price = $all_values_net_price[0];
            if(!empty($new_val_net_price)){
                $max_quantity = max($new_val_net_price['quantity'], $new_val_net_price['_quantity'], $new_val_net_price['__quantity']);
                if(!empty($max_quantity)){
                    $data_tour_net_price = array(array($new_val_net_price['quantity'] , $new_val_net_price['retail_price']), 
                    array($new_val_net_price['_quantity'], $new_val_net_price['_retail_price']), 
                    array($new_val_net_price['__quantity'], $new_val_net_price['__retail_price']), array((int)$max_quantity+1, $new_val_net_price['max_retail_price']));
                    $tour_net_price = TourNetPrice::save_tour_net_price($data_tour_net_price, $tour_id);
                }
            }
        }else{
            $tour_net_price = TourNetPrice::get_tour_net_price($tour_id);
            $tour_net_id_list_current = [];
            while (($row = db_fetch_array($tour_net_price))) {
                $tour_net_id_list_current[] = $row['id'];
            } 
            foreach ($all_values_net_price as $value) {
                if(count($all_values_net_price) != 0 && !empty($all_values_net_price)){
                    edit_tour_net_price($value, $tour_id);
                }
            }
        }
    }       
    $gateway_present_time = $data['gateway_present_date']." ".$data['gateway_present_time'];
    $_data = [
        'tour_leader_id'       => (int)$data['tour_leader'],
        'name'                 => $data['tour_name'],
        'stimulus_tour'        => $data['stimulus_tour'],
        'destination'          => (int)$data['booking_type'],
        'transit_airport_id'   => (int)$data['transit_airport'],
        'return_time'          => $data['return_date'],
        'gateway_present_time' => $gateway_present_time,
        'net_price'            => $data['net_price'],
        'maximum_discount'     => $data['maximum_discount'],
        'pax_quantity'         => (int)$data['pax_quantity'],
        'note'                 => $data['note'],
        'color_code'           => $data['color'],
        'airline_id'           => (int)$data['airline'],
        'gateway_id'           => (int)$data['gateway_id'],
        'retail_price'         => $data['retail_price'],
        'tl_quantity'          => (int)$data['tl_quantity'],
        'status'               => (int)$data['status'],
        'departure_date'       => $data['departure_date']
    ];
    if($tour) {
        $_data['updated_at'] = date('Y-m-d H:i:s');
        $_data['updated_by'] = $thisstaff->getId();
        if(isset($tour->status_edit_tour) && (int)$tour->status_edit_tour === TourNew::APPROVAL_EDIT_TOUR)
            $_data['status_edit_tour'] = 0;
        $tour->setAll($_data);
        $id = $tour->save(true);
    }else {
        $_data["created_at"] = date('Y-m-d H:i:s');
        $_data["created_by"] = $thisstaff->getId();
        $tour = TourNew::create($_data);
        if (!$tour) throw new Exception('Create error. Please check data again.');
        $id = $tour->save(true);
    }
    $log = TourLogOP::create(
        [
            'tour_id' => $id,
            'content' => json_encode($tour->ht),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId()
        ]
    );
    return $log->save();
}

if (isset($_REQUEST['layout']) && 'edit' === $_REQUEST['layout']){
    $tour_id = (int)trim($_REQUEST['id']);
    $results = TourNew::lookup($tour_id);
    if($results && ($results->destination === null || $results->destination === 0))
        $booking_type_list = TourDestination::loadAllDestination();

    if ($_POST['action'] === 'save') {
        $errors = array();
        try {
            $countHold = TourNew::countHoldCurrent($tour_id);
            if($countHold > 0 && (int)$results->status_edit_tour !== TourNew::APPROVAL_EDIT_TOUR)
                throw new Exception("Access denied");
            db_start_transaction();
            $data = $_POST;
            if (__update_tourNew($data)) {
                db_commit();
                FlashMsg::set('ticket_create', 'Update Successfully.');
                header('Location: '.$cfg->getUrl().'scp/tour-list.php?id='.$tour_id.'&layout=edit');
                exit;
            } else {
                db_rollback();
            }
        } catch (Exception $ex) {
            db_rollback();
            $errors[] = $ex->getMessage();
        }
    }
    $page='tour-edit.inc.php';
}

$_REQUEST['layout'] = isset($_REQUEST['layout']) ? $_REQUEST['layout'] : '';
if (isset($_REQUEST['layout'])) {
    switch ($_REQUEST['layout']) {
        case 'import_reservation':
            $tour_id = (int)trim($_REQUEST['tour']);
            $results = TourNew::lookup($tour_id);
            $page_inc ='tour-reservation.import.inc.php';
        break;
        case 'edit':
            $tour_id = (int)trim($_REQUEST['id']);
            $tour = TourNew::lookup($tour_id);
            $tour_net_price = TourNetPrice::get_tour_net_price($tour_id);
            $flight_ticket = TourNew::getAllFlightTicketOP($tour_id);
            $page_inc ='tour-edit.inc.php';
        break;
        default:
            $params = $qs = [];
            $total = 0;

            //load flow permission and tour no destination
            $params['group_des'] = $group_des;
            $params['tour_not_des'] = 1;

            if(isset($_REQUEST['booking_type_id']) && trim($_REQUEST['booking_type_id'])) {
                $params['booking_type_id'] = trim($_REQUEST['booking_type_id']);
                $qs += ['booking_type_id' => trim($_REQUEST['booking_type_id'])];
            }

            if(isset($_REQUEST['tour_name']) && trim($_REQUEST['tour_name'])) {
                $params['tour_name'] = trim($_REQUEST['tour_name']);
                $qs += ['tour_name' => trim($_REQUEST['tour_name'])];
            }

            if (isset($_REQUEST['tour']) && ($_REQUEST['tour'])) {
                $params['tour'] = trim($_REQUEST['tour']);
                $qs += ['tour' => trim($_REQUEST['tour'])];
            }

            if(isset($_REQUEST['customer_name']) && trim($_REQUEST['customer_name'])) {
                $params['customer_name'] = trim($_REQUEST['customer_name']);
                $qs += ['customer_name' => trim($_REQUEST['customer_name'])];
            }

            if(isset($_REQUEST['customer_phone_number']) && trim($_REQUEST['customer_phone_number'])) {
                $params['customer_phone_number'] = trim($_REQUEST['customer_phone_number']);
                $qs += ['customer_phone_number' => trim($_REQUEST['customer_phone_number'])];
            }

            if(isset($_REQUEST['tour_name']) && trim($_REQUEST['tour_name'])) {
                $params['tour_name'] = trim($_REQUEST['tour_name']);
                $qs += ['tour_name' => trim($_REQUEST['tour_name'])];
            }

            if (isset($_REQUEST['passport_no']) && ($_REQUEST['passport_no'])) {
                $params['passport_no'] = ($_REQUEST['passport_no']);
                $qs += ['passport_no' => trim($_REQUEST['passport_no'])];
            }

            if (isset($_REQUEST['tour_status']) && (int)($_REQUEST['tour_status'])) {
                $params['tour_status'] = (int)($_REQUEST['tour_status']);
                $qs += ['tour_status' => (int)($_REQUEST['tour_status'])];
            }

            if (isset($_REQUEST['from']) && trim($_REQUEST['from'])) {
                $params['from'] = trim($_REQUEST['from']);
                $qs += ['from' => trim($_REQUEST['from'])];
            }
            if (isset($_REQUEST['to']) && trim($_REQUEST['to'])) {
                $params['to'] = trim($_REQUEST['to']);
                $qs += ['to' => trim($_REQUEST['to'])];
            }

            if(isset($_REQUEST['booking_code']) && trim($_REQUEST['booking_code'])) {
                $params['booking_code'] = trim($_REQUEST['booking_code']);
                $qs += ['booking_code' => trim($_REQUEST['booking_code'])];
            }

            if(isset($_REQUEST['flight_code']) && trim($_REQUEST['flight_code'])) {
                $params['flight_code'] = trim($_REQUEST['flight_code']);
                $qs += ['flight_code' => trim($_REQUEST['flight_code'])];
            }

            if(isset($_REQUEST['flight_ticket_code']) && trim($_REQUEST['flight_ticket_code'])) {
                $params['flight_ticket_code'] = trim($_REQUEST['flight_ticket_code']);
                $qs += ['flight_ticket_code' => trim($_REQUEST['flight_ticket_code'])];
            }
        
            $page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
            $offset = ($page - 1) * PAGE_LIMIT;
            $results = TourNew::getPagination($params, $offset, $total, PAGE_LIMIT);
            $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
            $pageNav->setURL('tour-list.php', $qs ?: null);
            $page_inc = "tour-list.inc.php";
        break;
    }
}

$nav->setTabActive('operator', 1);
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page_inc);
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php if (!defined('OSTSCPINC') || !$thisstaff) {
    die('Invalid path');
} ?>
<style>
    td p:first-child {
        margin-top: 0;
    }
    td p:last-child {
        margin-bottom: 0;
    }

    table.list {
        margin-left: -100px;
    }

    table.list tbody td,
    table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }
</style>
<?php $tour_list = []; ?>
<form action="<?php echo $cfg->getUrl() . 'scp/visa-tour-guest.php?tour=' . $_REQUEST['tour'] . '&layout=list' ?>"
      method="POST">
    <?php if (isset($url)): ?><a href="<?php echo $url ?>" target="_blank">Down</a> <?php endif; ?>
    <?php csrf_token(); ?>
    <p class="">
        <a href="#" class="no-pjax btn_sm btn-primary" name="sms" target="_blank" onclick="checkTick_()"> Send SMS</a>
        <?php if(isset($_REQUEST['tour']) && $_REQUEST['tour']): ?>
            <a href="#" class="no-pjax btn_sm btn-success" name="export" target="_blank" onclick="checkTick()">Export</a>
        <?php endif;?>
    </p>
    <table class="list" border="0" cellspacing="1" cellpadding="0" width="1258">
        <caption><?php echo $showing; ?></caption>
        <thead>
        <tr>
            <th><input type="checkbox" id="checkAll"></th>
            <th><?php echo __('Passport'); ?></th>
            <th><?php echo __('Passenger'); ?></th>
            <th><?php echo __('DOB'); ?></th>
            <th><?php echo __('Booking'); ?></th>
            <?php if(!isset($_REQUEST['tour']) || (int)$_REQUEST['tour'] === 0): ?>
                <th><?php echo __('Tour'); ?></th>
            <?php endif;?>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Appointment'); ?></th>
            <th><?php echo __('Deadline'); ?></th>
            <th><?php echo __('#'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $time = time(); ?>
        <?php $i = 0 ;while ($results && ($row = db_fetch_array($results))):?>
            <?php include STAFFINC_DIR . 'visa.list-guest-tour-row.inc.php' ?>
        <?php endwhile; ?>
        </tbody>
    </table>
</form>
<?php
if ($count): //Show options..
    echo '<div>&nbsp;' . __('Page') . ':' . $pageNav->getPageLinks() . '&nbsp;</div>';
endif;
?>
<script>
    function checkTick() {
        var check = $("input:checkbox:checked").map(function () {
            return $(this).val();
        }).get();
        var index = check.indexOf("on");
        if (index > -1) {
            check.splice(index, 1);
        }
        $("a[name='export']").attr("href", "<?php
            echo $cfg->getUrl() . 'scp/visa-tour-guest.php?tour=' . $_REQUEST['tour'] . '&action=export'
            ?>" + '&ticks=' + check)

    }
    function checkTick_() {
        var check = $("input:checkbox:checked").map(function () {
            return $(this).val();
        }).get();
        var index = check.indexOf("on");
        if (index > -1) {
            check.splice(index, 1);
        }
        $("a[name='sms']").attr("href", "<?php
            echo $cfg->getUrl() . 'scp/visa-tour-guest.php?tour=' . $_REQUEST['tour'] . '&layout=sms'
            ?>" + '&ticks=' + check + '&send=send_group')

    }

    $( document ).ready(function() {
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
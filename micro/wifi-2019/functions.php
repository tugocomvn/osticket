<?php

use Medoo\Medoo;

class Awing {
    private static $database;
    private static $data;
    private static $success;

    private static function post(array $request_data) {
        $curl = curl_init();

        curl_setopt_array(
            $curl,
            [
                CURLOPT_URL            => API_URL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => json_encode($request_data),
                CURLOPT_HTTPHEADER     => array(
                    "Content-Type: application/json",
                    "cache-control: no-cache",
                ),
            ]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception($err);
        } else {
            return $response;
        }
    }

    public static function get_data($request_data) {
        return static::post($request_data);
    }

    private static function _init_db() {
        if (static::$database) return true;

        static::$database = new Medoo(
            [
                'database_type' => 'mysql',
                'database_name' => DB_NAME,
                'server'        => DB_HOST,
                'username'      => DB_USERNAME,
                'password'      => DB_PASSWORD,
            ]
        );

        static::$database->query("SET time_zone = '+7:00';");
        static::$database->query("SET NAMES utf8;");
    }

    public static function saveEventObject($data) {
        static::_init_db();
        $event_object = static::$database->get(EVENT_OBJECT_TABLE, ["id"], $data);
        if ($event_object) return $event_object['id'];

        static::$database->insert(
            EVENT_OBJECT_TABLE,
            [
                "wifi_id"      => $data['wifi_id'],
                "name"         => $data['name'],
                "email"        => $data['email'],
                "phone_number" => $data['phone_number'],
                "campaignId"   => $data['campaignId'],
                "created_at"   => date('Y-m-d H:i:s'),
            ]
        );

        return static::$database->id();
    }

    public static function saveWifi($data) {
        static::_init_db();
        $wifi = static::$database->get(WIFI_TABLE, ["id"], $data);
        if ($wifi) return $wifi['id'];

        $wifi_data = [
            "macAddress" => $data['macAddress'],
            "timeline"   => $data['timeline'],
            "eventLabel" => $data['eventLabel'],
            "placeId"    => $data['placeId'],
            "created_at" => date('Y-m-d H:i:s'),
        ];

        static::$database->insert(WIFI_TABLE, $wifi_data);
        return static::$database->id();
    }

    public static function requestData($request_data) {
        $data = Awing::get_data($request_data);
        echo date('Y-m-d H:i:s').": $data\r\n";
        $data = json_decode($data, true);
        static::saveData($data);
    }

    public static function saveData($data) {
        if (!isset($data['resultCode']) || $data['resultCode'] !== 'Success') return false;
        if (!isset($data['data']) || !count($data['data'])) return false;

        foreach ($data['data'] as $_data) {
            if (!$_data || !is_array($_data)) continue;
            $wifi_data = [
                "macAddress" => $_data['macAddress'],
                "timeline"   => $_data['timeline'],
                "eventLabel" => $_data['eventLabel'],
                "placeId"    => $_data['placeId'],
            ];

            $wifi_id = Awing::saveWifi($wifi_data);

            $event = json_decode($_data['eventObject'], true);
            if (!$event || !is_array($event)) continue;
            if ( !(isset($event['email']) && !empty(trim($event['email'])))
                && !(isset($event['phone']) && !empty(trim($event['phone'])))) continue;

            $event_data = [
                'campaignId'   => API_CAMPAIGN_ID,
                "wifi_id"      => $wifi_id,
                "name"         => isset($event['name']) ? trim($event['name']) : '',
                "email"        => isset($event['email']) ? trim($event['email']) : '',
                "phone_number" => isset($event['phone']) ? trim($event['phone']) : '',
            ];

            Awing::saveEventObject($event_data);
        }
    }

    public static function getDataToSendVoucher() {
        static::_init_db();
        $where = [
            'OR' => [
                'voucher_id'    => null,
                'voucher_id[=]' => '',
            ],
        ];

        $event_objects = static::$database->select(EVENT_OBJECT_TABLE, ["id", "name", "email", "phone_number", "campaignId"], $where);
        if (!$event_objects) {
            echo "No event\r\n";
            return false;
        }

        $sent = false;
        foreach ($event_objects as $obj) {
            $voucher = Awing::getVoucher($obj['campaignId']);
            if (!$voucher) {
                echo "No voucher\r\n";
                break;
            }

            $sent = false;
            if (isset($obj['phone_number']) && $obj['phone_number']) {

                if (defined('DEV_ENV') && DEV_ENV)
                    $obj['phone_number'] = '0392998182';

                echo date('Y-m-d H:i:s') . ': Send SMS to ' . $obj['phone_number'] . "\r\n";
                Voucher::sendSMS($obj['name'], $obj['phone_number'], $voucher['code_voucher']);
                $sent = true;
            }

            if (isset($obj['email']) && $obj['email']) {

                if (defined('DEV_ENV') && DEV_ENV)
                    $obj['email'] = 'phamquocbuu@gmail.com';

                echo date('Y-m-d H:i:s') . ': Send email to ' . $obj['email'] . "\r\n";
                Voucher::sendEmail($obj['name'], $obj['email'], $voucher['code_voucher']);
                $sent = true;
            }

            if ($sent) {
                static::_update_voucher_id($obj['id'], $voucher['id']);
            }
        }

        if (!$sent) echo "No voucher sent\r\n";
    }

    private static function _update_voucher_id($event_object_id, $voucher_id) {
        static::_init_db();
        $date = date('Y-m-d H:i:s');
        $data = [
            'voucher_id' => $voucher_id,
            'voucher_time' => $date,
        ];
        $where = [ 'id' => $event_object_id ];
        static::$database->update(EVENT_OBJECT_TABLE, $data, $where);

        $data = [
            'wifi_eventobject_id' => $event_object_id,
            'voucher_id' => $voucher_id,
            'send_date' => $date,
        ];

        static::$database->insert(EVENT_OBJECT_VOUCHER_TABLE, $data);

        $data = [ 'status' => SEND ];
        $where = [ 'id' => $voucher_id ];
        static::$database->update(VOUCHER_TABLE, $data, $where);
    }

    public static function getVoucher($campaign = null, $not_sent = true, $code = null) {
        static::_init_db();

        $where = [];
        if ($campaign) {
            $cols = [ 'group_id', 'name' ];
            $group = static::$database->get(GROUP_VOUCHER_TABLE, $cols, [ 'campaignId' => $campaign ]);
            if (!$group) return false;

            $where['group_id'] = $group['group_id'];
        }

        if (!is_null($not_sent) && $not_sent === true) $where['status'] = NOT_SEND;
        if (!is_null($code) && !empty($code)) $where['code_voucher'] = $code;
        $cols = ['id', 'code_voucher', 'status', 'group_id'];

        return static::$database->get(VOUCHER_TABLE, $cols, $where);
    }

    public static function getGroupByName($group_name){
        static::_init_db();
        $group_name = trim($group_name);

        $cols = ['group_id', 'name'];
        $where = [ 'name' => $group_name ];
        $group = static::$database->get(GROUP_VOUCHER_TABLE, $cols, $where);

        return $group;
    }

    public static function getGroupByCampaignID($campaignId){
        static::_init_db();
        $campaignId = trim($campaignId);

        $cols = ['group_id', 'name'];
        $where = [ 'campaignId' => $campaignId ];
        $group = static::$database->get(GROUP_VOUCHER_TABLE, $cols, $where);

        return $group;
    }

    public static function getGroupById($id) {
        static::_init_db();
        $id = trim($id);

        $cols = '*';
        $where = [ 'group_id' => $id ];
        $group = static::$database->get(GROUP_VOUCHER_TABLE, $cols, $where);

        return $group;
    }

    public static function addGroup($name, $campaignId, $content, $from, $to) { // đặt tên function phù hợp hơn
        static::_init_db();
        $data = [
            'name'       => $name,
            'campaignId' => $campaignId,
            'content'    => $content,
            'date_start' => $from,
            'date_end'   => $to,
        ];

        try {
            static::$database->insert(GROUP_VOUCHER_TABLE, $data);
        } catch (Exception $ex) { return false; }

        $id = static::$database->id();
        if ($id && static::getGroupById($id)) return true;
        return false;
    }

    public static function updateGroup($id,$data)
    {
        static::_init_db();
        $where = ['group_id'=>$id];
        $update = static::$database->update(GROUP_VOUCHER_TABLE,$data,$where);
        return  $update->rowCount();
    }

    public static function selectDataGroup($offset,$search = null) {
        static::_init_db();
        if(!is_null($search))
        {
            $where = ['name' => $search];
        }else
            $where = ['ORDER' => ['date_create' => 'DESC'], 'LIMIT' => [$offset, PAGE_LIMIT]];

        $data = static::$database->select(
            GROUP_VOUCHER_TABLE,
            ['group_id', 'name', 'campaignId', 'content', 'date_start', 'date_end'],
            $where
        );
        return $data;
    }

    public static function getTotalGroup($search = null){
        static::_init_db();
        if(!is_null($search))
        {
            $where = ['name' => $search];
            return static::$database->count(GROUP_VOUCHER_TABLE,$where);
        }
        return static::$database->count(GROUP_VOUCHER_TABLE);
    }

    public static function getTotalData($code) {
        static::_init_db();

        $where = [];
        if ($code) {
            $where = [
                'code_voucher' => $code,
            ];
        }
        return static::$database->count(
            GROUP_VOUCHER_TABLE,
            [ "[>]".GROUP_VOUCHER_TABLE => [ "group_id" => "group_id" ] ],
            $where
        );
    }

    // list voucher
    public static function getListLimit($code, $offset) {
        static::_init_db();
        $cols = [
            VOUCHER_TABLE.'.id',
            VOUCHER_TABLE.'.group_id',
            VOUCHER_TABLE.'.code_voucher',
            GROUP_VOUCHER_TABLE.'.name',
            GROUP_VOUCHER_TABLE.'.content',
            VOUCHER_TABLE.'.status',
            GROUP_VOUCHER_TABLE.'.date_start',
            GROUP_VOUCHER_TABLE.'.date_end',
            VOUCHER_TABLE.'.date_create'
        ];

        $where = [];
        if ($code) {
            $where = [
                'code_voucher' => $code,
                'LIMIT' => [$offset, PAGE_LIMIT]
            ];
        }
        $result = static::$database->select(
            VOUCHER_TABLE,
            [ "[>]".GROUP_VOUCHER_TABLE => [ "group_id" => "group_id" ] ],
            $cols,
            $where
        );

        return $result;
    }

    public static function insertDataVoucher($vouchers, $status, $category) {
        static::_init_db();

        foreach($vouchers as $voucher) {
            $data = [
                'code_voucher' => $voucher,
                'status'       => $status,
                'group_id'     => $category,
            ];
            static::$database->insert(VOUCHER_TABLE, $data);
        }
    }

    public static function getStatusName($status) {
        switch ($status) {
            case NOTSEND:
                return "Mã của bạn không có giá trị sử dụng";
                break;
            case SENT:
                return "Mã của bạn có thể sử dụng";
                break;
            case NOTREDEEM:
                return "Mã của bạn có thể sử dụng";
                break;
            case REDEEM:
                return "Mã của bạn có thể sử dụng";
                break;
        }
    }

    public static function getResultContent($row) {
        static::_init_db();

        $cols = [ 'group_id', 'content', 'date_start', 'date_end'];
        $group = static::$database->get(GROUP_VOUCHER_TABLE, $cols, [ 'group_id' => $row['group_id'] ]);
        if (!$group) return "<p>Voucher có mã ".$row['code_voucher']." không hợp lệ</p>";

        switch ($row['status']) {
            case SEND:
            case NOT_ACTIVE:
                $str_result =  "<p>Voucher có mã <strong>".$row['code_voucher']."</strong> đã được gửi đến khách hàng </p>";
                $str_result .= "<p><i> ".$group['content']. "</i></p>";
                $str_result .= "<p>Thời gian áp dụng: từ ngày "
                    .date('d/m/Y', strtotime($group['date_start']))
                    . " đến hết ngày ".date('d/m/Y', strtotime($group['date_end']))." </p>";
                break;

            case ACTIVE:
                $str_result =  "<p>Voucher có mã ".$row['code_voucher']." đã được sử dụng </p>";
                break;

            case NOT_SEND:
            default:
                $str_result =  "<p>Không tìm thấy mã voucher ".$row['code_voucher']."</p>";
                break;
        }

        return $str_result;
    }

    public static function checkActiveVoucher($code)
    {
        static::_init_db();

        $where = ['code_voucher' => trim($code),
            'status' => SEND];
        return static::$database->count(VOUCHER_TABLE, $where)>0?true:false;
    }

    public static function activeVoucher($code_voucher)
    {
        static::_init_db();
        $data = ['status' => ACTIVE];
        $where = ['code_voucher' => $code_voucher];
        return static::$database->update(VOUCHER_TABLE,$data,$where);
    }

    public static function checkBookingCodeRedeem($booking_code)
    {
        static::_init_db();
        $where = ['booking_code' => $booking_code];
        return static::$database->count(VOUCHER_REDEEM_TABLE,$where)>0?true:false;
    }

    public static function insertRedeem($booking_code_new,$code_voucher,$user_id){
        static::_init_db();
        $data = [
            'booking_code' => $booking_code_new,
            'code_voucher' => $code_voucher,
            'user_id'      => $user_id,
        ];
        static::$database->insert(VOUCHER_REDEEM_TABLE, $data);
    }

    public static function insertUser($name,$sdt,$email){
        static::_init_db();
        $data = [
            'customer_name' => $name,
            'phone_number'  => $sdt,
            'email'         => $email,
        ];
        static::$database->insert(USER_VOUCHER_TABLE, $data);
        return static::$database->id();
    }

    public static function findUser($sdt){
        static::_init_db();
        $col = ['user_id'];
        $where = ['phone_number'  => $sdt];
        return static::$database->get(USER_VOUCHER_TABLE,$col, $where);
    }

    public static function getTotalRedeem(){
        static::_init_db();
        return static::$database->count(VOUCHER_REDEEM_TABLE);
    }

    public static function getListRedeem($offset){
        static::_init_db();
        $cols = [
            'customer_name',
            'phone_number',
            'code_voucher',
            'date_redeem'
        ];
        $join = ["[>]".USER_VOUCHER_TABLE  => ["user_id" => "user_id"]];
        $where = [
            'ORDER' => ['date_redeem' => 'DESC'],
            'LIMIT' => [$offset, PAGE_LIMIT]
        ];
        return static::$database->select(VOUCHER_REDEEM_TABLE,$join,$cols,$where);
    }

    public static function redeemVoucher($booking_code_new,$code_voucher,$user_id){
        static::_init_db();
        static::$success = true;
        static::$data = [
            'booking_code' => $booking_code_new,
            'code_voucher' => $code_voucher,
            'user_id'      => $user_id,
        ];
        static::$database->action(function(){
            //Add voucher in VOUCHER_REDEEM_TABLE
            static::$database->insert(VOUCHER_REDEEM_TABLE, static::$data);
            $add_listRedeem = intval(static::$database->id());
            if( $add_listRedeem <= 0) {
                static::$success = false;
                return false;
            }

            //Active voucher in VOUCHER_TABLE
            static::$database->update(VOUCHER_TABLE, ['status' => ACTIVE], ['code_voucher' => static::$data['code_voucher']]);
            $active = static::$database->has(VOUCHER_TABLE, ['code_voucher' => static::$data['code_voucher'],'status' =>ACTIVE]);
            if(!$active) {
                static::$success = false;
                return false;
            }
        });
        return static::$success;
    }
}

class Voucher {
    function generate_string($length, $total, $pre = '', $pos = '') {
        $input = PERMITTED_CHARS;
        $input_length = strlen($input);
        $arr_Code = array();
        $temp = 0;
        $max_combination = pow(strlen($input), $length);
        $try = 0;
        while ($temp < $total && $try < $max_combination) {
            $random_string = $pre; //them tien to neu co

            for ($i = 0; $i < $length; $i++) { //random ma
                $random_character = $input[mt_rand(0, $input_length - 1)];
                $random_string .= $random_character;
            }

            if($pos != '') $random_string .= $pos; //kiem tra cohau to
            $try++;
            if (Awing::getVoucher(null, null, $random_string) || in_array($random_string, $arr_Code))
                continue;

            array_push($arr_Code, $random_string);
            $temp++;
        }
        return $arr_Code;
    }

    public static function random_status($total){
        $status = array();
        for($i = 0; $i < $total; $i++){
            array_push($status,rand(1,4));
        }
        return $status;
    }

    public static function sendEmail($name, $email, $voucher) {
        $subject = 'Tugo Travel kính tặng quý khách voucher';
        $content = "Tugo gửi quý khách mã voucher: $voucher giảm giá 300.000đ. Truy cập website: Tugo.com.vn để tham khảo chương trình hoặc liên hệ hotline: 1900555543";
        static::_send_email($email, $name, $subject, $content, false, 'Tugo Travel');
    }

    public static function sendSMS($name, $phone_number, $voucher) {
        $message = "Tugo gửi tặng quý khách mã voucher: $voucher giảm giá 300.000đ. Truy cập website: Tugo.com.vn để tham khảo chương trình hoặc liên hệ hotline: 1900555543";
        static::_send_sms($phone_number, $message);
    }

    private static function _send_sms($phone_number, $message) {
        $url = 'http://support.tugo.com.vn/sms/sms.php';
        $curl = curl_init();
        $request_data = [
            'phone_number' => $phone_number,
            'message'      => $message,
            'code'         => '*&CH*a(21*h&A^4%34pSU$v^&s*()',
            'checking'     => hash('sha256', ($phone_number . $message . $phone_number)),
        ];
        curl_setopt_array(
            $curl,
            [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => $request_data,
                CURLOPT_HTTPHEADER     => array(
                    "Content-Type: multipart/form-data",
                    "cache-control: no-cache",
                ),
            ]
        );

        $response = curl_exec($curl);
        var_dump($response);
        $err = curl_error($curl);

        curl_close($curl);

        return true;
    }

    private static function _send_email($to, $to_name, $subject, $content, $from = false, $from_name = false, $files = null) {
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = SYSTEM_EMAIL_SERVER;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = SYSTEM_EMAIL_USERNAME;                 // SMTP username
        $mail->Password = SYSTEM_EMAIL_PASSWORD;                           // SMTP password
        $mail->SMTPSecure = SYSTEM_EMAIL_SMTPSecure;                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = SYSTEM_EMAIL_SMTPPort;                                    // TCP port to connect to
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true,
            ),
        );

        if (isset($files) && count($files)) {
            foreach ($files as $file)
                $mail->addAttachment($file['path'], $file['name']);         // Add attachments
        }

        $mail->setFrom($from ?: SYSTEM_EMAIL_USERNAME, $from_name ?: SYSTEM_EMAIL_NAME);
        if (is_array($to) && is_array($to_name)) {
            foreach ($to as $key => $value) {
                $mail->addAddress($value, $to_name[$key]);     // Add a recipient
            }
        } elseif (is_string($to) && is_string($to_name)) {
            $mail->addAddress($to, $to_name);     // Add a recipient
        } else {
            echo "Invalid email address\r\n";
            return false;
        }

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $content;
        $mail->AltBody = substr(strip_tags($content), 32);

        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

    public static function  createBookingCodeVoucher()
    {
        $permitted_chars = PERMITTED_CHARS;
        $max_combination = pow(strlen($permitted_chars), SIZE_BOOKING_CODE);
        $try = 0;
        $input_length = strlen($permitted_chars);
        while ($try < $max_combination) {
            $random_booking_code = "";
            for ($i = 0; $i < SIZE_BOOKING_CODE; $i++) { //random ma
                $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                $random_booking_code .= $random_character;
            }

            $check = Awing::checkBookingCodeRedeem($random_booking_code);

            if(!$check){ //ma booking_code kha dung
                return $random_booking_code;
            }

            $try++;
        }
        return '';
    }
}


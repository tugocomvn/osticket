<?php
// list guests per tour

if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if(!$thisstaff->canViewVisaDocuments()) die('Access Denied');
?>
<style>
    .label-female {
        background: deeppink;
    }
</style>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/visa-tour-list.php"?>">Back</a></p>
<h2>Visa Documents</h2>
<div class="pull-right">
    <p>Tour <strong><?php echo $tour->name ?></strong></p>
    <p><?php
        echo $booking_type_list[(int)$tour->destination]['name']?:''.' - ';
        if($tour->gateway_present_time) echo date('d/m/Y', strtotime($tour->gateway_present_time)) ?>
    </p>
</div>
<?php include STAFFINC_DIR.'visa.list-guest-tour.inc.php' ?>
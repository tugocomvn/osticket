<?php

require_once(INCLUDE_DIR . 'class.orm.php');
require_once INCLUDE_DIR.'class.flight-ticket.php';

class PaxModel extends VerySimpleModel {
    static $meta = [
        'table' => PAX_TABLE,
        'pk' => ['id'],
        'ordering' => ['full_name'],
        'joins' => [
            'tours' => [
                'reverse' => 'PaxTourModel.pax_id',
            ],
            'infos' => [
                'reverse' => 'PaxInfoModel.pax_id',
            ],
        ],
    ];
}

class Pax extends PaxModel {
    function save($refetch=false) {
        if (count($this->dirty)) //XXX: doesn't work??
            $this->set('updated_at', new SqlFunction('NOW'));
        return parent::save($refetch);
    }

    function checkAndCreate() {

    }

    public static function search(array $keyword_pax) {
        $sql = "SELECT DISTINCT t.tour_id
                FROM ost_pax p JOIN ost_pax_info i ON p.id=i.pax_id JOIN ost_pax_tour t ON p.id=t.pax_id AND i.id=t.pax_info WHERE 1 ";

        if (isset($keyword_pax['pax_name']) && $keyword_pax['pax_name']) {
            $sql .= " AND p.full_name LIKE '%".$keyword_pax['pax_name']."%' ";
        }

        if (isset($keyword_pax['pax_phone_number']) && $keyword_pax['pax_phone_number']) {
            $phone_number = preg_replace('/[^0-9]/', '', $keyword_pax['pax_phone_number']);
            $phone_number = preg_replace('/^0/', '', $phone_number);
            $migrate_phone = _String::convertMobilePhoneNumber($phone_number);
            $sql .= " AND 
                (
                REPLACE(REPLACE(REPLACE(t.phone_number, '-', ''), '.', ''), ' ', '') LIKE '%".$phone_number."%' 
                OR REPLACE(REPLACE(REPLACE(t.phone_number, '-', ''), '.', ''), ' ', '') LIKE '%".$migrate_phone."%'
                ) 
            ";
        }

        if (isset($keyword_pax['pax_passport_no']) && $keyword_pax['pax_passport_no']) {
            $passport_no = str_replace(' ', '', $keyword_pax['pax_passport_no']);
            $passport_no = str_replace('-', '', $passport_no);
            $passport_no = str_replace('.', '', $passport_no);
            $sql .= " AND REPLACE(REPLACE(REPLACE(i.passport_no, '-', ''), '.', ''), ' ', '') LIKE '%".$passport_no."%' ";
        }

        if (isset($keyword_pax['pax_booking_code']) && $keyword_pax['pax_booking_code']) {
            $booking_code = str_replace(BOOKING_CODE_PREFIX, '', $keyword_pax['pax_booking_code']);
            $booking_code = str_replace(BOOKING_CODE_PREFIX_SHORT, '', $booking_code);
            $booking_code = str_replace(trim(BOOKING_CODE_PREFIX, '-'), '', $booking_code);
            $booking_code = str_replace(trim(BOOKING_CODE_PREFIX_SHORT, '-'), '', $booking_code);
            $booking_code = str_replace(' ', '', $booking_code);
            $booking_code = preg_replace('/^0+/', '', $booking_code);
            $sql .= " AND t.booking_code LIKE '%".$booking_code."%' ";
        }

        $res = db_query($sql);
        if (!$res) return null;
        $list = [];
        while ($res && ($row = db_fetch_array($res))) {
            $list[] = $row['tour_id'];
        }
        return $list ? $list : null;
    }
}

class PaxTourModel extends VerySimpleModel {
    static $meta = [
        'table' => PAX_TOUR_TABLE,
        'pk' => ['pax_id', 'tour_id'],
        'joins' => [
            'pax' => [
                'constraint' => ['pax_id' => 'PaxModel.id']
            ],
        ]
    ];
}

class PaxTour extends PaxTourModel {
    static function getData($tour_id, $join_booking = false) {
        $sql = "SELECT DISTINCT p.*, i.passport_no, i.doe, i.nationality, t.tour_id, t.booking_code, t.phone_number, t.room, t.note, tl,
            t.visa_estimated_due_date, t.apply_date, t.receiving_date, t.visa_result, t.room_type ".(
            $join_booking ? " , v.staff, v.ticket_id " : " "
            )."
          FROM ost_pax p JOIN ost_pax_info i ON p.id=i.pax_id
          JOIN ost_pax_tour t ON p.id=t.pax_id AND i.id=t.pax_info
          ".(
              $join_booking ? " LEFT JOIN booking_view v ON trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM t.booking_code)) = trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM v.booking_code)) " : " "
            )."
          WHERE t.tour_id=".db_input($tour_id)
        . " ORDER BY `order` ";
        return db_query($sql);
    }

    static function countPax($tour_id, $group_by_room = false, $by_visa = false) {
        if (!$tour_id) return null;
        $tour_id = (int)$tour_id;
        if (!$tour_id) return null;

        if (isset($_SESSION['__countPax']['timeout']) && $_SESSION['__countPax']['timeout'] > time()+1000
            && isset($_SESSION['__countPax']['data'][ $tour_id ]) && is_numeric($_SESSION['__countPax']['data'][ $tour_id ])
            && !$group_by_room && !$by_visa) {
            return $_SESSION['__countPax']['data'][ $tour_id ];
        }

        $sql = "SELECT ".
            ($group_by_room ? " COUNT(DISTINCT CONCAT(room_type,'_',room)) as total " : " COUNT(*) as total " )
            . ($group_by_room ? ", room_type " : ( $by_visa ? ", visa_result " : " " ) )
            ." FROM ost_pax_tour WHERE tour_id=$tour_id "
            . ($group_by_room ? " GROUP BY room_type " : ( $by_visa ? " GROUP BY visa_result " : " "));

        $res = db_query($sql);
        if (!$res) return null;

        if (!$group_by_room && !$by_visa) {
            $row = db_fetch_array($res);
            $_SESSION['__countPax']['timeout'] = time() + 60;
            $_SESSION['__countPax']['data'][ $tour_id ] = 0;

            if (!$row || !isset($row['total'])) return 0;

            $_SESSION['__countPax']['data'][ $tour_id ] = $row['total'];
            return $row['total'];

        } elseif (!$group_by_room && $by_visa) {

            $data = [];
            while ($row = db_fetch_array($res)) {
                $data[$row['visa_result']] = $row;
            }

            return $data;

        } else {

            $data = [];
            while ($row = db_fetch_array($res)) {
                $data[$row['room_type']] = $row;
            }

            $tl_room = PaxTour::getTourLeaderRoom($tour_id);

            if (!$tl_room) {
                if (isset($data['DBL']) && isset($data['DBL']['total']))
                    $data['DBL']['total'] = $data['DBL']['total']+1;
                else
                    $data['DBL'] = ['total' => 1, 'room_type' => 'DBL'];
            }

            return $data;
        }
    }

    static function calcIncome($tour_id) { // from booking
        if (!$tour_id || !is_numeric($tour_id)) return 0;
        $tour_id = (int)$tour_id;

        $sql = "SELECT sum(total_retail_price) as total from (
            select DISTINCT b.ticket_id, b.number, b.total_retail_price, b.booking_code
            from booking_view b
            where trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code))) IN
                 (
                   select distinct trim(LEADING '0' FROM
                                        trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM p.booking_code)))
                   from ost_pax_tour p
                   WHERE p.tour_id = ".db_input($tour_id).
            "
                 )
        ) as a";

        $res = db_query($sql);
        if (!$res || !($row = db_fetch_array($res)) || !isset($row['total'])) return 0;
        return $row['total'];
    }

    static function calcIncomePayment($tour_id) { // from booking
        if (!$tour_id || !is_numeric($tour_id)) return 0;
        $tour_id = (int)$tour_id;

        $sql = "SELECT ".sprintf(" SUM( CASE WHEN topic_id = %s THEN amount ELSE -1*amount END ) AS %s ", THU_TOPIC, 'total')."
        from (
           select DISTINCT b.topic_id, b.ticket_id, b.number, b.amount, b.booking_code
           from payment_tmp b
           where
            1    
            and (exclude is null or exclude like '' or exclude=0)
            AND trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code))) IN (
             select distinct trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM p.booking_code)))
             from ost_pax_tour p
             WHERE p.tour_id = ".db_input($tour_id).
            "
           )) as a";

        $res = db_query($sql);
        if (!$res || !($row = db_fetch_array($res)) || !isset($row['total'])) return 0;
        return $row['total'];
    }

    static function getPayments($tour_id, $exclude = 1) {
        if (!$tour_id || !is_numeric($tour_id)) return 0;
        $tour_id = (int)$tour_id;

        $sql = "select distinct b.ticket_id, b.booking_code, b.amount, b.receipt_code, b.time, b.quantity, b.topic_id
        from payment_tmp b
        WHERE 
          1
          ".($exclude ? " and (exclude is null or exclude like '' or exclude=0) " : " ")."
          AND trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code))) IN (
          select DISTINCT trim(LEADING '0' FROM
                               trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM p.booking_code)))
          FROM ost_pax_tour p
          WHERE p.tour_id = ".db_input($tour_id)
            . "
        )
        group by b.ticket_id";

        return db_query($sql);
    }

    public static function CountBookingCodeInDifferentTour($booking_code) {
        if (!$booking_code || empty($booking_code)) return 0;
        $booking_code_trim = _String::trimBookingCode($booking_code);
        $sql = "SELECT COUNT(DISTINCT tour_id) FROM ".OST_PAX_TOUR_TABLE." p WHERE trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM p.booking_code))=".db_input($booking_code_trim);
        $res = db_count($sql);
        if(!$res) return 0;
        return $res;
    }

    static function getPaymentsGroup($tour_id) {
        if (!$tour_id || !is_numeric($tour_id)) return 0;
        $tour_id = (int)$tour_id;
        $sql = "select distinct x.ticket_id, x.booking_code, sum(x.amount * (IF(x.topic_id = 17, 1, -1))) as amount, total_quantity as quantity
            from (
                select distinct b.ticket_id,
                          trim(LEADING '0' FROM
                               trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code))) as booking_code,
                          b.amount,
                          b.quantity,
                          b.topic_id,
                          b.receipt_code,
                          (select v.total_quantity
                           from booking_view v
                           WHERE trim(LEADING '0' FROM
                                      trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM v.booking_code)))
                                   LIKE trim(LEADING '0' FROM
                                             trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code)))
                           LIMIT 1)                                                                 as total_quantity
                from payment_tmp b
                where
                    1 
                    and (exclude is null or exclude like '' or exclude=0)
                    AND trim(LEADING '0' FROM
                     trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM b.booking_code))) IN (
                  select DISTINCT trim(LEADING '0' FROM
                                       trim(LEADING '".BOOKING_CODE_PREFIX."' FROM trim(LEADING '".BOOKING_CODE_PREFIX_SHORT."' FROM p.booking_code)))
                  FROM ost_pax_tour p
                  WHERE p.tour_id = ".db_input($tour_id)."
                )
                group by b.ticket_id
            ) x
            group by x.booking_code";

        return db_query($sql);
    }

    static function getTourLeaderRoom($tour_id) {
        if (!$tour_id) return null;
        $tour_id = (int)$tour_id;
        if (!$tour_id) return null;

        $sql = "SELECT pax_id, room, room_type FROM ost_pax_tour WHERE tour_id=$tour_id AND tl=1 LIMIT 1";
        $res = db_query($sql);
        if (!$res) return null;

        $row = db_fetch_array($res);
        return $row;
    }

    static function remove($ids, $tour_id = 0) {
        if (!is_array($ids) || !count($ids)) return null;
        if (!$tour_id) return null;

        $sql = "DELETE FROM ost_pax_tour WHERE pax_id IN (".implode(',', $ids).") AND tour_id = ".db_input((int)$tour_id);
        return db_query($sql);
    }
}

class PaxInfoModel extends VerySimpleModel {
    static $meta = [
        'table' => PAX_INFO_TABLE,
        'pk' => ['id'],
        'ordering' => ['passport_no'],
        'joins' => [
            'pax' => [
                'constraint' => ['pax_id' => 'PaxModel.id']
            ],
        ]
    ];
}

class PaxInfo extends PaxInfoModel {

}


class TourModel extends VerySimpleModel {
    static $meta = [
        'table' => TOUR_TABLE,
        'pk' => ['id'],
        'ordering' => ['departure_date'],
    ];
}

class Tour extends TourModel {
    private static $tour_list = [];

    public static function getPagination(array $params, $offset, $limit = 30, &$total) {
        $sql = "SELECT *
                FROM ".TOUR_TABLE." t ";

        if (isset($params['join_account_general']) && $params['join_account_general']) {
            $sql .= " LEFT JOIN ".ACC_GENERAL." a ON t.id=a.tour_id";
        }
        $sql .= " WHERE 1 ";

        $limit = " LIMIT $limit OFFSET $offset ";

        if(isset($params['group_des']) && is_array($params['group_des']))
            if(count($params['group_des']) > 0)
                if(isset($params['tour_not_des']) && $params['tour_not_des'] === 1)
                    $sql .= " AND ( destination IN (" . implode(",", $params['group_des']) . ")  OR destination is null ) "; // allow view tour NOT destination
                else
                    $sql .= " AND destination IN (" . implode(",", $params['group_des']) . ") ";
            else
                $sql .= " AND 1 = 0 ";

        if (isset($params['tour_name']) && $params['tour_name']) {
            $sql .= " AND t.name LIKE ".db_input('%'.trim($params['tour_name']).'%')." ";
        }

        if (isset($params['booking_type_id']) && is_numeric($params['booking_type_id']) && $params['booking_type_id']) {
            $tour = (int)trim($params['booking_type_id']);
            $sql .= " AND destination = ".db_input($tour)." ";
        } elseif (is_array($params['tour'])) {
            $params['tour'] = array_filter($params['tour']);
            $params['tour'] = array_unique($params['tour']);
            if (count($params['tour'])) {
                $sql .= " AND destination IN (" . implode(',', $params['tour']) . ") ";
            }
        }

        if($params['number_phone']){
           $tour_id = VisaTour::searchNumberPhone($params['number_phone']);
           if($tour_id)
               $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
           else
               $sql .= " AND 1 = 0 ";
        }

        if($params['passport_no']){
           $tour_id = VisaTour::searchPassport($params['passport_no']);
           if($tour_id)
               $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
           else
               $sql .= " AND 1 = 0 ";
        }

        if($params['name_pax']){
           $tour_id = VisaTour::searchNamePax($params['name_pax']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['booking_code']){
            $tour_id = self::sreachBookingCode($params['booking_code']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if (isset($params['airline']) && is_numeric($params['airline']) && $params['airline']) {
            $airline = (int)trim($params['airline']);
            $sql .= " AND airline = ".db_input($airline)." ";
        }

        if (isset($params['from']) && strtotime($params['from'])) {
            $from = trim($params['from']);
            $sql .= " AND date(gather_date) >= ".db_input($from)." ";
        }

        if (isset($params['to']) && strtotime($params['to'])) {
            $to = trim($params['to']);
            $sql .= " AND date(gather_date) <= ".db_input($to)." ";
        }

        if(isset($params['tour_status']) && is_numeric($params['tour_status']) && $params['tour_status']) {
            $sql .= " AND status = ".db_input($params['tour_status']);
        }else{
            $sql .= " AND status = ".TourStatus::Enable."";
        }

        if (isset($params['country']) && is_numeric($params['country']) && $params['country']){
            $country_id = (int)trim($params['country']);
            $sql .= " AND country_id = ". $country_id. " ";
        }

        if (isset($params['destination']) && is_numeric($params['destination']) && $params['destination']){
            $destination_id = (int)$params['destination'];
            $sql .= " AND t.destination = ". $destination_id. " AND t.departure_date > now() ";
        }

        $reservation_ids = null;
        include_once INCLUDE_DIR.'class.booking.php';
        if (isset($params['reservation_id']) && is_numeric($params['reservation_id']) && $params['reservation_id']) {
            $item = BookingReservation::lookup($params['reservation_id']);

            if (!$item || !(isset($item->tour_id) && $item->tour_id)) {
                $sql .= ' AND 1=0 ';
            } else {
                $sql .= ' AND id='.db_input($item->tour_id). ' ';
            }
        }

        if (isset($params['customer_name']) && !empty(trim($params['customer_name']))) {
            $reservations = BookingReservation::objects()->filter(['customer_name__like' => '%'.trim($params['customer_name']).'%'])->all();

            if (is_null($reservation_ids)) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if (isset($params['customer_phonenumber']) && !empty(trim($params['customer_phonenumber']))) {
            $reservations = BookingReservation::objects()->filter(['phone_number__like' => '%'.trim($params['customer_phonenumber']).'%'])->all();

            if (!$reservation_ids) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if(isset($params['staff_id']) && is_numeric($params['staff_id']) && $params['staff_id']){
            $staff_id = (int)trim($params['staff_id']);
            $reservations = BookingReservation::objects()->filter(['staff_id' => $staff_id])->all();
            if (is_null($reservation_ids)) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if (is_array($reservation_ids)) {
            $reservation_ids = array_filter($reservation_ids);
            $reservation_ids = array_unique($reservation_ids);
        }

        if (is_array($reservation_ids)) {
            if (count($reservation_ids))
                $sql .= " AND id IN (".implode(',', $reservation_ids).") ";
            else
                $sql .= " AND 1=0 ";
        }

        $order = "  case when date(gather_date) > NOW() then 0 when date(gather_date) <= NOW() then 1 end,
         case when date(gather_date) > NOW() then gather_date end asc,
         case when date(gather_date) <= NOW() then gather_date end desc ";

        $res = db_query("$sql ORDER BY $order $limit");
        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function updateQuantity($tour_id) {
        if (!$tour_id) return null;

        try {
            $sql = "SELECT sum(sure_qty) as sure, sum(hold_qty) as hold FROM ".BOOKING_RESERVATION_TABLE." WHERE tour_id=".db_input($tour_id).
                " AND (visa_only IS NULL OR visa_only=0)";
            $res = db_query($sql);
            $hold = 0;
            $sure = 0;
            if ($res) {
                $row = db_fetch_array($res);
                if ($row) {
                    $hold = $row['hold'] ?: 0;
                    $sure = $row['sure'] ?: 0;
                }
            }

            if ( $tour = TourNew::lookup($tour_id) ) {
                $tour->set('hold_quantity', $hold);
                $tour->set('sure_quantity', $sure);
                $tour->save();
            }

            return true;
        } catch (Exception $ex) {return false;}
    }

    static function lookupCache($criteria) {
        if (is_numeric($criteria) && $criteria) {
            if (isset(static::$tour_list[$criteria]) && static::$tour_list[$criteria])
                return static::$tour_list[$criteria];

            $item = parent::lookup($criteria);
            static::$tour_list[$criteria] = $item;
            return $item;
        }

        return parent::lookup($criteria);
    }

    public static function countPax($tour_id) {
        $sql = "select count(distinct  pax_id) as total FROM ost_pax_tour where tour_id=".db_input((int)$tour_id);
        $res = db_query($sql);
        if (!$res) return 0;
        $data = db_fetch_array($res);
        if (!$data || !isset($data['total'])) return 0;
        return (int)$data['total'];
    }

    public static function __editTour($data)
    {
        global $thisstaff;

        if(!isset($_POST['destination']) || empty($_POST['destination'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn điểm đến.',
                ]
            );
            exit;
        }

        if(!isset($_POST['gateway_present_time']) || empty($_POST['gateway_present_time']) || !isset($_POST['gateway_present_time_']) || empty($_POST['gateway_present_time_'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn ngày và ngày tập trung.',
                ]
            );
            exit;
        }

        if(!isset($_POST['return_time']) || empty($_POST['return_time'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn ngày về.',
                ]
            );
            exit;
        }

        if(!isset($_POST['tour_name']) || empty($_POST['tour_name'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Dòng Tour Name không được để trống.',
                ]
            );
            exit;
        }

        if(!isset($_POST['pax_quantity']) || empty($_POST['pax_quantity'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn số khách tối đa.',
                ]
            );
            exit;
        }

        $tour = TourNew::lookup($data['tour_id']);

        if(!$tour){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => '- Tour is Required.',
                ]
            );
            exit;
        }

        if($tour)
        {
            $gather_date = $data['gather_date']." ".$data['gather_time'];
            $data_edit = [
                'last_updated_at'           => date('Y-m-d H:i'),
                'last_updated_by'           => $thisstaff->getId(),
                'name'                      => $data['tour_name'],
                'code'                      => $data['code'],
                'code_b'                    => $data['code_b'],
                'destination'               => $data['booking_type'],
                'tour_leader'               => $data['tour_leader'],
                'quantity'                  => $data['quantity'],
                'tl_quantity'               => $data['tl_quantity'],
                'transit_airport'           => $data['transit_airport'],
                'notes'                     => $data['note'],
                'retail_price'              => $data['retail_price'],
                'net_price'                 => $data['net_price'],
                'return_date'               => date_create_from_format('d/m/Y',$data['return_date'])->format('Y-m-d'),
                'gather_date'               => date_create_from_format('d/m/Y H:i',$gather_date)->format('Y-m-d H:i'),
                'status'                    => $data['tour_status'],
                'tour_date_a'               => (!empty($data['tour_date_a']))?date_create_from_format('d/m/Y',$data['tour_date_a'])->format('Y-m-d'):NULL,
                'tour_date_b'               => (!empty($data['tour_date_b']))?date_create_from_format('d/m/Y',$data['tour_date_b'])->format('Y-m-d'):NULL,
                'tour_date_c'               => (!empty($data['tour_date_c']))?date_create_from_format('d/m/Y',$data['tour_date_c'])->format('Y-m-d'):NULL,
                'tour_date_d'               => (!empty($data['tour_date_d']))?date_create_from_format('d/m/Y',$data['tour_date_d'])->format('Y-m-d'):NULL,
                'departure_flight_code'     => $data['departure_flight_code'],
                'return_flight_code'        => $data['return_flight_code'],
                'departure_flight_code_b'   => $data['departure_flight_code_b'],
                'return_flight_code_b'      => $data['return_flight_code_b'],
                'itinerary_a'               => $data['itinerary_a'],
                'itinerary_b'               => $data['itinerary_b'],
                'itinerary_c'               => $data['itinerary_c'],
                'itinerary_d'               => $data['itinerary_d']
            ];
        }
        $tour->setAll($data_edit);
        $id__ = $tour->save(true);
        $log = TourHistory::create(
            [
                'ost_tour_history_id' => $id__,
                'snapshot' => json_encode($tour->ht),
                'time' => date('Y-m-d H:i:s'),
            ]
        );
        $log_id = $log->save();
        $tour->set('log_id_ost_tour', $log_id);
        $tour->save(true);
        echo json_encode(
            [
                'result' => 1,
                'message' => 'Đã lưu thành công trang sẽ tự quay về trang danh sách .',
            ]
        );
        exit;
    }

    public static function sreachBookingCode($booking_code){
        $sql = "SELECT DISTINCT opt.tour_id FROM ost_pax_tour opt WHERE opt.booking_code LIKE '%".$booking_code."%' ";
        $res = db_query($sql);
        $list = [];
        while ($res && $row = db_fetch_array($res)) {
            $list[] = $row['tour_id'];
        }
        return $list;
    }


    public static function getLeaders()
    {
        $sql = "SELECT DISTINCT id, `value` as name_tl FROM ".LIST_ITEM_TABLE." li WHERE list_id = ".TOUR_LEADER_LIST." ORDER BY name_tl";
        return db_query($sql);
    }

    public static function getTransitAirport()
    {
        $sql = "SELECT DISTINCT id, `value` as transit_airport FROM " . LIST_ITEM_TABLE . " li WHERE list_id = ".TRANSIT_AIRPORT." ORDER BY transit_airport";
        return db_query($sql);
    }

}

class VisaResult {
    private static $_result = [
        0 => 'N/A',
        1 => 'Passed',
        2 => 'Failed',
        3 => 'Passed not go',
    ];

    public static function getResult($number) {
        if (!isset(self::$_result[ $number ]) || !self::$_result[ $number ]) return null;
        return self::$_result[ $number ];
    }

    public static function getAll() {
        return self::$_result;
    }
}

class TourStatus extends GeneralObject {
    const Enable = 1; //tour mở
    const Disable = 2; // tour đóng
}

class ListOfColors extends GeneralObject {
    const COLOR_hex007bff = '#007bff';
    const COLOR_hex17a2b8 = '#17a2b8';
    const COLOR_hex28a745 = '#28a745';
    const COLOR_hexffc107 = '#ffc107';
    const COLOR_hexdc3545 = '#dc3545';
    const COLOR_hex3b4866 = '#3b4866';
    const COLOR_hex2c514e = '#2c514e';
    const COLOR_hex5b7a3f = '#5b7a3f';
    const COLOR_hex564370 = '#564370';
    const COLOR_hex34373d = '#34373d';
    const COLOR_hex57663b = '#57663b';
    const COLOR_hex51462c = '#51462c';
    const COLOR_hex7a3f69 = '#7a3f69';

}

class TourHistoryModel extends VerySimpleModel
{
    static $meta = [
        'table' => OST_TOUR_HISTORY,
        'pk' => ['id'],
    ];
}

class TourHistory extends TourHistoryModel
{

}

class PaxTourHistoryModel extends VerySimpleModel
{
    static $meta = [
        'table' => OST_PAX_TOUR_HISTORY_TABLE,
        'pk' => ['id'],
    ];
}

class PaxTourHistory extends PaxTourHistoryModel
{

}
//tour - flight ticket
class TourNewModel extends VerySimpleModel
{
    static $meta = [
        'table' => TOUR_NEW_TABLE,
        'pk' => ['id'],
    ];
}

class TourNew extends TourNewModel {
    const REQUEST_EDIT_TOUR = 1;
    const APPROVAL_EDIT_TOUR = 2;
    public static function insert_to_table($data) {
        if(!is_array($data)) return null;
        $newkeys = array('id', 'name', 'destination', 'tour_leader_id' , 'gateway_id', 'gateway_present_time',
            'return_time', 'pax_quantity', 'net_price', 'retail_price', 'transit_airport_id', 'airline_id',
            'country_id', 'sure_quantity', 'hold_quantity', 'created_at', 'created_by', 'updated_at', 'updated_by',
            'tl_quantity', 'log_id_ost_tour', 'status', 'departure_date');
        $tour_old = Tour::lookup($data['id']);
        $flight_ticket_old = [
            'departure_flight_code' => $tour_old->departure_flight_code,
            'return_flight_code' => $tour_old->return_flight_code,
            'arrival_time' => $tour_old->arrival_time,
            'departure_flight_code_b' => $tour_old->departure_flight_code_b,
            'return_flight_code_b' => $tour_old->return_flight_code_b,
            'arrival_time_b' => $tour_old->arrival_time_b,
            'note' => $tour_old->notes,
        ];
        $note_old = json_encode($flight_ticket_old);
        $data_news = self::newKeys($data, $newkeys);
        $mer_data = array_merge($data_news, [
            "note" => $note_old,
        ]);
        foreach ($mer_data as $key => $value){
            $fields[] = "`$key` = ".db_input($value);
        }
        $sql = 'INSERT INTO '.static::$meta['table'];
        $sql .= ' SET '.implode(', ', $fields);
        $res = db_query($sql);
        if(!isset($res)) return null;
        return true;
    }

    public static function newKeys($existing, $newkeys){
        if(!is_array($existing)) return false;
        $data = array();
        $i = 0 ;
        foreach( $existing as $k=>$v){
            $data[$newkeys[$i]] = $v;
            $i++;
        }
        return $data;
    }
    public static function getPagination(array $params, $offset, &$total, $limit = PAGE_LIMIT) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = "  case when date(gateway_present_time) > NOW() then 0 when date(gateway_present_time) <= NOW() then 1 end,
         case when date(gateway_present_time) > NOW() then gateway_present_time end asc,
         case when date(gateway_present_time) <= NOW() then gateway_present_time end desc ";
        $where = "  ";
        $total = 0;

        if (isset($params['group_des'])) {
            if(is_array($params['group_des'])) {
                $params['group_des'] = array_unique(array_filter($params['group_des']));
                if(count($params['group_des']) > 0) {
                    if(isset($params['tour_not_des']) && $params['tour_not_des'] === 1)
                        $where .= " AND ( destination IN (" . implode(",", $params['group_des']) . ") 
                            OR destination = 0 OR destination is null ) "; // allow view tour NOT destination
                    else
                        $where .= " AND destination IN (" . implode(",", $params['group_des']) . ") ";
                } else {
                    $where .= " AND 1 = 0 ";
                }
            } elseif (is_numeric(trim($params['group_des']))) {
                $where .= " AND destination =". db_input((int)$params['group_des']) . " ";
            }
        }

        if (isset($params['tour'])) {
            if(is_array($params['tour'])) {
                $params['group_des'] = array_unique(array_filter($params['tour']));
                if(count($params['tour']) > 0) {
                    if(isset($params['tour_not_des']) && $params['tour_not_des'] === 1)
                        $where .= " AND ( destination IN (" . implode(",", $params['tour']) . ") 
                            OR destination = 0 OR destination is null ) "; // allow view tour NOT destination
                    else
                        $where .= " AND destination IN (" . implode(",", $params['tour']) . ") ";
                } else {
                    $where .= " AND 1 = 0 ";
                }
            } elseif (is_numeric(trim($params['tour']))) {
                $where .= " AND destination =". db_input((int)$params['tour']) . " ";
            }
        }

        if (isset($params['booking_type_id'])) {
            if(is_array($params['booking_type_id'])) {
                $params['booking_type_id'] = array_unique(array_filter($params['booking_type_id']));
                if(count($params['booking_type_id']) > 0) {
                    if(isset($params['tour_not_des']) && $params['tour_not_des'] === 1)
                        $where .= " AND ( destination IN (" . implode(",", $params['booking_type_id']) . ") 
                            OR destination = 0 OR destination is null ) "; // allow view tour NOT destination
                    else
                        $where .= " AND destination IN (" . implode(",", $params['booking_type_id']) . ") ";
                } else {
                    $where .= " AND 1 = 0 ";
                }
            } elseif (is_numeric(trim($params['booking_type_id']))) {
                $where .= " AND destination =". db_input((int)$params['booking_type_id']) . " ";
            }
        }

        if (isset($params['tour_name']) && !empty($params['tour_name']))
            $where .= " AND t.name LIKE " . db_input('%' . ($params['tour_name']) . '%') . " ";

        if(isset($params['tour_status']) && is_numeric($params['tour_status']) && $params['tour_status']) {
            $where .= " AND t.status = ".db_input($params['tour_status']);
        }else{
            $where .= " AND t.status = ".TourStatus::Enable."";
        }

        if($params['customer_name']){
           $tour_id = VisaTour::searchNamePax($params['customer_name']);
            if($tour_id)
                $where .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $where .= " AND 1 = 0 ";
        }

        if($params['passport_no']){
            $tour_id = VisaTour::searchPassport($params['passport_no']);
            if($tour_id)
                $where .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $where .= " AND 1 = 0 ";
        }

        if($params['customer_phone_number']){
            $tour_id = VisaTour::searchNumberPhone($params['customer_phone_number']);
            if($tour_id)
                $where .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $where .= " AND 1 = 0 ";
        }

        if (isset($params['from']) && strtotime($params['from'])) {
            $from = trim($params['from']);
            $where .= " AND date(gateway_present_time) >= ".db_input($from)." ";
        }

        if (isset($params['to']) && strtotime($params['to'])) {
            $to = trim($params['to']);
            $where .= " AND date(gateway_present_time) <= ".db_input($to)." ";
        }

        if($params['booking_code']){
            $tour_id = Tour::sreachBookingCode($params['booking_code']);
            if($tour_id)
                $where .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $where .= " AND 1 = 0 ";
        }

        if($params['flight_ticket_code']){
            $tour_id = FlightTicketOP::searchTourFromCode($params['flight_ticket_code']);
            if($tour_id)
                $where .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $where .= " AND 1 = 0 ";
        }

        if($params['flight_code']){
            $tour_id = FlightOP::searchTourFromCode($params['flight_code']);
            if($tour_id)
                $where .= " AND ( t.id IN "."('".implode("','",$tour_id)."') ";
            else
                $where .= " AND ( 1 = 0 ";

            $where .= " OR t.note like ".db_input('%'.$params['flight_code'].'%') .") ";
        }

        $sql = " SELECT ";

        $sql .= " ( SELECT count(*) FROM  ".BOOKING_RESERVATION_TABLE. " as b 
                    WHERE t.id = b.tour_id and date(b.due_at) > current_date()
                        and (b.hold_qty > 0 or b.sure_qty > 0) 
                    ) as hold_qty_current, ";

        if (isset($params['join_account_general']) && $params['join_account_general']) {
            $sql .= " t.name, t.id, t.gateway_present_time as gather_date, 
                t.airline_id as airline, 
                 a.*   FROM " .static::$meta['table']." t ";
            $sql .= " LEFT JOIN ".ACC_GENERAL." a ON t.id=a.tour_id";
        } else {
            $sql .= " t.* FROM " .static::$meta['table']." t ";
        }

        $sql .= " WHERE 1 $where ";

        $sql_count = "SELECT COUNT(*) as total FROM ($sql) as A";
        $res_total = db_query($sql_count);

        if ($res_total) {
            $total_row = db_fetch_array($res_total);
            if ($total_row && isset($total_row['total'])) {
                $total = $total_row['total'];
            }
        }
        $res = db_query("$sql ORDER BY $order $limit");
        return $res;
    }

    public static function countHoldCurrent($tour_id){
        $sql = "SELECT count(*) as 'num_hold' FROM  ".BOOKING_RESERVATION_TABLE. " 
                    WHERE tour_id = ".(int)$tour_id." and date(due_at) > current_date() and (hold_qty > 0 or sure_qty > 0) ";
        $res = db_query($sql);
        if($res) {
            $number = db_fetch_array($res);
            return (int)$number['num_hold'];
        };
        return 0;
    }

    public static function getPaginationReservation(array $params, $offset, $limit = 30, &$total) {
        $sql = "SELECT t.*, t.color_code as `color_codes`, t.gateway_present_time as `gather_date`, t.pax_quantity as `quantity`,
                t.tour_leader_id as `tour_leader`
                FROM ".TOUR_NEW_TABLE." t ";

        if (isset($params['join_account_general']) && $params['join_account_general']) {
            $sql .= " LEFT JOIN ".ACC_GENERAL." a ON t.id=a.tour_id";
        }
        $sql .= " WHERE 1 ";

        $limit = " LIMIT $limit OFFSET $offset ";

        if (isset($params['tour_name']) && $params['tour_name']) {
            $sql .= " AND t.name LIKE ".db_input('%'.trim($params['tour_name']).'%')." ";
        }

        if (isset($params['tour']) && is_numeric($params['tour']) && $params['tour']) {
            $tour = (int)trim($params['tour']);
            $sql .= " AND destination = ".db_input($tour)." ";
        } elseif (is_array($params['tour'])) {
            $params['tour'] = array_filter($params['tour']);
            $params['tour'] = array_unique($params['tour']);
            if (count($params['tour'])) {
                $sql .= " AND destination IN (" . implode(',', $params['tour']) . ") ";
            }
        }

        if($params['number_phone']){
            $tour_id = VisaTour::searchNumberPhone($params['number_phone']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['passport_no']){
            $tour_id = VisaTour::searchPassport($params['passport_no']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if($params['name_pax']){
            $tour_id = VisaTour::searchNamePax($params['name_pax']);
            if($tour_id)
                $sql .= " AND t.id IN "."('".implode("','",$tour_id)."')";
            else
                $sql .= " AND 1 = 0 ";
        }

        if (isset($params['airline']) && is_numeric($params['airline']) && $params['airline']) {
            $airline = (int)trim($params['airline']);
            $sql .= " AND airline_id = ".db_input($airline)." ";
        }

        if (isset($params['from']) && strtotime($params['from'])) {
            $from = trim($params['from']);
            $sql .= " AND date(gateway_present_time) >= ".db_input($from)." ";
        }

        if (isset($params['to']) && strtotime($params['to'])) {
            $to = trim($params['to']);
            $sql .= " AND date(gateway_present_time) <= ".db_input($to)." ";
        }

        if(isset($params['tour_status']) && is_numeric($params['tour_status']) && $params['tour_status']) {
            $sql .= " AND status = ".db_input($params['tour_status']);
        }else{
            $sql .= " AND status = ".TourStatus::Enable."";
        }

        if (isset($params['country']) && is_numeric($params['country']) && $params['country']){
            $country_id = (int)trim($params['country']);
            $sql .= " AND country_id = ". $country_id. " ";
        }

        if (isset($params['destination']) && is_numeric($params['destination']) && $params['destination']){
            $destination_id = (int)$params['destination'];
            $sql .= " AND t.destination = ". $destination_id. " AND t.departure_date > now() ";
        }

        $reservation_ids = null;
        include_once INCLUDE_DIR.'class.booking.php';
        if (isset($params['reservation_id']) && is_numeric($params['reservation_id']) && $params['reservation_id']) {
            $item = BookingReservation::lookup($params['reservation_id']);

            if (!$item || !(isset($item->tour_id) && $item->tour_id)) {
                $sql .= ' AND 1=0 ';
            } else {
                $sql .= ' AND id='.db_input($item->tour_id). ' ';
            }
        }

        if (isset($params['customer_name']) && !empty(trim($params['customer_name']))) {
            $reservations = BookingReservation::objects()->filter(['customer_name__like' => '%'.trim($params['customer_name']).'%'])->all();

            if (is_null($reservation_ids)) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if (isset($params['customer_phonenumber']) && !empty(trim($params['customer_phonenumber']))) {
            $reservations = BookingReservation::objects()->filter(['phone_number__like' => '%'.trim($params['customer_phonenumber']).'%'])->all();

            if (!$reservation_ids) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if(isset($params['staff_id']) && is_numeric($params['staff_id']) && $params['staff_id']){
            $staff_id = (int)trim($params['staff_id']);
            $reservations = BookingReservation::objects()->filter(['staff_id' => $staff_id])->all();
            if (is_null($reservation_ids)) $reservation_ids = [];
            foreach ($reservations as $row) {
                if ($row->tour_id)
                    $reservation_ids[] = $row->tour_id;
            }
        }

        if (is_array($reservation_ids)) {
            $reservation_ids = array_filter($reservation_ids);
            $reservation_ids = array_unique($reservation_ids);
        }

        if (is_array($reservation_ids)) {
            if (count($reservation_ids))
                $sql .= " AND id IN (".implode(',', $reservation_ids).") ";
            else
                $sql .= " AND 1=0 ";
        }

        $order = "  case when date(gateway_present_time) > NOW() then 0 when date(gateway_present_time) <= NOW() then 1 end,
         case when date(gateway_present_time) > NOW() then gateway_present_time end asc,
         case when date(gateway_present_time) <= NOW() then gateway_present_time end desc ";

        $res = db_query("$sql ORDER BY $order $limit");
        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function updateQuantity($tour_id) {
        if (!$tour_id) return null;

        try {
            $sql = "SELECT sum(sure_qty) as sure, sum(hold_qty) as hold FROM ".BOOKING_RESERVATION_TABLE." WHERE tour_id=".db_input($tour_id).
                " AND (visa_only IS NULL OR visa_only=0) 
                 AND status <> ".BookingReservation::BOOKING_RESERVATION_DISABLE."
                AND (isoverdue=0 OR isoverdue IS NULL)";
            $res = db_query($sql);
            $hold = 0;
            $sure = 0;
            if ($res) {
                $row = db_fetch_array($res);
                if ($row) {
                    $hold = $row['hold'] ?: 0;
                    $sure = $row['sure'] ?: 0;
                }
            }

            if ( $tour = TourNew::lookup($tour_id) ) {
                $tour->set('hold_quantity', $hold);
                $tour->set('sure_quantity', $sure);
                $tour->save();
            }

            return true;
        } catch (Exception $ex) {return false;}
    }

    public static function getAllFlightTicketOP($tour_id) {
        if(empty($tour_id)) return null;
        $sql = "SELECT ft.id as id_flight_ticket, ft.flight_ticket_code, 
                f.id as flight_id, f.flight_code, f.departure_at, f.airport_code_from, airport_code_to, f.arrival_at
                FROM ".FLIGHT_TICKET_TOUR_TABLE." ftt
            LEFT JOIN ".FLIGHT_TICKET_TABLE." ft ON ft.id = ftt.flight_ticket_id
            LEFT JOIN ".FLIGHT_TICKET_FLIGHT_OP_TABLE." ftf ON ftf.flight_ticket_id = ft.id 
            LEFT JOIN ".FLIGHT_TABLE." f ON f.id = ftf.flight_id
            WHERE ftt.tour_id =".db_input($tour_id);
        return db_query($sql);
    }

    public static function getBookingTypeName($booking_type_id)
    {
        $sql = "SELECT name FROM booking_type_list WHERE id=".db_input($booking_type_id);
        $res = db_query($sql);
        if(!isset($res)) return null;
        $row = db_fetch_array($res);
        if(!isset($row)) return null;
        return $row['name'];
    }

    public static function getAllLeaders()
    {
        $sql = "SELECT DISTINCT id, `value` as name_tl FROM ".LIST_ITEM_TABLE." li WHERE list_id = ".TOUR_LEADER_LIST." ORDER BY name_tl";
        return db_query($sql);
    }

    public static function getAllBookingType()
    {
        $sql = "SELECT DISTINCT id, `name` as name_booking_type FROM booking_type_list ORDER BY name_booking_type";
        return db_query($sql);
    }

    public static function getAllTransitAirport()
    {
        $sql = "SELECT DISTINCT id, `value` as transit_airport FROM " . LIST_ITEM_TABLE . " li WHERE list_id = ".TRANSIT_AIRPORT." ORDER BY transit_airport";
        return db_query($sql);
    }

    public static function getAllDepartureFlight()
    {
        $sql = "SELECT DISTINCT id, `value` as departure_flight FROM " . LIST_ITEM_TABLE . " li WHERE list_id = ".GATEWAY_LIST."";
        return db_query($sql);
    }

    public static function getAllAirline()
    {
        $sql = "SELECT DISTINCT id, `value` as airline_name FROM " . LIST_ITEM_TABLE . " li WHERE list_id = ".AIRLINE_LIST."";
        return db_query($sql);
    }

    public static function countPax($tour_id) {
        $sql = "select count(distinct  pax_id) as total FROM ost_pax_tour where tour_id=".db_input((int)$tour_id);
        $res = db_query($sql);
        if (!$res) return 0;
        $data = db_fetch_array($res);
        if (!$data || !isset($data['total'])) return 0;
        return (int)$data['total'];
    }

}

class TourExportLogModel extends VerySimpleModel
{
    static $meta = [
        'table' => TOUR_EXPORT_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class TourExportLog extends TourExportLogModel
{
}
class TourNetPriceModel extends VerySimpleModel 
{
    static $meta = [
        'table' => TOUR_NET_PRICE_TABLE,
        'pk' => ['id'],
    ];
}
class TourNetPrice extends TourNetPriceModel {
    public static function get_tour_net_price($tour_id)
    {
        $sql = "SELECT * FROM " .static::$meta['table']."  WHERE tour_id =".db_input($tour_id)." ORDER BY quantity ASC";
        return db_query($sql);
    }

    public static function is_array_empty($arr){
        if(is_array($arr)){     
            foreach($arr as $key => $value){
                if(!empty($value) || $value != NULL || $value != ""){
                    return true;
                    break;
                }
            }
            return false;
        }
      }
    public static function save_tour_net_price($arr_data, $tour_id) {
        global $thisstaff;
        $_arr_data = self::is_array_empty($arr_data);
        if($_arr_data === false) return false;
        $all_values = [];
        $info = ['"'.date('Y-m-d H:i:s').'"', '"'.$thisstaff->getId().'"', '"'.(int)$tour_id.'"'];
        $query = "INSERT INTO ".TOUR_NET_PRICE_TABLE." (quantity, retail_price, created_at, created_by, tour_id) VALUES ";

        foreach($arr_data as $key) {
            $row_values = [];
            foreach($key as $s_key => $s_value) {
                $row_values[] = '"'.$s_value.'"';
            }
            $data = array_merge($row_values, $info);
            $all_values[] = '('.implode(',', $data).')';
        }
        //Implode all rows
        $query .= implode(',', $all_values);
        $res = db_query($query);
        if(!$res) return false;
        return $res;
    }

    public static function max_quantity($tour_id)
    {
        $sql = "SELECT MAX(quantity) as max_quantity FROM " .static::$meta['table']."  WHERE tour_id =".db_input($tour_id)."";
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if ($row && isset($row['max_quantity']))
            return $row['max_quantity'];
    }

    public static function size_tour_retail_price($tour_id, $pax_quantity = 0){
        if(!isset($tour_id)) return null;
        $tour = TourNew::lookup($tour_id);
        if (!$tour) return null;
        $sql = " SELECT retail_price FROM " .static::$meta['table']." WHERE tour_id =".db_input($tour_id)." ORDER BY quantity ASC";
        $res = db_query($sql);
        while ($res && $row = db_fetch_array($res)) {
            if($pax_quantity <= $row['quantity']){
                return $row['retail_price'];
                break;
            }else {
                return self::max_retail_price($tour_id);
                break;
            }
        }
    }
    public static function max_retail_price($tour_id){
        $sql = "SELECT MAX(quantity) FROM ".static::$meta['table']." WHERE tour_id=".db_input($tour_id);
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if ($row && isset($row['retail_price']))
            return $row['retail_price'];
    }
}

<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class ObjectEventModel extends \VerySimpleModel {
    static $meta = [
        'table' => OBJECT_EVENT_TABLE,
        'pk' => ['id'],
    ];
}

class ObjectEvent extends ObjectEventModel {

}

class AutoActionContentModel extends \VerySimpleModel {
    static $meta = [
        'table' => AUTO_ACTION_CONTENT_TABLE,
        'pk' => ['id'],
    ];
}

class AutoActionContent extends AutoActionContentModel {
    public static function getPagination(array $params, $offset, &$total, $limit = PAGE_LIMIT) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY i.name ASC ";
        $where = "  ";
        $total = 0;

        if (isset($params['name']) && !empty($params['name']))
            $where .= " AND name LIKE ".db_input('%'.$params['name'].'%')." ";

        if (isset($params['description']) && !empty($params['description']))
            $where .= " AND description LIKE ".db_input('%'.$params['description'].'%')." ";

        if (isset($params['sms_content']) && !empty($params['sms_content']))
            $where .= " AND sms_content LIKE ".db_input('%'.$params['sms_content'].'%')." ";

        if (isset($params['notification_title']) && !empty($params['notification_title']))
            $where .= " AND notification_title LIKE ".db_input('%'.$params['notification_title'].'%')." ";

        if (isset($params['notification_content']) && !empty($params['notification_content']))
            $where .= " AND notification_content LIKE ".db_input('%'.$params['notification_content'].'%')." ";

        if (isset($params['notification_url']) && !empty($params['notification_url']))
            $where .= " AND notification_url LIKE ".db_input('%'.$params['notification_url'].'%')." ";

        if (isset($params['email_subject']) && !empty($params['email_subject']))
            $where .= " AND email_subject LIKE ".db_input('%'.$params['email_subject'].'%')." ";

        if (isset($params['email_content']) && !empty($params['email_content']))
            $where .= " AND email_content LIKE ".db_input('%'.$params['email_content'].'%')." ";

        $sql = " SELECT id, name, description,
            send_after, trigger_id,
            status, type, schedule
            FROM " .static::$meta['table']." i WHERE 1 $where $order";

        $sql_count = "SELECT COUNT(*) as total FROM ($sql) as A";
        $res_total = db_query($sql_count);

        if ($res_total) {
            $total_row = db_fetch_array($res_total);
            if ($total_row && isset($total_row['total'])) {
                $total = $total_row['total'];
            }
        }
        $res = db_query("$sql $limit");
        return $res;
    }

    public static function getAvailableSMS() {
        return static::_get_available();
    }

    private static function _get_available() {
        $recursion_hour = (int)date('H');
        $recursion_minute = (int)date('i');

        if (defined('DEV_ENV') && DEV_ENV === 1)
            $recursion_minute = 0;

        $recursion_date_in_month = date('j');
        $recursion_day_of_week = date('w');
        $recursion_month = date('n');

        $sql = "SELECT * FROM ".static::$meta['table']." WHERE 1
            AND status = 1
            AND (
                schedule = 'recursion'
                
                AND (find_in_set('".$recursion_hour."', recursion_hour) 
                    or recursion_hour like '-1' 
                    or recursion_hour is null)
                    
                AND (find_in_set('".$recursion_minute."', recursion_minute) 
                    or recursion_minute like '-1' 
                    or recursion_minute is null)
                    
                AND (find_in_set('".$recursion_date_in_month."', recursion_date_in_month) 
                    or recursion_date_in_month like '-1' 
                    or recursion_date_in_month is null)
                    
                AND (find_in_set('".$recursion_day_of_week."', recursion_day_of_week) 
                    or recursion_day_of_week like '-1' 
                    or recursion_day_of_week is null)
                    
                AND (find_in_set('".$recursion_month."', recursion_month) 
                    or recursion_month like '-1' 
                    or recursion_month is null)
            )";

        return db_query($sql);
    }
}


class AutoActionContentLogModel extends \VerySimpleModel {
    static $meta = [
        'table' => AUTO_ACTION_CONTENT_LOG_TABLE,
        'pk' => ['id'],
    ];
}

class AutoActionContentLog extends AutoActionContentLogModel {

}

class AutoActionCampaignModel extends \VerySimpleModel {
    static $meta = [
        'table' => AUTO_ACTION_CAMPAIGN_TABLE,
        'pk' => ['id'],
    ];
}

class AutoActionCampaign extends AutoActionCampaignModel {
    public static function createIfNotExists($type, $value) {
        $type = trim($type);
        $value = trim($value);

        $data = [
            'type' => $type,
            'value' => $value,
        ];
        $campaign = static::lookup($data);
        if (!$campaign) {
            $campaign = static::create($data);
            return $campaign->save();
        }

        return $campaign->id;
    }

    public static function fetchAll() {
        $sql = "SELECT * FROM ".static::$meta['table']." order by value";
        $res = db_query($sql);
        if (!$res) return [];
        $data = [];
        while($res && ($row = db_fetch_array($res))) {
            if (!isset($data[ $row['type'] ]))
                $data[ $row['type'] ] = array();

            $data[ $row['type'] ][ $row['id'] ] = $row['value'];
        }

        return $data;
    }
}

<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.export.php');
global $thisstaff;

$depts = array_filter( $thisstaff->getDepts() );
$teams = array_filter( $thisstaff->getTeams() );

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'ticket_history':
            $days = isset($_REQUEST['days']) ? intval($_REQUEST['days']) : 7;
            $ts = strftime('%Y%m%d');
            $where = 'AND 
                            (
                            t.dept_id IN ('.($depts ? implode(',', db_input($depts)) : 0).')
                            '.($teams && count($teams) ? 'OR t.team_id IN ('. implode(',', db_input($teams)).')' : '')  .'
                            OR t.staff_id='.db_input($thisstaff->getId())
                .' )';
            $sql = "SELECT DISTINCT
                      t.ticket_id,
                      t.number,
                      t.created,
                      s.username  AS assigned,
                      s2.username AS updater,
                      xx.body,
                      xx.created  AS updated,
                      st.name,
                      t.isoverdue,
                      c.subject,
                      SUBSTRING(
                            body,
                            LOCATE('[[', body)+2,
                            LOCATE(']]', body) -
                            LOCATE('[[', body) - 2
                        ) as id
                    FROM ost_ticket t LEFT JOIN (SELECT
                                                   tr.ticket_id,
                                                   tr.created,
                                                   tr.body,
                                                   tr.staff_id
                                                 FROM ost_ticket_thread tr
                                                   JOIN (SELECT
                                                           tx.ticket_id,
                                                           min(tx.created) AS ctime
                                                         FROM ost_ticket_thread tx
                                                         WHERE tx.created >= DATE_SUB(NOW(), INTERVAL $days DAY)
                                                         GROUP BY tx.ticket_id) AS x
                                                     ON tr.ticket_id = x.ticket_id AND tr.created = x.ctime
                                                 WHERE tr.created >= DATE_SUB(NOW(), INTERVAL $days DAY)
                                                ) AS xx ON t.ticket_id = xx.ticket_id
                      LEFT JOIN ost_ticket_status st ON st.id = t.status_id
                      LEFT JOIN ost_staff s ON s.staff_id = t.staff_id
                      LEFT JOIN ost_staff s2 ON s2.staff_id = xx.staff_id
                      LEFT JOIN ost_ticket__cdata c ON t.ticket_id = c.ticket_id
                    WHERE 1 AND xx.created >= DATE_SUB(NOW(), INTERVAL $days DAY)
                    $where
                    GROUP BY t.ticket_id
                    ORDER BY xx.created
                    ";

            if (!$thisstaff || !$thisstaff->can_export_tickets())
                $errors['err'] = __('Only staff can export');
            elseif (!Export::saveTicketHistory($sql, "tickets-history-$ts.csv", 'csv'))
                $errors['err'] = __('Internal error: Unable to dump query results');

            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
}

$page='reports.inc.php';
$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.system_logs" />',
    "$('#content').data('tipNamespace', 'dashboard.system_logs');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

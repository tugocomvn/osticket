<?php
require('staff.inc.php');
use PHPExcel_Settings;

require_once(INCLUDE_DIR.'class.list.php');
require_once(INCLUDE_DIR.'class.pax.php');
include_once INCLUDE_DIR.'data/country.list.php';
include_once INCLUDE_DIR.'data/roomtype.list.php';
include_once(INCLUDE_DIR.'class.qrcode.php');
require_once INCLUDE_DIR.'class.tour-destination.php';
require_once INCLUDE_DIR.'class.passport.php';

global $thisstaff, $cfg;

$referer = isset($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'], '/scp/tour-guest.php') !== false || strpos($_SERVER['HTTP_REFERER'], '/scp/operator.php') !== false)
    ? $_SERVER['HTTP_REFERER'] : '/scp/tour-list.php';

if (!isset($_REQUEST['tour']) || !(int)$_REQUEST['tour']) {
    header('Location: '.$referer);
    exit;
}

$country_list = \Tugo\Data\Country::getList();
$roomtype_list = \Tugo\Data\RoomType::getList();


$display_info_list = [
    'airline',
    'gather_date',
    'departure_date',
    'return_date',
    'departure_flight_code',
    'return_flight_code',
    'tour_leader',
    'transit_airport',
    'departure_flight_code_b',
    'return_flight_code_b',
    'arrival_time',
    'arrival_time_b',
    'notes',
];

function ___save_pax_data($tour_id, $data, $index = 0) {
    global $thisstaff;
    $pax = $pax_info = $pax_tour = null;
    $pax_info = PaxInfo::lookup(['passport_no' => $data['passport_no']]);
    if ($pax_info) // tìm khách theo số passport
        $pax = Pax::lookup($pax_info->pax_id);

    if ($pax) { // cập nhật khách theo thông tin mới
        $_data = [
            'full_name' => trim($data['full_name']),
            'dob' => trim($data['dob']),
            'gender' => trim($data['gender']),
            'updated_at' => new SqlFunction('NOW'),
        ];
        $pax->setAll($_data);
        $pax->save();

    } else {
        $pax = Pax::lookup( // tìm khách theo thông tin nhập vào
            [
                'full_name' => $data['full_name'],
                'dob' => $data['dob'],
                'gender' => $data['gender']
            ]
        );

        if (!$pax) {
            $_data = [
                'full_name' => trim($data['full_name']),
                'dob' => trim($data['dob']),
                'gender' => trim($data['gender']),
                'created_at' => new SqlFunction('NOW'),
            ];
            $pax = Pax::create($_data);
            $pax->save(true);
        } // end if

        if (!$pax) throw new Exception('Lỗi tạo khách');
    } // end pax check

    if (!$pax_info) {
        $_data = [
            'pax_id' => $pax->id,
            'passport_no' => trim($data['passport_no']),
            'doe' => trim($data['doe']),
            'nationality' => trim($data['nationality']),
            'created_at' => new SqlFunction('NOW'),
        ];
        $pax_info = PaxInfo::create($_data);
        $pax_info->save(true);
    } else {
        $_data = [
            'pax_id' => $pax->id,
            'passport_no' => trim($data['passport_no']),
            'doe' => trim($data['doe']),
            'nationality' => trim($data['nationality']),
            'updated_at' => new SqlFunction('NOW'),
        ];
        $pax_info->setAll($_data);
        $pax_info->save(true);
    }// end elseif

    if (!$pax_info) throw new Exception('Lỗi tạo thông tin khách');

    $pax_tour = PaxTour::lookup(
        [
            'pax_id' => $pax->id,
            'tour_id' => $tour_id,
            'pax_info' => $pax_info->id,
        ]
    );

    if (!$pax_tour) {
        $pax_tour = PaxTour::lookup(
            [
                'pax_id' => $pax->id,
                'tour_id' => $tour_id,
            ]
        );
    }

    if (!$pax_tour) {
        $pax_tour = PaxTour::lookup(
            [
                'pax_id' => $pax->id,
                'tour_id' => $tour_id,
                'pax_info' => $pax_info->id,
                'booking_code' => $data['booking_code'],
                'phone_number' => $data['phone_number'],
            ]
        );
    } // end else

    if (isset($data['visa_result']) && (int)trim($data['visa_result']))
        $data['visa_result'] = (int)trim($data['visa_result']);
    else
        $data['visa_result'] = 0;

    $_data = [
        'pax_id' => $pax->id,
        'tour_id' => $tour_id,
        'pax_info' => $pax_info->id,
        'booking_code' => trim($data['booking_code']),
        'room' => trim($data['room']),
        'room_type' => trim($data['room_type']),
        'phone_number' => trim($data['phone_number']),
        'note' => trim($data['note']),
        'receiving_date' => !empty($data['receiving_date']) && $data['receiving_date'] !== '0000-00-00 00:00:00' && !is_null($data['receiving_date']) ? $data['receiving_date'] : null,
        'apply_date' => !empty($data['apply_date']) && $data['apply_date'] !== '0000-00-00 00:00:00' && !is_null($data['apply_date']) ? $data['apply_date'] : null,
        'visa_estimated_due_date' => !empty($data['visa_estimated_due_date']) && $data['visa_estimated_due_date'] !== '0000-00-00 00:00:00' && !is_null($data['visa_estimated_due_date']) ? $data['visa_estimated_due_date'] : null,
        'visa_result' => $data['visa_result'],
        'tl' => isset($data['tl']) && $data['tl'] ? 1 : 0,
        'order' => (int)$index,
    ];

    if (!$pax_tour) {
        $_data['created_at'] = new SqlFunction('NOW');
        $pax_tour = PaxTour::create($_data);
        $id = $pax_tour->save(true);
    } else {
        $_data['updated_at'] = new SqlFunction('NOW');
        $pax_tour->setAll($_data);
        $id = $pax_tour->save();
    }// end if
    if (!$pax_tour) throw new Exception('Lỗi tạo thông tin khách đi tour');
    $log = PaxTourHistory::create(
        [
            'ost_pax_tour_history_id' => $id,
            'snapshot' => json_encode($pax_tour->ht),
            'time' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId(),
        ]
    );
    $log_id = $log->save();
    $pax_tour->set('log_id_ost_pax_tour', $log_id);
    $pax_tour->save(true);
} // end function

function ___validate_data(&$list, $type = false) {
    $date_format = [
        'd-m-Y',
        'j-m-Y',
        'd-n-Y',
        'j-n-Y',
        'd/m/Y',
        'j/m/Y',
        'd/n/Y',
        'j/n/Y',
        'd-m-y',
        'j-m-y',
        'd-n-y',
        'j-n-y',
        'd/m/y',
        'j/m/y',
        'd/n/y',
        'j/n/y',
    ];

    $easy = 'upload_booking' === $type;

    foreach ($list as $index => $data) {
        $index_ = $index+1;
        if (!$easy) {
            foreach ($list as $_index => $_data) {
                $_index_ = $_index+1;
                if ($index == $_index) continue;

                if ($data['passport_no'] == $_data['passport_no'])
                    throw new Exception(
                        'Trùng số hộ chiếu: '.$data['passport_no']." - Dòng $index_ và $_index_"
                    );
            }
        }

        if (!empty($data['phone_number'])) {
            $list[$index]['phone_number'] = str_replace(
                " ",
                "",
                trim(trim($data['phone_number']), "'\"")
            );
        }

        $check_date_flags = false;
        foreach ($date_format as $_format) {
            if (date_create_from_format($_format, $data['doe'])) {
                $list[$index]['doe']
                    = date_create_from_format($_format, $data['doe'])->format('Y-m-d');
                $check_date_flags = true;
            }
        }
        if (!$check_date_flags && !$easy)
            throw new Exception('Dòng '.($index+1)
                                .': Ngày hết hạn không đúng định dạng "ngày/tháng/năm":'. $data['doe']);
        //////////
        $check_date_flags = false;
        foreach ($date_format as $_format) {
            if (date_create_from_format($_format, $data['dob'])) {
                $list[$index]['dob']
                    = date_create_from_format($_format, $data['dob'])->format('Y-m-d');
                $check_date_flags = true;
            }
        }
        if (!$check_date_flags && !$easy)
            throw new Exception('Dòng '.($index+1)
                                .': Ngày sinh không đúng định dạng "ngày/tháng/năm":'. $data['dob']);
        //////////
        $check_date_flags = false;
        foreach ($date_format as $_format) {
            if (date_create_from_format($_format, $data['receiving_date']) && !empty(trim($data['receiving_date']))) {
                $list[$index]['receiving_date']
                    = date_create_from_format($_format, $data['receiving_date'])->format('Y-m-d');
                $check_date_flags = true;
            } elseif (empty(trim($data['receiving_date']))) {
                $list[$index]['receiving_date'] = null;
                $check_date_flags = true;
            }
        }
        if (!$check_date_flags && !$easy)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày nhận không đúng định dạng "ngày/tháng/năm":'. $data['receiving_date']
            );
        //////////
        $check_date_flags = false;
        foreach ($date_format as $_format) {
            if (date_create_from_format($_format, $data['apply_date']) && !empty(trim($data['apply_date']))) {
                $list[$index]['apply_date']
                    = date_create_from_format($_format, $data['apply_date'])->format('Y-m-d');
                $check_date_flags = true;
            } elseif (empty(trim($data['apply_date']))) {
                $list[$index]['apply_date'] = null;
                $check_date_flags = true;
            }
        }
        if (!$check_date_flags && !$easy)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày nộp hồ sơ visa không đúng định dạng "ngày/tháng/năm":'. $data['apply_date']
            );
        //////////
        $check_date_flags = false;
        foreach ($date_format as $_format) {
            if (date_create_from_format($_format, $data['visa_estimated_due_date'])
                && !empty(trim($data['visa_estimated_due_date']))) {
                $list[$index]['visa_estimated_due_date']
                    = date_create_from_format($_format, $data['visa_estimated_due_date'])->format('Y-m-d');
                $check_date_flags = true;
            } elseif (empty(trim($data['visa_estimated_due_date']))) {
                $list[$index]['visa_estimated_due_date'] = null;
                $check_date_flags = true;
            }
        }
        if (!$check_date_flags && !$easy)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày dự kiến có visa không đúng định dạng "ngày/tháng/năm":'
                . $data['visa_estimated_due_date']
            );
    } // big foreach
}

function ___tour_pax_import() {
    set_time_limit(0);
    ini_set('memory_limit', -1);

    $tour_id = $_POST['tour_id'];
    $tour_pax_list = PaxTour::getData($tour_id);
    $pax_id_list_current = [];
    $pax_list_new = [];
    while (($row = db_fetch_array($tour_pax_list))) {
        $pax_id_list_current[] = $row['id'];
    }

    require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
    $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

    $file = $_FILES['file'];

    if (isset($file['error']) &&  is_array($file['error'])) {
        $msg = '';
        foreach ($file['error'] as $key => $error)
            $msg .= $error.': '.$key.'; ';

        throw new Exception('Lỗi: '.$msg);
    }

    if (!isset($file['size']) || !$file['size'] || $file['size'] > 20*1024*1024) {
        throw new Exception('Dung lượng file quá lớn hoặc bằng 0');
    }

    if (!isset($file['name']) || !$file['name']) {
        throw new Exception('File không có tên');
    }

    if (!in_array($file['type'], [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ])) {
        throw new Exception('Bắt buộc sử dụng file Excel (xls, xlsx).');
    }

    $allowed =  array('xls','xlsx');
    $filename = $file['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if(!in_array($ext,$allowed) ) {
        throw new Exception('Bắt buộc sử dụng file Excel (xls, xlsx).');
    }

    $upload_dir = ROOT_DIR.'public_file'.DIRECTORY_SEPARATOR.'import_tour_pax'.DIRECTORY_SEPARATOR.date('Y-m-d');
    if (!file_exists($upload_dir))
        @mkdir($upload_dir, 0766, true);

    $new_name = uniqid('OP_tour_pax_', true).'_'.microtime(true).'.'.$ext;
    $file_path = $upload_dir.DIRECTORY_SEPARATOR.$new_name;
    if (!move_uploaded_file($file['tmp_name'], $file_path))
        throw new Exception('Lỗi di chuyển file');

    $objPHPExcel = PHPExcel_IOFactory::load($file_path);
    $objWorksheet = $objPHPExcel->getActiveSheet();
    $highestRow = $objWorksheet->getHighestRow();

    $easy = 'upload_booking' === $_REQUEST['type'];
    $blank_row = false;

    for ($row = 2; $row <= $highestRow; $row++) { // validation
        $name = $objWorksheet->getCell('A'.$row)->getValue();
        $phone = $objWorksheet->getCell('B'.$row)->getValue();
        $code = $objWorksheet->getCell('C'.$row)->getValue();
        $sex = $objWorksheet->getCell('D'.$row)->getValue();
        $dob = $objWorksheet->getCell('E'.$row)->getFormattedValue();
        if (is_numeric($dob)) {
            $dob = date(
                'd/m/Y',
                PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCell('E'.$row)->getValue())
            );
        }
        $passport_no = $objWorksheet->getCell('F'.$row)->getValue();
        $doe = $objWorksheet->getCell('G'.$row)->getFormattedValue();
        if (is_numeric($doe)) {
            $doe = date(
                'd/m/Y',
                PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCell('G'.$row)->getValue())
            );
        }
        $room = $objWorksheet->getCell('H'.$row)->getValue();

        if ($easy) {
            if (empty(trim($code))) {
                if ($blank_row) break;

                $blank_row = true;
                continue;
            }
        } else {
            if (empty(trim($name)) && empty(trim($phone)) && empty(trim($code))) {
                if ($blank_row) break;

                $blank_row = true;
                continue;
            }
        }

        $name = trim($name);
        $phone = _String::formatPhoneNumber($phone);
        $passport_no = trim($passport_no);
        $dob = trim($dob);
        $doe = trim($doe);
        $code = str_replace(' ', '', $code);
        $room = str_replace(' ', '', $room);
        $sex = strtolower($sex) == 'f' ? 0 : 1;

        $data = [
            'passport_no'  => $passport_no,
            'full_name'    => $name,
            'dob'          => $dob,
            'doe'          => $doe,
            'room'         => $room,
            'gender'       => $sex,
            'phone_number' => $phone,
            'booking_code' => $code,
        ];

        $pax_list_new[] = $data;
    }

    if (is_array($pax_list_new))
        $pax_list_new = array_filter($pax_list_new);
    if (!count($pax_list_new))
        return false;

    ___validate_data($pax_list_new, $_REQUEST['type']);

    foreach ($pax_list_new as $data) { // ___save_pax_data
        ___save_pax_data($tour_id, $data);
    }
}

function ___tour_pax_edit() {
    if (!isset($_POST['group-loyalty']) || !$_POST['group-loyalty'] || !is_array($_POST['group-loyalty'])) {
        $_POST['group-loyalty'] = [];
    }

    $tour_id = $_POST['tour_id'];
    $tour_pax_list = PaxTour::getData($tour_id);
    $pax_id_list_current = [];
    $pax_id_list_new = [];
    while (($row = db_fetch_array($tour_pax_list))) {
        $pax_id_list_current[] = $row['id'];
    }

    foreach ($_POST['group-loyalty'] as $index => $data) {
        if (isset($data['pax_id']))
            $pax_id_list_new[] = $data['pax_id'];
    }

    if ($pax_id_list_current) {
        $remove_pax_id = array_diff($pax_id_list_current, $pax_id_list_new);
        PaxTour::remove($remove_pax_id, $tour_id);
    }

    foreach ($_POST['group-loyalty'] as $index => $data) {
        $index_ = $index+1;
        foreach ($_POST['group-loyalty'] as $_index => $_data) {
            $_index_ = $_index+1;
            if ($index == $_index) continue;

            if ($data['passport_no'] == $_data['passport_no'])
                throw new Exception(
                    'Trùng số hộ chiếu: '.$data['passport_no']." - Dòng $index_ và $_index_"
                );
        }
    }

    foreach ($_POST['group-loyalty'] as $index => $data) {
        if (!date_create_from_format('d/m/Y', $data['dob']))
            throw new Exception(
                'Dòng '.($index+1).': Ngày sinh không đúng định dạng "ngày/tháng/năm":'. $data['dob']
            );

        if (!date_create_from_format('d/m/Y', $data['doe']))
            throw new Exception(
                'Dòng '.($index+1).': Ngày hết hạn không đúng định dạng "ngày/tháng/năm":'. $data['doe']
            );

        if (!empty($data['phone_number'])) {
            $data['phone_number'] = str_replace(
                " ",
                "",
                trim(trim($data['phone_number']), "'\"")
            );
        }

        $check_date_flags = false;
        $_format = 'd/m/Y';
        if (date_create_from_format($_format, $data['receiving_date']) && !empty(trim($data['receiving_date']))) {
            $data['receiving_date']
                = date_create_from_format('d/m/Y', $data['receiving_date'])->format('Y-m-d');
            $check_date_flags = true;
        } elseif (empty(trim($data['receiving_date']))) {
            $data['receiving_date'] = null;
            $check_date_flags = true;
        }

        if (!$check_date_flags)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày nhận không đúng định dạng "ngày/tháng/năm":'. $data['receiving_date']
            );

        $check_date_flags = false;
        if (date_create_from_format($_format, $data['apply_date']) && !empty(trim($data['apply_date']))) {
            $data['apply_date']
                = date_create_from_format('d/m/Y', $data['apply_date'])->format('Y-m-d');
            $check_date_flags = true;
        } elseif (empty(trim($data['apply_date']))) {
            $data['apply_date'] = null;
            $check_date_flags = true;
        }

        if (!$check_date_flags)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày nộp hồ sơ visa không đúng định dạng "ngày/tháng/năm":'. $data['apply_date']
            );

        $check_date_flags = false;
        if (date_create_from_format($_format, $data['visa_estimated_due_date'])
            && !empty(trim($data['visa_estimated_due_date']))) {
            $data['visa_estimated_due_date']
                = date_create_from_format('d/m/Y', $data['visa_estimated_due_date'])->format('Y-m-d');
            $check_date_flags = true;
        } elseif (empty(trim($data['visa_estimated_due_date']))) {
            $data['visa_estimated_due_date'] = null;
            $check_date_flags = true;
        }

        if (!$check_date_flags)
            throw new Exception(
                'Dòng '.($index+1)
                .': Ngày dự kiến có visa không đúng định dạng "ngày/tháng/năm":'. $data['visa_estimated_due_date']
            );

        $data['dob'] = date_create_from_format('d/m/Y', $data['dob'])->format('Y-m-d');
        $data['doe'] = date_create_from_format('d/m/Y', $data['doe'])->format('Y-m-d');

        ___save_pax_data($tour_id, $data, $index);
    }
}

$tour_id = (int)trim($_REQUEST['tour']);
$tour = null;
if ($tour_id) {
    $tour = TourNew::lookup($tour_id);
}
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);
$sql = 'SELECT DISTINCT destination from '.TOUR_NEW_TABLE.' WHERE id= '.$tour_id.'';
$res = db_query($sql);
if (!$res) {
    $tour_arr = -1;
} else {
    $row = db_fetch_array($res);
    if (!$row)
        $tour_arr = -1;
    else
        $tour_arr = (int)$row['destination'] ?: -1;
}

$tour_pax_list = null;
$tl_room = null;

$content_export = null;
if (isset($_REQUEST['action']) && 'export' === $_REQUEST['action']) {
    if (!$tour_id)
        exit('Tour not found');

    $tour_pax_list = PaxTour::getData($tour_id, true);
    $total_pax = PaxTour::countPax($tour_id);
    $total_pax_by_room = PaxTour::countPax($tour_id, true);
    $tl_room = PaxTour::getTourLeaderRoom($tour_id);

    if (!$tour)
        exit('Tour not found');

    $item = $tour;
    $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);

    if ($tour_leader
        && isset($tour_leader->value)
        && $tour_leader
        && isset($tour_leader->value)
        && ($conf = $tour_leader->getConfigurationForm())) {
        foreach ($conf->getFields() as $f) {
            if (!in_array($f->get('name'), ['full_name', 'phone_number', 'passport_no', 'doe'])) continue;
            $tour_leader_data[$f->get('name')] = ['label' => $f->get('label'), 'value' => is_array($f->getWidget()->value) ? array_pop($f->getWidget()->value) : $f->getWidget()->value];
        }
    }

    require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
    $dir = INCLUDE_DIR.'../public_file/';
    $file_name = 'Tour - '
        . preg_replace('/[^a-zA-Z0-9-_]/', '-', _String::khongdau($tour->name))
        .' - Pax List - ' . Format::userdate("Y-m-d_H-i-s", time()).".xlsx";

    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
    $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    $objPHPExcel = new PHPExcel();
    $properties = $objPHPExcel->getProperties();
    $properties->setCreator(NOTIFICATION_TITLE);
    $properties->setLastModifiedBy("System");
    $properties->setTitle(NOTIFICATION_TITLE." - Tour Passenger List");
    $properties->setSubject(NOTIFICATION_TITLE." - Tour Passenger List");
    $properties->setDescription(NOTIFICATION_TITLE." - Tour Passenger List - ".Format::userdate("Y-m-d H:i:s", time()));
    $properties->setKeywords(NOTIFICATION_TITLE." - Tour Passenger List");
    $properties->setCategory(NOTIFICATION_TITLE." - Tour Passenger List");

    $sheet = $objPHPExcel->getActiveSheet();

    $max = $col_1 = $col_2 = 0;
    $excel_row = 1;
    //info tugo
    $sheet->setCellValue('A'.$excel_row++, TUGO_JSC);
    $sheet->setCellValue('A'.$excel_row++, TUGO_ADDRESSS);
    $sheet->setCellValue('A'.$excel_row++, TUGO_TEL);
    $sheet->setCellValue('A'.$excel_row++, TUGO_NO_LICENSE);

    //set width
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(37);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(7);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);

    $excel_row += 2;
    $departure_date = $tour->gateway_present_time;
    $airline = DynamicListItem::lookup((int)$tour->airline_id);
    if($airline)
        $airline = $airline->getValue();
    else
        $airline = '';
    if($departure_date !== null && $departure_date !== 0){
        $departure_date = date('d/m/Y',strtotime($departure_date));
    }else $departure_date = '';

    $flight_detail = 'FLIGHT DETAIL: '.$departure_date.'                  '.$airline;
    $sheet->setCellValue('A'.$excel_row, $flight_detail);
    $sheet->getStyle('A'.$excel_row.':D'.$excel_row)->applyFromArray(
        array(
            'font' => array(
                'bold' => true
            ),
        )
    );

    $sheet->getStyle('A'.$excel_row.':D'.$excel_row)->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff00')
            )
        )
    );

    $sheet->getStyle('G'.$excel_row.':J'.($excel_row+2))->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'c0c0c0')
            )
        )
    );
    $sheet->setCellValue('G'.$excel_row, 'TOTAL');
    $sheet->setCellValue('H'.$excel_row, $total_pax . ' + 1 TL');
    $sheet->getStyle('H'.$excel_row.':J'.$excel_row)->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS
            ),
            'font' => array(
                'bold' => true
            ),
        )
    );
    $excel_row++;

    $sheet->setCellValue('G'.$excel_row, 'Trưởng Đoàn');
    $sheet->setCellValue('H'.$excel_row, $tour_leader_data['full_name']['value']);
    $sheet->getStyle('H'.$excel_row.':J'.$excel_row)->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS
            ),
            'font' => array(
                'bold' => true
            ),
        )
    );
    $excel_row++;
    $sheet->setCellValue('G'.$excel_row, 'ROOM');
    $string_pax_room = '';
    foreach ($total_pax_by_room as $_room) {
        $string_pax_room .= $_room['total'] . ' ' .$_room['room_type'] . ' + ';
    }
    $string_pax_room = trim($string_pax_room);
    $string_pax_room = trim($string_pax_room, '+');
    $string_pax_room = trim($string_pax_room);
    $sheet->setCellValue('H'.$excel_row, $string_pax_room);
    $sheet->getStyle('H'.$excel_row.':J'.$excel_row)->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS
            ),
        )
    );
    $excel_row++;

    //info departure && return flight
    $excel_row -= 2;
    $sheet->getStyle('B'.$excel_row.':C'.($excel_row+1))->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'c0c0c0')
            )
        )
    );
    $departure_info = '';
    $return_info = '';
    $airport_code = FlightOP::searchAirportCodeFromTour((int)$tour->id);

    if (count($airport_code) === 2) {
        $departure_info = date('d M', strtotime($airport_code[0]['departure_at'])) . ' - '
            . $airport_code[0]['airport_code_from'] . ' ' . $airport_code[0]['airport_code_to'] . ' '
            . date('Hi', strtotime($airport_code[0]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[0]['arrival_at']));

        $return_info = date('d M', strtotime($airport_code[1]['departure_at'])) . ' - '
            . $airport_code[1]['airport_code_from'] . ' ' . $airport_code[1]['airport_code_to'] . ' '
            . date('Hi', strtotime($airport_code[1]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[1]['arrival_at']));
    } elseif (count($airport_code) === 4) {
        $departure_info = date('d M', strtotime($airport_code[0]['departure_at'])) . ' - '
            . $airport_code[0]['airport_code_from'] . ' ' . $airport_code[0]['airport_code_to'] . ' ' . $airport_code[1]['airport_code_to'] . ' - '
            . date('Hi', strtotime($airport_code[0]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[1]['arrival_at']));

        $return_info = date('d M', strtotime($airport_code[2]['departure_at'])) . ' - '
            . $airport_code[2]['airport_code_from'] . ' ' . $airport_code[2]['airport_code_to'] . ' ' . $airport_code[3]['airport_code_to'] . ' - '
            . date('Hi', strtotime($airport_code[2]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[3]['arrival_at']));
    }

    $sheet->setCellValue('B'.$excel_row++, $departure_info);
    $sheet->setCellValue('B'.$excel_row++, $return_info);

    $col_2 = $excel_row;

    $max = $col_1 > $col_2 ? $col_1 : $col_2;

    $excel_row = $max + 2;

    $sheet->getStyle('A'.$excel_row.':K'.($excel_row + 1))->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dd7e6b')
            ),
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        )
    );
    $sheet->getStyle('A'.$excel_row.':K'.$excel_row)->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS)
        )
    );
    $gather_date = $tour->gateway_present_time;
    if($gather_date !== null && $gather_date !== 0){
        $gather_date = date('d/m/Y H:i',$gather_date);
    }else $gather_date = '';
    $gather = 'TẬP TRUNG SÂN BAY: '.$gather_date;
    $sheet->setCellValue('A'.$excel_row++, $gather);

    $header = [
        'NO',
        'NAME',
        'PHONE',
        'SALES',
        'CODE',
        'SEX',
        'DOB',
        'PPNo',
        'DOE',
        'ROOM',
        'NOTED',
    ];

    $col = 'A';
    $row_start_table = $excel_row;
    foreach ($header as $title) {
        $sheet->setCellValue($col++.$excel_row, $title);
    }

    $excel_row++;
    $content_pax_list =[];
    if ($tour_pax_list) {
        $count = 1;
        while (($row = db_fetch_array($tour_pax_list))) {
            $content_pax_list[] = $row;
            if(!$row['dob'] && $row['dob'] === null) $dob = '';
            else
                $dob = date('d/m/Y', strtotime($row['dob']));

            if(!$row['doe'] && $row['doe'] === null) $dob = '';
            else
                $doe = date('d/m/Y', strtotime($row['doe']));

            $data = [
                $count++,
                $row['full_name'],
                $row['phone_number'],
                $row['staff'],
                $row['nationality'],
                (isset($row['gender']) && $row['gender']) ? 'M' : 'F',
                $dob,
                $row['passport_no'],
                $doe,
                $row['room_type'].' '.$row['room'],
                $row['note'],
                isset($row['tl']) && $row['tl'] ? 'X' : "",
            ];
            $sheet->fromArray($data, NULL, 'A' . $excel_row++);
        }

        if (!$tl_room) {
            $data = [
                $count++,
                $tour_leader_data['full_name']['value'],
                $tour_leader_data['phone_number']['value'],
                'TL',
                '',
                '',
                '',
                $tour_leader_data['passport_no']['value'],
                $tour_leader_data['doe']['value'] !== null ?date('d/m/Y', $tour_leader_data['doe']['value']) :'',
                'DBL',
                '',
                '',
            ];
            $sheet->fromArray($data, NULL, 'A' . $excel_row);
            $sheet->getStyle('A'.$excel_row.':K'.$excel_row)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'c39bff')
                    ),
                    'font' => array(
                        'bold' => true
                    ),
                )
            );
        }
    }
    $row_end_table = $excel_row;
    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            ),
        )
    );
    $objPHPExcel->getActiveSheet()->getStyle('A'.$row_start_table.':'.'K'.$row_end_table)->applyFromArray($styleArray);
    $sheet->getStyle('A1'.':'.'K'.$row_end_table)->applyFromArray(
        array(
            'font' => array(
                'size' => 14,
                'name' => 'Cambria'
            ),
        )
    );
    $sheet->getStyle('C'.$row_start_table.':'.'K'.$row_end_table)->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        )
    );

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    $objWriter->save($dir.$file_name);

    $objPHPExcel->disconnectWorksheets();
    $content_export['leader'] = $tour_leader_data;
    $content_export['pax'] = $content_pax_list;
    $content_export['aircode'] = $airport_code;
    $content_export['tour'] = $tour;
    $content_export = json_encode($content_export);

    $log = TourExportLog::create([
            'tour_id' => $tour->id,
            'content' => $content_export,
            'filename' => $file_name,
            'created_by' => $thisstaff->getId(),
            'created_at' => date('Y-m-d H:i:s')
    ]);
    $log->save();

    unset($objWriter);
    unset($objPHPExcel);
    unset($tour_pax_list);
    unset($tour_leader_data);
    unset($tour_data);

    $file = $dir.$file_name;
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }

    exit;
}

if (isset($_SERVER['REQUEST_METHOD']) && 'POST' === $_SERVER['REQUEST_METHOD'] && $_POST) {
    if (!isset($_POST['tour_id']) || !isset($_GET['tour']) || $_POST['tour_id'] != $_GET['tour']) {
        header('Location: '.$referer);
        exit;
    }

    ?>
    <style>
        #msg_info { margin: 0; padding: 5px; margin-bottom: 10px; color: #3a87ad; border: 1px solid #bce8f1;  background-color: #d9edf7; }
        #msg_error { margin: 0; padding: 5px 10px 5px 36px; margin-bottom: 10px; border: 1px solid #a00; background: url('/scp/images/icons/error.png') 10px 50% no-repeat #fff0f0; }
    </style><?php

    $errors = [];

    try {
        db_start_transaction();

        if (isset($_POST['action']) && 'import' === $_POST['action']) {
            ___tour_pax_import();
        } else {
            ___tour_pax_edit();
        }

        db_commit();
    } catch(Exception $ex) {
        db_rollback();
        $errors[] = $ex->getMessage();
    }

    echo "<script>parent.$('#overlay').stop().fadeOut();parent.$('#overlay, #loading').hide();</script>";

    if ($errors) {
        foreach ($errors as $error) {
            ?>
            <div id="msg_error"><?php echo $error ?></div>
            <?php
        }
        exit;
    } else {
        ?>
        <div id="msg_info">Lưu thông tin thành công. Trang sẽ tự chuyển về danh sách.</div>
        <?php $url = $cfg->getUrl().'scp/tour-guest.php?tour='.$_POST['tour_id'].'&layout=list'; ?>
        <script>parent.setTimeout(function() { parent.window.location = "<?php echo $url  ?>"; }, 1500);</script>
        <?php
    }

    //
    exit;
}

if ($tour_id) {
    $tour_pax_list = PaxTour::getData($tour_id, true);
    $tl_room = PaxTour::getTourLeaderRoom($tour_id);
}

if($thisstaff->canViewOperatorLists() || $thisstaff->canEditOparetorLists()) {
    $nav->addSubMenu([
        'desc'      => __('Operator Lists'),
        'title'     => __('View All Lists'),
        'href'      => 'operator.php',
        'iconclass' => 'newTicket',
        'id'        => 'tour_pax',
    ], (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
}

$_tour_check = is_numeric($tour_arr) ? [$tour_arr] : $tour_arr;
if($tour_arr === -1 || array_intersect($_tour_check,$group_des)) {
    $page = 'tour-pax.inc.php';
    $mobile = false;
    if (isset($_REQUEST['layout']) && 'list' === $_REQUEST['layout'])
        $page='tour-pax.list.inc.php';

    if (isset($_REQUEST['layout']) && 'import' === $_REQUEST['layout'])
        $page='tour-pax.import.inc.php';

    if (isset($_REQUEST['layout']) && 'upload_booking' === $_REQUEST['layout'])
        $page='tour-pax.import.inc.php';

    if (isset($_REQUEST['layout']) && 'print' === $_REQUEST['layout'])
        $page='tour-pax.print.inc.php';

    if (isset($_REQUEST['layout']) && 'passport_upload' === $_REQUEST['layout']) {
        $page='tour-pax.passport-upload.inc.php';
        $url = $cfg->getUrl().'scp/tour-guest.php?tour='.(int)$_REQUEST['tour'].'&layout=passport_upload';
        $token = QrCodeToken::createToken();
        $file_name = QrCodeToken::createAndSaveQrCode($url,$errors, $token);
        $mobile = true;
    }
    if (isset($_REQUEST['layout']) && 'import_fast' === $_REQUEST['layout']) {
        $page='tour.inc.php';
    }
}else{
    header('Location: '.$cfg->getBaseUrl().'/scp/tour-list.php');
    exit;
}

$ost->addExtraHeader('<meta name="tip-namespace" content="operator.all_list" />',
     "$('#content').data('tipNamespace', 'operator.all_list');");

$nav->setTabActive('operator');
if (!$mobile)
    require(STAFFINC_DIR.'header.inc.php');
else
    require(STAFFINC_DIR.'header.mobile.inc.php');

require(STAFFINC_DIR.$page);

if (!$mobile)
    include(STAFFINC_DIR.'footer.inc.php');
else
    include(STAFFINC_DIR.'footer.mobile.inc.php');

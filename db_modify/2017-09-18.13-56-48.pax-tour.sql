CREATE TABLE ost_pax
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  full_name VARCHAR(64),
  dob DATE,
  gender TINYINT,
  uuid VARCHAR(16)
);
CREATE UNIQUE INDEX ost_pax_uuid_uindex ON ost_pax (uuid);
CREATE INDEX ost_pax_full_name_index ON ost_pax (full_name);

CREATE TABLE ost_pax_tour
(
  pax_id INT,
  tour_id INT,
  pax_info INT,
  booking_code VARCHAR(16),
  room_id INT,
  CONSTRAINT ost_pax_tour_pax_id_tour_id_pk PRIMARY KEY (pax_id, tour_id)
);
CREATE INDEX ost_pax_tour_room_index ON ost_pax_tour (room_id);

ALTER TABLE ost_pax_tour ADD phone_number VARCHAR(16) NULL;
CREATE INDEX ost_pax_tour_phone_number_index ON ost_pax_tour (phone_number);

CREATE TABLE ost_pax_info
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  pax_id INT,
  passport_no VARCHAR(32),
  doe DATE
);
CREATE UNIQUE INDEX ost_pax_info_passport_no_uindex ON ost_pax_info (passport_no);
CREATE INDEX ost_pax_info_doe_index ON ost_pax_info (doe);
CREATE INDEX ost_pax_info_pax_id_index ON ost_pax_info (pax_id);

ALTER TABLE ost_pax ADD created_at DATETIME NULL;
ALTER TABLE ost_pax ADD updated_at DATETIME NULL;
ALTER TABLE ost_pax_info ADD created_at DATETIME NULL;
ALTER TABLE ost_pax_info ADD updated_at DATETIME NULL;
ALTER TABLE ost_pax_tour ADD created_at DATETIME NULL;
ALTER TABLE ost_pax_tour ADD updated_at DATETIME NULL;

ALTER TABLE ost_pax CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE ost_pax_info CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE ost_pax_tour CHANGE room_id room VARCHAR(10);
ALTER TABLE receipt CHANGE price_nl price_nl DECIMAL (12,0);
ALTER TABLE receipt CHANGE price_te price_te DECIMAL (12,0);
ALTER TABLE receipt CHANGE surcharge surcharge DECIMAL (12,0);
ALTER TABLE receipt CHANGE total_retail_price total_retail_price DECIMAL (12,0);
ALTER TABLE receipt CHANGE staked staked DECIMAL (12,0);
ALTER TABLE receipt CHANGE remaining_charge remaining_charge DECIMAL (12,0);
ALTER TABLE receipt DROP place_air;
ALTER TABLE receipt ADD created_by int(10) NOT NULL;
ALTER TABLE receipt ADD created_at DATETIME NOT NULL;
ALTER TABLE receipt ADD updated_by int(10) NOT NULL;
ALTER TABLE receipt ADD updated_at DATETIME NOT NULL;
ALTER TABLE receipt ADD status tinyint;



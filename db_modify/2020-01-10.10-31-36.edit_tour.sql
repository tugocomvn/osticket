/*branch: reservation */

alter table tour
    add status_edit_tour smallint default 0 not null;

alter table ost_groups
    add can_request_edit_tour tinyint(1) unsigned default 0 null;

alter table ost_groups
    add can_approve_edit_tour tinyint(1) unsigned default 0 null;


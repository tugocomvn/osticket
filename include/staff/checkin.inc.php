<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->can_do_checkin()) die('Access Denied');
?>

<style>
    .wrapper {
        text-align: center;
    }
    canvas {
        margin: 20px auto;
    }

    h1 {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 24px;
        color: #086600;
    }
</style>
<div class="wrapper">
    <?php
    $_test_ip = !empty($_REQUEST['test_ip']) ? $_REQUEST['test_ip'] : '';
    $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : getenv('REMOTE_ADDR');
    if ($_test_ip) $ip = $_test_ip;

    $not_in_office = false;
    if (!in_array($ip, config('tugo_ip')) && $_SERVER['SERVER_NAME'] !== 'localhost')
        $not_in_office = true;
    ?>
    <?php if(!$is_checked_in): ?>
        <h2>Chào <?php echo $thisstaff->getName() ?>. <?php if (!$is_sunday): ?>Hôm nay bạn chưa chấm công.<?php else: ?>Chúc ngày nghỉ vui vẻ.<?php endif; ?></h2>
    <?php endif; ?>
    <canvas id="canvas" width="400" height="400" style="background-color:#660066"></canvas>

    <?php if($is_sunday): ?>
        <p>Hôm nay không cần chấm công đâu. Hãy vào menu khác để làm việc.</p>

    <?php elseif($is_checked_in): ?>
        <h1>Chào <?php echo $thisstaff->getName() ?>. Bạn đã chấm công.</h1>
        <p>Hãy vào menu khác để làm việc. Chúc một ngày làm việc hiệu quả.</p>

    <?php elseif($not_in_office): ?>
        <h3>Bạn chưa vào văn phòng, hoặc đang <strong>không</strong> sử dụng mạng của <?php echo COMPANY_TITLE ?>.</h3>
        <p>Hãy kiểm tra lại wifi hoặc gắn dây cáp mạng <?php echo COMPANY_TITLE ?> nhé.</p>

    <?php else: ?>
        <form action="/scp/checkin.php" method="post">
            <?php csrf_token(); ?>
            <p>Bấm bút bên dưới để chấm công</p>
            <button class="btn_sm btn-success">Checkin</button>
        </form>
    <?php endif; ?>

    <script>
        $(document).ready(function() {
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            var radius = canvas.height/2;
            ctx.translate(radius, radius);
            radius = radius*0.90
            drawClock();
            setInterval(drawClock, 1000);

            function drawClock() {
                drawFace(ctx, radius);
                drawNumbers(ctx, radius);
                drawTime(ctx, radius);
            }

            function drawTime(ctx, radius){
                var now = new Date();
                var hour = now.getHours();
                var minute = now.getMinutes();
                var second = now.getSeconds();
                //hour
                hour=hour%12;
                hour=(hour*Math.PI/6)+(minute*Math.PI/(6*60))+(second*Math.PI/(360*60));
                drawHand(ctx, hour, radius*0.5, radius*0.07);
                //minute
                minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
                drawHand(ctx, minute, radius*0.8, radius*0.07);
                // second
                second=(second*Math.PI/30);
                drawHand(ctx, second, radius*0.9, radius*0.02);
            }

            function drawHand(ctx, pos, length, width) {
                ctx.beginPath();
                ctx.lineWidth = width;
                ctx.lineCap = "round";
                ctx.moveTo(0,0);
                ctx.rotate(pos);
                ctx.lineTo(0, -length);
                ctx.stroke();
                ctx.rotate(-pos);
            }

            function drawNumbers(ctx, radius) {
                var ang;
                var num;
                ctx.font = radius*0.15 + "px arial";
                ctx.textBaseline="middle";
                ctx.textAlign="center";
                for(num= 1; num < 13; num++){
                    ang = num * Math.PI / 6;
                    ctx.rotate(ang);
                    ctx.translate(0, -radius*0.85);
                    ctx.rotate(-ang);
                    ctx.fillText(num.toString(), 0, 0);
                    ctx.rotate(ang);
                    ctx.translate(0, radius*0.85);
                    ctx.rotate(-ang);
                }
            }

            function drawFace(ctx, radius) {
                var grad;

                ctx.beginPath();
                ctx.arc(0, 0, radius, 0, 2*Math.PI);
                ctx.fillStyle = 'white';
                ctx.fill();

                grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
                grad.addColorStop(0, '#333');
                grad.addColorStop(0.5, 'white');
                grad.addColorStop(1, '#333');
                ctx.strokeStyle = grad;
                ctx.lineWidth = radius*0.1;
                ctx.stroke();

                ctx.beginPath();
                ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
                ctx.fillStyle = '#333';
                ctx.fill();
            }
        });
    </script>
</div>

<?php
/*********************************************************************
    class.cron.php

    Nothing special...just a central location for all cron calls.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    TODO: The plan is to make cron jobs db based.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
//TODO: Make it DB based!
require_once INCLUDE_DIR.'class.signal.php';
require_once INCLUDE_DIR.'class.format.php';
class Cron {

    function MailFetcher() {
        require_once(INCLUDE_DIR.'class.mailfetch.php');
        MailFetcher::run(); //Fetch mail..frequency is limited by email account setting.
    }

    function TicketMonitor() {
        require_once(INCLUDE_DIR.'class.ticket.php');
        require_once(INCLUDE_DIR.'class.lock.php');
        Ticket::checkOverdue(); //Make stale tickets overdue
        TicketLock::cleanup(); //Remove expired locks
    }

    function PurgeLogs() {
        global $ost;
        // Once a day on a 5-minute cron
        if (rand(1,300) == 42)
            if($ost) $ost->purgeLogs();
    }

    function PurgeDrafts() {
        require_once(INCLUDE_DIR.'class.draft.php');
        Draft::cleanup();
    }

    function CleanOrphanedFiles() {
        require_once(INCLUDE_DIR.'class.file.php');
        AttachmentFile::deleteOrphans();
    }

    function MaybeOptimizeTables() {
        // Once a week on a 5-minute cron
        $chance = rand(1,2000);
        switch ($chance) {
        case 42:
            @db_query('OPTIMIZE TABLE '.TICKET_LOCK_TABLE);
            break;
        case 242:
            @db_query('OPTIMIZE TABLE '.SYSLOG_TABLE);
            break;
        case 442:
            @db_query('OPTIMIZE TABLE '.DRAFT_TABLE);
            break;

        // Start optimizing core ticket tables when we have an archiving
        // system available
        case 142:
            #@db_query('OPTIMIZE TABLE '.TICKET_TABLE);
            break;
        case 542:
            #@db_query('OPTIMIZE TABLE '.FORM_ENTRY_TABLE);
            break;
        case 642:
            #@db_query('OPTIMIZE TABLE '.FORM_ANSWER_TABLE);
            break;
        case 342:
            #@db_query('OPTIMIZE TABLE '.FILE_TABLE);
            # XXX: Please do not add an OPTIMIZE for the file_chunk table!
            break;

        // Start optimizing user tables when we have a user directory
        // sporting deletes
        case 742:
            #@db_query('OPTIMIZE TABLE '.USER_TABLE);
            break;
        case 842:
            #@db_query('OPTIMIZE TABLE '.USER_EMAIL_TABLE);
            break;
        }
    }

    function IndexingPhoneNumber() {
        $count = 0;
        $limit = 20000;
        $offset = 0;

        while (self::_index_phone_number_from_form($limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_phone__number_from_search('U', $limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_phone__number_from_search('H', $limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_phone__number_from_search('T', $limit, $offset, $count)) {
            $offset += $limit;
        }

        self::_push_phone_number_to_table();

        echo "\r\nTotal: {$count} phone numbers updated\r\n";
        //        // // // ////        // // // ////        // // // ////        // // // //

        $count = 0;

        while (self::_index_email_from_form($limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_email_from_search('U', $limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_email_from_search('H', $limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_email_from_search('T', $limit, $offset, $count)) {
            $offset += $limit;
        }

        $offset = 0;
        while (self::_index_email_from_user_email($limit, $offset, $count)) {
            $offset += $limit;
        }

        echo "\r\nTotal: {$count} email addresses updated\r\n";
    } // end function

    function _index_phone_number_from_form($limit, $offset, &$count) {
        $flag = false;
        echo "______".$offset."\r\n";
        $sql = sprintf(
            "SELECT DISTINCT
                  e.object_id, v.value
                FROM %s v
                  JOIN %s e ON e.id = v.entry_id
                  JOIN %s u ON e.object_id = u.id
                WHERE e.object_type = 'U' and `value` REGEXP %s ORDER BY e.object_id DESC LIMIT $offset, $limit"
            , FORM_ANSWER_TABLE
            , FORM_ENTRY_TABLE
            , USER_TABLE
            , "'"._String::UNIVERSAL_PHONE_NUMBER_REGEX_MYSQL."'"
        );

        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $flag = true;

            if (!isset($row['value']) || empty($row['value']))
                continue;
            $phone_numbers = _String::getPhoneNumbers($row['value']);

            if ($phone_numbers) {
                foreach ($phone_numbers as $phone) {
                    $sql = 'INSERT IGNORE INTO '.INDEX_PHONE_TABLE
                        . ' SET object_type='.db_input($row['object_type'])
                        . ', object_id='.db_input($row['object_id'])
                        . ', phone_number='.db_input($phone);

                    if (db_query($sql))
                        $count++;
                }
            } // end if check value
        } // end while

        return $flag;
    }

    function _index_email_from_form($limit, $offset, &$count) {
        $flag = false;
        echo "______".$offset."\r\n";
        $sql = sprintf(
            "SELECT DISTINCT
                  e.object_id, v.value
                FROM %s v
                  JOIN %s e ON e.id = v.entry_id
                  JOIN %s u ON e.object_id = u.id
                WHERE e.object_type = 'U' and `value` REGEXP %s ORDER BY e.object_id DESC LIMIT $offset, $limit"
            , FORM_ANSWER_TABLE
            , FORM_ENTRY_TABLE
            , USER_TABLE
            , "'"._String::UNIVERSAL_EMAIL_REGEX_MYSQL."'"
        );

        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $flag = true;

            if (!isset($row['value']) || empty($row['value']))
                continue;

            $emails = _String::getEmailsFromString($row['value']);

            if ($emails) {
                foreach ($emails as $email) {
                    $sql = 'INSERT IGNORE INTO '.INDEX_EMAIL_TABLE
                        . ' SET object_type='.db_input($row['object_type'])
                        . ', object_id='.db_input($row['object_id'])
                        . ', email='.db_input($email);

                    if (db_query($sql))
                        $count++;
                }
            } // end if check value
        } // end while

        return $flag;
    }

    function _index_email_from_user_email($limit, $offset, &$count) {
        $flag = false;
        echo "______".$offset."\r\n";
        $sql = sprintf(
            "SELECT DISTINCT
                  user_id, address
                FROM %s
                LIMIT $offset, $limit"
            , USER_EMAIL_TABLE
        );

        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $flag = true;

            if (!isset($row['address']) || empty($row['address']))
                continue;

            $emails = _String::getEmailsFromString($row['address']);

            if ($emails) {
                foreach ($emails as $email) {
                    $sql = 'INSERT IGNORE INTO '.INDEX_EMAIL_TABLE
                        . ' SET object_type='.db_input('U')
                        . ', object_id='.db_input($row['user_id'])
                        . ', email='.db_input($email);

                    if (db_query($sql))
                        $count++;
                }
            } // end if check value
        } // end while

        return $flag;
    }

    function _index_phone__number_from_search($type, $limit, $offset, &$count) {
        $flag = false;
        echo "______".$offset."\r\n";
        $sql = sprintf(
            "SELECT DISTINCT
                  s.object_id, s.object_type, s.content, s.title
                FROM %s s
                WHERE 1 AND ( s.content REGEXP %s OR s.title REGEXP %s ) AND s.object_type='$type'
                ORDER BY s.object_id DESC LIMIT $offset, $limit"
            , TABLE_PREFIX . '_search'
            , "'"._String::UNIVERSAL_PHONE_NUMBER_REGEX_MYSQL."'"
            , "'"._String::UNIVERSAL_PHONE_NUMBER_REGEX_MYSQL."'"
        );

        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $flag = true;
            if (isset($row['content']) && !empty($row['content'])) {
                $phone_numbers = _String::getPhoneNumbers($row['content']);

                if ($phone_numbers) {
                    foreach ($phone_numbers as $phone) {
                        $sql = 'INSERT IGNORE INTO '.INDEX_PHONE_TABLE
                            . ' SET object_type='.db_input($type)
                            . ', object_id='.db_input($row['object_id'])
                            . ', phone_number='.db_input($phone);

                        if (db_query($sql))
                            $count++;
                    }
                }
            } // end if check content
            if (isset($row['title']) && !empty($row['title'])) {
                $phone_numbers = _String::getPhoneNumbers($row['title']);

                if ($phone_numbers) {
                    foreach ($phone_numbers as $phone) {
                        $sql = 'INSERT IGNORE INTO '.INDEX_PHONE_TABLE
                            . ' SET object_type='.db_input($type)
                            . ', object_id='.db_input($row['object_id'])
                            . ', phone_number='.db_input($phone);

                        if (db_query($sql))
                            $count++;
                    }
                }
            } // end if check title
        } // end while

        return $flag;
    }

    function _index_email_from_search($type, $limit, $offset, &$count) {
        $flag = false;
        echo "______".$offset."\r\n";
        $sql = sprintf(
            "SELECT DISTINCT
                  s.object_id, s.object_type, s.content, s.title
                FROM %s s
                WHERE 1 AND ( s.content REGEXP %s OR s.title REGEXP %s ) AND s.object_type='$type'
                ORDER BY s.object_id DESC LIMIT $offset, $limit"
            , TABLE_PREFIX . '_search'
            , "'"._String::UNIVERSAL_EMAIL_REGEX_MYSQL."'"
            , "'"._String::UNIVERSAL_EMAIL_REGEX_MYSQL."'"
        );

        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $flag = true;
            if (isset($row['content']) && !empty($row['content'])) {
                $emails = _String::getEmailsFromString($row['content']);

                if ($emails) {
                    foreach ($emails as $email) {
                        $sql = 'INSERT IGNORE INTO '.INDEX_EMAIL_TABLE
                            . ' SET object_type='.db_input($type)
                            . ', object_id='.db_input($row['object_id'])
                            . ', email='.db_input($email);

                        if (db_query($sql))
                            $count++;
                    }
                } // end if check value
            } // end if check content
            if (isset($row['title']) && !empty($row['title'])) {
                $emails = _String::getEmailsFromString($row['title']);

                if ($emails) {
                    foreach ($emails as $email) {
                        $sql = 'INSERT IGNORE INTO '.INDEX_EMAIL_TABLE
                            . ' SET object_type='.db_input($type)
                            . ', object_id='.db_input($row['object_id'])
                            . ', email='.db_input($email);

                        if (db_query($sql))
                            $count++;
                    }
                } // end if check value
            } // end if check title
        } // end while

        return $flag;
    }

    function _push_phone_number_to_table() {
        $sql = "INSERT IGNORE INTO phone_number(phone_number)
              SELECT DISTINCT phone_number FROM ost_pax_tour
              WHERE char_length(phone_number) =10";

        db_query($sql);

        sleep(1);

        $sql = "INSERT IGNORE INTO phone_number(phone_number)
              SELECT DISTINCT phone_number FROM ost_phone_number_index
              WHERE char_length(phone_number) =10";

        db_query($sql);
        sleep(1);

        $sql = "INSERT IGNORE INTO phone_number(phone_number)
            SELECT DISTINCT callernumber as num
            FROM ost_call_log
            WHERE
              callernumber REGEXP '^0?[^2][0-9]{8}$' AND CAST(callernumber AS CHAR) NOT LIKE '1900555543'
              AND char_length(callernumber) >=9 and char_length(callernumber) <=10";

        db_query($sql);
        sleep(1);

        $sql = "INSERT IGNORE INTO phone_number(phone_number)
            SELECT DISTINCT destinationnumber as num
            FROM ost_call_log
            WHERE
              destinationnumber REGEXP '^0?[^2][0-9]{8}$' AND CAST(destinationnumber AS CHAR) NOT LIKE '1900555543'
              AND char_length(destinationnumber) >=9 and char_length(destinationnumber) <=10";

        db_query($sql);
    }

    function ExportPayments() {
        require_once(INCLUDE_DIR.'class.booking.php');
        require_once(INCLUDE_DIR.'class.myemail.php');
        $params = [
            'startDate' => Format::userdate('Y-m-d', time()),
            'endDate' => Format::userdate('Y-m-d', time()),
        ];
        $file_path = Payment::export($params);
        $to = [
            'phamquocbuu@gmail.com',
            'ketoan@tugo.com.vn',
            'beckerbao29@gmail.com',
        ];
        $to_name = [
            'Buu Pham',
            'Thuy Vuong',
            'Bao Nguyen',
        ];
        $subject = 'Payment Daily Report - '.Format::userdate('Y-m-d H:i:s', time());
        $content = "<h3>Payment Daily Report</h3>";
        $content .= "<p>Xuất ngày: ".Format::userdate('Y-m-d H:i:s', time())."</p>";
        $content .= "<p>Bao gồm tất cả payment được tạo từ đầu ngày đến thời gian xuất report.</p>";
        $content .= "<p>Xem file đính kèm</p>";
        $content .= "<hr /><p><small>Đây là email tự động, được cài trong cronjob.</small></p>";
        $files = [
            [
                'path' => $file_path,
                'name' => 'Payment-Report_'.Format::userdate('Y-m-d_H-i-s', time()).'.xlsx',
            ]
        ];
        MyEmail::send($to, $to_name, $subject, $content, false, false, $files);
    }

    function UpdateBookingTicketId() {
        try {
            global $argv;
            $total = 0;
            $page = 1;
            $offset = 0;
            $time = isset($argv[1]) ? (int)$argv[1] : 3;
            $sql = "SELECT COUNT(DISTINCT ticket_id) as total FROM payment_tmp
                WHERE 1 AND `time`>=UNIX_TIMESTAMP()-3600*24*$time
                AND ( payment_tmp.booking_ticket_id = 0 OR payment_tmp.booking_ticket_id IS NULL OR payment_tmp.booking_ticket_id LIKE '' )
                AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''"
            ;
            $res = db_query($sql);
            if ($res && ($data = db_fetch_array($res)) && isset($data['total'])) {
                $total = $data['total'];
            }

            if (!$total) return false;
            echo "Total: $total\r\n";

            $limit = 20;
            $flag = false;
            while ($offset < $total) {
                echo $offset."\r\n";
                $sql = "SELECT ticket_id FROM payment_tmp
                    WHERE 1 AND `time`>=UNIX_TIMESTAMP()-3600*24*$time
                    AND ( payment_tmp.booking_ticket_id = 0 OR payment_tmp.booking_ticket_id IS NULL OR payment_tmp.booking_ticket_id LIKE '' )
                    AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>'' LIMIT $limit OFFSET $offset;"
                ;

                $res = db_query($sql);
                if (!$res) break;
                $ids = [];
                while (($row = db_fetch_array($res))) {
                    if (!$row) {
                        $flag = true;
                        break;
                    }
                    $ids[] = (int)$row['ticket_id'];
                }

                $ids = array_unique($ids);
                $ids = array_filter($ids);

                $page++;
                $offset = ($page-1) * $limit;

                if (!$ids) continue;

                $sql = "UPDATE payment_tmp
                    JOIN booking_view
                    ON payment_tmp.ticket_id IN (".implode(',', $ids).")
                    AND ( payment_tmp.booking_ticket_id = 0 OR payment_tmp.booking_ticket_id IS NULL OR payment_tmp.booking_ticket_id LIKE '' )
                    AND booking_view.booking_code IS NOT NULL AND booking_view.booking_code<>''
                    AND payment_tmp.booking_code IS NOT NULL AND payment_tmp.booking_code<>''
                    AND  trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM booking_view.booking_code)) LIKE
                    trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM payment_tmp.booking_code))
                    SET payment_tmp.booking_ticket_id=booking_view.ticket_id;"
                ;
                db_query($sql);

                if ($flag) break;
            }
        } catch(Exception $ex) {
            echo $ex->getMessage()."\r\n";
        }
    }

    function BirthdaySMS() {
        global $cfg;

        $sql = sprintf("SELECT p.full_name, p.dob, t.phone_number FROM `ost_pax` p
            JOIN ost_pax_tour t ON p.id=t.pax_id
            WHERE month(dob)=%d and day(dob)=%d
            and t.phone_number is not null and trim(t.phone_number) <> ''
            AND t.phone_number <> 0",
            date('m'),
            date('d')
        );

        $res = db_query($sql);
        if (!$res) return;
        while (($row = db_fetch_array($res))) {
            $number = $row['phone_number'];
            if (!_String::simpleCheckMobilePhone($number))
                continue;

            $content = sprintf("Du lich Tugo tran trong gui den Quy khach %s loi chuc tot dep nhan ngay sinh nhat. Tugo kinh tang quy khach voucher 500.000 khi dat tour Chau Au, Uc, My.", $row['full_name']);

            try {
                $check_sql = "SELECT phone_number FROM ost_sms_log
                    WHERE phone_number LIKE ".db_input($number)."
                    AND type LIKE 'Birthday'
                    AND YEAR(send_time)=".db_input(date('Y'))." LIMIT 1";
                $check_res = db_query($check_sql);

                if ($check_res && ($check_row = db_fetch_array($check_res)) && isset($check_row['phone_number']))
                    throw new Exception('Already Sent');

                $error = [];
                _SMS::send($number, $content, $error, $type = 'Birthday');
                if ($error) var_dump($error);
            } catch (Exception $ex) {
                echo date('Y-m-d H:i:s').': '.$ex->getMessage()."\r\n";
            }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }

            sleep(2);
        }
    }

    function updateTourCountryID() {
        try {
            db_start_transaction();

            $list = DynamicList::lookup(TOUR_LIST_ID);

            foreach ($list->getAllItems() as $item) {
                $country = null;
                $fields = $item->getFieldName(TOUR_PROERTIES_FORM_ID);
                $data = $item->getTableConfiguration();

                foreach ($fields as $_id => $_field) {
                    if (isset($_field['type']) && $_field['type'] == 'files') continue;
                    if ( !( 'destination' === $_field['name'] && (int)$data[$_id]) ) continue;

                    $des = DynamicListItem::lookup((int)$data[$_id]);
                    $des_cfg = $des->getConfiguration();

                    foreach ($des_cfg as $cfg_id => $cfg_value) {
                        $check = DynamicFormField::lookup($cfg_id);
                        if ( !( 'country' === $check->get('name') && is_array($cfg_value) ) ) continue;

                        $array_keys = array_keys($cfg_value);
                        if (!$array_keys) continue;

                        $country = $array_keys[0];
                        break;
                    } // end foreach $des_cfg
                } // end foreach $fields

                if (!$country) continue;

                $sql = " UPDATE ost_tour SET country_id=".db_input((int)$country)
                    ." WHERE id=".db_input($item->getId())." LIMIT 1";
                db_query($sql);
            } // end foreach $list->getAllItems()

            db_commit();
        } catch (Exception $ex) { db_rollback(); }
    } // end function

    function cleanAttachment() {
        include_once INCLUDE_DIR.'class.file.php';

        $sql = "SELECT id FROM ost_file WHERE created<=".db_input(date('Y-m-d', time()-3600*24*OLD_ATTACHMENT_CLEAN));
        $res = db_query($sql);
        if (!$res) return;
        while ($res && ($row = db_fetch_array($res))) {
            if (!isset($row['id']) || !$row['id']) continue;

            $file = new AttachmentFile($row['id']);
            $file->delete();
        }

        AttachmentFile::deleteOrphans();

    }

    function paxReturnSMS() {
        global $cfg;
        $sms_settings = $cfg->getSmsAutoSettings();

        if (FIX_AFTER_SALES_SMS) {
            self::_fix_after_sms($sms_settings);
            return;
        }

        if (isset($sms_settings['after_sms_country_id_hanquoc_content']) && $sms_settings['after_sms_country_id_hanquoc_content']
            && isset($sms_settings['after_sms_country_id_hanquoc']) && $sms_settings['after_sms_country_id_hanquoc']) {
            self::_after_sms($sms_settings['after_sms_country_id_hanquoc'], 'Han Quoc', trim($sms_settings['after_sms_country_id_hanquoc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_nhatban_content']) && $sms_settings['after_sms_country_id_nhatban_content']
            && isset($sms_settings['after_sms_country_id_nhatban']) && $sms_settings['after_sms_country_id_nhatban']) {
            self::_after_sms($sms_settings['after_sms_country_id_nhatban'], 'Nhat Ban', trim($sms_settings['after_sms_country_id_nhatban_content']));
        }

        if (isset($sms_settings['after_sms_country_id_anhquoc_content']) && $sms_settings['after_sms_country_id_anhquoc_content']
            && isset($sms_settings['after_sms_country_id_anhquoc']) && $sms_settings['after_sms_country_id_anhquoc']) {
            self::_after_sms($sms_settings['after_sms_country_id_anhquoc'], 'Anh Quoc', trim($sms_settings['after_sms_country_id_anhquoc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_chauau_content']) && $sms_settings['after_sms_country_id_chauau_content']
            && isset($sms_settings['after_sms_country_id_chauau']) && $sms_settings['after_sms_country_id_chauau']) {
            self::_after_sms($sms_settings['after_sms_country_id_chauau'], 'Chau Au', trim($sms_settings['after_sms_country_id_chauau_content']));
        }

        if (isset($sms_settings['after_sms_country_id_uc_content']) && $sms_settings['after_sms_country_id_uc_content']
            && isset($sms_settings['after_sms_country_id_uc']) && $sms_settings['after_sms_country_id_uc']) {
            self::_after_sms($sms_settings['after_sms_country_id_uc'], 'Uc', trim($sms_settings['after_sms_country_id_uc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_canada_content']) && $sms_settings['after_sms_country_id_canada_content']
            && isset($sms_settings['after_sms_country_id_canada']) && $sms_settings['after_sms_country_id_canada']) {
            self::_after_sms($sms_settings['after_sms_country_id_canada'], 'Canada', trim($sms_settings['after_sms_country_id_canada_content']));
        }
    }

    function _fix_after_sms($sms_settings) {
        echo date('Y-m-d H:i:s').": Fix _fix_after_sms\r\n";
        if (isset($sms_settings['after_sms_country_id_hanquoc_content']) && $sms_settings['after_sms_country_id_hanquoc_content']
            && isset($sms_settings['after_sms_country_id_hanquoc']) && $sms_settings['after_sms_country_id_hanquoc']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_hanquoc'], 'Han Quoc', trim($sms_settings['after_sms_country_id_hanquoc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_nhatban_content']) && $sms_settings['after_sms_country_id_nhatban_content']
            && isset($sms_settings['after_sms_country_id_nhatban']) && $sms_settings['after_sms_country_id_nhatban']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_nhatban'], 'Nhat Ban', trim($sms_settings['after_sms_country_id_nhatban_content']));
        }

        if (isset($sms_settings['after_sms_country_id_anhquoc_content']) && $sms_settings['after_sms_country_id_anhquoc_content']
            && isset($sms_settings['after_sms_country_id_anhquoc']) && $sms_settings['after_sms_country_id_anhquoc']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_anhquoc'], 'Anh Quoc', trim($sms_settings['after_sms_country_id_anhquoc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_chauau_content']) && $sms_settings['after_sms_country_id_chauau_content']
            && isset($sms_settings['after_sms_country_id_chauau']) && $sms_settings['after_sms_country_id_chauau']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_chauau'], 'Chau Au', trim($sms_settings['after_sms_country_id_chauau_content']));
        }

        if (isset($sms_settings['after_sms_country_id_uc_content']) && $sms_settings['after_sms_country_id_uc_content']
            && isset($sms_settings['after_sms_country_id_uc']) && $sms_settings['after_sms_country_id_uc']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_uc'], 'Uc', trim($sms_settings['after_sms_country_id_uc_content']));
        }

        if (isset($sms_settings['after_sms_country_id_canada_content']) && $sms_settings['after_sms_country_id_canada_content']
            && isset($sms_settings['after_sms_country_id_canada']) && $sms_settings['after_sms_country_id_canada']) {
            self::_after_sms_fix($sms_settings['after_sms_country_id_canada'], 'Canada', trim($sms_settings['after_sms_country_id_canada_content']));
        }
    }

    function _after_sms_fix($country_id, $country_name, $content) {
        echo date('Y-m-d H:i:s')." - $country_name\r\n";
        $sql = "SELECT DISTINCT t.pax_id, t.tour_id, t.phone_number
            FROM ost_tour p
                JOIN ost_pax_tour t ON p.id=t.tour_id
            WHERE 1
              and t.phone_number is not null and trim(t.phone_number) <> ''
              AND t.phone_number <> 0
              AND t.visa_result=1
              AND ( after_sales_sms IS NULL OR after_sales_sms = 0 OR after_sales_sms = '' )
              AND datediff(NOW(), p.return_date) >= ".AFTER_SMS_DATE_DIFF."
            and p.country_id=".db_input($country_id)
            ." ORDER BY p.return_date DESC LIMIT  ".AFTER_SMS_LIMIT;

        $res = db_query($sql);
        if (!$res) return;
        while (($row = db_fetch_array($res))) {
            $number = $row['phone_number'];
            if (!_String::simpleCheckMobilePhone($number))
                continue;

            echo "Phone Number: ".$row['phone_number']."\r\n";

            $_sql_select = sprintf(
                "SELECT MAX(id) as id FROM ost_sms_log WHERE phone_number LIKE %s AND type LIKE %s",
                db_input($number),
                db_input('After Sales '.date('Y-m').' '._String::khongdau($country_name))
            );

            $_res = db_query($_sql_select);
            if (!$_res || !($_row = db_fetch_array($_res)) || !isset($_row['id']) || !$_row['id']) continue;

            echo "after_sales_sms: ".$_row['id']."\r\n\r\n";

            $_sql_update = sprintf(
                "UPDATE ost_pax_tour SET after_sales_sms=%s WHERE phone_number LIKE %s AND tour_id=%s
                    AND (after_sales_sms = 0 OR after_sales_sms IS NULL)",
                db_input($_row['id']),
                db_input($row['phone_number']),
                db_input($row['tour_id'])
            );

            db_query($_sql_update);
        } // end while
    }

    function _after_sms($country_id, $country_name, $content) {
        $type = 'After Sales '.date('Y-m').' '._String::khongdau($country_name);
        $type_wildcard = 'After Sales '.date('Y-').'% '._String::khongdau($country_name);
        echo date('Y-m-d H:i:s')." - $type\r\n";

        $sql = "SELECT DISTINCT t.pax_id, t.tour_id, substring(replace(replace(replace(replace(t.phone_number, '`', ''), '\'', ''), '.', ''), ' ', ''), 1, 10) as phone_number
            FROM ost_tour p
                JOIN ost_pax_tour t ON p.id=t.tour_id
            WHERE 1
              and t.phone_number is not null and trim(t.phone_number) <> ''
              AND t.phone_number <> 0
              AND t.visa_result=1
              AND ( after_sales_sms IS NULL OR after_sales_sms = 0 OR after_sales_sms = '' )
              AND datediff(NOW(), p.return_date) >= ".AFTER_SMS_DATE_DIFF."
              and length(replace(replace(replace(replace(phone_number, '`', ''), '\'', ''), '.', ''), ' ', '')) = 10
              and substring(replace(replace(replace(replace(phone_number, '`', ''), '\'', ''), '.', ''), ' ', ''), 2, 1) <> 2
              and substring(replace(replace(replace(replace(phone_number, '`', ''), '\'', ''), '.', ''), ' ', ''), 2, 1) <> 1
            and p.country_id=".db_input($country_id)
            ." ORDER BY p.return_date DESC LIMIT  ".AFTER_SMS_LIMIT;

        $res = db_query($sql);
        if (!$res) return;
        $check_list = [];
        while (($row = db_fetch_array($res))) {
            $number = $row['phone_number'];
            if (!_String::simpleCheckMobilePhone($number))
                continue;

            if ( isset($check_list[ $number ]) && $check_list[ $number ] ) continue;
            try {
                $sms_log_id = 0;
                // kiểm tra có gửi after sales sms cho số này chưa
                $check_sql = "SELECT id, phone_number FROM ost_sms_log
                    WHERE phone_number LIKE ".db_input($number)."
                    AND type LIKE ".db_input($type_wildcard)."
                    AND YEAR(send_time)=".date('Y')." LIMIT 1";

                $check_res = db_query($check_sql);
                if ($check_res && ($check_row = db_fetch_array($check_res)) && isset($check_row['phone_number'])) {
                    echo "Already Sent\r\n";

                    if ($check_row['id']) // lấy SMS log ID nếu đã gửi
                        $sms_log_id = $check_row['id'];
                }

                // chưa có thì gửi SMS
                if (!$sms_log_id) {
                    $error = [];

                    _SMS::send($number, $content, $error, $type);
                    echo date('Y-m-d H:i:s')." - Send SMS to: ".$row['phone_number']."\r\n";
                    if ($error) var_dump($error);

                    $sms_log_id = db_insert_id();
                    var_dump($sms_log_id);
                    var_dump($row['tour_id']);
                    $check_list[ $number ] = $sms_log_id;
                }

                if (!$sms_log_id) throw new Exception('No SMS sent');

                // cập nhật SMS log ID vào bảng pax tour
                echo "UPDATE ost_pax_tour SET after_sales_sms\r\n";
                $sql = sprintf(
                    "UPDATE ost_pax_tour SET after_sales_sms=%s
                        WHERE substring(replace(replace(replace(replace(phone_number, '`', ''), '\'', ''), '.', ''), ' ', ''), 1, 10) LIKE %s
                        AND tour_id=%s
                        AND (after_sales_sms = 0 OR after_sales_sms IS NULL)",
                    db_input($sms_log_id),
                    db_input($row['phone_number']),
                    db_input($row['tour_id'])
                );

                db_query($sql);
            } catch (Exception $ex) {
                echo date('Y-m-d H:i:s').': '.$ex->getMessage()."\r\n";
            }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }

            sleep(2);
        } // end while
    }

    function FBOfflineConversion() {
        global $argv;

        $diff = isset($argv[1]) && is_numeric($argv[1]) ? (int)$argv[1] : 2;

        ini_set('display_errors', 0);
        error_reporting(~E_ALL);

        $file = INCLUDE_DIR.'../fb_data/fb_offline_data.csv';
        $fp = fopen($file, 'w');
        fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        $headers = [
            "match_keys.phone",
            "match_keys.country",
            "event_name",
            "event_time",
            "value",
            "currency",
            "order_id",
        ];
        fputcsv($fp, $headers);

        $sql = "select p.full_name, pt.phone_number, b.total_quantity, b.total_retail_price, p.dob, p.gender, b.created, b.booking_code
            from ost_pax p join ost_pax_tour pt  ON p.id=pt.pax_id
            join booking_view b
            on (trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM b.booking_code))) LIKE (trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM pt.booking_code)))
            WHERE 1
              and
                  b.created >= '".date('Y-m-d', time()-24*3600*$diff)."'
              and b.status LIKE '%\"53\"%'
              and pt.phone_number <> '0' and pt.phone_number is not null and pt.phone_number <> ''
            group by p.id, pt.booking_code
            order by  p.full_name";
        $res = db_query($sql);

        if ($res) {
            $phone_check = [];

            while ($res && ($row = db_fetch_array($res))) {
                $phone_numbers = _String::splitAndNormalize($row['phone_number']);

                if (!$phone_numbers) continue;
                if (!is_array($phone_numbers) && is_numeric($phone_numbers)) $phone_numbers = [ $phone_numbers ];
                $datetime = new DateTime($row['created'], new DateTimeZone('+0700'));
                $created = $datetime->format(DateTime::ATOM); // Updated ISO8601

                if ($phone_numbers) {
                    foreach ($phone_numbers as $phone) {
                        if (in_array($phone, $phone_check)) continue;
                        $phone_check[] = $phone;

                        $sql = "SELECT count(id) FROM fb_offline_event_upload WHERE phone_number LIKE ".db_input((string)$phone). " AND event_time LIKE ".db_input($created);
                        $count = db_count($sql);
                        if (is_numeric($count) && $count > 0) continue;

                        fputcsv($fp, [
                            (string)$phone,
                            'VN',
                            'Purchase',
                            $created,
                            $row['total_retail_price'],
                            'VND',
                            $row['booking_code'],
                        ]);

                        $sql = "INSERT IGNORE INTO fb_offline_event_upload SET phone_number=".db_input((string)$phone).", event_time=".db_input($created);
                        db_query($sql);
                    }
                }
            }
        }

        $sql = "select v.value as phone_number, b.total_quantity, b.total_retail_price, t.closed as created, b.booking_code
            from ost_ticket t join ost_user u ON t.user_id=u.id
            join ost_form_entry e on u.id=e.object_id and e.object_type LIKE 'U'
            join ost_form_entry_values v on e.id=v.entry_id and v.field_id=".USER_PHONE_NUMBER_FIELD."
            join booking_view b on t.number LIKE b.ticket_number
            where t.closed>='".date('Y-m-d', time()-24*3600*$diff)."'
            and t.status_id=".TICKET_STATUS_SUCCESS."
            and v.value is not null and length(v.value)=10";


        $res = db_query($sql);

        if ($res) {
            $phone_check = [];

            while ($res && ($row = db_fetch_array($res))) {
                $phone_numbers = _String::splitAndNormalize($row['phone_number']);

                if (!$phone_numbers) continue;
                if (!is_array($phone_numbers) && is_numeric($phone_numbers)) $phone_numbers = [ $phone_numbers ];
                $datetime = new DateTime($row['created'], new DateTimeZone('+0700'));
                $created = $datetime->format(DateTime::ATOM); // Updated ISO8601

                if ($phone_numbers) {
                    foreach ($phone_numbers as $phone) {
                        if (in_array($phone, $phone_check)) continue;
                        $phone_check[] = $phone;

                        $sql = "SELECT count(id) FROM fb_offline_event_upload WHERE phone_number LIKE ".db_input((string)$phone). " AND event_time LIKE ".db_input($created);
                        $count = db_count($sql);
                        if (is_numeric($count) && $count > 0) continue;

                        fputcsv($fp, [
                            (string)$phone,
                            'VN',
                            'Purchase',
                            $created,
                            $row['total_retail_price'],
                            'VND',
                            $row['booking_code'],
                        ]);

                        $sql = "INSERT IGNORE INTO fb_offline_event_upload SET phone_number=".db_input((string)$phone).", event_time=".db_input($created);
                        db_query($sql);
                    }
                }
            }
        }

        fclose($fp);
    }

    function TagMaker() {
        require_once(INCLUDE_DIR.'class.tag.php');
        global $argv;

        $offset = $count = 0;
        if (isset($argv[1]) && (int)$argv[1]) $offset = (int)$argv[1];
        $sql = "
            select distinct t.ticket_id, s.title, s.content
            from ost_ticket t
                   left join ost_ticket_tag tt on t.ticket_id = tt.ticket_id
                   JOIN ost__search s on t.ticket_id = s.object_id and object_type = 'T'
            where tt.ticket_id is null
              and t.dept_id IN (". (
            (is_array(unserialize(SALE_DEPT)) && count(unserialize(SALE_DEPT)))
                ? implode(',', unserialize(SALE_DEPT))
                : '0'
            ).")
            order by t.ticket_id desc
            limit 10000 offset  ".$offset."
        ";

        $res = db_query($sql);
        while($res && ($row = db_fetch_array($res))) {
            $tags = TagIndexing::getAllTags($row['title']);
            if (!$tags || !count($tags)) {
                $tags = TagIndexing::getAllTags($row['content']);
            }

            if ($tags && count($tags)) {
                Tag::update($row['ticket_id'], $tags);
                $count++;
            }
        }

        echo "Updated $count tickets\r\n";
    }

    function EmaticMC() {
        global $argv;

        require_once(INCLUDE_DIR.'class.ematic.php');

        $days = isset($argv[1]) && is_numeric($argv[1]) ? (int)trim($argv[1]) : 1;

        $sql = "SELECT title FROM ".TICKET_THREAD_TABLE." WHERE (thread_type='R' or thread_type='M')
            and date(created) >= date_sub(date(now()), INTERVAL $days day);";
        $res = db_query($sql);
        $count = 0;
        $x = 0;
        echo "___________________\r\n".date('Y-m-d H:i:s')."\r\n";
        while($res && ($row = db_fetch_array($res))) {
            $text = [];
            preg_match('/\[(.*)\]/', $row['title'], $text);
            if (!$text || !isset($text[1]) || empty(trim($text[1]))) continue;

            $email = trim($text[1]);
            if (strpos($email, '@') === false) continue;

            $text = [];
            $name = "";
            preg_match('/\:(.*)\[/', $row['title'], $text);
            if ($text && isset($text[1]) && !empty($text[1]))
                $name = trim($text[1]);

            $fields = [
                'fullname' => $name,
                'address' => "HCMC",
            ];

            $error = [];
            $result = Ematic::mailchimpMembers($email, $fields, $error);
            echo ++$x.": $email : ";
            if (!$result && $error) echo array_pop($error);
            else echo "OK";

            echo "\r\n";

            if ($count++ == 20) {
                $count = 0;
                sleep(2);
            }
        }
        echo "\r\n";
        echo date('Y-m-d H:i:s')." - Total: $x emails\r\n___________________\r\n";
    }

    function bookingPhonenumber() {
        /*
         * select phone_number, count(distinct date(date)) as count, GROUP_CONCAT(date(date)) as date
from booking_phone_number group by phone_number having count>=2;

        select x_date, sum(if(x_count>0,1, 0))
from (
     select date(r.date)               x_date,
            (select count(*)
             from tmp_return a
             where a.name = r.name
               and a.date < r.date) as x_count
     from tmp_return r
     )  as a group by date(x_date);

        select x_date, sum(if(x_count>0,1, 0))
from (
     select date(r.date)               x_date,
            (select count(*)
             from booking_phone_number a
             where a.phone_number like r.phone_number
               and a.date < r.date) as x_count
     from booking_phone_number r
     )  as a group by date(x_date);
         */
        $count = 0;

        $sql = "select ticket_id, booking_code, phone_number, customer, date(created) as date from booking_tmp
            where status like '{\"53\":\"%'
            and phone_number <> '0902582102'
            and created is not null
            and created <> '' and created <> 0";
        $res = db_query($sql);

        $sql_insert = "INSERT IGNORE INTO booking_phone_number
            SET ticket_id = %s, phone_number = %s, date = %s, booking_code = %s";
        while ($res && ($row = db_fetch_array($res))) {
            $phones_a = _String::getPhoneNumbers($row['phone_number']);
            $phones_b = _String::getPhoneNumbers($row['customer']);

            if (!is_array($phones_a)) $phones_a = [];
            if (!is_array($phones_b)) $phones_b = [];
            $phones = array_merge($phones_a, $phones_b);

            if (!is_array($phones)) $phones = [];
            $phones = array_unique(array_filter($phones));

            if (!$phones) continue;

            foreach ($phones as $number) {
                echo ++$count."\r";
                db_query(sprintf(
                    $sql_insert,
                    db_input($row['ticket_id']),
                    db_input($number),
                    db_input($row['date']),
                    db_input($row['booking_code'])
                ));
            } // foreach
        } // while

        $sql = "select b.ticket_id, b.booking_code, t.phone_number, date(b.created) as date from ost_pax_tour t
            join booking_tmp b on trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM b.booking_code))
            like trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM t.booking_code))
            where b.status like '{\"53\":\"%'
            and t.phone_number <> '0902582102'
            and b.created is not null
            and b.created <> '' and b.created <> 0 and t.phone_number <> 0 and t.phone_number is not null
            and t.phone_number <> ''";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $phones = _String::getPhoneNumbers($row['phone_number']);
            if (!is_array($phones)) $phones = [];
            $phones = array_unique(array_filter($phones));

            if (!$phones) continue;

            foreach ($phones as $number) {
                echo ++$count."\r";
                db_query(sprintf(
                    $sql_insert,
                    db_input($row['ticket_id']),
                    db_input($number),
                    db_input($row['date']),
                    db_input($row['booking_code'])
                ));
            } // foreach
        } // while

        echo "\r\nTotal: $count\r\n";
    }

    function phone11() {
        $prefix = [
            "0123" => "083",
            "0124" => "084",
            "0125" => "085",
            "0127" => "081",
            "0129" => "082",
            "0120" => "070",
            "0121" => "079",
            "0122" => "077",
            "0126" => "076",
            "0128" => "078",
            "0162" => "032",
            "0163" => "033",
            "0164" => "034",
            "0165" => "035",
            "0166" => "036",
            "0167" => "037",
            "0168" => "038",
            "0169" => "039",
            "0186" => "056",
            "0188" => "058",
            "0199" => "059",
        ];

        self::_migrate_phone_11_10_index($prefix);
        self::_migrate_phone_11_10_pax_tour($prefix);
        self::_migrate_phone_11_10_form($prefix);
        self::_migrate_phone_11_10_api_user($prefix);
        self::_migrate_phone_11_10_phone_number($prefix);
    }

    function _migrate_phone_11_10_index($prefix) {
        $sql = "select  COUNT( phone_number) from ost_phone_number_index
            where phone_number like '01%' and length(phone_number)=11;";
        $total = db_count($sql);
        echo "ost_phone_number_index, Total: $total\r\n";

        if ($total) {
            $sql = "select  id, phone_number from ost_phone_number_index
            where phone_number like '01%' and length(phone_number)=11;";
            $res = db_query($sql);
            $count = 0;

            while ($res && ($row = db_fetch_array($res))) {
                $count++;
                foreach($prefix as $old => $new) {
                    if (
                        preg_match('/^'.$old.'/', $row['phone_number'])
                        || preg_match('/^'.$old.'/', '0'.$row['phone_number'])
                    ) {
                        $new_number = $new.substr(preg_replace('/^0/', '', $row['phone_number']), 3);
                        $sql_migrate = "UPDATE ost_phone_number_index SET phone_number=".db_input($new_number)." WHERE id=".db_input($row['id']);
                        try {
                            db_query($sql_migrate);
                        } catch (Exception $ex) { };
                    }
                }

                echo $count."/".$total."\r";
            } // while
        } // if

        db_query("DELETE FROM ost_phone_number_index WHERE length(phone_number)<>10");
        echo "\r\nDONE ost_phone_number_index\r\n";
    }

    function _migrate_phone_11_10_phone_number($prefix) {
        $sql = "select  phone_number from phone_number
            WHERE
                ( (length(phone_number) = 9 and phone_number LIKE '9%') OR (length(phone_number) = 10 and phone_number LIKE '1%') )";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $new_number = '0'.$row['phone_number'];
            $sql_migrate = "UPDATE phone_number SET phone_number=".db_input($new_number)
                ." WHERE phone_number=".db_input($row['phone_number']);
            try {
                db_query($sql_migrate);
            } catch (Exception $ex) { };
        }

        $sql = "select  COUNT( phone_number) from phone_number
            where phone_number like '01%' and length(phone_number)=11;";
        $total = db_count($sql);
        echo "phone_number, Total: $total\r\n";

        if ($total) {
            $sql = "select  phone_number from phone_number
            where phone_number like '01%' and length(phone_number)=11;";
            $res = db_query($sql);
            $count = 0;

            while ($res && ($row = db_fetch_array($res))) {
                $count++;
                foreach($prefix as $old => $new) {
                    if (
                        preg_match('/^'.$old.'/', $row['phone_number'])
                        || preg_match('/^'.$old.'/', '0'.$row['phone_number'])
                    ) {
                        $new_number = $new.substr(preg_replace('/^0/', '', $row['phone_number']), 3);
                        $sql_migrate = "UPDATE IGNORE phone_number SET phone_number=".db_input($new_number)." WHERE phone_number=".db_input($row['phone_number']);
                        try {
                            db_query($sql_migrate);
                        } catch (Exception $ex) { };
                    }
                }

                echo $count."/".$total."\r";
            } // while
        } // if

        db_query("DELETE FROM phone_number WHERE length(phone_number)<>10");
        echo "\r\nDONE phone_number\r\n";
    }

    function _migrate_phone_11_10_api_user($prefix) {
        $sql = "select  COUNT( phone_number) from api_user
            where phone_number like '01%' and length(phone_number)=11;";
        $total = db_count($sql);
        echo "api_user, Total: $total\r\n";

        if ($total) {
            $sql = "select  uuid, phone_number from api_user
            where phone_number like '01%' and length(phone_number)=11;";
            $res = db_query($sql);
            $count = 0;

            while ($res && ($row = db_fetch_array($res))) {
                $count++;
                foreach($prefix as $old => $new) {
                    if (
                        preg_match('/^'.$old.'/', $row['phone_number'])
                        || preg_match('/^'.$old.'/', '0'.$row['phone_number'])
                    ) {
                        $new_number = $new.substr(preg_replace('/^0/', '', $row['phone_number']), 3);
                        $sql_migrate = "UPDATE api_user SET phone_number=".db_input($new_number)." WHERE uuid=".db_input($row['uuid']);
                        try {
                            db_query($sql_migrate);
                        } catch (Exception $ex) { };
                    }
                }

                echo $count."/".$total."\r";
            } // while
        } // if

        echo "\r\nDONE api_user\r\n";
        ////////////////
        $sql = "select  COUNT( phone_number) from api_cloud_message
            where phone_number like '01%' and length(phone_number)=11;";
        $total = db_count($sql);
        echo "api_cloud_message, Total: $total\r\n";

        if ($total) {
            $sql = "select  id, phone_number from api_cloud_message
            where phone_number like '01%' and length(phone_number)=11;";
            $res = db_query($sql);
            $count = 0;

            while ($res && ($row = db_fetch_array($res))) {
                $count++;
                foreach($prefix as $old => $new) {
                    if (
                        preg_match('/^'.$old.'/', $row['phone_number'])
                        || preg_match('/^'.$old.'/', '0'.$row['phone_number'])
                    ) {
                        $new_number = $new.substr(preg_replace('/^0/', '', $row['phone_number']), 3);
                        $sql_migrate = "UPDATE api_cloud_message SET phone_number=".db_input($new_number)." WHERE id=".db_input($row['id']);
                        try {
                            db_query($sql_migrate);
                        } catch (Exception $ex) { };
                    }
                }

                echo $count."/".$total."\r";
            } // while
        } // if

        echo "\r\nDONE api_cloud_message\r\n";
    }

    function _migrate_phone_11_10_pax_tour($prefix) {
        db_query("UPDATE ost_pax_tour SET phone_number=REPLACE(REPLACE(phone_number, ' ', ''), '.', '')");

        $sql = "select  pax_id, tour_id, phone_number from ost_pax_tour
            WHERE length(phone_number) <> 10 and phone_number <> '0'";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $new_number = preg_replace('/[^0-9]/', '', $row['phone_number']);
            $sql_migrate = "UPDATE ost_pax_tour SET phone_number=".db_input($new_number)
                ." WHERE pax_id=".db_input($row['pax_id'])
                ." AND tour_id=".db_input($row['tour_id']);
            try {
                db_query($sql_migrate);
            } catch (Exception $ex) { };
        }

        $sql = "select  pax_id, tour_id, phone_number from ost_pax_tour
            WHERE (length(phone_number) = 9 and phone_number LIKE '9%') OR (length(phone_number) = 10 and phone_number LIKE '1%')";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $new_number = '0'.$row['phone_number'];
            $sql_migrate = "UPDATE ost_pax_tour SET phone_number=".db_input($new_number)
                ." WHERE pax_id=".db_input($row['pax_id'])
                ." AND tour_id=".db_input($row['tour_id']);
            try {
                db_query($sql_migrate);
            } catch (Exception $ex) { };
        }

        $sql = "select  COUNT( phone_number) from ost_pax_tour
            where phone_number like '01%' and length(phone_number)=11;";
        $total = db_count($sql);
        echo "ost_pax_tour, Total: $total\r\n";

        if ($total) {
            $sql = "select  pax_id, tour_id, phone_number from ost_pax_tour
            where phone_number like '01%' and length(phone_number)=11;";
            $res = db_query($sql);
            $count = 0;

            while ($res && ($row = db_fetch_array($res))) {
                $count++;
                foreach($prefix as $old => $new) {
                    if (
                        preg_match('/^'.$old.'/', $row['phone_number'])
                        || preg_match('/^'.$old.'/', '0'.$row['phone_number'])
                    ) {
                        $new_number = $new.substr(preg_replace('/^0/', '', $row['phone_number']), 3);
                        $sql_migrate = "UPDATE ost_pax_tour SET phone_number=".db_input($new_number)
                            ." WHERE pax_id=".db_input($row['pax_id'])
                            ." AND tour_id=".db_input($row['tour_id']);
                        try {
                            db_query($sql_migrate);
                        } catch (Exception $ex) { };
                    }
                }

                echo $count."/".$total."\r";
            } // while
        } // if

        echo "\r\nDONE ost_pax_tour\r\n";
    }

    function _migrate_phone_11_10_form($prefix) {
        db_query("UPDATE ost_form_entry_values SET `value`=REPLACE(REPLACE(`value`, ' ', ''), '.', '') where field_id=3");

        $sql = "select  entry_id, value from ost_form_entry_values
            WHERE field_id=3 and value <> '1900555543' and
                ( (length(value) = 9 and value LIKE '9%') OR (length(value) = 10 and value LIKE '1%') )";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $new_number = '0'.$row['value'];
            $sql_migrate = "UPDATE ost_form_entry_values SET value=".db_input($new_number)
                ." WHERE field_id=3 and entry_id=".db_input($row['entry_id']);
            try {
                db_query($sql_migrate);
            } catch (Exception $ex) { };
        }

        $sql = "select  COUNT( value) from ost_form_entry_values
            where field_id=3 and value <> '1900555543' and value like '01%' and length(value)=11;";
        $total = db_count($sql);
        echo "ost_form_entry_values, Total: $total\r\n";

        if (!$total) return;

        $sql = "select  entry_id, value from ost_form_entry_values
            where field_id=3 and value <> '1900555543' and value like '01%' and length(value)=11;";
        $res = db_query($sql);
        if (!$res) return;

        $count = 0;

        while (($row = db_fetch_array($res))) {
            $count++;
            foreach($prefix as $old => $new) {
                if (
                    preg_match('/^'.$old.'/', $row['value'])
                    || preg_match('/^'.$old.'/', '0'.$row['value'])
                ) {
                    $new_number = $new.substr(preg_replace('/^0/', '', $row['value']), 3);
                    $sql_migrate = "UPDATE ost_form_entry_values SET value=".db_input($new_number)
                        ." WHERE field_id=3 and entry_id=".db_input($row['entry_id']);
                    try {
                        db_query($sql_migrate);
                    } catch (Exception $ex) { };
                }
            }

            echo $count."/".$total."\r";
        }

        echo "\r\nDONE ost_form_entry_values\r\n";
    }

    function TuThien() {
        // ---------------------------------
        // Select data to table
        // ---------------------------------
        if (!defined('TUTHIEN_TOURS'))
            return;

        $tours = unserialize(TUTHIEN_TOURS);
        if (!$tours || !is_array($tours) || !array_filter($tours)) return;
        $tours = array_filter($tours);

        $sql = "
            SELECT A.*
            FROM (
                     SELECT p.id    as pax_id,
                            p.full_name,
                            p.dob, pt.tour_id
                     FROM ost_pax p
                              JOIN ost_pax_tour pt ON p.id = pt.pax_id
                     WHERE pt.tour_id IN (
                         select id
                         from ost_tour
                         where gather_date >= '".TUTHIEN_START_TIME."'
                           AND name NOT LIKE '%ROT VISA%'
                           and destination IN (select id
                                               from ost_list_items
                                               where id IN ("
                                            .implode(',', $tours)
                                            ."))
                     )
                 ) AS A
                     LEFT JOIN mkt_tuthien B ON A.pax_id = B.pax_id AND A.tour_id = B.tour_id
            WHERE B.pax_id IS NULL
              AND B.tour_id IS NULL;
        ";

        $tour_sql = "select id, gather_date, destination
                         from ost_tour
                         where gather_date >= '".TUTHIEN_START_TIME."'
                           AND name NOT LIKE '%ROT VISA%'
                           and destination IN (select id
                                               from ost_list_items
                                               where id IN ("
            .implode(',', $tours)
            ."))";

        $res_tour = db_query($tour_sql);
        $tour_list = [];
        while($res_tour && ($row = db_fetch_array($res_tour))) {
            $tour_list[ $row['id'] ] = $row;
        }

        $des_sql = "select id, value
               from ost_list_items
               where id IN ("
            .implode(',', $tours)
            .")";

        $res_des = db_query($des_sql);
        $des_list = [];
        while($res_des && ($row = db_fetch_array($res_des))) {
            $des_list[ $row['id'] ] = $row['value'];
        }

        echo $sql."\r\n";

        $res = db_query($sql);
        if (!$res) {
            echo "No data to do\r\n";
            return;
        }

        // ---------------------------------
        // Export file to CSV
        // ---------------------------------
        $filename = ROOT_DIR.'public_file/tuthien.csv';

        if (file_exists($filename))
            @unlink($filename);

        $fp = @fopen($filename, 'w');
        if (!$fp) {
            echo "Cannot open file for writting\r\n";
            return;
        }

        fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        $headers = [
            'pax_id',
            'tour_id',
            'full_name',
            'dob',
            'tour_name',
            'gather_date',
        ];
        fputcsv($fp, $headers);
        $sql_insert = "REPLACE INTO mkt_tuthien(
            pax_id,
            tour_id,
            full_name,
            dob,
            tour_name,
            gather_date
            ) VALUES ";

        $flag = false;
        while ($res && ($row = db_fetch_array($res))) {
            try {
                if (!date_create_from_format('Y-m-d', $row['dob'])) continue;
                if (!date_create_from_format('Y-m-d H:i:s', $tour_list[ $row['tour_id'] ]['gather_date'])) continue;
                $name = substr(trim($row['full_name']), 0, 64);

                $csv_row = [
                    $row['pax_id'],
                    $row['tour_id'],
                    $name,$row['dob'],
                    $des_list[ $tour_list[ $row['tour_id'] ]['destination'] ],
                    $tour_list[ $row['tour_id'] ]['gather_date'],
                ];

                $sql_insert .= sprintf(
                    "(
                       %d,
                       %d,
                       %s,
                       %s,
                       %s,
                       %s
                    ),",
                    db_input((int)$row['pax_id'], false),
                    db_input((int)$row['tour_id'], false),
                    db_input($name),
                    db_input($row['dob']),
                    db_input($des_list[ $tour_list[ $row['tour_id'] ]['destination'] ]),
                    db_input($tour_list[ $row['tour_id'] ]['gather_date'])
                );

                fputcsv($fp, $csv_row);
                $flag = true;
            } catch(Exception $ex) { echo $ex->getMessage()."\r\n"; }
        }

        fclose($fp);

        if ($flag) {
            $sql_insert = trim($sql_insert, ',');
            echo $sql_insert."\r\n";
            db_query($sql_insert);
        }

        echo "DONE: ".date("Y-m-d H:i:s")."\r\n";
    }

    function __unassign_no_answer_1st() {
        echo "__unassign_no_answer_1st\r\n";
        // update khong nghe may lan 1
        $sql = ' SELECT ticket_id
            FROM ost_ticket t
            WHERE 1
              AND updated IS NOT NULL
              AND updated >= \''.date('Y-m-d H:i:s', time()-3600*24*30 ).'\'
              AND (
                TIMESTAMPDIFF(SECOND, updated, NOW()) >= 3600 * '.config('unassign_no_answer_1st').'
              )
              AND status_id = '.NO_ANSWER_1ST_STATUS.'
              AND topic_id IN ('.implode(',', unserialize(SALE_TOPIC)).')
              AND staff_id<>0 AND staff_id IS NOT NULL
              AND (unassign='.db_input(TicketUnassign::NONE).' OR unassign IS NULL OR unassign LIKE \'\')
              ORDER BY updated DESC
            LIMIT  '.UNASSIGN_CRON_LIMIT;

        echo $sql."\r\n";

        $res = db_query($sql);
        $status = TicketStatus::lookup(NO_ANSWER_1ST_STATUS);
        $status_name = $status->getName();
        $check = false;
        while($res && ($row = db_fetch_array($res))) {
            $check = true;
            if (!isset($row['ticket_id']) || !is_numeric($row['ticket_id']))
                continue;

            $ticket = new Ticket($row['ticket_id']);
            if (!$ticket) continue;

            try {
                db_start_transaction();

                echo date('Y-m-d H:i:s')." - Ticket ID: ".$row['ticket_id']."\r\n";
                $errors = [];
                $ticket->postNote(
                    [
                        'note' => '<p>Ticket was automatically unassigned</p>
                    <p><strong>By Rule:</strong> Status <em>'.$status_name.'</em> @ '.config('unassign_no_answer_1st').' hours</p>
                    <p><strong>Last Staff:</strong> '
                            .$ticket->getAssignee().'</p>',
                        'title' => 'Automatically Unassign'
                    ], $errors, 'Cronjob'
                );

                $ticket->setUnassign(TicketUnassign::NO_ANSWER_1ST);
                $ticket->setStatus(OPENING_STATUS);
                $ticket->unassign();

                db_commit();
            } catch (Exception $ex) { db_rollback(); }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }
        } // end while

        return $check;
    }

    function __unassign_overdue() {
        // update overdue
        echo "__unassign_overdue\r\n";
        $sql = ' SELECT ticket_id
            FROM ost_ticket t
            WHERE 1
              AND t.updated IS NOT NULL
              AND t.updated >= \''.date('Y-m-d H:i:s', time()-3600*24*30 ).'\'
              AND (
                TIMESTAMPDIFF(SECOND, updated, NOW()) >= 3600 * '.config('unassign_overdue').'
              )
              AND isoverdue=1
              AND topic_id IN ('.implode(',', unserialize(SALE_TOPIC)).')
              AND staff_id<>0 AND staff_id IS NOT NULL
              AND (unassign='.db_input(TicketUnassign::NONE).' OR unassign IS NULL OR unassign LIKE \'\')
              ORDER BY IFNULL(updated, IFNULL(lastmessage, IFNULL(lastresponse, created))) DESC
            LIMIT  '.UNASSIGN_CRON_LIMIT;

        echo $sql."\r\n";

        $res = db_query($sql);
        $check = false;
        while($res && ($row = db_fetch_array($res))) {
            $check = true;
            if (!isset($row['ticket_id']) || !is_numeric($row['ticket_id']))
                continue;

            $ticket = new Ticket($row['ticket_id']);
            if (!$ticket) continue;

            try {
                db_start_transaction();

                echo date('Y-m-d H:i:s')." - Ticket ID: ".$row['ticket_id']."\r\n";
                $errors = [];
                $status_name = $ticket->getStatus();
                $ticket->postNote(
                    [
                        'note' => '<p>Ticket was automatically unassigned</p>
                    <p><strong>By Rule:</strong> <em>Overdue</em> @ '.( (int)config('unassign_overdue') /24 ).' days</p>
                    <p><strong>Last Staff:</strong> '
                            .$ticket->getAssignee().'</p>
                    <p><strong>Last Status:</strong> '
                            . $status_name.'</p>',
                        'title' => 'Automatically Unassign'
                    ], $errors, 'Cronjob'
                );

                $ticket->setUnassign(TicketUnassign::OVERDUE_5D);
                $ticket->setStatus(OPENING_STATUS);
                $ticket->unassign();

                db_commit();
            } catch(Exception $ex) { db_rollback(); }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }
        } // end while

        return $check;
    }

    function __unassign_welcome() {
        echo "__unassign_welcome\r\n";
        $sql = "
            SELECT ticket_id, unassign
            FROM ost_ticket t
            WHERE 1
              AND t.updated IS NOT NULL
              AND t.updated >= '".date('Y-m-d H:i:s', time()-3600*24*30 )."'
              AND (
                TIMESTAMPDIFF(SECOND, t.updated, NOW()) >= 3600 * ".config('unassign_transfer')."
              )
              AND topic_id IN (".implode(',', unserialize(SALE_TOPIC)).")
              AND t.status_id IN (
              select ts.id
              FROM ost_ticket_status ts
              WHERE ts.state = 'open'
            )
              AND (t.staff_id IS NULL OR t.staff_id = 0 OR t.staff_id LIKE '')
              AND (unassign IN (".implode(',', [
                TicketUnassign::SMS,
                TicketUnassign::OVERDUE_5D,
                TicketUnassign::NO_ANSWER_1ST,
            ]).") )
            LIMIT ".UNASSIGN_WELCOME_CRON_LIMIT;

        echo $sql."\r\n";

        $res = db_query($sql);
        $check = false;
        while($res && ($row = db_fetch_array($res))) {
            $check = true;
            if (!isset($row['ticket_id']) || !is_numeric($row['ticket_id']))
                continue;

            $ticket = new Ticket($row['ticket_id']);
            if (!$ticket) continue;

            try {
                db_start_transaction();

                echo date('Y-m-d H:i:s')." - Ticket ID: ".$row['ticket_id']."\r\n";

                if (TicketUnassign::NO_ANSWER_1ST == $row['unassign']) {
                    $rule = 'Gọi khách không nghe máy và không xử lý tiếp, sau '
                        .( (int)config('unassign_no_answer_1st') + 24 ).' giờ';

                } elseif (TicketUnassign::OVERDUE_5D == $row['unassign']) {
                    $rule = 'Ticket overdue và không xử lý tiếp, sau '
                        .( (int)config('unassign_overdue') /24+1 ).' ngày';

                } elseif (TicketUnassign::SMS == $row['unassign']) {
                    $rule = 'Ticket mới và không ai nhận về xử lý, sau '
                        .( (int)config('unassign_no_answer_1st') /24+1 ).' ngày';

                } else {
                    throw new Exception('No matching rule.');
                }

                $errors = [];
                echo "Post note\r\n";
                $ticket->postNote(
                    [
                        'note' => '<p>Ticket was automatically closed and transfer to Welcome</p>
                    <p><strong>By Rule:</strong> <em>'.$rule.'</em></p>',
                        'title' => 'Automatically Closed and Transfer to Welcome'
                    ], $errors, 'Cronjob'
                );

                $ticket->setUnassign(TicketUnassign::WELCOME);
                $ticket->setStatus(CLOSED_STATUS);

                self::__ticket_to_welcome($ticket);

                db_commit();
            } catch (Exception $ex) { db_rollback(); }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }
        } // end while

        return $check;
    }

    function __ticket_to_welcome($ticket) {
        $text = '';
        $threads = $ticket->getThreadEntries(false);

        foreach($threads as $thread) {
            $text .= "<h3>".($thread['title'] ?: 'Internal Note')."<small> &bull; by ".$thread['poster']." @".$thread['created']."</small></h3>";

            if ('html' === $thread['format']) {
                $html = $thread['body'];
                $body = $html->display();
            } else {
                $body = $thread['body'];
            }

            $text .= "<div>".$body."</div>";
            $text .= "<hr />";
        } // end foreach

        $text; // TODO: send email to welcome
        $subject = $ticket->getSubject();
        $vars = [
            'poster' => 'Cronjob',
            'handovered' => true,
            'subject' => $subject,
            'response' => $text,
        ];
        $errors = [];
        $to = WELCOME_SUPPORT_EMAIL; //

        $ticket->postReply($vars, $errors, $to);
        if ($errors) {
            $text = '';

            foreach ($errors as $key => $value) {
                $text .= "$key: $value\r\n";
            }

            throw new Exception($text);
        }
    }

    function __no_action_sms() {
        // update overdue
        echo "__no_action_sms\r\n";

        $sql = "SELECT DISTINCT t.ticket_id, v.value
            FROM ost_ticket t
            JOIN ost_user u ON t.user_id = u.id
              AND topic_id IN (".implode(',', unserialize(SALE_TOPIC)).")
              AND (
                  TIMESTAMPDIFF(SECOND,
                    IFNULL(t.updated, IFNULL(lastmessage, IFNULL(lastresponse, t.created))),
                    NOW())
                  >= 3600 * ".config('no_action_sms')."
                )
            JOIN ost_form_entry e ON u.id = e.object_id AND e.object_type = 'U'
            JOIN ost_form_entry_values v ON e.id = v.entry_id
              AND v.field_id = ".USER_PHONE_NUMBER_FIELD."
              AND v.value <> '0'
              AND v.value <> ''
              AND v.value IS NOT NULL
            WHERE 1
              AND IFNULL(t.updated, IFNULL(lastmessage, IFNULL(lastresponse, t.created)))
                >= '".date('Y-m-d H:i:s', time()-3600*24*30 )."'
              AND t.status_id IN (
                select ts.id FROM ost_ticket_status ts WHERE ts.state='open'
              )
              AND ( t.staff_id IS NULL OR t.staff_id = 0 OR t.staff_id LIKE '')
              AND (unassign=".TicketUnassign::NONE." OR unassign LIKE '' OR unassign IS NULL)
              AND (isoverdue=0 OR isoverdue IS NULL OR isoverdue LIKE '')
              AND (
                  (t.created = t.updated OR t.updated IS NULL OR t.updated LIKE '')
                OR
                  v.value NOT IN (
                    SELECT destinationnumber FROM ost_call_log lg WHERE 1
                      AND lg.add_time >='".date('Y-m-d H:i:s', time()-3600*24*30)."'
                      AND (v.value LIKE lg.destinationnumber OR v.value LIKE CONCAT('0', lg.destinationnumber) )
                                                                    AND t.created < lg.starttime
                                                                    AND lg.direction LIKE 'outbound'
                  )
              )
              ORDER BY IFNULL(t.updated, IFNULL(lastmessage, IFNULL(lastresponse, t.created))) DESC
            LIMIT ".UNASSIGN_CRON_LIMIT;

        echo $sql."\r\n";

        $res = db_query($sql);
        $check = false;
        $sms = NO_ACTION_SMS;
        while($res && ($row = db_fetch_array($res))) {
            $check = true;
            if (!isset($row['ticket_id']) || !is_numeric($row['ticket_id']))
                continue;

            if (empty($row['value']) || !$row['value'])
                continue;

            $phones = _String::getPhoneNumbers($row['value']);
            if (is_array($phones) && count($phones) && ( $phones = array_unique(array_filter($phones)) ))
                $phones = $phones[0];

            if (!_String::isMobileNumber($phones)) continue;

            $ticket = new Ticket($row['ticket_id']);
            if (!$ticket) continue;

            try {
                db_start_transaction();

                echo date('Y-m-d H:i:s')." - Ticket ID: ".$row['ticket_id']."\r\n";
                $error = [];
                $errors = [];

                $ticket->postNote(
                    [
                        'note' => '<p>SMS was sent automatically</p>
                    <p><strong>By Rule:</strong> <em>No Action on new Ticket</em> @ '.config('no_action_sms').' hours</p>
                    <hr />
                    <p><strong>SMS:</strong> '
                            .str_replace("\n", "<br />", $sms).'</p>',
                        'title' => 'Automatically SMS to ['.$phones.']'
                    ], $errors, 'Cronjob'
                );

                $ticket->setUnassign(TicketUnassign::SMS);

                echo "Send SMS to $phones\r\n";
                _SMS::send($phones, $sms, $error, 'No Action', $row['ticket_id']);
                sleep(1);

                db_commit();
            } catch (Exception $ex) { db_rollback(); }

            if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                break;
            }
        } // end while

        return $check;
    }

    function noActionSMS() {
        do {
            $check = false;
            try {
                $check = self::__no_action_sms();
            } catch (Exception $ex) { break; }

            break;
        } while($check);
    }

    function passportReminder() {
        global $cfg;

        try {
            $settings = $cfg->getSmsAutoSettings();

            if (!$settings)
                return false;
            $passport_expiration_reminder = $settings['passport_expiration_reminder'];

            if (!$passport_expiration_reminder)
                return false;

            $passport_expiration_reminder_content = $settings['passport_expiration_reminder_content'];
            if (!$passport_expiration_reminder_content)
                return false;

                $passport_expiration_reminder_1st_before = $settings['passport_expiration_reminder_1st_before'];
            if (!$passport_expiration_reminder_1st_before)
                return false;

            $passport_expiration_reminder_2nd_before = $settings['passport_expiration_reminder_2nd_before'];
            if (!$passport_expiration_reminder_2nd_before)
                return false;

            $sql = "SELECT
                    DISTINCT phone_number, DATEDIFF(doe, NOW()) AS day_to_expire
                FROM
                    ost_pax_info i
                        JOIN
                    ost_pax_tour t ON t.pax_id = i.pax_id
                        AND i.id = t.pax_info
                WHERE
                    1
                        AND ((DATEDIFF(doe, NOW()) = $passport_expiration_reminder_1st_before
                        OR DATEDIFF(doe, NOW()) = $passport_expiration_reminder_2nd_before))
                        AND phone_number <> 0
                        AND phone_number <> ''
                        AND phone_number IS NOT NULL
                ";
            $result = db_query($sql);
            if (!$result) return;
            while ($row = db_fetch_array($result)) {
                $error = [];
                $msg = str_replace('DAY_TO_EXPIRE', $row['day_to_expire'], $passport_expiration_reminder_content);
                echo "Send SMS to ".$row['phone_number'] .":: Content: ".$msg."\r\n";

                _SMS::send($row['phone_number'], $msg, $error, 'Passport Reminder');

                if ($error) {
                    echo "Error: ".$error['message']."\r\n";
                }
            }

        } catch (Exception $ex) {
            echo $ex->getMessage()."\r\n";
        }
    }

    function unassign() {
        do {
            $check = false;
            try {
                $check = self::__unassign_no_answer_1st();
            } catch (Exception $ex) { break; }

            break;
        } while($check);

        do {
            $check = false;
            try {
                $check = self::__unassign_overdue();
            } catch (Exception $ex) { break; }

            break;
        } while($check);

        do {
            $check = false;
            try {
                $check = self::__unassign_welcome();
            } catch (Exception $ex) { break; }

            break;
        } while($check);
    }

    function Vision() {
        require_once(INCLUDE_DIR.'tugo.vision.php');
        $sql = "SELECT id, tour_id, booking_code, filename FROM pax_passport_photo WHERE status=0 OR status IS NULL"; //select passport photo not updated
        $res = db_query($sql);
        while ($res && ($row=db_fetch_array($res))) {
            echo date('Y-m-d H:i:s').': '.$row['filename']."\r\n";
            $filePath = ROOT_DIR.'passport_upload/'.$row['filename'];
            if (!$row['tour_id']) continue;
            if (!$row['booking_code']) continue;
            if (!$row['id']) continue;
            if (!file_exists($filePath)) continue;
            try {
                \Tugo\Vision::read($row['id'], $row['tour_id'], $row['booking_code'], $filePath);
            } catch (\Exception $ex) { echo $ex->getMessage()."\r\n"; }
        }

    }

    function CalendarAlert() {
        require_once(INCLUDE_DIR.'class.calendar.php');

        global $cfg;

        $dept = $cfg->getDefaultDept();
        $url = $cfg->getUrl();
        $email = $dept->getEmail();
        $tpl = $dept->getTemplate();
        $msg = $tpl->getNewMessageAlertMsgTemplate();
        if (!$dept || !$email || !$tpl || !$msg)
            return false;


        foreach (Calendar::getTodayEntries() as $e) {
            if (!isset($e['email']))
                continue;

            if (!isset($e['list']) || !is_array($e['list']) || !$e['list'])
                continue;

            $content = '<ol>';
            foreach ($e['list'] as $li) {
                $content .= '<li>';
                $content .= sprintf(
                    'Ticket number: <a target="_blank" href="%s">%s</a><br />Todo: <strong>%s</strong><br />Time: %s',
                    $url.'scp/tickets.php?id='.$li['id'],
                    $li['number'],
                    $li['content'],
                    date('d/m/Y H:i:s', strtotime($li['start']))
                );
                $content .= '</li>';
            }
            $content .= '</ol>';

            $msg = $content;
            $subj = 'Todo list ' . date('d/m/Y');

            $email->sendAlert($e['email'], $subj, $msg);
        }
    }

    function OverdueAlert() {
        require_once(INCLUDE_DIR.'class.ticket.php');
        global $thisstaff, $cfg;

        $settings = $cfg->getSmsAutoSettings();

        if (!$settings)
            return false;

        $date = date('Y-m-d', time()-24*3600*5);

        $sql_status = sprintf(
            "SELECT DISTINCT  id
              FROM %s
              WHERE `name` LIKE '%%thành công%%'
              OR `name` LIKE '%%hủy%%'"
            , TICKET_STATUS_TABLE
        );

        $res_status = db_query($sql_status);
        if (!$res_status) return false;
        $status_id = [];
        while(($row_status = db_fetch_array($res_status))) {
            $status_id[] = $row_status['id'];
        }

        $status_id = array_unique($status_id);
        $status_id = array_filter($status_id);
        if (!$status_id) return false;
        $status_id_string = implode(",", $status_id);

        $sql = sprintf(
            <<<TAG
                SELECT DISTINCT t.ticket_id
                FROM %s t
                  JOIN %s r ON t.ticket_id = r.ticket_id AND r.thread_type IN ('S', 'R')
                WHERE isoverdue = 1 AND status_id NOT IN (%s) AND t.created >= %s
                      AND t.dept_id IN (%s)
                      AND t.ticket_id NOT IN (
                  SELECT t.ticket_id
                  FROM %s t
                    JOIN %s r ON t.ticket_id = r.ticket_id AND r.thread_type IN ('AE', 'AS', 'Overdue')
                  WHERE isoverdue = 1 AND status_id NOT IN (%s)

                )
TAG
            ,
            TICKET_TABLE,
            TICKET_THREAD_TABLE,
            $status_id_string,
            db_input($date),
            implode(',', unserialize(SALE_DEPT)),
            TICKET_TABLE,
            TICKET_THREAD_TABLE,
            $status_id_string
        );

        if(($res=db_query($sql))) {
            $count = 0;
            while ($row = db_fetch_array($res)) {
                echo "\r\n-------- Ticket: ". @$row['ticket_id'] ." --------\r\n";
                if (!($ticket = Ticket::lookup($row['ticket_id']))) continue;
                $errors = [];

                if (isset($settings['send_email_overdue_tickets']) && $settings['send_email_overdue_tickets']
                    && isset($settings['send_email_overdue_tickets_title']) && !empty($settings['send_email_overdue_tickets_title'])
                    && isset($settings['send_email_overdue_tickets_content']) && !empty($settings['send_email_overdue_tickets_content'])
                ) {
                    echo date('Y-m-d H:i:s')." - __Send Email...\r\n";
                    $ticket->postOverdueEmail([], $errors);
                    echo "__Done Email\r\n";
                }

                if ($errors) {
                    foreach ($errors as $e)
                        echo $e . "\r\n";
                }
                $errors = [];

                if (isset($settings['send_sms_overdue_tickets']) && $settings['send_sms_overdue_tickets']
                    && isset($settings['send_sms_overdue_tickets_content']) && !empty($settings['send_sms_overdue_tickets_content'])
                ) {
                    echo date('Y-m-d H:i:s')." - __Send SMS...\r\n";
                    $ticket->postOverdueSMS([], $errors);
                    echo "__Done SMS\r\n";
                }

                if ($errors) {
                    foreach ($errors as $e)
                        echo $e . "\r\n";
                }

                echo "-------- ".++$count. " --------\r\n";
            }
        }
    }

    function checkNoAnswer() {
        define('MINUTE_TO_CHECK', 60);
        define('NO_ANSWER_LIMIT', 10);
        echo Format::userdate("Y-m-d H:i:s", time())." - start checking no answer calls\r\n";
        $sql = sprintf(
            "SELECT COUNT(*) as total FROM %s
                    WHERE `direction` LIKE 'inbound'
                    AND `disposition` IN ('NO ANSWER')
                    AND `starttime` >= DATE_SUB(NOW(), INTERVAL %s MINUTE)
                    AND TIMESTAMPDIFF(second ,starttime, endtime)>3
                    "
            , CALL_LOG_TABLE
            , MINUTE_TO_CHECK
        );

        $res = db_query($sql);
        if (!$res) {
            echo Format::db_datetime(time())." - done checking no answer calls; no thing to do\r\n";
            return false;
        }

        $row = db_fetch_array($res);
        if (!$row || !isset($row['total']) || !$row['total'] || $row['total'] < NO_ANSWER_LIMIT) {
            echo Format::userdate("Y-m-d H:i:s", time())." - done checking no answer calls; no thing to do\r\n";
            return false;
        }

        require_once __DIR__.'/class.myemail.php';
        $to = [];
        $to_name = [];

        $to[] = 'missedcall@tugo.com.vn';
        $to_name[] = COMPANY_TITLE.' Missed Calls';

        $subject = COMPANY_TITLE." - Cảnh báo về tổng đài - ".Format::userdate("Y-m-d H:i:s", time());
        $content = "<p>
                Tổng đài có số lượng cuộc gọi vào mà NO ANSWER/BUSY vượt quá ngưỡng quy định: ".$row['total']." cuộc gọi, trong vòng 60 phút vừa qua.
                    </p>
                    <p>Thời gian kiểm tra và gửi email: ".Format::userdate("Y-m-d H:i:s", time())."</p>
                    <hr />
                    <p><small>Đây là email tự động. Cronjob này được cài đặt trên server.</small></p>
                    ";
        MyEmail::send($to, $to_name, $subject, $content);
    }

    function noteCallLog() {
        // select các call log chưa note
        $sql = sprintf(
            "SELECT * FROM %s WHERE (`note` IS NULL OR `note` LIKE '') AND date(add_time)>=%s" // limit to new logs
            , CALL_LOG_TABLE
            , db_input(date('Y-m-d', time()-24*3600*3))
        );

        $res = db_query($sql);
        if (!$res) return false;

        while(($row = db_fetch_array($res))) {
            if (!isset($row['destinationnumber']) && !isset($row['callernumber']))
                continue;

            echo "\r\n" .date("Y-m-d H:i:s") . " - Row ID: ".$row['id'];

            $sql_update = sprintf(
                "UPDATE %s SET `note` = '1' WHERE `id` = %s"
                , CALL_LOG_TABLE
                , db_input($row['id'])
            );

            db_query($sql_update);
            //// //// //// //// //// //// //// //// //// ////
            //// //// //// //// //// //// //// //// //// ////
            $ticket_list = [];
            $sql_search = sprintf( // search from search table for tickets and threads have the phonenumber
                "SELECT
                              object_type,
                              object_id
                            FROM %s `search`
                            WHERE (search.phone_number LIKE %s
                              OR search.phone_number LIKE %s)"
                , TABLE_PREFIX . 'phone_number_index'
                , $row['direction'] == 'inbound' ? db_input($row['callernumber']) : db_input($row['destinationnumber'])
                , $row['direction'] == 'inbound' ? db_input('0'.$row['callernumber']) : db_input('0'.$row['destinationnumber'])
            );
            echo $sql_search."\r\n";
            $res_search = db_query($sql_search);
            if (!$res_search) continue;
            $thread_list = [];
            $user_list = [];
            while(($res_row = db_fetch_array($res_search))) {
                if ($res_row['object_type'] == 'T')
                    $ticket_list[] = $res_row['object_id'];
                elseif ($res_row['object_type'] == 'H')
                    $thread_list[] = $res_row['object_id'];
                elseif ($res_row['object_type'] == 'U')
                    $user_list[] = $res_row['object_id'];
            }
            $thread_list = array_filter($thread_list);
            $thread_list = array_unique($thread_list);
            //// //// //// //// //// //// //// //// //// ////

            //// //// //// //// //// //// //// //// //// ////
            if ($thread_list) {
                $thread_list_string = implode(',', $thread_list);
                $sql_search_thread = sprintf(
                    "SELECT DISTINCT  t.ticket_id FROM %s t WHERE t.id IN (" . $thread_list_string . ")"
                    , TICKET_THREAD_TABLE
                );
                echo $sql_search_thread . "\r\n";
                $res_thread_search = db_query($sql_search_thread);
                if ($res_thread_search) {
                    while (($res_row = db_fetch_array($res_thread_search))) {
                        $ticket_list[] = $res_row['ticket_id'];
                    }
                }
            }
            //// //// //// //// //// //// //// //// //// ////

            //// //// //// //// //// //// //// //// //// ////
            if ($user_list) {
                $user_list_string = implode(',', $user_list);
                $sql_search_user = sprintf(
                    "SELECT DISTINCT  t.ticket_id FROM %s t WHERE t.user_id IN (" . $user_list_string . ")"
                    , TICKET_TABLE
                );
                echo $sql_search_user . "\r\n";
                $res_user_search = db_query($sql_search_user);
                if ($res_user_search) {
                    while (($res_row = db_fetch_array($res_user_search))) {
                        $ticket_list[] = $res_row['ticket_id'];
                    }
                }
            }
            //// //// //// //// //// //// //// //// //// ////

            //// //// //// //// //// //// //// //// //// ////
            $ticket_list = array_unique($ticket_list);
            $ticket_list = array_filter($ticket_list);

            if (!$ticket_list) continue;
            $ticket_list_string = implode(',', $ticket_list);
            //// //// //// //// //// //// //// //// //// ////
            $sql_inner = sprintf(
                "SELECT DISTINCT t.ticket_id FROM %s t WHERE t.ticket_id IN (".$ticket_list_string.")
                    ORDER BY  updated DESC LIMIT 1"
                , TICKET_TABLE
            );
            echo $sql_inner."\r\n";
            $res_inner = db_query($sql_inner);
            if (!$res_inner) continue;
            if (!($row_inner = db_fetch_array($res_inner))) continue;

            // tạo note
            $content = sprintf(
                "<h5>Call log</h5>
                <p>Direction: <strong>%s</strong></p>
                <p>From: <strong>%s</strong></p>
                <p>To: <strong>%s</strong></p>
                <p>Status: <strong>%s</strong></p>
                <p>Start time: <strong>%s</strong></p>
                <p>Total: <strong>%ss</strong></p>

                <p style='color:rebeccapurple;font-weight: bold'>-- <a href=\"http://apps.worldfone.vn/externalcrm/playback2.php?calluuid=%s&secrect=7a5ba09cbb9bd7677c5c308f765644e7&version=3\" target='_blank' class='no-pjax'>Download Audio</a> --</p>
                "
                , $row['direction']
                , $row['callernumber']
                , $row['destinationnumber']
                , $row['disposition']
                , $row['starttime']
                , $row['billduration']
                , $row['calluuid']
            );

            $ticket = Ticket::lookup($row_inner['ticket_id']);
            if (!$ticket) continue;

            $vars = [
                'title' => 'System - Call log',
                'body' => $content,
                'note' => $content,
            ];
            $errors = [];
            $n = $ticket->postNote($vars, $errors, 'Cronjob');

            if (!$n) continue;

            echo " - Ticket ID: ".$row_inner['ticket_id'] . "\r\n";

            $sql_update = sprintf(
                "UPDATE %s SET `note` = '1', `ticket_id` = %s WHERE `id` = %s"
                , CALL_LOG_TABLE
                , db_input($row_inner['ticket_id'])
                , db_input($row['id'])
            );

            db_query($sql_update);
        } // end while
    } // end func

    function run(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running osticket cron...\r\n";
        self::CalendarAlert();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done osticket cron\r\n";
    }

    function runAutomation(){ //called by outside cron NOT autocron
        echo date('Y-m-d H:i:s')." - Running Automation...\r\n";
        self::Automation();
        echo date('Y-m-d H:i:s')." - Done Automation\r\n";
    }

    function runAutomationDaily(){ //called by outside cron NOT autocron
        echo date('Y-m-d H:i:s')." - Running Daily Automation...\r\n";

        if (defined('RUN_BOOKING_FULLPAID') && RUN_BOOKING_FULLPAID)
            self::AutomationDailyBookingFullpaid();

        if (defined('RUN_LONG_TIME_NO_SEE') && RUN_LONG_TIME_NO_SEE)
            self::AutomationDailyLongTimeNoSee();

        echo date('Y-m-d H:i:s')." - Done Daily Automation\r\n";
    }

    function AutomationDailyLongTimeNoSee() {
        include_once INCLUDE_DIR."class.auto_action.php";
        include_once INCLUDE_DIR."class.trigger.php";

        $sql = "select t.gateway_present_time, t.departure_date, pt.phone_number, t.id as tour_id
            from tour t
                     join ost_pax_tour pt on t.id = pt.tour_id
            where
            (
            date(ifnull(t.gateway_present_time, t.departure_date)) = subdate(current_date, ".LONG_TIME_NO_SEE_LIMIT_1.")
            OR date(ifnull(t.gateway_present_time, t.departure_date)) = subdate(current_date, ".LONG_TIME_NO_SEE_LIMIT_2.")
            OR date(ifnull(t.gateway_present_time, t.departure_date)) = subdate(current_date, ".LONG_TIME_NO_SEE_LIMIT_3.")
            )
            and pt.phone_number is not null and pt.phone_number <> 0 and pt.phone_number not like ''
            and t.name not like '%rớt visa%'
            and t.name not like '%rot visa%'
            ";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            if (empty($row['phone_number']) || empty($row['tour_id'])) continue;
            $phone_numbers = _String::getPhoneNumbers($row['phone_number']);
            if (empty($phone_numbers) || !is_array($phone_numbers)) continue;

            echo "Tour ID: ". $row['tour_id']."\r\n";
            foreach($phone_numbers as $phone) {
                try {
                    if (!date_create_from_format('Y-m-d H:i:s', $row['gateway_present_time'])
                        && !date_create_from_format('Y-m-d H:i:s', $row['departure_date']))
                        continue;

                    $date = isset($row['gateway_present_time'])
                    && date_create_from_format('Y-m-d H:i:s', $row['gateway_present_time'])
                        ? $row['gateway_present_time'] : $row['departure_date'];
                    LongTimeNoSeeTrigger::fire($phone, $date);
                } catch (Exception $ex) {
                    echo date('Y-m-d H:i:s')." - ERROR: ".$ex->getMessage()."\r\n";
                }
            }
        }
    }

    function AutomationDailyBookingFullpaid() {
        include_once INCLUDE_DIR."class.auto_action.php";
        include_once INCLUDE_DIR."class.trigger.php";
        $_status = trim(json_encode(trim('Hủy')), '"');
        $_status = sprintf(" AND (  b.status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
        $sql = "select *
            FROM (
                     select b.booking_code_trim, b.ticket_id,
                            b.total_retail_price,
                            sum(p.amount * (case when p.topic_id = ".THU_TOPIC." then 1 else -1 end)) as amount,
                            from_unixtime(p.time)                                          as time,
                            b.phone_number
                     from booking_view b
                              join payment_tmp p on b.booking_code_trim = p.booking_code_trim
                     where date(from_unixtime(p.time)) = subdate(current_date, ".BOOKING_FULLPAID_LIMIT.")
                     $_status
                     group by b.booking_code_trim
                 ) as x
            where x.total_retail_price = x.amount
            order by x.time desc
        ";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            if (empty($row['ticket_id']) || empty($row['phone_number'])) continue;
            $phone_numbers = _String::getPhoneNumbers($row['phone_number']);
            if (empty($phone_numbers) || !is_array($phone_numbers)) continue;

            echo "Booking Ticket ID: ". $row['ticket_id']."\r\n";
            foreach($phone_numbers as $phone) {
                try {
                    echo "Phone number: $phone\r\n";
                    BookingFullPaidTrigger::fire($row['ticket_id'], $phone);
                } catch (Exception $ex) {
                    echo date('Y-m-d H:i:s')." - ERROR: ".$ex->getMessage()."\r\n";
                }
            }
        }
    }

    function Automation() {
        include_once INCLUDE_DIR."class.auto_action.php";
        include_once INCLUDE_DIR."class.trigger.php";
        include_once INCLUDE_DIR."bi.tracking.php";

        // lấy automation content có thể chạy
        $autos = AutoActionContent::getAvailableSMS();

        // lấy waiting content ứng với trigger và send_after
        while($autos && ($auto = db_fetch_array($autos))) {
            echo "_________________________\r\n";
            echo date('Y-m-d H:i:s')." : Automation ".$auto['id']."\r\n";
            $waitings = AutoActionWaiting::getAll(
                $auto['trigger_id'],
                $auto['send_after'],
                'phone',
                AutoActionWaiting::STATUS_WAITING
            );

            $count = 0;
            while($waitings && ($wating = db_fetch_array($waitings))
                && $count < (int)AUTOMATION_CONTENT_DAILY_LIMIT) {
                if (!$wating || !$wating['object_id']) {
                    echo date('Y-m-d H:i:s')." : No object ID\r\n";
                    continue;
                }

                $phone = PhoneNumberList::lookup($wating['object_id']);
                if (!$phone || !isset($phone->phone_number) || !$phone->phone_number) {
                    echo date('Y-m-d H:i:s')." : No phone number\r\n";
                    continue;
                }

                // ráp vào gửi
                $error = [];
                echo date('Y-m-d H:i:s')." : Checking ".$phone->phone_number."\r\n";

                $staff_name = null;

                if (in_array($auto['trigger_id'], [TriggerList::TICKET_CANCEL, TriggerList::TICKET_CANCEL_REMIND_2WEEK])) {
                    if (!isset($wating['ticket_id']) || empty($wating['ticket_id'])) {
                        echo date('Y-m-d H:i:s')." : No ticket found\r\n";
                        continue;
                    }

                    $ticket = Ticket::lookup($wating['ticket_id']);
                    if (!$ticket) {
                        echo date('Y-m-d H:i:s')." : No ticket found\r\n";
                        continue;
                    }

                    $StatusId = $ticket->getStatusId();
                    if ($auto['trigger_id'] == TriggerList::TICKET_CANCEL_REMIND_2WEEK
                        || $auto['trigger_id'] == TriggerList::TICKET_CANCEL) {
                        Signal::send(
                            'TriggerList.TICKET_CANCEL_REMIND_2WEEK',
                            $ticket,
                            $StatusId
                        );
                    }

                    $staff = $ticket->getStaff();
                    if ($staff) {
                        $staff_name = (string)$staff;
                        $staff_name = trim($staff_name);
                    }
                }

                if (Ticket::countOpenByPhoneNumber($phone->phone_number)) {
                    $w = AutoActionWaiting::lookup($wating['id']);
                    if ($w) {
                        $w->set('status', AutoActionWaiting::STATUS_NOACTION);
                        $w->save();
                    }

                    echo date('Y-m-d H:i:s')." : New ticket available, no action, close this waiting\r\n";
                    continue;
                }

                if (Ticket::countSuccessByPhoneNumber($phone->phone_number, $wating['add_time'])) {
                    $w = AutoActionWaiting::lookup($wating['id']);
                    if ($w) {
                        $w->set('status', AutoActionWaiting::STATUS_NOACTION);
                        $w->save();
                    }

                    echo date('Y-m-d H:i:s')." : Successful ticket available, no action, close this waiting\r\n";
                    continue;
                }

                $code_id = 0;
                $urls = _String::getUrl($auto['sms_content']);
                if ($urls) {
                    $param = \BI\TrackingUrlparam::getCode($wating['object_id'], 'phone', $code_id);

                    $utm_campaign = AutoActionCampaign::lookup(['type' => 'campaign', 'id' => $auto['aac_campaign_id']]);
                    $utm_source = AutoActionCampaign::lookup(['type' => 'source', 'id' => $auto['aac_utm_source_id']]);
                    $utm_medium = AutoActionCampaign::lookup(['type' => 'medium', 'id' => $auto['aac_utm_medium_id']]);
                    $utm_content = AutoActionCampaign::lookup(['type' => 'content', 'id' => $auto['aac_utm_content_id']]);

                    $utm_term = null;
                    if ($auto['aac_utm_term_id'])
                        $utm_term = AutoActionCampaign::lookup(['type' => 'content', 'id' => $auto['aac_utm_term_id']]);

                    $utm = \BI\TrackingUrlparam::getUTM(
                        TRACKING_UTM_CAMPAIGN . ($utm_campaign ? _String::spaceToUnderscore($utm_campaign->value) : ''),
                        TRACKING_UTM_MEDIUM . ($utm_medium ? _String::spaceToUnderscore($utm_medium->value) : ''),
                        TRACKING_UTM_SOURCE . ($utm_source ? _String::spaceToUnderscore($utm_source->value) : ''),
                        TRACKING_UTM_CONTENT . ($utm_content ? _String::spaceToUnderscore($utm_content->value) : ''),
                        TRACKING_UTM_CAMPAIGN . ($utm_term ? _String::spaceToUnderscore($utm_term->value)
                            : _String::spaceToUnderscore(TriggerList::caseTitleName($auto['trigger_id'])))
                    );
                    if ($param) $param .= "&$utm";
                    $auto['sms_content'] = _String::putParamToUrl($auto['sms_content'], $urls, $param, true);
                }

                if (mb_strpos($auto['sms_content'], 'STAFF_NAME_ASSIGN') !== false && $staff_name) {
                    $auto['sms_content'] = str_replace(
                        'STAFF_NAME_ASSIGN',
                        $staff_name,
                        $auto['sms_content']
                    );
                } else {
                    $auto['sms_content'] = str_replace(
                        'STAFF_NAME_ASSIGN',
                        '',
                        $auto['sms_content']
                    );

                    $auto['sms_content'] = str_replace(
                        '  ',
                        ' ',
                        $auto['sms_content']
                    );
                }

                if (!defined('DEV_ENV') || 0 !== DEV_ENV) {
                    var_dump($auto['sms_content']);
                } else {
                    _SMS::send(
                        $phone->phone_number,
                        $auto['sms_content'],
                        $error,
                        'AutoAction_'.$auto['id'],
                        $auto['last_log_id']
                    );
                }

                $w = AutoActionWaiting::lookup($wating['id']);
                if ($w) {
                    $w->set('status', AutoActionWaiting::STATUS_SENT);
                    $w->set('auto_action_content_log_id', $auto['last_log_id']);
                    $w->set('tracking_urlparam_id', $code_id);
                    $w->save();
                }

                $count++;
            }
        }
    }

    function runVision(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running osticket cron...\r\n";
        self::Vision();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done osticket cron\r\n";
    }

    function runBookingExpenseForecast(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date('Y-m-d H:i:s')." - BookingExpenseForecast analytics cron...\r\n";
        BookingExpenseForecast::analytics();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo date('Y-m-d H:i:s')." - Done BookingExpenseForecast analytics cron\r\n";
    }

    function runNoActionSMS(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running noActionSMS cron...\r\n";
        self::noActionSMS();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done noActionSMS cron\r\n";
    }

    function runPassportExpirationReminder(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running passport_expiration_reminder cron...\r\n";
        self::passportReminder();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done passport_expiration_reminder cron\r\n";
    }

    function runUnassign(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running unassign cron...\r\n";
        self::unassign();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done unassign cron\r\n";
    }

    function runTuThien(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date('Y-m-d H:i:s').": Export tu-thien data to CSV...\r\n";
        self::TuThien();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo date('Y-m-d H:i:s').": Done Export tu-thien data to CSV\r\n";
    }

    function runBookingPhonenumber(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date('Y-m-d H:i:s').": Running bookingPhonenumber...\r\n";
        self::bookingPhonenumber();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo date('Y-m-d H:i:s').": Done bookingPhonenumber\r\n";
    }

    function runPhone11(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running migrate phone number 11 to 10 digit...\r\n";
        self::phone11();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done migrate phone number 11 to 10 digit\r\n";
    }

    function runEmaticMC(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running EmaticMC...\r\n";
        self::EmaticMC();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done EmaticMC\r\n";
    }


    function runTag(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running tag maker...\r\n";
        self::TagMaker();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done tag maker\r\n";
    }

    function runFBOfflineConversion(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running FB Offline Conversion cron...\r\n";
        self::FBOfflineConversion();

        echo "Done FB Offline Conversion cron\r\n";
    }

    function run_birthday(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Start sending Birthday SMS...\r\n";
        self::BirthdaySMS();
        echo "Done sending Birthday SMS\r\n";
    }

    function run_pax_return_sms(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Start sending after sales SMS...\r\n";
        self::updateTourCountryID();
        self::paxReturnSMS();
        echo "Done sending after sales SMS\r\n";
    }

    function run_clean_attachment(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Start cleanAttachment...\r\n";
        self::cleanAttachment();
        echo "Done cleanAttachment\r\n";
    }

    function runMigrateFiles(){ //called by outside cron NOT autocron
        include_once INCLUDE_DIR.'class.file.php';

        $sql = "SELECT id FROM ost_file WHERE bk LIKE 'D'";
        $res = db_query($sql);
        if (!$res) return;
        while ($res && ($row = db_fetch_array($res))) {
            if (!isset($row['id']) || !$row['id']) continue;

            $file = new AttachmentFile($row['id']);
            $file->migrate('F');
        }
    }

    function run_booking_ticket_id(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date("Y-m-d H:i:s")." - Running run_booking_ticket_id cron...\r\n";
        self::UpdateBookingTicketId();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo date("Y-m-d H:i:s")." - Done run_booking_ticket_id cron\r\n";
    }

    function run_cron_every_minute(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        if (defined('CHECKING_BOOKING_CODE') && CHECKING_BOOKING_CODE) {
            echo date('Y-m-d H:i:s').": Running CHECKING_BOOKING_CODE cron...\r\n";
            self::CheckingBookingCode();
            echo date('Y-m-d H:i:s').": Done CHECKING_BOOKING_CODE cron\r\n";
        }

        $data = array('autocron'=>false);
        Signal::send('cron', $data);

    }

    function call_indexing_phone(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Indexing phone numbers...\r\n";
        self::IndexingPhoneNumber();

        echo "Done Indexing phone numbers\r\n";
    }

    function run_export_payment(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running export payment...\r\n";
        self::ExportPayments();

        echo "Done export payment\r\n";
    }

    function update_agent_list(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running update agent list cron...\r\n";
        require_once __DIR__.'/../vendor/autoload.php';
        require_once(INCLUDE_DIR.'/update-agent-list.php');

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo "Done update agent list cron\r\n";
    }

    function run_call_google_sheet_calllog(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo "Running send call log to google sheet...\r\n";

        require_once __DIR__.'/../vendor/autoload.php';
        require_once(INCLUDE_DIR.'/google.php');


        echo "Done  send call log to google sheet\r\n";
    }

    function run_overdue(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date('Y-m-d H:i:s')." - Running osticket overdue cron...\r\n";
        self::OverdueAlert();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
        echo date('Y-m-d H:i:s')." - Done osticket overdue cron\r\n";
    }

    function run_default(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        self::MailFetcher();
        self::TicketMonitor();
        self::PurgeLogs();
        // Run file purging about every 10 cron runs
        if (mt_rand(1, 9) == 4)
            self::CleanOrphanedFiles();
        self::PurgeDrafts();
        self::MaybeOptimizeTables();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
    }

    function run_fetch(){ //called by outside cron NOT autocron
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        echo date("Y-m-d H:i:s")." - Start run_fetch\r\n";

        self::MailFetcher();
        self::TicketMonitor();
        self::PurgeLogs();
        // Run file purging about every 10 cron runs
        if (mt_rand(1, 9) == 4)
            self::CleanOrphanedFiles();
        self::PurgeDrafts();
        self::MaybeOptimizeTables();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);

        echo date("Y-m-d H:i:s")." - Done run_fetch\r\n";
    }

    function run_booking_type_id() {
        echo date("Y-m-d H:i:s")." - START booking_type_id\r\n";
        self::bookingTypeID();
        echo date("Y-m-d H:i:s")." - DONE booking_type_id\r\n";

        echo date("Y-m-d H:i:s")." - START paymentTypeID\r\n";
        self::paymentTypeID();
        echo date("Y-m-d H:i:s")." - DONE paymentTypeID\r\n";
    }

    function paymentTypeID() {
        $count = 0;
        try {
            $payment_type_exclude_cache = [];

            $sql = "SELECT ticket_id, outcome_type
                FROM payment_tmp
                WHERE
                ( (outcome_type_id = 0 OR outcome_type_id IS NULL OR outcome_type_id LIKE '') AND outcome_type IS NOT NULL AND outcome_type NOT LIKE '' )
            ";
            $res = db_query($sql);
            while ($res && ($row = db_fetch_array($res))) {
                try {
                    if (isset($row['ticket_id']) && isset($row['outcome_type']) && $row['outcome_type'] && $row['ticket_id']
                        && self::_update_type_id($row, 'payment_tmp', 'outcome_type', $payment_type_exclude_cache)) {
                        $count++;
                    }

                } catch(Exception $ex) {
                    echo $ex->getMessage()."\r\n";
                    continue;
                }
            }

            $sql = "SELECT ticket_id, income_type
                FROM payment_tmp
                WHERE
                ( (income_type_id = 0 OR income_type_id IS NULL OR income_type_id LIKE '') AND income_type IS NOT NULL AND income_type NOT LIKE '' )
            ";
            $res = db_query($sql);
            while ($res && ($row = db_fetch_array($res))) {
                try {
                    if (isset($row['ticket_id']) && isset($row['income_type']) && $row['income_type'] && $row['ticket_id']
                        && self::_update_type_id($row, 'payment_tmp', 'income_type', $payment_type_exclude_cache)) {
                        $count++;
                    }

                } catch(Exception $ex) {
                    echo $ex->getMessage()."\r\n";
                    continue;
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage()."\r\n";
        }
        echo "UPDATED $count rows\r\n";
    }

    function _get_type_id($type_json) {
        $tmp = explode('":"', $type_json);
        if (!is_array($tmp) || !isset($tmp[0])) return null;
        $id = preg_replace('/[^0-9]/', '', $tmp[0]);
        return $id;
    }

    function _update_type_id($row, $table_name, $column_name, &$payment_type_exclude_cache) {
        if (isset($row['ticket_id']) && isset($row[ $column_name ]) && $row[ $column_name ] && $row['ticket_id']) {
            $id = self::_get_type_id($row[ $column_name ]);
            if (!$id || !is_numeric($id)) return false;

            $exclude = 0;
            if (isset($payment_type_exclude_cache[$column_name][$id])) {
                if ((int)trim($payment_type_exclude_cache[$column_name][$id])) {
                    $exclude = 1;
                }
            } else {
                $exclude = $payment_type_exclude_cache[$column_name][$id] = self::_is_exclude($id);
            }

            $_sql = "UPDATE $table_name
                SET ".$column_name."_id=".db_input($id).", exclude=".db_input($exclude)."
                WHERE ticket_id = ".db_input($row['ticket_id']);

            $res = db_query($_sql);
            if ($res) {
                echo "Updated ticket_id ".$row['ticket_id'].", $column_name ".$id."; exclude: $exclude\r\n";
                return true;
            }
        }

        return false;
    }

    function _is_exclude($payment_type_id) {
        $list_item = DynamicListItem::lookup($payment_type_id);
        $config = $list_item->getConfiguration();
        if (isset($config[INCOME_EXCLUDE]) && $config[INCOME_EXCLUDE])
            return 1;
        if (isset($config[OUTCOME_EXCLUDE]) && $config[OUTCOME_EXCLUDE])
            return 1;

        return 0;
    }

    function bookingTypeID() {
        $count = 0;
        try {
            $sql = "SELECT ticket_id, booking_type FROM booking_tmp WHERE booking_type_id = 0 OR booking_type_id IS NULL OR booking_type_id LIKE ''";
            $res = db_query($sql);
            while ($res && ($row = db_fetch_array($res))) {
                try {
                    if (!isset($row['ticket_id']) || !isset($row['booking_type']) || !$row['booking_type'] || !$row['ticket_id']) continue;
                    $tmp = explode('":"', $row['booking_type']);
                    if (!is_array($tmp) || !isset($tmp[0])) continue;
                    $id = preg_replace('/[^0-9]/', '', $tmp[0]);
                    if (!$id || !is_numeric($id)) continue;
                    $_sql = "UPDATE booking_tmp SET booking_type_id=".db_input($id)." WHERE ticket_id = ".db_input($row['ticket_id']);
                    if (db_query($_sql)) echo "Updated ticket_id ".$row['ticket_id'].", booking_type_id ".$id."\r\n";
                    $count++;
                } catch(Exception $ex) {
                    echo $ex->getMessage()."\r\n";
                    continue;
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage()."\r\n";
        }
        echo "UPDATED $count rows\r\n";
    }

    function run_call_log() {
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        self::noteCallLog();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
    }

    function run_call_log_no_answer() {
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        self::checkNoAnswer();

        $data = array('autocron'=>false);
        Signal::send('cron', $data);
    }

    function run_callin_alert() {
        require_once __DIR__.'/class.myemail.php';
        echo date('Y-m-d H:i:s')." - Run Call IN alert\r\n";
        self::CallInAlert(CALLIN_ALERT_HOUR_LIMIT, CALLIN_ALERT_HOUR);
        self::CallInAlert(CALLIN_ALERT_DAY_LIMIT, CALLIN_ALERT_DAY);
        echo date('Y-m-d H:i:s')." - DONE Call IN alert\r\n";
    }

    function CallInAlert($limit, $minutes) {
        $sql = sprintf(
            "SELECT callernumber, COUNT(callernumber) as total FROM %s
                WHERE `direction` LIKE 'inbound'
                AND `starttime` >= DATE_SUB(NOW(), INTERVAL %s MINUTE)
                GROUP BY callernumber
                HAVING total > %d
            "
            , CALL_LOG_TABLE
            , $minutes
            , $limit
        );

        $res = db_query($sql);
        if (!$res) {
            echo date('Y-m-d H:i:s')." - DONE Call IN alert, nothing to do\r\n";
            return false;
        }

        $list = [];
        while (($row = db_fetch_array($res))) {
            $list[] = "<li>".$row['callernumber'].": ".$row['total']." lần</li>";
        }

        if (!$list) {
            echo date('Y-m-d H:i:s')." - DONE Call IN alert, nothing to do\r\n";
            return false;
        }

        $to = [];
        $to_name = [];

        $to[] = 'phamquocbuu@gmail.com';
        $to_name[] = COMPANY_TITLE.' Call in alert';

        $subject = COMPANY_TITLE." - Call in alert - ".Format::userdate("Y-m-d H:i:s", time());
        $content = "<p>
            Tổng đài có số lượng cuộc gọi vào của một số điện thoại nhiều bất thường: trên "
            .$limit." cuộc gọi trong ".$minutes." phút. Vui lòng kiểm tra.
            </p>
            <p>Danh sách các số gọi vào:</p>
            <ol>
            ".implode('', $list)."
            </ol>
            <p>Thời gian kiểm tra và gửi email: ".Format::userdate("Y-m-d H:i:s", time())."</p>
            <hr />
            <p><small>Đây là email tự động. Cronjob này được cài đặt trên server.</small></p>
            ";
        MyEmail::send($to, $to_name, $subject, $content);
    }

    function CheckingBookingCode() {
        global $ost;
        if (!$ost || $ost->isUpgradePending())
            return;

        $sql = "select ticket_id, booking_code
            from booking_tmp
            WHERE booking_code in (
                select booking_code
                from (
                         SELECT booking_code, count(distinct ticket_id) as dup
                         FROM booking_tmp mto
                         group by mto.booking_code
                         having dup > 1
                     ) as x
            )";
        $res = db_query($sql);
        if (!$res) return null;
        $list = [];
        $content = "<h3>Các mã booking bị trùng: </h3>\r\n";
        $i = 1;
        while (($row = db_fetch_array($res))) {
            echo $row['booking_code']."\r\n";
            $list[] = $row['booking_code'];
            $content .= "<p>$i :::: id: <a href=\"".$ost->getConfig()->getBaseUrl()."/scp/tickets.php?id=".$row['ticket_id']."\">".$row['ticket_id']."</a>, booking code:". $row['booking_code']."</p>\r\n";
            $i++;
        }

        if (!$list) return null;

        require_once __DIR__.'/class.myemail.php';
        $to = [];
        $to_name = [];

        $to[] = 'phamquocbuu@gmail.com';
        $to_name[] = 'Bửu Phạm';

        $subject = COMPANY_TITLE." - Mã booking bị trùng - ".Format::userdate("Y-m-d H:i:s", time());
        $content .= "
        <p>Tổng cộng: ".count($list)." booking trùng mã nhau</p>
        <p>Thời gian kiểm tra và gửi email: ".Format::userdate("Y-m-d H:i:s", time())."</p>
                    <hr />
                    <p><small>Đây là email tự động. Cronjob này được cài đặt trên server.</small></p>
                    ";
        MyEmail::send($to, $to_name, $subject, $content);
    }
    function run_ticket_return()
    {
        require_once INCLUDE_DIR.'class.ticket-return.php';
        global $ost;
        if (!$ost || $ost->isUpgradePending())
        return;

        $sql = "SELECT a.ticket_id, a.user_id, a.created
            FROM ost_ticket as a LEFT JOIN ".OST_TICKET_COUNT." as b
            ON a.ticket_id = b.ticket_id
            WHERE b.ticket_id is null
            AND a.topic_id IN (".implode(',', unserialize(SALE_TOPIC)).")
            AND b.ticket_id IS NULL
            AND (a.created BETWEEN ".db_input(DATE_TICKET_RETURN)." AND NOW())";
        $res = db_query($sql);
echo $sql."\r\n";
        $_sql_count = "SELECT COUNT(*) as total FROM ($sql) as X";
        $_res = db_query($_sql_count);
        $total = 0;
        if ($_res) {
            $_row = db_fetch_array($_res);
            if ($_row && isset($_row['total'])) $total = $_row['total'];
        }

        $count = 0;
        echo date('Y-m-d H:i:s')." - ticket_return Action:: $total\r\n";
        while ($res && ($row = db_fetch_array($res))) {
            $ticket_id = $row['ticket_id'];
            $user_id = $row['user_id'];
            $created = $row['created'];
            TicketReturn::returnTicket($ticket_id, $user_id, $created);
            $count++;
            echo "\rProgress: $count / $total :: ";
        }
        echo date('Y-m-d H:i:s')." - ticket_return DONE\r\n";
    }
    function  runStatistic()
    {
        $count = 0;
        echo date('Y-m-d H:i:s')." Start cron statistic age,gender \r\n";
        try {
            include_once INCLUDE_DIR.'class.demographic.php';
            global $ost;
            if (!$ost || $ost->isUpgradePending())
                return;

            $results = \Tugo\AgeGenderPax::countPaxAgeGroupInTour();
            //------save data age_group
            while ($results && ($row = db_fetch_array($results)))
            {
                try {
                    $count++;
                    $news = \Tugo\GraphicAgeGroup::lookup($row['gather_date']);
                    if ($news) {
                        //EDIT
                        $data_age_group = [
                            '0_25' => $row['0_25'],
                            '25_35' => $row['25_35'],
                            '35_55' => $row['35_55'],
                            'older_55' => $row['55+']
                        ];
                        $news->setAll($data_age_group);
                    } else {
                        //CREATE
                        $data_age_group = [
                            'gather_date' => $row['gather_date'],
                            '0_25' => $row['0_25'],
                            '25_35' => $row['25_35'],
                            '35_55' => $row['35_55'],
                            'older_55' => $row['55+']
                        ];
                        $news = \Tugo\GraphicAgeGroup::create($data_age_group);
                        $news->save();
                    }
                }catch (Exception $ex) {
                    echo date('Y-m-d H:i:s').$ex->getMessage()." in save data age_group \r\n";
                }
            }
            echo date('Y-m-d H:i:s').': has '.$count." data handle in group_age table\r\n";
            $count=0;

            $results = \Tugo\AgeGenderPax::countPaxAgeInTour();
            //------save date age and amount flow date
            while ($results && ($row = db_fetch_array($results))){
                try{
                    $count++;
                    $news = \Tugo\GraphicAge::lookup(['gather_date' => $row['gather_date'],'age' => $row['age']]);
                    if($news)
                    {
                        //EDIT
                        $data_age = [
                            'amount'         => $row['amount_age']
                        ];
                        $news->setAll($data_age);
                    }else{
                        //CREATE
                        $data_age = [
                            'gather_date' => $row['gather_date'],
                            'age'            => $row['age'],
                            'amount'         => $row['amount_age']
                        ];
                        $news = \Tugo\GraphicAge::create($data_age);
                        $news->save();
                    }
                }catch (Exception $ex) {
                    echo date('Y-m-d H:i:s').$ex->getMessage()." in save data age  \r\n";
                }
            }
            echo date('Y-m-d H:i:s').': has '.$count." data handle in age table\r\n";
            $count=0;

            //-------save Gender ----------
            $results = \Tugo\AgeGenderPax::countPaxGenderInTour();
            while ($results && ($row = db_fetch_array($results))) {
                try {
                    $count++;
                    $news = \Tugo\GraphicGender::lookup(['gather_date' => $row['gather_date'], 'gender' => $row['gender']]);
                    if ($news) {
                        //EDIT
                        $data_gender = [
                            'amount' => $row['amount_gender']
                        ];
                        $news->setAll($data_gender);
                    } else {
                        //CREATE
                        $data_gender = [
                            'gather_date' => $row['gather_date'],
                            'gender' => $row['gender'],
                            'amount' => $row['amount_gender']
                        ];
                        $news = \Tugo\GraphicGender::create($data_gender);
                        $news->save();
                    }
                } catch (Exception $ex) {
                    echo date('Y-m-d H:i:s').$ex->getMessage() . " in save data gender \r\n";
                }
            }
            echo date('Y-m-d H:i:s').': has '.$count." data handle in gender table\r\n";
            $count = 0;
            echo date('Y-m-d H:i:s')." Success \r\n";
        } catch (Exception $ex) {
            echo date('Y-m-d H:i:s').$ex->getMessage()."\r\n";
        }
    }

    //-----cron-Loyalty------------
    function runLoyaltyMembership(){
        // TODO: force 10 digit only
        echo date('Y-m-d H:i:s')." Start cron loyalty \r\n";
        $sql = "SELECT ost_pax_tour.phone_number as `phone_number`, ost_pax.full_name as `full_name`, ost_pax.dob as `dob`
                FROM ost_pax_tour
                JOIN ost_pax ON ost_pax_tour.pax_id = ost_pax.id
                WHERE phone_number IS NOT NULL and LENGTH(phone_number) >= 10 and LENGTH(phone_number) <=11";
        $results = db_query($sql);

        $total = db_num_rows($results);
        $count_error = 0;
        $count_create = 0;
        $count_exist = 0;
        $i = 0;
        while ($results && ($row = db_fetch_array($results))) {
            $i++;
            try {

                $phone_number = $row['phone_number'];
                if(strlen($phone_number) == 11) // TODO: force 10 digit only
                    $phone_number = _String::convertMobilePhoneNumber($phone_number);
                $check_isMobile = (_String::simpleCheckMobilePhone($phone_number));

                $full_name = $row['full_name'];
                $dob = $row['dob'];
                if($check_isMobile ) {
                    $count_create++;
                    Tugo\User::register($phone_number, $full_name, '', TRUE,$dob);

                }else{
                    $count_error++;
                }

            } catch (Exception $e) {
                //check exist in api_user table
                $count_create--;
               if($e->getCode() === 1525)
               {

                   $count_exist++;
               }else{
                   $count_error++;
               }
            }
            echo "create $i/$total \r";

        }
        echo "There are $count_create/$total number phone registered \r\n";
        if($count_exist > 0)
            echo "There are $count_exist/$total registered in api_user table \r\n";
        if($count_error > 0)
            echo "There are $count_error/$total number phone do not register \r\n";
        echo date('Y-m-d H:i:s')." Finish";

    }

    //-----cron runSendEmailOverReservation-------------------
    function runSendEmailOverReservation(){
        echo date('Y-m-d H:i:s')." Start cron mail send overdue in reservation \r\n";

        require INCLUDE_DIR.'class.pax.php';
        require_once __DIR__.'/class.myemail.php';
        $overdue_1 = BookingReservation::getDueFlowTime(TIME_OVERDUE_GROUP1);
        $overdue_2 = BookingReservation::getDueFlowTime(TIME_OVERDUE_GROUP2);

        $content_staff = [];
        $send_success = 0;
        $total = 0;
        $email_staff = Staff::getEmailSaleOnly();
        //OVER DUE due - NOW < 12h
        while ($overdue_1 && ($row = db_fetch_array($overdue_1))) {
            //set title
            if (!isset($content_staff[$row['staff_id']]['title_group1'])) {
                $content_staff[$row['staff_id']]['title_group1'] = '<strong>' . TITLE_TIME_OVERDUE_GROUP1 . '</strong>';
            }

            $string_data = BookingReservation::set_content($row);
            $content_staff[$row['staff_id']][] = $string_data;
        }

        //OVER DUE NOW - due < 24h
        while ($overdue_2 && ($row = db_fetch_array($overdue_2))) {
            //set title
            if (!isset($content_staff[$row['staff_id']]['title_group2'])) {
                $content_staff[$row['staff_id']]['title_group2'] = '<strong>' . TITLE_TIME_OVERDUE_GROUP2 . '</strong>';
            }

            $string_data = BookingReservation::set_content($row);
            $content_staff[$row['staff_id']][] = $string_data;
        }

        //set content and send
        foreach ($content_staff as $staff_id => $content)
        {
            $content_email= '<ol>';
            foreach ($content as $key =>$data)
            {
                if($key !== 'title_group1' && $key !== 'title_group2')
                    $content_email .= '<li>'.$data.'</li>';
                else
                    $content_email .= '<h3>'.$data.'</h3>';

            }
            $content_email.= '</ol>';
            $total++;

            //send mail
            $title = 'Thông báo giữ chỗ';
            $to_name = SYSTEM_EMAIL_NAME;
            $check = MyEmail::send($email_staff[$staff_id], $to_name, $title, $content_email);
            if($check)
                $send_success++;
        }

        echo "There are $send_success/$total email sent for sale \r\n";
        echo date('Y-m-d H:i:s')." Finish";
    }

    //------cron count call
    function runCountCall(){
        global  $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count call \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countCall();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countCall($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                        && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countCall($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }

    //cron count sms
    function runCountSms(){
        global $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count sms \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countSms();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countSms($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                    && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countSms($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }
    function runCountAge(){
        global $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count age \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countAge();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countAge($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                    && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countAge($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        $count_succ = 0;
        $count = 0;
        $total = db_num_rows($res);
        echo "Total $total\r\n";
        while($res && ($row = db_fetch_array($res))){
            try{
                $result = AnalysisAge::insert($row);
                if($result)
                    $count_succ++;
            }
            catch (Exception $ex)
            {
                echo date('Y-m-d H:i:s ').$ex->getMessage()."\r\n";
            }
            $count++;
            $percentage = round($count*100/$total, 1);
            echo "Progress: $percentage%                      \r";
        }
        echo "\r\n";
        echo date('Y-m-d H:i:s').':'.$count_succ.'/'.$total." successed \r\n";
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }
    function runCountPax(){
        global $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count pax \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countPax();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countPax($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                    && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countPax($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        $count_succ = 0;
        $count = 0;
        $total = db_num_rows($res);
        echo "Total $total\r\n";
        while($res && ($row = db_fetch_array($res))){
            try{
                $result = AnalysisPax::insert($row);
                if($result)
                    $count_succ++;
            }
            catch (Exception $ex)
            {
                echo date('Y-m-d H:i:s ').$ex->getMessage()."\r\n";
            }
            $count++;
            $percentage = round($count*100/$total, 1);
            echo "Progress: $percentage%                      \r";
        }
        echo "\r\n";
        echo date('Y-m-d H:i:s').':'.$count_succ.'/'.$total." successed \r\n";
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }
    function runCountBooking(){
        global $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count booking \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countBooking();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countBooking($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                    && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countBooking($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        $count_succ = 0;
        $count = 0;
        $total = db_num_rows($res);
        echo "Total $total\r\n";
        while($res && ($row = db_fetch_array($res))){
            try{
                $result = AnalysisBooking::insert($row);
                if($result) {
                    $count_succ++;
                }
            }
            catch (Exception $ex)
            {
                echo date('Y-m-d H:i:s ').$ex->getMessage()."\r\n";
            }
            $count++;
            $percentage = round($count*100/$total, 1);
            echo "Progress: $percentage%                      \r";
        }
        echo "\r\n";
        echo date('Y-m-d H:i:s').':'.$count_succ.'/'.$total." successed \r\n";
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }
    function runCountTicket(){
        global $argv;
        $isDate = true;
        echo date('Y-m-d H:i:s')." Start cron count ticket \r\n";
        require_once INCLUDE_DIR.'class.analysis.php';

        switch (count($argv)){
            case 1:
                $res = Analysis::countTicket();
                break;
            case 2:
                list($year, $month, $day) = explode('-', $argv[1]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $res = Analysis::countTicket($from);
                }else
                    $isDate = false;
                break;
            case 3:
                list($year, $month, $day) = explode('-', $argv[1]);
                list($year_to, $month_to, $day_to) = explode('-', $argv[2]);
                if(strtotime($argv[1]) && checkdate($month, $day, $year)
                    && strtotime($argv[2]) && checkdate($month_to, $day_to, $year_to)){
                    $from = date('Y-m-d', strtotime($argv[1]));
                    $to = date('Y-m-d', strtotime($argv[2]));
                    $res = Analysis::countTicket($from, $to);
                }else
                    $isDate = false;
                break;
            default:
                echo date('Y-m-d H:i:s') . ' Syntax error. Expect $argv0 [,date $argv1] [,date $argv2]';
                return;
        }
        if(!$isDate){
            echo date('Y-m-d H:i:s')." Error! Invalid date format.Please enter the date in the format as \"2019-09-09\". \r\n";
            return;
        }
        echo date('Y-m-d H:i:s')." Finish";
        echo "\r\n";
    }

    public static function runMigrateTour() {

        echo date('Y-m-d H:i:s')." Start cron migrate tour \r\n";
        require_once INCLUDE_DIR.'class.pax.php';
        $sql = "SELECT DISTINCT id, name, destination, tour_leader, gateway, gather_date, return_date, quantity, net_price, retail_price,
            transit_airport, airline, country_id, sure_quantity, hold_quantity, created_at, created_by, last_updated_at, last_updated_by,
            tl_quantity, log_id_ost_tour, status, departure_date
            FROM ".TOUR_TABLE."";
        $res = db_query($sql);
        $count = 0;
        $total = db_num_rows($res);
        while ($res && ($row = db_fetch_array($res))) {
            try {
               $tour = TourNew::insert_to_table($row);
               if (!$tour) continue;
            }
            catch (Exception $ex)
            {
                echo date('Y-m-d H:i:s').$ex->getMessage()."\r\n";
            }
            $count++;
            echo "Progress: $count / $total\r";
        }
        echo "DONE: ".date("Y-m-d H:i:s")."\r\n";
    }

    public static function runUpdateHashUrlReceipt()
    {
        echo date('Y-m-d H:i:s')." Start cron migrate tour \r\n";
        require_once INCLUDE_DIR.'class.receipt.php';
        $sql = "SELECT id, hash_url FROM receipt";
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            try{
                ReceiptLog::get_hash_url_log((int)$row['id']);
            }
            catch (Exception $ex)
            {
                echo date('Y-m-d H:i:s ').$ex->getMessage()."\r\n";
            }
        }
        echo "DONE: ".date("Y-m-d H:i:s")."\r\n";
    }
    //cron send booking lack commitment || contract
    function runSendEmailBookingLackDocument(){
        echo date('Y-m-d H:i:s')." Start cron mail send booking lack document \r\n";

        require INCLUDE_DIR.'class.pax.php';
        require_once __DIR__.'/class.myemail.php';

        $from = date('Y-m-d', time() - 1*30*24*60*60);
        $to = date('Y-m-d');
        $resultLackCommitment = Booking::getBookingLackDocument($from, $to, BookingDocument::COMMITMENT_FILE_UPLOAD);
        $resultLackContract = Booking::getBookingLackDocument($from, $to, BookingDocument::CONTRACT_FILE_UPLOAD);

        $content_staff = [];
        $send_success = 0;
        $total = 0;
        $email_staff = Staff::getEmailSaleOnly(false, true);
        // LACK commitment
        while ($resultLackCommitment && ($row = db_fetch_array($resultLackCommitment))) {
            //set title
            if (!isset($content_staff[$row['staff_id']]['title_group1'])) {
                $content_staff[$row['staff_id']]['title_group1'] = '<strong>' . TITLE_BOOKING_LACK_COMMITMENT . '</strong>';
            }

            $string_data = $row['booking_code'];
            $content_staff[$row['staff_id']][] = $string_data;
        }

        //LACK contract
        while ($resultLackContract && ($row = db_fetch_array($resultLackContract))) {
            //set title
            if (!isset($content_staff[$row['staff_id']]['title_group2'])) {
                $content_staff[$row['staff_id']]['title_group2'] = '<strong>' . TITLE_BOOKING_LACK_CONTRACT . '</strong>';
            }

            $string_data = $row['booking_code'];
            $content_staff[$row['staff_id']][] = $string_data;
        }

        //set content and send
        foreach ($content_staff as $staff_id => $content)
        {
            $content_email= '<ol>';
            foreach ($content as $key =>$data)
            {
                if($key !== 'title_group1' && $key !== 'title_group2')
                    $content_email .= '<li>'.$data.'</li>';
                else
                    $content_email .= '<h3>'.$data.'</h3>';

            }
            $content_email.= '</ol>';
            $total++;

            //send mail
            $title = 'Thông báo booking';
            $to_name = SYSTEM_EMAIL_NAME;
            $check = MyEmail::send($email_staff[$staff_id], $to_name, $title, $content_email);
            if($check)
                $send_success++;
        }

        echo "There are $send_success/$total email sent for sale \r\n";
        echo date('Y-m-d H:i:s')." Finish";
    }
}
?>

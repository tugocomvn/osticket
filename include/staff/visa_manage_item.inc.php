<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canManageVisaItems()) die('Access Denied');
?>
<style>
    table.list tbody td {
        padding: 0.75em;
    }
</style>
<h2>Document Items Management</h2>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <tbody>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_paper_require.php" ?>">Required Documents</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/status_finance.php" ?>">Finance Status</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_status_job.php" ?>">Job Statuses</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_status_marital.php" ?>">Marital Statuses</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_status.php" ?>">Visa Statuses</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_passport_location.php" ?>">Passport Locations</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_type.php" ?>">Visa types</a></td></tr>
    <tr><td><a href="<?php echo $cfg->getUrl() ."scp/visa_appointment.php" ?>">Visa appointment</a></td></tr>
    </tbody>
</table>
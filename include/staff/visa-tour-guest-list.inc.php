<?php
// list all guests

if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canMangeVisaPaxDocuments() && !$thisstaff->canViewVisaDocuments()) die('Access Denied');
?>
<style>
    .label-female {
        background: deeppink;
    }
    .tab-content {
        overflow: visible;
    }
</style>
<h2><?php echo __('Visa Documents - All Tours'); ?></h2>
<form action="<?php echo $cfg->getUrl() ."scp/visa-tour-guest.php" ?>" method="get">
    <?php csrf_token(); ?>
    <table>
        <tr>
            <td>Full Name</td>
            <td>
                <input type="text" name="name" value="<?php
                if (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) echo trim($_REQUEST['name']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Day of Birth</td>
            <td>
                <input style="width: 100px;" type="text" name="dob" value="<?php
                if (isset($_REQUEST['dob']) && date_create_from_format('Y-m-d', $_REQUEST['dob'])) echo date('d/m/Y', strtotime($_REQUEST['dob'])) ?>" class="input-field dp" placeholder="" />
            </td>
            <td>Passport No.</td>
            <td>
                <input type="text" name="passport_code" value="<?php
                if (isset($_REQUEST['passport_code']) && !empty($_REQUEST['passport_code'])) echo trim($_REQUEST['passport_code']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Phone</td>
            <td>
                <input type="text" name="phone_number" value="<?php
                if (isset($_REQUEST['phone_number']) && !empty($_REQUEST['phone_number'])) echo trim($_REQUEST['phone_number']);
                ?>" class="input-field" placeholder="" />
            </td>

        </tr>
        <tr>
            <td>Booking Code</td>
            <td>
                <input type="text" name="booking_code" value="<?php
                if (isset($_REQUEST['booking_code']) && !empty($_REQUEST['booking_code'])) echo trim($_REQUEST['booking_code']);
                ?>" class="input-field" placeholder="" />
            </td>

            <td><label for="visa_result">Visa Document Status</label></td>
            <td>
                <select class="input-field visa_result" name="visa_result">
                    <option value=>- All -</option>
                    <?php foreach(\Tugo\VisaStatusList::caseTitleName() as $_id => $_name): ?>
                        <option value="<?php echo $_id ?>"
                            <?php if(isset($_REQUEST['visa_result']) && $_REQUEST['visa_result'] && ($_REQUEST['visa_result'] == $_id)) echo 'selected'?>>
                            <?php echo $_name ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>Tên Tour</td>
            <td>
                <input type="text" name="keyword_tour" value="<?php
                if (isset($_REQUEST['keyword_tour']) && !empty($_REQUEST['keyword_tour']))
                    echo trim($_REQUEST['keyword_tour']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Nhân viên phụ trách</td>
            <td>
                <select name='staff' class="staff">
                    <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                    <?php while ($listStaff && ($row = db_fetch_array($listStaff))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                            $row['staff_id'],($_REQUEST['staff'] == $row['staff_id']?'selected="selected"':''),$row['username']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Điểm đến</td>
            <td>
                <select name="destination[]" class="input-field" multiple style="height: 12em;">
                    <option value=><?php echo __('-- All --') ?></option>
                    <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                        <?php foreach($booking_type_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                <?php if (isset($_REQUEST['destination']) && in_array($item['id'],$_REQUEST['destination'])) echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="6">
                <input type="submit" href="" class="btn_sm btn-primary" Value="<?php echo __('Search');?>" />
                <input type="reset" class="btn_sm btn-default" Value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
            </td>
        </tr>
        <script>
            function reset_form(event) {
                console.log(event);
                form = $(event).parents('form');
                form.find('input[type="text"], select, .hasDatepicker').val('');
                form.submit();
            }
        </script>
    </table>
</form>
<?php include STAFFINC_DIR.'visa.list-guest-tour.inc.php' ?>

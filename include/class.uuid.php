<?php

class UUID {
    public static function get($type) {
        $sql = sprintf(
            "SELECT MAX(uuid_value) as uuid_value FROM %s WHERE `type` LIKE %s LIMIT 1",
            UUID_TABLE,
            db_input($type)
        );
        $res = db_query($sql);
        if (!$res) return null;

        $row = db_fetch_array($res);

        if ($row && isset($row['uuid_value']))
            return $row['uuid_value'];

        return null;
    }

    public static function set($type, $value) {
        $value = sprintf('%07d', $value);
        $row = null;
        $sql = sprintf(
            "SELECT * FROM %s WHERE
                trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM `uuid_value`))>%s
                AND `type` LIKE %s",
            UUID_TABLE,
            db_input(ltrim(str_replace('TG123-', '', $value)), false),
            db_input($type)
        );
        $res = db_query($sql);
        if ($res) {
            $row = db_fetch_array($res);
        }

        if (!$res || !$row) {
            $sql = sprintf(
                "REPLACE INTO %s SET uuid_value = %s, `type` = %s",
                UUID_TABLE,
                db_input($value),
                db_input($type)
            );
            db_query($sql);
        }

        $sql = sprintf(
            "DELETE FROM %s WHERE `type` LIKE %s
                AND trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM `uuid_value`)) like %s",
            UUID_TMP_TABLE,
            db_input($type),
            db_input(ltrim(str_replace('TG123-', '', $value), '0'))
        );
        db_query($sql);
    }

    public static function generate($type) {
        $value = self::get($type); // get max value existing in DB
        if ($value < 100000) {
            $value = 100000;
        }

        do {
            $value = $value ? ++$value : 1;
        } while ( !self::check_valid($type, sprintf('%07d', $value)) );

        try {
            if ($value) {
                $value = sprintf('%07d', $value);
                $sql = sprintf(
                    "INSERT INTO %s SET uuid_value = %s, `type` = %s",
                    UUID_TMP_TABLE,
                    db_input($value),
                    db_input($type)
                );
                db_query($sql);
            }
        } catch(Exception $e) {
            return self::generate($type);
        }

        return $value;
    }

    private static function check_valid($type, $value) {
        $sql = sprintf(
            "SELECT uuid_value FROM %s WHERE `type` LIKE %s
                AND trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM `uuid_value`)) like %s LIMIT 1",
            UUID_TMP_TABLE,
            db_input($type),
            db_input(ltrim(str_replace('TG123-', '', $value), '0'))
        );
        $res = db_query($sql);
        $row = db_fetch_array($res);

        if ($row && isset($row['uuid_value']) && $row['uuid_value']) return false;

        switch ($type) {
            case 'booking_code':
                $check = self::check_valid_booking_code($value);
                break;
            default:
                $check = false;
                break;
        }

        return $check;
    }

    private static function check_valid_booking_code($value) {
        $sql = sprintf(
            "SELECT `value` FROM %s WHERE `field_id` = %s
                AND trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM `value`)) LIKE %s LIMIT 1",
            FORM_ANSWER_TABLE,
            BOOKING_CODE_FIELD,
            db_input(ltrim(str_replace('TG123-', '', $value), '0'))
        );
        $res = db_query($sql);
        $row = db_fetch_array($res);

        if ($row && isset($row['value']) && $row['value']) return false;
        return true;
    }
}

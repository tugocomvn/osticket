alter table ost_ticket
  add unassign tinyint default 0 null;

update ost_ticket set ost_ticket.unassign = 0 where ost_ticket.unassign is null or ost_ticket.unassign like '';

alter table ost_ticket modify unassign tinyint default 0 not null;

create index ost_call_log_direction_index
  on ost_call_log (direction);

create index ost_form_entry_values_entry_id_index
  on ost_form_entry_values (entry_id);

create index ost_form_entry_values_field_id_index
  on ost_form_entry_values (field_id);

ALTER TABLE ost_form_entry_values
  CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
/*
SELECT COUNT(t.ticket_id)
FROM ost_ticket t
       JOIN ost_user u ON t.user_id = u.id
  AND t.topic_id IN (1, 13, 20, 12)
  AND (
                              TIMESTAMPDIFF(SECOND, IFNULL(t.updated,
                                                           IFNULL(lastmessage, IFNULL(lastresponse, t.created))),
                                            NOW())
                              >= 3600 * 48
                            )
       JOIN ost_form_entry e ON u.id = e.object_id AND e.object_type = 'U'
       JOIN ost_form_entry_values v ON e.id = v.entry_id
  AND v.field_id = 3
  AND v.phone_number <> '0'
  AND v.phone_number <> ''
  AND v.phone_number IS NOT NULL
WHERE 1
  AND t.status_id IN (
  select ts.id FROM ost_ticket_status ts WHERE ts.state='open'
)
  AND t.created>='2019-01-01 00:00:00'
  AND (
      t.created = t.updated
    OR
      v.phone_number NOT IN (
        SELECT destinationnumber FROM ost_call_log lg WHERE 1
                                                        AND lg.add_time >='2019-01-01 00:00:00'
                                                        AND (v.phone_number LIKE lg.destinationnumber OR v.phone_number LIKE CONCAT('0', lg.destinationnumber) )
                                                        AND t.created < lg.starttime
                                                        AND lg.direction LIKE 'outbound'
      )
  )


ORDER BY IFNULL(t.updated, IFNULL(lastmessage, IFNULL(lastresponse, t.created))) DESC
LIMIT 100;
*/

create table ost_ticket_unassign_log
(
  id int auto_increment,
  ticket_id int null,
  unassign tinyint null,
  created_at datetime null,
  constraint ost_ticket_unassign_log_pk
    primary key (id)
);

create index ost_ticket_unassign_log_created_at_index
  on ost_ticket_unassign_log (created_at);

create index ost_ticket_unassign_log_ticket_id_index
  on ost_ticket_unassign_log (ticket_id);

create index ost_ticket_unassign_log_unassign_index
  on ost_ticket_unassign_log (unassign);




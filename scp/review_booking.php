<?php
require('staff.inc.php');

include_once INCLUDE_DIR.'class.booking.php';
include_once INCLUDE_DIR.'class.pax.php';

$visaReview = BookingReservation::visaReview();
$visaReviewed = BookingReservation::visaReviewed();

$nav->setTabActive('visa');
//$nav->setActiveSubMenu(3,'visa');
$ost->addExtraHeader('<meta name="tip-namespace" content="visa.visa" />',
    "$('#content').data('tipNamespace', 'visa.visa_tour_list');");

$page = "review_booking.inc.php";
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');

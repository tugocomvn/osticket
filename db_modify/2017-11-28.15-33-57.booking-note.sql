CREATE TABLE booking_note
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  booking_ticket_id INT,
  content TEXT
);
CREATE INDEX booking_note_booking_ticket_id_index ON booking_note (booking_ticket_id);
CREATE INDEX `booking_note_content190_index` ON booking_note (`content`(190));

CREATE TABLE booking_status
(
  booking_ticket_id INT PRIMARY KEY,
  booking_status_id TINYINT
);
CREATE INDEX booking_status_booking_status_index ON booking_status (booking_status_id);
ALTER TABLE booking_status MODIFY booking_status_id INT;

CREATE TABLE booking_status_log
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  booking_ticket_id INT,
  booking_status_id INT,
  updated_at DATETIME,
  updated_by INT
);
CREATE INDEX booking_status_log_booking_ticket_id_index ON booking_status_log (booking_ticket_id);
CREATE INDEX booking_status_log_booking_status_id_index ON booking_status_log (booking_status_id);
CREATE INDEX booking_status_log_updated_at_index ON booking_status_log (updated_at);
CREATE INDEX booking_status_log_updated_by_index ON booking_status_log (updated_by);

CREATE TABLE booking_status_list
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(64)
);
CREATE UNIQUE INDEX booking_status_list_name_uindex ON booking_status_list (name);

ALTER TABLE booking_status_list CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE booking_note CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE booking_status_list ADD is_default TINYINT(1) DEFAULT 0 NULL;
CREATE INDEX booking_status_list_is_default_index ON booking_status_list (is_default);

ALTER TABLE booking_status DROP PRIMARY KEY;
ALTER TABLE booking_status ADD PRIMARY KEY (booking_ticket_id, booking_status_id);
DROP INDEX booking_status_booking_status_index ON booking_status;
CREATE INDEX booking_status_booking_status_id_index ON booking_status (booking_status_id);
ALTER TABLE booking_status DROP PRIMARY KEY;
ALTER TABLE booking_status ADD PRIMARY KEY (booking_ticket_id);

ALTER TABLE booking_status_list ADD is_finished TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE booking_status_list ADD class VARCHAR(16) NULL;

ALTER TABLE booking_note ADD created_at DATETIME NULL;
CREATE INDEX booking_note_created_at_index ON booking_note (created_at);
ALTER TABLE booking_note ADD created_by INT NULL;
CREATE INDEX booking_note_created_by_index ON booking_note (created_by);
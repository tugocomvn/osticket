<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class OfflateModel extends \VerySimpleModel {
    static $meta = [
        'table' => STAFF_OL_TABLE,
        'pk' => ['ticket_id'],
    ];
}

class Offlate extends OfflateModel {
    public static function calc($start, $end) {
        if($end - $start <= 7200)
        {
            return "Đi trễ";
        }elseif($end - $start > 7200 && $end - $start <= 18000){
            return "Nghỉ nửa ngày";
        }else{
            return ceil(abs(($end + 1) - $start) / 86400);
        }
    }

    public static function status($status) {
        switch ($status) {
            case '0':
            case '':
                return 'Chờ duyệt...';
                break;
            case '1':
                return '<span class="label label-primary">Duyệt lần 1</span>';
                break;
            case '2':
                return '<span class="label label-success">Duyệt xong</span>';
                break;
            case '-1':
                return '<span class="label label-warning">Xem lại</span>';
                break;
        }
    }

    public static function count() {
        global $thisstaff;
        $sql = sprintf("select count(distinct  ticket_id)
            from offlate_tmp where staff_offer = %s
            and ol_status=-1", $thisstaff->getId());

        $alert = db_count($sql);

        $sql = sprintf("select count(distinct  t.ticket_id)
            from offlate_tmp o join ost_ticket t on t.ticket_id=o.ticket_id
            where t.staff_id = %s
                   and ( (o.ol_status is null or o.ol_status like '') 
                   OR (o.ol_status < 2 AND o.ol_status > -1) )", $thisstaff->getId());

        $approve = db_count($sql);

        return ['alert' => $alert ?: 0, 'approve' => $approve ?: 0];
    }
}

/*branch hotfix/24.3.4 */

alter table tour_pax_visa_profile
	add visa_image_filename text null;
	
alter table tour_pax_visa_profile
	add visa_image_uploaded_by int unsigned null;

alter table tour_pax_visa_profile
	add visa_image_uploaded_at datetime null;
	
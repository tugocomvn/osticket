<?php
namespace Booking;

require_once(INCLUDE_DIR . 'class.orm.php');

class NoteModel extends \VerySimpleModel {
    static $meta = [
        'table' => BOOKING_NOTE_TABLE,
        'pk' => [
            'id'
        ],
    ];
}

class Note extends NoteModel {

}

<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canEditOfflates() || !$ticket) die('Access Denied');
?>

<?php
$info=Format::htmlchars(($errors && $_POST)?$_POST:$ticket->getUpdateInfo());
if ($_POST)
    $info['duedate'] = Format::date($cfg->getDateFormat(),
        strtotime($info['duedate']));
$offlate_code = Ticket::fromOfflateTicketId($ticket->getId());
?>

<form action="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-ol" method="post" id="save"  enctype="multipart/form-data">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="update">
    <input type="hidden" name="a" value="edit-ol">
    <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
    <h2><?php echo sprintf(__('Update Ticket #%s'),$ticket->getNumber());?></h2>
    <table class="form_table" width="1058" border="0" cellspacing="0" cellpadding="2">
        <tbody>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Ticket Information'); ?></strong>: <?php echo __("Due date overrides SLA's grace period."); ?></em>
            </th>
        </tr>

        <tr>
            <td width="160" class="required">
                <?php echo __('Help Topic');?>:
            </td>
            <td>
                <select disabled="disabled">
                    <option value="" selected >&mdash; <?php echo __('Select Help Topic');?> &mdash;</option>
                    <?php
                    $this_help_id = null;
                    if($topics=Topic::getHelpTopics()) {
                        foreach($topics as $id =>$name) {
                            if (!in_array($id, [OL_FORM])) continue;
                            if ($info['topicId']==$id) $this_help_id = $id;
                            echo sprintf('<option value="%d" %s>%s</option>',
                                $id, ($info['topicId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                <input type="hidden" name="topicId" value="<?php echo $this_help_id ?>">
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>

        </tr>
        </tbody>
    </table>
    <table class="form_table dynamic-forms" width="940" border="0" cellspacing="0" cellpadding="2">
        <?php
        if ($forms) {
            $this_topic = $ticket->getTopic();
            foreach ($forms as $form) {
                if (!$form || !$form->getForm()) continue;

                if ($this_topic->getFormId() == $form->getForm()->get('id'))
                    $form->render(
                        true,
                        false,
                        array('mode'=>'edit','width'=>160,'entry'=>$form, 'disabled' => ['booking_code'], 'booking' => 1)
                    );
            }
        } ?>

        <?php
        if($thisstaff->canAssignTickets()) { ?>
            <tr>
                <td width="160" style="font-weight: bold"><?php echo __('Người duyệt');?>:</td>
                <td>
                    <select id="assignId" name="assignId" required>
                        <option value=>&mdash; <?php echo __('Select Staff Accept');?> &mdash;</option>
                        <?php
                        if(($users=Staff::getAvailableStaffMembers())) {
                            foreach($users as $id => $name) {
                                if ($thisstaff->getId() == $id) continue;
                                $k="s$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                             $k,(($info['assignId']==$k || $ticket->getStaffId() ==$id)?'selected="selected"':''),$name);
                            }
                        }
                        ?>
                    </select>&nbsp;<span class='error'>&nbsp;<?php echo $errors['assignId']; ?></span>
                </td>
            </tr>
        <?php } ?>
    </table>
    <p style="padding-left:250px;">
        <input type="submit" class="btn_sm btn-primary" name="submit" value="<?php echo __('Save');?>">
        <input type="reset"  class="btn_sm btn-default" name="reset"  value="<?php echo __('Reset');?>">
        <input type="button" class="btn_sm btn-danger" name="cancel" value="<?php echo __('Cancel');?>" onclick='window.location.href="tickets.php?id=<?php echo $ticket->getId(); ?>"'>
    </p>
</form>
<div style="display:none;" class="dialog draggable" id="user-lookup">
    <div class="body"></div>
</div>
<script type="text/javascript">
    $('table.dynamic-forms').sortable({
        items: 'tbody',
        handle: 'th',
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).children().each(function() {
                    $(this).width($(this).width());
                });
            });
            ui=ui.clone().css({'background-color':'white', 'opacity':0.8});
            return ui;
        }
    });
</script>

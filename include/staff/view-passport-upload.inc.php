<?php
if (!defined('OSTSCPINC') || !$thisstaff) {
    die('Invalid path');
}

if (!$thisstaff->canViewVisaDocuments()) {
    die('Access Denied');
}
?>

<h2>Hiển thị ảnh và thông tin passport</h2>
<table style="margin-left: 25%; margin-top: 10px">
    <tr>
        <td>Tên khách</td>
        <td><?php echo getNamePax($result['result']) ?></td>
    </tr>
    <tr>
        <td>Số passport</td>
        <td> <?php echo getPassportNo($result['result']) ?></td>
    </tr>
    <tr>
        <td>Mã booking</td>
        <td> <?php echo $result['booking_code'] ?></td>
    </tr>
    <tr>
        <td>Tour name</td>
        <td><?php echo $result['tour_name'] ?></td>
    </tr>
    <tr>
        <td>Người upload</td>
        <td><?php echo $listStaff[$result['upload_by']] ?></td>
    </tr>
    <tr>
        <td>Thời gian upload</td>
        <td> <?php if ($result['upload_at'] !== 0 && $result['upload_at'] !== null)
                echo date('d/m/Y H:i', strtotime($result['upload_at'])) ?></td>
    </tr>
    <tr>
        <td>Trang thái:</td>
        <td>
            <?php switch ((int)$result['status']):
                case \Tugo\PassportPhoto::PASSPORT_SCAN_WAITING: ?>
                    <span class="label label-small label-default">Chưa xử lý</span>
                    <?php break;
                case \Tugo\PassportPhoto::PASSPORT_SCAN_FAILURE: ?>
                    <span class="label label-small label-danger">Thất bại</i></span>
                    <?php break;
                case \Tugo\PassportPhoto::PASSPORT_SCAN_SUCCESS: ?>
                    <span class="label label-small label-success">Thành công</i></span>
                <?php break; endswitch; ?>
        </td>
    </tr>
</table>
<br>
<div class="mt-5 centered">
    <?php if (isset($dataImage) && $dataImage !== null) :?>
        <img src="<?php echo $dataImage ?>" width="80%">
    <?php else: ?>
        <div>No find passport photo</div>
    <?php endif; ?>
</div>

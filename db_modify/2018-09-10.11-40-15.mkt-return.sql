CREATE TABLE booking_phone_number
(
  ticket_id int PRIMARY KEY,
  booking_code varchar(13),
  phone_number varchar(11),
  date datetime
);
CREATE INDEX booking_phone_number_booking_code_index ON booking_phone_number (booking_code);
CREATE INDEX booking_phone_number_phone_number_index ON booking_phone_number (phone_number);
CREATE INDEX booking_phone_number_date_index ON booking_phone_number (date);

CREATE UNIQUE INDEX booking_phone_number_phone_number_booking_code_date_uindex ON booking_phone_number (phone_number, booking_code, date);
DROP INDEX booking_phone_number_booking_code_index ON booking_phone_number;
DROP INDEX booking_phone_number_phone_number_index ON booking_phone_number;
DROP INDEX booking_phone_number_date_index ON booking_phone_number;
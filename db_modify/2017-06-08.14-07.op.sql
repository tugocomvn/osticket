ALTER TABLE ost_list ADD allow_user_update TINYINT(2) DEFAULT 0 NULL;

ALTER TABLE ost_groups ADD can_view_operator_list TINYINT(1) DEFAULT '0' NULL;
ALTER TABLE ost_groups ADD can_edit_operator_list TINYINT(1) DEFAULT '0' NULL;
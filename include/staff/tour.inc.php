<?php
if (!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
.required {
    color: #FF0000;
}
.input-field{
    padding: 6px 12px;
    border: 1px solid #ccc;
    margin-bottom: 15px;
}
table, td {
    margin: auto;
    margin-bottom: 10px;
}
.ui-button {
    padding: 0;
}

.ui-selectmenu-icon.ui-icon {
    margin-top: 0.75em;
}

.ui-selectmenu-open {
    z-index: 999;
}
</style>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl() . "scp/tour-list.php" ?>">Back</a></p>
<h2>Tour - Fast Create</h2>
    <table>
        <tr>
            <td class="label-input">Điểm đến<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <select name="booking_type_id" id="booking_type" class="input-field" style="height:30px">
                <option value=>- Select -</option>
                    <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                        <?php foreach($booking_type_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                    <?php if (isset($_REQUEST['booking_type_id']) && $_REQUEST['booking_type_id'] == $item['id']) echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label-input">Hãng bay<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <select name="airline" id="airline" class="input-field" style="height:30px">
                <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($airlines_list && ($row = db_fetch_array($airlines_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($row['id']?'"selected"':''),$row['airline_name']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Điểm quá cảnh</td>
        </tr>
        <tr>
            <td>
                <select id="transit_airport" class="input-field" style="height:30px">
                    <option value=> BAY THẲNG </option>
                    <?php while ($transits_airport_list && ($row = db_fetch_array($transits_airport_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($row['id']?'"selected"':''),$row['transit_airport']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Điểm khởi hành</td>
        </tr>
        <tr>
            <td>
                <select id="gateway_id" class="input-field" style="height:30px">
                    <?php while ($departure_flight_list && ($row = db_fetch_array($departure_flight_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($row['id']?'"selected"':''),$row['departure_flight']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Ngày và giờ tập trung<span class="required">(*)</span> (có thể ước lượng 3-4 giờ trước giờ bay)</td>
        </tr>
        <tr>
            <td>
                <input type="time" id="gateway_present_time_" size=15 name="gather_t">
                <input class="dp input-field" type="text" id="gateway_present_time" size=15 name="gather_d">
            </td>
        </tr>
        <tr>
            <td>Ngày khởi hành<span class="required">(*)</span></td>
        </tr>
        <tr>
            <td>
                <input type="text" class="dp input-field" size="15" id="departure_date">
            </td>
        </tr>
        <tr>
            <td>Ngày về<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <input class="dp input-field" type="text" id="return_time" size=15 value="" ></td>
            </td>
        </tr>
        <?php if($thisstaff->canEditRetailPrice()):?>
        <tr>
            <td>Giá bán khách</td>
        </tr>
        <tr>
            <td>
                <input type="number" class="input-field" min="0" id="retail_price"><span id="retail_price_money"></span>
                <script>
                    $('#retail_price').off('change, keyup').on('change, keyup', function() {
                        money_format();
                    });
                </script>
            </td>
        </tr>
        <?php endif;?>  
        <?php if ($thisstaff->canEditNetPrice()):?>
        <tr>
            <td>Giá net</td>
        </tr>
        <tr>
            <td>
                Số lượng  <=
                <input type="text" style="width: 20px;" maxlength="3" id="first_quantity" class="input-field">
                <span>Giá</span>
                <input type="text" class="input-field" min="0" id="first_net_price"><span id="first_net_price_money"></span>
            </td>
            <script>
                $('#first_net_price').off('change, keyup').on('change, keyup', function() {
                    money_format();
                });
            </script>
        </tr>
        <tr>
            <td>
                Số lượng  <=
                <input type="text" style="width: 20px;" maxlength="3" id="second_quantity" class="input-field">
                <span>Giá</span>
                <input type="text" class="input-field" min="0" id="second_net_price"><span id="second_net_price_money"></span>
            </td>
            <script>
                $('#second_net_price').off('change, keyup').on('change, keyup', function() {
                    money_format();
                });
            </script>
        </tr>
        <tr>
            <td>
                Số lượng  <=
                <input type="text" style="width: 20px;" maxlength="3" id="third_quantity" class="input-field">
                <span>Giá</span>
                <input type="text" class="input-field" min="0" id="third_net_price"><span id="third_net_price_money"></span>
            </td>
            <script>
                $('#third_net_price').off('change, keyup').on('change, keyup', function() {
                    money_format();
                });
            </script>
        </tr>
        <tr>
            <td>
                <span>Trường hợp còn lại, Giá</span>
                <input type="text" class="input-field" min="0" id="max_net_price"><span id="net_price_money"></span>
            </td>
            <script>
                $('#max_net_price').off('change, keyup').on('change, keyup', function() {
                    money_format();
                });
            </script>
        </tr>
        <?php endif;?>
        <tr>
            <td>Giảm giá tối đa</td>
        </tr>
        <tr>
            <td>
                <input type="number" class="input-field" min="0" id="maximum_discount"><span id="maximum_discount_money"></span>
                <script>
                    $('#maximum_discount').off('change, keyup').on('change, keyup', function() {
                        money_format();
                    });
                </script>
            </td>
        </tr>
        <tr>
            <td>Tour kích cầu
                <input name="check_stimulus_tour" id="check_stimulus_tour" type="hidden" >
                <input type="checkbox" name="stimulus_tour" id="stimulus_tour">
            </td>
        </tr>
        <tr>
            <td>Tour leader</td>
        </tr>
        <tr>
            <td>
                <select id="tour_leader_id" class="input-field" style="height:30px">
                    <?php while ($leaders_list && ($row = db_fetch_array($leaders_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($row['id']?'"selected"':''),$row['name_tl']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Số khách tối đa<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <input style="width :80px;" type="number" class="input-field" min="0" id="pax_quantity"></td>
            </td>
        </tr>
        <tr>
            <td>Số lượng leader</td>
        </tr>
        <tr>
            <td>
                <input style="width :80px;" type="number" max="4"
                       class="input-field" min="1" id="tl_quantity" value="1"></td>
            </td>
        </tr>
        <tr>
            <td>Note</td>
        </tr>
        <tr>
            <td>
                <textarea  rows="5" cols="50" id="note" placeholder="Nhập nội dung ...."></textarea>
            </td>
        </tr>
        <tr>
            <td>Chọn màu</td>
        </tr>
        <tr>
            <td>
                <select name="color" id="color">
                    <option value=>- Select -</option>
                    <?php foreach($list_colors as $key => $value): ?>
                        <option value="<?php echo $key ?>"
                            <?php if($key===$tour->color_code): ?>
                                selected
                            <?php endif; ?>
                            <?php if (isset($results->color_code) && $results->color_code == $key)
                                echo "selected" ?>
                                data-style="background-color: <?php echo $key ?>"
                        >
                            <?php echo $key ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
    <hr>
    <div>
        <div>
            <h3>Tour Name:<span class="required">(*)</span> </h3>
            <input class="input-field" style="width: 500px;" type="text" id="tour_name"></td>
        </div>
        <p>
            <button name="view" id="view">Xem trước nội dung</button>
        </p>
        <div id="message" class="message">Click "Xem trước nội dung" to load data format</div>
        <p>Chú ý: Mỗi dòng là thông tin của 01 chặng bay. Bao gồm cả code vé</p>
        <div id="main_flight_ticket"></div>
        <h2>Xem trước nội dung</h2>
        <div id="view_flight_ticket"></div>
        <p class="centered">
            <button class="btn_sm btn-primary" id="save" name="save">Confirm & Save</button>
            <a href="<?php echo $cfg->getUrl()."scp/tour-list.php"?>" class="btn_sm btn-danger">Cancel</a>
        </p>
    </div>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        var _hot;
        function remove_row(row){
            //_hot.alter('remove_row', row, 1);
            _hot.setDataAtCell(row,0,null);
            _hot.setDataAtCell(row,1,null);
            _hot.setDataAtCell(row,2,null);
            _hot.setDataAtCell(row,3,null);
            _hot.setDataAtCell(row,4,null);
        }
        $(document).ready(function () {
        var $refreshButton = $('#save');
        $('#booking_type option:first-child').attr('disabled', true);
        var $container = $("#main_flight_ticket");
        var $console = $("#message");
        var $parent = $container.parent();
        _hot = new Handsontable($container[0], {
            columnSorting: true,
            startRows: 4,
            startCols: 6,
            rowHeaders: true,
            className: "htCenter",
            width: '100%',
            licenseKey: 'non-commercial-and-evaluation',
            colWidths: [150, 150, 150, 200, 300, 50],
            height: '200px',
            nestedHeaders: [
                ['CODE', 'NGÀY', 'SHCB', 'HÀNH TRÌNH', 'GIỜ BAY', '#'],
            ],
            columns: [
                {},
                {
                    allowInvalid: false
                },
                {
                    allowInvalid: false
                },
                {
                    allowInvalid: false
                },
                {},
                {data: "action", renderer: "html"}
            ],
            minSpareCols: 0,
            minSpareRows: 1,
            contextMenu: true,
            cells: function (row, col, prop, value) {
                var cellProperties = {};
                cellProperties.renderer = fisrtRowRenderer;
                return cellProperties;
            }
        });
        function fisrtRowRenderer(instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            if (!value || value === '' || value == null) {
                td.innerHTML = "";
            }
            if(col === 5){
                switch (row) {
                    case 0: td.innerHTML = "<buttton class='btn_sm btn-xs btn-primary' onclick='remove_row("+row+")'>Clear</button>";
                            break;
                    case 1:td.innerHTML = "<buttton class='btn_sm btn-xs btn-primary' onclick='remove_row("+row+")'>Clear</button>";
                        break;
                    case 2:td.innerHTML = "<buttton class='btn_sm btn-xs btn-primary' onclick='remove_row("+row+")'>Clear</button>";
                        break;
                    case 3: td.innerHTML = "<buttton class='btn_sm btn-xs btn-primary' onclick='remove_row("+row+")'>Clear</button>";
                        break;
                }
            }
        }
        $parent.find('button[name=view]').click(function () {
            $.ajax({
            url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/tour/load',
            data: {data: _hot.getData()}, // returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
                if (res.result === 1) {
                    var data = [], row;
                    for (var i = 0, ilen = res.all_values.length; i < ilen; i++) {
                    row = [];
                    row[0] = res.all_values[i].flight_ticket_code;
                    row[1] = res.all_values[i].departure_at;
                    row[2] = res.all_values[i].flight_code;
                    row[3] = res.all_values[i].airport_code_from_to;
                    row[4] = res.all_values[i].departure_time;
                    row[5] = res.all_values[i].arrival_time;
                    row[6] = res.all_values[i].arrival_at;
                    data[res.index[i] + 1] = row;
                    }
                    hot.loadData(data);
                    $console.text(res.message);
                    messageSuccess();
                }else{
                    $console.text(res.message);
                    messageError();
                    }
                }
            });
        });
        var
            $_container = $("#view_flight_ticket"),
            $console = $("#message"),
            $parent = $_container.parent(),
            hot;
        hot = new Handsontable($_container[0], {
            columnSorting: true,
            startRows: 4,
            startCols: 6,
            rowHeaders: true,
            className: "htCenter",
            width: '100%',
            licenseKey: 'non-commercial-and-evaluation',
            colWidths: [150, 150, 150, 200, 80, 80, 140],
            height: '200px',
            nestedHeaders: [
                ['CODE', 'NGÀY', 'SHCB', 'HÀNH TRÌNH' , {label: 'GIỜ BAY', colspan: 3}],
            ],
            columns: [
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
                {
                    readOnly: true
                },
            ],
            minSpareCols: 0,
            minSpareRows: 1,
            contextMenu: true,
        });
        $parent.find('button[name=save]').click(function () {
            if (confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {   
                $refreshButton.prop("disabled", true);
                var first_net_price         = $('#first_net_price').val();
                var first_quantity          = $('#first_quantity').val();
                var second_net_price        = $('#second_net_price').val();
                var second_quantity         = $('#second_quantity').val();
                var third_net_price         = $('#third_net_price').val();
                var third_quantity          = $('#third_quantity').val();
                var max_net_price           = $('#max_net_price').val();
                var max_quantity            = Math.max.apply(Math, [first_quantity, second_quantity, third_quantity]);
                var max_quantity            = (max_net_price != "")? (max_quantity + 1): "";
                var tour_leader_id          = $('#tour_leader_id').find(":selected").val();
                var destination             = $('#booking_type').find(":selected").val();
                var transit_airport         = $('#transit_airport').find(":selected").val();
                var airline                 = $('#airline').find(":selected").val();
                var gateway_id              = $('#gateway_id').find(":selected").val();
                var gateway_present_time    = $('#gateway_present_time').val();
                var gateway_present_time_   = $('#gateway_present_time_').val();
                var return_time             = $('#return_time').val();
                var retail_price            = $('#retail_price').val();
                var maximum_discount        = $('#maximum_discount').val();
                var pax_quantity            = $('#pax_quantity').val();
                var tour_name               = $('#tour_name').val();
                var stimulus_tour           = $('#check_stimulus_tour').val();
                var note                    = $('#note').val();
                var color                   = $('#color').find(":selected").val();
                var departure_date          = $('#departure_date').val();
                var tl_quantity             = $('#tl_quantity').val();
                $.ajax({
                    url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/tour/save',
                    data: {
                        "data"                  : _hot.getData(),
                        "tour_leader_id"        : tour_leader_id,
                        "tour_name"             : tour_name,
                        "stimulus_tour"         : stimulus_tour,
                        "destination"           : destination,
                        "airline_id"            : airline,
                        "transit_airport"       : transit_airport,
                        "return_time"           : return_time,
                        "gateway_present_time"  : gateway_present_time,
                        "gateway_present_time_" : gateway_present_time_,
                        "net_price"             : [[first_quantity ,first_net_price], [second_quantity, second_net_price], [third_quantity, third_net_price], [max_quantity, max_net_price]],
                        "retail_price"          : retail_price,
                        "maximum_discount"      : maximum_discount,
                        "pax_quantity"          : pax_quantity,
                        "gateway_id"            : gateway_id,
                        "note"                  : note,
                        "color"                 : color,
                        "tl_quantity"           : tl_quantity,
                        "departure_date"        : departure_date
                    },
                    dataType: 'json',
                    type: 'POST',
                    success: function (res) {
                        if (res.result === 1) {
                            $refreshButton.prop("disabled", true);
                            $console.text(res.message);
                            messageSuccess();
                            <?php $url = $cfg->getUrl().'scp/tour-list.php'; ?>
                            parent.setTimeout(function() { parent.window.location = "<?php echo $url ?>"; }, 1000);
                        } else{
                            $refreshButton.prop("disabled", false);
                            $console.text(res.message);
                            messageError();
                        }
                    },
                });
                }
            });
        });
        $('#booking_type,#gateway_present_time').change(function () {
            var name_= $('#booking_type').find(":selected").text();
            var dept = $('#gateway_present_time').val();
                function reformatDateString(s) {
                    var b = s.split("/");
                    return b.join('.');
                }
            var newdate = reformatDateString(dept);
            var mmyyyy = newdate.substr(3,8);
            note_(name_,mmyyyy,newdate);
        });

        function messageError(){
            document.getElementById("message").style.color = "Red";
        }

        function messageSuccess(){
            document.getElementById("message").style.color = "#4CAF50";
        }

        function note_(name_,mmyyyy,newdate){
            $('#tour_name').val(name_ +' '+ mmyyyy +' '+ newdate);
        }

        function money_format(){
            retail_price = parseInt($('#retail_price').val());
            first_net_price = parseInt($('#first_net_price').val());
            second_net_price = parseInt($('#second_net_price').val());
            third_net_price = parseInt($('#third_net_price').val());
            max_net_price = parseInt($('#max_net_price').val());
            maximum_discount = parseInt($('#maximum_discount').val());
            $('#first_net_price_money').text(first_net_price.formatMoney(0, '.', ',')+'đ');
            $('#second_net_price_money').text(second_net_price.formatMoney(0, '.', ',')+'đ');
            $('#third_net_price_money').text(third_net_price.formatMoney(0, '.', ',')+'đ');
            $('#net_price_money').text(max_net_price.formatMoney(0, '.', ',')+'đ');
            $('#retail_price_money').text(retail_price.formatMoney(0, '.', ',')+'đ');
            $('#maximum_discount_money').text(maximum_discount.formatMoney(0, '.', ',')+'đ');
        }
        money_format();
        $('#color').change(function () {
            $('#color').css("background-color",$(this).children("option:selected").val());
        });
        $('#stimulus_tour').click(function(){
        if($(this).is(':checked')){
            $('input[name="check_stimulus_tour"]').val(1);
        } else {
            $('input[name="check_stimulus_tour"]').val(0);
        }
    });

        (function($) {

            $.widget( "custom.xselectmenu", $.ui.selectmenu, {
                _renderItem: function( ul, item ) {
                    var li = $( "<li>" ),
                        wrapper = $( "<div>", { text: item.label } );

                    if ( item.disabled ) {
                        li.addClass( "ui-state-disabled" );
                    }

                    $( "<span>", {
                        style: item.element.attr( "data-style" ),
                        "class": "ui-icon ui-icon-blank " + item.element.attr( "data-class" )
                    })
                        .appendTo( wrapper );

                    return li.append( wrapper ).appendTo( ul );
                },
                _renderButtonItem: function( item ) {
                    var buttonItem = $( "<span>", {
                        "class": "ui-selectmenu-text"
                    })
                    this._setText( buttonItem, item.label );

                    buttonItem
                        .css( "background-color", item.value )
                        .css("padding", "0.75em")
                        .css('color', 'white');

                    return buttonItem;
                }
            });

            $( "#color" ).xselectmenu();
        })(jQuery);
    </script>

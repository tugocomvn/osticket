
<?php
require('staff.inc.php');
require '../vendor/autoload.php';
$staff_id = $thisstaff->getUserId();
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    //$client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }
    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);
            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

$namedoc="";
function getFileDoc($service, $fileId) {
    global $namedoc;
    global $staff_id;
    try {
        $response = $service->files->export($fileId, 'application/pdf', array(
            'alt' => 'media'));
        $content = $response->getBody()->getContents();
        $namedoc = $service->files->get($fileId)->getName();
        $filename = sprintf("%s.pdf",'document_'.$staff_id);
        $myfile = fopen($filename, "w+") or die("Unable to open file!");
        fwrite($myfile, $content);
        fclose($myfile);
        return 1;
    } catch (Exception $e) {
        return 0;
    }
}
$client = getClient();
$service = new Google_Service_Drive($client);
if (isset($_REQUEST['id']))
{
    $id = $_REQUEST['id'];
    getFileDoc($service,$id);
}

?>
<html>
<body>
<?php
$nav->setTabActive('dashboard');
$nav->setActiveSubMenu(1);
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.dashboard" />',
    "$('#content').data('tipNamespace', 'dashboard.dashboard');");
require(STAFFINC_DIR.'header.inc.php');
$src = sprintf("%s.pdf#toolbar=0&navpanes=0&scrollbar=0",'document_'.$staff_id); //doc file tai ve
?>

<h3>Tài liệu: <?php echo $namedoc ?></h3>
<embed src=<?php echo $src ?> width="100%" height="100%" target="_blank">

<?php
include(STAFFINC_DIR.'footer.inc.php');
?>
</body>
</html>
CREATE TABLE IF NOT EXISTS `tour_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tour_id` INT NULL,
  `content` LONGTEXT NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`tour_id` ASC),
  INDEX `index3` (`created_at` ASC),
  INDEX `index4` (`created_by` ASC))
ENGINE = InnoDB;

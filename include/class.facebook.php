<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\CustomAudience;
use FacebookAds\Object\Values\CustomAudienceTypes;

session_start();

class OST_Facebook {
    static function init(&$errors) {
        $fb = new Facebook([
            'app_id' => APP_ID,
            'app_secret' => APP_SECRET,
        ]);

        $helper = $fb->getRedirectLoginHelper();

        if (!isset($_SESSION['facebook_access_token'])) {
            $_SESSION['facebook_access_token'] = null;
        }

        if (!$_SESSION['facebook_access_token']) {
            $helper = $fb->getRedirectLoginHelper();
            try {
                $_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
            } catch(FacebookResponseException $e) {
                // When Graph returns an error
                $errors[] = 'Graph returned an error: ' . $e->getMessage();
                return false;
            } catch(FacebookSDKException $e) {
                // When validation fails or other local issues
                $errors[] = 'Facebook SDK returned an error: ' . $e->getMessage();
                return false;
            }
        }

        if ($_SESSION['facebook_access_token']) {
            foreach ($_COOKIE as $k=>$v) {
                if(strpos($k, "FBRLH_")!==FALSE) {
                    $_SESSION[$k]=$v;
                }
            }

            return true;
        } else {
            $permissions = ['ads_management'];
            $loginUrl = $helper->getLoginUrl(
                sprintf('https://support.tugo.com.vn/scp/facebook.php?do=%s&source=%s&audience_set=%s', @$_REQUEST['do'], @$_REQUEST['source'], @$_REQUEST['audience_set']),
                $permissions
            );
            header('Location: '.$loginUrl);
            exit;
        }
    }
    static function PushDataToFB($source, $set, &$errors) {
        global $fb_audience_file_name;
        global $fb_audience_file;
        $errors = null;
        self::init($errors);
        if ($errors) return false;

        if (!$source) {
            $errors[] = 'Choose an phone number source!';
            return false;
        }

        if (!$set) {
            $errors[] = 'Choose an audience set!';
            return false;
        }

        set_time_limit(0);
        ini_set('memory_limit', -1);

        // Initialize a new Session and instantiate an Api object
        Api::init(
            APP_ID, // App ID
            APP_SECRET,
            $_SESSION['facebook_access_token'] // Your user access token
        );

        $schema = array(CustomAudienceTypes::PHONE);

        //
        $count = 0;
        $limit = 10000;
        $offset = 0;

        $fb_audience_file_name = md5(uniqid(rand(0,99999), true).microtime(true)).'.csv';
        $fb_audience_file = fopen(ROOT_DIR.'public_file/'.$fb_audience_file_name, 'w');
        $header = $schema;
        fputcsv($fb_audience_file, $header);

        do {
            $audience = new CustomAudience('6065926465709');
            try {
                $check = self::_fetch_and_push($audience, $limit, $offset, $count, $errors, $source);
            } catch (Exception $ex) {
                $errors[ $ex->getCode() ] = $ex->getMessage();
                break;
            }
            $offset += $limit;
//            sleep(2);
        } while($check);

        fclose($fb_audience_file);

        return $count;
    }

    function _fetch_and_push($audience, $limit, $offset, &$count, &$errors, $source = 'all') {
        global $fb_audience_file;
        $flag = false;

        $source = strtolower(trim(strval($source)));
        if ($source == 'all') {
            $sql = "SELECT phone_number as value FROM phone_number LIMIT $offset, $limit";
        } elseif ($source == 'operator') {
            $sql = "SELECT phone_number as value FROM ost_pax_tour LIMIT $offset, $limit";
        } else {
            throw new Exception('Invalid type');
        }

        $phone_list_ = [];
        if(($res=db_query($sql))) {

            $count_tmp = 0;
            while ($row = db_fetch_array($res)) {
                if (!$row || !isset($row['value']) || empty($row['value'])) continue;

                $flag = true;
                $tmp = trim($row['value']);
                $trim = ltrim($tmp, '0');
                if (intval($trim) == 0)
                    continue;
                if (strlen($trim) < 9 || strlen($trim) > 10)
                    continue;

                $tmp_phone = OST_FB_API_Format::PhoneNumber($tmp);

                if (!$tmp_phone) continue;
                $phone_list_[] = [$tmp_phone];
                fputcsv($fb_audience_file, [$tmp_phone]);
                $count_tmp++;
            }

            $phone_list_ = array_filter($phone_list_);
            if (!is_array($phone_list_) || !count($phone_list_))
                return false;

            try {
                $audience->addUsers($phone_list_, [CustomAudienceTypes::PHONE]);
                $count += $count_tmp;
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
                throw new Exception($e->getMessage());
            }
        }

        return $flag;
    }
}

class OST_FB_API_Format {
    public static function PhoneNumber($string) {
        $tmp_phone = explode('/', $string);
        if (is_array($tmp_phone) && count($tmp_phone)) $tmp_phone = $tmp_phone[0];
        if (!$tmp_phone) return null;

        $tmp_phone = preg_replace('/[^0-9]/', '', $tmp_phone);

        if (strpos($tmp_phone, '0') === 0)
            $tmp_phone = substr($tmp_phone, 1, strlen($tmp_phone)-1);

        if (strpos($tmp_phone, '84') !== 0)
            $tmp_phone = '84'.$tmp_phone;

        return $tmp_phone;
    }
}

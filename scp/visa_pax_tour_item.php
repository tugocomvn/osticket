<?php
require('staff.inc.php');

include_once INCLUDE_DIR.'class.visa.php';
require_once  INCLUDE_DIR.'class.staff.php';
require_once INCLUDE_DIR.'class.visa-tour.php';
require_once INCLUDE_DIR.'class.address.php';
require_once  INCLUDE_DIR.'class.tour-destination.php';
require_once INCLUDE_DIR.'class.passport.php';

function listCheckToBinary($listCheck, $list_length)
{
    $strBin = str_repeat('0',$list_length);
    if(is_array($listCheck)) {
        foreach ($listCheck as $value) {
            $strBin = substr_replace($strBin, '1', $value, 1);
        }
    }
    return $strBin;
}
function loadCheckList($num_paper_require,$list_length)
{
    if(!is_numeric($num_paper_require))
    {
        $num_paper_require = (int)$num_paper_require;
    }

    $strBin = base_convert($num_paper_require,10,2); //convert dec to bin (string)
    $strBin = str_repeat('0',$list_length - strlen($strBin)).$strBin;

    return $strBin;
}

function setContentSMS($vars){
    $content = SMS_APPOINTMENT_START.trim($vars['pax']);
    $content .= ' lich hen '.trim($vars['appointment']);

    if(isset($vars['tour']) && trim($vars['tour']))
        $content.= '(tour: '.trim($vars['tour']).')';

    $content.= ' vao luc '.trim($vars['time_appointment']);
    $content.= ' ngay '.trim($vars['date_appointment']);

    if(isset($vars['note']) && $vars['note']){
        $content .= '('.trim($vars['note']).')';
    }
    return $content;
}

function sendSMS($vars, &$error,&$mess){
    if (!isset($vars['phone_number'])){
        $error = true;
        $mess[] = 'Khách hàng chưa có số điện thoại';
        return;
    }

    if (!\_String::isMobileNumber($vars['phone_number'])) {
        $error = true;
        $mess[] = 'Số điện thoại không đúng';
        return;
    }

    if(!isset($vars['appointment']) || !$vars['appointment']) {
        $error = true;
        $mess[] = 'Tin nhắn chưa được gửi, vì chưa chọn cuộc hẹn!';
        return;
    }

    if(!isset($vars['date_appointment']) || empty($vars['date_appointment'])
        || !isset($vars['time_appointment']) || empty($vars['time_appointment'])) {
        $error = true;
        $mess[] = 'Tin nhắn chưa được gửi, vì chưa chọn thời gian cuộc hẹn!';
        return;
    }

    if($error){
        $content = setContentSMS($vars);
        $vars['sms_content'] = $content;

        $send_sms = VisaTourGuest::_sendSMSVisa($vars, $error, (int)trim($_REQUEST['id']));

        if($send_sms)
            $mess['success'] = '&& Gửi SMS thành công';
        else {
            $error= true;
            $mess[] = 'Gửi không thành công, vui lòng thao tác lại';
        }
    }
}

function saveVisaImage($profile_id, &$error, &$mess ){
    global $thisstaff;
    $profile = \Tugo\VisaTourPaxProfile::lookup($profile_id);

    if(!$profile){
        $error = true;
        $mess[] = 'Không tìm thấy hồ sơ khách';
        return;
    }

    if((int)$_FILES['image']['error'] > 0) {
        $error = true;
        $mess[] = 'Có lỗi xảy ra khi upload ảnh này!';
        return;
    }

    if(isset($_FILES['image']) && $profile) {
        //$file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_type = $_FILES['image']['type'];
        $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

        $extensions = array("jpeg", "jpg", "png");

        if (in_array($file_ext, $extensions) === false) {
            $error = true;
            $mess[]  = "Định dạng không cho phép, vui lòng chọn  JPEG or PNG file.";
        }

        if ($file_size > 5*1024*1024) {
            $error = true;
            $mess[] = 'Ảnh phải có kích thước không quá 5 MB';
        }

        $file_name = md5(mt_rand(0,9999).$profile->id.time());
        $directory = __DIR__.'/../visa_upload/';

        if (! is_dir($directory) && ! mkdir($directory) && ! is_dir($directory)) {
            $error = true;
            $mess[] = 'Directory ' . $directory . ' was not created';
        }
        if($error)
            return;

        $destination = $directory.$file_name.'.png';
        $res = move_uploaded_file($file_tmp, $destination);

        if ($res) {
            $profile->set('visa_image_filename', $file_name);
            $profile->set('visa_image_uploaded_by', $thisstaff->getId());
            $profile->set('visa_image_uploaded_at', new SqlFunction("NOW"));
            $profile->save(true);

            $log = \Tugo\TourPaxVisaProfileHistory::create(
                [
                    'tour_pax_visa_profile_id' => $profile_id,
                    'snapshot' => json_encode($profile->ht),
                    'time' => date('Y-m-d H:i:s'),
                    'created_by' => $thisstaff->getId(),
                ]
            );

            $log_id = $log->save();
            $profile->set('log_id_profile_history', $log_id);
            $profile->save(true);

            //add log upload visa image
            $time_action_log = date('Y-m-d H:i:s');
            \Tugo\VisaFollowAction::addLogAction((int)$profile_id, $profile->ht,
                                        \Tugo\VisaFollowAction::VISA_UPLOAD_VISA_IMAGE, $time_action_log);

            $data_action = \Tugo\VisaFollowAction::countProfileVisa($time_action_log);
            //add data to tugo_osticket_visa
            while ($data_action && ($row = db_fetch_array($data_action))){
                try{
                    \Tugo\AnlysisVisa::insert($row);
                }catch (Exception $ex) {
                    echo $ex->getMessage();
                }
            }
        }else{
            $error = true;
            $mess[] = "Tải lên ảnh không thành công!";
        }
        
    }
}

function savePassportPhoto($passport, &$error, &$mess){
    global $thisstaff;
    if(!$passport || $passport === '')
    {
        $error = true;
        $mess[] = 'Không có số passport';
        return;
    }

    if((int)$_FILES['image_passport']['error'] > 0) {
        $error = true;
        $mess[] = 'Có lỗi xảy ra khi upload ảnh này!';
        return;
    }

    if(isset($_FILES['image_passport'])) {
        //$file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image_passport']['size'];
        $file_tmp = $_FILES['image_passport']['tmp_name'];
        $file_type = $_FILES['image_passport']['type'];
        $file_ext = strtolower(end(explode('.', $_FILES['image_passport']['name'])));

        $extensions = array("jpeg", "jpg", "png");

        if (in_array($file_ext, $extensions) === false) {
            $error = true;
            $mess[]  = "Định dạng không cho phép, vui lòng chọn  JPEG or PNG file.";
        }

        if ($file_size > 5*1024*1024) {
            $error = true;
            $mess[] = 'Ảnh phải có kích thước không quá 5 MB';
        }

        $file_name = md5(mt_rand(0,9999).time());
        $directory = __DIR__.'/../passport_upload/';

        if (! is_dir($directory) && ! mkdir($directory) && ! is_dir($directory)) {
            $error = true;
            $mess[] = 'Directory ' . $directory . ' was not created';
        }
        if($error)
            return;

        $destination = $directory.$file_name;
        $res = move_uploaded_file($file_tmp, $destination);

        if ($res) {
            $new = \Tugo\PassportPhotoUpload::lookup(['passport_no' => $passport,]);

            $data =  [
                'passport_no' => $passport,
                'filename'    => $file_name,
                'upload_at'   => new SqlFunction("NOW"),
                'upload_by'   => $thisstaff->getId(),
            ];

            if (!$new) {
                $new = \Tugo\PassportPhotoUpload::create($data);

            } else {
                $new->setAll($data);
            }

            if ($new)
                $new->save();
        }else{
            $error = true;
            $mess[] = "Tải lên ảnh không thành công!";
        }
    }
}

function loadVisaImage($profile_id){
    $profile = \Tugo\VisaTourPaxProfile::lookup($profile_id);

    if(!$profile){
        return '';
    }

    if($profile->visa_image_filename)
        return $profile->visa_image_filename.'.png';

    return '';
}

function convertFileToBas64($path){
    if(!file_exists($path))
        return null;

    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

define('LIST_LENGTH_PAPER',\Tugo\RequiredPapers::getTotalItem());
define('LIST_LENGTH_FINANCE',\Tugo\StatusFinance::getTotalItem());

if(isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['tour']) && !empty($_REQUEST['tour']))
{
    $id_pax = $_REQUEST['id'];
    $tour_id = $_REQUEST['tour'];
    $tour = TourNew::lookup($tour_id);

    //only access when have permission
    $group_market = $thisstaff->getGroup()->getDestinations();
    $group_des = TourDestination::getDestinationFromMarket($group_market);
    $booking_type_list = TourDestination::loadList($group_des);

    if(!in_array($tour->destination,$group_des) || empty($group_des))
        $tour_id = $_REQUEST['tour'] = 0;

    $paxInfo = VisaTourGuest::getPaxFromId($id_pax,$tour_id);
    $paxInfo = db_fetch_array($paxInfo);
}

if($paxInfo['tour_pax_visa_profile_id']) {
    $id = $paxInfo['tour_pax_visa_profile_id'];
}else $id = 0;

// ----------------Lay lich su visa tu  FORM-------------
if (!isset($_POST['group-loyalty']) || !$_POST['group-loyalty'] || !is_array($_POST['group-loyalty'])) {
    $_POST['group-loyalty'] = [];
}
else {
    $group_loyalty = $_POST['group-loyalty'];
}
//-----------------LAY thong ti bo sung visa tu FORM-------------
if (!isset($_POST['group-loyalty-info']) || !$_POST['group-loyalty-info'] || !is_array($_POST['group-loyalty-info'])) {
    $_POST['group-loyalty-info'] = [];
}
else {
    $group_loyalty_info = $_POST['group-loyalty-info'];
}
//----------------LAy cuoc hen voi khach tu FORM----------------
if (!isset($_POST['group-loyalty-appointment']) || !$_POST['group-loyalty-appointment'] || !is_array($_POST['group-loyalty-appointment'])) {
    $group_loyalty_appointment = [];
}
else {
    $group_loyalty_appointment = $_POST['group-loyalty-appointment'];
}
if(isset($_POST['date-deadline']) && $_POST['date-deadline']){
    $dateDeadline = date('Y-m-d',strtotime($_POST['date-deadline']));
}else
    $dateDeadline = null;
if(isset($_POST['visa-code']) && $_POST['visa-code']){
    $visaCode = strtoupper(trim($_POST['visa-code']));
}else
    $visaCode = null;

$listCheckRequiredPapers = [];
$listCheckStatusFinance = [];
$checkJobStatus = null;
$checkMarital = null;
$checkVisaStatus = null;
$checkPassportLocation = null;
$checkVisaType = null;
$checkStaff = null;
$mess = [];
$error = false;

if(isset($_REQUEST['paper_require'])){
    $listCheckRequiredPapers = $_REQUEST['paper_require'];
}
if(isset($_REQUEST['status_finance'])){
    $listCheckStatusFinance = $_REQUEST['status_finance'];
}
if(isset($_REQUEST['status_job'])){
    $checkJobStatus = $_REQUEST['status_job'];
}
if(isset($_REQUEST['status_marital'])){
    $checkMarital = $_REQUEST['status_marital'];
}
if(isset($_REQUEST['status_visa'])){
    $checkVisaStatus = $_REQUEST['status_visa'];
}
if(isset($_REQUEST['location_passport'])){
    $checkPassportLocation = $_REQUEST['location_passport'];
}
if(isset($_REQUEST['visa_type'])){
    $checkVisaType = $_REQUEST['visa_type'];
}
if(isset($_REQUEST['staff_id']) && $_REQUEST['staff_id']){
    $checkStaff = $_REQUEST['staff_id'];
}else{
    $checkStaff = $thisstaff->getId();
}
//address
if(isset($_REQUEST['street']) && $_REQUEST['street'])
    $street = trim($_REQUEST['street']);
else
    $street = null;
if(isset($_REQUEST['city']) && $_REQUEST['city'])
    $city = (int)$_REQUEST['city'];
else
    $city = 0;
if(isset($_REQUEST['district']) && $_REQUEST['district'])
    $district = (int)$_REQUEST['district'];
else
    $district = 0;
if(isset($_REQUEST['ward']) && $_REQUEST['ward'])
    $ward = (int)$_REQUEST['ward'];
else
    $ward = 0;

$listRequiredPapers = \Tugo\RequiredPapers::getAllItem();
$listStatusFinance = \Tugo\StatusFinance::getAllItem();
$listJobStatus = \Tugo\JobStatus::getItemExist();
$listMarital = \Tugo\StatusMarital::getItemExist();
$listVisaStatus = \Tugo\StatusVisa::getItemExist();
$listPassportLocation = \Tugo\PassportLocation::getItemExist();
$listVisaType = \Tugo\VisaType::getItemExist();
$listVisaAppointment = \Tugo\VisaAppointment::getItemExist();
$listStaff = Staff::getAllUsername();
$listCityAddress = CityAddress::getAll();

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit']) && ('edit' === $_POST['edit'])) {
    if(!$thisstaff->canMangeVisaPaxDocuments()){
        $error = true;
        $mess[] = 'Không thể thực hiện hành động này';
    }

    if((!($id_pax) || !($tour_id)) && !$error){
        $error = true;
        $mess[] = 'Không tồn tại khách hàng này';
    }

    if($id_pax && $tour_id && !$error)
    {
        $time_action_log = date('Y-m-d H:i:s');
        $news = \Tugo\VisaTourPaxProfile::lookup($id);
        $action_update_visa_status = 0;
        $action_visa_assign = 0;
        $action_visa_history = 0;
        $action_visa_information = 0;
        if(!$news)
        {
            //CREATE
            $data = [
                'paper_require' => bindec(listCheckToBinary($listCheckRequiredPapers,LIST_LENGTH_PAPER)),
                'finance_status' => bindec(listCheckToBinary($listCheckStatusFinance,LIST_LENGTH_FINANCE)),
                'job_status_id' => $checkJobStatus,
                'marital_status_id' => $checkMarital,
                'visa_status_id' => $checkVisaStatus,
                'passport_location_id' => $checkPassportLocation,
                'visa_type_id' => $checkVisaType,
                'staff_id'=> $checkStaff,
                'deadline'=> $dateDeadline,
                'visa_code'=> $visaCode,
                'street_address'=> $street,
                'city_id'=>$city,
                'district_id'=> $district,
                'ward_id'=> $ward,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $thisstaff->getId(),
            ];
            $news = \Tugo\VisaTourPaxProfile::create($data);
            $news->save(true);
            $action_update_visa_status = (int)$checkVisaStatus;
            $action_visa_assign = \Tugo\VisaFollowAction::VISA_ASSIGNED_PROFILE;
            $id = $news->ht['id'];
            VisaTourGuest::insert_visa_profile($id_pax,$tour_id,$id);
            \Tugo\VisaFollowAction::addLogAction($id, $news->ht,
                \Tugo\VisaFollowAction::VISA_CREATED_PROFILE, $time_action_log);
        }else{
            //EDIT
            if($news->paper_require != bindec(listCheckToBinary($listCheckRequiredPapers,LIST_LENGTH_PAPER))) {
                $data['paper_require'] = bindec(listCheckToBinary($listCheckRequiredPapers,LIST_LENGTH_PAPER));
            }
            if($news->finance_status != bindec(listCheckToBinary($listCheckStatusFinance,LIST_LENGTH_FINANCE))) {
                $data['finance_status'] = bindec(listCheckToBinary($listCheckStatusFinance,LIST_LENGTH_FINANCE));
            }
            if($checkJobStatus != $news->job_status_id) {
                $data['job_status_id'] = $checkJobStatus;
            }
            if($checkMarital != $news->marital_status_id) {
                $data['marital_status_id'] = $checkMarital;
            }
            if($checkVisaStatus != $news->visa_status_id) {
                $action_update_visa_status = (int)$checkVisaStatus;
                $data['visa_status_id'] = $checkVisaStatus;
            }
            if($checkPassportLocation != $news->passport_location_id) {
                $data['passport_location_id'] = $checkPassportLocation;
            }
            if($checkVisaType != $news->visa_type_id) {
                $data['visa_type_id'] = $checkVisaType;
            }
            if($checkStaff != $news->staff_id) {
                $action_visa_assign = \Tugo\VisaFollowAction::VISA_ASSIGNED_PROFILE;
                $data['staff_id'] = $checkStaff;
            }
            if($dateDeadline != $news->deadline){
                $data['deadline'] = $dateDeadline;
            }
            if($visaCode != $news->visa_code){
                $data['visa_code'] = $visaCode;
            }
            if($street != $news->street_address){
                $data['street_address'] = $street;
            }
            if($city != $news->city_id){
                $data['city_id'] = $city;
            }
            if($district != $news->district_id){
                $data['district_id'] = $district;
            }
            if($ward != $news->ward_id){
                $data['ward_id'] = $ward;
            }

            $data['updated_by'] = $thisstaff->getId();
            $data['updated_at'] = date('Y-m-d H:i:s');
            $news->setAll($data);
            $news->save(true);

            if(isset($data) && count($data) > 2)
                \Tugo\VisaFollowAction::addLogAction($id, $news->ht, \Tugo\VisaFollowAction::VISA_EDITED_PROFILE, $time_action_log);
        }

        if(isset($action_update_visa_status) && $action_update_visa_status !== 0) {
            switch ((int)$checkVisaStatus) {
                case \Tugo\VisaStatusList::VISA_APPROVED_NOT_GO:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_APPROVED_NOT_GO;
                    break;
                case \Tugo\VisaStatusList::VISA_APPROVED:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_APPROVED;
                    break;
                case \Tugo\VisaStatusList::WAITING_DOCUMENT:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_WAITING_DOCUMENT;
                    break;
                case \Tugo\VisaStatusList::WAITING_APPLY:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_WAITING_APPLY;
                    break;
                case \Tugo\VisaStatusList::EMBASSY_PROCESSING:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_EMBASSY_PROCESSING;
                    break;
                case \Tugo\VisaStatusList::APPLIED_TO_EMBASSY:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_APPLIED_TO_EMBASSY;
                    break;
                case \Tugo\VisaStatusList::VISA_REJECTED:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_REJECTED;
                    break;
                case \Tugo\VisaStatusList::DOCUMENT_PROCESSING:
                    $action_update_visa_status = \Tugo\VisaFollowAction::VISA_STATUS_DOCUMENT_PROCESSING;
                    break;
                default:
                    $action_update_visa_status = 0;
                    break;

            }
            \Tugo\VisaFollowAction::addLogAction($id, $news->ht, $action_update_visa_status, $time_action_log);
        }

        //add log action assign staff to visa_profile
        if(isset($action_visa_assign) && $action_visa_assign !== 0) {
            $staff = new Staff((int)$checkStaff);
            if(!$staff)
                 $staff = null;

            //add log action assign staff
            \Tugo\VisaFollowAction::addLogAction($id, $news->ht, $action_visa_assign, $time_action_log);
            // add log action receive visa staff
            \Tugo\VisaFollowAction::addLogAction($id, $news->ht, \Tugo\VisaFollowAction::VISA_RECEIVED_PROFILE,
                                                $time_action_log, $staff);
        }

        $log = \Tugo\TourPaxVisaProfileHistory::create(
            [
                'tour_pax_visa_profile_id' => $id,
                'snapshot' => json_encode($news->ht),
                'time' => date('Y-m-d H:i:s'),
                'created_by' => $thisstaff->getId(),
            ]
        );
        $log_id = $log->save();
        $news->set('log_id_profile_history', $log_id);
        $news->save(true);
        //VISA HISTORY
        $history_pax_list = \Tugo\VisaHistory::getHistoryPax($id);
        $history_id_list_current = []; //Mang chua cac id trong DB
        $history_id_list_new = []; // Mang chua id post từ FORM

        while ($history_pax_list && ($row = db_fetch_array($history_pax_list))) {
            $history_id_list_current[] = $row['id'];
        }

        foreach ($group_loyalty as $value)
        {
            $value['text-input'] = trim($value['text-input']);
            $history_id_list_new[] = $value['history_id'];
            //Kiem tra dòng trống trên rong se bo qua
            if(empty($value['history_id']) && empty($value['date-input']) && empty($value['text-input']))
            {
                continue;
            }
            if(empty($value['date-input']) || empty($value['text-input']))
            {
                $error = true;
                $mess[] = 'Nội dung hoặc ngày sự kiện của lịch sử visa không để trống!';
                break;
            }
            $date_event = date_create_from_format('d/m/Y', $value['date-input']);
            if($date_event)
            {
                $date_event = $date_event->format('Y-m-d');
            }else{
                $error = true;
                $mess[] = 'Ngày sự kiện của lịch sử visa không đúng định dạng';
                break;
            }
            $id__ = null;
            if(!empty($value['history_id']))
            {
                //EDIT
                $news = \Tugo\VisaHistory::lookup($value['history_id']);
                if($news)
                {
                    if(($value['text-input'] != $news->visa_content) || ($date_event != $news->date_event )) {
                        $data_edit = [
                            'visa_content' => $value['text-input'],
                            'staff_id_edit' => $thisstaff->getId(),
                            'date_event' => $date_event
                        ];
                        $action_visa_history = \Tugo\VisaFollowAction::VISA_EDITED_VISA_HISTORY;
                        $news->setAll($data_edit);
                        $news->save(true);
                        $id__ = (int)$value['history_id'];
                    }
                }
            }
            else{
                //CREATE
                $data_create = [
                    'tour_pax_visa_profile_id' => $id,
                    'visa_content' => $value['text-input'],
                    'staff_id_create' => $thisstaff->getId(),
                    'date_event' => $date_event,
                ];
                $action_visa_history = \Tugo\VisaFollowAction::VISA_CREATED_VISA_HISTORY;
                $news = \Tugo\VisaHistory::create($data_create);
                $id__ = $news->save(true);
            }



            if(isset($id__) && $id__ !== null && $id__) {
                //add log action create || edit visa history
                if(isset($action_visa_history) && $action_visa_history !== 0)
                    \Tugo\VisaFollowAction::addLogAction($id__,$news->ht, $action_visa_history, $time_action_log);

                $log = \Tugo\VisaHistoryAction::create(
                    [
                        'visa_history_action_log_id' => $id__,
                        'snapshot'                   => json_encode($news->ht),
                        'time'                       => date('Y-m-d H:i:s'),
                        'created_by'                 => $thisstaff->getId(),
                    ]
                );
                $log_id = $log->save();
                $news->set('log_id_visa_history', $log_id);
                $news->save(true);
            }
        }
        //Tim va xoa phan tu trong DB

        $history_id_delete = array_diff($history_id_list_current,$history_id_list_new); //Tra ve cac ID bi xoa o form
        foreach ($history_id_delete as $value){
            //add log before delete visa history
            $visa_history_delete = \Tugo\VisaHistory::lookup((int)$value);
            if($visa_history_delete) {
                //add log action delete visa_history
                \Tugo\VisaFollowAction::addLogAction((int)$value, $visa_history_delete->ht,
                    \Tugo\VisaFollowAction::VISA_DELETED_VISA_HISTORY, $time_action_log);

                $log = \Tugo\VisaHistoryAction::create(
                    [
                        'visa_history_action_log_id' => (int)$value,
                        'snapshot'                   => json_encode($visa_history_delete->ht),
                        'time'                       => date('Y-m-d H:i:s'),
                        'created_by'                 => $thisstaff->getId(),
                    ]
                );
                $log->save();
            }

            //delete visa history
            \Tugo\VisaHistory::deleteItem($value);
        }
        //---------------------------INFORMATION ADD---------------------
        $info_pax_list = \Tugo\VisaInformation::getInfoPax($id);
        $info_id_list_new = []; //Mang chua id tu FORM
        $info_id_list_current = []; //Mang chua cac id trong DB
        while ($info_pax_list && ($row = db_fetch_array($info_pax_list)))
        {
            $info_id_list_current[] = $row['id'];
        }

        $id__ = null;
        foreach ($group_loyalty_info as $value)
        {
            $value['text-input-info'] = trim($value['text-input-info']);
            $info_id_list_new[] = $value['info_id'];
            if(empty($value['info_id']) && empty($value['text-input-info']))
            {
                continue;
            }
            if(!empty($value['info_id']))
            {
                //EDIT
                $news = \Tugo\VisaInformation::lookup($value['info_id']);
                if($news && empty($value['text-input-info']))
                {
                    $error = true;
                    $mess[] = 'Thông tin bổ sung visa trống';
                    break;
                }
                if($news && !empty($value['text-input-info']) && ($news->visa_content != $value['text-input-info']))
                {
                    $data_edit = [
                        'visa_content' => $value['text-input-info'],
                        'staff_id_edit' => $thisstaff->getId(),
                    ];
                    $action_visa_information = \Tugo\VisaFollowAction::VISA_EDITED_VISA_INFORMATION;
                    $news->setAll($data_edit);
                    $news->save(true);
                    $id__ = (int)$value['info_id'];
                }
            }
            else{
                //CREATE
                if(!empty($value['text-input-info'])) {
                    $data_create = [
                        'tour_pax_visa_profile_id' => $id,
                        'visa_content' => $value['text-input-info'],
                        'staff_id_create' => $thisstaff->getId(),
                    ];
                    $action_visa_information = \Tugo\VisaFollowAction::VISA_CREATED_VISA_INFORMATION;
                    $news = \Tugo\VisaInformation::create($data_create);
                    $id__ = $news->save(true);
                }
            }
            if(isset($id__) && $id__ !== null && $id__){
                //add log action create|| edit visa_information
                \Tugo\VisaFollowAction::addLogAction($id__, $news->ht, $action_visa_information, $time_action_log);

                $log = \Tugo\VisaInformationAction::create(
                    [
                        'visa_information_action_log_id' => $id__,
                        'snapshot' => json_encode($news->ht),
                        'time' => date('Y-m-d H:i:s'),
                        'created_by' => $thisstaff->getId(),
                    ]
                );
                $log_id = $log->save();
                $news->set('log_id_visa_information', $log_id);
                $news->save(true);
            }
        }
        $info_id_delete = array_diff($info_id_list_current,$info_id_list_new); //Tra ve cac ID bi mat
        foreach ($info_id_delete as $value){
            //add log id visa_information before delete id
            $visa_information_delete = \Tugo\VisaInformation::lookup((int)$value);
            if($visa_information_delete){
                //add log action delete visa information
                \Tugo\VisaFollowAction::addLogAction((int)$value, $visa_information_delete->ht,
                                        \Tugo\VisaFollowAction::VISA_DELETED_VISA_INFORMATION, $time_action_log);

                $log = \Tugo\VisaInformationAction::create(
                    [
                        'visa_information_action_log_id' => (int)$value,
                        'snapshot' => json_encode($visa_information_delete->ht),
                        'time' => date('Y-m-d H:i:s'),
                        'created_by' => $thisstaff->getId(),
                    ]
                );
                $log->save();
            }

            // delete id visa_information
            \Tugo\VisaInformation::deleteItem($value);
        }


        if(isset($_FILES['image']) && $_FILES['image'] && $_FILES['image']['name'] !== '')
            saveVisaImage($id,$error, $mess);

        if(isset($_FILES['image_passport']) && $_FILES['image_passport'] && $_FILES['image_passport']['name'] !== '')
            savePassportPhoto($_POST['passport_no'], $error, $mess);

        $data_action = \Tugo\VisaFollowAction::countProfileVisa($time_action_log);
        //add data to tugo_osticket_visa
        while ($data_action && ($row = db_fetch_array($data_action))){
            try{
                \Tugo\AnlysisVisa::insert($row);
            }catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
        //----------- KIEM TRA THONG BAO VA CHUYEN TRANG  --------------------
        if(!$error) {
            $mess_success = 'Cập nhật thành công '.$mess['success'];
            FlashMsg::set('ticket_create', $mess_success);
            $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/visa_pax_tour_item.php') !== false
                ? $cfg->getBaseUrl() . '/scp/visa_pax_tour_item.php?id='.$id_pax.'&tour=' .$tour_id : $cfg->getBaseUrl() . '/scp/visa_pax_tour_item.php';
            header('Location: '. $referer);
            exit();
        }
    }
}

$info_pax_list = Tugo\VisaInformation::getInfoPax($id);
$history_pax_list = \Tugo\VisaHistory::getHistoryPax($id);
$visaProfile = \Tugo\VisaTourPaxProfile::lookup($id);

if($visaProfile->paper_require) {
    $listCheckRequiredPapers = loadCheckList($visaProfile->paper_require,LIST_LENGTH_PAPER);
}
if($visaProfile->finance_status) {
    $listCheckStatusFinance = loadCheckList($visaProfile->finance_status,LIST_LENGTH_FINANCE);
}
$visa_image_filename = loadVisaImage($id);

$dataPassportPhoto = null;
$passport_photo = \Tugo\PassportPhotoUpload::lookup(['passport_no' => trim($paxInfo['passport_no'])]);
$pax_passport_photo = \Tugo\PassportPhoto::getFileNamePassport($paxInfo['booking_code'],trim($paxInfo['passport_no']),  1);
$pax_passport_photo = db_fetch_array($pax_passport_photo);
if($passport_photo) //load file name in table passport_photo_upload
    $path = '../passport_upload/' . $passport_photo->filename;
elseif($pax_passport_photo && $pax_passport_photo['filename']){
    //load file name in table pax_passport_photo
    $path = '../passport_upload/' . $pax_passport_photo['filename'];
}
if(isset($path) && $path)
    $dataPassportPhoto = convertFileToBas64($path);

$nav->setTabActive('visa');
$nav->setActiveSubMenu(3,'visa');
$ost->addExtraHeader('<meta name="tip-namespace" content="visa.visa" />',
                     "$('#content').data('tipNamespace', 'visa.visa_tour_list');");

$page = "visa_pax_tour_item.inc.php";
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');

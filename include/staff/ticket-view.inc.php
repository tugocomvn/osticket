<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');

//Make sure the staff is allowed to access the page.
if(!@$thisstaff->isStaff() || !$ticket->checkStaffAccess($thisstaff)) die('Access Denied');
require_once(INCLUDE_DIR.'class.tag.php');
require_once(INCLUDE_DIR.'class.booking.php');
require_once(INCLUDE_DIR.'class.ticket.php');
//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info=($_POST && $errors)?Format::input($_POST):array();

//Auto-lock the ticket if locking is enabled.. If already locked by the user then it simply renews.
if($cfg->getLockTime() && !$ticket->acquireLock($thisstaff->getId(),$cfg->getLockTime()))
    $warn.=__('Unable to obtain a lock on the ticket');

//Get the goodies.
$dept  = $ticket->getDept();  //Dept
$staff = $ticket->getStaff(); //Assigned or closed by..
$user  = $ticket->getOwner(); //Ticket User (EndUser)
$team  = $ticket->getTeam();  //Assigned team.
$sla   = $ticket->getSLA();
$lock  = $ticket->getLock();  //Ticket lock obj
$_id    = $ticket->getId();    //Ticket ID.
$ticket_number = $ticket->getNumber();
$unassign = $ticket->getUnassign();
$booking_obj = Booking::select_ticket_from_booking_view($ticket_number);

$calendar = $ticket->getCalendarEntries();
$check_user = $user
    && $user->getEmail()
    && !in_array($user->getEmail(), [
        'it@tugo.com.vn', 'beckerbao29@gmail.com', 'beckerbao@gmail.com', 'phamquocbuu@gmail.com',
        'support@tugo.vn', 'support@tugo.com.vn'
    ]);

//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = sprintf(
            __('Current ticket status (%s) does not allow the end guest to reply.'),
            $ticket->getStatus());
elseif ($ticket->isAssigned()
        && (($staff && $staff->getId()!=$thisstaff->getId())
            || ($team && !$team->hasMember($thisstaff))
        ))
    $warn.= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
            sprintf(__('Ticket is assigned to %s'),
                implode('/', $ticket->getAssignees())
                ));

if (!$errors['err']) {

    if ($lock && $lock->getStaffId()!=$thisstaff->getId())
        $errors['err'] = sprintf(__('This ticket is currently locked by %s'),
                $lock->getStaffName());
    elseif (($emailBanned=TicketFilter::isBanned($ticket->getEmail())))
        $errors['err'] = __('Email is in banlist! Must be removed before any reply/response');
    elseif (!Validator::is_valid_email($ticket->getEmail()))
        $errors['err'] = __('EndGuest email address is not valid! Consider updating it before responding');
}

$unbannable=($emailBanned) ? BanList::includes($ticket->getEmail()) : false;

if($ticket->isOverdue())
    $warn.='&nbsp;&nbsp;<span class="Icon overdueTicket">'.__('Marked overdue!').'</span>';

?>
<?php
$ReplyToEmail = $ticket->getReplyToEmail();
$ticketname   = $ticket->getName();
$ticketphone  = $ticket->getPhoneNumber();
$vars = [
    'email' => $ReplyToEmail,
    'phone' => $ticketphone,
    'name'  =>  $ticketname,
];
//call funtion postReply class.ticket.php
//$sendcontract = new Ticket();
//$sendcontract->postReply($vars, $alert=true, $claim=true);
//var_dump($vars);
?>
<?php
// get ticket form
if (!isset($forms) || !$forms) $forms=DynamicFormEntry::forTicket($ticket->getId());

// set ticket as read
if ($forms && $ticket->getStaffId() && $ticket->getStaffId() == $thisstaff->getId()) {
    $break = false;
    $ticket->markAsRead();
    $ticket->reload();
}

?>
<table width="1058" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td width="20%" class="">
             <h2><a href="tickets.php?id=<?php echo $ticket->getId(); ?>"
             title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
             <?php echo sprintf(__('Ticket #%s'), $ticket_number); ?></a></h2>
        </td>
        <td width="auto" class="flush-right">
            <?php
            if ( ($thisstaff->canBanEmails()
                    || $thisstaff->canEditTickets()
                    || ($dept && $dept->isManager($thisstaff))) && ($unassign != TicketUnassign::WELCOME) ) { ?>
            <span class="btn_sm btn-default action-button pull-right" data-dropdown="#action-dropdown-more">
                <i class="icon-caret-down pull-right"></i>
                <span ><i class="icon-cog"></i> <?php echo __('More');?></span>
            </span>
            <?php
            }
            // Status change options
            if ($unassign != TicketUnassign::WELCOME)
                echo TicketStatus::status_options();

            if ($thisstaff->canEditTickets()
                && !in_array($ticket->getTopicId(), array_merge(unserialize(IO_TOPIC),[OL_FORM,BOOKING_FORM]))
                && $unassign != TicketUnassign::WELCOME
            ) { ?>
                <a class="btn_sm btn-default action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php
            } elseif ($thisstaff->canEditBookings() && in_array($ticket->getTopicId(), [BOOKING_TOPIC])) { ?>
                <a class="btn_sm btn-default action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-booking"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php
            } elseif ($thisstaff->canEditOfflates() && in_array($ticket->getTopicId(), [OL_FORM])) { ?>
                <a class="btn_sm btn-default action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-ol"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
                <?php
            } elseif ($thisstaff->canEditPayments() && in_array($ticket->getTopicId(), unserialize(IO_TOPIC))) { ?>
                <a class="btn_sm btn-default action-button pull-right" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit-io"><i class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
            <?php
            }
            if ($ticket->isOpen()
                    && !$ticket->isAssigned()
                    && $thisstaff->canAssignTickets()
                    && $ticket->getDept()->isMember($thisstaff)
                    && $unassign != TicketUnassign::WELCOME
            ) {?>
                <a id="ticket-claim" class="btn_sm btn-default action-button pull-right confirm-action" href="#claim"><i class="icon-user"></i> <?php
                    echo __('Claim'); ?></a>

            <?php
            }?>
            <span class="btn_sm btn-default  action-button pull-right" data-dropdown="#action-dropdown-print">
                <i class="icon-caret-down pull-right"></i>
                <a id="ticket-print" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print"><i class="icon-print"></i> <?php
                    echo __('Print'); ?></a>
            </span>
            <div id="action-dropdown-print" class="action-dropdown anchor-right">
              <ul>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=0"><i
                 class="icon-file-alt"></i> <?php echo __('Ticket Thread'); ?></a>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=1"><i
                 class="icon-file-text-alt"></i> <?php echo __('Thread + Internal Notes'); ?></a>
              </ul>
            </div>
            <?php if($unassign != TicketUnassign::WELCOME): ?>
                <div id="action-dropdown-more" class="action-dropdown anchor-right">
                    <ul>
                        <li><a class="change-user" href="#tickets/<?php
                        echo $ticket->getId(); ?>/contract"><i class="icon-file-alt"></i> <?php
                        echo __('Send contract to customer...'); ?></a></li>
                        <?php
                        if($thisstaff->canEditTickets()) { ?>
                            <li><a class="change-user" href="#tickets/<?php
                                echo $ticket->getId(); ?>/change-user"><i class="icon-user"></i> <?php
                                    echo __('Change Owner'); ?></a></li>
                            <?php
                        }
                        if($thisstaff->canDeleteTickets()) {
                            ?>
                            <li><a class="ticket-action" href="#tickets/<?php
                                echo $ticket->getId(); ?>/status/delete"
                                   data-href="tickets.php"><i class="icon-trash"></i> <?php
                                    echo __('Delete Ticket'); ?></a></li>
                            <?php
                        }
                        if($ticket->isOpen() && ($dept && $dept->isManager($thisstaff))) 

                            if($ticket->isAssigned()) { ?>
                                <li><a  class="confirm-action" id="ticket-release" href="#release"><i class="icon-user"></i> <?php
                                        echo __('Release (unassign) Ticket'); ?></a></li>
                                <?php
                            }
                            if($ticket->isOpen() && ($dept && $dept->isManager($thisstaff))){
                            if(!$ticket->isOverdue()) { ?>
                                <li><a class="confirm-action" id="ticket-overdue" href="#overdue"><i class="icon-bell"></i> <?php
                                        echo __('Mark as Overdue'); ?></a></li>
                                <?php
                            }

                            if($ticket->isAnswered()) { ?>
                                <li><a class="confirm-action" id="ticket-unanswered" href="#unanswered"><i class="icon-circle-arrow-left"></i> <?php
                                        echo __('Mark as Unanswered'); ?></a></li>
                                <?php
                            } else { ?>
                                <li><a class="confirm-action" id="ticket-answered" href="#answered"><i class="icon-circle-arrow-right"></i> <?php
                                        echo __('Mark as Answered'); ?></a></li>
                                <?php
                            }
                        } ?>
                        <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                            ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                            ><i class="icon-paste"></i> <?php echo __('Manage Forms'); ?></a></li>
                        <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                            ?>/merge" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                            ><i class="icon-copy"></i> <?php echo __('Merge Ticket'); ?></a></li>

                        <?php           if($thisstaff->canBanEmails()) {
                            if(!$emailBanned) {?>
                                <li><a class="confirm-action" id="ticket-banemail"
                                       href="#banemail"><i class="icon-ban-circle"></i> <?php echo sprintf(
                                            Format::htmlchars(__('Ban Email <%s>')),
                                            $ticket->getEmail()); ?></a></li>
                                <?php
                            } elseif($unbannable) { ?>
                                <li><a  class="confirm-action" id="ticket-banemail"
                                        href="#unbanemail"><i class="icon-undo"></i> <?php echo sprintf(
                                            Format::htmlchars(__('Unban Email <%s>')),
                                            $ticket->getEmail()); ?></a></li>
                                <?php
                            }
                        }?>
                    </ul>
                </div>
            <?php endif; ?>
        </td>
    </tr>
</table>

<!--Ticket tags-->
<table width="1058" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td class="has_bottom_border">
            <?php
            $tags_res = null;
            $tags = [];
            if ($ticket && $ticket->getId()) {
                $tags_res = Tag::get($ticket->getId());
                $tag_items = '';
                if ($tags_res) {
                    while (($_tag = db_fetch_array($tags_res))) {
                        $tags[] = $_tag['content'];
                        $tag_items .= '<a target="_blank"
                        href="'.trim($cfg->getUrl(), '/').'/scp/tickets.php?a=search&query='
                            .urlencode('[').$_tag['content'].urlencode(']')
                            .'&basic_search=Search" class="mytag_class no-pjax">'.$_tag['content'].'</a>';
                    }
                }
            }
            ?>
            <span class="tag_items"><?php echo $tag_items ?></span>
            <input class="ticket_tag" type="text" value="<?php echo implode(',', $tags) ?>" data-role="tagsinput" style="display: none;" />
            <a href="#" class="manage_tags">Manage Tags</a>
            <button style="display:none" type="button" class="done_manage_tags btn_sm btn-primary">Done</button>
        </td>
    </tr>
</table>

<!-- Booking List -->
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <table cellspacing="0" cellpadding="4" width="100%" border="0">
        <tr>
            <td>
                <h2>Bookings</h2>
            </td>
        </tr>
        <?php $booking_flags = false; ?>
        <?php while ($booking_obj && ($row = db_fetch_array($booking_obj))): ?>
            <tr>
                <th>Mã Booking</th>
                <td><?php echo $row['booking_code'] ?></td>

                <th>Tên tour</th>
                <td><?php echo _String::json_decode($row['booking_type']) ?></td>

                <th>Trạng thái</th>
                <td><?php echo _String::json_decode($row['status']) ?></td>
            </tr>
            <?php $booking_flags = true; endwhile; ?>
        <?php if(!$booking_flags): ?>
            <tr><td>No booking.</td></tr>
        <?php endif; ?>
    </table>
</table>
<hr>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Status');?>:</th>
                    <td><?php echo $ticket->getStatus(); ?></td>
                </tr>
                <tr>
                    <?php
                    $priority = Priority::lookup($ticket->getPriorityId());
                    $priority_color = $priority->getColor();
                    ?>
                    <th><?php echo __('Priority');?>:</th>
                    <td <?php if('normal' !== $priority->getTag()): ?>style="background-color: <?php echo $priority_color ?>"<?php endif; ?>><?php echo $ticket->getPriority(); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Department');?>:</th>
                    <td><?php echo Format::htmlchars($ticket->getDeptName()); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCreateDate()); ?></td>
                </tr>
            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Guest'); ?>:</th>
                    <td><a href="#tickets/<?php echo $ticket->getId(); ?>/user"
                        onclick="javascript:
                            $.userLookup('ajax.php/tickets/<?php echo $ticket->getId(); ?>/user',
                                    function (user) {
                                        $('#user-'+user.id+'-name').text(user.name);
                                        $('#user-'+user.id+'-email').text(user.email);
                                        $('#user-'+user.id+'-phone').text(user.phone);
                                        $('select#emailreply option[value=1]').text(user.name+' <'+user.email+'>');
                                    });
                            return false;
                            "><i class="icon-user"></i> <span id="user-<?php echo $ticket->getOwnerId(); ?>-name"
                            ><?php echo Format::htmlchars($ticket->getName());
                        ?></span></a>
                        <?php
                        if($user) {
                            echo sprintf('&nbsp;&nbsp;<a href="tickets.php?a=search&uid=%d" title="%s" data-dropdown="#action-dropdown-stats">(<b>%d</b>)</a>',
                                    urlencode($user->getId()), __('Related Tickets'), $user->getNumTickets());
                        ?>
                            <div id="action-dropdown-stats" class="action-dropdown anchor-right">
                                <ul>
                                    <?php
                                    if(($open=$user->getNumOpenTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=open&uid=%s"><i class="icon-folder-open-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open));

                                    if(($closed=$user->getNumClosedTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=closed&uid=%d"><i
                                                class="icon-folder-close-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed));
                                    ?>
                                    <li><a href="tickets.php?a=search&all_ticket=1&uid=<?php echo $ticket->getOwnerId(); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
                                    <li><a href="users.php?id=<?php echo
                                    $user->getId(); ?>"><i class="icon-user
                                    icon-fixed-width"></i> <?php echo __('Manage Guest'); ?></a></li>
<?php if ($user->getOrgId()) { ?>
                                    <li><a href="orgs.php?id=<?php echo $user->getOrgId(); ?>"><i
                                        class="icon-building icon-fixed-width"></i> <?php
                                        echo __('Manage Organization'); ?></a></li>
<?php } ?>
                                </ul>
                            </div>
                    <?php
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Email'); ?>:</th>
                    <td>
                        <span id="user-<?php echo $ticket->getOwnerId(); ?>-email"><?php echo $ticket->getEmail(); ?></span>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Phone'); ?>:</th>
                    <td>
                        <span id="user-<?php echo $ticket->getOwnerId(); ?>-phone"><?php echo ($phone_number = $ticket->getPhoneNumber()); ?></span>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Source'); ?>:</th>
                    <td><?php
                        echo Format::htmlchars($ticket->getSource());

                        if($ticket->getIP())
                            echo '&nbsp;&nbsp; <span class="faded">('.$ticket->getIP().')</span>';
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <tr>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <?php
                if($ticket->isOpen()) { ?>
                <tr>
                    <th width="100"><?php echo __('Assigned To');?>:</th>
                    <td>
                        <?php
                        if($ticket->isAssigned()) {
                            echo Format::htmlchars(implode('/', $ticket->getAssignees()));
                            if ($staff && $staff->getMobilePhone())
                                echo ' ('.$staff->getMobilePhone().')';
                        }
                        else
                            echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                        ?>
                    </td>
                </tr>
                <?php
                } else { ?>
                <tr>
                    <th width="100"><?php echo __('Closed By');?>:</th>
                    <td>
                        <?php
                        if(($staff = $ticket->getStaff()))
                            echo Format::htmlchars($staff->getName());
                        else
                            echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                        ?>
                    </td>
                </tr>
                <?php
                } ?>
                <tr>
                    <th><?php echo __('SLA Plan');?>:</th>
                    <td><?php echo $sla?Format::htmlchars($sla->getName()):'<span class="faded">&mdash; '.__('None').' &mdash;</span>'; ?></td>
                </tr>
                <?php
                if($ticket->isOpen()){ ?>
                <tr>
                    <th><?php echo __('Due Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getEstDueDate()); ?></td>
                </tr>
                <?php
                }else { ?>
                <tr>
                    <th><?php echo __('Close Date');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCloseDate()); ?></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </td>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <tr>
                    <th width="100"><?php echo __('Help Topic');?>:</th>
                    <td><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Message');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastMsgDate()); ?></td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Response');?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastRespDate()); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
<?php
$idx = 0;
foreach (DynamicFormEntry::forTicket($ticket->getId()) as $form) {
    // Skip core fields shown earlier in the ticket view
    // TODO: Rewrite getAnswers() so that one could write
    //       ->getAnswers()->filter(not(array('field__name__in'=>
    //           array('email', ...))));
    $answers = array_filter($form->getAnswers(), function ($a) {
        return !in_array($a->getField()->get('name'),
                array('email','subject','name','priority'));
        });
    if (count($answers) == 0)
        continue;
    ?>
        <tr>
        <td colspan="1">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
            <?php foreach($answers as $a) {
                if (!($v = $a->display())) continue; ?>
                <tr>
                    <th width="100"><?php
    echo $a->getField()->get('label');
                    ?>:</th>
                    <td><?php
    echo $v;
                    ?></td>
                </tr>
                <?php } ?>
            </table>
        </td>
        <td>
            <?php if(isset($unassign) && TicketUnassign::getText($unassign)): ?>
                <p>

                </p>
                <table>
                    <tr>
                        <th>
                            Unassign Status
                        </th>
                        <td>
                            <span class="label <?php echo TicketUnassign::getLabel($unassign) ?>">
                                <?php echo TicketUnassign::getText($unassign) ?>
                            </span>
                        </td>
                    </tr>
                </table>
            <?php endif; ?>

        </td>
        </tr>
    <?php
    $idx++;
    } ?>
</table>
<div class="clear"></div>
<h2 style="padding:10px 0 5px 0; font-size:11pt;"><?php echo Format::htmlchars($ticket->getSubject()); ?></h2>
<?php
$tcount = $ticket->getThreadCount();
$tcount+= $ticket->getNumNotes();
?>
<ul id="threads">
    <li><a class="active" id="toggle_ticket_thread" href="#"><?php echo sprintf(__('Ticket Thread (%d)'), $tcount); ?></a></li>
</ul>

<div id="ticket_thread">
    <?php
    $count_reply = 0;
    $threadTypes=array('M'=>'message','R'=>'response', 'N'=>'note', 'S'=>'sms', 'AE'=>'automatic-email', 'AS'=>'automatic-sms', 'Overdue' => 'automatic-sms');
    /* -------- Messages & Responses & Notes (if inline)-------------*/
    $types = array('M', 'R', 'N', 'S', 'AE', 'AS', 'Overdue');
    if(($thread=$ticket->getThreadEntries($types))) {
       foreach($thread as $entry) { ?>
               <?php if('R' == $entry['thread_type']) $count_reply++; ?>
        <table class="thread-entry <?php echo $threadTypes[$entry['thread_type']]; ?>" cellspacing="0" cellpadding="1" width="1058" border="0">
            <tr>
                <th colspan="4" width="100%">
                <div>
                    <span class="pull-left">
                        <span style="display:inline-block"><?php echo _Format::db_short_datetime($entry['created']);?></span>
                        <span class="time-elapsed"><?php if (isset($entry['created']) && $entry['created']) echo (' - '. _String::time_elapsed_string(Misc::db2gmtime($entry['created']))); ?></span>
                        <span style="display:inline-block;padding:0 1em" class="faded title"><?php echo Format::truncate($entry['title'], 100); ?></span>
                    </span>
                    <span class="pull-right" style="white-space:no-wrap;display:inline-block">
                        <span style="vertical-align:middle;" class="textra"></span>
                        <span style="vertical-align:middle;"
                            class="tmeta faded title"><?php
                            $poster = $entry['name'] ?: $entry['poster'];
                            $name = $ticket->getName();
                            if (isset($name->name)) $name = $name->name;
                            if ($name === $poster) {
                                ?><span class="label label-danger"><?php echo Format::htmlchars($poster); ?></span><?php
                            } else
                                echo Format::htmlchars($poster);
                            ?></span>
                    </span>
                </div>
                </th>
            </tr>
            <tr><td colspan="4" class="thread-body" id="thread-id-<?php
                echo $entry['id']; ?>"><div><?php
                echo $entry['body']->toHtml(); ?></div></td></tr>
            <?php
            if($entry['attachments']
                    && ($tentry = $ticket->getThreadEntry($entry['id']))
                    && ($urls = $tentry->getAttachmentUrls())
                    && ($links = $tentry->getAttachmentsLinks())) {?>
            <tr>
                <td class="info" colspan="4"><?php echo $tentry->getAttachmentsLinks(); ?></td>
            </tr> <?php
            }
            if ($urls) { ?>
                <script type="text/javascript">
                    $('#thread-id-<?php echo $entry['id']; ?>')
                        .data('urls', <?php
                            echo JsonDataEncoder::encode($urls); ?>)
                        .data('id', <?php echo $entry['id']; ?>);
                </script>
<?php
            } ?>
        </table>
        <?php
        if($entry['thread_type']=='M')
            $msgId=$entry['id'];
       }
    } else {
        echo '<p>'.__('Error fetching ticket thread - get technical help.').'</p>';
    }?>
</div>
<div class="clear" style="padding-bottom:10px;"></div>
<?php if($errors['err']) { ?>
    <div id="msg_error"><?php echo $errors['err']; ?></div>
<?php }elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php }elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php } else ?>
<?php
// allow custom field to display in reply/handover
$allow = ['bookingcode', 'flag', 'colorlabel'];

$input_subject = '';
$sql = sprintf(
    "SELECT `subject` FROM %s c 
      JOIN %s r ON c.ticket_id=r.ticket_id
      AND c.ticket_id=%s AND r.thread_type='R' LIMIT 1
    ",
    TICKET_TABLE.'__cdata',
    TICKET_THREAD_TABLE,
    db_input($ticket->getId())
);

if (($res = db_query($sql))) {
    $row = db_fetch_row($res);
    if ($row) {
        $input_subject = $row[0];
    }
}
?>

<?php if ($errors && !$errors['err']) {?>
    <?php foreach($errors as $e) { ?><div class="error-banner"><?php echo $e ?></div><?php } ?>
    <?php
} ?>
<?php if($unassign != TicketUnassign::WELCOME): ?>
    <div id="response_options">
        <?php if(!$check_user): ?>
            <div class="error-banner"><p><strong>Vui lòng cập nhật thông tin khách trước khi Assign</strong></p></div>
        <?php endif; ?>
        <ul class="tabs">
            <?php
            if($thisstaff->canPostReply() && $staff && $check_user) { ?>
                <li><a id="reply_tab" href="#reply"><?php echo __('Post Reply');?></a></li>
                <?php
            } ?>
            <?php if ($staff && $check_user) { ?>
                <li><a id="note_tab" href="#note"><?php echo __('Post Internal Note');?></a></li>
            <?php } ?>
            <?php
            if($thisstaff->canTransferTickets() && $staff && $check_user) { ?>
                <li><a id="transfer_tab" href="#transfer"><?php echo __('Department Transfer');?></a></li>
                <?php
            }

            if($thisstaff->canAssignTickets() && $check_user) { ?>
                <li><a id="assign_tab" href="#assign"><?php echo $ticket->isAssigned()?__('Reassign Ticket'):__('Assign Ticket'); ?></a></li>
                <?php
            } ?>

            <?php
            if($thisstaff->canPostReply() && $staff && $check_user) { ?>
                <li><a id="handovered_tab" href="#handovered"><?php echo __('Handovered Reply');?></a></li>
                <?php
            } ?>


            <?php
            if($thisstaff->canPostReply() && $staff && $check_user) { ?>
                <li><a id="sms_tab" href="#sms"><?php echo __('Send SMS');?></a></li>
                <?php
            } ?>
            
            <?php
            if($thisstaff->canPostReply() && $staff && $check_user) { ?>
                <li><a id="contract_tab" href="#contract"><?php echo __('Send SMS Contract');?></a></li>
                <?php
            } ?>
        </ul>
        <?php 
        $ReplyToEmail = $ticket->getReplyToEmail();
        if ($staff && $check_user) { ?>
            <form id="contract" action="tickets.php?id=<?php echo $ticket->getId(); ?>#contract" name="contract" method="post" enctype="multipart/form-data">

                <?php csrf_token(); ?>
                <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                <input type="hidden" name="a" value="contract">
                <span class="error"></span>
                <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Tên khách'); ?>:</strong></label>
                            </td>
                            <td>
                                 <?php echo $ticket->getName() ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Phone number'); ?>:</strong></label>
                            </td>
                            <td>
                                <?php echo $phone_number?>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Email'); ?>:</strong></label>
                            </td>
                            <td>
                                <?php echo $ReplyToEmail ?>
                            </td>
                        </tr>
                         <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('SMS contract'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                            </td>
                            <td>
                                <div class="error"><?php echo $errors['note']; ?></div>
                                <textarea name="sms_contract_content" id="sms_contract_content" cols="80"></textarea>
                            </td>
                        </tr>
                </table>
                <p style="padding:0 165px;">
                    <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Send contract');?>">
                
                </p>
            </form> 
            <?php
        } ?>  
        <?php
        $ReplyToEmail = $ticket->getReplyToEmail();
        if($thisstaff->canPostReply() && $staff && $check_user) { ?>
            <form id="reply" action="tickets.php?id=<?php echo $ticket->getId(); ?>#reply" name="reply" method="post" enctype="multipart/form-data">
                <?php if( preg_match('/^[0-9]+\@tugo/', $ReplyToEmail) ) { ?>
                    <p class="error-banner"><strong>KHÁCH CHƯA CÓ EMAIL</strong></p>
                <?php } else { ?>
                    <?php csrf_token(); ?>
                    <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                    <input type="hidden" name="msgId" value="<?php echo $msgId; ?>">
                    <input type="hidden" name="a" value="reply">
                    <span class="error"></span>
                    <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
                        <tbody id="to_sec">
                        <tr>
                            <td width="120">
                                <label><strong><?php echo __('To'); ?>:</strong></label>
                            </td>
                            <td>
                                <?php
                                # XXX: Add user-to-name and user-to-email HTML ID#s
                                $to =sprintf('%s &lt;%s&gt;',
                                             Format::htmlchars($ticket->getName()), $ReplyToEmail);
                                $emailReply = (!isset($info['emailreply']) || $info['emailreply']);
                                ?>
                                <select id="emailreply" name="emailreply">
                                    <option value="1" <?php echo $emailReply ?  'selected="selected"' : ''; ?>><?php echo $to; ?></option>
                                    <option value="0" <?php echo !$emailReply ? 'selected="selected"' : ''; ?>
                                    >&mdash; <?php echo __('Do Not Email Reply'); ?> &mdash;</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                        <?php
                        if(1) { //Make CC optional feature? NO, for now.
                            ?>
                            <tbody id="cc_sec"
                                   style="display:<?php echo $emailReply?  'table-row-group':'none'; ?>;">
                            <tr>
                                <td width="120">
                                    <label><strong><?php echo __('Collaborators'); ?>:</strong></label>
                                </td>
                                <td>
                                    <input type='checkbox' value='1' name="emailcollab" id="emailcollab"
                                        <?php echo ((!$info['emailcollab'] && !$errors) || isset($info['emailcollab']))?'checked="checked"':''; ?>
                                           style="display:<?php echo $ticket->getNumCollaborators() ? 'inline-block': 'none'; ?>;"
                                    >
                                    <?php
                                    $recipients = __('Add Recipients');
                                    if ($ticket->getNumCollaborators())
                                        $recipients = sprintf(__('Recipients (%d of %d)'),
                                                              $ticket->getNumActiveCollaborators(),
                                                              $ticket->getNumCollaborators());

                                    echo sprintf('<span><a class="collaborators preview"
                            href="#tickets/%d/collaborators"><span id="recipients">%s</span></a></span>',
                                                 $ticket->getId(),
                                                 $recipients);
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                            <?php
                        } ?>
                        <tbody id="resp_sec">
                        <?php
                        if($errors['response']) {?>
                            <tr><td width="120">&nbsp;</td><td class="error"><?php echo $errors['response']; ?>&nbsp;</td></tr>
                            <?php
                        }?>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Response');?>:</strong></label>
                            </td>
                            <td>
                                <?php if ($cfg->isCannedResponseEnabled()) { ?>
                                    <select id="cannedResp" name="cannedResp">
                                        <option value="0" selected="selected"><?php echo __('Select a canned response');?></option>
                                        <option value='original'><?php echo __('Original Message'); ?></option>
                                        <option value='lastmessage'><?php echo __('Last Message'); ?></option>
                                        <?php
                                        if(($cannedResponses=Canned::responsesByDeptId($ticket->getDeptId()))) {
                                            echo '<option value="0" disabled="disabled">
                                ------------- '.__('Premade Replies').' ------------- </option>';
                                            foreach($cannedResponses as $id =>$title)
                                                echo sprintf('<option value="%d">%s</option>',$id,$title);
                                        }
                                        ?>
                                    </select>
                                    <br>
                                <?php } # endif (canned-resonse-enabled)
                                $signature = '';
                                switch ($thisstaff->getDefaultSignatureType()) {
                                    case 'dept':
                                        if ($dept && $dept->canAppendSignature())
                                            $signature = $dept->getSignature();
                                        break;
                                    case 'mine':
                                        $signature = $thisstaff->getSignature();
                                        break;
                                } ?>
                            </td>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label>
                                    <strong><?php echo __('Email subject');?>:</strong>
                                    <span class="error">&nbsp;*</span>
                                </label>
                            </td>
                            <td>
                                <input type="text" name="subject" value="<?php if($input_subject) echo Format::htmlchars($input_subject); ?>" size="60" placeholder="Enter a new Subject, used for email" required/>
                                <input type="hidden" name="draft_id" value=""/>
                                <textarea name="response" id="response" cols="50"
                                          data-draft-namespace="ticket.response"
                                          data-signature-field="signature" data-dept-id="<?php echo $dept->getId(); ?>"
                                          data-signature="<?php
                                          echo Format::htmlchars(Format::viewableImages($signature)); ?>"
                                          placeholder="<?php echo __(
                                              'Start writing your response here. Use canned responses from the drop-down above'
                                          ); ?>"
                                          data-draft-object-id="<?php echo $ticket->getId(); ?>"
                                          rows="9" wrap="soft"
                                          class="richtext ifhtml draft draft-delete"><?php
                                    echo $info['response']; ?></textarea>
                                <div id="reply_form_attachments" class="attachments">
                                    <?php
                                    print $response_form->getField('attachments')->render();
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="120">
                                <label for="signature" class="left"><?php echo __('Signature');?>:</label>
                            </td>
                            <td>
                                <?php
                                $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                                ?>
                                <label><input type="radio" name="signature" value="none" checked="checked"> <?php echo __('None');?></label>
                                <?php
                                if($thisstaff->getSignature()) {?>
                                    <label><input type="radio" name="signature" value="mine"
                                            <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> <?php echo __('My Signature');?></label>
                                    <?php
                                } ?>
                                <?php
                                if($dept && $dept->canAppendSignature()) { ?>
                                    <label><input type="radio" name="signature" value="dept"
                                            <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>>
                                        <?php echo sprintf(__('Department Signature (%s)'), Format::htmlchars($dept->getName())); ?></label>
                                    <?php
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="120">
                                <label><strong><?php echo __('Ticket Status');?>:</strong></label>
                            </td>
                            <td>
                                <select name="reply_status_id">
                                    <?php
                                    $statusId = $info['reply_status_id'] ?: $ticket->getStatusId();
                                    $states = array('open');
                                    if ($thisstaff->canCloseTickets())
                                        $states = array_merge($states, array('closed'));

                                    foreach (TicketStatusList::getStatuses(
                                        array('states' => $states)) as $s) {
                                        if (!$s->isEnabled()) continue;
                                        $selected = ($statusId == $s->getId());
                                        echo sprintf('<option value="%d" %s>%s%s</option>',
                                                     $s->getId(),
                                                     $selected
                                                         ? 'selected="selected"' : '',
                                                     __($s->getName()),
                                                     $selected
                                                         ? (' ('.__('current').')') : ''
                                        );
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p  style="padding:0 165px;">
                        <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Post Reply');?>">
                        <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                    </p>
                <?php } ?>
            </form>
            <?php
        } ?>

        <?php if ($staff) { ?>
            <form id="note" action="tickets.php?id=<?php echo $ticket->getId(); ?>#note" name="note" method="post" enctype="multipart/form-data">
                <?php csrf_token(); ?>
                <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime(); ?>">
                <input type="hidden" name="a" value="postnote">
                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <?php
                    if($errors['postnote']) {?>
                        <tr>
                            <td width="120">&nbsp;</td>
                            <td class="error"><?php echo $errors['postnote']; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label><strong><?php echo __('Internal Note'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                        </td>
                        <td>
                            <div>
                                <div class="faded" style="padding-left:0.15em"><?php
                                    echo __('Note title - summary of the note (optional)'); ?></div>
                                <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                                <br/>
                                <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                            </div>
                            <br/>
                            <div class="error"><?php echo $errors['note']; ?></div>
                            <textarea name="note" id="internal_note" cols="80"
                                      placeholder="<?php echo __('Note details'); ?>"
                                      rows="9" wrap="soft" data-draft-namespace="ticket.note"
                                      data-draft-object-id="<?php echo $ticket->getId(); ?>"
                                      class="richtext ifhtml draft draft-delete"><?php echo $info['note'];
                                ?></textarea>
                            <div class="attachments">
                                <?php
                                print $note_form->getField('attachments')->render();
                                ?>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td width="120">
                            <label><?php echo __('Ticket Status');?>:</label>
                        </td>
                        <td>
                            <?php echo TicketStatus::status_options(false); ?>
                        </td>
                    </tr>
                    <?php if ($forms)
                        foreach ($forms as $form) {
                            $form->render(true, false, array('mode'=>'edit', 'width'=>160, 'entry'=>$form, 'allow' => $allow));
                        } ?>

                    <tr>
                        <td></td>
                        <td>
                            <p>CHÚ Ý: Sau cuộc gọi nhỡ (khách không nghe máy/máy bận), hệ thống sẽ TỰ ĐỘNG gửi SMS báo cuộc gọi nhỡ cho khách, không cần gửi SMS thêm.</p>
                            <p>Nếu gọi mà máy báo FAILED (hoặc không nghe tiếng bíp bíp kéo dài/nhạc chờ), gọi lại lần nữa để đảm bảo không phải do lỗi kết nối.</p>
                        </td>
                    </tr>
                </table>

                <p  style="padding-left:165px;">
                    <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Post Note');?>">
                    <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                </p>
            </form>
        <?php } ?>
        <?php
        if($thisstaff->canTransferTickets() && $staff && $check_user) { ?>
            <form id="transfer" action="tickets.php?id=<?php echo $ticket->getId(); ?>#transfer" name="transfer" method="post" enctype="multipart/form-data">
                <?php csrf_token(); ?>
                <input type="hidden" name="ticket_id" value="<?php echo $ticket->getId(); ?>">
                <input type="hidden" name="a" value="transfer">
                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <?php
                    if($errors['transfer']) {
                        ?>
                        <tr>
                            <td width="120">&nbsp;</td>
                            <td class="error"><?php echo $errors['transfer']; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td width="120">
                            <label for="deptId"><strong><?php echo __('Department');?>:</strong></label>
                        </td>
                        <td>
                            <?php
                            echo sprintf('<span class="faded">'.__('Ticket is currently in <b>%s</b> department.').'</span>', $ticket->getDeptName());
                            ?>
                            <br>
                            <select id="deptId" name="deptId">
                                <option value="0" selected="selected">&mdash; <?php echo __('Select Target Department');?> &mdash;</option>
                                <?php
                                if($depts=Dept::getDepartments()) {
                                    foreach($depts as $id =>$name) {
                                        if($id==$ticket->getDeptId()) continue;
                                        echo sprintf('<option value="%d" %s>%s</option>',
                                                     $id, ($info['deptId']==$id)?'selected="selected"':'',$name);
                                    }
                                }
                                ?>
                            </select>&nbsp;<span class='error'>*&nbsp;<?php echo $errors['deptId']; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label><strong><?php echo __('Comments'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                        </td>
                        <td>
                    <textarea name="transfer_comments" id="transfer_comments"
                              placeholder="<?php echo __('Enter reasons for the transfer'); ?>"
                              class="richtext ifhtml no-bar" cols="80" rows="7" wrap="soft"><?php
                        echo $info['transfer_comments']; ?></textarea>
                            <span class="error"><?php echo $errors['transfer_comments']; ?></span>
                        </td>
                    </tr>
                </table>
                <p style="padding-left:165px;">
                    <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Transfer');?>">
                    <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                </p>
            </form>
            <?php
        } ?>
        <?php
        if($thisstaff->canAssignTickets() && $check_user) { ?>
            <form id="assign" action="tickets.php?id=<?php echo $ticket->getId(); ?>#assign" name="assign" method="post" enctype="multipart/form-data">
                <?php csrf_token(); ?>
                <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                <input type="hidden" name="a" value="assign">
                <table style="width:100%" border="0" cellspacing="0" cellpadding="3">

                    <?php
                    if($errors['assign']) {
                        ?>
                        <tr>
                            <td width="120">&nbsp;</td>
                            <td class="error"><?php echo $errors['assign']; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label for="assignId"><strong><?php echo __('Assignee');?>:</strong></label>
                        </td>
                        <td>
                            <select id="assignId" name="assignId">
                                <option value="0" selected="selected">&mdash; <?php echo __('Select an Agent OR a Team');?> &mdash;</option>
                                <?php
                                if ($ticket->isOpen()
                                    && !$ticket->isAssigned()
                                    && $ticket->getDept()->isMember($thisstaff))
                                    echo sprintf('<option value="%d">'.__('Claim Ticket (comments optional)').'</option>', $thisstaff->getId());

                                $sid=$tid=0;

                                if ($dept->assignMembersOnly())
                                    $users = $dept->getAvailableMembers();
                                else
                                    $users = Staff::getSaleOnly(true, true);

                                if ($users) {
                                    echo '<OPTGROUP label="'.sprintf(__('Agents (%d)'), count($users)).'">';
                                    $staffId=$ticket->isAssigned()?$ticket->getStaffId():0;
                                    foreach($users as $id => $name) {
                                        if($staffId && $staffId==$id)
                                            continue;
                                        $k="s$id";
                                        echo sprintf('<option value="%s" %s>%s</option>',
                                                     $k,(($info['assignId']==$k)?'selected="selected"':''), $name);
                                    }
                                    echo '</OPTGROUP>';
                                }

                                if(($teams=Team::getActiveTeams())) {
                                    echo '<OPTGROUP label="'.sprintf(__('Teams (%d)'), count($teams)).'">';
                                    $teamId=(!$sid && $ticket->isAssigned())?$ticket->getTeamId():0;
                                    foreach($teams as $id => $name) {
                                        if($teamId && $teamId==$id)
                                            continue;

                                        $k="t$id";
                                        echo sprintf('<option value="%s" %s>%s</option>',
                                                     $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                                    }
                                    echo '</OPTGROUP>';
                                }
                                ?>
                            </select>
                            &nbsp;<span class='error'>*&nbsp;<?php echo $errors['assignId']; ?></span>

                            <?php echo (!$staff ? "<p class='error'>Assgin before writing note/reply</p>" : '') ?>
                            <?php
                            if ($ticket->isAssigned() && $ticket->isOpen()) { ?>
                                <div class="faded"><?php echo sprintf(__('Ticket is currently assigned to %s'),
                                                                      sprintf('<b>%s</b>', $ticket->getAssignee())); ?></div> <?php
                            } elseif ($ticket->isClosed()) { ?>
                                <div class="faded"><?php echo __('Assigning a closed ticket will <b>reopen</b> it!'); ?></div>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label><strong><?php echo __('Comments');?>:</strong><span class='error'>&nbsp;</span></label>
                        </td>
                        <td>
                    <textarea name="assign_comments" id="assign_comments"
                              cols="80" rows="7" wrap="soft"
                              placeholder="<?php echo __('Enter reasons for the assignment or instructions for assignee'); ?>"
                              class="richtext ifhtml no-bar"><?php echo $info['assign_comments']; ?></textarea>
                            <span class="error"><?php echo $errors['assign_comments']; ?></span><br>
                        </td>
                    </tr>
                </table>
                <p  style="padding-left:165px;">
                    <input class="btn_sm btn-primary" type="submit" value="<?php echo $ticket->isAssigned()?__('Reassign'):__('Assign'); ?>">
                    <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                </p>
            </form>
            <?php
        } ?>

        <?php
        if($thisstaff->canPostReply() && $staff && $check_user) { ?>
            <form id="handovered" action="tickets.php?id=<?php echo $ticket->getId(); ?>#handovered" name="handovered" method="post" enctype="multipart/form-data">
                <?php csrf_token(); ?>
                <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                <input type="hidden" name="msgId" value="<?php echo $msgId; ?>">
                <input type="hidden" name="a" value="handovered">
                <span class="error"></span>
                <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
                    <tbody id="to_sec">
                    <tr>
                        <td width="120">
                            <label><strong><?php echo __('To'); ?>:</strong></label>
                        </td>
                        <td>
                            <input type="email" required value="" size="60" name="emailreply" />
                        </td>
                    </tr>
                    </tbody>
                    <?php
                    if(1) { //Make CC optional feature? NO, for now.
                        ?>
                        <tbody id="cc_sec"
                               style="display:<?php echo $emailReply?  'table-row-group':'none'; ?>;">
                        <tr>
                            <td width="120">
                                <label><strong><?php echo __('Collaborators'); ?>:</strong></label>
                            </td>
                            <td>
                                <input type='checkbox' value='1' name="emailcollab" id="emailcollab"
                                    <?php echo ((!$info['emailcollab'] && !$errors) || isset($info['emailcollab']))?'checked="checked"':''; ?>
                                       style="display:<?php echo $ticket->getNumCollaborators() ? 'inline-block': 'none'; ?>;"
                                >
                                <?php
                                $recipients = __('Add Recipients');
                                if ($ticket->getNumCollaborators())
                                    $recipients = sprintf(__('Recipients (%d of %d)'),
                                                          $ticket->getNumActiveCollaborators(),
                                                          $ticket->getNumCollaborators());

                                echo sprintf('<span><a class="collaborators preview"
                            href="#tickets/%d/collaborators"><span id="recipients">%s</span></a></span>',
                                             $ticket->getId(),
                                             $recipients);
                                ?>
                            </td>
                        </tr>
                        </tbody>
                        <?php
                    } ?>
                    <tbody id="resp_sec">
                    <?php
                    if($errors['response']) {?>
                        <tr><td width="120">&nbsp;</td><td class="error"><?php echo $errors['response']; ?>&nbsp;</td></tr>
                        <?php
                    }?>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label><strong><?php echo __('Response');?>:</strong></label>
                        </td>
                        <td>
                            <?php if ($cfg->isCannedResponseEnabled()) { ?>
                                <select id="cannedResp" name="cannedResp">
                                    <option value="0" selected="selected"><?php echo __('Select a canned response');?></option>
                                    <option value='original'><?php echo __('Original Message'); ?></option>
                                    <option value='lastmessage'><?php echo __('Last Message'); ?></option>
                                    <?php
                                    if(($cannedResponses=Canned::responsesByDeptId($ticket->getDeptId()))) {
                                        echo '<option value="0" disabled="disabled">
                                ------------- '.__('Premade Replies').' ------------- </option>';
                                        foreach($cannedResponses as $id =>$title)
                                            echo sprintf('<option value="%d">%s</option>',$id,$title);
                                    }
                                    ?>
                                </select>
                                <br>
                            <?php } # endif (canned-resonse-enabled)
                            $signature = '';
                            switch ($thisstaff->getDefaultSignatureType()) {
                                case 'dept':
                                    if ($dept && $dept->canAppendSignature())
                                        $signature = $dept->getSignature();
                                    break;
                                case 'mine':
                                    $signature = $thisstaff->getSignature();
                                    break;
                            } ?>
                        </td>
                    <tr>
                        <td width="120" style="vertical-align:top">
                            <label>
                                <strong><?php echo __('Subject');?>:</strong>
                                <span class="error">&nbsp;*</span>
                            </label>
                        </td>
                        <td>
                            <input type="text" name="subject" value="<?php if($input_subject) echo Format::htmlchars($input_subject); ?>" size="60" placeholder="Enter a new Subject, used for email" required/>
                            <input type="hidden" name="draft_id" value=""/>
                            <textarea name="response" id="response" cols="50"
                                      data-draft-namespace="ticket.response"
                                      data-signature-field="signature" data-dept-id="<?php echo $dept->getId(); ?>"
                                      data-signature="<?php
                                      echo Format::htmlchars(Format::viewableImages($signature)); ?>"
                                      placeholder="<?php echo __(
                                          'Start writing your response here. Use canned responses from the drop-down above'
                                      ); ?>"
                                      data-draft-object-id="<?php echo $ticket->getId(); ?>"
                                      rows="9" wrap="soft"
                                      class="richtext ifhtml draft draft-delete"><?php
                                echo $info['response']; ?></textarea>
                            <div id="reply_form_attachments" class="attachments">
                                <?php
                                print $response_form->getField('attachments')->render();
                                ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="120">
                            <label for="signature" class="left"><?php echo __('Signature');?>:</label>
                        </td>
                        <td>
                            <?php
                            $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                            ?>
                            <label><input type="radio" name="signature" value="none" checked="checked"> <?php echo __('None');?></label>
                            <?php
                            if($thisstaff->getSignature()) {?>
                                <label><input type="radio" name="signature" value="mine"
                                        <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> <?php echo __('My Signature');?></label>
                                <?php
                            } ?>
                            <?php
                            if($dept && $dept->canAppendSignature()) { ?>
                                <label><input type="radio" name="signature" value="dept"
                                        <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>>
                                    <?php echo sprintf(__('Department Signature (%s)'), Format::htmlchars($dept->getName())); ?></label>
                                <?php
                            } ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120">
                            <label><strong><?php echo __('Ticket Status');?>:</strong></label>
                        </td>
                        <td>
                            <select name="reply_status_id">
                                <?php
                                $statusId = $info['reply_status_id'] ?: $ticket->getStatusId();
                                $states = array('open');
                                if ($thisstaff->canCloseTickets())
                                    $states = array_merge($states, array('closed'));

                                foreach (TicketStatusList::getStatuses(
                                    array('states' => $states)) as $s) {
                                    if (!$s->isEnabled()) continue;
                                    $selected = ($statusId == $s->getId());
                                    echo sprintf('<option value="%d" %s>%s%s</option>',
                                                 $s->getId(),
                                                 $selected
                                                     ? 'selected="selected"' : '',
                                                 __($s->getName()),
                                                 $selected
                                                     ? (' ('.__('current').')') : ''
                                    );
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p  style="padding:0 165px;">
                    <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Handovered Reply');?>">
                    <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                </p>
            </form>
            <?php
        } ?>

        <?php if ($staff && $check_user) { ?>
            <form id="sms" action="tickets.php?id=<?php echo $ticket->getId(); ?>#sms" name="sms" method="post" enctype="multipart/form-data">
                <?php if (empty($phone_number)) {
                    ?><p class="error-banner"><strong>KHÁCH CHƯA CÓ SỐ ĐIỆN THOẠI</strong></p><?php
                } else { ?>
                    <?php csrf_token(); ?>
                    <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
                    <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime(); ?>">
                    <input type="hidden" name="a" value="sendsms">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <?php
                        if($errors) {?>
                            <tr>
                                <td width="120">&nbsp;</td>
                                <td class="error">
                                    <ul>
                                        <?php foreach($errors as $e) {
                                            ?><li><?php echo $e ?></li><?php
                                        } ?>
                                    </ul>
                                </td>
                            </tr>
                            <?php
                        } ?>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Response');?>:</strong></label>
                            </td>
                            <td>
                                <?php if ($cfg->isCannedResponseEnabled()) { ?>
                                    <select id="cannedResp" name="cannedResp">
                                        <option value="0" selected="selected"><?php echo __('Select a canned response');?></option>
                                        <option value='original'><?php echo __('Original Message'); ?></option>
                                        <option value='lastmessage'><?php echo __('Last Message'); ?></option>
                                        <?php
                                        if(($cannedResponses=Canned::responsesByDeptId($ticket->getDeptId()))) {
                                            echo '<option value="0" disabled="disabled">
                                ------------- '.__('Premade Replies').' ------------- </option>';
                                            foreach($cannedResponses as $id =>$title)
                                                echo sprintf('<option value="%d">%s</option>',$id,$title);
                                        }
                                        ?>
                                    </select>
                                    <br>
                                <?php } # endif (canned-resonse-enabled)
                                $signature = '';
                                switch ($thisstaff->getDefaultSignatureType()) {
                                    case 'dept':
                                        if ($dept && $dept->canAppendSignature())
                                            $signature = $dept->getSignature();
                                        break;
                                    case 'mine':
                                        $signature = $thisstaff->getSignature();
                                        break;
                                } ?>
                            </td>
                        <tr>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Phone number'); ?>:</strong></label>
                            </td>
                            <td>
                                <?php echo $phone_number ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('SMS Content'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                            </td>
                            <td>
                                <div class="error"><?php echo $errors['note']; ?></div>
                                <textarea name="sms_content" id="sms_content" cols="80"
                                          placeholder="<?php echo __('Nội dung SMS. Tối đa '.SMS_CHAR_LIMIT.' kí tự. Su dung Tieng Viet KHONG DAU. Nếu viết tiếng Việt CÓ DẤU thì dấu tiếng Việt sẽ được tự động lược bỏ.'); ?>"
                                          rows="3" maxlength="<?php echo SMS_CHAR_LIMIT ?>" minlength="10" required wrap="soft" data-draft-namespace="ticket.sms_content"
                                          data-draft-object-id="<?php echo $ticket->getId(); ?>"
                                          class="draft draft-delete"><?php echo $info['sms_content'];
                                    ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" style="vertical-align:top">
                                <label><strong><?php echo __('Character Count'); ?>:</strong></label>
                            </td>
                            <td>
                                <div id="sms_char_count"><?php echo $info['sms_content'] ? strlen($info['sms_content']) : '0' ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <p>CHÚ Ý: Sau cuộc gọi nhỡ (khách không nghe máy/máy bận), hệ thống sẽ TỰ ĐỘNG gửi SMS báo cuộc gọi nhỡ cho khách, không cần gửi SMS thêm.</p>
                                <p>Nếu gọi mà máy báo FAILED (hoặc không nghe tiếng bíp bíp kéo dài/nhạc chờ), gọi lại lần nữa để đảm bảo không phải do lỗi kết nối.</p>
                            </td>
                        </tr>

                    </table>

                    <p  style="padding-left:165px;">
                        <input class="btn_sm btn-primary" type="submit" value="<?php echo __('Send SMS');?>">
                        <input class="btn_sm btn-default" type="reset" value="<?php echo __('Reset');?>">
                    </p>
                <?php } ?>
            </form>
            <script>
                (function($) {
                    $('#sms_content').off('change keyup')
                        .on('change keyup', function(event) {
                            $('#sms_char_count').text($('#sms_content').val().length);
                        })
                    $('#sms_content').off('blur')
                        .on('blur', function(event) {
                            $('#sms_content').val($('#sms_content').val().trim());
                            $('#sms_char_count').text($('#sms_content').val().length);
                        })
                })(jQuery);
            </script>
        <?php } ?>
    </div>
<?php endif; ?>

<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

<div id="jqxLoader"></div>
<div id='scheduler'></div>

<div style="display:none;" class="dialog" id="print-options">
    <h3><?php echo __('Ticket Print Options');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="print-form" name="print-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="print">
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <fieldset class="notes">
            <label class="fixed-size" for="notes"><?php echo __('Print Notes');?>:</label>
            <input type="checkbox" id="notes" name="notes" value="1"> <?php echo __('Print <b>Internal</b> Notes/Comments');?>
        </fieldset>
        <fieldset>
            <label class="fixed-size" for="psize"><?php echo __('Paper Size');?>:</label>
            <select id="psize" name="psize">
                <option value="">&mdash; <?php echo __('Select Print Paper Size');?> &mdash;</option>
                <?php
                  $psize =$_SESSION['PAPER_SIZE']?$_SESSION['PAPER_SIZE']:$thisstaff->getDefaultPaperSize();
                  foreach(Export::$paper_sizes as $v) {
                      echo sprintf('<option value="%s" %s>%s</option>',
                                $v,($psize==$v)?'selected="selected"':'', __($v));
                  }
                ?>
            </select>
        </fieldset>
        <hr style="margin-top:3em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="reset" class="btn_sm btn-default" value="<?php echo __('Reset');?>">
                <input type="button" class="close btn_sm btn-danger" value="<?php echo __('Cancel');?>">
            </span>
            <span class="buttons pull-right">
                <input type="submit" class="btn_sm btn-primary" value="<?php echo __('Print');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="claim-confirm">
        <?php echo __('Are you sure you want to <b>claim</b> (self assign) this ticket?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="answered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>answered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unanswered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>unanswered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="overdue-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <font color="red"><b>overdue</b></font>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="banemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>ban</b> %s?'), $ticket->getEmail());?> <br><br>
        <?php echo __('New tickets from the email address will be automatically rejected.');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unbanemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>remove</b> %s from ban list?'), $ticket->getEmail()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="release-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>unassign</b> ticket from <b>%s</b>?'), $ticket->getAssigned()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="changeuser-confirm">
        <span id="msg_warning" style="display:block;vertical-align:top">
        <?php echo sprintf(Format::htmlchars(__('%s <%s> will longer have access to the ticket')),
            '<b>'.Format::htmlchars($ticket->getName()).'</b>', Format::htmlchars($ticket->getEmail())); ?>
        </span>
        <?php echo sprintf(__('Are you sure you want to <b>change</b> ticket owner to %s?'),
            '<b><span id="newuser">this guy</span></b>'); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo __('Are you sure you want to DELETE this ticket?');?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered, including any associated attachments.');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="confirm-form" name="confirm-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="process">
        <input type="hidden" name="do" id="action" value="">
        <hr style="margin-top:1em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="button" class="close btn_sm btn-default" value="<?php echo __('Cancel');?>">
            </span>
            <span class="buttons pull-right">
                <input type="submit" class="btn_sm btn-primary" value="<?php echo __('OK');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>

<script type="text/javascript">
$(function() {
    $(document).on('click', 'a.change-user', function(e) {
        e.preventDefault();
        var tid = <?php echo $ticket->getOwnerId(); ?>;
        var cid = <?php echo $ticket->getOwnerId(); ?>;
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        $.userLookup(url, function(user) {
            if(cid!=user.id
                    && $('.dialog#confirm-action #changeuser-confirm').length) {
                $('#newuser').html(user.name +' &lt;'+user.email+'&gt;');
                $('.dialog#confirm-action #action').val('changeuser');
                $('#confirm-form').append('<input type=hidden name=user_id value='+user.id+' />');
                $('#overlay').show();
                $('.dialog#confirm-action .confirm-action').hide();
                $('.dialog#confirm-action p#changeuser-confirm')
                .show()
                .parent('div').show().trigger('click');
            }
        });
    });
<?php
    // Set the lock if one exists
    if ($lock) { ?>
!function() {
  var setLock = setInterval(function() {
    if (typeof(window.autoLock) === 'undefined')
      return;
    clearInterval(setLock);
    autoLock.setLock({
      id:<?php echo $lock->getId(); ?>,
      time: <?php echo $cfg->getLockTime(); ?>}, 'acquire');
  }, 50);
}();
<?php } ?>
});
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var _add = false;
        if ($("#jqxLoader"))
            $("#jqxLoader").jqxLoader({ text: "", width: 60, height: 60 });

        appointments = JSON.parse('<?php echo json_encode($calendar); ?>');
        // prepare the data
        var source =
        {
            dataType: 'json',
            dataFields: [
                { name: 'id', type: 'string' },
                { name: 'content', type: 'string' },
                { name: 'start', type: 'date', format: "yyyy-MM-dd HH:mm" },
                { name: 'end', type: 'date', format: "yyyy-MM-dd HH:mm" },
                { name: 'create', type: 'string' }
            ],
            id: 'id',
            localData: appointments
        };
        var adapter = new $.jqx.dataAdapter(source);
        $("#scheduler").jqxScheduler({
            date: new $.jqx.date(),
            width: 936,
            height: 550,
            rowsHeight: 10,
            source: adapter,
            editDialogCreate: function (dialog, fields, editAppointment) {
                // hide repeat option
                fields.repeatContainer.hide();
                // hide status option
                fields.statusContainer.hide();
                // hide timeZone option
                fields.timeZoneContainer.hide();
                // hide color option
                fields.colorContainer.hide();
                fields.resourceLabel.hide();
                fields.locationLabel.hide();
                fields.allDayLabel.hide();
                fields.descriptionLabel.hide();
                fields.allDayContainer.hide();
                fields.descriptionContainer.hide();
                fields.locationContainer.hide();
                fields.repeatContainer.hide();
                fields.repeatLabel.hide();

                fields.subjectLabel.html("Title");
                fields.fromLabel.html("Start");
                fields.toLabel.html("End");
            },
            ready: function () {
            },
            appointmentDataFields:
            {
                from: "start",
                to: "end",
                id: "id",
                subject: "content"
            },
            view: 'weekView',
            views:
                [
                    {
                        type: "weekView",
                        timeRuler: { scaleStartHour: 8, scaleEndHour: 17 },
                        workTime:
                        {
                            fromDayOfWeek: 1,
                            toDayOfWeek: 6,
                            fromHour: 8,
                            toHour: 18
                        }
                    },
                    {
                        type: "monthView",
                        timeRuler: { scaleStartHour: 8, scaleEndHour: 17 },
                        workTime:
                        {
                            fromDayOfWeek: 1,
                            toDayOfWeek: 6,
                            fromHour: 8,
                            toHour: 17
                        }
                    }
                ]
        });

        $("#scheduler").on('appointmentDelete', function (event) {
            if (_add) return false;
            var args = event.args;
            var appointment = args.appointment;
            $('#jqxLoader').jqxLoader('open');
            $.post('/scp/ajax.php/tickets/<?php echo $_id ?>/'+appointment.id+'/delete-calendar', {
                id: appointment.id,
                start_date: appointment.from.toString(),
                end_date: appointment.to.toString(),
                title: appointment.subject
            }, function (data) {
                $('#jqxLoader').jqxLoader('close');
            })
        });

        $("#scheduler").on('appointmentAdd', function (event) {
            if (!_add) {
                _add = true;
            } else {
                _add = false;
                return false;
            }
            var args = event.args;
            var appointment = args.appointment;
            $('#jqxLoader').jqxLoader('open');
            console.log(appointment);
            _id = appointment.id;
            $.post('/scp/ajax.php/tickets/<?php echo $_id ?>/'+appointment.id+'/save-calendar', {
                id: appointment.id,
                start_date: appointment.from.toString(),
                end_date: appointment.to.toString(),
                title: appointment.subject
            }, function (data) {
                console.log(data);
                console.log(_id);
                a = appointment;
                a.id = data;
                console.log(a);
                $('#scheduler').jqxScheduler('beginAppointmentsUpdate');
                $("#scheduler").jqxScheduler('ensureAppointmentVisible', _id);
                $("#scheduler").jqxScheduler('deleteAppointment', _id);
                $('#scheduler').jqxScheduler('addAppointment', a);
                $('#scheduler').jqxScheduler('endAppointmentsUpdate');

                $('#jqxLoader').jqxLoader('close');
            })
        });

        $("#scheduler").on('appointmentDoubleClick', function (event) {
            var args = event.args;
            var appointment = args.appointment;
            // appointment fields
            // originalData - the bound data.
            // from - jqxDate object which returns when appointment starts.
            // to - jqxDate objet which returns when appointment ends.
            // status - String which returns the appointment's status("busy", "tentative", "outOfOffice", "free", "").
            // resourceId - String which returns the appointment's resouzeId
            // hidden - Boolean which returns whether the appointment is visible.
            // allDay - Boolean which returns whether the appointment is allDay Appointment.
            // resiable - Boolean which returns whether the appointment is resiable Appointment.
            // draggable - Boolean which returns whether the appointment is resiable Appointment.
            // id - String or Number which returns the appointment's ID.
            // subject - String which returns the appointment's subject.
            // location - String which returns the appointment's location.
            // description - String which returns the appointment's description.
            // tooltip - String which returns the appointment's tooltip.

            $("#log").html("appointmentDoubleClick is raised")

            console.log(a=appointment);
        });

        $("#scheduler").on('cellClick', function (event) {
            var args = event.args;
            var cell = args.cell;

            $("#log").html("cellClick is raised");
        });

        $("#scheduler").on('appointmentChange', function (event) {
            var args = event.args;
            var appointment = args.appointment;
            $('#jqxLoader').jqxLoader('open');
            $.post('/scp/ajax.php/tickets/<?php echo $_id ?>/'+appointment.id+'/save-calendar', {
                id: appointment.id,
                start_date: appointment.from.toString(),
                end_date: appointment.to.toString(),
                title: appointment.subject
            }, function (data) {
                $('#jqxLoader').jqxLoader('close');
            })

            // appointment fields
            // originalData - the bound data.
            // from - jqxDate object which returns when appointment starts.
            // to - jqxDate objet which returns when appointment ends.
            // status - String which returns the appointment's status("busy", "tentative", "outOfOffice", "free", "").
            // resourceId - String which returns the appointment's resouzeId
            // hidden - Boolean which returns whether the appointment is visible.
            // allDay - Boolean which returns whether the appointment is allDay Appointment.
            // resiable - Boolean which returns whether the appointment is resiable Appointment.
            // draggable - Boolean which returns whether the appointment is resiable Appointment.
            // id - String or Number which returns the appointment's ID.
            // subject - String which returns the appointment's subject.
            // location - String which returns the appointment's location.
            // description - String which returns the appointment's description.
            // tooltip - String which returns the appointment's tooltip.

            $("#log").html("appointmentChange is raised");
        });
    });
</script>
<script>
    (function($) {
        var elt = $('.ticket_tag');
        var last_req;

        if (!elt || !elt.length)
            return false;
        elt.tagsinput({
            tagClass: function(item) {
                return get_tag_label_class(item.length);
            },
            trimValue: true,
            typeaheadost: {
                source: function (typeahead, query) {
                    if(query.length > 2) {
                        if (last_req) last_req.abort();
                        last_req = $.ajax({
                            url: '/scp/ajax.php/tickets/tags/'+query,
                            dataType: 'json',
                            success: function (data) {
                                typeahead.process(data);
                            }
                        });
                    }
                }
            },
            freeInput: true
        });

        $('.bootstrap-tagsinput').hide();

        $('.manage_tags').off('click')
            .on('click', function() {
                $('.tag_items').fadeOut(function() {
                    $('.bootstrap-tagsinput').fadeIn();
                });
                $('.manage_tags').fadeOut(function() {
                    $('.done_manage_tags').fadeIn();
                });
            });

        $('.done_manage_tags').off('click')
            .on('click', function() {
                $('.done_manage_tags').text('Please wait...');
                items = $('.ticket_tag').tagsinput('items');
                $('.tag_items').html('');
                data = [];

                if (items && items.length) {
                    items.forEach(function(elm) {
                        data.push(elm);
                        $('.tag_items').append('<a href="#" class="'+get_tag_label_class(elm.length)+'">'+elm+'</a>');
                    });
                }

                $('.done_manage_tags, .bootstrap-tagsinput').prop('disabled', true);

                $.post('/scp/ajax.php/tickets/<?php echo $ticket->getId() ?>/tags', {
                    data
                }, function(result) {
                    $('.ticket_tag').tagsinput('removeAll');

                    if (result && result.length) {
                        result.forEach(function(elm) {
                            $('.ticket_tag').tagsinput('add', elm);
                        });
                    }

                    $('.done_manage_tags, .bootstrap-tagsinput').prop('disabled', false);
                    $('.done_manage_tags').text('Done');
                    $('.bootstrap-tagsinput').fadeOut(function() {
                        $('.tag_items').fadeIn();
                    });
                    $('.done_manage_tags').fadeOut(function() {
                        $('.manage_tags').fadeIn();
                    });
                })
            });

        function update_tags() {
            $('.mytag_class').each(function(index, elm) {
                $(elm).addClass(get_tag_label_class($(elm).html().length));
            });
            $('.mytag_class').removeClass('mytag_class');
        }
        update_tags();

        function get_tag_label_class(length) {
            switch (length%5) {
                case 1:
                    return 'label label-primary';
                case 2:
                    return 'label label-danger';
                case 3:
                    return 'label label-success';
                case 4:
                    return 'label label-default';
                case 5:
                    return 'label label-warning';
                default:
                    return 'label label-default';
            }
        }
    })(jQuery);
</script>

<script>
if (!isSafari) {
    (function($) {
        classx = '.thread-body';
        if ($(classx).length) {
            action = function (str, p1, p2, p3, offset, string) {
                return ' <b class="new_phonenumber">'+str+'</b> ';
            }

            action_old = function (str, p1, p2, p3, offset, string) {
                reg = new RegExp('[^0-9]', 'gi');
                reg_pre = new RegExp('^\\+84', 'gi');
                plain = str.replace(reg_pre, '0');
                plain = plain.replace(reg, '');

                if ('0' !== plain.charAt(0))
                    plain = '0'+plain;

                pre = plain.substr(0, 4);
                if (typeof window.phone_prefix[pre] !== 'undefined') {
                    new_number = window.phone_prefix[pre]+plain.substr(4);

                    return ' <b class="old_phonenumber">'+str+'</b> (số mới: '+new_number+') ';
                } else {
                    return ' <b class="old_phonenumber">'+str+'</b> ';
                }
            }

            $(classx).each(function(index, elm) {
                highlight_new_phonenumber(elm, action);
                highlight_old_phonenumber(elm, action_old);
            });
        }

        user_phonenumber_class = '[id*="phone"]';
        faded_title_class = '.faded.title';

        if ($(user_phonenumber_class).length) {
            action_simple = function(str) {
                reg = new RegExp('[^0-9]', 'gi');
                reg_pre = new RegExp('^\\+84', 'gi');
                plain = str.replace(reg_pre, '0');
                plain = plain.replace(reg, '');

                if ('0' !== plain.charAt(0))
                    plain = '0'+plain;

                pre = plain.substr(0, 4);
                if (typeof window.phone_prefix[pre] !== 'undefined') {
                    new_number = window.phone_prefix[pre]+plain.substr(4);
                    return str+' (số mới: '+new_number+')';
                } else {
                    return str;
                }
            }

            $(user_phonenumber_class).each(function(index, elm) {
                highlight_old_phonenumber(elm, action_simple);
            });
        }
        if ($(faded_title_class).length) {
            action_simple = function(str) {
                pre = str.substr(0, 4);
                if (typeof window.phone_prefix[pre] !== 'undefined') {
                    new_number = window.phone_prefix[pre]+str.substr(4);
                    return str+' (số mới: '+new_number+')';
                } else {
                    return str;
                }
            }

            $(faded_title_class).each(function(index, elm) {
                highlight_old_phonenumber(elm, action_simple);
            });

        }

        function highlight_new_phonenumber(classx, action) {
            result = $(classx).html();
            reg = new RegExp(window.phonenumber_querystr_new, 'gi');
            final_str = result.replace(reg, action);
            $(classx).html(final_str);
        }

        function highlight_old_phonenumber(classx, action) {
            result = $(classx).html();
            reg = new RegExp(window.phonenumber_querystr_old, 'gi');
            final_str = result.replace(reg, action);
            $(classx).html(final_str);
        }
    })(jQuery);
}
</script>

<script>
    (function($) {
        $('.thread-body>div').readmore({
            speed: 20,
            collapsedHeight: 200,
            moreLink: '<a href="#" class="read_more_link">Bấm để đọc tiếp</a>',
            lessLink: '<a href="#" class="read_more_link">Close</a>',
            blockCSS: 'display: block; width: 100%; margin-bottom: 1em !important;'
        });
    })(jQuery);
</script>

<?php
require('staff.inc.php');
if(!$thisstaff || !$thisstaff->getId() || !$thisstaff->can_view_ticket_analytics()) exit('Access denied');

$page = 'staff-booking.inc.php';
$nav->setTabActive('analytics');
$ost->addExtraHeader('<meta name="tip-namespace" content="analytics.StaffBooking" />',
    "$('#content').data('tipNamespace', 'analytics.StaffBooking');");

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.$page);
require_once(STAFFINC_DIR.'footer.inc.php');
ALTER TABLE `paper_require` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_job` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_marital` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_visa` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `passport_location` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `visa_type` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;

CREATE TABLE `osticket`.`tour_pax_visa_profile` ( `id` INT NOT NULL AUTO_INCREMENT , `paper_require` TINYINT NOT NULL , `job_status_id` TINYINT NOT NULL , `marital_status_id` TINYINT NOT NULL , `visa_status_id` TINYINT NOT NULL , `passport_location_id` TINYINT NOT NULL , `visa_type_id` TINYINT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `ost_pax_tour` ADD `tour_pax_visa_profile_id` INT NOT NULL AFTER `tl`;

ALTER TABLE `paper_require` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_job` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_marital` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_visa` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `passport_location` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `visa_type` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `tour_pax_visa_profile` CHANGE `paper_require` `paper_require` INT NOT NULL;
alter table visa_information drop column pax_id;
ALTER TABLE `visa_information` CHANGE `visa_profile_id` `tour_pax_visa_profile_id` INT(10) NOT NULL;
alter table visa_history drop column pax_id;
alter table visa_history change visa_profile_id `​​tour_pax_visa_profile_id` int(10) not null;
alter table visa_history change `​​tour_pax_visa_profile_id` tour_pax_visa_profile_id int(10) not null;

ALTER TABLE `visa_history` CHANGE `date_event` `date_event` DATETIME NOT NULL;
ALTER TABLE `status_job` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `status_marital` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `status_visa` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `passport_location` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `visa_type` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `tour_pax_visa_profile` CHANGE `paper_require` `paper_require` BIGINT UNSIGNED NOT NULL;
ALTER TABLE `visa_history` CHANGE `date_event` `date_event` DATE NOT NULL;

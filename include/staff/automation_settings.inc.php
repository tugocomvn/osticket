<link rel="stylesheet" type="text/css" href="<?php echo $cfg->getUrl() ?>scp/css/selectize.css" />
<style>
    form tr td:nth-child(2n+1) {
        padding-left: 2em;
    }

    form textarea {
        width: 90%;
        height: 5em;
        resize: none;
    }
</style>
<h2>Automation Config <small>List</small></h2>
<div class="clearfix"></div>
<div class="pull-right">
    <a href="<?php $cfg->getUrl() ?>/scp/automation_settings.php?action=add"
        class="btn_sm btn-info">Add New Content</a>
</div>
<div class="clearfix"></div>
<form action="<?php $cfg->getUrl() ?>/scp/automation_settings.php" method="get">
    <table>
        <tr>
            <td>Name</td>
            <td>
                <input type="text" name="name" class="input-field" placeholder="Name"
                       value="<?php if (isset($_REQUEST['name']) && trim($_REQUEST['name']))
                           echo trim($_REQUEST['name']) ?>">
            </td>
            <td>Description</td>
            <td>
                <input type="text" name="description" class="input-field" placeholder="Description"
                       value="<?php if (isset($_REQUEST['description']) && trim($_REQUEST['description']))
                           echo trim($_REQUEST['description']) ?>">
            </td>
            <td>Campaign</td>
            <td>
                <input type="text" name="campaign" class="input-field" placeholder="Campaign"
                       value="<?php if (isset($_REQUEST['campaign']) && trim($_REQUEST['campaign']))
                           echo trim($_REQUEST['campaign']) ?>">
            </td>
        </tr>
        <tr>
            <td>UTM Source</td>
            <td>
                <input type="text" name="utm_source" class="input-field" placeholder="UTM Source"
                       value="<?php if (isset($_REQUEST['utm_source']) && trim($_REQUEST['utm_source']))
                           echo trim($_REQUEST['utm_source']) ?>">
            </td>
            <td>UTM Medium</td>
            <td>
                <input type="text" name="utm_medium" class="input-field" placeholder="UTM Medium"
                       value="<?php if (isset($_REQUEST['utm_medium']) && trim($_REQUEST['utm_medium']))
                           echo trim($_REQUEST['utm_medium']) ?>">
            </td>
            <td>UTM Term</td>
            <td>
                <input type="text" name="utm_term" class="input-field" placeholder="UTM Term"
                       value="<?php if (isset($_REQUEST['utm_term']) && trim($_REQUEST['utm_term']))
                           echo trim($_REQUEST['utm_term']) ?>">
            </td>
            <td>UTM Content</td>
            <td>
                <input type="text" name="utm_content" class="input-field" placeholder="UTM Content"
                       value="<?php if (isset($_REQUEST['utm_content']) && trim($_REQUEST['utm_content']))
                           echo trim($_REQUEST['utm_content']) ?>">
            </td>
        </tr>
        <tr>
            <td>Schedule</td>
            <td>
                <select name="schedule" id="schedule" class="input-field">
                    <option value=>- All -</option>
                    <option value="now">Now</option>
                    <option value="one_time">One Time</option>
                    <option value="recursion">Recursion</option>
                </select>
            </td>
            <td>Type</td>
            <td>
                <select name="type" id="type" class="input-field">
                    <option value=>- All -</option>
                    <option value="sms">SMS</option>
                    <option value="email">Email</option>
                    <option value="notification">Notification</option>
                </select>
            </td>

            <td>Send From</td>
            <td>
                <input type="text" name="from" class="input-field dp" placeholder="Send From"
                       value="<?php if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from']))
                           echo date('d/m/Y', strtotime(trim($_REQUEST['from']))) ?>">
            </td>
            <td>Send To</td>
            <td>
                <input type="text" name="to" class="input-field dp" placeholder="Send To"
                       value="<?php if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to']))
                           echo date('d/m/Y', strtotime(trim($_REQUEST['to'])))  ?>">
            </td>
        </tr>
        <tr>
            <td colspan="2">Title/Subject/Content</td>
            <td colspan="6">
                <textarea name="content" id="content" class="input-field" placeholder="Title/Subject/Content"><?php
                    if (isset($_REQUEST['content']) && trim($_REQUEST['content']))
                        echo trim($_REQUEST['content']) ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p class="centered">
                    <button class="btn_sm btn-primary">Search</button>
                    <button class="btn_sm btn-default" type="reset">Reset</button>
                </p>
            </td>
        </tr>
    </table>
</form>

<table class="list" width="1050px">
<caption>
    <?php echo $pageNav->showing() ?>
</caption>
    <thead>
    <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Description</th>
        <th>Type</th>
        <th>Trigger</th>
        <th>Send After</th>
        <th>Schedule</th>
        <th>Action</th>
    </tr>
    </thead>

    <tbody>
    <?php $no = $offset; ?>
    <?php while($auto_action_content && ($row = db_fetch_array($auto_action_content))): ?>
        <tr>
            <td><?php echo ++$no; ?></td>
            <td><?php echo trim($row['name']) ?></td>
            <td>
                <?php echo trim($row['description']) ?>
            </td>
            <td>
                <?php echo $row['type'] ?>
            </td>
            <td><?php echo (isset($trigger_list[$row['trigger_id']]) ? $trigger_list[$row['trigger_id']] : '') ?></td>
            <td>
                <?php if (
                ( !isset($row['status']) || !$row['status'] )
                ): ?>
                <del>
                <?php endif; ?>
                <?php
                if ((int)$row['send_after'] && (int)$row['send_after'] %60 == 0)
                    echo ( (int)$row['send_after'] /60 ).' hrs';
                else
                    echo ((int)$row['send_after']) .' mins';
                ?>
                <?php if (
                ( !isset($row['status']) || !$row['status'] )
                ): ?>
                </del>
                <?php endif; ?>

            </td>
            <td>
                <?php echo $row['schedule'] ?>
            </td>
            <td>
                <a href="<?php $cfg->getUrl() ?>/scp/automation_settings.php?action=edit&id=<?php echo $row['id'] ?>"
                    class="btn_sm btn-xs btn-primary">
                    Edit
                </a>


                <form class="pull-right" action="<?php $cfg->getUrl()
                    ?>/scp/automation_settings.php"
                      method="post"
                >
                    <button class="btn_sm btn-xs btn-danger confirm ">Delete</button>
                    <input type="hidden" name="action" value="delete">
                    <?php csrf_token() ?>
                    <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                </form>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
</table>
<div> Page:
<?php echo $pageNav->getPageLinks(); ?>
</div>
<script type="text/javascript" src="<?php echo $cfg->getUrl() ?>scp/js/standalone/selectize.min.js"></script>
<script>
    (function($) {
        $('.confirm').off('click').on('click', function(e) {
            if (!confirm('Bạn có chắc chắn muốn xóa?')) {
                e.preventDefault();
                return false;
            }
        });
    })(jQuery);
</script>

CREATE TABLE booking_action
(
  booking_ticket_id INT PRIMARY KEY,
  email TINYINT
);
CREATE INDEX booking_action_email_index ON booking_action (email);

ALTER TABLE booking_action ADD staff_id INT NULL;
CREATE INDEX booking_action_staff_id_index ON booking_action (staff_id);
ALTER TABLE booking_action ADD action_time DATETIME NULL;
CREATE INDEX booking_action_action_time_index ON booking_action (action_time);
ALTER TABLE booking_action MODIFY email TINYINT(2);
create table account_view
(
  name varchar(32) not null,
  constraint account_view_pk
    primary key (name)
);

alter table account_spend_item
  add views varchar(256) null;


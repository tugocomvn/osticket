<?php
use function GuzzleHttp\json_encode;
use function GuzzleHttp\json_decode;

require_once(INCLUDE_DIR.'class.pax.php');

class TourAjaxAPI extends AjaxController {
    public static function loadDataFormat() {

        $data_array = $_POST['data'];

        $non_empty_rows = array_filter($data_array, 'implode');
        
        if(!isset($non_empty_rows) || empty($non_empty_rows)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Vui lòng điền thông tin chặng bay.',
                ]
            );
            exit;
        }
        // change name key 
        $__data = self::changeNameKey2dArray($non_empty_rows);

        foreach ($__data as $key => $value) {
            $data[] = self::format_values_in_form($value);
        }
        $out = array(
            'all_values' => $data,
            'result' => 1,
            'message' => 'Dữ liệu đã được xử lý từ hệ thống. Vui lòng kiểm tra và điều chỉnh kỹ thông tin trước khi nhấn nút "Confirm & Save"',
            'index' => range(-1,2),
        );
        echo json_encode($out);
        exit;
    }
    
    public static function changeNameKey2dArray($arr)
    {
        if(!array($arr) || !count($arr)) return null;
        $second_level = array('flight_ticket_code', 'departure_date', 'flight_code', 'airport_code_from_to', 'flight_time');
        for($d = 0; $d < count($arr); $d++ )
        {
            for($s = 0; $s < count($arr[$d]); $s++ )
            {
                $output[$d][$second_level[$s]] = $arr[$d][$s];
            }
        }
        return $output;
    }

    public static function format_values_in_form($arr) {

        if(empty($arr)) return null;
        $_data = [];
        $airport_code_from_to = $arr['airport_code_from_to'];
        $flight_code = $arr['flight_code'];
        $flight_ticket_code = $arr['flight_ticket_code'];
        if(isset($airport_code_from_to)) {
            $_data = [
                "airport_code_from_to" => trim($airport_code_from_to),
                "flight_code" => trim($flight_code),
                "flight_ticket_code" => trim($flight_ticket_code),
            ];
        }

        $departure_date = $arr['departure_date'];
        $time = $arr['flight_time'];
        $date = self::format_date($departure_date);
        $time_from_arr = self::format_time($time);
        foreach ($time_from_arr as $key => $value) {
            $_time[] = self::timeFormat($value);
        }
        if(isset($date) && isset($time)) {
            $mer_data = array_merge($_data, [
                "departure_at" => date("d/m/Y", strtotime($date)),
                "departure_time" => date("H:i", strtotime($date." ".$_time[0])),
            ]);
        }   
        $time_to = self::format_time_to($time, $date);
        if(isset($time_to) && isset($date)){
            $mer_data = array_merge($mer_data, [
                "arrival_at" => date("d/m/Y", strtotime($time_to)),
                "arrival_time" => date("H:i", strtotime($time_to)),
            ]);
        }
        if(!$mer_data) return null;
        return $mer_data;
    }

    public static function format_time_to($time_str, $date_str)
    {
        if(!isset($time_str) && !isset($date_str)) return null;
        $parentheses = "(";
        $plus_day = "+";
        if (stripos($time_str, $plus_day) !== false || stripos($time_str, $parentheses) !== false) {
            $time_to = self::format_time($time_str);
            $_time_to = self::timeFormat($time_to[1]);
            $plus_date = date('Y-m-d', strtotime($date_str) + (24*60*60));
            return $plus_date." ".$_time_to;
        }else{
            $time_to = self::format_time($time_str);
            return $date_str." ".$time_to[1];
        }
    }

    public static function timeFormat($time)
    {
        if(!isset($time)) return null;
            preg_match_all('/(2?[0-3]|[01]?[0-9]):[0-5][0-9]/', $time, $match);
        return $match[0][0];
    }

    public static function format_date($date_str)
    {
        if(!isset($date_str)) return null;
        return date('Y-m-d', strtotime($date_str));
    }

    public static function format_time($time_str)
    {
        if(!isset($time_str)) return null;
        $exp_time_str = explode("-", $time_str);
        return $exp_time_str;
    }

    public static function get_values_airport_code_from_to($str_airport_code)
    {
        if(empty($str_airport_code)) return null;
        $exp_airport_code = explode('-', $str_airport_code);
        if(is_array($exp_airport_code)){
            $name_key_ariport = [
                'airport_code_from',
                'airport_code_to'
            ];
            return array_combine($name_key_ariport, $exp_airport_code);
        }
    }

    public static function saveTourFlightTicket(){
        global $thisstaff;  
        if(!isset($_POST['destination']) || empty($_POST['destination'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn điểm đến.',
                ]
            );
            exit;
        }

        if(!isset($_POST['airline_id']) || empty($_POST['airline_id'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn hãng bay.',
                ]
            );
            exit;
        }

        if(!isset($_POST['gateway_present_time']) || empty($_POST['gateway_present_time']) || !isset($_POST['gateway_present_time_']) || empty($_POST['gateway_present_time_'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn giờ và ngày tập trung.',
                ]
            );
            exit;
        }

        if(!isset($_POST['departure_date']) || empty($_POST['departure_date'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn ngày khởi hành.',
                ]
            );
            exit;
        }

        if(!isset($_POST['return_time']) || empty($_POST['return_time'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn ngày về.',
                ]
            );
            exit;
        }

        if(!isset($_POST['tour_name']) || empty($_POST['tour_name'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Dòng Tour Name không được để trống.',
                ]
            );
            exit;
        }
    
        if(!isset($_POST['pax_quantity']) || empty($_POST['pax_quantity'])){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn số khách tối đa.',
                ]
            );
            exit;
        }
        $data_array = $_POST['data'];

        $non_empty_rows = array_filter($data_array, 'implode');

        $__data = self::changeNameKey2dArray($non_empty_rows);

        $gateway_present_time = date_create_from_format('d/m/Y', $_POST['gateway_present_time'])->format('Y-m-d');
        $gateway_present_time_ = date_create_from_format('H:i', $_POST['gateway_present_time_'])->format('H:i');
        $_data = [
            'name'                    => trim($_POST['tour_name']),
            'destination'             => (int)$_POST['destination'],
            'gateway_present_time'    => date_create_from_format('Y-m-d H:i', $gateway_present_time." ".$gateway_present_time_)->format('Y-m-d H:i'),
            'return_time'             => date_create_from_format('d/m/Y', $_POST['return_time'])->format('Y-m-d'),
            'created_by'              => $thisstaff->getId(),
            'created_at'              => date('Y-m-d H:i:s'),
            'tour_leader_id'          => (int)$_POST['tour_leader_id'],
            'transit_airport_id'      => (int)$_POST['transit_airport'],
            'retail_price'            => $_POST['retail_price'],
            'net_price'               => $_POST['net_price'],
            'airline_id'              => $_POST['airline_id'],
            'maximum_discount'        => $_POST['maximum_discount'],
            'gateway_id'              => (int)$_POST['gateway_id'],
            'stimulus_tour'           => (int)$_POST['stimulus_tour'],
            'pax_quantity'            => (int)$_POST['pax_quantity'],
            'note'                    => $_POST['note'],
            'tl_quantity'             => (int)$_POST['tl_quantity'],
            'color_code'              => $_POST['color'],
            'departure_date'          => date_create_from_format('d/m/Y', $_POST['departure_date'])->format('Y-m-d'),
        ];
        $sql = "SELECT name FROM ".TOUR_NEW_TABLE." WHERE name LIKE ".db_input(trim($_data['name']));
        $duplicate = db_query($sql);
        if (mysqli_num_rows($duplicate) > 0)
        {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Duplicate Tour Name.',
                ]
            );
            exit;
        }
        $new_tour = TourNew::create($_data);
        $tour_id = $new_tour->save(true); 
        if($tour_id){
            TourNetPrice::save_tour_net_price($_POST['net_price'], $tour_id);
        }
        $log = TourLogOP::create(
            [
                'tour_id' => $tour_id,
                'content' => json_encode($new_tour->ht),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $thisstaff->getId()
            ]
        );
        $log->save(true);
        $new = self::save_tour_flight_ticket($__data, $tour_id);

        if(!isset($new) || !empty($new)){
            echo json_encode(
                [
                    'result' => 1,                    
                    'message' => 'Đã lưu thành công trang sẽ tự quay về trang danh sách.',
                ]
            );
            exit;
        }
      
    }
    public static function save_tour_flight_ticket($data, $tour_id) {
        global $thisstaff;
        if(empty($data) || empty($tour_id)) return null;
        $tour = TourNew::lookup($tour_id);
        if(isset($tour)){
            foreach ($data as $key => $value) {
                $data = self::get_values_in_form_import($value);
                $_data_flight = [
                    'airport_code_from' => $data['airport_code_from'],
                    'airport_code_to' => $data['airport_code_to'],
                    'airline_id' => $tour->airline_id,
                    'flight_code' => $value['flight_code'],
                    'departure_at' => $data['departure_at'],
                    'arrival_at' => $data['arrival_at'],
                    'created_at' => date('Y-m-d H:i:s'), 
                    'created_by' => $thisstaff->getId(), 
                ]; 
                $new_flight = FlightOP::create($_data_flight);
                $flight_id = $new_flight->save(true);
                $log = FlightLogOP::create(
                    [
                        'flight_id' => $flight_id,
                        'content' => json_encode($new_flight->ht),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $thisstaff->getId()
                    ]
                );
                $log->save(true);
                $_data_flight_ticket = [
                    'flight_ticket_code' => $value['flight_ticket_code'],
                    'total_quantity' => $tour->pax_quantity,
                    'net_price' => $tour->net_price,
                    'retail_price' => $tour->retail_price
                ];
                $new_flight_ticket = FlightTicketOP::create($_data_flight_ticket);
                $flight_ticket_id = $new_flight_ticket->save(true);
                $log = FlightTicketLogTR::create(
                    [
                        'flight_ticket_id' => $flight_ticket_id,
                        'content' => json_encode($new_flight_ticket->ht),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $thisstaff->getId()
                    ]
                );
                $log->save(true);
                $id_flight_ticket_flight = [
                    'flight_ticket_id' => $flight_ticket_id,
                    'flight_id' => $flight_id
                ];
                $flight_ticket_flight = FlightTicketFlightOP::create($id_flight_ticket_flight);
                $flight_ticket_flight->save(true);
                $data_flight_ticket_tour = [
                    'flight_ticket_id' => $flight_ticket_id,
                    'tour_id' => $tour_id
                ];
                $new_flight_ticket_tour = FlightTicketTourOP::create($data_flight_ticket_tour);
                $new_flight_ticket_tour->save('true');
                if(!isset($new_flight_ticket_tour)) return null;
            }
            return $new_flight_ticket_tour;
        }
    }

    public static function getBookingName($booking_type)
    {
        $sql = "SELECT booking_type FROM booking_tmp WHERE booking_type_id=".db_input($booking_type);
        $res = db_query($sql);
        if(!isset($res)) return null;
        $row = db_fetch_array($res);
        if(!isset($row)) return null;
        $booking_name_decode = _String::json_decode($row['booking_type']);
        return $booking_name_decode;
    }

    public static function get_values_in_form_import($arr) {
        if(empty($arr)) return null;
        $_data = [];
        $airport_code_from_to = $arr['airport_code_from_to'];
        if(isset($airport_code_from_to)) {
            $airport_code_arr = self::get_values_airport_code_from_to($airport_code_from_to);
            $_data = [
                "flight_ticket_code" => $arr['flight_ticket_code'],
                "airport_code_from" => $airport_code_arr['airport_code_from'],
                "airport_code_to" => $airport_code_arr['airport_code_to'],
            ];
        }
        $departure_date = $arr['departure_date'];
        $time = $arr['flight_time'];
        $date = self::format_date($departure_date);
        $time_from_arr = self::format_time($time);
        foreach ($time_from_arr as $key => $value) {
            $_time[] = self::timeFormat($value);
        }
        if(isset($date) && isset($time)) {
            $mer_data = array_merge($_data, [
                "departure_at" => $date." ".$_time[0],
            ]);
        }   
        $time_to = self::format_time_to($time, $date);
        if(isset($time_to) && isset($date)){
            $mer_data = array_merge($mer_data, [
                "arrival_at" => $time_to,
            ]);
        }
        if(!$mer_data) return null;
        return $mer_data;
    }

    public static function requestEditTour(){
        global $thisstaff;
        if(!$thisstaff->canRequestEditTour()){
            echo json_encode(
                [
                    'result'  => (int)$_POST['tour_id'],
                    'message' => 'Không có quyền chỉnh sừa tour',
                ]
            );
        exit;
        }

        $tour = TourNew::lookup((int)$_POST['tour_id']);
        if(!$tour) {
            echo json_encode(
                [
                    'result'  => (int)$_POST['tour_id'],
                    'message' => 'Không tìm thấy tour này.',
                ]
            );
            exit;
        }
        if(isset($tour->status_edit_tour) && (int)$tour->status_edit_tour === 0
            && $tour->status_edit_tour !== TourNew::APPROVAL_EDIT_TOUR){
            $tour->set('status_edit_tour', TourNew::REQUEST_EDIT_TOUR);
            $tour->save();
            echo json_encode(
                [
                    'result'  => 1,
                    'message' => 'Đã gửi yêu cầu thành công!',
                ]
            );
            exit;
        }

        echo json_encode(
            [
                'result'  => 0,
                'message' => 'Không thể gửi request này.',
            ]
        );
        exit;
    }

    public static function approvalEditTour(){
        global $thisstaff;
        if(!$thisstaff->canApproveEditTour()){
            echo json_encode(
                [
                    'result'  => (int)$_POST['tour_id'],
                    'message' => 'Không có quyền phê duyệt tour',
                ]
            );
            exit;
        }

        $tour = TourNew::lookup((int)$_POST['tour_id']);
        if(!$tour) {
            echo json_encode(
                [
                    'result'  => (int)$_POST['tour_id'],
                    'message' => 'Không tìm thấy tour này.',
                ]
            );
            exit;
        }
        if(isset($tour->status_edit_tour) && (int)$tour->status_edit_tour !== 0
            && (int)$tour->status_edit_tour === TourNew::REQUEST_EDIT_TOUR){
            $tour->set('status_edit_tour', TourNew::APPROVAL_EDIT_TOUR);
            $tour->save();
            echo json_encode(
                [
                    'result'  => 1,
                    'message' => 'Đã phê duyệt thành công!',
                ]
            );
            exit;
        }

        echo json_encode(
            [
                'result'  => 0,
                'message' => 'Không thể phê duyệt.',
            ]
        );
        exit;
    }
}
?>

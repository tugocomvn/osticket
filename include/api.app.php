<?php
include_once INCLUDE_DIR.'../vendor/autoload.php';
include_once INCLUDE_DIR.'tugo.user.php';
include_once INCLUDE_DIR.'tugo.api.php';
include_once INCLUDE_DIR.'tugo.news.php';
include_once INCLUDE_DIR.'class.address.php';

use \Tugo\Token;
use \Tugo\User;
use \Tugo\App;
use \Tugo\News;

class AppApiController extends ApiController
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Expires: Sun, 01 Jan 1991 00:00:00 GMT');
    }

    public function register()
    {
        try {
            $data = $this->getRequest('json');

            if (!isset($data['phone_number'])) {
                throw new Exception('Chưa nhập số điện thoại', 1001);
            }

            if (!isset($data['device_id'])) {
                throw new Exception('Chưa có mã thiết bị', 1002);
            }

            if (!isset($data['customer_name'])) {
                throw new Exception('Chưa nhập tên khách hàng', 1003);
            }

            $user = User::register($data['phone_number'], $data['customer_name'], $data['device_id']);
            if (!$user) {
                throw new Exception('Đăng ký chưa thành công, vui lòng thử lại', 1005);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function login()
    {
        try {
            $data = $this->getRequest('json');

            if (!isset($data['phone_number'])) {
                throw new Exception('Chưa nhập số điện thoại', 1001);
            }

            if (!isset($data['device_id'])) {
                throw new Exception('Chưa có mã thiết bị', 1002);
            }

            if (!isset($data['otp'])) {
                throw new Exception('Chưa có mã OTP', 1017);
            }

            $cloud_message_token = isset($data['cloud_message_token']) && !empty($data['cloud_message_token']) ?
                trim($data['cloud_message_token']) : null;

            $user = User::login($data['phone_number'], $data['device_id'], $data['otp'], $cloud_message_token);
            if (!$user) {
                throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại', 1004);
            }

            $token = Token::getNewToken($user, $data['device_id']);

            if (!$token) {
                throw new Exception('Lỗi tạo mã token', 1008);
            }

            $refresh_token = Token::getNewRefreshToken($user['uuid'], $data['device_id']);

            if (!$refresh_token) {
                throw new Exception('Lỗi tạo mã refresh token', 1009);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => [
                        'user'          => $user,
                        'token'         => $token,
                        'refresh_token' => $refresh_token,
                    ]
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function refresh()
    {
        try {
            $data = $this->getRequest('json');

            if (!isset($data['refresh_token'])) {
                throw new Exception('Chưa có mã refresh_token', 1007);
            }

            if (!isset($data['uuid']) && !isset($data['phone_number'])) {
                throw new Exception('Chưa có thông tin khách hàng', 1010);
            }

            if (!isset($data['device_id'])) {
                throw new Exception('Chưa có mã thiết bị', 1002);
            }

            if (!isset($data['phone_number'])) {
                $data['phone_number'] = null;
            }
            if (!isset($data['uuid'])) {
                $data['uuid'] = null;
            }

            $token = User::refresh($data['uuid'], $data['phone_number'], $data['device_id'], $data['refresh_token']);

            if (!$token) {
                throw new Exception('Lỗi tạo mã token', 1008);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => [
                        'token' => $token,
                    ]
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function logout()
    {
        try {
            $data = $this->getRequest('json');
            try {
                $payload = Token::verifyToken($data);
            } catch (Exception $ex) {
                $payload = Token::decode($data, config('signing_secret'), array(config('signing_algorithm')));
            }

            if (!isset($data['device_id'])) {
                throw new Exception('Chưa có mã thiết bị', 1002);
            }

            $user = User::logout($payload, $data['device_id']);

            if (!$user) {
                throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại', 1004);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function qrcode()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);
            $qrcode = User::generateQRCode($payload);
            if (!$qrcode) {
                throw new Exception('Không tạo được QR Code', 1011);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => [
                        'qrcode' => $qrcode
                    ]
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function feedback()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);

            if (!isset($data['request_content'])) {
                throw new Exception('Chưa có nội dung', 1013);
            }

            $email = isset($data['email']) && $data['email'] ? $data['email'] : "";

            $res = User::sendRequest($payload, $email, $data['request_content'], "FEEDBACK");
            if (!$res) {
                throw new Exception('Không gửi được, vui lòng thử lại', 1018);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function request()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);

            if (!isset($data['request_content'])) {
                throw new Exception('Chưa có nội dung', 1013);
            }

            $email = isset($data['email']) && $data['email'] ? $data['email'] : "";

            $res = User::sendRequest($payload, $email, $data['request_content'], "REQUEST");
            if (!$res) {
                throw new Exception('Không gửi được, vui lòng thử lại', 1018);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function otp()
    {
        try {
            $data = $this->getRequest('json');

            if (!isset($data['phone_number'])) {
                throw new Exception('Chưa có số điện thoại', 1001);
            }

            if (!isset($data['device_id'])) {
                throw new Exception('Chưa có mã thiết bị', 1002);
            }

            if (!isset($data['checksum'])) {
                throw new Exception('Chưa có mã checksum', 1015);
            }

            $string = trim($data['phone_number']) . config('sms_secret') . trim($data['device_id']);
            $checksum = hash('sha256', $string);

            if ($checksum != trim($data['checksum'])) {
                throw new Exception('Dữ liệu không hợp lệ', 1016);
            }

            $user = User::get(['phone_number' => $data['phone_number']]);
            if (!$user) {
                throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại', 1004);
            }

            if (!App::sendOTP($data['phone_number'])) {
                throw new Exception('Lỗi gửi OTP', 1014);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function pointHistory()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);
            $page = isset($data['page']) ? abs((int)$data['page']) : 1;
            $page = $page ?: 1;
            $limit = 10;
            $total = 0;
            $_data = User::getPointHistory($payload, $total, $page, $limit);

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $_data,
                    'total'  => $total,
                    'page'   => $page,
                    'limit'  => $limit,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function news()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);

            $page = isset($data['page']) ? abs((int)$data['page']) : 1;
            $page = $page ?: 1;

            $payload = (array)$payload;
            if (!$payload || !isset($payload['uuid'])) {
                throw new Exception('Không tìm thấy thông tin khách', 1518);
            }

            $limit = 10;
            $params = [];
            $params['app_uuid'] = $payload['uuid'];
            $offset = ($page - 1) * $limit;
            $total = 0;

            $point = \Tugo\News::getPagination($params, $offset, $limit, $total);
            $data = [];
            while (($row = db_fetch_array($point))) {
                $data[] = [
                    'id'      => $row['id'],
                    'title'   => $row['title'],
                    'content' => $row['content'],
                ];
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $data,
                    'total'  => $total,
                    'page'   => $page,
                    'limit'  => $limit,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function profile()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);
            $user = User::getProfile($payload);

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $user,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function updateProfile()
    {
        try {
            $data = $this->getRequest('json');
            $payload = Token::verifyToken($data);
            if (!isset($data['customer_name'])) {
                throw new Exception('Chưa nhập tên khách hàng', 1003);
            }

            $user = User::update($payload, $data['customer_name']);

            if (!$user) {
                throw new Exception('Cập nhật bị lỗi.', 1019);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $user,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response(
                $ex->getCode() ?: 1999,
                json_encode([
                    'status'  => 'Error',
                    'message' => $ex->getMessage(),
                    'code'    => $ex->getCode() ?: 1999
                ]),
                'text/json'
            );
            exit;
        }
    }

    public function notification()
    {
        Http::response(
            200,
            json_encode([
                'status' => 'Success',
                'code'   => 1,
            ]),
            'text/json'
        );
    }

    public function verifyOTP()
    {
        try {
            $data = $this->getRequest('json');
            $id = (int)trim($data['id']);
            $type = trim($data['type']);

            $user = User::get(['phone_number' => $data['phone_number']]);
            if ($user) {
                //send OTP
                App::sendOTP($data['phone_number']);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function pointHistoryOTP()
    {
        try {
            $data = $this->getRequest('json');
            $user = User::get(['phone_number' => $data['phone_number']]);
            if (!$data || !isset($data['otp'])
                || !$user
                || !App::verifyOTP($data['phone_number'], $data['otp'])) {
                throw new Exception('OTP không đúng.', 1523);
            }

            $page = 1;
            $limit = 1000;
            $total = 0;

            $_data = User::getPointHistory($user, $total, $page, $limit);

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code' => 1,
                    'data' => $data,
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999, json_encode([
                'status'  => 'Error',
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode() ?: 1999
            ]), 'text/json');
            exit;
        }
    }

    public function membership()
    {
        try {
            /** TODO: trường hợp ai đó có số điện thoại của khách khác, hoặc có danh sách số điện thoại, có thể dò ra thông tin khách bằng cách thay đổi request phone_number **/
            $data = $this->getRequest('json');
            if (!isset($data['phone_number'])) {
                throw new Exception('Chưa có số điện thoại', 1001);
            }

            $user = User::get(['phone_number' => $data['phone_number']]);
            if (!$user) {
                throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại', 1004);
            }

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $user
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999,
                json_encode([
                    'status'  => 'Error',
                    'message' => $ex->getMessage(),
                    'code'    => $ex->getCode() ?: 1999
                ]), 'text/json');
            exit;
        }
    }

    public function totalPointOTP()
    {
        try {
            $data = $this->getRequest('json');
            if (!isset($data['phone_number'])) {
                throw new Exception('Chưa có số điện thoại', 1001);
            }

            $user = User::get(['phone_number' => $data['phone_number']]);
            if (!$user) {
                throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại', 1004);
            }

            if (!App::verifyOTP($data['phone_number'], $data['otp'])) {
                throw new Exception('OTP không đúng.', 1523);
            }
            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code'   => 1,
                    'data'   => $user['user_point']
                ]),
                'text/json'
            );
        } catch (Exception $ex) {
            Http::response($ex->getCode() ?: 1999,
                json_encode(['status'  => 'Error',
                             'message' => $ex->getMessage(),
                             'code'    => $ex->getCode() ?: 1999
                ]), 'text/json');
            exit;
        }
    }

    function getAddress(){
        try{
            $data = $this->getRequest('json');
            $id = (int)trim($data['id']);
            $type = trim($data['type']);

            if(!$id || !$type)
                throw new Exception('Không tìm thấy thông tin', 1003);

            if($type === 'district'){
                $results = DistrictAddress::getItem($id);
            }elseif($type === 'ward'){
                $results = WardAddress::getItem($id);
            }
            if(!isset($results) || !$results)
                throw new Exception('Không tìm thấy thông tin', 1003);
            $data = [];
            while ($results && ($row = db_fetch_array($results)))
            {
                $data[] = [
                    'id' => $row['id'],
                    'name' => $row['name']
                ];
            }
            if(empty($data))
                throw new Exception('Không tìm thấy thông tin', 1003);

            Http::response(
                200,
                json_encode([
                    'status' => 'Success',
                    'code' => 1,
                    'data' => $data,
                ]),
                'text/json'
            );
        }catch (Exception $ex) {
            Http::response(
                $ex->getCode() ?: 1999,
                json_encode([
                    'status' => 'Error',
                    'message' => $ex->getMessage(),
                    'code' => $ex->getCode() ?: 1999
                ]),
                'text/json'
            );
            exit;
        }
    }
}

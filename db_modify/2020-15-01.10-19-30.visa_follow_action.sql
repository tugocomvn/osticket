/*branch: visa */

alter table visa_follow_action
    add staff_id int unsigned default 0 not null;

create index visa_follow_action_staff_id_index on visa_follow_action(staff_id)	
	
	
<script>
    (function(w,d,s,c){
        a=d.createElement(s),
        e=d.getElementsByTagName(s)[0];
        a.async=1;
        a.src=c;
        e.parentNode.insertBefore(a,e);
        a.addEventListener('load', function () {
            function makeid() {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                for (var i = 0; i < 10; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                return text;
            }

            cookie = Cookies.get('__my_ost_uuid');
            if (!cookie) {
                var time = new Date();
                time_iso = time.toISOString();
                var id = makeid();
                var agent = navigator.userAgent;
                hash = md5(time_iso+id+agent);
                Cookies.set('<?php echo _UUID::getName() ?>', hash, { expires: <?php echo _UUID::getExpiredTime() ?>, path: '/' });
            }
        }, false);
    })(window,document,'script','/scp/js/myost.js?v=1.1');
</script>
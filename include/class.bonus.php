<?php
// If you installed via composer, just use this code to require autoloader on the top of your projects.
require_once ROOT_DIR.'vendor/autoload.php';
// Using Medoo namespace
use Medoo\Medoo;

class BonusCheck
{

    private static $database = null;
    // Initialize
    private static function init_connet(){
        if (self::$database === null) {
            self::$database = new Medoo(['database_type' => DBTYPE,
                'database_name' => DBNAME,
                'server' => DBHOST,
                'username' => DBUSER,
                'password' => DBPASS,
                "charset" => "utf8",
                "logging" => true,
            ]);
        }
    }

    public static function get_Table_api_user(){
        self::init_connet();
        return self::$database->select('api_user','*',null);
    }
    public static function get_report(){
        self::init_connet();
        $data =  self::$database->select(COMMISSION_REPORT_TABLE,
        [
            'staff_create_id',
            'staff_id_of_report',
            'month_of_report',
            'year_of_report',
            'date_create',
            'note',
            'id'
        ]);
        return $data;
        //return self::$database->select('report', '*');
    }
    public static function get_report_from_ID(){
        self::init_connet();
        $data =  self::$database->select(COMMISSION_REPORT_TABLE,
            [
                'staff_create_id',
                'staff_id_of_report',
                'month_of_report',
                'year_of_report',
                'date_create',
                'note',
                'id'
            ]);
        return $data;
        //return self::$database->select('report', '*');
    }
    public static function getData()
    {
        $sql = "SELECT * FROM ".COMMISSION_BOOKING_TABLE;
        return db_query($sql);
    }
    public static function get_staff(){
        self::init_connet();
        return self::$database->select('ost_staff',['staff_id','firstname', 'lastname'],
            ['ORDER' => 'firstname']);
    }
    public static function insert_report($month,$year,$agent,$id,$note){
        self::init_connet();
        self::$database->insert(COMMISSION_REPORT_TABLE, [
            "month_of_report" => $month,
            "year_of_report" => $year,
            "staff_id_of_report" => $agent,
            "staff_create_id" => $id,
            "note" => $note
        ]);
        $report_id = self:: $database->id();
        return $report_id;
    }
    public static function insert_booking($booking_code,$total_retail_price,$total_passenger,$user_id,$report_id,$note){
        self::init_connet();
        self::$database->insert(COMMISSION_BOOKING_TABLE, [
            "booking_code" => $booking_code,
            "total_retail_price" => $total_retail_price,
            "total_quantity" => $total_passenger,
            "user_id" => $user_id,
            "report_id" => $report_id,
            "note" => $note
        ]);
        $report_id = self:: $database->id();
        return $report_id;
    }
    public static function get_Booking($code)
    {
           $sql =  "SELECT `booking_view`.`total_retail_price`,`booking_view`.`total_quantity`,`booking_view`.`booking_code`,
                    `booking_view`.`staff`,`booking_view`.`staff_id`,`ccb`.`report_id`,`ccb`.`note` 
                    FROM `booking_view`
                    LEFT JOIN ".COMMISSION_BOOKING_TABLE." as ccb ON `booking_view`.`booking_code_trim` 
                                LIKE trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM `ccb`.booking_code) ) 
                    LEFT JOIN ".COMMISSION_REPORT_TABLE." as cc ON `ccb`.`report_id` = `cc`.`id` 
                            WHERE `booking_view`.`booking_code_trim` IN ".$code;
           $res = db_query($sql);
           $results = array();
           while ($res && $row = db_fetch_array($res))
           {
               array_push($results,$row);
           }
            return $results;
    }
    public static function get_Booking_from_Report($id)
    {
        self::init_connet();
        $data = self::$database->select(COMMISSION_BOOKING_TABLE,
            [
                "[>]".COMMISSION_REPORT_TABLE => [COMMISSION_BOOKING_TABLE.".report_id" => "id"],
            ],
            [
            'booking_id',
            'total_retail_price',
            'total_quantity',
            'booking_code',
            'user_id',
            'report_id',
            'month_of_report',
            'year_of_report',
            COMMISSION_BOOKING_TABLE.'.note',
        ],[
            'report_id' => $id
        ]);
        //var_dump(self::$database->log());
        return $data;
    }
    public static function insert_report_booking($booking_code,$total_retail_price,$total_passenger,$user_id,$report_id,$note)
    {
        $sql = sprintf("INSERT INTO ".COMMISSION_BOOKING_TABLE."(booking_code,total_retail_price,total_quantity,user_id,report_id,note)
                                VALUES(%s,%d,%d,%d,%d,%s) ON DUPLICATE KEY UPDATE note = %s"
                                ,db_input($booking_code),$total_retail_price,$total_passenger,$user_id,$report_id,db_input($note),db_input($note));
        db_query($sql);
    }
    public static function update_note_booking($booking_id,$note)
    {
        $sql = sprintf("UPDATE ".COMMISSION_BOOKING_TABLE." SET `note`= N'%s' WHERE booking_id= %d",$note,$booking_id);
        db_query($sql);
    }
}


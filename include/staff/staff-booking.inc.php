<?php
if(!defined('OSTSCPINC') || !$thisstaff ) die('Access Denied');
?>
<style>
    iframe {
        width: 90%;
        height: 850px;
        margin: auto;
        border: none;
    }

    #container {
        width: 99%;
    }
    #content {
        text-align: center;
    }
</style>

<h2>Staff Booking</h2>
<div>
    <iframe width="600" height="338" src="https://datastudio.google.com/embed/reporting/cd32490f-a19a-4abf-923e-616ed2ed2705/page/98f6" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

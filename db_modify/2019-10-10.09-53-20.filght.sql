create table flight
(
  id int auto_increment primary key,
  departure_flight_code   varchar(10) null,
  return_flight_code   varchar(10) null,
  departure_flight_code_b   varchar(10) null,
  return_flight_code_b   varchar(10) null,
  flight_time_stage_1 datetime null,
  flight_time_arrives_stage_1 datetime null,
  flight_time_stage_2 datetime null,
  flight_time_arrives_stage_2 datetime null,
  flight_time_stage_1_b datetime null,
  flight_time_arrives_stage_1_b datetime null,
  flight_time_stage_2_b datetime null,
  flight_time_arrives_stage_2_b datetime null
);
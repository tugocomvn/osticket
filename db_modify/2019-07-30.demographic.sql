CREATE TABLE `demographic_age` (
  `gather_date` date NOT NULL,
  `age` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_age`
  ADD PRIMARY KEY (`gather_date`,`age`);
  
CREATE TABLE `demographic_age_group` (
  `gather_date` date NOT NULL,
  `0_25` int(11) NOT NULL,
  `25_35` int(11) NOT NULL,
  `35_55` int(11) NOT NULL,
  `older_55` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_age_group`
  ADD PRIMARY KEY (`gather_date`);
  
CREATE TABLE `demographic_gender` (
  `gather_date` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_gender`
  ADD PRIMARY KEY (`gather_date`,`gender`);
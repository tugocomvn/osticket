<?php

$display_info_list = [
    'airline',
    'gather_date',
];
$tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
$flight_code = FlightOP::searchFlightCodeFromTour((int)$tour->id);
$departure_flight_code = '';
$return_flight_code = '';
if(count($flight_code) === 4){
    $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
    $return_flight_code = $flight_code[2].'-'.$flight_code[3];
}elseif(count($flight_code) === 2){
    $departure_flight_code = $flight_code[0];
    $return_flight_code = $flight_code[1];
} ?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <h4>Operator <small>Upload Passport Main Page</small></h4>
        <h5>Tour: <?php echo $tour->name ?></h5>
    </div>
    <div class="col-md-6">
        <div>
            <h3>Tour Info:</h3>
            <?php
            $airlines_list = ListUtil::getList(AIRLINE_LIST);
            $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
            $flight_code = FlightOP::searchFlightCodeFromTour((int)$tour->id);
            $departure_flight_code = '';
            $return_flight_code = '';
            if(count($flight_code) === 4){
                $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
                $return_flight_code = $flight_code[2].'-'.$flight_code[3];
            }elseif(count($flight_code) === 2){
                $departure_flight_code = $flight_code[0];
                $return_flight_code = $flight_code[1];
            } ?>
            <br />
            <span>Airline: <?php echo $airlines_list[$tour->airline_id] ?></span>
            <br>
            <span>Departure flight code: <?php echo $departure_flight_code ?> </span>
            <br>
            <span>Return flight code: <?php echo $return_flight_code ?> </span>
            <br>
            <span>Tour leader: <?php if($tour_leader) echo $tour_leader->getValue() ?></span>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card mb-4">
            <div class="row no-gutters">
                <div class="col-md-8">
                    <div class="card-body">
                        <p>Điền mã booking, sau đó chọn ảnh chụp passport.</p>
                        <p>Chú ý: Mỗi lần chụp 01 mặt passport. Chụp gần, rõ nét, chú ý lấy đủ phần code ở cuối trang (2 dòng có dấu &lt;&lt;&lt;&lt;&lt;&lt;&lt;)</p>
                        <div class="form-group">
                            <label for="" class="d-block d-sm-none">Booking Code</label>
                            <div class="input-group">
                                <div class="input-group-prepend d-none d-sm-block">
                                    <div class="input-group-text">Booking Code</div>
                                </div>
                                <input type="text" class="form-control booking_code" id="" placeholder="" value="TG123-">
                                <input type="hidden" class="form-control tour_id" id="" value="<?php echo $_REQUEST['tour'] ?>">
                            </div>
                        </div>
                        <p><small class="booking_quantity"></small></p>
                        <div class="form-group">
                            <label for="" class="d-block d-sm-none">Passport Photo</label>
                            <div class="input-group">
                                <div class="input-group-prepend d-none d-sm-block">
                                    <div class="input-group-text">Passport Photo</div>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="" aria-describedby="inputGroupFileAddon01" disabled>
                                    <label class="custom-file-label" for="">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <span class="passport_photo_status"></span>
                        <div class="progress" style="display: none;">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="passport_photo_preview">
                        <img src alt="" class="card-img">
                    </div>
                    <?php csrf_token(); ?>
                </div>
            </div>
        </div>

    </div>
    <?php if(isset($file_name) && $file_name !== ''): ?>
        <div class="col-md-8">Dùng điện thoại quét mã QR Code bên dưới để vào đúng trang này!</div>
        <div class="col-md-8">
            <img src="../qr_code/<?php echo $file_name ?>" alt="qr_code">
        </div>
    <?php endif;?>
</div>

<script>
    (function($) {
        $('.booking_code').on('change', function(e) {
            _self = $(e.target);
            file = _self.parents('.card').find('.custom-file-input');

            booking_code = _self.val().trim();
            if (!booking_code) {
                file.prop('disabled', true);
                return false;
            }

            _self.val(booking_code);

            file.prop('disabled', false);
            $('.booking_quantity').text('Đang kiểm tra mã booking...');
            var jqxhr = $.getJSON( '/scp/ajax.php/pax/booking',
                {
                    booking_code: booking_code,
                    __CSRFToken__: $('[name=__CSRFToken__]').val()
                },
                function() {
                })
                .done(function(data) {
                    $('.booking_quantity').text('Mã booking này có '+data.total+' pax. Đã nhập '+data.added+' pax. Còn lại '+data.remain+' pax.');
                })
                .fail(function(err) {
                    $('.booking_quantity').text('Mã booking không tồn tại');
                });
        });

        $('.custom-file-input').on('change',function(e){
            //get the file name
            _self = e.target;
            var fileName = e.target.files[0].name;
            fontsize = getComputedStyle($(_self).next('label')[0]).fontSize;
            fontsize = fontsize.replace('px', '');
            em = $(_self).width()/fontsize;
            em = Math.round(em)-5;

            if (fileName.length > 20)
                fileName = '...'+fileName.substr(fileName.length-em);
            $(this).next('.custom-file-label').text(fileName);

            parent = $(this).parents('.card');
            parent.find('.progress').show();
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);

                reader.onload = function(e) {
                    booking_code = $('.booking_code').val().trim();
                    tour_id = $('.tour_id').val().trim();
                    parent = $(input).parents('.card');
                    _status = parent.find('.passport_photo_status');

                    if (!booking_code) {
                        _status.text('Booking Code is required!');
                        return false;
                    }

                    _status.text('Uploading...');
                    parent.find('.passport_photo_preview img').attr('src', '');
                    var jqxhr = $.post( '/scp/ajax.php/pax/passport',
                        {
                            data: e.target.result,
                            name: input.files[0].name,
                            booking_code: booking_code,
                            tour_id: tour_id,
                            __CSRFToken__: $('[name=__CSRFToken__]').val()
                        },
                        function() {
                    })
                        .done(function(data) {
                            var response = jQuery.parseJSON(data);
                            if ('1' === response.result+'') {
                                parent.find('.passport_photo_preview img').attr('src', e.target.result);
                            }

                            _status.text(response.message);
                        })
                        .fail(function(err) {
                            _status.text(err);
                        })
                        .always(function() {
                            parent.find('.progress').hide();
                            $('[type=file]').val('').prop('disabled', true);
                            $('.custom-file-label').text('Choose file');
                            $('.booking_code').val('TG123-');
                            $('.booking_quantity').text('');
                        });
                }
            }
        }
    })(jQuery);
</script>

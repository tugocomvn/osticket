<?php
$sms_type_by_day = TicketAnalytics::smsAnalytics($_REQUEST['from'], $_REQUEST['to'], 'sms_type_by_day', $_REQUEST);
$sms_type_by_day_arr = [];
$dates_source = [];
while($sms_type_by_day && ($row = db_fetch_array($sms_type_by_day))) {
    $type = _String::json_decode($row['type']);
    if (!isset($sms_type_by_day_arr[ $type ]))
        $sms_type_by_day_arr[ $type ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $dates_source[] = $row['week'].'-'.$row['year'];
            $sms_type_by_day_arr[ $type ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $dates_source[] = $row['month'].'-'.$row['year'];
            $sms_type_by_day_arr[ $type ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $sms_type_by_day_arr[ $type ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}

$dates_source = array_filter(array_unique($dates_source));

foreach ($sms_type_by_day_arr as $_type => $_date_data) {
    foreach ($dates_source as $_date) {
        if (!isset($sms_type_by_day_arr[ $_type ][ $_date ]))
            $sms_type_by_day_arr[ $_type ][ $_date ] = 0;
    }

    uksort($sms_type_by_day_arr[ $_type ], "___date_compare");
}
?>
<td colspan="2">
    <h2>SMS Type by Day</h2>
    <canvas id="sms_type" width="1000" height="500"></canvas>
</td>
<script>
    var sms_type_elm = document.getElementById("sms_type").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(<?php echo count($sms_type_by_day_arr) ?>);
    var sms_type = new Chart(sms_type_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($sms_type_by_day_arr as $_type => $_date): ?>
                {
                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                    backgroundColor:  colors[<?php echo $i++ ?>],
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    fill: true
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>
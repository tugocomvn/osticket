<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once  __DIR__.'/functions.php';
session_start();
$campaignId = null;
$name = null;
$content = null;
$from = $to = null;
$result = null;

if(isset($_SESSION['result'])) {
    $result['result'] = $_SESSION['result'];
    $result['mess'] = $_SESSION['mess'];

    unset($_SESSION['result']);
    unset($_SESSION['mess']);
}


if(isset($_REQUEST['campaignId'])) {
    $campaignId = trim($_REQUEST['campaignId']);
}

if(isset($_REQUEST['name'])) {
    $name = trim($_REQUEST['name']);
}

if (isset($_REQUEST['content'])){
    $content = trim($_REQUEST['content']);
}

if (isset($_REQUEST['from']))
    $from = $_REQUEST['from'];
else $from = date('Y-m-d', time());

if (isset($_REQUEST['to']))
    $to = $_REQUEST['to'];
else $to = date('Y-m-d', time() + 24*3600*30);

//var_dump($from.$to);
if($from > $to){
    $result['result'] = false;
    $result['mess'] = 'Ngày bắt đầu không lớn hơn ngày kết thúc';
}
if($_POST && isset($_POST['create']) && $_POST['create'] === 'create'
                                     && ($from <= $to) && !empty($name) && !empty($campaignId))
{
    //check tên không trùng nhau
    if (!Awing::getGroupByName($name) && !Awing::getGroupByCampaignID($campaignId)
                                      && Awing::addGroup($name, $campaignId, $content,$from, $to))
    {
        $_SESSION['result'] = true;
        $_SESSION['mess']  = 'Dữ liệu đã được thêm vào cơ sở dữ liệu';

        header('Location: ./back_create_group.php');
        exit; // exit tại đây thì các biến bên trên không còn công dụng gì nữa (trừ các $_SESSION)
    }else {
        $result['result'] = false;
        $result['mess'] = 'Nhóm voucher đã tồn tại trong cơ sở dữ liệu';
    }

}

require_once (__DIR__."/create_group_vou.php");

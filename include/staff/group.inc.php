<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$info = $qs = array();
if($group && $_REQUEST['a']!='add'){
    $title=__('Update Group');
    $action='update';
    $submit_text=__('Save Changes');
    $info=$group->getInfo();
    $info['id']=$group->getId();
    $info['depts']=$group->getDepartments();
    $info['des']=$group->getDestinations();
    $qs += array('id' => $group->getId());
}else {
    $title=__('Add New Group');
    $action='create';
    $submit_text=__('Create Group');
    $info['isactive']=isset($info['isactive'])?$info['isactive']:1;
    $info['can_create_tickets']=isset($info['can_create_tickets'])?$info['can_create_tickets']:1;
    $qs += array('a' => $_REQUEST['a']);
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form action="groups.php?<?php echo Http::build_query($qs); ?>" method="post" id="save" name="group">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2><?php echo __('Group Access and Permissions');?></h2>
 <table class="form_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em><strong><?php echo __('Group Information');?></strong>: <?php echo __("Disabled group will limit agents' access. Admins are exempted.");?></em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
                <?php echo __('Name');?>:
            </td>
            <td>
                <input type="text" size="30" name="name" value="<?php echo $info['name']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['name']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                <?php echo __('Status');?>:
            </td>
            <td>
                <input type="radio" name="isactive" value="1" <?php echo $info['isactive']?'checked="checked"':''; ?>><strong><?php echo __('Active');?></strong>
                &nbsp;
                <input type="radio" name="isactive" value="0" <?php echo !$info['isactive']?'checked="checked"':''; ?>><strong><?php echo __('Disabled');?></strong>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['status']; ?></span>
                <i class="help-tip icon-question-sign" href="#status"></i>
            </td>
        </tr>

        <tr>
            <th colspan="2">
                <em><strong><?php echo __('General Permissions');?></strong>:</em>
            </th>
        </tr>
        <tr><td><?php echo __('View <strong>personal</strong> Checkin summary');?></td>
            <td>
                <input type="radio" name="can_view_personal_checkin"  value="1"   <?php echo $info['can_view_personal_checkin']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_personal_checkin"  value="0"   <?php echo !$info['can_view_personal_checkin']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View <strong>ALL</strong> Checkin summary');?></td>
            <td>
                <input type="radio" name="can_view_all_checkin"  value="1"   <?php echo $info['can_view_all_checkin']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_all_checkin"  value="0"   <?php echo !$info['can_view_all_checkin']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('Do <strong>Checkin</strong>');?></td>
            <td>
                <input type="radio" name="can_do_checkin"  value="1"   <?php echo $info['can_do_checkin']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_do_checkin"  value="0"   <?php echo !$info['can_do_checkin']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Ticket Activity');?></td>
            <td>
                <input type="radio" name="can_view_ticket_activity"  value="1"   <?php echo $info['can_view_ticket_activity']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_ticket_activity"  value="0"   <?php echo !$info['can_view_ticket_activity']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View General Statistics');?></td>
            <td>
                <input type="radio" name="can_view_general_statistics"  value="1"   <?php echo $info['can_view_general_statistics']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_general_statistics"  value="0"   <?php echo !$info['can_view_general_statistics']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Agent Directory');?></td>
            <td>
                <input type="radio" name="can_view_agent_directory"  value="1"   <?php echo $info['can_view_agent_directory']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_agent_directory"  value="0"   <?php echo !$info['can_view_agent_directory']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Ticket Analytics');?></td>
            <td>
                <input type="radio" name="can_view_ticket_analytics"  value="1"   <?php echo $info['can_view_ticket_analytics']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_ticket_analytics"  value="0"   <?php echo !$info['can_view_ticket_analytics']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Booking Returning');?></td>
            <td>
                <input type="radio" name="can_view_booking_returning"  value="1"   <?php echo $info['can_view_booking_returning']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_booking_returning"  value="0"   <?php echo !$info['can_view_booking_returning']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Agent Statistics');?></td>
            <td>
                <input type="radio" name="can_view_agent_statistics"  value="1"   <?php echo $info['can_view_agent_statistics']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_agent_statistics"  value="0"   <?php echo !$info['can_view_agent_statistics']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Guest Directory');?></td>
            <td>
                <input type="radio" name="can_view_guest_directory"  value="1"   <?php echo $info['can_view_guest_directory']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_guest_directory"  value="0"   <?php echo !$info['can_view_guest_directory']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('Edit Guest Info');?></td>
            <td>
                <input type="radio" name="can_edit_guest_info"  value="1"   <?php echo $info['can_edit_guest_info']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_guest_info"  value="0"   <?php echo !$info['can_edit_guest_info']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('View Organizations');?></td>
            <td>
                <input type="radio" name="can_view_organizations"  value="1"   <?php echo $info['can_view_organizations']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_organizations"  value="0"   <?php echo !$info['can_view_organizations']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('Create Organizations');?></td>
            <td>
                <input type="radio" name="can_create_organizations"  value="1"   <?php echo $info['can_create_organizations']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_organizations"  value="0"   <?php echo !$info['can_create_organizations']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('Edit Organizations');?></td>
            <td>
                <input type="radio" name="can_edit_organizations"  value="1"   <?php echo $info['can_edit_organizations']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_organizations"  value="0"   <?php echo !$info['can_edit_organizations']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>

        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Group Permissions');?></strong>: <?php echo __('Applies to all group members');?>&nbsp;</em>
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>Create</b> Tickets');?></td>
            <td>
                <input type="radio" name="can_create_tickets"  value="1"   <?php echo $info['can_create_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_tickets"  value="0"   <?php echo !$info['can_create_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to open tickets on behalf of users.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Tickets</td>');?>
            <td>
                <input type="radio" name="can_edit_tickets"  value="1"   <?php echo $info['can_edit_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_tickets"  value="0"   <?php echo !$info['can_edit_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to edit tickets.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Post Reply</b>');?></td>
            <td>
                <input type="radio" name="can_post_ticket_reply"  value="1"   <?php echo $info['can_post_ticket_reply']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_post_ticket_reply"  value="0"   <?php echo !$info['can_post_ticket_reply']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to post a ticket reply.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Close</b> Tickets');?></td>
            <td>
                <input type="radio" name="can_close_tickets"  value="1" <?php echo $info['can_close_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_close_tickets"  value="0" <?php echo !$info['can_close_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to close tickets.  Agents can still post a response.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Assign</b> Tickets');?></td>
            <td>
                <input type="radio" name="can_assign_tickets"  value="1" <?php echo $info['can_assign_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_assign_tickets"  value="0" <?php echo !$info['can_assign_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to assign tickets to agents.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Transfer</b> Tickets');?></td>
            <td>
                <input type="radio" name="can_transfer_tickets"  value="1" <?php echo $info['can_transfer_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_transfer_tickets"  value="0" <?php echo !$info['can_transfer_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to transfer tickets between departments.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Delete</b> Tickets');?></td>
            <td>
                <input type="radio" name="can_delete_tickets"  value="1"   <?php echo $info['can_delete_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_delete_tickets"  value="0"   <?php echo !$info['can_delete_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __("Ability to delete tickets (Deleted tickets can't be recovered!)");?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can Ban Emails');?></td>
            <td>
                <input type="radio" name="can_ban_emails"  value="1" <?php echo $info['can_ban_emails']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_ban_emails"  value="0" <?php echo !$info['can_ban_emails']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to add/remove emails from banlist via ticket interface.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can Manage Premade');?></td>
            <td>
                <input type="radio" name="can_manage_premade"  value="1" <?php echo $info['can_manage_premade']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_manage_premade"  value="0" <?php echo !$info['can_manage_premade']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to add/update/disable/delete canned responses and attachments.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can Manage FAQ');?></td>
            <td>
                <input type="radio" name="can_manage_faq"  value="1" <?php echo $info['can_manage_faq']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_manage_faq"  value="0" <?php echo !$info['can_manage_faq']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to add/update/disable/delete knowledgebase categories and FAQs.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can View Agent Stats');?></td>
            <td>
                <input type="radio" name="can_view_staff_stats"  value="1" <?php echo $info['can_view_staff_stats']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_staff_stats"  value="0" <?php echo !$info['can_view_staff_stats']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view stats of other agents in allowed departments.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can Export Tickets');?></td>
            <td>
                <input type="radio" name="can_export_tickets"  value="1" <?php echo $info['can_export_tickets']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_export_tickets"  value="0" <?php echo !$info['can_export_tickets']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to export tickets.');?></i>
            </td>
        </tr>

        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Finance Access');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Payment Chart');?></td>
            <td>
                <input type="radio" name="can_view_payment_chart"  value="1" <?php echo $info['can_view_payment_chart']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_payment_chart"  value="0" <?php echo !$info['can_view_payment_chart']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view payments chart.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Payment List');?></td>
            <td>
                <input type="radio" name="can_view_payment"  value="1" <?php echo $info['can_view_payment']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_payment"  value="0" <?php echo !$info['can_view_payment']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view payments.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Create</b> Payment');?></td>
            <td>
                <input type="radio" name="can_create_payment"  value="1" <?php echo $info['can_create_payment']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_payment"  value="0" <?php echo !$info['can_create_payment']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit payments.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Payment');?></td>
            <td>
                <input type="radio" name="can_edit_payment"  value="1" <?php echo $info['can_edit_payment']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_payment"  value="0" <?php echo !$info['can_edit_payment']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit payments.');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Receipt Access');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>Create</b> Receipt');?></td>
            <td>
                <input type="radio" name="can_create_receipt"  value="1" <?php echo $info['can_create_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_receipt"  value="0" <?php echo !$info['can_create_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create receipt.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Receipt');?></td>
            <td>
                <input type="radio" name="can_view_receipt"  value="1" <?php echo $info['can_view_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_receipt"  value="0" <?php echo !$info['can_view_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view receipt.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Receipt');?></td>
            <td>
                <input type="radio" name="can_edit_receipt"  value="1" <?php echo $info['can_edit_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_receipt"  value="0" <?php echo !$info['can_edit_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to edit receipt.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Receipt List');?></td>
            <td>
                <input type="radio" name="can_view_receipt_list"  value="1" <?php echo $info['can_view_receipt_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_receipt_list"  value="0" <?php echo !$info['can_view_receipt_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view receipts.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Approve</b> Receipt');?></td>
            <td>
                <input type="radio" name="can_approve_receipt"  value="1" <?php echo $info['can_approve_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_approve_receipt"  value="0" <?php echo !$info['can_approve_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to approve receipt.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Reject</b> Receipt');?></td>
            <td>
                <input type="radio" name="can_reject_receipt"  value="1" <?php echo $info['can_reject_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_reject_receipt"  value="0" <?php echo !$info['can_reject_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to approve receipt.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Manage</b> All Receipt');?></td>
            <td>
                <input type="radio" name="can_manage_all_receipt"  value="1" <?php echo $info['can_manage_all_receipt']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_manage_all_receipt"  value="0" <?php echo !$info['can_manage_all_receipt']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to all receipt.');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Tour');?></strong>:
            </th>
        </tr>

        <tr><td><?php echo __('Can <b>Cancel</b> Tour');?></td>
            <td>
                <input type="radio" name="can_cancel_tour"  value="1" <?php echo $info['can_cancel_tour']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_cancel_tour"  value="0" <?php echo !$info['can_cancel_tour']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to cancel tour.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Net Price');?></td>
            <td>
                <input type="radio" name="can_view_net_price"  value="1" <?php echo $info['can_view_net_price']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_net_price"  value="0" <?php echo !$info['can_view_net_price']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view net price.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Retail Price');?></td>
            <td>
                <input type="radio" name="can_view_retail_price"  value="1" <?php echo $info['can_view_retail_price']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_retail_price"  value="0" <?php echo !$info['can_view_retail_price']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view retail price.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Net Price');?></td>
            <td>
                <input type="radio" name="can_edit_net_price"  value="1" <?php echo $info['can_edit_net_price']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_net_price"  value="0" <?php echo !$info['can_edit_net_price']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view net price.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Retail Price');?></td>
            <td>
                <input type="radio" name="can_edit_retail_price"  value="1" <?php echo $info['can_edit_retail_price']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_retail_price"  value="0" <?php echo !$info['can_edit_retail_price']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view net price.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Request</b> Edit Tour ');?></td>
            <td>
                <input type="radio" name="can_request_edit_tour"  value="1" <?php echo $info['can_request_edit_tour']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_request_edit_tour"  value="0" <?php echo !$info['can_request_edit_tour']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view net price.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Approve</b> Edit Tour ');?></td>
            <td>
                <input type="radio" name="can_approve_edit_tour"  value="1" <?php echo $info['can_approve_edit_tour']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_approve_edit_tour"  value="0" <?php echo !$info['can_approve_edit_tour']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view net price.');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Booking Reservation');?></strong>:
            </th>
        </tr>
        <tr>
            <td><?php echo __('Can <b>View</b> Approve Leader');?></td>
            <td>
                <input type="radio" name="can_view_approve_leader"  value="1" <?php echo $info['can_view_approve_leader']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_approve_leader"  value="0" <?php echo !$info['can_view_approve_leader']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve leader.');?></i>
            </td>
        </tr>
        <tr>
            <td><?php echo __('Can <b>View</b> Approve Operator');?></td>
            <td>
                <input type="radio" name="can_view_approve_operator"  value="1" <?php echo $info['can_view_approve_operator']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_approve_operator"  value="0" <?php echo !$info['can_view_approve_operator']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve operator.');?></i>
            </td>
        </tr>
        <tr>
            <td><?php echo __('Can <b>View</b> Approve Visa');?></td>
            <td>
                <input type="radio" name="can_view_approve_visa"  value="1" <?php echo $info['can_view_approve_visa']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_approve_visa"  value="0" <?php echo !$info['can_view_approve_visa']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve visa.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b> Approve </b> Leader');?></td>
            <td>
                <input type="radio" name="can_approve_leader"  value="1" <?php echo $info['can_approve_leader']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_approve_leader"  value="0" <?php echo !$info['can_approve_leader']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b> Approve </b> Operator');?></td>
            <td>
                <input type="radio" name="can_approve_operator"  value="1" <?php echo $info['can_approve_operator']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_approve_operator"  value="0" <?php echo !$info['can_approve_operator']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b> Approve </b> Visa');?></td>
            <td>
                <input type="radio" name="can_approve_visa"  value="1" <?php echo $info['can_approve_visa']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_approve_visa"  value="0" <?php echo !$info['can_approve_visa']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view approve.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Hold</b> Reservation');?></td>
            <td>
                <input type="radio" name="can_hold_reservation"  value="1" <?php echo $info['can_hold_reservation']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_hold_reservation"  value="0" <?php echo !$info['can_hold_reservation']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view bookings.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Cancel Member</b> Reservation ');?></td>
            <td>
                <input type="radio" name="can_cancel_member_reservation"  value="1" <?php echo $info['can_cancel_member_reservation']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_cancel_member_reservation"  value="0" <?php echo !$info['can_cancel_member_reservation']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to cancel member reservation.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Cancel Leader</b> Reservation');?></td>
            <td>
                <input type="radio" name="can_cancel_leader_reservation"  value="1" <?php echo $info['can_cancel_leader_reservation']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_cancel_leader_reservation"  value="0" <?php echo !$info['can_cancel_leader_reservation']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to cancel leader reservation.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Cancel Super</b> Reservation');?></td>
            <td>
                <input type="radio" name="can_cancel_super_reservation"  value="1" <?php echo $info['can_cancel_super_reservation']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_cancel_super_reservation"  value="0" <?php echo !$info['can_cancel_super_reservation']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to cancel leader reservation.');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Booking Access');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Bookings List');?></td>
            <td>
                <input type="radio" name="can_view_booking"  value="1" <?php echo $info['can_view_booking']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_booking"  value="0" <?php echo !$info['can_view_booking']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view bookings.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Create</b> Bookings');?></td>
            <td>
                <input type="radio" name="can_create_booking"  value="1" <?php echo $info['can_create_booking']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_booking"  value="0" <?php echo !$info['can_create_booking']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit bookings.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Bookings');?></td>
            <td>
                <input type="radio" name="can_edit_booking"  value="1" <?php echo $info['can_edit_booking']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_booking"  value="0" <?php echo !$info['can_edit_booking']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit bookings.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Export</b> Bookings');?></td>
            <td>
                <input type="radio" name="can_export_booking"  value="1" <?php echo $info['can_export_booking']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_export_booking"  value="0" <?php echo !$info['can_export_booking']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to export bookings.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can change booking <b>Status</b>');?></td>
            <td>
                <input type="radio" name="can_change_booking_status"  value="1" <?php echo $info['can_change_booking_status']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_change_booking_status"  value="0" <?php echo !$info['can_change_booking_status']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to change booking status. (Unfinished bookings)');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Offlate Access');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Offlate List');?></td>
            <td>
                <input type="radio" name="can_view_offlate"  value="1" <?php echo $info['can_view_offlate']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_offlate"  value="0" <?php echo !$info['can_view_offlate']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view offlate.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Create</b> Offlate');?></td>
            <td>
                <input type="radio" name="can_create_offlate"  value="1" <?php echo $info['can_create_offlate']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_offlate"  value="0" <?php echo !$info['can_create_offlate']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit offlate.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Accept</b> offlate(Leader)');?></td>
            <td>
                <input type="radio" name="can_edit_offlate"  value="1" <?php echo $info['can_edit_offlate']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_offlate"  value="0" <?php echo !$info['can_edit_offlate']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to create/edit offlate.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Accept</b> Offlate(Manager)');?></td>
            <td>
                <input type="radio" name="can_accept_offlate"  value="1" <?php echo $info['can_accept_offlate']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_accept_offlate"  value="0" <?php echo !$info['can_accept_offlate']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to accept offlate.');?></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Operator Access');?></strong>:
            </th>
        </tr>

        <tr><td><?php echo __('Can <b>View</b> Operator List');?></td>
            <td>
                <input type="radio" name="can_view_operator_list"  value="1" <?php echo $info['can_view_operator_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_operator_list"  value="0" <?php echo !$info['can_view_operator_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view Operator List.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can <b>EDIT</b> Operator List');?></td>
            <td>
                <input type="radio" name="can_edit_operator_list"  value="1" <?php echo $info['can_edit_operator_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_operator_list"  value="0" <?php echo !$info['can_edit_operator_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to EDIT operator list items.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can <b>CREATE</b> Operator List');?></td>
            <td>
                <input type="radio" name="can_create_operator_list_item"  value="1" <?php echo $info['can_create_operator_list_item']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_create_operator_list_item"  value="0" <?php echo !$info['can_create_operator_list_item']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to CREATE operator list items.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can edit Operator <strong>PAX</strong> List');?></td>
            <td>
                <input type="radio" name="op_edit_pax"  value="1" <?php echo $info['op_edit_pax']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="op_edit_pax"  value="0" <?php echo !$info['op_edit_pax']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to add/edit operator pax list.');?></i>
            </td>
        </tr>

        <tr><td><?php echo __('Can View <strong>Tour</strong> List');?></td>
            <td>
                <input type="radio" name="can_view_tour_list"  value="1" <?php echo $info['can_view_tour_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_tour_list"  value="0" <?php echo !$info['can_view_tour_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view tour list.');?></i>
            </td>
        </tr>



        <tr><td><?php echo __('Can edit Settlement');?></td>
            <td>
                <input type="radio" name="op_edit_settlement"  value="1" <?php echo $info['op_edit_settlement']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="op_edit_settlement"  value="0" <?php echo !$info['op_edit_settlement']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view and edit Settlement.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can view Settlement List');?></td>
            <td>
                <input type="radio" name="op_view_settlement_list"  value="1" <?php echo $info['op_view_settlement_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="op_view_settlement_list"  value="0" <?php echo !$info['op_view_settlement_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                <i><?php echo __('Ability to view and NOT edit Settlement.');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can view Expected Settlement');?></td>
            <td>
                <input type="radio" name="op_view_expected_settlement"  value="1" <?php echo $info['op_view_expected_settlement']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="op_view_expected_settlement"  value="0" <?php echo !$info['op_view_expected_settlement']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr><td><?php echo __('Can view Actual Settlement');?></td>
            <td>
                <input type="radio" name="op_view_actual_settlement"  value="1" <?php echo $info['op_view_actual_settlement']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="op_view_actual_settlement"  value="0" <?php echo !$info['op_view_actual_settlement']?'checked="checked"':''; ?> /><?php echo __('No');?>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Loyalty Permission');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Member List </br> History Used Point');?></td>
            <td>
                <input type="radio" name="can_view_member_list"  value="1" <?php echo $info['can_view_member_list']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_member_list"  value="0" <?php echo !$info['can_view_member_list']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view member list, history used point');?></i>
            </td>
        </tr>
        <tr><td><?php echo __('Can <b>Edit</b> Point Loyalty');?></td>
            <td>
                <input type="radio" name="can_edit_point_loyalty"  value="1" <?php echo $info['can_edit_point_loyalty']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_edit_point_loyalty"  value="0" <?php echo !$info['can_edit_point_loyalty']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to edit point loyalty');?></i>
            </td>
        </tr>

        <!--------------------VISA--------------------- !-->
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Visa Access');?></strong>:
            </th>
        </tr>
        <tr><td><?php echo __('Can <b>View</b> Documents');?></td>
            <td>
                <input type="radio" name="can_view_visa_documents"  value="1" <?php echo $info['can_view_visa_documents']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_view_visa_documents"  value="0" <?php echo !$info['can_view_visa_documents']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to view documents.');?></i>
            </td>
        </tr><tr><td><?php echo __('Can <b>Manage</b> Items');?></td>
            <td>
                <input type="radio" name="can_manage_visa_items"  value="1" <?php echo $info['can_manage_visa_items']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_manage_visa_items"  value="0" <?php echo !$info['can_manage_visa_items']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to manage items.');?></i>
            </td>
        </tr><tr><td><?php echo __('Can <b>Manage</b> Passenger Documents');?></td>
            <td>
                <input type="radio" name="can_manage_visa_pax_documents"  value="1" <?php echo $info['can_manage_visa_pax_documents']?'checked="checked"':''; ?> /><?php echo __('Yes');?>
                &nbsp;&nbsp;
                <input type="radio" name="can_manage_visa_pax_documents"  value="0" <?php echo !$info['can_manage_visa_pax_documents']?'checked="checked"':''; ?> /><?php echo __('No');?>
                &nbsp;&nbsp;<i><?php echo __('Ability to manage passenger documents.');?></i>
            </td>
        </tr>
        <!----------------------END VISA--------------------- !-->

        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Department Access');?></strong>:
                <i class="help-tip icon-question-sign" href="#department_access"></i>
                &nbsp;<a class="selectAll" href="#deptckb"><?php echo __('Select All');?></a>
                &nbsp;&nbsp;
                <a class="selectNone" href="#deptckb"><?php echo __('Select None');?></a></em>
            </th>
        </tr>
        <?php
         $sql='SELECT dept_id,dept_name FROM '.DEPT_TABLE.' ORDER BY dept_name';
         if(($res=db_query($sql)) && db_num_rows($res)){
            while(list($id,$name) = db_fetch_row($res)){
                $ck=($info['depts'] && in_array($id,$info['depts']))?'checked="checked"':'';
                echo sprintf('<tr><td colspan=2>&nbsp;&nbsp;<input type="checkbox" class="deptckb" name="depts[]" value="%d" %s>%s</td></tr>',$id,$ck,$name);
            }
         }
        ?>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Tour Access');?></strong>:
                    <i class="help-tip icon-question-sign" href="#tour_access"></i>
                    &nbsp;<a class="selectAll" href="#tourckb"><?php echo __('Select All');?></a>
                    &nbsp;&nbsp;
                    <a class="selectNone" href="#tourckb"><?php echo __('Select None');?></a></em>
            </th>
        </tr>
        <?php $list_market = TourMarket::objects()->filter(['status' => 1])->order_by('name');
        foreach ($list_market as $market): ?>
            <tr>
                <td colspan=2>
                    <input type="checkbox" class="tourckb" name="des[]" value="<?php echo $market->id?>"
                    <?php if($info['des'] && in_array($market->id, $info['des'])) echo 'checked' ?>>
                    <?php echo $market->name ?>
                </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Admin Notes');?></strong>: <?php echo __('Internal notes viewable by all admins.');?>&nbsp;</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <textarea class="richtext no-bar" name="notes" cols="21"
                    rows="8" style="width: 80%;"><?php echo $info['notes']; ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
<p style="text-align:center">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="<?php echo __('Reset');?>">
    <input type="button" name="cancel" value="<?php echo __('Cancel');?>" onclick='window.location.href="groups.php"'>
</p>
</form>

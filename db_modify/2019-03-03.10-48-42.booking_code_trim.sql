alter table booking_tmp
  add booking_code_trim varchar(10) null after booking_code;

create index booking_tmp_booking_coide_trim_index
  on booking_tmp (booking_code_trim);

alter table payment_tmp
  add booking_code_trim int null;

create index payment_tmp_booking_code_trim_index
  on payment_tmp (booking_code_trim);

alter table payment_tmp modify booking_code_trim int null after booking_code;
alter table payment_tmp modify booking_code_trim varchar(10) null;


UPDATE booking_tmp
SET booking_code_trim = trim( trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM booking_code)) )
WHERE (booking_code_trim IS NULL OR booking_code_trim LIKE '')
  AND (booking_code IS NOT NULL AND booking_code NOT LIKE '')
;


DELIMITER $$

CREATE FUNCTION SPLIT_STR(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
)
  RETURNS VARCHAR(255) DETERMINISTIC
BEGIN
  RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
                           LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
                 delim, '');
END$$

DELIMITER ;

UPDATE payment_tmp
SET booking_code_trim = trim( trim(LEADING '0' FROM SPLIT_STR(booking_code, '-', 2) ) )
WHERE (booking_code_trim IS NULL OR booking_code_trim LIKE '')
  AND (booking_code IS NOT NULL AND booking_code NOT LIKE '')
  AND (booking_code NOT like '%&%' AND  booking_code NOT like '%,%' AND  booking_code NOT like '%+%');

alter table ost_pax_tour
  add booking_code_trim varchar(10) null after booking_code;

create index ost_pax_tour_booking_code_trim_index
  on ost_pax_tour (booking_code_trim);


UPDATE ost_pax_tour
SET booking_code_trim = trim( trim(LEADING '0' FROM SPLIT_STR(booking_code, '-', 2)) )
WHERE (booking_code_trim IS NULL OR booking_code_trim LIKE '')
  AND (booking_code IS NOT NULL AND booking_code NOT LIKE '');

UPDATE ost_pax_tour
SET booking_code_trim = trim( trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM booking_code)) )
WHERE (booking_code_trim IS NULL OR booking_code_trim LIKE '')
  AND (booking_code IS NOT NULL AND booking_code NOT LIKE '')
  and length( trim( trim(LEADING '0' FROM trim(LEADING 'TG123-' FROM booking_code)) ) ) <= 10
;

SELECT p.id as pax_id, t.id as tour_id, p.full_name, p.dob, i.value, t.gather_date, bv.created as booking_date
FROM ost_pax_tour pt
       JOIN ost_pax p ON p.id = pt.pax_id
       JOIN ost_tour t ON t.id = pt.tour_id
       JOIN ost_list_items i ON i.id = t.destination
       JOIN booking_tmp bv ON pt.booking_code_trim LIKE bv.booking_code_trim
WHERE bv.created >= '2018-01-01'
  AND i.id IN ( 1 );


create table mkt_tuthien
(
  pax_id int not null,
  tour_id int not null,
  full_name varchar(64) null,
  dob date null,
  tour_name varchar(64) null,
  gather_date date null,
  booking_date datetime null,
  constraint mkt_tuthien_pk
    primary key (pax_id, tour_id)
);

alter table mkt_tuthien modify gather_date datetime null;


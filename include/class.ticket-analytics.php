<?php
class TicketAnalytics {
    private static $source = "case
         when e.address IN ('phamquocbuu@gmail.com', 'beckerbao29@gmail.com', 'beckerbao@gmail.com') then 'FB'
         when e.address IN ('it@tugo.com.vn', 'support@tugo.com.vn'
                                              'support@tugo.vn') then 'WEB'
         else 'Other'
           end";

    public static $source_name = [
        1 => 'Facebook',
        2 => 'Website',
        3 => 'Other',
        4 => 'Email',
        5 => 'Phone',
    ];

    public static function analytics($from, $to, $type, array $params, &$chart_data, &$table_data) {
        $params['group'] = isset($params['group']) && in_array($params['group'], ['day', 'week', 'month']) ? $params['group'] : 'day';
        if (!$from)
            $from = date('Y-m-d', time()-24*3600*30);
        if (!$to)
            $to = date('Y-m-d');

        switch ($type) {
            case "tag_by_day":
                $chart_data = static::_tagByDay($from, $to, $params);
                $table_data = static::_tagCount($from, $to, $params);
                break;
            case "source_by_day":
                $chart_data = static::_sourceByDay($from, $to, $params);
                $table_data = static::_sourceCount($from, $to, $params);
                break;
            case "tag_and_source_by_day":
                $chart_data = static::_tagAndSourceByDay($from, $to, $params);
                $table_data = static::_tagAndSourceCount($from, $to, $params);
                break;
            case 'total':
                $chart_data = static::_total($from, $to, $params);
                break;
            default:
                $chart_data = $table_data = [];
                break;
        }
    }

    public static function bookingAnalytics($from, $to, $type, $params = []) {
        $params['group'] = isset($params['group']) && in_array($params['group'], ['day', 'week', 'month']) ? $params['group'] : 'day';
        if (!$from)
            $from = date('Y-m-d', time()-24*3600*30);
        if (!$to)
            $to = date('Y-m-d');

        switch ($type) {
            case "booking_by_day":
                $data = static::_booking_by_day($from, $to, $params);
                break;
            case "all_booking":
                $data = static::_all_booking($from, $to, $params);
                break;
            case "booking_type_by_day":
                $data = static::_booking_type_by_day($from, $to, $params);
                break;
            default:
                $data = [];
                break;
        }

        return $data;
    }

    public static function smsAnalytics($from, $to, $type, $params = []) {
        $params['group'] = isset($params['group']) && in_array($params['group'], ['day', 'week', 'month']) ? $params['group'] : 'day';
        if (!$from)
            $from = date('Y-m-d', time()-24*3600*30);
        if (!$to)
            $to = date('Y-m-d');

        switch ($type) {
            case "sms_type_by_day":
                $data = static::_sms_type_by_day($from, $to, $params);
                break;
            case "all_sms":
                $data = static::_all_sms($from, $to, $params);
                break;
            default:
                $data = [];
                break;
        }

        return $data;
    }

    public static function callAnalytics($from, $to, $type, $params = []) {
        $params['group'] = isset($params['group']) && in_array($params['group'], ['day', 'week', 'month']) ? $params['group'] : 'day';
        if (!$from)
            $from = date('Y-m-d', time()-24*3600*30);
        if (!$to)
            $to = date('Y-m-d');

        switch ($type) {
            case "callin_type_by_day":
                $data = static::_callin_type_by_day($from, $to, $params);
                break;
            case "callout_type_by_day":
                $data = static::_callout_type_by_day($from, $to, $params);
                break;
            case "all_call_by_day":
                $data = static::_all_call_by_day($from, $to, $params);
                break;
            default:
                $data = [];
                break;
        }

        return $data;
    }


    public static function bookingReturning($from, $to, $type) {
        if (!$from)
            $from = date('Y-m-d', time()-24*3600*30);
        if (!$to)
            $to = date('Y-m-d');

        switch ($type) {
            case "returning":
                $data = static::_booking_returning($from, $to);
                break;
            case "returning_month":
                $data = static::_booking_returning_by_month($from, $to);
                break;

            default:
                $data = [];
                break;
        }

        return $data;
    }

    private static function _all_tag($from, $to, $params = []) {
        $sql = "select tt.tag_id, count(distinct t.ticket_id) as total
            from ost_ticket t join ost_ticket_tag tt on t.ticket_id=tt.ticket_id
            WHERE date(t.created) >= ".db_input($from)." AND date(t.created) <= ".db_input($to)."
            AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
            and (
              tt.tag_id = (select MIN(tag_id) from ost_ticket_tag where ost_ticket_tag.ticket_id = t.ticket_id)
                or tt.tag_id is null
              )
            group by tt.tag_id
            order by total desc";

        return db_query($sql);
    }

    private static function _tagByDay($from, $to, $params = []) {
        $res = self::_all_tag($from, $to, $params);
        $types = [];
        while($res && ($row = db_fetch_array($res))) {
            $types[] = db_input($row['tag_id']);
        }

        switch ($params['group']) {
            case 'week':
                $group = 'week(t.created, 5), year(t.created)';
                $select = 'week(t.created, 5) as week, year(t.created) as year';
                break;
            case 'month':
                $group = 'month(t.created), year(t.created)';
                $select = 'month(t.created) as month, year(t.created) as year';
                break;
            case 'day':
            default:
                $group = 'date(t.created)';
                $select = 'date(t.created) as date';
                break;
        }

        $sql = "select $select, tg.content, count(distinct t.ticket_id) as total
            from ost_ticket t join ost_ticket_tag tt on t.ticket_id=tt.ticket_id
                join ost_tag tg ON tg.id=tt.tag_id
            WHERE date(t.created) >= ".db_input($from)." AND date(t.created) <= ".db_input($to)."
            AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
            and (
              tt.tag_id = (select MIN(tag_id) from ost_ticket_tag where ost_ticket_tag.ticket_id = t.ticket_id)
                or tt.tag_id is null
              )
            group by $group, tt.tag_id
            order by $group ASC ".( $types ? ", field(tag_id, ".implode(',', $types).");" : ' ');

        return db_query($sql);
    }

    private static function _tagCount($from, $to, $params = []) {
        $sql = "select ifnull(tt.tag_id, 0) as tag_id, count(distinct t.ticket_id) as total
            from ost_ticket t left join ost_ticket_tag tt on t.ticket_id=tt.ticket_id
            WHERE date(t.created) >= ".db_input($from)." AND date(t.created) <= ".db_input($to)."
            AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
            and (
              tt.tag_id = (select MIN(tag_id) from ost_ticket_tag where ost_ticket_tag.ticket_id = t.ticket_id)
                or tt.tag_id is null
              )
            group by tt.tag_id
            order by count(distinct t.ticket_id) desc "
            ;

        return db_query($sql);
    }

    private static function _sourceByDay($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(t.created, 5), year(t.created)';
                $select = 'week(t.created, 5) as week, year(t.created) as year';
                break;
            case 'month':
                $group = 'month(t.created), year(t.created)';
                $select = 'month(t.created) as month, year(t.created) as year';
                break;
            case 'day':
            default:
                $group = 'date(t.created)';
                $select = 'date(t.created) as date';
                break;
        }

        $sql = "
        select $select, (case when e.source = 3
                                          then case when t.source like 'Email' then 4
                                                    when t.source like 'Phone' then 5 
                                                    when t.source like 'Facebook' then 1
                                                    else 3
    end else e.source end) as sourcex, count(distinct t.ticket_id) as total
        from ost_ticket t
            join ost_ticket_thread tr on t.ticket_id =tr.ticket_id
            join ost_user_email e on e.user_id = tr.user_id
        WHERE date(t.created) >= ".db_input($from)."
          AND date(t.created) <= ".db_input($to)."
          AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
          AND tr.id = (select min(id) from ost_ticket_thread where ost_ticket_thread.ticket_id = t.ticket_id)
        group by $group, sourcex
        order by $group desc;
        ";

        return db_query($sql);
    }

    private static function _sourceCount($from, $to, $params = []) {
        $sql = "
            select (case when e.source = 3
                                          then case when t.source like 'Email' then 4
                                                    when t.source like 'Phone' then 5 
                                                    when t.source like 'Facebook' then 1
                                                    else 3
                    end else e.source end) as sourcex,
                   count(distinct t.ticket_id) as total
            from ost_ticket t
                join ost_ticket_thread tr on t.ticket_id =tr.ticket_id
                join ost_user_email e on e.user_id = tr.user_id
            WHERE date(t.created) >= ".db_input($from)."
              AND date(t.created) <= ".db_input($to)."
              AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
              AND tr.id = (select min(id) from ost_ticket_thread where ost_ticket_thread.ticket_id = t.ticket_id)
            group by sourcex
            order by count(distinct t.ticket_id) desc
        ";

        return db_query($sql);
    }

    private static function _total($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(tr.created, 5), year(tr.created)';
                $select = 'week(tr.created, 5) as week, year(tr.created) as year';
                break;
            case 'month':
                $group = 'month(tr.created), year(tr.created)';
                $select = 'month(tr.created) as month, year(tr.created) as year';
                break;
            case 'day':
            default:
                $group = 'date(tr.created)';
                $select = 'date(tr.created) as date';
                break;
        }

        $sql = "SELECT $select, count(distinct tr.ticket_id) as total
            FROM ost_ticket tr
             WHERE date(tr.created) >= ".db_input($from)."
              AND date(tr.created) <= ".db_input($to)."
              AND tr.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
            group by $group
            order by $group asc;";
        return db_query($sql);
    }

    private static function _tagAndSourceByDay($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(t.created, 5), year(t.created)';
                $select = 'week(t.created, 5) as week, year(t.created) as year';
                break;
            case 'month':
                $group = 'month(t.created), year(t.created)';
                $select = 'month(t.created) as month, year(t.created) as year';
                break;
            case 'day':
            default:
                $group = 'date(t.created)';
                $select = 'date(t.created) as date';
                break;
        }

        $sql = "
            select $select,
                   (case when e.source = 3
                    then case when t.source like 'Email' then 4
                            when t.source like 'Phone' then 5
                            when t.source like 'Facebook' then 1
                            else 3
                    end else e.source end) as sourcex,
                   tt.tag_id,
                   count(distinct t.ticket_id) as total
            from ost_ticket t
                   join ost_ticket_thread tr on t.ticket_id = tr.ticket_id
                    join ost_user_email e on e.user_id = tr.user_id
                   join ost_ticket_tag tt on t.ticket_id = tt.ticket_id
            WHERE date(t.created) >= ".db_input($from)."
              AND date(t.created) <= ".db_input($to)."
              AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
              AND tr.id = (select min(id) from ost_ticket_thread where ost_ticket_thread.ticket_id = t.ticket_id)
              and (
              tt.tag_id = (select MIN(tag_id) from ost_ticket_tag where ost_ticket_tag.ticket_id = t.ticket_id)
                or tt.tag_id is null
              )
            group by $group, sourcex, tt.tag_id
            order by total desc
        ";

        return db_query($sql);
    }

    private static function _tagAndSourceCount($from, $to, $params = []) {
        $sql = "
            select 
                   (case when e.source = 3
                    then case when t.source like 'Email' then 4
                            when t.source like 'Phone' then 5 
                            when t.source like 'Facebook' then 1
                            else 3
                    end else e.source end) as sourcex,
                   tt.tag_id,
                   count(distinct t.ticket_id) as total
            from ost_ticket t
                   join ost_ticket_thread tr on t.ticket_id = tr.ticket_id
                   join ost_user_email e on e.user_id = tr.user_id
                   join ost_ticket_tag tt on t.ticket_id = tt.ticket_id
            WHERE date(t.created) >= ".db_input($from)."
              AND date(t.created) <= ".db_input($to)."
              AND t.dept_id IN (".implode(',', unserialize(SALE_DEPT)).")
              AND tr.id = (select min(id) from ost_ticket_thread where ost_ticket_thread.ticket_id = t.ticket_id)
              and (
              tt.tag_id = (select MIN(tag_id) from ost_ticket_tag where ost_ticket_tag.ticket_id = t.ticket_id)
                or tt.tag_id is null
              )
            group by sourcex, tt.tag_id
            order by count(distinct t.ticket_id) desc
        ";

        return db_query($sql);
    }

    private static function _all_booking($from, $to, $params = []) {
        $sql = "select  booking_type, count(distinct  ticket_id) as total 
            from booking_view
            WHERE date(created) >= ".db_input($from)."
              AND date(created) <= ".db_input($to)."
            group by  booking_type order by total desc;";

        return db_query($sql);
    }

    private static function _booking_type_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(created, 5), year(created)';
                $select = 'week(created, 5) as week, year(created) as year';
                break;
            case 'month':
                $group = 'month(created), year(created)';
                $select = 'month(created) as month, year(created) as year';
                break;
            case 'day':
            default:
                $group = 'date(created)';
                $select = 'date(created) as date';
                break;
        }

        $res = self::_all_booking($from, $to, $params);
        $types = [];
        while($res && ($row = db_fetch_array($res))) {
            $types[] = db_input($row['booking_type']);
        }

        $sql = "select  $select, booking_type, count(distinct  ticket_id) as total 
            from booking_view
            WHERE date(created) >= ".db_input($from)."
              AND date(created) <= ".db_input($to)."
            group by  $group, booking_type 
            order by $group asc ".( $types ? ", field(booking_type, ".implode(',', $types).");" : ' ');

        return db_query($sql);
    }

    private static function _booking_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(created, 5), year(created)';
                $select = 'week(created, 5) as week, year(created) as year';
                break;
            case 'month':
                $group = 'month(created), year(created)';
                $select = 'month(created) as month, year(created) as year';
                break;
            case 'day':
            default:
                $group = 'date(created)';
                $select = 'date(created) as date';
                break;
        }

        $sql = "select  $select, count(distinct  ticket_id) as total 
            from booking_view
            WHERE date(created) >= ".db_input($from)."
              AND date(created) <= ".db_input($to)."
            group by  $group order by $group asc;";

        return db_query($sql);
    }

    private static function _sms_type_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\'), 5), year(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\'))';
                $select = 'week(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\'), 5) as week, year(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\')) as year';
                break;
            case 'month':
                $group = 'month(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\')), year(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\'))';
                $select = 'month(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\')) as month, year(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\')) as year';
                break;
            case 'day':
            default:
                $group = 'date(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\'))';
                $select = 'date(CONVERT_TZ(send_time, \'+00:00\', \'+07:00\')) as date';
                break;
        }

        $sql = "select type, $select, count(distinct  id) as total
         from ost_sms_log
         WHERE date(CONVERT_TZ(send_time, '+00:00', '+07:00')) >= ".db_input($from)."
              AND date(CONVERT_TZ(send_time, '+00:00', '+07:00')) <= ".db_input($to)."
        group by type, $group
        order by $group desc, type asc";

        return db_query($sql);
    }

    private static function _all_sms($from, $to, $params = []) {
        $sql = "select type, count(distinct  id) as total from ost_sms_log
            WHERE date(CONVERT_TZ(send_time, '+00:00', '+07:00')) >= ".db_input($from)."
            AND date(CONVERT_TZ(send_time, '+00:00', '+07:00')) <= ".db_input($to)."
            group by type
            order by total desc";

        return db_query($sql);
    }

    private static function _callin_type_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(starttime, 5), year(starttime)';
                $select = 'week(starttime, 5) as week, year(starttime) as year';
                break;
            case 'month':
                $group = 'month(starttime), year(starttime)';
                $select = 'month(starttime) as month, year(starttime) as year';
                break;
            case 'day':
            default:
                $group = 'date(starttime)';
                $select = 'date(starttime) as date';
                break;
        }

        $sql = "select count(distinct  id) as total,  $select, disposition as type
            from ost_call_log
            where direction like 'inbound'
            AND date(starttime) >= ".db_input($from)."
            AND date(starttime) <= ".db_input($to)."
            group by $group, disposition
            order by $group desc
            ;";

        return db_query($sql);
    }

    private static function _callout_type_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(starttime, 5), year(starttime)';
                $select = 'week(starttime, 5) as week, year(starttime) as year';
                break;
            case 'month':
                $group = 'month(starttime), year(starttime)';
                $select = 'month(starttime) as month, year(starttime) as year';
                break;
            case 'day':
            default:
                $group = 'date(starttime)';
                $select = 'date(starttime) as date';
                break;
        }

        $sql = "select count(distinct  id) as total,  $select, disposition as type
            from ost_call_log
            where direction like 'outbound'
            AND date(starttime) >= ".db_input($from)."
            AND date(starttime) <= ".db_input($to)."
            group by $group, disposition
            order by $group desc
            ;";

        return db_query($sql);
    }

    private static function _all_call_by_day($from, $to, $params = []) {
        switch ($params['group']) {
            case 'week':
                $group = 'week(starttime, 5), year(starttime)';
                $select = 'week(starttime, 5) as week, year(starttime) as year';
                break;
            case 'month':
                $group = 'month(starttime), year(starttime)';
                $select = 'month(starttime) as month, year(starttime) as year';
                break;
            case 'day':
            default:
                $group = 'date(starttime)';
                $select = 'date(starttime) as date';
                break;
        }

        $sql = "select count(distinct  id) as total,  $select, direction as type
            from ost_call_log
            where  date(starttime) >= ".db_input($from)."
            AND date(starttime) <= ".db_input($to)."
            group by $group, direction
            order by $group desc
            ;";

        return db_query($sql);
    }

    private static function _booking_returning($from, $to) {
        $sql = "        select x_date as date, sum(if(x_count>0,1, 0)) as total
            from (
                 select date(r.date)               x_date,
                        (select count(*)
                         from booking_phone_number a
                         where a.phone_number like r.phone_number
                           and a.date < r.date) as x_count
                 from booking_phone_number r
                 where date(r.date) >= ".db_input($from)."
                    AND date(r.date) <= ".db_input($to)."
                 )  as a group by x_date
        ;";
        return db_query($sql);
    }

    private static function _booking_returning_by_month($from, $to) {
        $sql = "        select x_year, x_month, sum(if(x_count>0,1, 0)) as total
            from (
                 select year(r.date) x_year, month(r.date) x_month,
                        (select count(*)
                         from booking_phone_number a
                         where a.phone_number like r.phone_number
                           and a.date < r.date) as x_count
                 from booking_phone_number r
                 where date(r.date) >= ".db_input($from)."
                    AND date(r.date) <= ".db_input($to)."
                 )  as a group by x_year, x_month
        ;";
        return db_query($sql);
    }

    public static function bookingReturingPhoneNumberCheck($phone_number) {
        $sql = "select phone_number, count(distinct date(date)) as count,
            GROUP_CONCAT(date(date)) as date, GROUP_CONCAT(booking_code) as booking_code
            from booking_phone_number group by phone_number having count>=1 and phone_number like ".db_input($phone_number).";
        ";
        return db_query($sql);
    }

    public static function bookingReturingPhoneNumber($from, $to) {
        $sql = "select phone_number, count(distinct date(date)) as count,
            GROUP_CONCAT(date(date)) as date, GROUP_CONCAT(booking_code) as booking_code
            from booking_phone_number 
            where date(date)>=".db_input($from)."
            and date(date)<=".db_input($to)."
            group by phone_number having count>=2 
        ";
        return db_query($sql);
    }
}

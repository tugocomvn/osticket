<?php 
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.flight-ticket.php');

$total = 0;
$page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
$offset = ($page - 1) * PAGE_LIMIT;
$results = FLightTicketFlight::getPagination($offset, $total, PAGE_LIMIT);
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('list-tour-review.php', $qs ?: null);
$page_inc = "list-tour-review.inc.php";
$nav->setActiveTab('operator');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page_inc);
include(STAFFINC_DIR.'footer.inc.php');
?>
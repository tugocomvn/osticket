<?php

if(!defined('INCLUDE_DIR')) die('!');

require_once(INCLUDE_DIR.'class.point.php');
require_once(INCLUDE_DIR.'tugo.user.php');
require_once (INCLUDE_DIR.'class.pax.php');

class PointAjaxAPI extends AjaxController {

    function getNote($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, "Login required");
        elseif (!($note = QuickNote::lookup($id)))
            Http::response(205, "Note not found");

        Http::response(200, $note->display());
    }

    function updatePoint($id) {
        global $thisstaff;
        $point = null;

        $pointUser =  \Tugo\User::getPoint($id);
        if (!$thisstaff)
            $this->response(403, "Login required");
        elseif (!isset($_POST['type']) || !$_POST['type'] || !isset(UserPoint::$types[trim($_POST['type'])]))
            $this->response(422, "Chưa chọn loại");
        elseif (!isset($_POST['change_point']) || !$_POST['change_point'] || !is_numeric(trim($_POST['change_point'])))
            $this->response(422, "Chưa điền số điểm");
        elseif (!isset($_POST['subject']) || !$_POST['subject'])
            $this->response(422, "Chưa điền lý do cập nhật điểm");
        elseif (!isset($_POST['booking_code']) || empty($_POST['booking_code'])) {
            $this->response(422, "Mã booking không được để trống");
        }
        elseif (isset($_POST['booking_code'])) {
            $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', trim($_POST['booking_code']));
            $_booking_code_trim = ltrim($_booking_code_trim, '0');
            $check_1 = Booking::lookup(['booking_code_trim' => $_booking_code_trim,]);
            $check_2 = Booking::lookup(['booking_code' => trim($_POST['booking_code'])]);

            if (!$check_1 && !$check_2) { // dữ liệu sai
                $this->response(422, "Mã booking sai");
            }
        }
        elseif ((trim($_POST['type']) == '-') && abs($pointUser) < abs(intval(trim($_POST['change_point']))))
            $this->response(422, "Không đủ điểm sử dụng");

        $type = trim($_POST['type']);
        $change_point = ($type == '-' ? -1 : 1) * abs(intval(trim($_POST['change_point'])));
        $subject = trim($_POST['subject']);
        $booking_code = trim($_POST['booking_code']);
        $note = trim($_POST['note']);

        $point = UserPoint::create([
            'updater_id' => $thisstaff->getId(),
            'updater' => $thisstaff->getUserName(),
            'subject' => $subject,
            'booking_code' => $booking_code,
            'change_point' => $change_point,
            'note' => $note,
            'user_uuid' => $id,
            'date' => new SqlFunction('NOW'),
        ]);

        if (!$point) Http::response(500, "Unable to create new note");

        if (!$point->save(true))
            $this->response(500, "Unable to save point update");

        \Tugo\User::changePoint($id, $change_point);

        $show_options = true;
        include STAFFINC_DIR . 'templates/point.tmpl.php';
    }

    function deleteNote($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, "Login required");
        elseif (!($note = QuickNote::lookup($id)))
            Http::response(205, "Note not found");
        elseif (!$note->delete())
            Http::response(500, "Unable to remove note");

        Http::response(204, "Deleted notes can be recovered by loading yesterday's backup");
    }

    function createNote($ext_id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, "Login required");
        elseif (!isset($_POST['note']) || !$_POST['note'])
            Http::response(422, "Send `note` parameter");
        elseif (!($note = QuickNote::create(array(
                'staff_id' => $thisstaff->getId(),
                'body' => Format::sanitize($_POST['note']),
                'created' => new SqlFunction('NOW'),
                'ext_id' => $ext_id,
                ))))
            Http::response(500, "Unable to create new note");
        elseif (!$note->save(true))
            Http::response(500, "Unable to create new note");

        $show_options = true;
        include STAFFINC_DIR . 'templates/note.tmpl.php';
    }
}

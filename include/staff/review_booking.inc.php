<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/reservation.css">

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs"  id="myTab">
        <li class="active">
            <a href="#visareview" data-toggle="tab">Visa Review
                <?php if($visaReview && ($rows = db_num_rows($visaReview))): ?>
                    <span class="label label-warning"><?php echo $rows ?></span>
                <?php endif; ?>
            </a>
        </li>
        <li>
            <a href="#visareviewed" data-toggle="tab">Visa Reviewed</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="visareview">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.visareview.inc.php' ?>
            </div>
        </div>
        <div class="tab-pane" id="visareviewed">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.visareviewed.inc.php' ?>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        $(document).ready(function () {
            if (window.location.hash === '')
                activeTab('visareview');
            else
                activeTab(window.location.hash.replace('#', ''));
        });

        function activeTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        }

        $('#myTab a').click(function (e) {
            e.preventDefault();
            window.location.hash = $(e.target).attr("href");
            $(this).tab('show');
        });
    })(jQuery);
</script>

<?php 
require_once(INCLUDE_DIR . 'class.flight-ticket.php');
class FlightTicketAjaxAPI extends AjaxController {
    public static function __validation($__data){

        $col_airport_code_from_to = array_filter(array_column($__data,'airport_code_from_to'));
        if(!isset($col_airport_code_from_to) || empty($col_airport_code_from_to)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Hành trình bay không được để trống.',
                ]
            );
            exit;
        }

        $col_departure_date = array_filter(array_column($__data,'departure_date'));
        if(!isset($col_departure_date) || empty($col_departure_date)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Ngày đi không được để trống.',
                ]
            );
            exit;
        }

        $col_flight_code = array_filter(array_column($__data,'flight_code'));
        if(!isset($col_flight_code) || empty($col_flight_code)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Số hiệu chuyến bay không được để trống.',
                ]
            );
            exit;
        }

        $col_flight_time = array_filter(array_column($__data,'flight_time'));
        if(!isset($col_flight_time) || empty($col_flight_time)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Giờ bay không được để trống.',
                ]
            );
            exit;
        }

        $col_flight_ticket_code = array_filter(array_column($__data,'flight_ticket_code'));
        if(!isset($col_flight_ticket_code) || empty($col_flight_ticket_code)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Mã vé không được để trống.',
                ]
            );
            exit;
        }

        $col_total_quantity = array_filter(array_column($__data,'total_quantity'));
        if(!isset($col_total_quantity) || empty($col_total_quantity)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Số lượng không được để trống.',
                ]
            );
            exit;
        }   
    }    

    public static function changeNameKey2dArray($arr)
    {
        $second_level = array('airport_code_from_to', 'departure_date', 'flight_code', 'flight_time', 'flight_ticket_code', 'total_quantity');
        for($d = 0; $d < count($arr); $d++ )
        {
            for($s = 0; $s < count($arr[$d]); $s++ )
            {
                $output[$d][$second_level[$s]] = $arr[$d][$s];
            }
        }
        return $output;
    }

    public static function format_values() {

        $data_array = $_POST['data'];

        foreach($data_array as $key => $value) {
            $_data_array[$key] = array_filter($value);
        }
        $___data = array_filter($_data_array);
        if(!isset($___data) || empty($___data)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa có nội dung.',
                ]
            );
            exit;
        }
        // change name key 
        $__data = self::changeNameKey2dArray($___data);

        $data = self::__validation($__data);

        foreach ($__data as $key => $value) {
            $data[] = self::format_values_in_form($value);
        }
        $out = array(
            'all_values' => $data,
            'result' => 1,
            'message' => 'Dữ liệu đã được xử lý từ hệ thống. Vui lòng kiểm tra và điều chỉnh kỹ thông tin trước khi nhấn nút "Confirm & Save"',
            'index' => range(-1,2),
        );
        echo json_encode($out);
        exit;
    }

    public static function save_flight_ticket()
    {        
        $data_array = $_POST['data'];
        $airline_id = $_POST['airline'];
        foreach($data_array as $key => $value) {
            $_data_array[$key] = array_filter($value);
        }
        
        if(!isset($airline_id) || empty($airline_id)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Vui lòng chọn hãng bay.',
                ]
            );
            exit;
        }

        $___data = array_filter($_data_array);
        if(!isset($___data) || empty($___data)){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa có nội dung.',
                ]
            );
            exit;
        }

        $__data = self::changeNameKey2dArray($___data);
        $data = self::__validation($__data);
        $flight_ticket_flight = self::save_flight_ticket_flight($__data, $airline_id);
        if(!isset($flight_ticket_flight) || empty($flight_ticket_flight)){
        echo json_encode(
            [
                'result' => 1,
                'message' => 'Lưu thành công trang sẽ tự động chuyển về trang danh sách sau 3s',
            ]
        );
        exit;
        }
    }
    
    public static function save_flight_ticket_flight($__data, $airline_id)
    {
        global $thisstaff;
        if(empty($__data)) return null;
        foreach ($__data as $key => $value) {
            $data = self::get_values_in_form_import($value);
            $_data = [
                'airline_id' => $airline_id,
                'airport_code_from' => $data['airport_code_from'],
                'airport_code_to' => $data['airport_code_to'],
                'flight_code' => $value['flight_code'],
                'departure_at' => $data['departure_at'],
                'arrival_at' => $data['arrival_at'],
                'created_at' => date('Y-m-d H:i:s'), 
                'created_by' => $thisstaff->getId(), 
            ];
            $new = Flight::create($_data);
            $id = $new->save(true);
            $log = FlightLogOP::create(
                [
                    'flight_id' => $id,
                    'content' => json_encode($new->ht),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $thisstaff->getId()
                ]
            );
            $log->save(true);
            $__data = [
                'flight_ticket_code' => $value['flight_ticket_code'],
                'total_quantity' => $value['total_quantity']
            ];
            $_new = FlightTicket::create($__data);
            $_id = $_new->save(true);
            $log = FlightTicketLogTR::create(
                [
                    'flight_ticket_id' => $_id,
                    'content' => json_encode($new->ht),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $thisstaff->getId()
                ]
            );
            $log->save(true);
            $__new = [
                'flight_ticket_id' => $_id,
                'flight_id' => $id
            ];
            $flight_ticket_flight = FlightTicketFlightTR::create($__new);
            $flight_ticket_flight->save(true);
        }
    }


    public static function format_values_in_form($arr) {

        if(empty($arr)) return null;
        $_data = [];
        $airport_code_from_to = $arr['airport_code_from_to'];
        $flight_code = $arr['flight_code'];
        $flight_ticket_code = $arr['flight_ticket_code'];
        $total_quantity = $arr['total_quantity'];
        if(isset($airport_code_from_to)) {
            $_data = [
                "airport_code_from_to" => trim($airport_code_from_to),
                "flight_code" => trim($flight_code),
                "flight_ticket_code" => trim($flight_ticket_code),
                "total_quantity" => trim($total_quantity)
            ];
        }

        $departure_date = $arr['departure_date'];
        $time = $arr['flight_time'];
        $date = self::format_date($departure_date);
        $time_from_arr = self::format_time($time);
        if(isset($date) && isset($time)) {
            $mer_data = array_merge($_data, [
                "departure_at" => date("d/m/Y", strtotime($date)),
                "departure_time" => date("H:i", strtotime($date." ".$time_from_arr[0])),
            ]);
        }   

        $time_to = self::format_time_to($time, $date);
        if(isset($time_to) && isset($date)){
            $mer_data = array_merge($mer_data, [
                "arrival_at" => date("d/m/Y", strtotime($time_to)),
                "arrival_time" => date("H:i", strtotime($time_to)),
            ]);
        }
        if(!$mer_data) return null;
        return $mer_data;
    }

    public static function get_values_in_form_import($arr)
    {
        if(empty($arr)) return null;
        $_data = [];
        $airport_code_from_to = $arr['airport_code_from_to'];
        if(isset($airport_code_from_to)) {
            $airport_code_arr = self::get_values_airport_code_from_to($airport_code_from_to);
            $_data = [
                "airport_code_from" => $airport_code_arr['airport_code_from'],
                "airport_code_to" => $airport_code_arr['airport_code_to'],
            ];
        }
        $departure_date = $arr['departure_date'];
        $time = $arr['flight_time'];
        $date = self::format_date($departure_date);
        $time_from_arr = self::format_time($time);
        if(isset($date) && isset($time)) {
            $mer_data = array_merge($_data, [
                "departure_at" => $date." ".$time_from_arr[0],
            ]);
        }        
        $time_to = self::format_time_to($time,$date);
        if(isset($time_to) && isset($date)){
            $mer_data = array_merge($mer_data, [
                "arrival_at" => $time_to,
            ]);
        }
        if(!$mer_data) return null;
        return $mer_data;
    }

    public static function format_time_to($time_str,$date_str)
    {
        if(!isset($time_str) && !isset($date_str)) return null;
        $plus_day = "+1";
        if (stripos($time_str, $plus_day) !== false) {
            $time_to = self::format_time($time_str);
            $plus_date = date('Y-m-d', strtotime($date_str) + (24*60*60));
            return $plus_date." ".$time_to[1];
        }else{
            $time_to = self::format_time($time_str);
            return $date_str." ".$time_to[1];
        }
    }

    public static function format_date($date_str)
    {
        if(!isset($date_str)) return null;
        $_date_str = substr($date_str, -5);
        return date('Y-m-d', strtotime($_date_str));
    }

    public static function format_time($time_str)
    {
        if(!isset($time_str)) return null;
        $exp_time_str = explode(" ", $time_str);
        foreach($exp_time_str as $key => $value) {
            $new_time[] = substr($value, 0, 2).':'.substr($value, 2, 2);
        }
        return $new_time;
    }

    public static function get_values_airport_code_from_to($str_airport_code)
    {
        if(empty($str_airport_code)) return null;
        $exp_airport_code = explode('-', $str_airport_code);
        if(is_array($exp_airport_code)){
            $name_key_ariport = [
                'airport_code_from',
                'airport_code_to'
            ];
            return array_combine($name_key_ariport, $exp_airport_code);
        }
    }
}
?>

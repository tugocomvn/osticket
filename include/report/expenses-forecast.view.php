<?php
//if(!defined('OSTSCPINC') || !$thisstaff || !($thisstaff->canViewSettlementList() || $thisstaff->canViewExpectedSettlement())) die('Access Denied');

use \koolreport\widgets\koolphp\Table;
use \koolreport\widgets\google\ComboChart;

switch ($_REQUEST['group']) {
    case 'week':
        $select = 'week(pay_date, 5) as week, year(pay_date) as year';
        $group = 'week(pay_date, 5), year(pay_date) ';
        break;
    case 'month':
        $select = 'month(pay_date) as month, year(pay_date) as year';
        $group = 'month(pay_date) , year(pay_date) ';
        break;
    case 'day':
    default:
        $select = 'pay_date';
        $group = 'pay_date';
        break;
}

ComboChart::create(array(
    "dataSource"=>$this->dataStore('booking_pay_forecast'),
    "width"=>"100%",
    "height"=>"500px",
    "columns"=>array(
        "pay_date"=>array(
            "label"=>"Pay Date"
        ),
        "land"=>array(
            "type"=>"number",
            "label"=>"Land",
            "suffix"=>" $",
            "chartType"=>"bar",
        ),
        "flight"=>array(
            "type"=>"number",
            "label"=>"Flight",
            "suffix"=>" $",
            "chartType"=>"bar",
        ),
        "total"=>array(
            "type"=>"number",
            "label"=>"Land+Flight",
            "suffix"=>" $",
            "chartType"=>"line",
        ),
    ),
    "options"=>array(
        "title"=>"Expense Forecast base on bookings"
    )
));

Table::create(array(
    "dataSource"=>$this->dataStore('booking_pay_forecast')
    ,
    "columns"=>array(
        "pay_date"=>array(
            "label"=>"Pay Date",
            "footerText"=>"<strong>TOTAL</strong>",
            'formatValue'=>function($value)
            {
                if (isset($_REQUEST['group']) && $_REQUEST['group'] === 'week') {
                    $value = explode('-', $value);
                    $week_start = new DateTime();
                    $week_start->setISODate($value[1],$value[0]);
                    return "w".$value[0]." ::: ".$week_start->format('d-M-Y');
                }
                return $value;
            }
        ),
        "land"=>array(
            "type"=>"number",
            "label"=>"Land",
            "suffix"=>" $",
            "footer"=>"sum",
        ),
        "flight"=>array(
            "type"=>"number",
            "label"=>"Flight",
            "suffix"=>" $",
            "footer"=>"sum",
        ),
        "total"=>array(
            "type"=>"number",
            "label"=>"Land + Flight",
            "suffix"=>" $",
            "footer"=>"sum",
        ),
    ),
    "showHeader"=>true,
    "showFooter"=>"top",
    "paging"=>array(
        "pageSize"=>20,
        "pageIndex"=>0,
        "align"=>"center"
    ),
    "cssClass"=>array(
        "table"=>"form_table fixed outter_table"
    )
));
?>


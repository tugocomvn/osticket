
<?php
//require_once(__DIR__.'/guest/config.php');
require_once(__DIR__.'/staff.inc.php');


function getList_items($id =LIST_ID){
//    $sql = "SELECT * FROM `ost_list_items` WHERE `list_id`= 22";
//    $result = db_query($sql);
//    return $result;
    //$list = new DynamicList();
    $object = DynamicList::lookup($id);
    return $object->getItems();
}

$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.dashboard" />',
    "$('#content').data('tipNamespace', 'dashboard.dashboard');");
include(STAFFINC_DIR.'header.inc.php');
$results = getList_items();
$link_ = 'https://drive.google.com/file/d/1-IeLEVLrnQPU6gZONRr0pR0QlxRZMcJH/view?usp=sharing';
?>
<h2>DANH SÁCH</h2>

<table class="list" border="0" cellspacing="1" cellpadding="2" style=" width:100%">
    <thead>
    <tr>
        <th>Tên</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($results as $key):?>
        <tr>
            <?php $link= 'read_spreadsheet.php?id='.$key->getId() ?>
            <td><a href ="<?php echo $link ?>"><?php echo $key->getConfiguration()[PROPERTIES1]?></a></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
<?php
include(STAFFINC_DIR.'footer.inc.php');
?>

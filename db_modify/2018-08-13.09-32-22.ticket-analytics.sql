ALTER TABLE ost_groups ADD can_view_ticket_analytics tinyint(1) DEFAULT '0' NULL;

ALTER TABLE ost_user_email ADD source tinyint(1) NULL;

update ost_user_email
set source = case
               when address IN ('phamquocbuu@gmail.com', 'beckerbao29@gmail.com', 'beckerbao@gmail.com') then '1'
               when address IN ('it@tugo.com.vn', 'support@tugo.com.vn' 'support@tugo.vn') then '2'
               else '3' end;


-- --

update ost_user_email
set source = case
               when address IN ('phamquocbuu@gmail.com', 'beckerbao29@gmail.com', 'beckerbao@gmail.com') then '1'
               when address IN ('it@tugo.com.vn', 'support@tugo.com.vn' 'support@tugo.vn') then '2'
               else '3' end;

-- -- ----
-- source by day
select date(t.created) as date, e.source, count(distinct t.ticket_id) as total
from ost_ticket t
       join ost_ticket_thread tr on t.ticket_id = tr.ticket_id
       join ost_user u on u.id = (select user_id
                                  from ost_ticket_thread
                                  where ost_ticket_thread.ticket_id = t.ticket_id
                                  order by id asc
                                  limit 1)
       join ost_user_email e on e.user_id = u.id
WHERE date(t.created) >= '2018-02-01'
  AND date(t.created) <= '2018-05-01'
group by date(t.created), e.source
order by date(t.created) desc;
-- ------

-- total ---

SELECT date(tr.created) as date, count(distinct tr.ticket_id) as total
FROM ost_ticket tr
WHERE date(tr.created) >= '2018-06-01'
  AND date(tr.created) <= '2018-06-30'
  AND tr.dept_id IN (1, 7)
group by date(tr.created)
order by date(tr.created) asc;

-- --
-- source count

select e.source, count(distinct t.ticket_id) as total
from ost_ticket t
       join ost_user u on u.id = (select user_id
                                  from ost_ticket_thread
                                  where ost_ticket_thread.ticket_id = t.ticket_id
                                  order by id asc
                                  limit 1)
       join ost_user_email e on e.user_id = u.id
WHERE date(t.created) >= '2018-06-01'
  AND date(t.created) <= '2018-06-30'
  AND t.dept_id IN (1, 7)
group by e.source
order by count(distinct t.ticket_id) desc;

-- --
-- source by day
select date(t.created) as date, e.source, count(distinct t.ticket_id) as total
from ost_ticket t
       join ost_ticket_thread tr on t.ticket_id = tr.ticket_id
       join ost_user u on u.id = (select user_id
                                  from ost_ticket_thread
                                  where ost_ticket_thread.ticket_id = t.ticket_id
                                  order by id asc
                                  limit 1)
       join ost_user_email e on e.user_id = u.id
WHERE date(t.created) >= '2018-06-01'
  AND date(t.created) <= '2018-06-30'
  AND t.dept_id IN (1, 7)
group by date(t.created), e.source
order by date(t.created) desc;
;

-- --
-- tag count
select ifnull(tt.tag_id, 0) as tag_id, count(distinct t.ticket_id) as total
from ost_ticket t
       left join ost_ticket_tag tt on t.ticket_id = tt.ticket_id
WHERE date(t.created) >= '2018-06-01'
  AND date(t.created) <= '2018-06-30'
  AND t.dept_id IN (1, 7)
group by tt.tag_id
order by count(distinct t.ticket_id) desc;

-- --
-- tag by day
select date(t.created) as date, tt.tag_id, count(distinct t.ticket_id) as total
from ost_ticket t
       join ost_ticket_tag tt on t.ticket_id = tt.ticket_id
WHERE date(t.created) >= '2018-06-01'
  AND date(t.created) <= '2018-06-30'
  AND t.dept_id IN (1, 7)
group by date(t.created), tt.tag_id
order by date(t.created) ASC;

-- --
CREATE TABLE account_item
(
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(64),
  description varchar(255),
  default_quantity mediumint,
  default_value int
);
CREATE INDEX account_item_name_index ON account_item (name);
CREATE INDEX account_item_default_quantity_index ON account_item (default_quantity);
CREATE INDEX account_item_default_value_index ON account_item (default_value);


CREATE TABLE account_expected
(
  tour_id int,
  account_item_id int,
  quantity mediumint,
  value decimal(10,2),
  total decimal(12,2),
  rate decimal(6),
  total_vnd decimal(14),
  CONSTRAINT account_expected_tour_id_account_item_id_pk PRIMARY KEY (tour_id, account_item_id)
);

ALTER TABLE account_item RENAME TO account_spend_item;
ALTER TABLE account_expected DROP rate;
ALTER TABLE account_expected RENAME TO account_expected_detail;

CREATE TABLE account_general
(
  tour_id int PRIMARY KEY,
  expected_profit decimal(12,2),
  expected_income decimal(12,2),
  expected_outcome decimal(12,2),
  actual_profit decimal(12,2),
  actual_income decimal(12,2),
  actual_outcome decimal(12,2),
  rate decimal(7),
  note text
);
ALTER TABLE account_general ADD booking_income decimal(12,2) NULL;
ALTER TABLE account_general
  MODIFY COLUMN booking_income decimal(12,2) AFTER actual_income;

ALTER TABLE ost_groups ADD op_view_actual_settlement tinyint(1) DEFAULT '0' NULL;
ALTER TABLE ost_groups ADD op_edit_settlement tinyint(1) DEFAULT '0' NULL;
ALTER TABLE ost_groups ADD op_view_settlement_list tinyint(1) DEFAULT '0' NULL;
ALTER TABLE ost_groups ADD op_view_expected_settlement tinyint(1) DEFAULT '0' NULL;

-- ---------
ALTER TABLE account_expected_detail ADD created_by int NULL;
ALTER TABLE account_expected_detail ADD created_at int NULL;
CREATE INDEX account_expected_detail_created_at_index ON account_expected_detail (created_at);
CREATE INDEX account_expected_detail_quantity_index ON account_expected_detail (quantity);
CREATE INDEX account_expected_detail_total_index ON account_expected_detail (total);
CREATE INDEX account_expected_detail_value_index ON account_expected_detail (value);

ALTER TABLE account_expected_detail ADD updated_at int NULL;
ALTER TABLE account_expected_detail ADD updated_by int NULL;

ALTER TABLE account_spend_item ADD created_at datetime NULL;
ALTER TABLE account_spend_item ADD created_by int NULL;
ALTER TABLE account_spend_item ADD updated_at datetime NULL;
ALTER TABLE account_spend_item ADD updated_by int NULL;
ALTER TABLE account_expected_detail MODIFY created_at datetime;
ALTER TABLE account_expected_detail MODIFY updated_at datetime;

ALTER TABLE account_spend_item ADD status tinyint(1) NULL;
CREATE INDEX account_spend_item_status_index ON account_spend_item (status);

ALTER TABLE account_spend_item CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE account_expected_detail CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE account_general CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE account_general ADD created_at datetime NULL;
ALTER TABLE account_general ADD created_by int NULL;
ALTER TABLE account_general ADD updated_at datetime NULL;
ALTER TABLE account_general ADD updated_by int NULL;


create table account_actual_detail
(
  tour_id int not null,
  account_item_id int not null,
  quantity mediumint null,
  value decimal(10,2) null,
  total decimal(12,2) null,
  total_vnd decimal(14) null,
  created_by int null,
  created_at datetime null,
  updated_at datetime null,
  updated_by int null,
  primary key (tour_id, account_item_id)
)
  collate=utf8_unicode_ci
;

create index account_actual_detail_created_at_index
  on account_actual_detail (created_at)
;

create index account_actual_detail_quantity_index
  on account_actual_detail (quantity)
;

create index account_actual_detail_total_index
  on account_actual_detail (total)
;

create index account_actual_detail_value_index
  on account_actual_detail (value)
;


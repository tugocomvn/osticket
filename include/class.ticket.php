<?php
/*********************************************************************
    class.ticket.php

    The most important class! Don't play with fire please.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
include_once(INCLUDE_DIR.'class.thread.php');
include_once(INCLUDE_DIR.'class.staff.php');
include_once(INCLUDE_DIR.'class.client.php');
include_once(INCLUDE_DIR.'class.team.php');
include_once(INCLUDE_DIR.'class.email.php');
include_once(INCLUDE_DIR.'class.dept.php');
include_once(INCLUDE_DIR.'class.topic.php');
include_once(INCLUDE_DIR.'class.lock.php');
include_once(INCLUDE_DIR.'class.file.php');
include_once(INCLUDE_DIR.'class.attachment.php');
include_once(INCLUDE_DIR.'class.banlist.php');
include_once(INCLUDE_DIR.'class.template.php');
include_once(INCLUDE_DIR.'class.variable.php');
include_once(INCLUDE_DIR.'class.priority.php');
include_once(INCLUDE_DIR.'class.sla.php');
include_once(INCLUDE_DIR.'class.canned.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.user.php');
require_once(INCLUDE_DIR.'class.collaborator.php');
require_once(INCLUDE_DIR.'class.calendar.php');
require_once(INCLUDE_DIR.'class.booking.php');
require_once(INCLUDE_DIR.'sheet.google.php');
require_once(INCLUDE_DIR . 'tugo.user.php');
require_once(INCLUDE_DIR . 'tugo.notification.php');
require_once(INCLUDE_DIR . 'class.point.php');
require_once(INCLUDE_DIR . 'class.tag.php');


class Ticket {

    var $id;
    var $number;

    var $ht;

    var $lastMsgId;

    var $status;
    var $dept;  //Dept obj
    var $sla;   // SLA obj
    var $staff; //Staff obj
    var $client; //Client Obj
    var $team;  //Team obj
    var $topic; //Topic obj
    var $tlock; //TicketLock obj

    var $thread; //Thread obj.
    var $calendar; //Calendar obj.
    const TABLE_NAME = 'offlate_tmp';

    function Ticket($id) {
        $this->id = 0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT  ticket.*, lock_id, dept_name '
            .' ,IF(sla.id IS NULL, NULL, '
                .'DATE_ADD(ticket.created, INTERVAL sla.grace_period HOUR)) as sla_duedate '
            .' ,count(distinct attach.attach_id) as attachments'
            .' FROM '.TICKET_TABLE.' ticket '
            .' LEFT JOIN '.DEPT_TABLE.' dept ON (ticket.dept_id=dept.dept_id) '
            .' LEFT JOIN '.SLA_TABLE.' sla ON (ticket.sla_id=sla.id AND sla.isactive=1) '
            .' LEFT JOIN '.TICKET_LOCK_TABLE.' tlock
                ON ( ticket.ticket_id=tlock.ticket_id AND tlock.expire>NOW()) '
            .' LEFT JOIN '.TICKET_ATTACHMENT_TABLE.' attach
                ON ( ticket.ticket_id=attach.ticket_id) '
            .' WHERE ticket.ticket_id='.db_input($id)
            .' GROUP BY ticket.ticket_id';

        //echo $sql;
        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;
        $this->ht = db_fetch_array($res);
        $this->id       = $this->ht['ticket_id'];
        $this->number   = $this->ht['number'];
        $this->unassign   = $this->ht['unassign'];
        $this->_answers = array();

        $this->loadDynamicData();

        //Reset the sub classes (initiated ondemand)...good for reloads.
        $this->status= null;
        $this->staff = null;
        $this->client = null;
        $this->team  = null;
        $this->dept = null;
        $this->sla = null;
        $this->tlock = null;
        $this->stats = null;
        $this->topic = null;
        $this->thread = null;
        $this->collaborators = null;

        return true;
    }

    function loadDynamicData() {
        if (!$this->_answers) {
            foreach (DynamicFormEntry::forTicket($this->getId(), true) as $form) {
                foreach ($form->getAnswers() as $answer) {
                    $tag = mb_strtolower($answer->getField()->get('name'))
                        ?: 'field.' . $answer->getField()->get('id');
                        $this->_answers[$tag] = $answer;
                }
            }
        }
        return $this->_answers;
    }

    static function importFromPost($stuff, $extra=array()) {
        if (is_array($stuff) && !$stuff['error']) {
            // Properly detect Macintosh style line endings
            ini_set('auto_detect_line_endings', true);
            $stream = fopen($stuff['tmp_name'], 'r');
        }
        elseif ($stuff) {
            $stream = fopen('php://temp', 'w+');
            fwrite($stream, $stuff);
            rewind($stream);
        }
        else {
            return __('Unable to parse submitted tickets');
        }

        return Ticket::importCsv($stream, $extra);
    }

    static function importPaymentFromPost($stuff, $extra=array()) {
        if (is_array($stuff) && !$stuff['error']) {
            // Properly detect Macintosh style line endings
            ini_set('auto_detect_line_endings', true);
            $stream = fopen($stuff['tmp_name'], 'r');
        }
        elseif ($stuff) {
            $stream = fopen('php://temp', 'w+');
            fwrite($stream, $stuff);
            rewind($stream);
        }
        else {
            return __('Unable to parse submitted tickets');
        }

        return Ticket::importPaymentCsv($stream, $extra);
    }

    static function importBookingFromPost($stuff, $extra=array()) {
        if (is_array($stuff) && !$stuff['error']) {
            // Properly detect Macintosh style line endings
            ini_set('auto_detect_line_endings', true);
            $stream = fopen($stuff['tmp_name'], 'r');
        }
        elseif ($stuff) {
            $stream = fopen('php://temp', 'w+');
            fwrite($stream, $stuff);
            rewind($stream);
        }
        else {
            return __('Unable to parse submitted tickets');
        }

        return Ticket::importBookingCsv($stream, $extra);
    }

    static function importCsv($stream, $defaults=array()) {
        global $ost, $cfg, $thisclient, $_FILES;
        $path = implode(DIRECTORY_SEPARATOR, [ROOT_DIR, 'public_file', 'import_tickets']);
        $output_name = "file_result_".microtime(1).".csv";

        if (!is_dir($path))
            mkdir($path, 0777);
        $fp = fopen($path.DIRECTORY_SEPARATOR.$output_name, 'w');

        $error_list = [];
        $ticket_success = [];
        $ticket_error = [];
        $fail_count = 0;

        $default_headers = array(
            'status'   => __('STATUS'),
            'name' => __('TEN KHACH'),
            'phone' => __('SDT'),
            'email' => __('EMAIL'),
            'tour' => __('TEN TOUR'),
            'quantity' => __('SO LUONG'),
            'requirement' => __('YEU CAU'),
            'note' => __('GHI CHU 1'),
            'note2' => __('GHI CHU 2'),
            'staff' => __('NV PHU TRACH'),
            'date' => __('NGAY XU LY'),
        );

        fputcsv($fp, array_merge($default_headers, ['ERROR']));

        if (!($data = fgetcsv($stream, 1000, ",")))
            return __('Whoops. Perhaps you meant to send some CSV records');

        $headers = array();

        // check required columns
        foreach ($data as $h) {
            foreach ($default_headers as $_name => $_label) {
                if (in_array(
                    mb_strtolower($h),
                    array( mb_strtolower($_name), mb_strtolower($_label) )
                )) {
                    $not_found = false;
                    if (!$_name)
                        return sprintf(__(
                            '%s: Field must have `variable` set to be imported'), $h);
                    $headers[$_name] = $_label;
                    break;
                } else {
                    $not_found = "$_name or $_label";
                }
            }

            if ($not_found) return __(sprintf('CSV file must include column labeled %', $not_found));
        }

        // 'name' and 'email' MUST be in the headers
        if (!isset($headers['name']) || !isset($headers['phone']))
            return __('CSV file must include `name` and `phone` columns');

        $tickets = $keys = array();

        while (($data = fgetcsv($stream, 1000, ",")) !== false) {
            $errors = [];
            if (count($data) == 1 && $data[0] == null)
                // Skip empty rows
                continue;
            elseif (count($data) != count($headers))
                $errors['data'] = sprintf(__('Bad data. Expected: %s'), implode(', ', $headers));

            // Validate according to field configuration
            $i = 0;
            $topicId = 1; // general inquiry
            $topic = Topic::lookup($topicId);
            $deptId = 1; // sales tugo
            $ipaddress = $_SERVER['REMOTE_ADDR'];
            $source = 'API';

            if (empty($data[1])) // required
                $errors['name'] = __('Customer name is required');
            if (empty($data[2]) && empty($data[3])) // required
                $errors['phone'] = __('Email OR Phone number is required');

            if (!empty($data[2]) && $data[2][0] != '0') // nếu số đt không bắt đầu là zero thì thêm zero
                $data[2] = '0'.$data[2];

            if (empty($data[3]) && !empty($data[2])) // nếu không có email thì tự thêm email dạng <phone>@tugo.com.vn
                $data[3] = $data[2].'@tugo.com.vn';

            // check user theo email, tạo user nếu chưa có
            $user = false;
            if (!empty($data[3]))
                $user = User::lookupByEmail($data[3]);
            $vars = [ // for user info
                'email' => $data[3],
                'phone' => $data[2],
                'name' => $data[1],
            ];

            if (!$user) {
                // Reject emails if not from registered clients (if
                // configured)
                $user_form = UserForm::getUserForm()->getForm($vars);
                if (!($user=User::fromVars($user_form->getClean())))
                    $errors['info'] = __('Incomplete client information');
            }

            $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
            if (!$number) $errors['number'] = __('Invalid ticket number');

            if (isset($errors) && count($errors)) {
                fputcsv($fp, array_merge($data, $errors));
                $fail_count++;
                continue;
            }

            // prepare data
            $vars = array_merge($vars, [
                'source' => $source,
                'topicId' => $topicId,
                'assignId' => '0',
                'subject' => trim($data[4]),
                'deptId' => $deptId,
                'message' => trim(sprintf(
                    "<p><strong>Khách hàng</strong>: %s</p>
                    <p><strong>SĐT</strong>: %s</p>
                    <p><strong>Email</strong>: %s</p>
                    <p>_________________________________</p>
                    <p><strong>Tên tour</strong>: %s</p>
                    <p><strong>Số lượng</strong>: %s</p>
                    <p><strong>Yêu cầu</strong>: %s</p>",
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[1]),
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[2]),
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[3]),
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[4]),
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[5]),
                    str_replace(["\r\n", "\n", "\r"], "<br />", $data[6])
                ))
            ]);

            // check if this ticket was created or not
            $sql = sprintf(
                "SELECT DISTINCT t.ticket_id, t.number FROM %s t JOIN %s tt ON t.ticket_id=tt.ticket_id
                WHERE tt.user_id=%d
                AND tt.poster LIKE %s
                AND tt.source LIKE %s
                AND tt.title LIKE %s
                AND (tt.body LIKE %s 
                    AND tt.body LIKE %s 
                    AND tt.body LIKE %s 
                    AND tt.body LIKE %s 
                    AND tt.body LIKE %s 
                    AND tt.body LIKE %s
                    )
                LIMIT 1",
                TICKET_TABLE,
                TICKET_THREAD_TABLE,
                db_input($user->getId()),
                db_input($user->getName()),
                db_input('API'),
                db_input('%'.$vars['subject'].'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[1]).'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[2]).'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[3]).'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[4]).'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[5]).'%'),
                db_input('%'.str_replace(["\r\n", "\n", "\r"], "<br />", $data[6]).'%')
            );

            $res = db_query($sql);
            if ($res) {
                $row = db_fetch_array($res);
                if ($row && isset($row['number']) && $row['number'])
                // oh shit, this row was imported
                    $errors['ticket'] = sprintf("Ticket exists: number [%s] / id [%d]", $row['number'], $row['ticket_id']);
            }

            if (!empty($data[7]))
                $vars['message'] .= sprintf(
                    '<p><strong>Ghi chú 1</strong>: %s</p>',
                    str_replace(["\r\n","\r","\n"], '<br />', $data[7])
                );

            if (!empty($data[8]))
                $vars['message'] .= sprintf(
                    '<p><strong>Ghi chú 2</strong>: %s</p>',
                    str_replace(["\r\n","\r","\n"], '<br />', $data[8])
                );

            if (!empty ($data[9])) {
                $assignee = strtolower(_String::khongdau($data[9]));
                $staff_list = [
                    'ngoc'  => 'ngoc.tran',
                    'trang' => 'trang.nguyen',
                    'lua'   => 'lua.nguyen',
                    'minh'  => 'minh.do',
                    'vy'    => 'vy.do',
                    'hanh'  => 'hanh.bui',
                    'dang'  => 'dang.le',
                    'vi'    => 'vi.ha',
                    'thu'   => 'thu.bui',
                ];

                if (isset($staff_list[$assignee]) && !empty($staff_list[$assignee]))
                    $vars['assignId'] = Staff::getIdByUsername($staff_list[$assignee]);
            }

            if (empty($data[10]) || !strtotime($data[10]))
                $issue_date = (new DateTime())->format("Y-m-d H:i:s");
            $issue_date = date("Y-m-d H:i:s", strtotime($data[10]));
            $vars['created'] = $issue_date;
            $ticket_form = TicketForm::getInstance();
            $fields = $ticket_form->getFields();
            foreach ($fields as $_id => $_field) { // set default value for paymentstatus
                if ($_field->get('name') == 'paymentstatus' && !isset($vars['paymentstatus']))
                    $vars['paymentstatus'] = $_field->getConfiguration()['default'];
            }

            $_errors = [];
            if (!($ticket = Ticket::create($vars, $_errors, 'API')) || !($_id = $ticket->getId())) {
                fputcsv($fp, array_merge($data, $_errors));
                $fail_count++;
            } else {
                $status = trim($data[0]);
                $status = strtolower(_String::khongdau($status));
                if ('huy' == $status)
                    $ticket->setStatus(7); // canceled

                $ticket_success[] = ["id" => $_id, "number" => $ticket->getNumber()];
            }
        } // END while

        fclose($fp);

        return [
            'ticket_success' => $ticket_success,
            'ticket_error' => $ticket_error,
            'file' => $output_name,
            'fail_count' => $fail_count,
        ];
    }

    static function importPaymentCsv($stream, $defaults=array()) {
        global $ost, $cfg, $thisclient, $_FILES;
        $path = implode(DIRECTORY_SEPARATOR, [ROOT_DIR, 'public_file', 'import_tickets']);
        $output_name = "file_result_payment_".microtime(1).".csv";

        if (!is_dir($path))
            mkdir($path, 0777);
        $fp = fopen($path.DIRECTORY_SEPARATOR.$output_name, 'w');

        $error_list = [];
        $ticket_success = [];
        $ticket_error = [];
        $fail_count = 0;
        $tickets = $keys = array();

        $sql = sprintf(
            "SELECT id, `name`, form_id FROM %s WHERE form_id IN (%s)",
            FORM_FIELD_TABLE,
            implode(',', unserialize(IO_FORM))
        );

        $io_form = [];
        if (($res = db_query($sql))) {
            while (($row = db_fetch_array($res)))
                $io_form[$row['form_id']][$row['name']] = intval($row['id']);
        }

        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $title_list = [
            'time' => 0,
            'booking_code' => 1,
            'receipt_code' => 2,
            'amount' => 5,
            'note' => 6,
            'quantity' => 4,
        ];

        while (($data = fgetcsv($stream, 1000, ",")) !== false) {
            $errors = [];
            if (count($data) == 1 && !$data[0])
                // Skip empty rows
                continue;
            foreach ($data as $_i => $_v)
                $data[$_i] = trim($_v);
            // Validate according to field configuration
            $i = 0;
            $if_thu = $data[5] > 0 ? true : false;
            $topicId = $if_thu ? THU_TOPIC : CHI_TOPIC; // general inquiry
            $formId = $if_thu ? THU_FORM : CHI_FORM; // general inquiry
            $topic = Topic::lookup($topicId);
            $deptId = 6; // finance tugo
            $source = 'API';

            if (empty($data[5])) // required
                $errors['money'] = __('Money Amount is required');

            $data[5] = str_replace('.', '', $data[5]);
            $data[5] = str_replace(',', '', $data[5]);

            if  (!intval($data[5]))
                $errors['money'] = __('Money Amount must be a number');

            $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
            if (!$number) $errors['number'] = __('Invalid ticket number');

            if (empty($data[0]) || !$data[0] || !date_create_from_format('d/m/Y', $data[0]))
                $errors['date'] = __('Invalid date');

            if (isset($errors) && count($errors)) {
                fputcsv($fp, array_merge($data, $errors));
                $fail_count++;
                continue;
            }

            if (!empty($data[0]) && ($_tmp = date_create_from_format('d/m/Y', $data[0])->format('Y-m-d 00:00:00')))
                $issue_date = $_tmp;

            $_note_arr = [];
            if (!empty($data[4]))
                $_note_arr[] = "[".$data[4]."]";
            if (!empty($data[3]))
                $_note_arr[] = "[".$data[3]."]";
            if (!empty($data[6]))
                $_note_arr[] = "[".$data[6]."]";
            $__note = implode(' - ', $_note_arr);

            $sql='INSERT INTO '.TICKET_TABLE.' SET '
                .' created='.(isset($issue_date) && !empty($issue_date) ? db_input($issue_date) : ' NOW() ')
                .' ,lastmessage= NOW()'
                .' ,`number`='.db_input($number)
                .' ,dept_id='.db_input($deptId)
                .' ,topic_id='.db_input($topicId)
                .' ,ip_address='.db_input($ipaddress)
                .' ,source='.db_input($source);

            $_errors = [];
            if (!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id))) {

                fputcsv($fp, array_merge($data, $_errors));
                $fail_count++;
            } else {
                $sql = sprintf(
                    "INSERT INTO %s SET form_id = %s
                    , object_id = %s
                    , object_type = %s
                    , `sort` = %s
                    , created = %s
                    , updated = %s ON DUPLICATE KEY UPDATE form_id = %s
                    , object_id = %s
                    , object_type = %s
                    , `sort` = %s
                    , created = %s
                    , updated = %s",
                    db_input(FORM_ENTRY_TABLE, false),
                    db_input($formId),
                    db_input($id),
                    db_input('T'),
                    db_input(0),
                    db_input(date('Y-m-d H:i:s')),
                    db_input(date('Y-m-d H:i:s')),
                    db_input($formId),
                    db_input($id),
                    db_input('T'),
                    db_input(0),
                    db_input(date('Y-m-d H:i:s')),
                    db_input(date('Y-m-d H:i:s'))
                );

                if (!@db_query($sql) || !($_id=db_insert_id())) {
                    fputcsv($fp, array_merge($data, $_errors));
                    $fail_count++;
                } else {
                    $fuck = [];
                    foreach ($title_list as $_name => $_index) {
                        $fuck[$_name] = $data[$_index];

                        if (!isset($io_form[$formId][$_name]) || !$io_form[$formId][$_name]) continue;

                        if ('time' == $_name && strtotime($issue_date))
                            $data[$_index] = strtotime($issue_date);

                        elseif ('note' == $_name && $__note)
                            $data[$_index] = trim($__note);

                        elseif ('amount' == $_name)
                            $data[$_index] = abs($data[$_index]);

                        elseif ('booking_code' == $_name)
                            $booking_code = trim($data[$_index]);

                        if (!isset($data[$_index]) || !$data[$_index]) continue;

                        $sql = sprintf(
                            "REPLACE INTO %s SET entry_id = %s
                        , field_id = %s
                        , `value` = %s",
                            db_input(FORM_ANSWER_TABLE, false),
                            db_input($_id),
                            db_input($io_form[$formId][$_name]),
                            db_input($data[$_index])
                        );

                        db_query($sql);
                    }

                    Payment::push($ticket->getId());
                    $ticket_success[] = ["id" => $id, "number" => $ticket->getNumber()];
                }
            }
        } // END while

        fclose($fp);

        return [
            'ticket_success' => $ticket_success,
            'ticket_error' => $ticket_error,
            'file' => $output_name,
            'fail_count' => $fail_count,
        ];
    }

    static function importBookingCsv($stream, $defaults=array()) {
        global $ost, $cfg, $thisclient, $_FILES;
        $path = implode(DIRECTORY_SEPARATOR, [ROOT_DIR, 'public_file', 'import_tickets']);
        $output_name = "file_result_booking_".microtime(1).".csv";

        if (!is_dir($path))
            mkdir($path, 0777);
        $fp = fopen($path.DIRECTORY_SEPARATOR.$output_name, 'w');

        $error_list = [];
        $ticket_success = [];
        $ticket_error = [];
        $fail_count = 0;
        $tickets = $keys = array();

        $sql = sprintf(
            "SELECT id, `name`, form_id FROM %s WHERE form_id IN (%s)",
            FORM_FIELD_TABLE,
            implode(',', [BOOKING_FORM])
        );

        $booking_form = [];
        if (($res = db_query($sql))) {
            while (($row = db_fetch_array($res)))
                $booking_form[$row['form_id']][$row['name']] = intval($row['id']);
        }

        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $title_list = [
            'booking_code' => 0,
            'status' => 1,
            'customer' => 2,
            'phone_number' => 3,
            'quantity' => 6,
            'receipt_code' => 17,
            'referral_code' => 18,
            'note' => 19,
        ];

        while (($data = fgetcsv($stream, 1000, ",")) !== false) {
            $errors = [];
            if (!count($data) || count($data) == 1)
                // Skip empty rows
                continue;

            if (strlen($data[0]) < 5) continue;

            foreach ($data as $_i => $_v)
                $data[$_i] = trim($_v);
            // Validate according to field configuration
            $i = 0;
            $topicId = BOOKING_TOPIC;
            $formId = BOOKING_FORM;
            $topic = Topic::lookup($topicId);
            $deptId = 1; // sales tugo
            $source = 'API';

            if (empty($data[0])) // required
                $errors['booking_code'] = __('Booking code is required');
            if (empty($data[2])) // required
                $errors['customer'] = __('Customer name is required');

            $data[5] = str_replace('.', '', $data[5]);
            $data[5] = str_replace(',', '', $data[5]);
            $data[5] = intval($data[5]);

            $data[6] = intval($data[6]);
            $data[17] = trim($data[17]);
            $data[18] = trim($data[18]);

            $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
            if (!$number) $errors['number'] = __('Invalid ticket number');

            if (isset($errors) && count($errors)) {
                fputcsv($fp, array_merge($data, $errors));
                $fail_count++;
                continue;
            }

            if (!empty($data[0])
                && date_create_from_format('d/m/Y', $data[0])
                && ($_tmp = date_create_from_format('d/m/Y', $data[0])->format('Y-m-d H:i:s'))
            )
                $issue_date = $_tmp;
            else
                $issue_date = (new DateTime())->format("Y-m-d H:i:s");

            $_note_arr = [];
            foreach ($data as $d) {
                if (!empty($d))
                    $_note_arr[] = "[".trim($d)."]";
            }
            $__note = implode(' - ', $_note_arr);

            $sql='INSERT INTO '.TICKET_TABLE.' SET '
                .' created='.(isset($issue_date) && !empty($issue_date) ? db_input($issue_date) : ' NOW() ')
                .' ,lastmessage= NOW()'
                .' ,`number`='.db_input($number)
                .' ,dept_id='.db_input($deptId)
                .' ,topic_id='.db_input($topicId)
                .' ,ip_address='.db_input($ipaddress)
                .' ,source='.db_input($source);

            $_errors = [];
            if (!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id))) {

                fputcsv($fp, array_merge($data, $_errors));
                $fail_count++;
            } else {
                $sql = sprintf(
                    "INSERT INTO %s SET form_id = %s
                    , object_id = %s
                    , object_type = %s
                    , `sort` = %s
                    , created = %s
                    , updated = %s ON DUPLICATE KEY UPDATE form_id = %s
                    , object_id = %s
                    , object_type = %s
                    , `sort` = %s
                    , created = %s
                    , updated = %s",
                    db_input(FORM_ENTRY_TABLE, false),
                    db_input($formId),
                    db_input($id),
                    db_input('T'),
                    db_input(0),
                    db_input(date('Y-m-d H:i:s')),
                    db_input(date('Y-m-d H:i:s')),
                    db_input($formId),
                    db_input($id),
                    db_input('T'),
                    db_input(0),
                    db_input(date('Y-m-d H:i:s')),
                    db_input(date('Y-m-d H:i:s'))
                );

                if (!@db_query($sql) || !($_id=db_insert_id())) {
                    fputcsv($fp, array_merge($data, $_errors));
                    $fail_count++;
                } else {
                    foreach ($title_list as $_name => $_index) {
                        if (!isset($booking_form[$formId][$_name]) || !$booking_form[$formId][$_name]) continue;

                        if ('time' == $_name && strtotime($issue_date))
                            $data[$_index] = strtotime($issue_date);

                        elseif ('note' == $_name && $__note)
                            $data[$_index] = trim($__note);

                        elseif ('amount' == $_name)
                            $data[$_index] = abs($data[$_index]);

                        if (!isset($data[$_index]) || !$data[$_index]) continue;

                        $sql = sprintf(
                            "REPLACE INTO %s SET entry_id = %s
                        , field_id = %s
                        , `value` = %s",
                            db_input(FORM_ANSWER_TABLE, false),
                            db_input($_id),
                            db_input($booking_form[$formId][$_name]),
                            db_input($data[$_index])
                        );

                        db_query($sql);
                    }

                    Booking::push($ticket->getId());
                }
                $ticket_success[] = ["id" => $id, "number" => $ticket->getNumber()];
            }
        } // END while

        fclose($fp);

        return [
            'ticket_success' => $ticket_success,
            'ticket_error' => $ticket_error,
            'file' => $output_name,
            'fail_count' => $fail_count,
        ];
    }

    function reload() {
        return $this->load();
    }

    function hasState($state) {
        return  (strcasecmp($this->getState(), $state)==0);
    }

    function isOpen() {
        return $this->hasState('open');
    }

    function isReopened() {
        return ($this->getReopenDate());
    }

    function isReopenable() {
        return $this->getStatus()->isReopenable();
    }

    function isClosed() {
         return $this->hasState('closed');
    }

    function isArchived() {
         return $this->hasState('archived');
    }

    function isDeleted() {
         return $this->hasState('deleted');
    }

    function isAssigned() {
        return ($this->isOpen() && ($this->getStaffId() || $this->getTeamId()));
    }

    function isOverdue() {
        return ($this->ht['isoverdue']);
    }

    function isAnswered() {
       return ($this->ht['isanswered']);
    }

    function isLocked() {
        return ($this->getLockId());
    }

    function checkStaffAccess($staff) {

        if(!is_object($staff) && !($staff=Staff::lookup($staff)))
            return false;

        // Staff has access to the department.
        if (!$staff->showAssignedOnly()
                && $staff->canAccessDept($this->getDeptId()))
            return true;

        // Only consider assignment if the ticket is open
        if (!$this->isOpen())
            return false;

        // Check ticket access based on direct or team assignment
        if ($staff->getId() == $this->getStaffId()
                || ($this->getTeamId()
                    && $staff->isTeamMember($this->getTeamId())
        ))
            return true;

        // No access bro!
        return false;
    }

    function checkUserAccess($user) {

        if (!$user || !($user instanceof EndUser))
            return false;

        //Ticket Owner
        if ($user->getId() == $this->getUserId())
            return true;

        //Collaborator?
        // 1) If the user was authorized via this ticket.
        if ($user->getTicketId() == $this->getId()
                && !strcasecmp($user->getRole(), 'collaborator'))
            return true;

        // 2) Query the database to check for expanded access...
        if (Collaborator::lookup(array(
                        'userId' => $user->getId(),
                        'ticketId' => $this->getId())))
            return true;

        return false;
    }

    //Getters
    function getId() {
        return  $this->id;
    }

    function getNumber() {
        return $this->number;
    }

    function getUnassign() {
        return $this->unassign;
    }

    function getOwnerId() {
        return $this->ht['user_id'];
    }

    function getOwner() {

        if (!isset($this->owner)
                && ($u=User::lookup($this->getOwnerId())))
            $this->owner = new TicketOwner(new EndUser($u), $this);

        return $this->owner;
    }

    function getEmail(){
        if ($o = $this->getOwner())
            return $o->getEmail();

        return null;
    }

    function getReplyToEmail() {
        //TODO: Determine the email to use (once we enable multi-email support)
        return $this->getEmail();
    }

    function getAuthToken() {
        # XXX: Support variable email address (for CCs)
        return md5($this->getId() . strtolower($this->getEmail()) . SECRET_SALT);
    }

    function getName(){
        if ($o = $this->getOwner())
            return $o->getName();
        return null;
    }

    function getSubject() {
        return (string) $this->_answers['subject'];
    }

    function getBookingCode() {
        return isset($this->_answers['bookingcode']) ? (string) $this->_answers['bookingcode'] : '';
    }

    /* Help topic title  - NOT object -> $topic */
    function getHelpTopic() {

        if(!$this->ht['helptopic'] && ($topic=$this->getTopic()))
            $this->ht['helptopic'] = $topic->getFullName();

        return $this->ht['helptopic'];
    }

    function getCreateDate() {
        return $this->ht['created'];
    }

    function getOpenDate() {
        return $this->getCreateDate();
    }

    function getReopenDate() {
        return $this->ht['reopened'];
    }

    function getUpdateDate() {
        return $this->ht['updated'];
    }

    function getDueDate() {
        return $this->ht['duedate'];
    }

    function getSLADueDate() {
        return $this->ht['sla_duedate'];
    }

    function getEstDueDate() {

        //Real due date
        if(($duedate=$this->getDueDate()))
            return $duedate;

        //return sla due date (If ANY)
        return $this->getSLADueDate();
    }

    function getCloseDate() {
        return $this->ht['closed'];
    }

    function getStatusId() {
        return $this->ht['status_id'];
    }

    function getStatus() {

        if (!$this->status && $this->getStatusId())
            $this->status = TicketStatus::lookup($this->getStatusId());

        return $this->status;
    }

    function getState() {

        if (!$this->getStatus())
            return '';

        return $this->getStatus()->getState();
    }

    function getDeptId() {
       return $this->ht['dept_id'];
    }

    function getDeptName() {

        if(!$this->ht['dept_name'] && ($dept = $this->getDept()))
            $this->ht['dept_name'] = $dept->getName();

       return $this->ht['dept_name'];
    }

    function getPriorityId() {
        global $cfg;

        if (($a = $this->_answers['priority'])
                && ($b = $a->getValue()))
            return $b->getId();
        return $cfg->getDefaultPriorityId();
    }

    function getPriority() {
        if (($a = $this->_answers['priority']) && ($b = $a->getValue()))
            return $b->getDesc();
        return '';
    }

    public static function getPriorityByList($list_id) {
        if (!$list_id || !is_array($list_id)) return null;
        $list_id = array_unique($list_id);
        $list_id = array_filter($list_id);
        if (!$list_id || !is_array($list_id)) return null;

        $sql = "SELECT
          t.ticket_id,
          IFNULL(v.value_id)
        FROM ost_ticket t
          JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T'
          JOIN ost_form_entry_values v ON e.id = v.entry_id
          JOIN ost_form_field f ON v.field_id = f.id AND f.name LIKE 'priority'
          WHERE p.priority <> 'normal' AND t.ticket_id IN (".implode(',', $list_id).")";
        $res = db_query($sql);
        if (!$res) return null;
        $result = [];
        while($res && ($row = db_fetch_array($res))) {
            $result[ $row['ticket_id'] ] = $row['value_id'];
        }

        return $result;
    }

    public static function getPriorityColorByList($list_id) {
        if (!$list_id || !is_array($list_id)) return null;
        $list_id = array_unique($list_id);
        $list_id = array_filter($list_id);
        if (!$list_id || !is_array($list_id)) return null;

        $sql = "SELECT
            t.ticket_id,
            p.priority_color
            FROM ost_ticket t
            JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T'
            JOIN ost_form_entry_values v ON e.id = v.entry_id
            JOIN ost_form_field f ON v.field_id = f.id AND f.name LIKE 'priority'
            JOIN ost_ticket_priority p ON v.value_id=p.priority_id
          WHERE p.priority <> 'normal' AND t.ticket_id IN (".implode(',', $list_id).")";
        $res = db_query($sql);
        if (!$res) return null;
        $result = [];
        while($res && ($row = db_fetch_array($res))) {
            $result[ $row['ticket_id'] ] = $row['priority_color'];
        }

        return $result;
    }

    public static function getColorByList($list_id) {
        if (!$list_id || !is_array($list_id)) return null;
        $list_id = array_unique($list_id);
        $list_id = array_filter($list_id);
        if (!$list_id || !is_array($list_id)) return null;

        $sql = "SELECT
            t.ticket_id,
            v.value
            FROM ost_ticket t
            JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T'
            JOIN ost_form_entry_values v ON e.id = v.entry_id
            JOIN ost_form_field f ON v.field_id = f.id AND f.name LIKE 'colorlabel'
          WHERE v.value IS NOT NULL AND v.value <> '' AND t.ticket_id IN (".implode(',', $list_id).")";
        $res = db_query($sql);
        if (!$res) return null;
        $result = [];
        while($res && ($row = db_fetch_array($res))) {
            $result[ $row['ticket_id'] ] = $row['value'];
        }

        return $result;
    }

    public static function getPaymentStatusList($list_id) {
        if (!$list_id || !is_array($list_id)) return null;
        $list_id = array_unique($list_id);
        $list_id = array_filter($list_id);
        if (!$list_id || !is_array($list_id)) return null;

        $sql = "SELECT
            t.ticket_id,
            v.value
            FROM ost_ticket t
            JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T'
            JOIN ost_form_entry_values v ON e.id = v.entry_id
            JOIN ost_form_field f ON v.field_id = f.id AND f.name LIKE 'paymentstatus'
          WHERE v.value IS NOT NULL AND v.value <> '' AND t.ticket_id IN (".implode(',', $list_id).")";
        $res = db_query($sql);
        if (!$res) return null;
        $result = [];
        while($res && ($row = db_fetch_array($res))) {
            $result[ $row['ticket_id'] ] = $row['value'];
        }

        return $result;
    }

    function getPhoneNumber() {
        if (($owner = $this->getOwner()))
            return (string)$owner->getPhoneNumber();
        else
            return '';
    }

    function getFormatedPhoneNumber() {
        $phone_number = $this->getPhoneNumber();
        return _String::formatPhoneNumber($phone_number);
    }

    function getSource() {
        return $this->ht['source'];
    }

    function getIP() {
        return $this->ht['ip_address'];
    }

    function getHashtable() {
        return $this->ht;
    }

    function getUpdateInfo() {
        global $cfg;

        $info=array('source'    =>  $this->getSource(),
                    'topicId'   =>  $this->getTopicId(),
                    'slaId' =>  $this->getSLAId(),
                    'user_id' => $this->getOwnerId(),
                    'duedate'   =>  $this->getDueDate()
                        ? Format::userdate($cfg->getDateFormat(),
                            Misc::db2gmtime($this->getDueDate()))
                        :'',
                    'time'  =>  $this->getDueDate()?(Format::userdate('G:i', Misc::db2gmtime($this->getDueDate()))):'',
                    );

        return $info;
    }

    function getLockId() {
        return $this->ht['lock_id'];
    }

    function getLock() {

        if(!$this->tlock && $this->getLockId())
            $this->tlock= TicketLock::lookup($this->getLockId(), $this->getId());

        return $this->tlock;
    }

    function acquireLock($staffId, $lockTime) {

        if(!$staffId or !$lockTime) //Lockig disabled?
            return null;

        //Check if the ticket is already locked.
        if(($lock=$this->getLock()) && !$lock->isExpired()) {
            if($lock->getStaffId()!=$staffId) //someone else locked the ticket.
                return null;

            //Lock already exits...renew it
            $lock->renew($lockTime); //New clock baby.

            return $lock;
        }
        //No lock on the ticket or it is expired
        $this->tlock = null; //clear crap
        $this->ht['lock_id'] = TicketLock::acquire($this->getId(), $staffId, $lockTime); //Create a new lock..
        //load and return the newly created lock if any!
        return $this->getLock();
    }

    function getDept() {
        global $cfg;

        if(!$this->dept)
            if(!($this->dept = Dept::lookup($this->getDeptId())))
                $this->dept = $cfg->getDefaultDept();

        return $this->dept;
    }

    function getUserId() {
        return $this->getOwnerId();
    }

    function getUser() {

        if(!isset($this->user) && $this->getOwner())
            $this->user = new EndUser($this->getOwner());

        return $this->user;
    }

    function getStaffId() {
        return $this->ht['staff_id'];
    }

    function getStaff() {

        if(!$this->staff && $this->getStaffId())
            $this->staff= Staff::lookup($this->getStaffId());

        return $this->staff;
    }

    function getTeamId() {
        return $this->ht['team_id'];
    }

    function getTeam() {

        if(!$this->team && $this->getTeamId())
            $this->team = Team::lookup($this->getTeamId());

        return $this->team;
    }

    function getAssignee() {

        if($staff=$this->getStaff())
            return $staff->getName();

        if($team=$this->getTeam())
            return $team->getName();

        return '';
    }

    function getAssignees() {

        $assignees=array();
        if($staff=$this->getStaff())
            $assignees[] = $staff->getName();

        if($team=$this->getTeam())
            $assignees[] = $team->getName();

        return $assignees;
    }

    function getAssigned($glue='/') {
        $assignees = $this->getAssignees();
        return $assignees?implode($glue, $assignees):'';
    }

    function getTopicId() {
        return $this->ht['topic_id'];
    }

    function getTopic() {

        if(!$this->topic && $this->getTopicId())
            $this->topic = Topic::lookup($this->getTopicId());

        return $this->topic;
    }


    function getSLAId() {
        return $this->ht['sla_id'];
    }

    function getSLA() {

        if(!$this->sla && $this->getSLAId())
            $this->sla = SLA::lookup($this->getSLAId());

        return $this->sla;
    }

    function getLastRespondent() {

        $sql ='SELECT  resp.staff_id '
             .' FROM '.TICKET_THREAD_TABLE.' resp '
             .' LEFT JOIN '.STAFF_TABLE. ' USING(staff_id) '
             .' WHERE  resp.ticket_id='.db_input($this->getId()).' AND resp.staff_id>0 '
             .'   AND  resp.thread_type="R"'
             .' ORDER BY resp.created DESC LIMIT 1';

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return null;

        list($id)=db_fetch_row($res);

        return Staff::lookup($id);

    }

    function getLastMessageDate() {
        return $this->ht['lastmessage'];
    }

    function getLastMsgDate() {
        return $this->getLastMessageDate();
    }

    function getLastResponseDate() {
        return $this->ht['lastresponse'];
    }

    function getLastRespDate() {
        return $this->getLastResponseDate();
    }


    function getLastMsgId() {
        return $this->lastMsgId;
    }

    function getLastMessage() {
        if (!isset($this->last_message)) {
            if($this->getLastMsgId())
                $this->last_message =  Message::lookup(
                    $this->getLastMsgId(), $this->getId());

            if (!$this->last_message)
                $this->last_message = Message::lastByTicketId($this->getId());
        }
        return $this->last_message;
    }

    function getThread() {

        if(!$this->thread)
            $this->thread = Thread::lookup($this);

        return $this->thread;
    }

    function getThreadCount() {
        return $this->getNumMessages() + $this->getNumResponses();
    }

    function getNumMessages() {
        return $this->getThread()->getNumMessages();
    }

    function getNumResponses() {
        return $this->getThread()->getNumResponses();
    }

    function getNumNotes() {
        return $this->getThread()->getNumNotes();
    }

    function getMessages() {
        return $this->getThreadEntries('M');
    }

    function getResponses() {
        return $this->getThreadEntries('R');
    }

    function getNotes() {
        return $this->getThreadEntries('N');
    }

    function getClientThread() {
        return $this->getThreadEntries(array('M', 'R'));
    }

    function getThreadEntry($id) {
        return $this->getThread()->getEntry($id);
    }

    function getThreadEntries($type, $order='') {
        return $this->getThread()->getEntries($type, $order);
    }

    //Collaborators
    function getNumCollaborators() {
        return count($this->getCollaborators());
    }

    function getNumActiveCollaborators() {

        if (!isset($this->ht['active_collaborators']))
            $this->ht['active_collaborators'] = count($this->getActiveCollaborators());

        return $this->ht['active_collaborators'];
    }

    function getActiveCollaborators() {
        return $this->getCollaborators(array('isactive'=>1));
    }


    function getCollaborators($criteria=array()) {

        if ($criteria)
            return Collaborator::forTicket($this->getId(), $criteria);

        if (!isset($this->collaborators))
            $this->collaborators = Collaborator::forTicket($this->getId());

        return $this->collaborators;
    }

    //UserList of recipients  (owner + cc email + collaborators)
    function getRecipients() {

        if (!isset($this->recipients)) {
            $list = new UserList();
            $list->add($this->getOwner());
            if ($collabs = $this->getActiveCollaborators()) {
                foreach ($collabs as $c)
                    $list->add($c);
            }

            $this->recipients = $list;
        }

        return $this->recipients;
    }


    function addCollaborator($user, $vars, &$errors) {

        if (!$user || $user->getId()==$this->getOwnerId())
            return null;

        $vars = array_merge(array(
                'ticketId' => $this->getId(),
                'userId' => $user->getId()), $vars);
        if (!($c=Collaborator::add($vars, $errors)))
            return null;

        $this->collaborators = null;
        $this->recipients = null;

        return $c;
    }

    //XXX: Ugly for now
    function updateCollaborators($vars, &$errors) {
        global $thisstaff;

        if (!$thisstaff) return;

        //Deletes
        if($vars['del'] && ($ids=array_filter($vars['del']))) {
            $collabs = array();
            foreach ($ids as $k => $cid) {
                if (($c=Collaborator::lookup($cid))
                        && $c->getTicketId() == $this->getId()
                        && $c->remove())
                     $collabs[] = $c;
            }

            $this->logNote(_S('Collaborators Removed'),
                    implode("<br>", $collabs), $thisstaff, false);
        }

        //statuses
        $cids = null;
        if($vars['cid'] && ($cids=array_filter($vars['cid']))) {
            $sql='UPDATE '.TICKET_COLLABORATOR_TABLE
                .' SET updated=NOW(), isactive=1 '
                .' WHERE ticket_id='.db_input($this->getId())
                .' AND id IN('.implode(',', db_input($cids)).')';
            db_query($sql);
        }

        $sql='UPDATE '.TICKET_COLLABORATOR_TABLE
            .' SET updated=NOW(), isactive=0 '
            .' WHERE ticket_id='.db_input($this->getId());
        if($cids)
            $sql.=' AND id NOT IN('.implode(',', db_input($cids)).')';

        db_query($sql);

        unset($this->ht['active_collaborators']);
        $this->collaborators = null;

        return true;
    }

    /* -------------------- Setters --------------------- */
    function setLastMsgId($msgid) {
        return $this->lastMsgId=$msgid;
    }
    function setLastMessage($message) {
        $this->last_message = $message;
        $this->setLastMsgId($message->getId());
    }

    //DeptId can NOT be 0. No orphans please!
    function setDeptId($deptId) {

        //Make sure it's a valid department//
        if(!($dept=Dept::lookup($deptId)) || $dept->getId()==$this->getDeptId())
            return false;


        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), dept_id='.db_input($deptId)
            .' WHERE ticket_id='.db_input($this->getId());

        return (db_query($sql));
    }

    //DeptId can NOT be 0. No orphans please!
    function setUnassign($id) {
        $unassign = $this->getUnassign();
        if (intval($unassign) === intval($id)) return;

        //Make sure it's a valid department//
        if (is_numeric($id)) {
            $conts = TicketUnassign::getConstants();
            if (!array_search($id, $conts)) {
                return false;
            }
        } else {
            return false;
        }

        try {
            db_start_transaction();

            $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), unassign='.db_input($id)
                .' WHERE ticket_id='.db_input($this->getId());

            db_query($sql);

            $sql = "INSERT INTO ost_ticket_unassign_log SET
                ticket_id =  ".db_input($this->getId()).",
                unassign=".db_input($id).",
                created_at=NOW()
            ";

            db_query($sql);

            $this->logNote('Unassign Flag', 'Removed unassign flag', 'SYSTEM', false);

            db_commit();
        } catch (Exception $ex) { db_rollback(); }
    }

    //Set staff ID...assign/unassign/release (id can be 0)
    function setStaffId($staffId) {

        if(!is_numeric($staffId)) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), staff_id='.db_input($staffId)
            .' WHERE ticket_id='.db_input($this->getId());

        if (!db_query($sql))
            return false;

        $this->staff = null;
        $this->ht['staff_id'] = $staffId;

        return true;
    }

    function setSLAId($slaId) {
        if ($slaId == $this->getSLAId()) return true;
        return db_query(
             'UPDATE '.TICKET_TABLE.' SET sla_id='.db_input($slaId)
            .' WHERE ticket_id='.db_input($this->getId()));
    }
    /**
     * Selects the appropriate service-level-agreement plan for this ticket.
     * When tickets are transfered between departments, the SLA of the new
     * department should be applied to the ticket. This would be useful,
     * for instance, if the ticket is transferred to a different department
     * which has a shorter grace period, the ticket should be considered
     * overdue in the shorter window now that it is owned by the new
     * department.
     *
     * $trump - if received, should trump any other possible SLA source.
     *          This is used in the case of email filters, where the SLA
     *          specified in the filter should trump any other SLA to be
     *          considered.
     */
    function selectSLAId($trump=null) {
        global $cfg;
        # XXX Should the SLA be overridden if it was originally set via an
        #     email filter? This method doesn't consider such a case
        if ($trump && is_numeric($trump)) {
            $slaId = $trump;
        } elseif ($this->getDept() && $this->getDept()->getSLAId()) {
            $slaId = $this->getDept()->getSLAId();
        } elseif ($this->getTopic() && $this->getTopic()->getSLAId()) {
            $slaId = $this->getTopic()->getSLAId();
        } else {
            $slaId = $cfg->getDefaultSLAId();
        }

        return ($slaId && $this->setSLAId($slaId)) ? $slaId : false;
    }

    //Set team ID...assign/unassign/release (id can be 0)
    function setTeamId($teamId) {

        if(!is_numeric($teamId)) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), team_id='.db_input($teamId)
            .' WHERE ticket_id='.db_input($this->getId());

        return (db_query($sql));
    }

    //Status helper.
    function setStatus($status, $comments='', $set_closing_agent=true) {
        global $thisstaff, $cfg;
        $new_sla_id = null;

        if ($status && is_numeric($status))
            $status = TicketStatus::lookup($status);

        if (!$status || !$status instanceof TicketStatus)
            return false;

        //
        // tìm config id của SLA_ID
        $sql = 'SELECT id FROM '.FORM_FIELD_TABLE.' field WHERE `type`=\'text\' AND `name`=\'sla_id\' LIMIT 1';
        $no_overdue_id = [];

        if(($res=db_query($sql))) {
            $field_id = db_fetch_row($res);
            // tìm các status set no overdue
            if (isset($field_id[0]) && is_numeric($field_id[0])) {
                $field_id = $field_id[0];
                $config = $status->getConfiguration();
                if (isset($config[$field_id]) && intval(trim($config[$field_id])))
                    $new_sla_id = intval(trim($config[$field_id]));
            }
        }
        //

        // XXX: intercept deleted status and do hard delete
        if (!strcasecmp($status->getState(), 'deleted'))
            return $this->delete($comments);

        if ($this->getStatusId() == $status->getId())
            return true;

        $sql = 'UPDATE '.TICKET_TABLE.' SET updated=NOW() '.
               ' ,status_id='.db_input($status->getId());

        if (!empty($this->getBookingCode())) {
                $sql .= ' ,sla_id='.db_input(NO_OVERDUE_SLA_ID);
        } else {
            if ($new_sla_id)
                $sql .= ' ,sla_id='.db_input($new_sla_id);
            else {
                $new_sla_id = $cfg->getDefaultSLAId();
                $sql .= ' ,sla_id='.db_input($new_sla_id);
            }
        }

        $ecb = null;
        switch($status->getState()) {
            case 'closed':
                $sql.=', closed=NOW(), duedate=NULL ';
                if ($thisstaff && $set_closing_agent)
                    $sql.=', staff_id='.db_input($thisstaff->getId());
                $this->clearOverdue();

                $ecb = function($t) {
                    $t->reload();
                    $t->logEvent('closed');
                    $t->deleteDrafts();
                };

                $status_id = $status->getId();
                Signal::send('TriggerList.TICKET_CANCEL', $this, $status_id);
                break;
            case 'open':
                // TODO: check current status if it allows for reopening
                if ($this->isClosed()) {
                    $sql .= ',closed=NULL, reopened=NOW() ';
                    $ecb = function ($t) {
                        $t->logEvent('reopened', 'closed');
                    };
                }

                // If the ticket is not open then clear answered flag
                if (!$this->isOpen())
                    $sql .= ', isanswered = 0 ';
                break;
            default:
                return false;

        }

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        if (!db_query($sql))
            return false;

        // Log status change b4 reload — if currently has a status. (On new
        // ticket, the ticket is opened and thereafter the status is set to
        // the requested status).
        if ($current_status = $this->getStatus()) {
            $note = sprintf(__('Status changed from %1$s to %2$s by %3$s'),
                    $this->getStatus(),
                    $status,
                    $thisstaff ?: 'SYSTEM');

            $alert = false;
            if ($comments) {
                $note .= sprintf('<hr>%s', $comments);
                // Send out alerts if comments are included
                $alert = true;
            }

            $note = $this->logNote(__('Status Changed'), $note, 'SYSTEM', $alert);

            Signal::send('ticket.changeStatus', $this);
        }
        // Log events via callback
        if ($ecb) $ecb($this);

        return $note;
    }

    function setState($state, $alerts=false) {

        switch(strtolower($state)) {
            case 'open':
                return $this->setStatus('open');
                break;
            case 'closed':
                return $this->setStatus('closed');
                break;
            case 'answered':
                return $this->setAnsweredState(1);
                break;
            case 'unanswered':
                return $this->setAnsweredState(0);
                break;
            case 'overdue':
                return $this->markOverdue();
                break;
            case 'notdue':
                return $this->clearOverdue();
                break;
            case 'unassined':
                return $this->unassign();
        }

        return false;
    }




    function setAnsweredState($isanswered) {

        $sql='UPDATE '.TICKET_TABLE.' SET isanswered='.db_input($isanswered)
            .' WHERE ticket_id='.db_input($this->getId());

        return (db_query($sql));
    }

    function reopen() {
        global $cfg;

        if (!$this->isClosed())
            return false;

        // Set status to open based on current closed status settings
        // If the closed status doesn't have configured "reopen" status then use the
        // the default ticket status.
        if (!($status=$this->getStatus()->getReopenStatus()))
            $status = $cfg->getDefaultTicketStatusId();

        return $status ? $this->setStatus($status, 'Reopened') : false;
    }

    function onNewTicket($message, $autorespond=true, $alertstaff=true) {
        global $cfg;

        //Log stuff here...

        if(!$autorespond && !$alertstaff) return true; //No alerts to send.

        /* ------ SEND OUT NEW TICKET AUTORESP && ALERTS ----------*/

        $this->reload(); //get the new goodies.
        if(!$cfg
                || !($dept=$this->getDept())
                || !($tpl = $dept->getTemplate())
                || !($email=$dept->getAutoRespEmail())) {
                return false;  //bail out...missing stuff.
        }

        $options = array(
            'inreplyto'=>$message->getEmailMessageId(),
            'references'=>$message->getEmailReferences(),
            'thread'=>$message);

        //Send auto response - if enabled.
        if($autorespond
                && $cfg->autoRespONNewTicket()
                && $dept->autoRespONNewTicket()
                &&  ($msg=$tpl->getAutoRespMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                    array('message' => $message,
                          'recipient' => $this->getOwner(),
                          'signature' => ($dept && $dept->isPublic())?$dept->getSignature():'')
                    );

            $email->sendAutoReply($this->getOwner(), $msg['subj'], $msg['body'],
                null, $options);
        }

        //Send alert to out sleepy & idle staff.
        if ($alertstaff
                && $cfg->alertONNewTicket()
                && ($email=$dept->getAlertEmail())
                && ($msg=$tpl->getNewTicketAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(), array('message' => $message));

            $recipients=$sentlist=array();
            //Exclude the auto responding email just incase it's from staff member.
            if ($message->isAutoReply())
                $sentlist[] = $this->getEmail();

            //Alert admin??
            if($cfg->alertAdminONNewTicket()) {
                $alert = $this->replaceVars($msg, array('recipient' => 'Admin'));
                $_default_email = $cfg->getDefaultEmail(); //will take the default email.
                $_default_email->sendAlert($cfg->getAdminEmail(), $alert['subj'], $alert['body'], null, $options);
                $sentlist[]=$cfg->getAdminEmail();
            }

            //Only alerts dept members if the ticket is NOT assigned.
            if($cfg->alertDeptMembersONNewTicket() && !$this->isAssigned()) {
                if(($members=$dept->getMembersForAlerts()))
                    $recipients=array_merge($recipients, $members);
            }

            if($cfg->alertDeptManagerONNewTicket() && $dept && ($manager=$dept->getManager()))
                $recipients[]= $manager;

            // Account manager
            if ($cfg->alertAcctManagerONNewMessage()
                    && ($org = $this->getOwner()->getOrganization())
                    && ($acct_manager = $org->getAccountManager())) {
                if ($acct_manager instanceof Team)
                    $recipients = array_merge($recipients, $acct_manager->getMembers());
                else
                    $recipients[] = $acct_manager;
            }

            foreach( $recipients as $k=>$staff) {
                if(!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }

        return true;
    }

    function onOpenLimit($sendNotice=true) {
        global $ost, $cfg;

        //Log the limit notice as a warning for admin.
        $msg=sprintf(_S('Maximum open tickets (%1$d) reached for %2$s'),
            $cfg->getMaxOpenTickets(), $this->getEmail());
        $ost->logWarning(sprintf(_S('Maximum Open Tickets Limit (%s)'),$this->getEmail()),
            $msg);

        if(!$sendNotice || !$cfg->sendOverLimitNotice())
            return true;

        //Send notice to user.
        if(($dept = $this->getDept())
            && ($tpl=$dept->getTemplate())
            && ($msg=$tpl->getOverlimitMsgTemplate())
            && ($email=$dept->getAutoRespEmail())) {

            $msg = $this->replaceVars($msg->asArray(),
                        array('signature' => ($dept && $dept->isPublic())?$dept->getSignature():''));

            $email->sendAutoReply($this->getOwner(), $msg['subj'], $msg['body']);
        }

        $user = $this->getOwner();

        //Alert admin...this might be spammy (no option to disable)...but it is helpful..I think.
        $alert=sprintf(__('Maximum open tickets reached for %s.'), $this->getEmail())."\n"
              .sprintf(__('Open tickets: %d'), $user->getNumOpenTickets())."\n"
              .sprintf(__('Max allowed: %d'), $cfg->getMaxOpenTickets())
              ."\n\n".__("Notice sent to the guest.");

        $ost->alertAdmin(__('Overlimit Notice'), $alert);

        return true;
    }

    function onResponse($response, $options=array()) {
        db_query('UPDATE '.TICKET_TABLE.' SET isanswered=1, lastresponse=NOW(), updated=NOW() WHERE ticket_id='.db_input($this->getId()));
        $this->reload();
        $vars = array_merge($options,
                array(
                    'activity' => _S('New Response'),
                    'threadentry' => $response));

        $this->onActivity($vars);
    }

    /*
     * Notify collaborators on response or new message
     *
     */

    function  notifyCollaborators($entry, $vars = array()) {
        global $cfg;

        if (!$entry instanceof ThreadEntry
                || !($recipients=$this->getRecipients())
                || !($dept=$this->getDept())
                || !($tpl=$dept->getTemplate())
                || !($msg=$tpl->getActivityNoticeMsgTemplate())
                || !($email=$dept->getEmail()))
            return;

        //Who posted the entry?
        $skip = array();
        if ($entry instanceof Message) {
            $poster = $entry->getUser();
            // Skip the person who sent in the message
            $skip[$entry->getUserId()] = 1;
            // Skip all the other recipients of the message
            foreach ($entry->getAllEmailRecipients() as $R) {
                foreach ($recipients as $R2) {
                    if (0 === strcasecmp($R2->getEmail(), $R->mailbox.'@'.$R->host)) {
                        $skip[$R2->getUserId()] = true;
                        break;
                    }
                }
            }
        } else {
            $poster = $entry->getStaff();
            // Skip the ticket owner
            $skip[$this->getUserId()] = 1;
        }

        $get_recipient = isset($vars['get_recipient']) && $vars['get_recipient'] ? true : false;
        $vars = array_merge($vars, array(
                    'message' => (string) $entry,
                    'poster' => $poster ?: _S('A collaborator'),
                    )
                );

        $msg = $this->replaceVars($msg->asArray(), $vars);

        $attachments = $cfg->emailAttachments()?$entry->getAttachments():array();
        $options = array('inreplyto' => $entry->getEmailMessageId(),
                         'thread' => $entry);

        $_recipient_list = []; // list những người thật sự được gửi email
        $recipients = is_array($recipients) ? array_unique($recipients) : $recipients;
        if ($recipients) {
            foreach ($recipients as $recipient) {
                // Skip folks who have already been included on this part of
                // the conversation
                if (isset($skip[$recipient->getUserId()]))
                    continue;
                $notice = $this->replaceVars($msg, array('recipient' => $recipient));
                if (!$get_recipient)
                    $email->send(
                        $recipient,
                        $notice['subj']. ' - %'.substr(md5(microtime(1)), 0, 6),
                        $notice['body']."\r\n<p style='color:white;'><small style='color:white;'>"._UUID::generate()."</small></p>",
                        $attachments,
                        $options
                    );

                if (is_object($recipient) && is_callable(array($recipient, 'getEmail'))) {
                    // Add personal name if available
                    if (is_callable(array($to, 'getName'))) {
                        $to = sprintf('"%s" <%s>',
                            $recipient->getName()->getOriginal(), $recipient->getEmail()
                        );
                        $_this_to = $recipient->getEmail();
                    }
                    else {
                        $to = $_this_to = $recipient->getEmail();
                    }

                } else {
                    $to = $_this_to = strval($recipient);
                }

                // ignore existing users
                $_this_to = preg_replace("/(\r\n|\r|\n)/s",'', trim($_this_to));

                if (($_check_user = User::lookupByEmail($_this_to))) {
                    if (mb_strpos(mb_strtolower($_check_user->getFullName()), 'tugo') === false)
                        continue;
                }

                $to = preg_replace("/(\r\n|\r|\n)/s",'', trim($to));
                if (in_array($to, $_recipient_list)) continue;

                $_recipient_list[] = $to;
            }
        }

        return $_recipient_list;
    }

    function onMessage($message, $autorespond=true) {
        global $cfg;

        db_query('UPDATE '.TICKET_TABLE.' SET isanswered=0,lastmessage=NOW() WHERE ticket_id='.db_input($this->getId()));


        // Reopen if closed AND reopenable
        // We're also checking autorespond flag because we don't want to
        // reopen closed tickets on auto-reply from end user. This is not to
        // confused with autorespond on new message setting
        if ($autorespond && $this->isClosed() && $this->isReopenable()) {
            $this->reopen();

            // Auto-assign to closing staff or last respondent
            // If the ticket is closed and auto-claim is not enabled then put the
            // ticket back to unassigned pool.
            if (!$cfg->autoClaimTickets()) {
                /// dont update as unassign
            }
            elseif (!($staff = $this->getStaff()) || !$staff->isAvailable()) {
                // Ticket has no assigned staff -  if auto-claim is enabled then
                // try assigning it to the last respondent (if available)
                // otherwise leave the ticket unassigned.
                if (($lastrep = $this->getLastRespondent())
                    && $lastrep->isAvailable()
                ) {
                    $this->setStaffId($lastrep->getId()); //direct assignment;
                }
                else {
                    // unassign - last respondent is not available.
                    $this->setStaffId(0);
                }
            }
        }

       /**********   double check auto-response  ************/
        if (!($user = $message->getUser()))
            $autorespond=false;
        elseif ($autorespond && (Email::getIdByEmail($user->getEmail())))
            $autorespond=false;
        elseif ($autorespond && ($dept=$this->getDept()))
            $autorespond=$dept->autoRespONNewMessage();


        if(!$autorespond
                || !$cfg->autoRespONNewMessage()
                || !$message) return;  //no autoresp or alerts.

        $this->reload();
        $dept = $this->getDept();
        $email = $dept->getAutoRespEmail();

        //If enabled...send confirmation to user. ( New Message AutoResponse)
        if($email
                && ($tpl=$dept->getTemplate())
                && ($msg=$tpl->getNewMessageAutorepMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                            array(
                                'recipient' => $user,
                                'signature' => ($dept && $dept->isPublic())?$dept->getSignature():''));

            $options = array(
                'inreplyto'=>$message->getEmailMessageId(),
                'thread'=>$message);
            $email->sendAutoReply($user, $msg['subj'], $msg['body'],
                null, $options);
        }
    }

    function onActivity($vars, $alert=true) {
        global $cfg, $thisstaff;

        //TODO: do some shit

        if (!$alert // Check if alert is enabled
                || !$cfg->alertONNewActivity()
                || !($dept=$this->getDept())
                || !($email=$cfg->getAlertEmail())
                || !($tpl = $dept->getTemplate())
                || !($msg=$tpl->getNoteAlertMsgTemplate()))
            return;

        // Alert recipients
        $recipients=array();

        //Last respondent.
        if ($cfg->alertLastRespondentONNewActivity())
            $recipients[] = $this->getLastRespondent();

        // Assigned staff / team
        if ($cfg->alertAssignedONNewActivity()) {

            if (isset($vars['assignee'])
                    && $vars['assignee'] instanceof Staff)
                 $recipients[] = $vars['assignee'];
            elseif ($this->isOpen() && ($assignee = $this->getStaff()))
                $recipients[] = $assignee;

            if ($team = $this->getTeam())
                $recipients = array_merge($recipients, $team->getMembers());
        }

        // Dept manager
        if ($cfg->alertDeptManagerONNewActivity() && $dept && $dept->getManagerId())
            $recipients[] = $dept->getManager();

        $options = array();
        $staffId = $thisstaff ? $thisstaff->getId() : 0;
        if ($vars['threadentry'] && $vars['threadentry'] instanceof ThreadEntry) {
            $options = array(
                'inreplyto' => $vars['threadentry']->getEmailMessageId(),
                'references' => $vars['threadentry']->getEmailReferences(),
                'thread' => $vars['threadentry']);

            // Activity details
            if (!$vars['comments'])
                $vars['comments'] = $vars['threadentry'];

            // Staff doing the activity
            $staffId = $vars['threadentry']->getStaffId() ?: $staffId;
        }

        $msg = $this->replaceVars($msg->asArray(),
                array(
                    'note' => $vars['threadentry'], // For compatibility
                    'activity' => $vars['activity'],
                    'comments' => $vars['comments']));

        $isClosed = $this->isClosed();
        $sentlist=array();
        foreach ($recipients as $k=>$staff) {
            if (!is_object($staff)
                    // Don't bother vacationing staff.
                    || !$staff->isAvailable()
                    // No need to alert the poster!
                    || $staffId == $staff->getId()
                    // No duplicates.
                    || isset($sentlist[$staff->getEmail()])
                    // Make sure staff has access to ticket
                    || ($isClosed && !$this->checkStaffAccess($staff))
                    )
                continue;
            $alert = $this->replaceVars($msg, array('recipient' => $staff));
            $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
            $sentlist[$staff->getEmail()] = 1;
        }

    }

    function onAssign($assignee, $comments, $alert=true) {
        global $cfg, $thisstaff;

        if($this->isClosed()) $this->reopen(); //Assigned tickets must be open - otherwise why assign?

        $unassign = $this->getUnassign();
        if ($unassign != TicketUnassign::NONE) {
            $this->setUnassign(TicketUnassign::NONE);
        }

        //Assignee must be an object of type Staff or Team
        if(!$assignee || !is_object($assignee)) return false;

        $this->reload();

        $comments = $comments ?: _S('Ticket assignment');
        $assigner = $thisstaff ?: _S('SYSTEM (Auto Assignment)');

        //Log an internal note - no alerts on the internal note.
        $note = $this->logNote(
            sprintf(_S('Ticket Assigned to %s'), $assignee->getName()),
            $comments, 'SYSTEM', false);
        if ($note && $note->getId())
            $_SESSION['___THREAD_ID'] = $note->getId();

        //See if we need to send alerts
        if(!$alert || !$cfg->alertONAssignment()) return true; //No alerts!

        $dept = $this->getDept();
        if(!$dept
                || !($tpl = $dept->getTemplate())
                || !($email = $dept->getAlertEmail()))
            return true;

        //recipients
        $recipients=array();
        if ($assignee instanceof Staff) {
            if ($cfg->alertStaffONAssignment())
                $recipients[] = $assignee;
        } elseif (($assignee instanceof Team) && $assignee->alertsEnabled()) {
            if ($cfg->alertTeamMembersONAssignment() && ($members=$assignee->getMembers()))
                $recipients = array_merge($recipients, $members);
            elseif ($cfg->alertTeamLeadONAssignment() && ($lead=$assignee->getTeamLead()))
                $recipients[] = $lead;
        }

        //Get the message template
        if ($recipients
                && ($msg=$tpl->getAssignedAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                        array('comments' => $comments,
                              'assignee' => $assignee,
                              'assigner' => $assigner
                              ));

            //Send the alerts.
            $sentlist=array();
            $options = array(
                'inreplyto'=>$note->getEmailMessageId(),
                'references'=>$note->getEmailReferences(),
                'thread'=>$note);
            foreach( $recipients as $k=>$staff) {
                if(!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }

        return true;
    }

    function onAssign_ol($assignee, $comments, $alert=true) {
        global $cfg, $thisstaff;

        if($this->isClosed()) $this->reopen(); //Assigned tickets must be open - otherwise why assign?

        //Assignee must be an object of type Staff or Team
        if(!$assignee || !is_object($assignee)) return false;

        $this->reload();

        $comments = $comments ?: _S('Absence request transfer');
        $assigner = $thisstaff ?: _S('SYSTEM (Auto Assignment)');

        //Log an internal note - no alerts on the internal note.
        $note = $this->logNote(
            sprintf(_S(ABSENCE_REQUEST_TRANSFER_TITLE), $assignee->getName(), $assignee->getUserName()),
            $comments, $thisstaff, false);

        //See if we need to send alerts
        if(!$alert || !$cfg->alertONAssignment()) return true; //No alerts!

        $dept = $this->getDept();
        if(!$dept
                || !($tpl = $dept->getTemplate())
                || !($email = $dept->getAlertEmail()))
            return true;

        //recipients
        $recipients=array();
        if ($assignee instanceof Staff) {
            if ($cfg->alertStaffONAssignment())
                $recipients[] = $assignee;
        }

        //Get the message template
        if ($recipients
                && ($msg=$tpl->getAssignedAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                        array('comments' => $comments,
                              'assignee' => $assignee,
                              'assigner' => $assigner
                              ));

            //Send the alerts.
            $sentlist=array();
            $options = array(
                'inreplyto'=>$note->getEmailMessageId(),
                'references'=>$note->getEmailReferences(),
                'thread'=>$note);
            foreach( $recipients as $k=>$staff) {
                if(!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
        }

        return true;
    }

    function onDeny_ol($comments, $alert=true) {
        global $cfg, $thisstaff;

        if($this->isClosed()) $this->reopen(); //Assigned tickets must be open - otherwise why assign?

        $this->reload();

        $comments = $comments ?: _S('Absence request deny');
        $offlate = Offlate::lookup($this->getId());
        if (!$offlate || !$offlate->staff_offer) return false;
        $staff = Staff::lookup($offlate->staff_offer);

        //Log an internal note - no alerts on the internal note.
        $note = $this->logNote(
            sprintf(_S(ABSENCE_REQUEST_DENIED_TITLE), $thisstaff->getName(), $thisstaff->getUserName()),
            $comments, $thisstaff, false);

        //See if we need to send alerts
        if(!$alert || !$cfg->alertONAssignment()) return true; //No alerts!

        $dept = $this->getDept();
        if(!$dept
            || !($tpl = $dept->getTemplate())
            || !($email = $dept->getAlertEmail()))
            return true;

        //recipients
        $recipients=array();
        if ($staff instanceof Staff) {
            if ($cfg->alertStaffONAssignment())
                $recipients[] = $staff;
        }

        //Get the message template
        if ($recipients
            && ($msg=$tpl->getAssignedAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                                      array('comments' => $comments,
                                            'assignee' => $staff,
                                            'assigner' => $thisstaff
                                      ));

            //Send the alerts.
            $sentlist=array();
            $options = array(
                'inreplyto'=>$note->getEmailMessageId(),
                'references'=>$note->getEmailReferences(),
                'thread'=>$note);
            foreach( $recipients as $k=>$_staff) {
                if(!is_object($_staff) || !$_staff->isAvailable() || in_array($_staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $_staff));
                $email->sendAlert($_staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $_staff->getEmail();
            }
        }

        return true;
    }

    function onApprove_ol($comments, $alert=true) {
        global $cfg, $thisstaff;

        if($this->isClosed()) $this->reopen(); //Assigned tickets must be open - otherwise why assign?

        $this->reload();

        $comments = $comments ?: _S('Absence request approve');
        $offlate = Offlate::lookup($this->getId());
        if (!$offlate || !$offlate->staff_offer) return false;
        $staff = Staff::lookup($offlate->staff_offer);

        //Log an internal note - no alerts on the internal note.
        $note = $this->logNote(
            sprintf(_S(ABSENCE_REQUEST_APPROVED_TITLE), $thisstaff->getName(), $thisstaff->getUserName()),
            $comments, $thisstaff, false);

        //See if we need to send alerts
        if(!$alert || !$cfg->alertONAssignment()) return true; //No alerts!

        $dept = $this->getDept();
        if(!$dept
            || !($tpl = $dept->getTemplate())
            || !($email = $dept->getAlertEmail()))
            return true;

        //recipients
        $recipients=array();
        if ($staff instanceof Staff) {
            if ($cfg->alertStaffONAssignment())
                $recipients[] = $staff;
        }

        //Get the message template
        if ($recipients
            && ($msg=$tpl->getAssignedAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                                      array('comments' => $comments,
                                            'assignee' => $staff,
                                            'assigner' => $thisstaff
                                      ));

            //Send the alerts.
            $sentlist=array();
            $options = array(
                'inreplyto'=>$note->getEmailMessageId(),
                'references'=>$note->getEmailReferences(),
                'thread'=>$note);
            foreach( $recipients as $k=>$_staff) {
                if(!is_object($_staff) || !$_staff->isAvailable() || in_array($_staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $_staff));
                $email->sendAlert($_staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $_staff->getEmail();
            }
        }

        return true;
    }

   function onOverdue($whine=true, $comments="") {
        global $cfg;

        if($whine && ($sla=$this->getSLA()) && !$sla->alertOnOverdue())
            $whine = false;

        //check if we need to send alerts.
        if(!$whine
                || !$cfg->alertONOverdueTicket()
                || !($dept = $this->getDept()))
            return true;

        //Get the message template
        if(($tpl = $dept->getTemplate())
                && ($msg=$tpl->getOverdueAlertMsgTemplate())
                && ($email = $dept->getAlertEmail())) {

            $msg = $this->replaceVars($msg->asArray(),
                array('comments' => $comments));

            //recipients
            $recipients=array();
            //Assigned staff or team... if any
            if($this->isAssigned() && $cfg->alertAssignedONOverdueTicket()) {
                if($this->getStaffId())
                    $recipients[]=$this->getStaff();
                elseif($this->getTeamId() && ($team=$this->getTeam()) && ($members=$team->getMembers()))
                    $recipients=array_merge($recipients, $members);
            } elseif($cfg->alertDeptMembersONOverdueTicket() && !$this->isAssigned()) {
                //Only alerts dept members if the ticket is NOT assigned.
                if ($members = $dept->getMembersForAlerts())
                    $recipients = array_merge($recipients, $members);
            }
            //Always alert dept manager??
            if($cfg->alertDeptManagerONOverdueTicket() && $dept && ($manager=$dept->getManager()))
                $recipients[]= $manager;

            $sentlist=array();
            foreach( $recipients as $k=>$staff) {
                if(!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null);
                $sentlist[] = $staff->getEmail();
            }

        }

        return true;

    }

    //ticket obj as variable = ticket number.
    function asVar() {
       return $this->getNumber();
    }

    function getVar($tag) {
        global $cfg;

        if($tag && is_callable(array($this, 'get'.ucfirst($tag))))
            return call_user_func(array($this, 'get'.ucfirst($tag)));

        switch(mb_strtolower($tag)) {
            case 'phone':
            case 'phone_number':
                return $this->getPhoneNumber();
                break;
            case 'auth_token':
                return $this->getAuthToken();
                break;
            case 'client_link':
                return sprintf('%s/view.php?t=%s',
                        $cfg->getBaseUrl(), $this->getNumber());
                break;
            case 'staff_link':
                return sprintf('%s/scp/tickets.php?id=%d', $cfg->getBaseUrl(), $this->getId());
                break;
            case 'create_date':
                return Format::date(
                        $cfg->getDateTimeFormat(),
                        Misc::db2gmtime($this->getCreateDate()),
                        $cfg->getTZOffset(),
                        $cfg->observeDaylightSaving());
                break;
             case 'due_date':
                $duedate ='';
                if($this->getEstDueDate())
                    $duedate = Format::date(
                            $cfg->getDateTimeFormat(),
                            Misc::db2gmtime($this->getEstDueDate()),
                            $cfg->getTZOffset(),
                            $cfg->observeDaylightSaving());

                return $duedate;
                break;
            case 'close_date':
                $closedate ='';
                if($this->isClosed())
                    $closedate = Format::date(
                            $cfg->getDateTimeFormat(),
                            Misc::db2gmtime($this->getCloseDate()),
                            $cfg->getTZOffset(),
                            $cfg->observeDaylightSaving());

                return $closedate;
                break;
            case 'user':
                return $this->getOwner();
                break;
            default:
                if (isset($this->_answers[$tag]))
                    // The answer object is retrieved here which will
                    // automatically invoke the toString() method when the
                    // answer is coerced into text
                    return $this->_answers[$tag];
        }

        return false;
    }

    //Replace base variables.
    function replaceVars($input, $vars = array()) {
        global $ost;

        $vars = array_merge($vars, array('ticket' => $this));

        return $ost->replaceTemplateVariables($input, $vars);
    }

    function markUnAnswered() {
        return (!$this->isAnswered() || $this->setAnsweredState(0));
    }

    function markAnswered() {
        return ($this->isAnswered() || $this->setAnsweredState(1));
    }

    function markOverdue($whine=true) {

        global $cfg;

        if($this->isOverdue())
            return true;

        $sql='UPDATE '.TICKET_TABLE.' SET isoverdue=1, updated=NOW() '
            .' WHERE ticket_id='.db_input($this->getId());

        if(!db_query($sql))
            return false;

        $this->logEvent('overdue');
        $this->onOverdue($whine);

        return true;
    }

    function clearOverdue() {

        if(!$this->isOverdue())
            return true;

        //NOTE: Previously logged overdue event is NOT annuled.

        $sql='UPDATE '.TICKET_TABLE.' SET isoverdue=0, updated=NOW() ';

        //clear due date if it's in the past
        if($this->getDueDate() && Misc::db2gmtime($this->getDueDate()) <= Misc::gmtime())
            $sql.=', duedate=NULL';

        //Clear SLA if est. due date is in the past
//         if($this->getSLADueDate() && Misc::db2gmtime($this->getSLADueDate()) <= Misc::gmtime())
//             $sql.=', sla_id=0 ';

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        return (db_query($sql));
    }

    //Dept Tranfer...with alert.. done by staff
    function transfer($deptId, $comments, $alert = true) {

        global $cfg, $thisstaff;

        if(!$thisstaff || !$thisstaff->canTransferTickets())
            return false;

        $currentDept = $this->getDeptName(); //Current department

        if(!$deptId || !$this->setDeptId($deptId))
            return false;

        // Reopen ticket if closed
        if($this->isClosed()) $this->reopen();

        $this->reload();
        $dept = $this->getDept();

        // Set SLA of the new department
        if(!$this->getSLAId() || $this->getSLA()->isTransient())
            $this->selectSLAId();

        // Make sure the new department allows assignment to the
        // currently assigned agent (if any)
        if ($this->isAssigned()
                && ($staff=$this->getStaff())
                && $dept->assignMembersOnly()
                && !$dept->isMember($staff)) {
            $this->setStaffId(0);
        }

        /*** log the transfer comments as internal note - with alerts disabled - ***/
        $title=sprintf(_S('Ticket transferred from %1$s to %2$s'),
            $currentDept, $this->getDeptName());
        $comments=$comments?$comments:$title;
        $note = $this->logNote($title, $comments, $thisstaff, false);
        if ($note && $note->getId())
            $_SESSION['___THREAD_ID'] = $note->getId();

        $this->logEvent('transferred');

        //Send out alerts if enabled AND requested
        if (!$alert || !$cfg->alertONTransfer())
            return true; //no alerts!!

         if (($email = $dept->getAlertEmail())
                     && ($tpl = $dept->getTemplate())
                     && ($msg=$tpl->getTransferAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(),
                array('comments' => $comments, 'staff' => $thisstaff));
            //recipients
            $recipients=array();
            //Assigned staff or team... if any
            if($this->isAssigned() && $cfg->alertAssignedONTransfer()) {
                if($this->getStaffId())
                    $recipients[]=$this->getStaff();
                elseif($this->getTeamId() && ($team=$this->getTeam()) && ($members=$team->getMembers()))
                    $recipients = array_merge($recipients, $members);
            } elseif($cfg->alertDeptMembersONTransfer() && !$this->isAssigned()) {
                //Only alerts dept members if the ticket is NOT assigned.
                if(($members=$dept->getMembersForAlerts()))
                    $recipients = array_merge($recipients, $members);
            }

            //Always alert dept manager??
            if($cfg->alertDeptManagerONTransfer() && $dept && ($manager=$dept->getManager()))
                $recipients[]= $manager;

            $sentlist=array();
            $options = array(
                'inreplyto'=>$note->getEmailMessageId(),
                'references'=>$note->getEmailReferences(),
                'thread'=>$note);
            foreach( $recipients as $k=>$staff) {
                if(!is_object($staff) || !$staff->isAvailable() || in_array($staff->getEmail(), $sentlist)) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $staff->getEmail();
            }
         }

         return true;
    }

    function claim() {
        global $thisstaff;

        if (!$thisstaff || !$this->isOpen() || $this->isAssigned())
            return false;

        $dept = $this->getDept();
        if ($dept->assignMembersOnly() && !$dept->isMember($thisstaff))
            return false;

        $comments = sprintf(_S('Ticket claimed by %s'), $thisstaff->getName());

        return $this->assignToStaff($thisstaff->getId(), $comments, false);
    }

    function assignToStaff($staff, $note, $alert=true) {

        if(!is_object($staff) && !($staff=Staff::lookup($staff)))
            return false;

        if (!$staff->isAvailable() || !$this->setStaffId($staff->getId()))
            return false;

        $this->onAssign($staff, $note, $alert);
        $this->logEvent('assigned');

        return true;
    }

    function assignToStaff_ol($staff, $note, $alert=true) {

        if(!is_object($staff) && !($staff=Staff::lookup($staff)))
            return false;

        if (!$staff->isAvailable() || !$this->setStaffId($staff->getId()))
            return false;

        $this->onAssign_ol($staff, $note, $alert);
        $this->logEvent('assigned');

        return true;
    }

    function deny_ol($note, $alert=true) {
        $this->onDeny_ol($note, $alert);
        $this->logEvent('assigned');
        return true;
    }

    function approve_ol($note, $alert=true) {
        $this->onApprove_ol($note, $alert);
        $this->logEvent('assigned');
        return true;
    }

    function assignToTeam($team, $note, $alert=true) {

        if(!is_object($team) && !($team=Team::lookup($team)))
            return false;

        if (!$team->isActive() || !$this->setTeamId($team->getId()))
            return false;

        //Clear - staff if it's a closed ticket
        //  staff_id is overloaded -> assigned to & closed by.
        if($this->isClosed())
            $this->setStaffId(0);

        $this->onAssign($team, $note, $alert);
        $this->logEvent('assigned');

        return true;
    }

    //Assign ticket to staff or team - overloaded ID.
    function assign($assignId, $note, $alert=true) {
        global $thisstaff;

        $rv=0;
        $id=preg_replace("/[^0-9]/", "", $assignId);
        if($assignId[0]=='t') {
            $rv=$this->assignToTeam($id, $note, $alert);
        } elseif($assignId[0]=='s' || is_numeric($assignId)) {
            $alert=($alert && $thisstaff && $thisstaff->getId()==$id)?false:$alert; //No alerts on self assigned tickets!!!
            //We don't care if a team is already assigned to the ticket - staff assignment takes precedence
            $rv=$this->assignToStaff($id, $note, $alert);

            $this->markAsUnRead();
        }

        return $rv;
    }

    function assign_ol($assignId, $note, $alert=true) {
        global $thisstaff;

        $res=0;
        $alert=($alert && $thisstaff && $thisstaff->getId()==$id)?false:$alert;
        if ($assignId == '-3' && ($res = $this->unassign())) {
            $res=$this->deny_ol($note, $alert);

        } elseif ($assignId == '-2' && ($res = $this->unassign())) {
            $res=$this->approve_ol($note, $alert);

        } elseif($assignId[0]=='s' || is_numeric($assignId)) {
            $id=preg_replace("/[^0-9]/", "", $assignId);

            $res=$this->assignToStaff_ol($id, $note, $alert);
        }

        $this->markAsUnRead();

        return $res;
    }

    //unassign primary assignee
    function unassign() {

        if(!$this->isAssigned()) //We can't release what is not assigned buddy!
            return true;

        //We can only unassigned OPEN tickets.
        if($this->isClosed())
            return false;

        //Unassign staff (if any)
        if($this->getStaffId() && !$this->setStaffId(0))
            return false;

        //unassign team (if any)
        if($this->getTeamId() && !$this->setTeamId(0))
            return false;

        $this->reload();

        return true;
    }

    function release() {
        return $this->unassign();
    }

    //Change ownership
    function changeOwner($user) {
        global $thisstaff;

        if (!$user
                || ($user->getId() == $this->getOwnerId())
                || !$thisstaff->canEditTickets())
            return false;

        $sql ='UPDATE '.TICKET_TABLE.' SET updated = NOW() '
            .', user_id = '.db_input($user->getId())
            .' WHERE ticket_id = '.db_input($this->getId());

        if (!db_query($sql))
            return false;

        $this->ht['user_id'] = $user->getId();
        $this->user = null;
        $this->collaborators = null;
        $this->recipients = null;

        //Log an internal note
        $note = sprintf(_S('%s changed ticket ownership to %s'),
                $thisstaff->getName(), $user->getName());

        //Remove the new owner from list of collaborators
        $c = Collaborator::lookup(array('userId' => $user->getId(),
                    'ticketId' => $this->getId()));
        if ($c && $c->remove())
            $note.= ' '._S('(removed as collaborator)');

        $note = $this->logNote('Ticket ownership changed', $note);
        if ($note && $note->getId())
            $_SESSION['___THREAD_ID'] = $note->getId();

        return true;
    }

    //Insert message from client
    function postMessage($vars, $origin='', $alerts=true) {
        global $cfg;

        $vars['origin'] = $origin;
        if(isset($vars['ip']))
            $vars['ip_address'] = $vars['ip'];
        elseif(!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $errors = array();
        if(!($message = $this->getThread()->addMessage($vars, $errors)))
            return null;

        $this->setLastMessage($message);

        //Add email recipients as collaborators...
        if ($vars['recipients']
                && (strtolower($origin) != 'email' || ($cfg && $cfg->addCollabsViaEmail()))
                //Only add if we have a matched local address
                && $vars['to-email-id']) {
            //New collaborators added by other collaborators are disable --
            // requires staff approval.
            $info = array(
                    'isactive' => ($message->getUserId() == $this->getUserId())? 1: 0);
            $collabs = array();
            foreach ($vars['recipients'] as $recipient) {
                // Skip virtual delivered-to addresses
                if (strcasecmp($recipient['source'], 'delivered-to') === 0)
                    continue;

                if (($user=User::fromVars($recipient)))
                    if ($c=$this->addCollaborator($user, $info, $errors))
                        $collabs[] = sprintf('%s%s',
                            (string) $c,
                            $recipient['source']
                                ? " ".sprintf(_S('via %s'), $recipient['source'])
                                : ''
                            );
            }
            //TODO: Can collaborators add others?
            if ($collabs) {
                //TODO: Change EndUser to name of  user.
                $this->logNote(_S('Collaborators added by end guest'),
                        implode("<br>", $collabs), _S('End Guest'), false);
            }
        }

        $this->notifyNewMsg();
        if(!$alerts) return $message; //Our work is done...

        // Do not auto-respond to bounces and other auto-replies
        $autorespond = isset($vars['flags'])
            ? !$vars['flags']['bounce'] && !$vars['flags']['auto-reply']
            : true;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        $this->onMessage($message, $autorespond); //must be called b4 sending alerts to staff.

        if ($autorespond && $cfg && $cfg->notifyCollabsONNewMessage())
            $this->notifyCollaborators($message, array('signature' => ''));

        $dept = $this->getDept();


        $variables = array(
                'message' => $message,
                'poster' => ($vars['poster'] ? $vars['poster'] : $this->getName())
                );
        $options = array(
                'inreplyto' => $message->getEmailMessageId(),
                'references' => $message->getEmailReferences(),
                'thread'=>$message);
        //If enabled...send alert to staff (New Message Alert)
        if($cfg->alertONNewMessage()
                && ($email = $dept->getAlertEmail())
                && ($tpl = $dept->getTemplate())
                && ($msg = $tpl->getNewMessageAlertMsgTemplate())) {

            $msg = $this->replaceVars($msg->asArray(), $variables);

            //Build list of recipients and fire the alerts.
            $recipients=array();
            //Last respondent.
            if($cfg->alertLastRespondentONNewMessage() || $cfg->alertAssignedONNewMessage())
                $recipients[]=$this->getLastRespondent();

            //Assigned staff if any...could be the last respondent
            if ($cfg->alertAssignedONNewMessage() && $this->isAssigned()) {
                if ($staff = $this->getStaff()) {
                    // will get default staff email
                    $recipients[] = $staff;

                    // get cc email if available
                    if (($cc_email = $staff->getCCEmail()))
                        $recipients[] = $cc_email;
                }
                elseif ($team = $this->getTeam())
                    $recipients = array_merge($recipients, $team->getMembers());
            }

            //Dept manager
            if($cfg->alertDeptManagerONNewMessage() && $dept && ($manager=$dept->getManager()))
                $recipients[]=$manager;

            // Account manager
            if ($cfg->alertAcctManagerONNewMessage()
                    && ($org = $this->getOwner()->getOrganization())
                    && ($acct_manager = $org->getAccountManager())) {
                if ($acct_manager instanceof Team)
                    $recipients = array_merge($recipients, $acct_manager->getMembers());
                else
                    $recipients[] = $acct_manager;
            }

            $sentlist=array(); //I know it sucks...but..it works.
            foreach( $recipients as $k=>$staff) {
                if(!$staff
                    || (
                        is_object($staff) && (
                            !$staff->getEmail()
                            || !$staff->isAvailable()
                            || in_array($staff->getEmail(), $sentlist)
                        )
                    )
                ) continue;
                $alert = $this->replaceVars($msg, array('recipient' => $staff));
                $email_string = is_object($staff) && is_callable(array($staff, 'getEmail'))
                    ? $staff->getEmail() // staff object
                    : $staff;

                if ($email_string
                    && ($user_from_email_string = User::fromVars(['email' => $email_string]))
                    && $this->getUserId()
                    && $this->getUserId() == $user_from_email_string->getId()
                ) continue; // check if this email is customer's email, ignore
                // we only send direct emails to customer. no alerts.

                $email->sendAlert($staff, $alert['subj'], $alert['body'], null, $options);
                $sentlist[] = $email_string; // email string
            }
        }

        $this->markAsUnRead();

        return $message;
    }

    function sendSMSAlertNewMsg() {
        if (!self::_sendZaloAlertNewMsg())
            self::_sendSMSAlertNewMsg();
    }

    function notifyNewMsg() {
        global $cfg;

        $staff = $this->getStaff();
        if (!$staff) return false;
        $phone_number = $staff->getMobilePhone();
        if (!$phone_number) return false;
        $phone_number = preg_replace('/[^\d]/', '', $phone_number);

        $user = $this->getUser();
        if (!$user) return false;
        $username = $user->getFullName();
        $user_phone = $this->getPhoneNumber();

        $url = $cfg->getBaseUrl().'/scp/tickets.php?id='.$this->getId();
        $message = sprintf(
            "Khách %s, số điện thoại %s, trả lời vào Ticket %s (%s). Bấm để xem. [%s]",
            $username ?: "Chua-Co-Ten",
            $user_phone ?: "Chua-Co-SDT",
            $this->getNumber(),
            $this->getSubject(),
            date("d/m/Y H:i:s")
        );

        $params = [];
        $params['phone_number'] = $phone_number;
        $params['url'] = $url;
        $params['created_by'] = 0;
        $params['save_as_news'] = true;

        $title = NOTIFICATION_TITLE;
        $_errors = [];
        $notification_send = \Tugo\Notification::send($title, $message, true, $params, $_errors);
    }

    function _sendZaloAlertNewMsg() {
        global $cfg;
        $staff = $this->getStaff();
        if (!$staff) return false;

        $zalo_uid = $staff->getZaloUid();
        if (!$zalo_uid) return false;
        $zalo_uid = trim($zalo_uid);

        $user = $this->getUser();
        if (!$user) return false;
        $username = $user->getFullName();
        $user_phone = $this->getPhoneNumber();

        $type = 'New Message';
        $message = sprintf(
            "Khách %s, số điện thoại %s, trả lời vào Ticket %s (%s). Xem tại: %s. [%s]",
            $username ?: "Chua-Co-Ten",
            $user_phone ?: "Chua-Co-SDT",
            $this->getNumber(),
            $this->getSubject(),
            $cfg->getBaseUrl().'/scp/tickets.php?id='.$this->getId(),
            date("d/m/Y H:i:s")
        );
        $error = [];
        $ticket_id = $this->getId();
        return _Zalo::sendTextToZalo($message, $zalo_uid, $error);
    }

    function _sendSMSAlertNewMsg() {
        $staff = $this->getStaff();
        if (!$staff) return false;

        $phone_number = $staff->getMobilePhone();
        if (!$phone_number) return false;
        $phone_number = preg_replace('/[^\d]/', '', $phone_number);

        $user = $this->getUser();
        if (!$user) return false;
        $username = $user->getFullName();
        $user_phone = $this->getPhoneNumber();

        $type = 'New Message';
        $message = sprintf(
            "Khach %s, sdt %s tra loi vao Ticket %s",
            $username ?: "Chua-Co-Ten",
            $user_phone ?: "Chua-Co-SDT",
            $this->getNumber()
        );
        $error = [];
        $ticket_id = $this->getId();
        _SMS::send($phone_number, $message, $error, $type, $ticket_id);
    }

    function postCannedReply($canned, $msgId, $alert=true) {
        global $ost, $cfg;

        if((!is_object($canned) && !($canned=Canned::lookup($canned))) || !$canned->isEnabled())
            return false;

        $files = array();
        foreach ($canned->attachments->getAll() as $file)
            $files[] = $file['id'];

        if ($cfg->isHtmlThreadEnabled())
            $response = new HtmlThreadBody(
                    $this->replaceVars($canned->getHtml()));
        else
            $response = new TextThreadBody(
                    $this->replaceVars($canned->getPlainText()));

        $info = array('msgId' => $msgId,
                      'poster' => __('SYSTEM (Canned Reply)'),
                      'response' => $response,
                      'cannedattachments' => $files);

        $errors = array();
        if (!($response=$this->postReply($info, $errors, false, false)))
            return null;

        $this->markUnAnswered();

        if(!$alert) return $response;

        $dept = $this->getDept();

        if(($email=$dept->getEmail())
                && ($tpl = $dept->getTemplate())
                && ($msg=$tpl->getAutoReplyMsgTemplate())) {

            if($dept && $dept->isPublic())
                $signature=$dept->getSignature();
            else
                $signature='';

            $msg = $this->replaceVars($msg->asArray(),
                    array(
                        'response' => $response,
                        'signature' => $signature,
                        'recipient' => $this->getOwner(),
                        ));

            $attachments =($cfg->emailAttachments() && $files)?$response->getAttachments():array();
            $options = array(
                'inreplyto'=>$response->getEmailMessageId(),
                'references'=>$response->getEmailReferences(),
                'thread'=>$response);
            $email->sendAutoReply($this, $msg['subj'], $msg['body'], $attachments,
                $options);
        }

        $this->clearOverdue(); //

        return $response;
    }


    /* public */
    function postReply($vars, &$errors, $alert=true, $claim=true) {
        global $thisstaff, $cfg;

        if (!$vars['poster'] && $thisstaff)
            $vars['poster'] = $thisstaff;

        if(!$vars['staffId'] && $thisstaff)
            $vars['staffId'] = $thisstaff->getId();

        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $respone_note = '';
        // send email to custom address (from form input)
        $_type = 'Reply';
        if (isset($vars['handovered']) && $vars['handovered'])  {
            $owner = $alert;
            $respone_note .= sprintf(
                '<p>____</p><p><em><strong>Note</strong>: Handed over to <a href="mailto:%s">%s</a></em></p>',
                $owner, $owner
            );
            $_type = 'Hand over';

        } else $owner = $this->getOwner();

        $_email = is_object($owner) ? $owner->getEmail() : $owner;
        $_user = User::lookupByEmail($_email);
        $vars['title'] = $owner ? sprintf(
            "%s to: %s [%s]",
            $_type,
            ($_user ? $_user->getFullName() : ''),
            $_email
        ) : '';

        if(!($response = $this->getThread()->addResponse($vars, $errors)))
            return null;

        $assignee = $this->getStaff();
        // Set status - if checked.
        if ($vars['reply_status_id']
                && $vars['reply_status_id'] != $this->getStatusId())
            $this->setStatus($vars['reply_status_id']);

        // Claim on response bypasses the department assignment restrictions
        if ($claim && $thisstaff && $this->isOpen() && !$this->getStaffId()
            && $cfg->autoClaimTickets()
        ) {
            $this->setStaffId($thisstaff->getId()); //direct assignment;
        }

        $this->onResponse($response, array('assignee' => $assignee)); //do house cleaning..

        /* email the user??  - if disabled - then bail out */
        if (!$alert) return $response;

        $dept = $this->getDept();

        if($thisstaff && $vars['signature']=='mine'){
            $_signature=$thisstaff->getSignature();
            $_avatar=$thisstaff->getAvatar();
            if (isset($_avatar) && !empty($_avatar) && isset($_signature) && !empty($_signature)){
                $url_avatar = $cfg->getUrl().'staff_avatar/'.$_avatar;
                $signature = '
                <img src="'.$url_avatar.'" width="100px""> 
                <br>
                '.$_signature.'';
            }else{
                $signature = $_signature;
            }
        }elseif($vars['signature']=='dept' && $dept && $dept->isPublic()){
            $signature=$dept->getSignature();
        }else{
            $signature='';
        }           

        $variables = array(
                'response' => $response,
                'signature' => $signature,
                'staff' => $thisstaff,
                'poster' => $thisstaff);
        $options = array(
                'inreplyto' => $response->getEmailMessageId(),
                'references' => $response->getEmailReferences(),
                'thread'=>$response);
        $_sending_list = [];

        if(($email=$dept->getEmail())
                && ($tpl = $dept->getTemplate())
                && ($msg=$tpl->getReplyMsgTemplate())) {

            if($vars['emailcollab']) // get collaborators emails for CC
                $_sending_list = $this->notifyCollaborators($response, array('get_recipient' => true));

            $_staff = $this->getStaff();

            if ($_staff && ($staff_email = $_staff->getCCEmail()))
                $_sending_list[] = $staff_email;

            $input_subject = isset($vars['subject']) ? $vars['subject'] : '';

            if (!$input_subject) {
                $sql = sprintf(
                    "SELECT `subject` FROM %s c 
                      JOIN %s r ON c.ticket_id=r.ticket_id
                      AND c.ticket_id=%s AND r.thread_type='R' LIMIT 1
                    ",
                    TICKET_TABLE.'__cdata',
                    TICKET_THREAD_TABLE,
                    db_input($this->id)
                );

                if (($res = db_query($sql))) {
                    $row = db_fetch_row($res);
                    if ($row) {
                        $input_subject = $row[0];
                    }
                }
            }

            $variables['ticket.subject'] = $input_subject;

            $tags = TagIndexing::getAllTags($input_subject);
            if ($tags && count($tags))
                Tag::update($this->id, $tags);

            $msg = $this->replaceVars($msg->asArray(),
                    $variables + array('recipient' => $this->getOwner()));

            // hack email sending "from" name
            if ($_staff) {
                $_staff_name = $_staff->getName();
            } else {
                $_staff_name = 'Tugo System';
            }

            $email->ht['name'] = $_staff_name;
            $email->address = $_staff_name.'<'.$email->ht['email'].'>';
            // end hacking
            $attachments = $cfg->emailAttachments()?$response->getAttachments():array();
            $options['cc'] = $_sending_list;
            $email->send(
                $owner,
                $msg['subj'],
                str_replace($respone_note, '', str_replace('</p> ', '</p>', $msg['body'])),
                $attachments,
                $options
            );
        }

        // remove emailcollab because we also need cc note when setting cc email
        // only update thread body - no more email sent
        // mềnh ngu vãi, chờ nó insert xong, rồi xuống đây update lại cũng đc mà =))
        if ($_sending_list && count($_sending_list)) {
            $respone_note .= sprintf(
                '<p>____</p><p><em><small><strong>CC</strong>: %s</small></em></p>',
                implode(", ", $_sending_list)
            );

            // update ticket thread, append the note
            $sql = sprintf(
                "UPDATE %s SET updated=NOW(), body=CONCAT(body, %s) WHERE id=%d",
                TICKET_THREAD_TABLE,
                db_input($respone_note),
                $response->getId()
            );
            db_query($sql);
        }

        $this->clearOverdue(); //
        if (($vars['staffId'] && $vars['staffId'] != $this->getStaffId()) || !$vars['staffId'])
            $this->markAsUnRead();
        elseif ($vars['staffId'] && $vars['staffId'] == $this->getStaffId())
            $this->markAsRead();

        return $response;
    }

    /* public */
    function postOverdueEmail($vars, &$errors, $alert=true, $claim=true) {
        global $thisstaff, $cfg;

        $settings = $cfg->getSmsAutoSettings();

        if (!$settings || !(isset($settings['send_email_overdue_tickets']) && $settings['send_email_overdue_tickets']
            && isset($settings['sms_contract_content']) && !empty($settings['sms_contract_content'])
            && isset($settings['send_email_overdue_tickets_title']) && !empty($settings['send_email_overdue_tickets_title'])
            && isset($settings['send_email_overdue_tickets_content']) && !empty($settings['send_email_overdue_tickets_content'])))
            return false;

        $vars['poster'] = COMPANY_TITLE.' Ticket';
        $vars['staffId'] = null;

        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $owner = $this->getOwner();
        $_email = is_object($owner) ? $owner->getEmail() : $owner;

        if (!(filter_var($_email, FILTER_VALIDATE_EMAIL)
            && !preg_match('/^[0-9]+@tugo/', $_email))) {
            echo 'Invalid email address '.$_email."\r\n";
            return false;
        }

        $_user = User::lookupByEmail($_email);
        $_staff = $this->getStaff();
        $_sending_list = [];

        if ($_staff && ($staff_email = $_staff->getCCEmail()))
            $_sending_list[] = $staff_email;

        if ($_staff && ($staff_email = $_staff->getEmail()))
            $_sending_list[] = $staff_email;

        $vars['title'] = $owner ? sprintf(
            "%s to: %s [%s]",
            'Overdue alert email',
            ($_user ? $_user->getFullName() : ''),
            $_email
        ) : '';

        if (!$_staff->getMobilePhone() && !$_staff->getPhone()) return false;

        $msg = str_replace(
            [
                'AGENT_PHONE_NUMBER',
                'AGENT_NAME',
            ],
            [
                preg_replace('/[^0-9]/', '', $_staff->getMobilePhone() ?: $_staff->getPhone()),
                $_staff->getName()->getFull(),
            ],
            $settings['send_email_overdue_tickets_content']
        );

        $vars['response'] = $msg;
        $vars['subject'] = $vars['title'];
        $vars['type'] = 'AE';
        $title = $settings['send_email_overdue_tickets_title'] . " - Ticket [#".$this->getNumber()."]";

        if(!($response = $this->getThread()->addAutoResponse($vars, $errors)))
            return null;

        $assignee = $this->getStaff();

        $this->onResponse($response, array('assignee' => $assignee)); //do house cleaning..

        /* email the user??  - if disabled - then bail out */
        if (!$alert) return $response;

        $dept = $this->getDept();

        if(($email=$dept->getEmail())) {
            // hack email sending "from" name
            $_staff_name = $_staff->getName();
            $email->ht['name'] = $_staff_name;
            $email->address = $_staff_name.'<'.$email->ht['email'].'>';
            // end hacking
            $options['cc'] = $_sending_list;
            echo implode(',', [$_email] + $_sending_list)."\r\n";

            $email->send(
                $owner,
                $title. ' - %'.substr(md5(microtime(1)), 0, 6),
                $msg."\r\n<p style='color:white;'><small style='color:white;'>"._UUID::generate()."</small></p>",
                [],
                $options
            );
        }
        $respone_note = '';
        if ($_sending_list && count($_sending_list)) {
            $respone_note .= sprintf(
                '<p>____</p><p><em><small><strong>CC</strong>: %s</small></em></p>',
                implode(", ", $_sending_list)
            );

            // update ticket thread, append the note
            $sql = sprintf(
                "UPDATE %s SET updated=NOW(), body=CONCAT(body, %s) WHERE id=%d",
                TICKET_THREAD_TABLE,
                db_input($respone_note),
                $response->getId()
            );
            db_query($sql);
        }

        $this->markAsUnRead();

        return $response;
    }

    /* public */
    function postOverdueSMS($vars, &$errors, $alert=true, $claim=true) {
        global $thisstaff, $cfg, $___cron;
        $___cron = true;
        $settings = $cfg->getSmsAutoSettings();

        if (!$settings || !(isset($settings['send_sms_overdue_tickets']) && $settings['send_sms_overdue_tickets']
            && isset($settings['send_sms_overdue_tickets_content']) && !empty($settings['send_sms_overdue_tickets_content'])))
            return false;

        $vars['poster'] = COMPANY_TITLE.' Ticket';
        $vars['staffId'] = null;

        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $phone_number = $this->getPhoneNumber();
        if (!$phone_number) return false;

        $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
        if (!preg_match('/^0?[^2][0-9]{8}$/', $phone_number)) {
            echo 'Invalid mobile phone number '.$phone_number."\r\n";
            return false;
        }

        $owner = $this->getOwner();
        $_staff = $this->getStaff();
        $_email = is_object($owner) ? $owner->getEmail() : $owner;
        $_user = User::lookupByEmail($_email);
        $_sending_list = [];
        $staff_email = null;

        if ($_staff)
            $staff_email = $_staff->getEmail();

        if ($_staff && ($tmp_email = $_staff->getCCEmail()))
            $_sending_list[] = $tmp_email;

        echo $phone_number."\r\n";
        $vars['title'] = $phone_number ? sprintf(
            "%s to: %s [%s]",
            'Overdue alert SMS',
            ($_user ? $_user->getFullName() : ''),
            $phone_number
        ) : '';

        if (!$_staff->getMobilePhone() && !$_staff->getPhone()) return false;

        $msg = str_replace(
            [
                'AGENT_PHONE_NUMBER',
                'AGENT_NAME',
            ],
            [
                preg_replace('/[^0-9]/', '', $_staff->getMobilePhone() ?: $_staff->getPhone()),
                $_staff->getName()->getFull(),
            ],
            $settings['send_sms_overdue_tickets_content']
        );

        $msg = _String::khongdau($msg);

        $vars['response'] = $msg;
        $vars['content'] = $msg;
        $vars['sms_content'] = $msg;
        $vars['phone_number'] = $phone_number;
        $vars['subject'] = $vars['title'];
        $vars['type'] = 'Overdue';

        if(!($response = $this->getThread()->addAutoSMS($vars, $errors)))
            return null;

        $assignee = $this->getStaff();

        $this->onResponse($response, array('assignee' => $assignee)); //do house cleaning..

        /* email the user??  - if disabled - then bail out */
        if (!$alert) return $response;

        $dept = $this->getDept();

        if(isset($staff_email) && $staff_email && ($email=$dept->getEmail())) {
            $title = $settings['send_email_overdue_tickets_title'] . " - Ticket [#".$this->getNumber()."]";
            $msg = str_replace(
                [
                    'AGENT_PHONE_NUMBER',
                    'AGENT_NAME',
                ],
                [
                    preg_replace('/[^0-9]/', '', $_staff->getMobilePhone() ?: $_staff->getPhone()),
                    $_staff->getName()->getFull(),
                ],
                $settings['send_email_overdue_tickets_content']
            );
            // hack email sending "from" name
            $_staff_name = $_staff->getName();
            $email->ht['name'] = $_staff_name;
            $email->address = $_staff_name.'<'.$email->ht['email'].'>';
            // end hacking
            $options['cc'] = $_sending_list;
            echo 'Sending notification email...'."\r\n";
            echo implode(',', [$staff_email] + $_sending_list)."\r\n";
            $email->send(
                $staff_email,
                $title,
                $msg,
                [],
                $options
            );
            echo 'Done notification email.'."\r\n";
        }
        $respone_note = '';
        if ($staff_email) {
            $respone_note .= sprintf(
                '<p>____</p><p><em><small><strong>Send notification email to</strong>: %s</small></em></p>',
                $staff_email
            );
        }
        if ($_sending_list && count($_sending_list)) {
            $respone_note .= sprintf(
                '<p>____</p><p><em><small><strong>CC</strong>: %s</small></em></p>',
                implode(", ", $_sending_list)
            );
        }

        if ($respone_note) {
            // update ticket thread, append the note
            $sql = sprintf(
                "UPDATE %s SET updated=NOW(), body=CONCAT(body, %s) WHERE id=%d",
                TICKET_THREAD_TABLE,
                db_input($respone_note),
                $response->getId()
            );
            db_query($sql);
        }

        $this->markAsUnRead();

        return $response;
    }

    //Activity log - saved as internal notes WHEN enabled!!
    function logActivity($title, $note) {
        return $this->logNote($title, $note, 'SYSTEM', false);
    }

    // History log -- used for statistics generation (pretty reports)
    function logEvent($state, $annul=null, $staff=null) {
        global $thisstaff;

        if ($staff === null) {
            if ($thisstaff) $staff=$thisstaff->getUserName();
            else $staff='SYSTEM';               # XXX: Security Violation ?
        }
        # Annul previous entries if requested (for instance, reopening a
        # ticket will annul an 'closed' entry). This will be useful to
        # easily prevent repeated statistics.
        if ($annul) {
            db_query('UPDATE '.TICKET_EVENT_TABLE.' SET annulled=1'
                .' WHERE ticket_id='.db_input($this->getId())
                  .' AND state='.db_input($annul));
        }

        return db_query('INSERT INTO '.TICKET_EVENT_TABLE
            .' SET ticket_id='.db_input($this->getId())
            .', staff_id='.db_input($this->getStaffId())
            .', team_id='.db_input($this->getTeamId())
            .', dept_id='.db_input($this->getDeptId())
            .', topic_id='.db_input($this->getTopicId())
            .', timestamp=NOW(), state='.db_input($state)
            .', staff='.db_input($staff))
            && db_affected_rows() == 1;
    }

    //Insert Internal Notes
    function logNote($title, $note, $poster='SYSTEM', $alert=true) {

        $errors = array();
        //Unless specified otherwise, assume HTML
        if ($note && is_string($note))
            $note = new HtmlThreadBody($note);

        return $this->postNote(
                array(
                    'title' => $title,
                    'note' => $note,
                ),
                $errors,
                $poster,
                $alert
        );
    }

    public static function updateFromOl($id,$value,$name){
        if (!isset($forms) || !$forms) $forms=DynamicFormEntry::forTicket($id);
        // set ticket as read
        $break = false;
        foreach ($forms as $form) {
            if (!is_null($form->getField($name))) {
                $form->setAnswer($name, $value);
                $form->save();
                break;
            }
        }
    }

    function postNote($vars, &$errors, $poster, $alert=true) {
        global $cfg, $thisstaff;

        //Who is posting the note - staff or system?
        $vars['staffId'] = 0;
        $vars['poster'] = 'SYSTEM';
        if($poster && is_object($poster)) {
            $vars['staffId'] = $poster->getId();
            $vars['poster'] = $poster->getName();
        }elseif($poster) { //string
            $vars['poster'] = $poster;
        }
        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if(!($note=$this->getThread()->addNote($vars, $errors)))
            return null;

        // chỉ validate & update khi post mà có forms
        if (isset($vars['forms']) && is_array($vars['forms'])) {
            // Validate dynamic meta-data
            $forms = DynamicFormEntry::forTicket($this->getId());
            foreach ($forms as $form) {
                // Don't validate deleted forms
                if (!in_array($form->getId(), $vars['forms']))
                    continue;
                $form->setSource($_POST);
                if (!$form->isValid())
                    $errors = array_merge($errors, $form->errors());
            }

            if($errors) return false;

            // Update dynamic meta-data
            foreach ($forms as $f) {
                // Drop deleted forms
                $idx = array_search($f->getId(), $vars['forms']);
                if ($idx === false) {
                    $f->delete();
                }
                else {
                    $f->set('sort', $idx);
                    $f->save();
                }
            }
        }

        $alert = $alert && (
            isset($vars['flags'])
            // No alerts for bounce and auto-reply emails
            ? !$vars['flags']['bounce'] && !$vars['flags']['auto-reply']
            : true
        );

        // update updated time of ticket
        if (isset($vars['id']) && is_numeric($vars['id'])) {
            $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW() ';
            $sql.=' WHERE ticket_id='.db_input($vars['id']);

            db_query($sql);
        }

        // Get assigned staff just in case the ticket is closed.
        $assignee = $this->getStaff();

        if ($vars['note_status_id']
                && ($status=TicketStatus::lookup($vars['note_status_id']))) {
            if ($this->setStatus($status))
                $this->reload();
        }

        $activity = $vars['activity'] ?: _S('New Internal Note');
        $this->onActivity(array(
            'activity' => $activity,
            'threadentry' => $note,
            'assignee' => $assignee
        ), $alert);

        if (($vars['staffId'] && $vars['staffId'] != $this->getStaffId()) || !$vars['staffId'])
            $this->markAsUnRead();
        elseif ($vars['staffId'] && $vars['staffId'] == $this->getStaffId())
            $this->markAsRead();

        $this->clearOverdue(); //

        return $note;
    }

    function sendSMS($vars, &$errors, $poster, $alert=true) {
        global $cfg, $thisstaff;

        //Who is posting the note - staff or system?
        $vars['staffId'] = 0;
        $vars['poster'] = 'SYSTEM';
        if($poster && is_object($poster)) {
            $vars['staffId'] = $poster->getId();
            $vars['poster'] = $poster->getName();
        }elseif($poster) { //string
            $vars['poster'] = $poster;
        }
        if (!$vars['ip_address'] && $_SERVER['REMOTE_ADDR'])
            $vars['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if (!($phone_number = $this->getPhoneNumber()))
            $errors['phone_number'] = 'Require Guest\'s phone number';

        if (!isset($vars['sms_content']) || empty($vars['sms_content']))
            $errors['content'] = 'Content is King!';

        $vars['sms_content'] = trim($vars['sms_content']);

        if (strlen($vars['sms_content']) <= 10 || strlen($vars['sms_content']) > SMS_CHAR_LIMIT)
            $errors['content'] = 'Content must be more than 10 characters and less than '.SMS_CHAR_LIMIT.' characters.';

        $vars['phone_number'] = $phone_number;
        $vars['content'] = _String::khongdau($vars['sms_content']);
        $vars['title'] = 'Send SMS to: '.$phone_number;

        if(!($sms=$this->getThread()->addSMS($vars, $errors)))
            return null;

        // update updated time of ticket
        if (isset($vars['id']) && is_numeric($vars['id'])) {
            $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW() ';
            $sql.=' WHERE ticket_id='.db_input($vars['id']);

            db_query($sql);
        }

        // Get assigned staff just in case the ticket is closed.
        $assignee = $this->getStaff();

        if ($vars['note_status_id']
                && ($status=TicketStatus::lookup($vars['note_status_id']))) {
            if ($this->setStatus($status))
                $this->reload();
        }

        $activity = $vars['activity'] ?: _S('Send SMS');
        $this->onActivity(array(
            'activity' => $activity,
            'threadentry' => $sms,
            'assignee' => $assignee
        ), false);

        $this->clearOverdue(); //
        if (($vars['staffId'] && $vars['staffId'] != $this->getStaffId()) || !$vars['staffId'])
            $this->markAsUnRead();
        elseif ($vars['staffId'] && $vars['staffId'] == $this->getStaffId())
            $this->markAsRead();

        return $sms;
    }

    //Print ticket... export the ticket thread as PDF.
    function pdfExport($psize='Letter', $notes=false) {
        global $thisstaff;

        require_once(INCLUDE_DIR.'class.pdf.php');
        if (!is_string($psize)) {
            if ($_SESSION['PAPER_SIZE'])
                $psize = $_SESSION['PAPER_SIZE'];
            elseif (!$thisstaff || !($psize = $thisstaff->getDefaultPaperSize()))
                $psize = 'Letter';
        }

        $pdf = new Ticket2PDF($this, $psize, $notes);
        $name='Ticket-'.$this->getNumber().'.pdf';
        $pdf->Output($name, 'I');
        //Remember what the user selected - for autoselect on the next print.
        $_SESSION['PAPER_SIZE'] = $psize;
        exit;
    }

    function delete($comments='') {
        global $ost, $thisstaff;

        //delete just orphaned ticket thread & associated attachments.
        // Fetch thread prior to removing ticket entry
        $t = $this->getThread();

        $sql = 'DELETE FROM '.TICKET_TABLE.' WHERE ticket_id='.$this->getId().' LIMIT 1';
        if(!db_query($sql))
            return false;

        $t->delete();

        foreach (DynamicFormEntry::forTicket($this->getId()) as $form)
            $form->delete();

        $this->deleteDrafts();

        $sql = 'DELETE FROM '.TICKET_TABLE.'__cdata WHERE `ticket_id`='
            .db_input($this->getId());
        // If the CDATA table doesn't exist, that's not an error
        db_query($sql, false);

        // Log delete
        $log = sprintf(__('Ticket #%1$s deleted by %2$s'),
                $this->getNumber(),
                $thisstaff ? $thisstaff->getName() : __('SYSTEM'));

        if ($comments)
            $log .= sprintf('<hr>%s', $comments);

        $ost->logDebug(
                sprintf( __('Ticket #%s deleted'), $this->getNumber()),
                $log);

        return true;
    }

    function deleteDrafts() {
        Draft::deleteForNamespace('ticket.%.' . $this->getId());
    }

    function update($vars, &$errors) {

        global $cfg, $thisstaff;

        if(!$cfg || !$thisstaff || !$thisstaff->canEditTickets())
            return false;

        $fields=array();
        $fields['topicId']  = array('type'=>'int',      'required'=>1, 'error'=>__('Help topic selection is required'));
        $fields['slaId']    = array('type'=>'int',      'required'=>1, 'error'=>__('Select a valid SLA'));
        $fields['duedate']  = array('type'=>'date',     'required'=>0, 'error'=>__('Invalid date format - must be MM/DD/YY'));

        $fields['note']     = array('type'=>'text',     'required'=>1, 'error'=>__('A reason for the update is required'));
        $fields['user_id']  = array('type'=>'int',      'required'=>0, 'error'=>__('Invalid user-id'));

        if(!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] = __('Missing or invalid data - check the errors and try again');

        if($vars['duedate']) {
            if($this->isClosed())
                $errors['duedate']=__('Due date can NOT be set on a closed ticket');
            elseif(!$vars['time'] || strpos($vars['time'],':')===false)
                $errors['time']=__('Select a time from the list');
            elseif(strtotime($vars['duedate'].' '.$vars['time'])===false)
                $errors['duedate']=__('Invalid due date');
            elseif(strtotime($vars['duedate'].' '.$vars['time'])<=time())
                $errors['duedate']=__('Due date must be in the future');
        }

        // Validate dynamic meta-data
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            // Don't validate deleted forms
            if (!in_array($form->getId(), $vars['forms']))
                continue;
            $form->setSource($_POST);
            if (!$form->isValid())
                $errors = array_merge($errors, $form->errors());
        }

        if($errors) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW() '
            .' ,topic_id='.db_input($vars['topicId'])
            .' ,sla_id='.db_input($vars['slaId'])
            .' ,source='.db_input($vars['source'])
            .' ,duedate='.($vars['duedate']?db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time']))):'NULL');

        if($vars['user_id'])
            $sql.=', user_id='.db_input($vars['user_id']);
        if($vars['duedate']) { //We are setting new duedate...
            $sql.=' ,isoverdue=0';
        }

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        if(!db_query($sql))
            return false;

        if(!$vars['note'])
            $vars['note']=sprintf(_S('Ticket details updated by %s'), $thisstaff->getName());

        $note = $this->logNote(_S('Ticket Updated'), $vars['note'], $thisstaff);
        if ($note && $note->getId())
            $_SESSION['___THREAD_ID'] = $note->getId();

        // Decide if we need to keep the just selected SLA
        $keepSLA = ($this->getSLAId() != $vars['slaId']);

        // TODO: set form cho có data
        // Update dynamic meta-data
        foreach ($forms as $f) {
            // Drop deleted forms
            $idx = array_search($f->getId(), $vars['forms']);
            if ($idx === false) {
                $f->delete();
            }
            else {
                $f->set('sort', $idx);
                $f->save();
            }
        }

        // Reload the ticket so we can do further checking
        $this->reload();

        // Reselect SLA if transient
        if (!$keepSLA
                && (!$this->getSLA() || $this->getSLA()->isTransient()))
            $this->selectSLAId();

        // Clear overdue flag if duedate or SLA changes and the ticket is no longer overdue.
        if($this->isOverdue()
                && (!$this->getEstDueDate() //Duedate + SLA cleared
                    || Misc::db2gmtime($this->getEstDueDate()) > Misc::gmtime() //New due date in the future.
                    )) {
            $this->clearOverdue();
        }

        Signal::send('model.updated', $this);
        return true;
    }

    function updateIO($vars, &$errors) {

        global $cfg, $thisstaff;

        if(!$cfg || !$thisstaff || !$thisstaff->canEditPayments())
            return false;

        // Validate dynamic meta-data
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            // Don't validate deleted forms
            if (!in_array($form->getId(), $vars['forms']))
                continue;
            $form->setSource($_POST);
            if (!$form->isValid())
                $errors = array_merge($errors, $form->errors());
        }

        if($errors) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW() '
            .' ,topic_id='.db_input($vars['topicId'])
            .' ,sla_id='.db_input($vars['slaId'])
            .' ,source='.db_input($vars['source'])
            .' ,duedate='.($vars['duedate']?db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time']))):'NULL');

        if($vars['user_id'])
            $sql.=', user_id='.db_input($vars['user_id']);
        if($vars['duedate']) { //We are setting new duedate...
            $sql.=' ,isoverdue=0';
        }

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        if(!db_query($sql))
            return false;

        // Decide if we need to keep the just selected SLA
        $keepSLA = ($this->getSLAId() != $vars['slaId']);

        $__comment = '';
        // TODO: set form cho có data
        // Update dynamic meta-data
        foreach ($forms as $f) {
            // Drop deleted forms
            $idx = array_search($f->getId(), $vars['forms']);
            if ($idx === false) {
                $f->delete();
            }
            else {
                $f->set('sort', $idx);
                $f->save();
            }

            if (($___value = $f->getValue())) {
                $__comment .= $___value;
            }
        }

        $id = $this->getId();
        $hash = Payment::push($id);
        FlashMsg::set('payment_hash', $hash);
        $vars['note']=sprintf(_S('<p>Ticket details updated by %s</p><hr />'), $thisstaff->getName());
        $vars['note'].=$__comment;
        $vars['note'] .= '<hr><br><p><strong>'.$hash.'</strong></p>';
        $this->logNote(_S('Payment Updated - by '. $thisstaff->getName()), $vars['note'], $thisstaff);

        // Reload the ticket so we can do further checking
        $this->reload();

        // Reselect SLA if transient
        if (!$keepSLA
                && (!$this->getSLA() || $this->getSLA()->isTransient()))
            $this->selectSLAId();

        // Clear overdue flag if duedate or SLA changes and the ticket is no longer overdue.
        if($this->isOverdue()
                && (!$this->getEstDueDate() //Duedate + SLA cleared
                    || Misc::db2gmtime($this->getEstDueDate()) > Misc::gmtime() //New due date in the future.
                    )) {
            $this->clearOverdue();
        }

        Signal::send('model.updated', $this);

        return $hash;
    }

    function updateOL($vars, &$errors) {

        global $cfg, $thisstaff;

        if(!$cfg || !$thisstaff || !$thisstaff->canEditOfflates())
            return false;

        // Validate dynamic meta-data
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            // Don't validate deleted forms
            if (!in_array($form->getId(), $vars['forms']))
                continue;
            $form->setSource($_POST);
            if (!$form->isValid())
                $errors = array_merge($errors, $form->errors());
        }

        if($errors) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), lastmessage=NOW() '
            .' ,topic_id='.db_input($vars['topicId'])
            .' ,sla_id='.db_input($vars['slaId'])
            .' ,source='.db_input($vars['source'])
            .' ,duedate='.($vars['duedate']?db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time']))):'NULL');

        if($vars['user_id'])
            $sql.=', user_id='.db_input($vars['user_id']);
        if($vars['duedate']) { //We are setting new duedate...
            $sql.=' ,isoverdue=0';
        }

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        if(!db_query($sql))
            return false;

        // Decide if we need to keep the just selected SLA
        $keepSLA = ($this->getSLAId() != $vars['slaId']);

        $string_data = '';
        $agent = null;

        // TODO: set form cho có data
        // Update dynamic meta-data
        foreach ($forms as $f) {
            // Drop deleted forms
            $idx = array_search($f->getId(), $vars['forms']);
            if ($idx === false) {
                $f->delete();
            }
            else {
                $f->set('sort', $idx);
                $f->save();
            }
        }

        if ($this->getTopicId()) {
            if ($topic=Topic::lookup($this->getTopicId())) {
                if ($topic_form = $topic->getForm()) {
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                }
            }
        }

        // get some field from dynamic form
        if (isset($topic_form) && $topic_form) {
            $topic_form_form = $topic_form->getForm();
            if ($topic_form_form && is_callable([$topic_form_form, 'getDynamicFields'])) {
                foreach ($topic_form->getAnswers() as $answer) {
                    $field = $answer->getField();
                    $field->setForm($topic_form);
                    $val = $field->to_database($field->getClean());
                    if ($field->get('name') && $answer->get('field_id') == $field->get('id')) {
                        $fucking_vars[ $field->get('name') ] = $val;

                        if (strpos($field->get('name'), 'ol_status') !== false
                            || strpos($field->get('name'), 'time_accept') !== false)
                            continue;

                        if (strpos($field->get('name'), 'time') !== false && is_numeric(_String::json_decode($val)))
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), Format::userdate('d/m/Y H:i', _String::json_decode($val)));
                        else
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), _String::json_decode($val));
                    }
                }
            }
        }

        $string_data .= sprintf('<p>Số ngày nghỉ: %s</p>', Offlate::calc($fucking_vars['time_offer'], $fucking_vars['time_end']));

        if ($agent && isset($agent[1]) && $agent[1] != $this->getStaffId())
            $this->assignToStaff($agent[1]);

        $vars['note']=sprintf(_S('<p>Absence request details updated by %s</p><hr />'), $thisstaff->getName());
        $vars['note'].=$string_data;

        $assignId = $_POST['assignId'];
        $this->assign($assignId, $vars['note'], true);

        // Reload the ticket so we can do further checking
        $this->reload();

        // Reselect SLA if transient
        if (!$keepSLA
            && (!$this->getSLA() || $this->getSLA()->isTransient()))
            $this->selectSLAId();

        // Clear overdue flag if duedate or SLA changes and the ticket is no longer overdue.
        if($this->isOverdue()
            && (!$this->getEstDueDate() //Duedate + SLA cleared
                || Misc::db2gmtime($this->getEstDueDate()) > Misc::gmtime() //New due date in the future.
            )) {
            $this->clearOverdue();
        }

        Staff::push($this->getId());
        return true;
    }

    function updateBooking($vars, &$errors) {

        global $cfg, $thisstaff;

        if(!$cfg || !$thisstaff || !$thisstaff->canEditBookings())
            return false;

        // Validate dynamic meta-data
        $forms = DynamicFormEntry::forTicket($this->getId());
        foreach ($forms as $form) {
            // Don't validate deleted forms
            if (!in_array($form->getId(), $vars['forms']))
                continue;
            $form->setSource($_POST);
            if (!$form->isValid())
                $errors = array_merge($errors, $form->errors());
        }

        if($errors) return false;

        $sql='UPDATE '.TICKET_TABLE.' SET updated=NOW(), lastmessage=NOW() '
            .' ,topic_id='.db_input($vars['topicId'])
            .' ,sla_id='.db_input($vars['slaId'])
            .' ,source='.db_input($vars['source'])
            .' ,duedate='.($vars['duedate']?db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time']))):'NULL');

        if($vars['user_id'])
            $sql.=', user_id='.db_input($vars['user_id']);
        if($vars['duedate']) { //We are setting new duedate...
            $sql.=' ,isoverdue=0';
        }

        $sql.=' WHERE ticket_id='.db_input($this->getId());

        if(!db_query($sql))
            return false;

        // Decide if we need to keep the just selected SLA
        $keepSLA = ($this->getSLAId() != $vars['slaId']);

        $__comment = '';
         $agent = null;

        // TODO: set form cho có data
        // Update dynamic meta-data
        foreach ($forms as $f) {
            // Drop deleted forms
            $idx = array_search($f->getId(), $vars['forms']);
            if ($idx === false) {
                $f->delete();
            }
            else {
                $f->set('sort', $idx);
                $f->save();
            }

            if (($___value = $f->getValue())) {
                $__comment .= $___value;
            }
        }

        if ($this->getTopicId()) {
            if ($topic=Topic::lookup($this->getTopicId())) {
                if ($topic_form = $topic->getForm()) {
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                }
            }
        }

        // get some field from dynamic form
        if (isset($topic_form) && $topic_form) {
            $topic_form_form = $topic_form->getForm();
            if ($topic_form_form && is_callable([$topic_form_form, 'getDynamicFields'])) {
                foreach ($topic_form->getAnswers() as $answer) {
                    $field = $answer->getField();
                    $field->setForm($topic_form);
                    $val = $field->to_database($field->getClean());

                    if ('staff' == $field->get('name')) {
                        $agent = $val;
                    }
                }
            }
        }

        if ($agent && isset($agent[1]) && $agent[1] != $this->getStaffId())
            $this->assignToStaff($agent[1]);

        $vars['note']=sprintf(_S('<p>Ticket details updated by %s</p><hr />'), $thisstaff->getName());
        $vars['note'].=$__comment;

        $this->logNote(_S('Ticket Updated'), $vars['note'], $thisstaff);

        // Reload the ticket so we can do further checking
        $this->reload();

        // Reselect SLA if transient
        if (!$keepSLA
                && (!$this->getSLA() || $this->getSLA()->isTransient()))
            $this->selectSLAId();

        // Clear overdue flag if duedate or SLA changes and the ticket is no longer overdue.
        if($this->isOverdue()
                && (!$this->getEstDueDate() //Duedate + SLA cleared
                    || Misc::db2gmtime($this->getEstDueDate()) > Misc::gmtime() //New due date in the future.
                    )) {
            $this->clearOverdue();
        }

        Booking::push($this->getId());
        Signal::send('TriggerList.BOOKING_NEW', $this);
        return true;
    }

    function new_updateBooking($vars, &$errors){
        global $cfg,$thisstaff;
        if(!$cfg || !$thisstaff || !$thisstaff->canEditBookings())
            return false;
        $booking_tmp = Booking::lookup((int)$vars['id']);

        if(!$booking_tmp) return false;
        $res = Booking::upadteBooking($vars);
        // Reload the ticket so we can do further checking
        $this->reload();
        Booking::_calendar($this->getId());

        return $res;
    }
   /*============== Static functions. Use Ticket::function(params); =============nolint*/
    function getIdByNumber($number, $email=null) {

        if(!$number)
            return 0;

        $sql ='SELECT ticket.ticket_id FROM '.TICKET_TABLE.' ticket '
             .' LEFT JOIN '.USER_TABLE.' user ON user.id = ticket.user_id'
             .' LEFT JOIN '.USER_EMAIL_TABLE.' email ON user.id = email.user_id'
             .' WHERE ticket.`number`='.db_input($number);

        if($email)
            $sql .= ' AND email.address = '.db_input($email);

        if(($res=db_query($sql)) && db_num_rows($res))
            list($id)=db_fetch_row($res);

        return $id;
    }



    function lookup($id) { //Assuming local ID is the only lookup used!
        return ($id
                && is_numeric($id)
                && ($ticket= new Ticket($id))
                && $ticket->getId()==$id)
            ?$ticket:null;
    }

    function lookupByNumber($number, $email=null) {
        return self::lookup(self::getIdByNumber($number, $email));
    }

    static function isTicketNumberUnique($number) {
        return 0 == db_num_rows(db_query(
            'SELECT ticket_id FROM '.TICKET_TABLE.' WHERE `number`='.db_input($number)));
    }

    function getIdByMessageId($mid, $email) {

        if(!$mid || !$email)
            return 0;

        $sql='SELECT ticket.ticket_id FROM '.TICKET_TABLE. ' ticket '.
             ' LEFT JOIN '.TICKET_THREAD_TABLE.' msg USING(ticket_id) '.
             ' INNER JOIN '.TICKET_EMAIL_INFO_TABLE.' emsg ON (msg.id = emsg.message_id) '.
             ' WHERE email_mid='.db_input($mid).' AND email='.db_input($email);
        $id=0;
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id)=db_fetch_row($res);

        return $id;
    }

    /* Quick staff's tickets stats */
    function getStaffStats($staff,$topic_id=1) {//Minh Bao: add param $topic_id
        global $cfg;

        /* Unknown or invalid staff */
        if(!$staff || (!is_object($staff) && !($staff=Staff::lookup($staff))) || !$staff->isStaff())
            return null;

        $closed = TicketStatusList::getIds(false);
        $open = TicketStatusList::getIds(true);
        if (!$open || !$closed) return null;
        $open = implode(',', $open);
        $closed = implode(',', $closed);

        $where2 = '';
        if(!$cfg || !($cfg->showAssignedTickets() || $staff->showAssignedTickets()))
            $where2 =' AND ticket.staff_id=0 ';

        $where = array('(ticket.staff_id='.db_input($staff->getId()) .')');

        if(($teams=$staff->getTeams()))
            $where[] = ' ( ticket.team_id IN('.implode(',', db_input(array_filter($teams))).') )';

        if(!$staff->showAssignedOnly() && ($depts=$staff->getDepts())) //Staff with limited access just see Assigned tickets.
            $where[] = 'ticket.dept_id IN('.implode(',', db_input($depts)).') ';

        $where = implode(' OR ', $where);
        if ($where) $where = 'AND ( '.$where.' ) ';

        $sub = "select count(*) as qty, staff_id, status_id, dept_id, topic_id, isoverdue, isanswered, team_id
           FROM ost_ticket
           group by staff_id, status_id, dept_id, topic_id, isoverdue, isanswered";

        $sql = "SELECT SUM(
            case when ticket.isanswered = 0 ".$where2." and ticket.status_id IN (".$open.") then qty else 0 end
            ) AS open,
                 SUM(
            case when ticket.isanswered = 1 and ticket.status_id IN (".$open.") then qty else 0 end
            ) AS answered,
                 SUM(
            case when ticket.isoverdue = 1 and ticket.status_id IN (".$open.") then qty else 0 end
            ) AS overdue,
                 SUM(
            case when ticket.staff_id = ".db_input($staff->getId())." and ticket.status_id IN (".$open.") then qty else 0 end
            ) AS assigned,
                 SUM(
            case when (ticket.staff_id = 0 OR ticket.staff_id IS NULL) and ticket.status_id IN (".$open.") then qty else 0 end
            ) AS unassigned,
                 SUM(
            case when ticket.status_id IN (".$closed.") then qty else 0 end
            ) AS closed
          FROM ($sub) ticket
          WHERE 1
          AND ticket.dept_id NOT IN (".implode(',', unserialize(FINANCE_DEPT_ID)).")
            AND ticket.topic_id IN (".implode(',', unserialize(SALE_TOPIC_NO_TODO)).")
            $where;";

        $res = db_query($sql);
        if (!$res) return [];
        return db_fetch_array($res);
    }

    /* Quick client's tickets stats
       @email - valid email.
     */
    function getUserStats($user) {
        if(!$user || !($user instanceof EndUser))
            return null;

        $sql='SELECT count(open.ticket_id) as open, count(closed.ticket_id) as closed '
            .' FROM '.TICKET_TABLE.' ticket '
            .' LEFT JOIN '.TICKET_TABLE.' open
                ON (open.ticket_id=ticket.ticket_id AND open.status=\'open\') '
            .' LEFT JOIN '.TICKET_TABLE.' closed
                ON (closed.ticket_id=ticket.ticket_id AND closed.status=\'closed\')'
            .' WHERE ticket.user_id = '.db_input($user->getId());

        return db_fetch_array(db_query($sql));
    }

    protected function filterTicketData($origin, $vars, $forms, $user=false) {
        global $cfg;

        // Unset all the filter data field data in case things change
        // during recursive calls
        foreach ($vars as $k=>$v)
            if (strpos($k, 'field.') === 0)
                unset($vars[$k]);

        foreach ($forms as $F) {
            if ($F) {
                $vars += $F->getFilterData();
            }
        }

        if (!$user) {
            $interesting = array('name', 'email');
            $user_form = UserForm::getUserForm()->getForm($vars);
            // Add all the user-entered info for filtering
            foreach ($interesting as $F) {
                $field = $user_form->getField($F);
                $vars[$F] = $field->toString($field->getClean());
            }
            // Attempt to lookup the user and associated data
            $user = User::lookupByEmail($vars['email']);
        }

        // Add in user and organization data for filtering
        if ($user) {
            $vars += $user->getFilterData();
            $vars['email'] = $user->getEmail();
            $vars['name'] = $user->getName()->getOriginal();
            if ($org = $user->getOrganization()) {
                $vars += $org->getFilterData();
            }
        }
        // Don't include org information based solely on email domain
        // for existing user instances
        else {
            // Unpack all known user info from the request
            foreach ($user_form->getFields() as $f) {
                $vars['field.'.$f->get('id')] = $f->toString($f->getClean());
            }
            // Add in organization data if one exists for this email domain
            list($mailbox, $domain) = explode('@', $vars['email'], 2);
            if ($org = Organization::forDomain($domain)) {
                $vars += $org->getFilterData();
            }
        }

        try {
            // Make sure the email address is not banned
            if (TicketFilter::isBanned($vars['email'])) {
                throw new RejectedException(Banlist::getFilter(), $vars);
            }

            // Init ticket filters...
            $ticket_filter = new TicketFilter($origin, $vars);
            $ticket_filter->apply($vars);
        }
        catch (FilterDataChanged $ex) {
            // Don't pass user recursively, assume the user has changed
            return self::filterTicketData($origin, $ex->getData(), $forms);
        }
        return $vars;
    }

    /*
     * The mother of all functions...You break it you fix it!
     *
     *  $autorespond and $alertstaff overrides config settings...
     */
    static function create($vars, &$errors, $origin, $autorespond=true,
            $alertstaff=true) {
        global $ost, $cfg, $thisclient, $_FILES;

        // Don't enforce form validation for email
        $field_filter = function($type) use ($origin) {
            return function($f) use ($origin, $type) {
                // Ultimately, only offer validation errors for web for
                // non-internal fields. For email, no validation can be
                // performed. For other origins, validate as usual
                switch (strtolower($origin)) {
                case 'email':
                    return false;
                case 'staff':
                    // Required 'Contact Information' fields aren't required
                    // when staff open tickets
                    return $type != 'user'
                        || in_array($f->get('name'), array('name','email'));
                case 'web':
                    return !$f->get('private');
                default:
                    return true;
                }
            };
        };

        $reject_ticket = function($message) use (&$errors) {
            global $ost;
            $errors = array(
                'errno' => 403,
                'err' => __('This help desk is for use by authorized guests only'));
            $ost->logWarning(_S('Ticket Denied'), $message, false);
            return 0;
        };

        Signal::send('ticket.create.before', null, $vars);

        // Create and verify the dynamic form entry for the new ticket
        $form = TicketForm::getNewInstance();
        $form->setSource($vars);

        // If submitting via email or api, ensure we have a subject and such
        if (!in_array(strtolower($origin), array('web', 'staff'))) {
            foreach ($form->getFields() as $field) {
                $fname = $field->get('name');
                if ($fname && isset($vars[$fname]) && !$field->value)
                    $field->value = $field->parse($vars[$fname]);
            }
        }

        if (!$form->isValid($field_filter('ticket')))
            $errors[] = $form->errors();

        $vars['phone'] = _String::formatPhoneNumber($vars['phone']);
        $vars['email'] = trim($vars['email']);

        if ($vars['uid'])
            $user = User::lookup($vars['uid']);

        if (isset($vars['phone']) && $vars['phone']
            && ($check_user = User::lookupByPhone(trim($vars['phone'])))
            && ($user_email = $check_user->getEmail())) {

            if ( (!isset($vars['email']) || empty($vars['email']) || !$vars['email'])
                || (strpos(strtolower($vars['email']), '@tugo.com') !== false
                    && strpos(strtolower($user_email), '@tugo.com') === false) ) {
                $vars['email'] = $user_email;

            } elseif ($user_email !== $vars['email']) {
                $errors['email'] = 'Hệ thống đã có số điện thoại <strong class="color black">'.$vars['phone']
                    . '</strong>, khách này đang dùng email <strong class="color black">'
                    .$user_email.'</strong>. Hãy điền vào ô Email Address là <strong class="color black">'.$check_user->getEmail()
                    .'</strong> khi tạo ticket + note email mới (nếu có) bên dưới. Sau khi tạo ticket, có thể EDIT lại thông tin khách với email phù hợp.';
            }

        } elseif (isset($vars['email']) && $vars['email']
            && ($check_user = User::lookupByEmail(trim($vars['email'])))
            && ($user_phonenumber = _String::formatPhoneNumber($check_user->getPhoneNumber()))) {

            if (!isset($vars['phone']) || empty($vars['phone']) || !$vars['phone']) {
                $vars['phone'] = $user_phonenumber;

            } elseif ($user_phonenumber !== $vars['phone']) {
                $errors['phone'] = 'Hệ thống đã có email <strong class="color black">'.$vars['email']
                    . '</strong>, khách này đang dùng số điện thoại <strong class="color black">'
                    .$user_phonenumber.'</strong>. Hãy điền vào ô Phone Number là <strong class="color black">'.$user_phonenumber
                    .'</strong> khi tạo ticket + note số điện thoại mới (nếu có) bên dưới. Sau khi tạo ticket, có thể EDIT lại thông tin khách với số điện thoại phù hợp.';
            }
        }

        $id=0;
        $fields=array();
        switch (strtolower($origin)) {
            case 'web':
                $fields['topicId']  = array('type'=>'int',  'required'=>1, 'error'=>__('Select a help topic'));
                break;
            case 'staff':
                $fields['deptId']   = array('type'=>'int',  'required'=>0, 'error'=>__('Department selection is required'));
                $fields['topicId']  = array('type'=>'int',  'required'=>1, 'error'=>__('Help topic selection is required'));
                $fields['duedate']  = array('type'=>'date', 'required'=>0, 'error'=>__('Invalid date format - must be MM/DD/YY'));
            case 'api':
                $fields['source']   = array('type'=>'string', 'required'=>1, 'error'=>__('Indicate ticket source'));
                break;
            case 'email':
                $fields['emailId']  = array('type'=>'int',  'required'=>1, 'error'=>__('Unknown system email'));
                break;
            default:
                # TODO: Return error message
                $errors['err']=$errors['origin'] = __('Invalid ticket origin given');
        }

        if(!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] =__('Missing or invalid data - check the errors and try again');

        //Make sure the due date is valid
        if($vars['duedate']) {
            if(!$vars['time'] || strpos($vars['time'],':')===false)
                $errors['time']=__('Select a time from the list');
            elseif(strtotime($vars['duedate'].' '.$vars['time'])===false)
                $errors['duedate']=__('Invalid due date');
            elseif(strtotime($vars['duedate'].' '.$vars['time'])<=time())
                $errors['duedate']=__('Due date must be in the future');
        }

        if (!$errors) {

            # Perform ticket filter actions on the new ticket arguments
            $__form = null;
            if ($vars['topicId']) {
                if (($__topic=Topic::lookup($vars['topicId']))
                    && ($__form = $__topic->getForm())
                ) {
                    $__form = $__form->instanciate();
                    $__form->setSource($vars);
                }
            }

            try {
                $vars = self::filterTicketData($origin, $vars,
                    array($form, $__form), $user);
            }
            catch (RejectedException $ex) {
                return $reject_ticket(
                    sprintf(_S('Ticket rejected (%s) by filter "%s"'),
                    $ex->vars['email'], $ex->getRejectingFilter()->getName())
                );
            }

            //Make sure the open ticket limit hasn't been reached. (LOOP CONTROL)
            if ($cfg->getMaxOpenTickets() > 0
                    && strcasecmp($origin, 'staff')
                    && ($_user=TicketUser::lookupByEmail($vars['email']))
                    && ($openTickets=$_user->getNumOpenTickets())
                    && ($openTickets>=$cfg->getMaxOpenTickets()) ) {

                $errors = array('err' => __("You've reached the maximum open tickets allowed."));
                $ost->logWarning(sprintf(_S('Ticket denied - %s'), $vars['email']),
                        sprintf(_S('Max open tickets (%1$d) reached for %2$s'),
                            $cfg->getMaxOpenTickets(), $vars['email']),
                        false);

                return 0;
            }

            // Allow vars to be changed in ticket filter and applied to the user
            // account created or detected
            if (!$user && $vars['email'])
                $user = User::lookupByEmail($vars['email']);

            if (!$user) {
                // Reject emails if not from registered clients (if
                // configured)
                if (strcasecmp($origin, 'email') === 0
                    && !$cfg->acceptUnregisteredEmail()) {
                    list($mailbox, $domain) = explode('@', $vars['email'], 2);
                    // Users not yet created but linked to an organization
                    // are still acceptable
                    if (!Organization::forDomain($domain)) {
                        return $reject_ticket(
                            sprintf(_S('Ticket rejected (%s) (unregistered client)'),
                                $vars['email']));
                    }
                }

                $user_form = UserForm::getUserForm()->getForm($vars);
                if (!$user_form->isValid($field_filter('user'))
                    || !($user=User::fromVars($user_form->getClean())))
                    $errors['user'] = __('Incomplete client information');
            }
        }

        if ($vars['topicId']) {
            if ($topic=Topic::lookup($vars['topicId'])) {
                if ($topic_form = $topic->getForm()) {
                    $TF = $topic_form->getForm($vars);
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                    if (!$TF->isValid($field_filter('topic')))
                        $errors = array_merge($errors, $TF->errors());
                }
            }
            else  {
                $errors['topicId'] = 'Invalid help topic selected';
            }
        }

        // Any error above is fatal.
        if ($errors)
            return 0;

        Signal::send('ticket.create.validated', null, $vars);

        # Some things will need to be unpacked back into the scope of this
        # function
        if (isset($vars['autorespond']))
            $autorespond = $vars['autorespond'];

        # Apply filter-specific priority
        if ($vars['priorityId'])
            $form->setAnswer('priority', null, $vars['priorityId']);

        // If the filter specifies a help topic which has a form associated,
        // and there was previously either no help topic set or the help
        // topic did not have a form, there's no need to add it now as (1)
        // validation is closed, (2) there may be a form already associated
        // and filled out from the original  help topic, and (3) staff
        // members can always add more forms now

        // OK...just do it.
        $statusId = $vars['statusId'];
        $deptId = $vars['deptId']; //pre-selected Dept if any.
        $source = ucfirst($vars['source']);

        // Apply email settings for emailed tickets. Email settings should
        // trump help topic settins if the email has an associated help
        // topic
        if ($vars['emailId'] && ($email=Email::lookup($vars['emailId']))) {
            $deptId = $deptId ?: $email->getDeptId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $email->getPriorityId());
            if ($autorespond)
                $autorespond = $email->autoRespond();
            if (!isset($topic)
                    && ($T = $email->getTopic())
                    && ($T->isActive())) {
                $topic = $T;
            }
            $email = null;
            $source = 'Email';
        }

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $deptId = $deptId ?: $topic->getDeptId();
            $statusId = $statusId ?: $topic->getStatusId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $topic->getPriorityId());
            if ($autorespond)
                $autorespond = $topic->autoRespond();

            //Auto assignment.
            if (!isset($vars['staffId']) && $topic->getStaffId())
                $vars['staffId'] = $topic->getStaffId();
            elseif (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ?: $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Auto assignment to organization account manager
        if (($org = $user->getOrganization())
                && $org->autoAssignAccountManager()
                && ($code = $org->getAccountManagerId())) {
            if (!isset($vars['staffId']) && $code[0] == 's')
                $vars['staffId'] = substr($code, 1);
            elseif (!isset($vars['teamId']) && $code[0] == 't')
                $vars['teamId'] = substr($code, 1);
        }

        // Last minute checks
        $priority = $form->getAnswer('priority');
        if (!$priority || !$priority->getIdValue())
            $form->setAnswer('priority', null, $cfg->getDefaultPriorityId());
        $deptId = $deptId ?: $cfg->getDefaultDeptId();
        $statusId = $statusId ?: $cfg->getDefaultTicketStatusId();
        $topicId = isset($topic) ? $topic->getId() : 0;
        $ipaddress = $vars['ip'] ?: $_SERVER['REMOTE_ADDR'];
        $source = $source ?: 'Web';

        //We are ready son...hold on to the rails.
        $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        // sorry, but i must modify this fucking mother function :v hahaha - buupham
        $sql='INSERT INTO '.TICKET_TABLE.' SET '
            .' created='.(isset($vars['created']) && !empty($vars['created']) ? db_input($vars['created']) : ' NOW() ')
            .' ,lastmessage= NOW()'
            .' ,user_id='.db_input($user->getId())
            .' ,`number`='.db_input($number)
            .' ,dept_id='.db_input($deptId)
            .' ,topic_id='.db_input($topicId)
            .' ,ip_address='.db_input($ipaddress)
            .' ,source='.db_input($source);

        if (isset($vars['emailId']) && $vars['emailId'])
            $sql.=', email_id='.db_input($vars['emailId']);

        //Make sure the origin is staff - avoid firebug hack!
        if($vars['duedate'] && !strcasecmp($origin,'staff'))
             $sql.=' ,duedate='.db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time'])));


        if(!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id)))
            return null;

		/* ------------- ADD COLLABORATOR booking@tugo.com.vn -------MINH BAO */

		$info = array('ticketId' => $id,
                'userId' => 8,
				'isactive' => 1);//ID=8; booking@tugo.com.vn

		if (!($c=Collaborator::add($info,$errors))){

		}

		/* ---- END ADD COLLABORATOR ---->

        /* -------------------- POST CREATE ------------------------ */

        // Save the (common) dynamic form
        $form->setTicketId($id);
        $form->save();

        // Save the form data from the help-topic form, if any
        if ($topic_form) {
            $topic_form->setTicketId($id);
            $topic_form->save();
        }

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        // Add organizational collaborators
        if ($org && $org->autoAddCollabs()) {
            $pris = $org->autoAddPrimaryContactsAsCollabs();
            $members = $org->autoAddMembersAsCollabs();
            $settings = array('isactive' => true);
            $collabs = array();
            foreach ($org->allMembers() as $u) {
                if ($members || ($pris && $u->isPrimaryContact())) {
                    if ($c = $ticket->addCollaborator($u, $settings, $errors)) {
                        $collabs[] = (string) $c;
                    }
                }
            }
            //TODO: Can collaborators add others?
            if ($collabs) {
                //TODO: Change EndUser to name of  user.
                $ticket->logNote(sprintf(_S('Collaborators for %s organization added'),
                        $org->getName()),
                    implode("<br>", $collabs), $org->getName(), false);
            }
        }

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
        if (!isset($vars['message']) || !$vars['message'])
            $vars['message'] = $vars['title'];
        if (!isset($vars['body']) || !$vars['body'])
            $vars['body'] = $vars['title'];

        $message = $ticket->postMessage($vars , $origin, false);

        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['note']);
        }
        else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staffId'])
                 $ticket->assignToStaff($vars['staffId'], _S('Auto Assignment'));
            if ($vars['teamId'])
                // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], _S('Auto Assignment'),
                    !$vars['staffId']);
        }

        // Apply requested status — this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId)
            $ticket->setStatus($statusId, false, false);

        /**********   double check auto-response  ************/
        //Override auto responder if the FROM email is one of the internal emails...loop control.
        if($autorespond && (Email::getIdByEmail($ticket->getEmail())))
            $autorespond=false;

        # Messages that are clearly auto-responses from email systems should
        # not have a return 'ping' message
        if (isset($vars['flags']) && $vars['flags']['bounce'])
            $autorespond = false;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        //post canned auto-response IF any (disables new ticket auto-response).
        if ($vars['cannedResponseId']
            && $ticket->postCannedReply($vars['cannedResponseId'], $message->getId(), $autorespond)) {
                $ticket->markUnAnswered(); //Leave the ticket as unanswred.
                $autorespond = false;
        }

        //Check department's auto response settings
        // XXX: Dept. setting doesn't affect canned responses.
        if($autorespond && $dept && !$dept->autoRespONNewTicket())
            $autorespond=false;

        //Don't send alerts to staff when the message is a bounce
        //  this is necessary to avoid possible loop (especially on new ticket)
        if ($alertstaff && $message->isBounce())
            $alertstaff = false;

        /***** See if we need to send some alerts ****/
        $ticket->onNewTicket($message, $autorespond, $alertstaff);

        /************ check if the user JUST reached the max. open tickets limit **********/
        if($cfg->getMaxOpenTickets()>0
                    && ($user=$ticket->getOwner())
                    && ($user->getNumOpenTickets()==$cfg->getMaxOpenTickets())) {
            $ticket->onOpenLimit(($autorespond && strcasecmp($origin, 'staff')));
        }

        /* Start tracking ticket lifecycle events */
        $ticket->logEvent('created');

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('model.created', $ticket);

        /* Phew! ... time for tea (KETEPA) */

        return $ticket;
    }

    static function createIO($vars, &$errors, $origin, $autorespond=true,
            $alertstaff=true) {
        global $ost, $cfg, $thisstaff, $_FILES;

        // Don't enforce form validation for email
        $field_filter = function($type) use ($origin) {
            return function($f) use ($origin, $type) {
                // Ultimately, only offer validation errors for web for
                // non-internal fields. For email, no validation can be
                // performed. For other origins, validate as usual
                switch (strtolower($origin)) {
                case 'email':
                    return false;
                case 'staff':
                    // Required 'Contact Information' fields aren't required
                    // when staff open tickets
                    return $type != 'user'
                        || in_array($f->get('name'), array('name','email'));
                case 'web':
                    return !$f->get('private');
                default:
                    return true;
                }
            };
        };

        $reject_ticket = function($message) use (&$errors) {
            global $ost;
            $errors = array(
                'errno' => 403,
                'err' => __('This help desk is for use by authorized guests only'));
            $ost->logWarning(_S('Ticket Denied'), $message, false);
            return 0;
        };

        Signal::send('ticket.create.before', null, $vars);

        // Create and verify the dynamic form entry for the new ticket
        $vars['subject'] = 'Kế toán';
        $vars['message'] = 'Kế toán';
        $form = TicketForm::getNewInstance();
        $form->setSource($vars);

        // If submitting via email or api, ensure we have a subject and such
        if (!in_array(strtolower($origin), array('web', 'staff'))) {
            foreach ($form->getFields() as $field) {
                $fname = $field->get('name');
                if ($fname && isset($vars[$fname]) && !$field->value)
                    $field->value = $field->parse($vars[$fname]);
            }
        }

        if (!$form->isValid($field_filter('ticket')))
            $errors = array_merge(is_array($errors) ? $errors : [], $form->errors()[0]);

        // validate Department
        if (!isset($vars['deptId']) || empty($vars['deptId']))
            $errors = array_merge(is_array($errors) ? $errors : [], 'Department is required.');

        $staff_dept = $thisstaff->getGroup()->getDepartments();
        if (!in_array($vars['deptId'], $staff_dept))
            $errors = array_merge(is_array($errors) ? $errors : [], 'Wrong Department!!!');

        $dept_name = Dept::getNameById($vars['deptId']);
        if (strpos(strtolower($dept_name), 'finance') === false)
            $errors = array_merge(is_array($errors) ? $errors : [], 'Wrong Department!!!');

        $vars['subject'] .= ' - ' . $dept_name . ' - Create by ' . $thisstaff->getName();
        $vars['message'] .= ' - ' . $dept_name . ' - Create by ' . $thisstaff->getName();
        // END validate Department

        if ($vars['uid'])
            $user = User::lookup($vars['uid']);

        $id=0;
        $fields=array();
        $fields['message']  = array('type'=>'*',     'required'=>1, 'error'=>__('Message content is required'));

        if (!$errors) {

            # Perform ticket filter actions on the new ticket arguments
            $__form = null;
            if ($vars['topicId']) {
                if (($__topic=Topic::lookup($vars['topicId']))
                    && ($__form = $__topic->getForm())
                ) {
                    $__form = $__form->instanciate();
                    $__form->setSource($vars);
                }
            }

            try {
                $vars = self::filterTicketData($origin, $vars,
                    array($form, $__form), $user);
            }
            catch (RejectedException $ex) {
                return $reject_ticket(
                    sprintf(_S('Ticket rejected (%s) by filter "%s"'),
                    $ex->vars['email'], $ex->getRejectingFilter()->getName())
                );
            }
        }

        if ($vars['topicId']) {
            if ($topic=Topic::lookup($vars['topicId'])) {
                if ($topic_form = $topic->getForm()) {
                    $TF = $topic_form->getForm($vars);
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                    if (!$TF->isValid($field_filter('topic')))
                        $errors = array_merge($errors, $TF->errors());
                }
            }
            else  {
                $errors['topicId'] = 'Invalid help topic selected';
            }
        }

        // Any error above is fatal.
        if ($errors)
            return 0;

        Signal::send('ticket.create.validated', null, $vars);


        // If the filter specifies a help topic which has a form associated,
        // and there was previously either no help topic set or the help
        // topic did not have a form, there's no need to add it now as (1)
        // validation is closed, (2) there may be a form already associated
        // and filled out from the original  help topic, and (3) staff
        // members can always add more forms now

        // OK...just do it.
        $statusId = $vars['statusId'];
        $deptId = $vars['deptId']; //pre-selected Dept if any.
        $source = ucfirst($vars['source']);

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $deptId = $deptId ?: $topic->getDeptId();
            $statusId = $statusId ?: $topic->getStatusId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $topic->getPriorityId());
            if ($autorespond)
                $autorespond = $topic->autoRespond();

            //Auto assignment.
            if (!isset($vars['staffId']) && $topic->getStaffId())
                $vars['staffId'] = $topic->getStaffId();
            elseif (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ?: $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Last minute checks
        $priority = $form->getAnswer('priority');
        if (!$priority || !$priority->getIdValue())
            $form->setAnswer('priority', null, $cfg->getDefaultPriorityId());
        $deptId = $deptId ?: $cfg->getDefaultDeptId();
        $statusId = $statusId ?: $cfg->getDefaultTicketStatusId();
        $topicId = isset($topic) ? $topic->getId() : 0;
        $ipaddress = $vars['ip'] ?: $_SERVER['REMOTE_ADDR'];
        $source = $source ?: 'Web';

        $string_data = '';
        $booking_code = '';
        $fucking_vars = [];
        // get some field from dynamic form
        if (isset($topic_form) && $topic_form) {
            $topic_form_form = $topic_form->getForm();
            if ($topic_form_form && is_callable([$topic_form_form, 'getDynamicFields'])) {
                foreach ($topic_form->getAnswers() as $answer) {
                    $field = $answer->getField();
                    $field->setForm($topic_form);
                    $val = $field->to_database($field->getClean());
                    if ($field->get('name') && $answer->get('field_id') == $field->get('id')) {
                        $fucking_vars[ $field->get('name') ] = $val;

                        if ($field->get('name') == 'note' && isset($vars['loyalty'])){
                            $val = $val.'  '.$vars['loyalty'];
                        }

                        if ($field->get('name') == 'time' && is_numeric(_String::json_decode($val)))
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), Format::userdate('d M Y', _String::json_decode($val)));
                        else
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), _String::json_decode($val));
                    }

                    if ('booking_code' == $field->get('name')) {
                        $booking_code = $val;
                    }
                }
            }
        }

        $vars['subject'] .= ' - '.$booking_code;
        $vars['message'] .= '<hr><br>'.$string_data;

        //We are ready son...hold on to the rails.
        $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        $created = isset($fucking_vars['time']) ? Format::userdate('Y-m-d H:i:s', $fucking_vars['time']) : $vars['created'];
        // sorry, but i must modify this fucking mother function :v hahaha - buupham
        $sql='INSERT INTO '.TICKET_TABLE.' SET '
            .' created='.(isset($created) && !empty($created) ? db_input($created) : ' NOW() ')
            .' ,lastmessage= NOW()'
            .' ,`number`='.db_input($number)
            .' ,dept_id='.db_input($deptId)
            .' ,topic_id='.db_input($topicId)
            .' ,ip_address='.db_input($ipaddress)
            .' ,source='.db_input($source);

        if (isset($vars['emailId']) && $vars['emailId'])
            $sql.=', email_id='.db_input($vars['emailId']);

        //Make sure the origin is staff - avoid firebug hack!
        if($vars['duedate'] && !strcasecmp($origin,'staff'))
             $sql.=' ,duedate='.db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time'])));


        if(!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id)))
            return null;

		/* ------------- ADD COLLABORATOR booking@tugo.com.vn -------MINH BAO */

		$info = array('ticketId' => $id,
                'userId' => 8,
				'isactive' => 1);//ID=8; booking@tugo.com.vn

		if (!($c=Collaborator::add($info,$errors))){

		}

		/* ---- END ADD COLLABORATOR ---->

        /* -------------------- POST CREATE ------------------------ */

        // Save the (common) dynamic form
        $form->setTicketId($id);
        $form->save();

        // Save the form data from the help-topic form, if any
        if ($topic_form) {
            $topic_form->setTicketId($id);
            $topic_form->save();
        }

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
        $id = $ticket->getId();
        $hash = Payment::push($id);
        $vars['message'] .= '<hr><br><p><strong>'.$hash.'</strong></p>';
        $message = $ticket->postMessage($vars , $origin, false);

        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['note']);
        }
        else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staffId'])
                 $ticket->assignToStaff($vars['staffId'], _S('Auto Assignment'));
            if ($vars['teamId'])
                // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], _S('Auto Assignment'),
                    !$vars['staffId']);
        }

        // Apply requested status — this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId)
            $ticket->setStatus($statusId, false, false);

        /**********   double check auto-response  ************/
        //Override auto responder if the FROM email is one of the internal emails...loop control.
        if($autorespond && (Email::getIdByEmail($ticket->getEmail())))
            $autorespond=false;

        # Messages that are clearly auto-responses from email systems should
        # not have a return 'ping' message
        if (isset($vars['flags']) && $vars['flags']['bounce'])
            $autorespond = false;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        //post canned auto-response IF any (disables new ticket auto-response).
        if ($vars['cannedResponseId']
            && $ticket->postCannedReply($vars['cannedResponseId'], $message->getId(), $autorespond)) {
                $ticket->markUnAnswered(); //Leave the ticket as unanswred.
                $autorespond = false;
        }

        //Check department's auto response settings
        // XXX: Dept. setting doesn't affect canned responses.
        if($autorespond && $dept && !$dept->autoRespONNewTicket())
            $autorespond=false;

        //Don't send alerts to staff when the message is a bounce
        //  this is necessary to avoid possible loop (especially on new ticket)
        if ($alertstaff && $message->isBounce())
            $alertstaff = false;

        /***** See if we need to send some alerts ****/
        $ticket->onNewTicket($message, $autorespond, $alertstaff);

        /************ check if the user JUST reached the max. open tickets limit **********/
        if($cfg->getMaxOpenTickets()>0
                    && ($user=$ticket->getOwner())
                    && ($user->getNumOpenTickets()==$cfg->getMaxOpenTickets())) {
            $ticket->onOpenLimit(($autorespond && strcasecmp($origin, 'staff')));
        }

        /* Start tracking ticket lifecycle events */
        $ticket->logEvent('created');

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('model.created', $ticket);

        /* Phew! ... time for tea (KETEPA) */

        FlashMsg::set('payment_id', $id);
        FlashMsg::set('payment_hash', $hash);

        return $ticket;
    }
    static function createOL($vars, &$errors, $origin, $autorespond=true,
                             $alertstaff=true) {
        global $ost, $cfg, $thisstaff, $_FILES;

        // Don't enforce form validation for email
        $field_filter = function($type) use ($origin) {
            return function($f) use ($origin, $type) {
                // Ultimately, only offer validation errors for web for
                // non-internal fields. For email, no validation can be
                // performed. For other origins, validate as usual
                switch (strtolower($origin)) {
                    case 'email':
                        return false;
                    case 'staff':
                        // Required 'Contact Information' fields aren't required
                        // when staff open tickets
                        return $type != 'user'
                            || in_array($f->get('name'), array('name','email'));
                    case 'web':
                        return !$f->get('private');
                    default:
                        return true;
                }
            };
        };

        $reject_ticket = function($message) use (&$errors) {
            global $ost;
            $errors = array(
                'errno' => 403,
                'err' => __('This help desk is for use by authorized guests only'));
            $ost->logWarning(_S('Ticket Denied'), $message, false);
            return 0;
        };

        Signal::send('ticket.create.before', null, $vars);

        // Create and verify the dynamic form entry for the new ticket
        $vars['subject'] = 'Đề nghị nghỉ phép/đi trễ :: '.$thisstaff->getName().' :: '.date('d/m/Y');
        $vars['message'] = 'Đề nghị nghỉ phép/đi trễ ';
        $form = TicketForm::getNewInstance();
        $form->setSource($vars);

        // If submitting via email or api, ensure we have a subject and such
        if (!in_array(strtolower($origin), array('web', 'staff'))) {
            foreach ($form->getFields() as $field) {
                $fname = $field->get('name');
                if ($fname && isset($vars[$fname]) && !$field->value)
                    $field->value = $field->parse($vars[$fname]);
            }
        }

        if (!$form->isValid($field_filter('ticket')))
            $errors = array_merge(is_array($errors) ? $errors : [], $form->errors()[0]);

        $vars['message'] .= ' -- Nhân viên ' . $thisstaff->getName().', tạo lúc @'.date('H:i:s, d/m/Y');
        // END validate Department

        if ($vars['uid'])
            $user = User::lookup($vars['uid']);

        $id=0;
        $fields=array();
        $fields['message']  = array('type'=>'*',     'required'=>1, 'error'=>__('Message content is required'));

        if (!$errors) {

            # Perform ticket filter actions on the new ticket arguments
            $__form = null;
            if ($vars['topicId']) {
                if (($__topic=Topic::lookup($vars['topicId']))
                    && ($__form = $__topic->getForm())
                ) {
                    $__form = $__form->instanciate();
                    $__form->setSource($vars);
                }
            }

            try {
                $vars = self::filterTicketData($origin, $vars,
                    array($form, $__form), $user);
            }
            catch (RejectedException $ex) {
                return $reject_ticket(
                    sprintf(_S('Ticket rejected (%s) by filter "%s"'),
                        $ex->vars['email'], $ex->getRejectingFilter()->getName())
                );
            }
        }

        if ($vars['topicId']) {
            if ($topic=Topic::lookup($vars['topicId'])) {
                if ($topic_form = $topic->getForm()) {
                    $TF = $topic_form->getForm($vars);
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                    if (!$TF->isValid($field_filter('topic')))
                        $errors = array_merge($errors, $TF->errors());
                }
            }
            else  {
                $errors['topicId'] = 'Invalid help topic selected';
            }
        }

        // Any error above is fatal.
        if ($errors)
            return 0;

        Signal::send('ticket.create.validated', null, $vars);


        // If the filter specifies a help topic which has a form associated,
        // and there was previously either no help topic set or the help
        // topic did not have a form, there's no need to add it now as (1)
        // validation is closed, (2) there may be a form already associated
        // and filled out from the original  help topic, and (3) staff
        // members can always add more forms now

        // OK...just do it.
        $statusId = $vars['statusId'];
        $deptId = $vars['deptId']; //pre-selected Dept if any.
        $source = ucfirst($vars['source']);

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $deptId = $deptId ?: $topic->getDeptId();
            $statusId = $statusId ?: $topic->getStatusId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $topic->getPriorityId());
            if ($autorespond)
                $autorespond = $topic->autoRespond();

            //Auto assignment.
            if (!isset($vars['staffId']) && $topic->getStaffId())
                $vars['staffId'] = $topic->getStaffId();
            elseif (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ?: $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Last minute checks
        $priority = $form->getAnswer('priority');
        if (!$priority || !$priority->getIdValue())
            $form->setAnswer('priority', null, $cfg->getDefaultPriorityId());
        $deptId = $deptId ?: $cfg->getDefaultDeptId();
        $statusId = $statusId ?: $cfg->getDefaultTicketStatusId();
        $topicId = isset($topic) ? $topic->getId() : 0;
        $ipaddress = $vars['ip'] ?: $_SERVER['REMOTE_ADDR'];
        $source = $source ?: 'Web';

        $string_data = '';
        $booking_code = '';
        $fucking_vars = [];
        // get some field from dynamic form
        if (isset($topic_form) && $topic_form) {
            $topic_form_form = $topic_form->getForm();
            if ($topic_form_form && is_callable([$topic_form_form, 'getDynamicFields'])) {
                foreach ($topic_form->getAnswers() as $answer) {
                    $field = $answer->getField();
                    $field->setForm($topic_form);
                    $val = $field->to_database($field->getClean());
                    if ($field->get('name') && $answer->get('field_id') == $field->get('id')) {
                        $fucking_vars[ $field->get('name') ] = $val;

                        if (strpos($field->get('name'), 'ol_status') !== false
                            || strpos($field->get('name'), 'time_accept') !== false)
                            continue;

                        if (strpos($field->get('name'), 'time') !== false && is_numeric(_String::json_decode($val)))
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), Format::userdate('d/m/Y H:i', _String::json_decode($val)));
                        else
                            $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), _String::json_decode($val));
                    }
                }
            }
        }

        $string_data .= sprintf('<p>Số ngày nghỉ: %s</p>', Offlate::calc($fucking_vars['time_offer'], $fucking_vars['time_end']));

        $vars['message'] .= '<hr><br>'.$string_data;

        //We are ready son...hold on to the rails.
        $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        // sorry, but i must modify this fucking mother function :v hahaha - buupham
        $sql='INSERT INTO '.TICKET_TABLE.' SET '
            .' created= NOW() '
            .' ,lastmessage= NOW()'
            .' ,`number`='.db_input($number)
            .' ,dept_id='.db_input($deptId)
            .' ,topic_id='.db_input($topicId)
            .' ,ip_address='.db_input($ipaddress)
            .' ,source='.db_input($source);

        if (isset($vars['emailId']) && $vars['emailId'])
            $sql.=', email_id='.db_input($vars['emailId']);

        //Make sure the origin is staff - avoid firebug hack!
        if($vars['duedate'] && !strcasecmp($origin,'staff'))
            $sql.=' ,duedate='.db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time'])));


        if(!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id)))
            return null;

        /* ------------- ADD COLLABORATOR booking@tugo.com.vn -------MINH BAO */

        $info = array('ticketId' => $id,
            'userId' => 8,
            'isactive' => 1);//ID=8; booking@tugo.com.vn

        if (!($c=Collaborator::add($info,$errors))){

        }

        /* ---- END ADD COLLABORATOR ---->

        /* -------------------- POST CREATE ------------------------ */

        // Save the (common) dynamic form
        $form->setTicketId($id);
        $form->save();

        // Save the form data from the help-topic form, if any
        if ($topic_form) {
            $topic_form->setTicketId($id);
            $topic_form->save();
        }

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
        $id = $ticket->getId();
        Staff::push($id);
        $tmp_var = $vars;
        $tmp_var['message'] = 'Xin nghỉ phép';
        $message = $ticket->postMessage($tmp_var , $origin, false);

        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['message']);
        }
        else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staffId'])
                $ticket->assignToStaff($vars['staffId'], $vars['message']);
            if ($vars['teamId'])
                // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], _S('Auto Assignment'),
                    !$vars['staffId']);
        }

        // Apply requested status — this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId)
            $ticket->setStatus($statusId, false, false);

        /**********   double check auto-response  ************/
        //Override auto responder if the FROM email is one of the internal emails...loop control.
        if($autorespond && (Email::getIdByEmail($ticket->getEmail())))
            $autorespond=false;

        # Messages that are clearly auto-responses from email systems should
        # not have a return 'ping' message
        if (isset($vars['flags']) && $vars['flags']['bounce'])
            $autorespond = false;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        //post canned auto-response IF any (disables new ticket auto-response).
        if ($vars['cannedResponseId']
            && $ticket->postCannedReply($vars['cannedResponseId'], $message->getId(), $autorespond)) {
            $ticket->markUnAnswered(); //Leave the ticket as unanswred.
            $autorespond = false;
        }

        //Check department's auto response settings
        // XXX: Dept. setting doesn't affect canned responses.
        if($autorespond && $dept && !$dept->autoRespONNewTicket())
            $autorespond=false;

        //Don't send alerts to staff when the message is a bounce
        //  this is necessary to avoid possible loop (especially on new ticket)
        if ($alertstaff && $message->isBounce())
            $alertstaff = false;

        /***** See if we need to send some alerts ****/
        $ticket->onNewTicket($message, $autorespond, $alertstaff);

        /************ check if the user JUST reached the max. open tickets limit **********/
        if($cfg->getMaxOpenTickets()>0
            && ($user=$ticket->getOwner())
            && ($user->getNumOpenTickets()==$cfg->getMaxOpenTickets())) {
            $ticket->onOpenLimit(($autorespond && strcasecmp($origin, 'staff')));
        }

        /* Start tracking ticket lifecycle events */
        $ticket->logEvent('created');

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('model.created', $ticket);

        /* Phew! ... time for tea (KETEPA) */

        return $ticket;
    }
    static function createBooking($vars, &$errors, $origin, $autorespond=true,
                                  $alertstaff=true) {
        global $ost, $cfg, $thisstaff, $_FILES;
        db_query('SET autocommit = 0');
        db_query('start transaction');

        // Don't enforce form validation for email
        $field_filter = function($type) use ($origin) {
            return function($f) use ($origin, $type) {
                // Ultimately, only offer validation errors for web for
                // non-internal fields. For email, no validation can be
                // performed. For other origins, validate as usual
                switch (strtolower($origin)) {
                    case 'email':
                        return false;
                    case 'staff':
                        // Required 'Contact Information' fields aren't required
                        // when staff open tickets
                        return $type != 'user'
                            || in_array($f->get('name'), array('name','email'));
                    case 'web':
                        return !$f->get('private');
                    default:
                        return true;
                }
            };
        };

        $reject_ticket = function($message) use (&$errors) {
            global $ost;
            $errors = array(
                'errno' => 403,
                'err' => __('This help desk is for use by authorized guests only'));
            $ost->logWarning(_S('Ticket Denied'), $message, false);
            return 0;
        };

        Signal::send('ticket.create.before', null, $vars);

        // Create and verify the dynamic form entry for the new ticket
        $form = TicketForm::getNewInstance();
        $form->setSource($vars);

        // If submitting via email or api, ensure we have a subject and such
        if (!in_array(strtolower($origin), array('web', 'staff'))) {
            foreach ($form->getFields() as $field) {
                $fname = $field->get('name');
                if ($fname && isset($vars[$fname]) && !$field->value)
                    $field->value = $field->parse($vars[$fname]);
            }
        }

        if (!$form->isValid($field_filter('ticket')))
            $errors = array_merge($errors, $form->errors());

        if ($vars['uid'])
            $user = User::lookup($vars['uid']);

        $id=0;
        $fields=array();
        $fields['message']  = array('type'=>'*',     'required'=>1, 'error'=>__('Message content is required'));

        if (!$errors) {

            # Perform ticket filter actions on the new ticket arguments
            $__form = null;
            if ($vars['topicId']) {
                if (($__topic=Topic::lookup($vars['topicId']))
                    && ($__form = $__topic->getForm())
                ) {
                    $__form = $__form->instanciate();
                    $__form->setSource($vars);
                }
            }

            try {
                $vars = self::filterTicketData($origin, $vars,
                                               array($form, $__form), $user);
            }
            catch (RejectedException $ex) {
                return $reject_ticket(
                    sprintf(_S('Ticket rejected (%s) by filter "%s"'),
                            $ex->vars['email'], $ex->getRejectingFilter()->getName())
                );
            }
        }

        if ($vars['topicId']) {
            if ($topic=Topic::lookup($vars['topicId'])) {
                if ($topic_form = $topic->getForm()) {
                    $TF = $topic_form->getForm($vars);
                    $topic_form = $topic_form->instanciate();
                    $topic_form->setSource($vars);
                    if (!$TF->isValid($field_filter('topic')))
                        $errors = array_merge(is_array($errors) ? $errors : [], $TF->errors());
                }
            }
            else  {
                $errors['topicId'] = 'Invalid help topic selected';
            }
        }

        // Any error above is fatal.
        if ($errors)
            return 0;

        Signal::send('ticket.create.validated', null, $vars);


        // If the filter specifies a help topic which has a form associated,
        // and there was previously either no help topic set or the help
        // topic did not have a form, there's no need to add it now as (1)
        // validation is closed, (2) there may be a form already associated
        // and filled out from the original  help topic, and (3) staff
        // members can always add more forms now

        // OK...just do it.
        $statusId = $vars['statusId'];
        $deptId = $vars['deptId']; //pre-selected Dept if any.
        $source = ucfirst($vars['source']);

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $deptId = $deptId ?: $topic->getDeptId();
            $statusId = $statusId ?: $topic->getStatusId();
            $priority = $form->getAnswer('priority');
            if (!$priority || !$priority->getIdValue())
                $form->setAnswer('priority', null, $topic->getPriorityId());
            if ($autorespond)
                $autorespond = $topic->autoRespond();

            //Auto assignment.
            elseif (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ?: $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Last minute checks
        $priority = $form->getAnswer('priority');
        if (!$priority || !$priority->getIdValue())
            $form->setAnswer('priority', null, $cfg->getDefaultPriorityId());
        $deptId = $deptId ?: $cfg->getDefaultDeptId();
        $statusId = $statusId ?: $cfg->getDefaultTicketStatusId();
        $topicId = isset($topic) ? $topic->getId() : 0;
        $ipaddress = $vars['ip'] ?: $_SERVER['REMOTE_ADDR'];
        $source = $source ?: 'Web';

        $string_data = '';
        $booking_code = '';
        $fucking_vars = [];
        $agent = null;
        // get some field from dynamic form
        if (isset($topic_form) && $topic_form) {
            $topic_form_form = $topic_form->getForm();
            if ($topic_form_form && is_callable([$topic_form_form, 'getDynamicFields'])) {
                foreach ($topic_form->getAnswers() as $answer) {
                    $field = $answer->getField();
                    $field->setForm($topic_form);
                    $val = $field->to_database($field->getClean());
                    if ($field->get('name') && $answer->get('field_id') == $field->get('id')) {
                        $fucking_vars[ $field->get('name') ] = $val;
                        $_text = _String::json_decode($val);
                        if (_Date::isInYearTimeStamp($_text))
                            $_text = Format::userdate('d M Y', $_text);
                        $string_data .= sprintf('<p>%s: %s</p>', $field->getLabel(), $_text);
                    }

                    if ('booking_code' == $field->get('name')) {
                        $booking_code = $val;
                        $tmp_val = explode('-', $val);
                        $tmp_val = isset($tmp_val[1]) ? $tmp_val[1] : null;
                        UUID::set('booking_code', $tmp_val);
                    }

                    if ('staff' == $field->get('name')) {
                        $agent = $val;
                    }
                }
            }
        }

        $booking_check = Booking::lookup(['booking_code' => trim($booking_code)]);
        if ($booking_check) {
            $errors['booking_code'] = 'Mã Booking này đã tồn tại. 
            Hãy bấm vào [<a target="_blank" class="no-pjax" href="'.$cfg->getUrl().'scp/tickets.php?id='.$booking_check->ticket_id.'">link này</a>] 
            để kiểm tra. 
            Nếu booking cần tạo đã có, hãy đóng tab này. 
            Nếu không đúng booking của bạn, 
            hãy bấm [<a class="no-pjax" href="'.$cfg->getUrl().'scp/tickets.php?a=booking&new=1">New Booking</a>] 
            để tạo lại với mã booking mới.';

            db_query('ROLLBACK');
            db_query('SET autocommit = 1');
            return false;
        }

        $vars['subject'] = $booking_code;
        $vars['message'] = $string_data;

        if ($agent && isset($agent[1]))
            $vars['staffId'] = $agent[1];

        //We are ready son...hold on to the rails.
        $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        $created = isset($fucking_vars['time']) ? Format::userdate('Y-m-d H:i:s', $fucking_vars['time']) : $vars['created'];
        // sorry, but i must modify this fucking mother function :v hahaha - buupham
        $sql='INSERT INTO '.TICKET_TABLE.' SET '
            .' created='.(isset($created) && !empty($created) ? db_input($created) : ' NOW() ')
            .' ,lastmessage= NOW()'
            .' ,`number`='.db_input($number)
            .' ,dept_id='.db_input($deptId)
            .' ,topic_id='.db_input($topicId)
            .' ,ip_address='.db_input($ipaddress)
            .' ,source='.db_input($source);

        if (isset($vars['emailId']) && $vars['emailId'])
            $sql.=', email_id='.db_input($vars['emailId']);

        //Make sure the origin is staff - avoid firebug hack!
        if($vars['duedate'] && !strcasecmp($origin,'staff'))
            $sql.=' ,duedate='.db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time'])));


        if(!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id)))
            return null;

        /* -------------------- POST CREATE ------------------------ */

        // Save the (common) dynamic form
        $form->setTicketId($id);
        $form->save();

        // Save the form data from the help-topic form, if any
        if ($topic_form) {
            $topic_form->setTicketId($id);
            $topic_form->save();
        }

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        // update loyalty point
        $_index = 0;
        $customer_number_list = [];
        while (isset($_POST['group-loyalty'][$_index])) {
            try {
                $item = $_POST['group-loyalty'][$_index];

                if (!isset($item['customer-number']) || empty(trim($item['customer-number']))) {
                    throw new Exception('Mã số khách hàng không được để trống');
                }

                $check_user = \Tugo\User::get(['customer_number' => $item['customer-number']]);
                if (!$check_user)
                    throw new Exception('Khách hàng chưa đăng ký app');

                if (in_array($item['customer-number'], $customer_number_list))
                    throw new Exception('Mã số khách hàng bị trùng: '.$item['customer-number']);

                $customer_number_list[] = $item['customer-number'];

                try {
                    $phone_number = $check_user['phone_number'];
                    $ost_user = \User::lookupByPhone($phone_number);
                    if (!$ost_user) {
                        $ost_user = \User::checkAndCreate([
                            'phone' => $phone_number,
                            'name' => $check_user['customer_name'],
                        ]);
                    }

                    if ($ost_user) {
                        $ost_user->set('uuid', $check_user['uuid']);
                        $ost_user->save(true);
                    }
                } catch (Exception $ex) {

                }

                if (!isset($item['point']) || empty(trim($item['point'])) || 0 === intval(trim($item['point']))) {
                    throw new Exception('Điểm không được để trống, phải là số, và phải khác số 0');
                }

                if (!isset($item['type']) || empty(trim($item['type']))) {
                    throw new Excepton('Loại phải là tích điểm hoặc sử dụng điểm');
                }

                $type = trim($item['type']);
                $point_note = trim($item['note']);
                $point_subject = _String::json_decode($fucking_vars['booking_type'])
                    .' - '.$fucking_vars['booking_code']
                    .' - '.date('d M Y', $fucking_vars['dept_date']);
                $customer_number = trim($item['customer-number']);
                $change_point = abs(intval(trim($item['point'])));
                $change_point = ($type == '-' ? -1 : 1) * $change_point;
                $app_user = \Tugo\User::get(['customer_number' => $customer_number]);

                if (!$app_user) {
                    throw new Exception('Khách chưa đăng ký app');
                }

                $uuid = $app_user['uuid'];
                $point = UserPoint::create([
                    'updater_id' => $thisstaff->getId(),
                    'updater' => $thisstaff->getUserName(),
                    'subject' => $point_subject,
                    'note' => $point_note,
                    'change_point' => $change_point,
                    'user_uuid' => $uuid,
                    'booking_code' => trim($fucking_vars['booking_code']),
                ]);

                if (!$point) {
                    throw new Exception('Lỗi cập nhật điểm');
                }

                if (!$point->save(true)) {
                    throw new Exception('Lỗi cập nhật điểm');
                }

                if (!\Tugo\User::changePoint($uuid, $change_point)) {
                    throw new Exception('Lỗi cập nhật điểm');
                }

                $_index++;
            } catch (Exception $ex) {
                $errors['Exception_'.$ex->getCode()] = $ex->getMessage();
                return false;
            }
        }
        // END

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
        $message = $ticket->postMessage($vars , $origin, false);

        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['note']);
        }
        else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staffId'])
                $ticket->assignToStaff($vars['staffId'], _S('Auto Assignment'));
            if ($vars['teamId'])
                // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], _S('Auto Assignment'),
                                      !$vars['staffId']);
        }

        // Apply requested status — this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId)
            $ticket->setStatus($statusId, false, false);

        /**********   double check auto-response  ************/
        //Override auto responder if the FROM email is one of the internal emails...loop control.
        if($autorespond && (Email::getIdByEmail($ticket->getEmail())))
            $autorespond=false;

        # Messages that are clearly auto-responses from email systems should
        # not have a return 'ping' message
        if (isset($vars['flags']) && $vars['flags']['bounce'])
            $autorespond = false;
        if ($autorespond && $message->isAutoReply())
            $autorespond = false;

        //post canned auto-response IF any (disables new ticket auto-response).
        if ($vars['cannedResponseId']
            && $ticket->postCannedReply($vars['cannedResponseId'], $message->getId(), $autorespond)) {
            $ticket->markUnAnswered(); //Leave the ticket as unanswred.
            $autorespond = false;
        }

        //Check department's auto response settings
        // XXX: Dept. setting doesn't affect canned responses.
        if($autorespond && $dept && !$dept->autoRespONNewTicket())
            $autorespond=false;

        //Don't send alerts to staff when the message is a bounce
        //  this is necessary to avoid possible loop (especially on new ticket)
        if ($alertstaff && $message->isBounce())
            $alertstaff = false;

        /***** See if we need to send some alerts ****/
        $ticket->onNewTicket($message, $autorespond, $alertstaff);

        /************ check if the user JUST reached the max. open tickets limit **********/
        if($cfg->getMaxOpenTickets()>0
            && ($user=$ticket->getOwner())
            && ($user->getNumOpenTickets()==$cfg->getMaxOpenTickets())) {
            $ticket->onOpenLimit(($autorespond && strcasecmp($origin, 'staff')));
        }

        /* Start tracking ticket lifecycle events */
        $ticket->logEvent('created');

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('model.created', $ticket);

        /* Phew! ... time for tea (KETEPA) */

        $check = false;

        if ($ticket) { // push to google sheet
            Booking::push($ticket->getId());
            Signal::send('TriggerList.BOOKING_NEW', $ticket);

            if (defined("DEV_ENV") && DEV_ENV === 0) {
                $sheet = new GoogleSheet(GOOGLE_SHEET_BOOKING, CREDENTIALS_BOOKING_PATH);
                $sheet->getClient();
            }
            $retail_price = [];
            $net_price = [];
            $total_quantity = 0;
            $profit = 0;
            for ($i = 1; $i <=4; $i++) {
                if (!isset($fucking_vars['quantity'.$i]) || !$fucking_vars['quantity'.$i])
                    $fucking_vars['quantity'.$i] = 0;

                $total_quantity += $fucking_vars['quantity'.$i];

                if (isset($fucking_vars['retailprice'.$i]) && !empty($fucking_vars['retailprice'.$i]) && $fucking_vars['retailprice'.$i])
                    $retail_price[] = $fucking_vars['quantity'.$i].' x '.number_format($fucking_vars['retailprice'.$i]);
                if (isset($fucking_vars['netprice'.$i]) && !empty($fucking_vars['netprice'.$i]) && $fucking_vars['netprice'.$i])
                    $net_price[] = $fucking_vars['quantity'.$i].' x '.number_format($fucking_vars['netprice'.$i]);

                $profit += ($fucking_vars['quantity'.$i] * $fucking_vars['retailprice'.$i]) - ($fucking_vars['quantity'.$i] * $fucking_vars['netprice'.$i]);
            }
            $data = [
                [
                    $fucking_vars['booking_code'],
                    _String::json_decode($fucking_vars['status']) ?: '',
                    str_replace('<br />', "\r\n", $fucking_vars['customer']) ?: '',
                    str_replace('<br />', "\r\n", $fucking_vars['phone_number']) ?: '',
                    '' ?: '',
                    _String::json_decode($fucking_vars['booking_type']) ?: '',
                    $total_quantity,
                    date('m/d/Y', $fucking_vars['dept_date']) ?: '',
                    _String::json_decode($fucking_vars['partner']) ?: '',
                    '' ?: '',
                    '' ?: '',
                    _String::json_decode($fucking_vars['staff']) ?: '',
                    implode("\r\n", $retail_price),
                    implode("\r\n", $net_price),
                    number_format($profit),
                    '' ?: '',
                    date('m/d/Y') ?: '',
                    $fucking_vars['receipt_code'] ?: '',
                    $fucking_vars['referral_code'] ?: '',
                ]
            ];

            try {
                $range = 'BOOKING!A1:Z';
                $body = new Google_Service_Sheets_ValueRange(array('values' => $data ));
                $params = ['valueInputOption' => 'RAW'];

                if (defined("DEV_ENV") && DEV_ENV === 0) {
                    $result = $sheet->getService()->spreadsheets_values->append(GOOGLE_SHEET_BOOKING, $range, $body, $params);
                    $check = $result && (
                            (isset($result->updatedRows) && $result->updatedRows)
                            || (isset($result->updates) && isset($result->updates->updatedRange) && $result->updates->updatedRange)
                        );
                } else {
                    $check = true;
                }

                if ($check)
                    db_query('COMMIT');
            } catch(Exception $e) {
                $errors['google_sheet_ex'] = $e->getMessage();
            }
        }

        if (!$ticket || !$check) {
            db_query('ROLLBACK');
        }

        db_query('SET autocommit = 1');
        if ($check) {
            return $ticket;
        } else {
            $errors['google_sheet'] = 'Cannot update to Google Sheet. Try again!';
            return false;
        }
    }

    static function createBookingtmp($vars, &$errors,$autorespond=true, $alertstaff=true){
//        $errors['booking'] = 'Tao booking';
        global $ost, $cfg, $thisstaff, $_FILES;
        db_query('SET autocommit = 0');
        db_query('start transaction');

        Signal::send('ticket.create.before', null, $vars);

        // Any error above is fatal.
        if ($errors)
            return 0;

        Signal::send('ticket.create.validated', null, $vars);

                // OK...just do it.
        $statusId = $vars['statusId'];
        $source = ucfirst($vars['source']);

        if (!isset($topic)) {
            // This may return NULL, no big deal
            $topic = $cfg->getDefaultTopic();
        }

        // Intenal mapping magic...see if we need to override anything
        if (isset($topic)) {
            $statusId = $statusId ?: $topic->getStatusId();
//            $priority = $form->getAnswer('priority');
//            if (!$priority || !$priority->getIdValue())
//                $form->setAnswer('priority', null, $topic->getPriorityId());
//            if ($autorespond)
//                $autorespond = $topic->autoRespond();

            //Auto assignment.
            if(!isset($vars['priority']))
                $vars['priority'] = $cfg->getDefaultPriorityId();

            if (!isset($vars['teamId']) && $topic->getTeamId())
                $vars['teamId'] = $topic->getTeamId();

            //set default sla.
            if (isset($vars['slaId']))
                $vars['slaId'] = $vars['slaId'] ?: $cfg->getDefaultSLAId();
            elseif ($topic && $topic->getSLAId())
                $vars['slaId'] = $topic->getSLAId();
        }

        // Last minute checks
        $deptId = BOOKING_DEPARTMENT;
        $statusId = $statusId ?: $cfg->getDefaultTicketStatusId();
        $topicId = BOOKING_TOPIC;
        $ipaddress = $vars['ip'] ?: $_SERVER['REMOTE_ADDR'];
        $source = $source ?: 'Web';

        $string_data = '';
        $booking_code = $vars['booking_code'];

        $booking_check = Booking::lookup(['booking_code' => trim($booking_code)]);
        if ($booking_check) {
            $errors['booking_code'] = 'Mã Booking này đã tồn tại. 
            Hãy bấm vào [<a target="_blank" class="no-pjax" href="'.$cfg->getUrl().'scp/new_booking.php?id='.$booking_check->ticket_id.'">link này</a>] 
            để kiểm tra. 
            Nếu booking cần tạo đã có, hãy đóng tab này. 
            Nếu không đúng booking của bạn, 
            hãy bấm [<a class="no-pjax" href="'.$cfg->getUrl().'scp/new_booking.php?action=create">New Booking</a>] 
            để tạo lại với mã booking mới.';

            db_query('ROLLBACK');
            db_query('SET autocommit = 1');
            return false;
        }

        $vars['subject'] = $booking_code;
        $vars['message'] = $string_data;

//        if ($agent && isset($agent[1]))
//            $vars['staffId'] = $agent[1];

        //We are ready son...hold on to the rails.
        $number = $topic ? $topic->getNewTicketNumber() : $cfg->getNewTicketNumber();
        $created = isset($fucking_vars['time']) ? Format::userdate('Y-m-d H:i:s', $fucking_vars['time']) : $vars['created'];
        // sorry, but i must modify this fucking mother function :v hahaha - buupham
        $sql='INSERT INTO '.TICKET_TABLE.' SET '
            .' created='.(isset($created) && !empty($created) ? db_input($created) : ' NOW() ')
            .' ,lastmessage= NOW()'
            .' ,`number`='.db_input($number)
            .' ,dept_id='.db_input($deptId)
            .' ,topic_id='.db_input($topicId)
            .' ,ip_address='.db_input($ipaddress)
            .' ,`sla_id`=0 '
            .' ,isoverdue=0 '
            .' ,user_id=0 '
            .' ,source='.db_input($source);

        if (isset($vars['emailId']) && $vars['emailId'])
            $sql.=', email_id='.db_input($vars['emailId']);

        //Make sure the origin is staff - avoid firebug hack!
//        if($vars['duedate'] && !strcasecmp($origin,'staff'))
//            $sql.=' ,duedate='.db_input(date('Y-m-d G:i',Misc::dbtime($vars['duedate'].' '.$vars['time'])));


        if(!db_query($sql) || !($id=db_insert_id()) || !($ticket =Ticket::lookup($id)))
            return null;

        //create booking
        $vars['ticket_id'] = $ticket->getId();
        $vars['number'] = $ticket->getNumber();

        Booking::createBooking($vars);

        $ticket->loadDynamicData();

        $dept = $ticket->getDept();

        //post the message.
        $vars['title'] = $vars['subject']; //Use the initial subject as title of the post.
        $vars['userId'] = $ticket->getUserId();
//        $message = $ticket->postMessage($vars , $origin, false);

        // Configure service-level-agreement for this ticket
        $ticket->selectSLAId($vars['slaId']);

        // Assign ticket to staff or team (new ticket by staff)
        if($vars['assignId']) {
            $ticket->assign($vars['assignId'], $vars['note']);
        }
        else {
            // Auto assign staff or team - auto assignment based on filter
            // rules. Both team and staff can be assigned
            if ($vars['staff'])
                $ticket->assignToStaff($vars['staff'], _S('Auto Assignment'));
            if ($vars['teamId'])
                // No team alert if also assigned to an individual agent
                $ticket->assignToTeam($vars['teamId'], _S('Auto Assignment'),
                                      !$vars['staffId']);
        }

        // Apply requested status — this should be done AFTER assignment,
        // because if it is requested to be closed, it should not cause the
        // ticket to be reopened for assignment.
        if ($statusId)
            $ticket->setStatus($statusId, false, false);

        /* Start tracking ticket lifecycle events */
        $ticket->logEvent('created');

        // Fire post-create signal (for extra email sending, searching)
        Signal::send('model.created', $ticket);

        if (!$ticket  || !empty($errors)) {
            db_query('ROLLBACK');
            return 0;
        }

        db_query('SET autocommit = 1');

        return $ticket->getId();
    }

    /* routine used by staff to open a new ticket */
    static function open($vars, &$errors) {
        global $thisstaff, $cfg;

        if(!$thisstaff || !$thisstaff->canCreateTickets()) return false;

        if($vars['source'] && !in_array(strtolower($vars['source']),array('email','phone','other', 'facebook')))
            $errors['source']=sprintf(__('Invalid source given - %s'),Format::htmlchars($vars['source']));

        if (!$vars['uid']) {
            //Special validation required here
            if (!$vars['email'] || !Validator::is_email($vars['email']))
                $errors['email'] = __('Valid email address is required');

            if (!$vars['name'])
                $errors['name'] = __('Name is required');
        }

        if (!$thisstaff->canAssignTickets())
            unset($vars['assignId']);

        $create_vars = $vars;
        $tform = TicketForm::objects()->one()->getForm($create_vars);
        $create_vars['cannedattachments']
            = $tform->getField('message')->getWidget()->getAttachments()->getClean();

        if(!($ticket=Ticket::create($create_vars, $errors, 'staff', false)))
            return false;

        $vars['msgId']=$ticket->getLastMsgId();

        // post response - if any
        $response = null;
        if($vars['response'] && $thisstaff->canPostReply()) {

            $vars['response'] = $ticket->replaceVars($vars['response']);
            // $vars['cannedatachments'] contains the attachments placed on
            // the response form.
            $response = $ticket->postReply($vars, $errors, false);
        }

        // Not assigned...save optional note if any
        if (!$vars['assignId'] && $vars['note']) {
            if (!$cfg->isHtmlThreadEnabled()) {
                $vars['note'] = new TextThreadBody($vars['note']);
            }
            $note = $ticket->logNote(_S('New Ticket'), $vars['note'], $thisstaff, false);
        }
        else {
            // Not assignment and no internal note - log activity
            $note = $ticket->logActivity(_S('New Ticket by Agent'),
                sprintf(_S('Ticket created by agent - %s'), $thisstaff->getName()));
        }

        if ($note && $note->getId())
            $_SESSION['___THREAD_ID'] = $note->getId();

        $ticket->reload();

        if(!$cfg->notifyONNewStaffTicket()
                || !isset($vars['alertuser'])
                || !($dept=$ticket->getDept()))
            return $ticket; //No alerts.

        //Send Notice to user --- if requested AND enabled!!
        if(($tpl=$dept->getTemplate())
                && ($msg=$tpl->getNewTicketNoticeMsgTemplate())
                && ($email=$dept->getEmail())) {

            $message = (string) $ticket->getLastMessage();
            if($response) {
                $message .= ($cfg->isHtmlThreadEnabled()) ? "<br><br>" : "\n\n";
                $message .= $response->getBody();
            }

            if($vars['signature']=='mine')
                $signature=$thisstaff->getSignature();
            elseif($vars['signature']=='dept' && $dept && $dept->isPublic())
                $signature=$dept->getSignature();
            else
                $signature='';

            $attachments =($cfg->emailAttachments() && $response)?$response->getAttachments():array();

            $msg = $ticket->replaceVars($msg->asArray(),
                    array(
                        'message'   => $message,
                        'signature' => $signature,
                        'response'  => ($response) ? $response->getBody() : '',
                        'recipient' => $ticket->getOwner(), //End user
                        'staff'     => $thisstaff,
                        )
                    );

            $references = $ticket->getLastMessage()->getEmailMessageId();
            if (isset($response))
                $references = array($response->getEmailMessageId(), $references);
            $options = array(
                'references' => $references,
                'thread' => $ticket->getLastMessage()
            );
            $email->send($ticket->getOwner(), $msg['subj'], $msg['body'], $attachments,
                $options);
        }

        return $ticket;

    }

    static function io($vars, &$errors) {
        global $thisstaff, $cfg;

        if(!$thisstaff || !$thisstaff->canCreatePayments()) return false;

        $create_vars = $vars;
        if(!($ticket=Ticket::createIO($create_vars, $errors, 'staff', false)))
            return false;

        $ticket->reload();

        return $ticket;

    }
    static function ol($vars, &$errors) {
        global  $cfg;


        $create_vars = $vars;
        if(!($ticket=Ticket::createOL($create_vars, $errors, 'staff', false))) {
            return false;
        }

        $ticket->reload();

        return $ticket;
    }
    static function booking($vars, &$errors) {
        global $thisstaff, $cfg;

        if(!$thisstaff || !$thisstaff->canCreateBookings()) return false;

        $create_vars = $vars;
        if(!($ticket=Ticket::createBooking($create_vars, $errors, 'staff', false))) {
            return false;
        }

        $ticket->reload();

        return $ticket;
    }

    function checkOverdue() {
        // tìm config id của no overdue
        $sql = 'SELECT id FROM '.FORM_FIELD_TABLE.' field WHERE `type`=\'bool\' AND `name`=\'no_overdue\' LIMIT 1';
        $no_overdue_id = [];

        if(($res=db_query($sql))) {
            $field = db_fetch_row($res);
            // tìm các status set no overdue
            if (isset($field[0]) && is_numeric($field[0])) {
                $field = $field[0];
                $list = TicketStatusList::getStatuses();
                foreach($list as $l) {
                    $config = $l->getConfiguration();
                    if (isset($config[$field]) && $config[$field])
                        $no_overdue_id[] = $l->getId();
                }

                $no_overdue_id = array_unique($no_overdue_id);
                $no_overdue_id = array_filter($no_overdue_id);
            }
        }

        $sql='SELECT ticket_id FROM '.TICKET_TABLE.' T1 '
            .' INNER JOIN '.TICKET_STATUS_TABLE.' status
                ON (status.id=T1.status_id AND status.state="open")  '
            . ( $no_overdue_id ? (' AND status.id NOT IN ('.implode(',', $no_overdue_id).') ') : ' ' )
            .' LEFT JOIN '.SLA_TABLE.' T2 ON (T1.sla_id=T2.id AND T2.isactive=1) '
            .' WHERE isoverdue=0 '
            . ' AND T1.dept_id IN (' . implode(',', unserialize(SALE_DEPT)) . ') '
            .' AND ( (T1.updated is NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),T1.created))>=T2.grace_period*3600) '
            .' OR (T1.updated is NOT NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),T1.updated))>=T2.grace_period*3600) '
            .' OR (duedate is NOT NULL AND duedate < NOW()) '
            .' ) ORDER BY T1.created LIMIT 50'; //Age upto 50 tickets at a time?

        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id)=db_fetch_row($res)) {
                if(($ticket=Ticket::lookup($id)) && $ticket->markOverdue())
                    $ticket->logActivity(_S('Ticket Marked Overdue'),
                        _S('Ticket flagged as overdue by the system.'));
            }
        } else {
            //TODO: Trigger escalation on already overdue tickets - make sure last overdue event > grace_period.

        }
   }

   function merge($ticket_2) {
       global $thisstaff;

       $id = $this->getId();
       $number = $this->getNumber();
       $user_id = $this->getUserId();
       $number_2 = $ticket_2->getNumber();
       $id_2 = $ticket_2->getId();

       $sql = sprintf(
           "UPDATE %s SET ticket_id = %d, user_id = %d WHERE ticket_id = %d",
           TICKET_THREAD_TABLE,
           db_input($id, false),
           db_input($user_id, false),
           db_input($id_2, false)
       );

       $sql_2 = sprintf(
           "UPDATE %s SET ticket_id = %d WHERE ticket_id = %d",
           TICKET_ATTACHMENT_TABLE,
           db_input($id, false),
           db_input($id_2, false)
       );

       db_query($sql_2);

       $error = [];

       return db_query($sql)
           && $ticket_2->setStatus(7, 'Merged into ' . $number)
           && $this->postNote(['note' => 'Merged with ' . $number_2], $error, $thisstaff);
   }

   function getSMSSendToday() {
       $sql = sprintf(
           "SELECT COUNT(DISTINCT id) FROM %s 
              WHERE ticket_id = %d 
              AND DATE(created) = DATE(NOW())
              AND thread_type LIKE 'S'
            ",
           TICKET_THREAD_TABLE,
           $this->getId()
       );

       return db_count($sql);
   }

    function getCalendar() {

        if(!$this->calendar)
            $this->calendar = Calendar::lookup($this);

        return $this->calendar;
    }

   function getCalendarEntries() {
       $c = $this->getCalendar();
       if (!$c)
           return [];
       return $c->getEntries();
   }

   function getAllCalendarEntries($staff_id) {
       return Calendar::getAllEntries($staff_id);
   }

   function pay($data, &$errors) {
        $amount = $data['amount'];
        $staff = $data['staff'];
        $date = $data['date'];
        $pay_method = $data['pay_method'];
        $note = $data['note'];
        $thread_id = isset($data['thread_id']) && $data['thread_id'] ? intval($data['thread_id']) : 0;

        $sql = sprintf(
            'INSERT INTO %s SET
                ticket_id = %s,
                user_id = %s,
                staff_id = %s,
                pay_amount = %s,
                pay_method = %s,
                pay_time = NOW(),
                note = %s,
                thread_id = %s
            ',
            TICKET_PAYMENT_HISTORY_TABLE,
            db_input($this->getId(), false),
            db_input($this->getUserId(), false),
            db_input($staff->getId(), false),
            db_input($amount),
            db_input($pay_method),
            db_input($note),
            db_input($thread_id)
        );

        $res = db_query($sql);
        $id = db_insert_id();
        return ($res && $id) ? $id : false;
   }

   function get_payment() {
        $sql = sprintf(
            'SELECT SUM(pay_amount) FROM %s
                WHERE ticket_id = %s
            ',
            TICKET_PAYMENT_HISTORY_TABLE,
            db_input($this->getId(), false)
        );

       $res = db_result(db_query($sql));
       if ($res) return $res;
       return 0;
   }

   function markAsUnRead() {
       if (!($id = $this->getId())) return false;
       $sql = "UPDATE ".TICKET_TABLE." SET `read`=0 WHERE ticket_id=".intval($id);
       return db_query($sql);
   }

   function markAsRead() {
       if (!($id = $this->getId())) return false;
       $sql = "UPDATE ".TICKET_TABLE." SET `read`=1 WHERE ticket_id=".intval($id);
       return db_query($sql);
   }

   public static function getUpcomingOverdue($staff_id, $hours = null) {
       $time = ($hours ?: 6)*3600; // seconds
       $open = TicketStatusList::getIds(true);

       $sql='SELECT ticket_id, `number`,
            if(T1.updated IS NULL AND duedate IS NULL AND
                  TIME_TO_SEC(TIMEDIFF(NOW(), T1.created)) >=
                  (T2.grace_period * 3600 - '.intval($time).'),
                  
                 (T2.grace_period * 3600 - TIME_TO_SEC(TIMEDIFF(NOW(), T1.created))),
                if (T1.updated IS NOT NULL AND duedate IS NULL AND
                    TIME_TO_SEC(TIMEDIFF(NOW(), T1.updated)) >=
                    (T2.grace_period * 3600 - '.intval($time).'),
            
                    (T2.grace_period * 3600 - TIME_TO_SEC(TIMEDIFF(NOW(), T1.updated))),
                  IF (duedate IS NOT NULL AND
                      duedate < (NOW() + '.intval($time).'),
                      (duedate - NOW()),
                    0
                    )
                 )
              ) as time
            FROM '.TICKET_TABLE.' T1 '
           . ( $no_overdue_id ? (' AND status.id NOT IN ('.implode(',', $no_overdue_id).') ') : ' ' )
           .' LEFT JOIN '.SLA_TABLE.' T2 ON (T1.sla_id=T2.id AND T2.isactive=1) '
           .' WHERE isoverdue=0 '
           . ' AND T1.status_id IN ('.( $open ? implode(',', $open) : ' SELECT 1=0 ' ).') '
           .' AND T1.dept_id IN('.implode(',', unserialize(SALE_DEPT)).') '//sales dept
           .' AND T1.staff_id = '.db_input($staff_id)
           .' AND ( (T1.updated is NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),T1.created))>=(T2.grace_period*3600 - '.intval($time).')) '
           .' OR (T1.updated is NOT NULL AND duedate is NULL AND TIME_TO_SEC(TIMEDIFF(NOW(),T1.updated))>=(T2.grace_period*3600 - '.intval($time).')) '
           .' OR (duedate is NOT NULL AND duedate < (NOW()+'.intval($time).')) '
           .' ) ORDER BY `time` ASC LIMIT 20';

       $res = db_query($sql);
       if (!$res) return null;
       return $res;
   }

   public static function getRemindOverdue($staff_id, $days = 7 ) {
       $time = ($days ?: 6)*3600*24; // seconds
       $open = TicketStatusList::getIds(true);

       $sql='SELECT ticket_id, `number` FROM '.TICKET_TABLE.' T1 '
           . ( $no_overdue_id ? (' AND status.id NOT IN ('.implode(',', $no_overdue_id).') ') : ' ' )
           .' LEFT JOIN '.SLA_TABLE.' T2 ON (T1.sla_id=T2.id AND T2.isactive=1) '
           .' WHERE isoverdue=1 '
           . ' AND T1.status_id IN ('.( $open ? implode(',', $open) : ' SELECT 1=0 ' ).') '
           .' AND T1.dept_id IN('.implode(',', unserialize(SALE_DEPT)).') '//sales dept
           .' AND T1.staff_id = '.db_input($staff_id)
           .' AND ( (T1.updated is NULL AND duedate is NULL AND (TIME_TO_SEC(TIMEDIFF(NOW(),T1.created)) - '.intval($time).') < T2.grace_period*3600) '
           .' OR (T1.updated is NOT NULL AND duedate is NULL AND (TIME_TO_SEC(TIMEDIFF(NOW(),T1.updated)) - '.intval($time).') < T2.grace_period*3600) '
           .' OR (duedate is NOT NULL AND (duedate+'.intval($time).') >= NOW()) '
           .' ) ORDER BY T1.created LIMIT 20';

       $res = db_query($sql);
       if (!$res) return null;
       return $res;
   }

    public static function fromOfflateTicketId($ticket_id) {
        $ticket_id = intval($ticket_id);
        $ticket_id = db_input($ticket_id);
        $table = static::TABLE_NAME;
        $sql = "SELECT `number` FROM $table WHERE ticket_id = $ticket_id LIMIT 1";
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['number']) || empty($row['number'])) return null;
        return $row['number'];
    }

    public static function countOpenByPhoneNumber($phone_number, $id = null) {
        $sql = "select count(*) as total from ost_ticket t join ost_user u on u.id=t.user_id
            join ost_form_entry e on u.id=e.object_id and object_type like 'U'
            join ost_form_entry_values v on e.id=v.entry_id and v.field_id=".USER_PHONE_NUMBER_FIELD."
            join ost_ticket_status s on s.id=t.status_id
            where v.value like '$phone_number'
            AND t.topic_id IN (".implode(',', unserialize(SALE_TOPIC) ).")
            and s.state like 'open' ";

        if ($id && is_numeric($id))
            $sql .= " AND t.ticket_id <> $id";

        $res = db_query($sql);
        if (!$res) return 0;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['total'])) return 0;
        return intval($row['total']);
    }

    public static function countSuccessByPhoneNumber($phone_number, $add_time, $id = null) {
        $sql = "select count(*) as total from ost_ticket t join ost_user u on u.id=t.user_id
            join ost_form_entry e on u.id=e.object_id and object_type like 'U'
            join ost_form_entry_values v on e.id=v.entry_id and v.field_id=".USER_PHONE_NUMBER_FIELD."
            where v.value like '$phone_number'
            AND date(t.closed) >= date(".db_input($add_time).")
            AND t.topic_id IN (".implode(',', unserialize(SALE_TOPIC) ).")
            and t.status_id=".db_input(TICKET_STATUS_SUCCESS);

        if ($id && is_numeric($id))
            $sql .= " AND t.ticket_id <> $id";

        $res = db_query($sql);
        if (!$res) return 0;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['total'])) return 0;
        return intval($row['total']);
    }

}
?>

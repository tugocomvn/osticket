ALTER TABLE `ost_groups` ADD `can_create_report` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `group_name`;

CREATE TABLE `osticket`.`report` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `user_id` INT(11) NOT NULL , `date_of_report` DATE NOT NULL , `date_create` DATETIME NOT NULL , `note` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `report_booking` CHANGE `booking_code` `booking_code` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

CREATE TABLE `osticket`.`report_booking` ( `booking_id` INT(11) NOT NULL AUTO_INCREMENT , `booking_code` MEDIUMTEXT NOT NULL , `total_retail_price` DOUBLE NOT NULL , `total_passenger` INT(11) NOT NULL , `user_id` INT(11) NOT NULL , `report_id` INT(11) NOT NULL , PRIMARY KEY (`booking_id`)) ENGINE = InnoDB;
ALTER TABLE `report` CHANGE `note` `note` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `report` ADD `staff_id` INT(11) NOT NULL DEFAULT '0' AFTER `user_id`;

ALTER TABLE `report` CHANGE `date_of_report` `month_of_report` INT(4) NOT NULL;
ALTER TABLE `report` ADD `year_of_report` INT(4) NOT NULL AFTER `month_of_report`;

ALTER TABLE `report` CHANGE `date_create` `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `report` CHANGE `user_id` `staff_create_id` INT(11) NOT NULL;

ALTER TABLE `report_booking` ADD `note` VARCHAR(100) NOT NULL AFTER `report_id`;

ALTER TABLE `report_booking` CHANGE `note` `note` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;






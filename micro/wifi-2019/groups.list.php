<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once  __DIR__.'/functions.php';
require_once  __DIR__.'/Pagination.php';
session_start();


$page = isset($_REQUEST['page'])?intval($_REQUEST['page']):1;
$offset = ($page - 1) * PAGE_LIMIT;

if(isset($_REQUEST['search']) && $_REQUEST['search'] == 'search' && !empty($_REQUEST['code']))
{
    $groups = Awing::selectDataGroup($offset,trim($_REQUEST['code']));
    $total = Awing::getTotalGroup(trim($_REQUEST['code']));
}
else {
    $groups = Awing::selectDataGroup($offset);
    $total = Awing::getTotalGroup();
}

$config = [
    'code'        => "",
    'total'       => $total,
    'limit'       => PAGE_LIMIT,
    'full'        => false,
    'querystring' => 'page'
];
require_once (__DIR__."/groups.list.inc.php");

<?php
class Tracking {
    public static function update($data) {
        $table_name = TRACKING_TABLE;
        $sql = "INSERT INTO {$table_name} SET 
            `uuid` = '{$data["uuid"]}',
            `username` = '{$data["username"]}',
            `user_id` = '{$data["user_id"]}',
            `is_staff` = '{$data["is_staff"]}',
            `useragent` = '{$data["useragent"]}',
            `ip_address` = '{$data["ip_address"]}',
            `add_time` = '{$data["add_time"]}'
            ON DUPLICATE KEY UPDATE `last_login_time` = '{$data["add_time"]}'
        ";
        db_query($sql);
    }
}

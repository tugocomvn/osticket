<?php
if(!$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');
if(!@$thisstaff->isStaff() || !$ticket->checkStaffAccess($thisstaff) || !$thisstaff->canViewBookings()) die('Access Denied');

$dept  = $ticket->getDept();  //Dept
$staff = $ticket->getStaff(); //Assigned or closed by..
$user  = $ticket->getOwner(); //Ticket User (EndUser)
$team  = $ticket->getTeam();  //Assigned team.
$sla   = $ticket->getSLA();
$lock  = $ticket->getLock();  //Ticket lock obj
$_id    = $ticket->getId();    //Ticket ID.

$booking = Booking::get_booking_view($_id);
$booking_code = Booking::fromTicketId($ticket->getId());
$points = UserPoint::fromBookingCode($booking_code);
$booking_action = BookingAction::lookup($_id);
?>
<h3>Booking - <small>Send confirm email</small></h3>

<?php if($booking_action): ?>
    <p><em>Đã gửi email booking <i class="icon-ok"></i></em></p>
    <p><?php echo $booking_code ?></p>
    <p class="centered"><button type="button" class="close btn_sm btn-default">Close</button></p>
    <?php exit; ?>
<?php endif; ?>

<table class="ticket_info" cellspacing="0" cellpadding="0" width="100%" border="0">
    <?php
    $idx = 0;
    foreach (DynamicFormEntry::forTicket($ticket->getId()) as $form) {
        // Skip core fields shown earlier in the ticket view
        // TODO: Rewrite getAnswers() so that one could write
        //       ->getAnswers()->filter(not(array('field__name__in'=>
        //           array('email', ...))));
        $answers = array_filter($form->getAnswers(), function ($a) {
            return !in_array($a->getField()->get('name'),
                             array('email','subject','name','priority'));
        });
        if (count($answers) == 0)
            continue;
        ?>
        <tr>
            <td colspan="2">
                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                    <?php foreach($answers as $a) {
                        if (!($v = $a->display())) continue; ?>
                        <tr>
                            <th width="100"><?php
                                echo $a->getField()->get('label');
                                ?>:</th>
                            <td><?php
                                echo $v;
                                ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
        <?php
        $idx++;
    } ?>

    <tr>
        <table cellspacing="5" cellpadding="4" width="100%" border="0">
            <?php if (isset($points) && $points): ?>
                <?php foreach ($points as $point): ?>
                    <?php
                    $customer_numner = '';
                    $loyalty_user = \Tugo\User::get(['uuid' => $point->user_uuid]);
                    if ($loyalty_user)
                        $customer_numner = $loyalty_user['customer_number'];
                    ?>
                    <tr data-repeater-item>
                        <td><strong>Mã số khách hàng:</strong> <?php echo $customer_numner ?></td>
                        <td><strong>Loại:</strong>
                            <?php if ($point->change_point > 0) echo "Tích điểm"; ?>
                            <?php if ($point->change_point < 0) echo "Sử dụng điểm"; ?>
                        </td>
                        <td><strong>Số điểm:</strong> <?php echo $point->change_point ?></td>
                        <td><strong>Nội dung:</strong> <?php echo $point->subject ?></td>
                        <td><strong>Ghi Chú:</strong> <?php echo $point->note ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
    </tr>

    <tr>
        <table cellspacing="5" cellpadding="4" width="100%" border="0">
            <tr>
                <td><strong>Tổng giá bán: </strong> <?php echo number_format($booking['total_retail_price'], 0).'đ' ?></td>
                <td><strong>Tổng giá net: </strong> <?php echo number_format($booking['total_net_price'], 0).'đ' ?></td>
                <td><strong>Tổng lợi nhuận: </strong> <?php echo number_format($booking['total_profit'], 0).'đ' ?></td>
            </tr>
        </table>
    </tr>
</table>
<form action="#bookings/<?php echo $booking_id ?>/finish" method="post" id="form" onsubmit="return false;">
    <?php csrf_token(); ?>
    <input type="hidden" name="booking_ticket_id" value="<?php echo $booking_id ?>">

    <p>
        Gõ chữ "OK" (viết hoa) và ô sau để xác nhận: <input type="text" class="input-field" name="confirm" id="confirm" value="" placeholder='type "OK"'>
    </p>
    <p class="centered"><button class="btn_sm btn-primary" id="send_btn" disabled style="display: none">Send email</button> <button type="button" class="close btn_sm btn-default">Cancel</button></p>
</form>

<script>
    $(document).ready(function() {
        $('#confirm').off('change')
            .on('change, keyup', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }

                if('OK' !== $('#confirm').val()) {
                    $('#form').on('submit', function(e) {
                        e.preventDefault();
                        return false;
                    });
                    $('#send_btn').prop('disabled', true).hide();
                } else {
                    $('#form').off('submit').on('submit', function() {
                        $('#send_btn').prop('disabled', true).hide();
                    });
                    $('#send_btn').prop('disabled', false).show()
                }
            })
    });
</script>
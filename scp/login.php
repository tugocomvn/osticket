<?php
/*********************************************************************
    login.php

    Handles staff authentication/logins

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require_once('../main.inc.php');
if(!defined('INCLUDE_DIR')) die('Fatal Error. Kwaheri!');

// Bootstrap gettext translations. Since no one is yet logged in, use the
// system or browser default
TextDomain::configureForUser();

require_once(INCLUDE_DIR.'class.staff.php');
require_once(INCLUDE_DIR.'class.csrf.php');
require_once(INCLUDE_DIR.'class.tracking.php');

$content = Page::lookup(Page::getIdByType('banner-staff'));

$dest = $_SESSION['_staff']['auth']['dest'];
$msg = $_SESSION['_staff']['auth']['msg'];
$finance = isset($_SESSION['_staff']['auth']['finance']) ? $_SESSION['_staff']['auth']['finance'] : false;
$msg = $msg ?: ($content ? $content->getName() : __('Vui lòng điền tên đăng nhập (không có '.EMAIL_DOMAIN.') và mật khẩu'));
$dest=($dest && (!strstr($dest,'login.php') && !strstr($dest,'ajax.php')))?$dest:'index.php';
$show_reset = false;

unset($_SESSION['check_session_time']);
unset($_SESSION['check_session']);

function unparse_url($parsed_url) {
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
    return "$scheme$user$pass$host$port$path$query$fragment";
}

function removeParamUrl($url, $params_key = []){
    $arr_url = parse_url($url); //parse to array

    if($arr_url['query']) {
        parse_str($arr_url['query'], $url_query);

        foreach ($url_query as $key => $value) {
            if (in_array($key, $params_key))
                unset($url_query[$key]);
        }

        if(!isset($url_query) || empty($url_query))
            unset($arr_url['query']);
        else
            $arr_url['query'] = http_build_query($url_query);

        return unparse_url($arr_url);
    }
    return $url;
}
if($_POST) {
    // Check the CSRF token, and ensure that future requests will have to
    // use a different CSRF token. This will help ward off both parallel and
    // serial brute force attacks, because new tokens will have to be
    // requested for each attempt.
    if (!$ost->checkCSRFToken())
        Http::response(400, __('Valid CSRF Token Required'));

    // Rotate the CSRF token (original cannot be reused)
    $ost->getCSRF()->rotate();

    // Lookup support backends for this staff
    $username = trim($_POST['userid']);
    if ($user = StaffAuthenticationBackend::process($username,
            $_POST['passwd'], $errors)) {

        if ($finance) {
            $_SESSION['check_session'] = true;
            $_SESSION['check_session_time'] = time();
        }
        $_SESSION['check_session_request'] = 0;
        unset($_SESSION['check_session_request']);
        $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : getenv('REMOTE_ADDR');
        $cookie = _UUID::getCookie();
        if ($cookie) {
            Tracking::update([
                'uuid' => _UUID::getCookie(),
                'username' => $username,
                'user_id' => $user->getId(),
                'is_staff' => 1,
                'useragent' => $_SERVER['HTTP_USER_AGENT'],
                'ip_address' => $ip,
                'add_time' => date('Y-m-d H:i:s'),
            ]);
        }

        session_write_close();
        Http::redirect($dest);
        require_once('index.php'); //Just incase header is messed up.
        exit;
    }

    $msg = $errors['err']?$errors['err']:__('Vui lòng điền tên đăng nhập (không có '.EMAIL_DOMAIN.') và mật khẩu');
    $show_reset = true;
}
elseif(isset($_REQUEST['token']) && $_REQUEST['token']) {
    $token = trim($_REQUEST['token']);
    $username = StaffSigninToken::checkStaff(trim($_REQUEST['token']));
    if ($user = StaffAuthenticationBackend::process($username, null, $errors, $token)) {
        if ($finance) {
            $_SESSION['check_session'] = true;
            $_SESSION['check_session_time'] = time();
        }
        $_SESSION['check_session_request'] = 0;
        unset($_SESSION['check_session_request']);
        $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : getenv('REMOTE_ADDR');
        $cookie = _UUID::getCookie();
        if ($cookie) {
            Tracking::update([
                'uuid'       => _UUID::getCookie(),
                'username'   => $username,
                'user_id'    => $user->getId(),
                'is_staff'   => 1,
                'useragent'  => $_SERVER['HTTP_USER_AGENT'],
                'ip_address' => $ip,
                'add_time'   => date('Y-m-d H:i:s'),
            ]);
        }
        session_write_close();

        $dest = removeParamUrl($dest, ['token']);

        Http::redirect($dest);
        require_once('index.php'); //Just incase header is messed up.
        exit;
    }
}
elseif ($_GET['do']) {
    switch ($_GET['do']) {
    case 'ext':
        // Lookup external backend
        if ($bk = StaffAuthenticationBackend::getBackend($_GET['bk']))
            $bk->triggerAuth();
    }
    Http::redirect('login.php');
}
// Consider single sign-on authentication backends
elseif (!$thisstaff || !($thisstaff->getId() || $thisstaff->isValid())) {
    if (($user = StaffAuthenticationBackend::processSignOn($errors, false))
            && ($user instanceof StaffSession))
       @header("Location: $dest");
}

define("OSTSCPINC",TRUE); //Make includes happy!
include_once(INCLUDE_DIR.'staff/login.tpl.php');
?>

<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Active voucher</title>
</head>
<body>
    <div class="container bg-light rounded" style="width: 80%;">
        <?php require_once __DIR__.'/header.php'; ?>
        <div class="form-group">
            <form action="back_activeVoucher.php" method="post">
                <h3 class="my-3 text-center text-info">Kích hoạt voucher</h3>
                <div class="row my-3">
                    <h5 class="col-3">Tên khách hàng</h5>
                    <input type="text"  class="form-control-plaintext col-9 border border-info rounded" required="true"
                           name="name" value=<?php if($name != null) echo $name ?> >
                </div>

                <div class="row my-3">
                    <h5 class="col-3">Số điện thoại</h5>
                    <input type="number" placeholder="" class="form-control-plaintext col-9 border border-info rounded" required="true"
                           name="sdt" value=<?php if($sdt != null) echo $sdt ?>>
                </div>

                <div class="row my-3">
                    <h5 class="col-3">Email</h5>
                    <input type="email" placeholder="name@gmail.com" class="form-control-plaintext col-9 border border-info rounded" required="true"
                           name="email" value=<?php if($email != null) echo $email ?>>
                </div>

                <div class="row my-3">
                    <h5 class="col-3">Mã voucher</h5>
                    <input type="text" placeholder="" class="form-control-plaintext col-9 border border-info rounded" required="true"
                           name="voucher" value=<?php if($voucher != null) echo $voucher ?> >
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-lg rounded" name="active" value="active">Kích hoạt</button>
                </div>
            </form>
        </div>
        <?php if (isset($result) && ($result['result'] === false)): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Thông báo!</strong> <?php echo $result['mess']?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php elseif (isset($result) && ($result['result'] === true)): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Thành công!</strong> <?php echo $result['mess']?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
<footer></footer>
</html>

alter table ost_pax_tour
    add after_sales_sms int null;

create index ost_pax_tour_after_sales_sms_index
    on ost_pax_tour (after_sales_sms);

alter table ost_pax_tour modify after_sales_sms int null comment 'sms_log_id';


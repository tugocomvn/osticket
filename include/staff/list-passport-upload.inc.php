<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if( !$thisstaff->canViewVisaDocuments()) die('Access Denied');
?>
<style>
    td br {
        margin-bottom: 0.5em;
        display: block;
        content: "";
    }

    table.list tbody td, table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }

    table.list {
        margin-left: -100px;
    }

    td, td span, td p {
        word-break: break-all;
    }
</style>
<h2>Passport Upload</h2>
<form action="<?php echo $cfg->getUrl();?>scp/list-passport-upload.php" method="get">
    <?php csrf_token(); ?>
    <table>
        <tr>
            <td>Tên tour</td>
            <td>
                <input type="text" name="tour_name" value="<?php
                if (isset($_REQUEST['tour_name']) && !empty($_REQUEST['tour_name']))
                    echo trim($_REQUEST['tour_name']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Số hộ chiếu</td>
            <td>
                <input type="text" name="passport_code" value="<?php
                if (isset($_REQUEST['passport_code']) && !empty($_REQUEST['passport_code']))
                    echo trim($_REQUEST['passport_code']);
                ?>" class="input-field" placeholder="" />
            </td>
            <td>Booking Code</td>
            <td>
                <input type="text" name="booking_code" value="<?php
                if (isset($_REQUEST['booking_code']) && !empty($_REQUEST['booking_code'])) echo trim($_REQUEST['booking_code']);
                ?>" class="input-field" placeholder="" />
            </td>

        </tr>
        <tr>
            <td>Người Upload</td>
            <td>
                <select name='staff' class="staff">
                    <option value>----- All----- </option>
                    <?php foreach ($listStaff as $key=>$value): ?>
                        <option value="<?php echo $key ?>"
                            <?php if(isset($_REQUEST['staff']) && (int)$_REQUEST['staff'] == $key) echo 'selected' ?>>
                            <?php echo $value ?>
                        </option>
                    <?php endforeach;?>
                </select>
            </td>
            <td>Thời gian Upload From</td>
            <td>
                <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
            </td>
            <td>To</td>
            <td>
                <input class="dp input-field" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="6">
                <input type="submit" class="btn_sm btn-primary"
                       value="<?php echo __('Search');?>" />
                <input type="reset" class="btn_sm btn-default"
                       value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
            </td>
        </tr>
        <script>
            function reset_form(event) {
                console.log(event);
                form = $(event).parents('form');
                form.find('input[type="text"], select, .hasDatepicker').val('');
                form.submit();
            }
        </script>
    </table>
</form>

<table class="list" border="0" cellspacing="1" cellpadding="0" width="1258">
    <caption><?php echo $pageNav->showing()  ?></caption>
    <thead>
        <tr>
            <th>STT</th>
            <th>Tour</th>
            <th>Mã booking</th>
            <th style="width: 18%;">Số passport</th>
            <th style="width: 18%;">Tên khách</th>
            <th>Người upload</th>
            <th>Thời gian upload</th>
            <th>Kết quả</th>
            <th>#</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; while ($results && ($row = db_fetch_array($results))): ?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td><?php echo $row['tour_name'] ?></td>
                <td><?php echo $row['booking_code'] ?></td>
                <td><?php echo getPassportNo($row['result']) ?></td>
                <td><?php echo getNamePax($row['result']) ?></td>
                <td><?php echo $listStaff[$row['upload_by']] ?></td>
                <td><?php if($row['upload_at'] !== 0  && $row['upload_at'] !== null)
                    echo date('d/m/Y H:i', strtotime($row['upload_at'])) ?>
                </td>
                <td>
                    <?php switch ((int)$row['status']):
                    case \Tugo\PassportPhoto::PASSPORT_SCAN_WAITING: ?>
                        <span class="label label-small label-default">Chưa xử lý</span>
                    <?php break;
                    case \Tugo\PassportPhoto::PASSPORT_SCAN_FAILURE: ?>
                        <span class="label label-small label-danger">Thất bại</i></span>
                    <?php break;
                    case \Tugo\PassportPhoto::PASSPORT_SCAN_SUCCESS: ?>
                    <span class="label label-small label-success">Thành công</i></span>
                    <?php endswitch;?>
                </td>
                <td class="autohide-buttons">
                    <a href="<?php echo $cfg->getUrl() ?>scp/list-passport-upload.php?passport_id=<?php echo $row['passport_id'] ?>&action=view_image"
                       target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                       data-title="View Image Passport"><i class="icon-eye-open"></i></a>

                    <a href="<?php echo $cfg->getUrl(); ?>scp/visa-tour-guest.php?tour=<?php echo $row['tour_id'] ?>&layout=list"
                       target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                       data-title="View This Tour" >
                        <i class="icon-list"></i></a>

                    <?php if($row['status'] == \Tugo\PassportPhoto::PASSPORT_SCAN_SUCCESS): ?>
                        <a href="<?php echo $cfg->getUrl();
                        ?>scp/tour-guest.php?tour=<?php echo $row['tour_id'] ?>"
                           target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                           data-title="Edit pax info" >
                            <i class="icon-user"></i></a>
                    <?php endif; ?>

                    <?php if($row['status'] == \Tugo\PassportPhoto::PASSPORT_SCAN_FAILURE): ?>
                        <a href="<?php echo $cfg->getUrl().'scp/list-passport-upload.php?tour_id='.$row['tour_id'].'&passport_id='.$row['passport_id'].'&action=reupload_passport_photo' ?>"
                           target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                           data-title="Re Upload Passport Photo" >
                            <i class="icon-refresh"></i></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endwhile;?>
    </tbody>
</table>
<?php
if ($results): //Show options..
    echo '<div>&nbsp;' . __('Page') . ':' . $pageNav->getPageLinks() . '&nbsp;</div>';
endif;

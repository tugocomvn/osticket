<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <?php foreach($bi_menu_aside_data as $_name => $_item): ?>
            <li class="nav-item has-treeview
            <?php if (isset($TabActive) && ($TabActive === $_name || (
                    isset($_item['children']) && is_array($_item['children']) && in_array($TabActive, $_item['children'])
                ))) echo 'menu-open' ?>">
                <a href="<?php
                    if(isset($_item['available']) && !$_item['available'])
                        echo '#';
                    else
                        echo $cfg->getUrl().'scp/'. $_item['href']; ?>"
                   class="nav-link <?php if (isset($TabActive) && ($TabActive === $_name || (
                            isset($_item['children']) && is_array($_item['children']) && in_array($TabActive, $_item['children'])
                        ))) echo 'active' ?>">
                    <i class="nav-icon <?php echo $_item['icon-class'] ?>"></i>
                    <p>
                        <?php echo $_item['text'] ?>
                        <?php if(isset($_item['children']) && is_array($_item['children']) && count($_item['children'])): ?>
                        <i class="right fas fa-angle-left"></i>
                        <?php endif; ?>
                        <?php if(isset($_item['available']) && !$_item['available']): ?>
                            <span class="right badge badge-info" title="Comming Soon">
                                <i class="fas fa-tools"></i>
                            </span>
                        <?php endif; ?>
                    </p>
                </a>
                <?php if(isset($_item['children']) && is_array($_item['children']) && count($_item['children'])): ?>
                <ul class="nav nav-treeview">
                    <?php foreach($_item['children'] as $__name => $__item): ?>
                        <li class="nav-item ">
                            <a href="<?php echo $cfg->getUrl() ?>scp/<?php echo $__item['href'] ?>"
                               class="nav-link <?php if (isset($SubTabActive) && $SubTabActive === $__name) echo 'active' ?>">
                                <i class="far <?php echo $__item['icon-class'] ?> nav-icon"></i>
                                <p><?php echo $__item['text'] ?></p>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>
<!-- /.sidebar-menu -->
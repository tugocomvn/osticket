<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';
session_start();

$len = 0;
$total = 0;
$group = null;
$pattern = null;
$strWarn = "";
$class_col = "col-4";
$voucher = array();
$showAdd = "none";
$showSuccess = "none";
$pre = $posfix = '';

if ($_POST) {
    $result = null;
    if(isset($_POST['group_id'])) {
        $group = $_POST['group_id'];
    }
    if(isset($_POST['strlen'])){
        $len = $_POST['strlen'] ?: 5;
        if($len > COL_2){
            $class_col = "col-6";
        }
        if($len > COL_1 ){
            $class_col = "col-12";
        }
    }
    if(isset($_POST['pattern'])){
        $pattern = $_POST['pattern'];
    }
    if(isset($_POST['total'])){
        $total = $_POST['total'] ?: 1;
        if($total > MAX_TOTAL){
            $strWarn .= "Chỉ có thế tạo tối đa ".MAX_TOTAL." mã. ";
        }
        $status = NOT_SEND;
    }

    if(isset($_POST['create']) == "create"){
        unset($_SESSION['arrVoucher']);
        if($len < 0 || $total < 0) {
            $strWarn .= " Đầu vào không hợp lệ! Có thể đầu vào có có âm.";

        } elseif ($len < strlen($pattern)) {
            $strWarn .= " 'Mã mẫu' phải ít ký tự hơn hoặc bằng 'Số lượng kí tự'.";

        } else {
            if($len > strlen($pattern) && !is_null($pattern) && !empty($pattern)) { //tu them # cho du
                $str_add = '';

                for ($i=1; $i <= $len - strlen($pattern); $i++) {
                    $str_add .= '#';
                }

                $pattern = $pattern.$str_add;
            }

            if(!is_null($pattern) && !empty($pattern)) { //TG###KD
                $pos = strpos($pattern,"#");
                $len = substr_count($pattern,"#"); //so ki tu random
                $pre = strtoupper(substr($pattern,0, $pos)); //lay tien to //in hoa
                if (strlen($pattern) - $len - strlen($pre) > 0){ //co hau to
                    $pos =  $len + strlen($pre) - strlen($pattern); //so ki tu hau to
                    $posfix = strtoupper(substr($pattern, $pos)); //hau to //in hoa
                }
            }

            $result = Voucher::generate_string($len, $total, $pre, $posfix);

            $_SESSION['arrVoucher'] = $result;
        }
        $showAdd = "block";
    }

    if(isset($_POST['add']) == "add"){
        if(isset($_SESSION['arrVoucher'] )) {
            $data = $_SESSION['arrVoucher'];
            Awing::insertDataVoucher($data, $status, $group);
            $result = $_SESSION['arrVoucher'];
            unset($_SESSION['arrVoucher']);
            $showSuccess = "block";
        }
    }
}

require_once ("create_voucher.php");

<?php
$booking_type_by_day = TicketAnalytics::bookingAnalytics($_REQUEST['from'], $_REQUEST['to'], 'booking_type_by_day', $_REQUEST);
$booking_type_by_day_arr = [];
$dates_source = [];
while($booking_type_by_day && ($row = db_fetch_array($booking_type_by_day))) {
    $type = _String::json_decode($row['booking_type']);
    if (!isset($booking_type_by_day_arr[$type]))
        $booking_type_by_day_arr[$type] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $dates_source[] = $row['week'] . '-' . $row['year'];
            $booking_type_by_day_arr[$type][$row['week'] . '-' . $row['year']] = $row['total'];
            break;
        case 'month':
            $dates_source[] = $row['month'] . '-' . $row['year'];
            $booking_type_by_day_arr[$type][$row['month'] . '-' . $row['year']] = $row['total'];
            break;
        case 'day':
        default:
            $dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $booking_type_by_day_arr[$type][date(DATE_FORMAT, strtotime($row['date']))] = $row['total'];
            break;
    }
}

$dates_source = array_filter(array_unique($dates_source));

foreach ($booking_type_by_day_arr as $_type => $_date_data) {
    foreach ($dates_source as $_date) {
        if (!isset($booking_type_by_day_arr[ $_type ][ $_date ]))
            $booking_type_by_day_arr[ $_type ][ $_date ] = 0;
    }

    uksort($booking_type_by_day_arr[ $_type ], "___date_compare");
}
?>
<td colspan="2">
    <h2>Booking Type by Day <button class="btn_sm btn-xs btn-default" onclick="hide_all('booking_type')">Hide All</button></h2>
    <canvas id="booking_type" width="1000" height="800"></canvas>
</td>
<script>
    var booking_type_elm = document.getElementById("booking_type").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(<?php echo count($booking_type_by_day_arr) ?>);
    var booking_type = new Chart(booking_type_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($booking_type_by_day_arr as $_type => $_date): ?>
                {
                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                    backgroundColor:  colors[<?php echo $i++ ?>],
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    fill: true
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>
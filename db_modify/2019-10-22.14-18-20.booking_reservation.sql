ALTER TABLE `ost_groups` ADD `can_view_retail_price` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_view_net_price` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_approve` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_view_approve` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_hold_reservation` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

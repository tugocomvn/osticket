<?php
if (!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canCreatePayments()) {
    die('Access Denied');
}

if (!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
    function staffLoginPage($msg)
    {
        global $ost, $cfg;
        $_SESSION['_staff']['auth']['dest'] =
            '/' . ltrim($_SERVER['REQUEST_URI'], '/');
        $_SESSION['_staff']['auth']['msg'] = $msg;
        $_SESSION['_staff']['auth']['finance'] = true;
        require(SCP_DIR . 'login.php');
        exit;
    }
}

if (!isset($_SESSION['check_session']) || !$_SESSION['check_session']
    || !isset($_SESSION['check_session_time'])
    || (isset($_SESSION['check_session_time']) && time() - $_SESSION['check_session_time'] > 1000)) {
    $_SESSION['check_session_time'] = -10000;
    $_SESSION['check_session'] = false;
    $_SESSION['check_session_request'] = true;
    unset($_SESSION['check_session_time']);
    unset($_SESSION['check_session']);
    staffLoginPage(__('Enter password for access'));
    exit;
}

$info = array();
$info = Format::htmlchars(($errors && $_POST) ? $_POST : $info);
if (isset($_REQUEST['from_view']) && $_REQUEST['from_view'] === 'receipt' && $_REQUEST['receipt_id']) {
    $info['topicId'] = THU_TOPIC;
}

if (!$info['topicId']) {
    $info['topicId'] = $cfg->getDefaultTopicId();
}

$form = null;
if ($info['topicId'] && ($topic = Topic::lookup($info['topicId']))) {
    $form = $topic->getForm();
    if ($_POST && $form) {
        $form = $form->instanciate();
        $form->isValid();
    }
}

if ($_POST) {
    $info['duedate'] = Format::date($cfg->getDateFormat(),
        strtotime($info['duedate']));
}
?>

<?php
$results = Receipt::lookup((int)$_GET['receipt_id']);
if($results):
?>
<style>
    #container {
        width: 95%;
        margin: auto;
    }
    .receipt-input input[type="text"]{
        border: 0;
        border-bottom: 1px dotted
    }
</style>
<?php endif; ?>
<form action="tickets.php?a=io" method="post" id="save" enctype="multipart/form-data" class="repeater">
    <?php csrf_token();
    $width_form_create = '100%'; ?>
    <input type="hidden" name="do" value="create">
    <input type="hidden" name="a" value="io">
    <h2><?php echo __('Create a New Payment'); ?></h2>
    <?php ;
    if($results):
    $width_form_create = '48%'; ?>
    <table class="pull-left receipt-input" style="width: <?php echo $width_form_create ?>;">
        <tbody>
        <tr>
            <th colspan="2">
                Information from receipt
            </th>
        </tr>
        <tr>
            <td>
                <b><i><?php echo __('Mã phiếu thu:'); ?></i></b>
                <input style="width: 150px;" type="text" class="input-field" value="<?php echo $results->receipt_code ?>" readonly>
            </td>
            <td>
                <?php if (isset($results->status)): ?>
                    <?php if((int)$results->status === 0): ?>
                        <span class="label label-default pull-right">Chưa xử lý</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::MONEY_RECEIVED): ?>
                        <span class="label label-default pull-right">Đã nhận tiền</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::SEND_CODE): ?>
                        <span class="label label-primary pull-right">Đã gửi mã xác nhận</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::WAITING_VERIFYING): ?>
                        <span class="label label-primary pull-right">Khách xác nhận</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::APPROVED): ?>
                        <span class="label label-default pull-right">Kế toán approved</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::REJECTED): ?>
                        <span class="label label-danger pull-right">Đã hủy</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::UPLOAD_IMAGE): ?>
                        <span class="label label-success pull-right">Upload hình phiếu thu</span>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b><i><?php echo __('Booking code:'); ?></i></b>
                <input style="width: 150px;" type="text" class="input-field" value="<?php echo $results->booking_code ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Lộ trình:</label>
                <input type="text" class="input-field"  value="<?php echo $results->tour_name ?>" readonly>
            </td>
            <td>
                <label>Ngày khởi hành:</label>
                <input type="text" class="input-field"
                       value="<?php if (strtotime($results->departure_date)) echo date('d/m/Y', strtotime($results->departure_date)) ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Giá tiền 1 khách (NL):</b></label>
                <input type="text" class="input-field" value="<?php echo number_format($results->unit_price_nl) ?>" readonly>
            </td>
            <td>
                <label><b>Số lượng (NL):</b></label>
                <input style="width: 60px;" type="text" class="input-field" value="<?php echo $results->quantity_nl ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Giá tiền 1 khách (TE):</label>
                <input type="text" class="input-field"  value="<?php if(isset($results->unit_price_te)) echo number_format($results->unit_price_te) ?>" readonly>
            </td>
            <td>
                <label>Số lượng (TE):</label>
                <input style="width: 60px;" type="text" class="input-field"  value="<?php if(isset($results->quantity_te)) echo $results->quantity_te ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Phí phụ thu:</label>
                <input type="text" class="input-field"  value="<?php echo number_format($results->surcharge) ?>" readonly>
            </td>
            <td>
                <label>Nội dung phụ thu:</label>
                <input type="text" class="input-field" value="<?php echo $results->surcharge_note ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Tổng số tiền khách cần đóng:</b></label>
                <input type="text"class="input-field" value="<?php echo number_format($results->amount_to_be_received) ?>" readonly>
            </td>
            <td>
                <label><b>Số tiền khách đóng:</b></label>
                <input type="text" class="input-field" value="<?php echo number_format($results->amount_received) ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Số tiền còn lại:</b></label>
                <input type="text" class="input-field" value="<?php if(isset($results->balance_due)) echo number_format($results->balance_due) ?>" readonly>
            </td>
            <td>
                <label><b>Hình thức thanh toán:</b></label>
                <input type="text" class="input-field" value="<?php if(!empty($results->payment_method)) echo PaymentMethod::caseTitleName($results->payment_method)?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Tên khách đại diện:</b></label>
                <input type="text" class="input-field" value="<?php echo $results->customer_name ?>" readonly>
            </td>
            <td>
                <label><b>Số điện thoại khách:</b></label>
                <input type="text" class="input-field" value="<?php echo $results->customer_phone_number ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Tên nhân viên kinh doanh:</label>
                <input type="text" class="input-field" value="<?php echo $results->staff_name ?>" readonly>
            </td>
            <td>
                <label>Số điện thoại nhân viên kinh doanh:</label>
                <input type="text" class="input-field" value="<?php echo $results->staff_phone_number ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Tên nhân viên thu tiền:</label>
                <input type="text" class="input-field" value="<?php echo $results->staff_cashier_name ?>" readonly>
            </td>
            <td>
                <label>Số điện thoại nhân viên thu tiền:</label>
                <input type="text" class="input-field" value="<?php echo $results->staff_cashier_phone_number ?>" readonly>
            </td>
        </tr>
        </tbody>
    </table>
    <?php endif;?>
    <table class="form_table" width="<?php echo $width_form_create?>" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <!-- This looks empty - but beware, with fixed table layout, the user
             agent will usually only consult the cells in the first row to
             construct the column widths of the entire toable. Therefore, the
             first row needs to have two cells -->
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="2">
                <h4><?php echo __('New Payment'); ?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Payment Information and Options'); ?></strong>:</em>
            </th>
        </tr>
        <tr>
            <td width="160">
                <?php echo __('Department'); ?>:
            </td>
            <td>
                <select name="deptId">
                    <?php
                    $staff_dept = $thisstaff->getGroup()->getDepartments();

                    if ($depts = Dept::getDepartments()) {
                        $echo_selected = null;
                        foreach ($depts as $id => $name) {
                            if (strpos(strtolower($name), 'finance') === false) {
                                continue;
                            }
                            if (!in_array($id, $staff_dept)) {
                                continue;
                            }
                            $selected = false;
                            if (!$echo_selected
                                && (($info['deptId'] == $id)
                                    || ($staff_dept && in_array($id, $staff_dept)))
                            ) {
                                $selected = true;
                                $echo_selected = true;
                            }
                            echo sprintf('<option value="%d" %s>%s</option>', $id,
                                $selected ? 'selected="selected"' : '', $name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error"><?php echo $errors['deptId']; ?></font>
            </td>
        </tr>
        <tr>
            <td width="160" class="required">
                <?php echo __('Type'); ?>:
            </td>
            <td>
                <select name="topicId" onchange="javascript:
                        var data = $(':input[name]', '#dynamic-form').serialize();
                        $.ajax(
                          'ajax.php/form/help-topic/' + this.value,
                          {
                            data: data,
                            dataType: 'json',
                            success: function(json) {
                              if (json.type && json.type == 'io') { // form ke toan

                              } else {

                              }
                              $('#dynamic-form').empty().append(json.html);
                              $(document.head).append(json.media);
                            }
                          });">
                    <?php
                    if ($topics = Topic::getHelpTopics()) {
                        if (count($topics) == 1) {
                            $selected = 'selected="selected"';
                        } else { ?>
                            <option value="" selected>&mdash; <?php echo __('Select Help Topic'); ?> &mdash;</option>
                        <?php }
                        foreach ($topics as $id => $name) {
                            if (!in_array($id, unserialize(IO_TOPIC))) {
                                continue;
                            }
                            echo sprintf('<option value="%d" %s %s>%s</option>',
                                $id, ($info['topicId'] == $id) ? 'selected="selected"' : '',
                                $selected, $name);
                        }
                        if (count($topics) == 1 && !$form) {
                            if (($T = Topic::lookup($id))) {
                                $form = $T->getForm();
                            }
                        }
                        if (isset($_REQUEST['from_view']) && $_REQUEST['from_view'] === 'receipt' && $_REQUEST['receipt_id']){
                            echo '<input type="hidden" name="from_view"  value="'.$_REQUEST['from_view'].'">';
                            echo '<input type="hidden" name="receipt_id"  value="'.$_REQUEST['receipt_id'].'">';
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>
        </tr>

        </tbody>
        <tbody id="dynamic-form">
        <?php
        if ($form) {
            print $form->getForm()->getMedia();
            include(STAFFINC_DIR . 'templates/dynamic-form-io.tmpl.php');
        }
        ?>
        </tbody>
        <?php if($thisstaff->canEditPointLoyalty()): ?>
            <tbody>
            <tr>
                <td>Cập nhật điểm <small></small></td>
                <td>
                    <p class="">
                        <label for="customer-number">SĐT</label>
                        <input type="text" class="input-field" name="customer-number" id="customer-number"
                               onkeyup="getRank(this)" value=""/>
                        <button class="btn_sm btn-primary btn-xs" type="button"
                                onclick="getRank($(this).prev('input').get(0))"><i class="icon-search"></i></button>
                        <label for="type">Loại</label>
                        <select name="type" id="type">
                            <option value="+">Tích điểm</option>
                            <option value="-">Sử dụng điểm</option>
                        </select>
                        <label for="point">Số điểm</label> <input type="text" class="input-field" size="5" id="point"
                                                                  name="point" value=""/>
                        <label for="reason">Lí do </label> <input type="text" class="input-field" id="reason" name="reason"
                                                                  value=""/>
                        <button data-repeater-save class="btn_sm btn-xs btn-success" type="button" name="save" id="savePoint"
                                onclick="updatePoint()" disabled="true">Save
                        </button>
                    </p>
                    <p class="rank_description" id="description"><em style="color:gray;display:inline-block">nhập SĐT để kiểm tra thông tin điểm của khách</em></p>
                    <p id="point_update_success"></p>
                    <input type="hidden" name="id_history" id="id_history">
                    <p id="result_point"></p>
                    <table id="point_history">
                        <tbody>
                        <tr></tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        <?php endif;?>
    </table>
    <p style="text-align:center;">
        <input type="submit" name="submit" class="btn_sm btn-primary"
               value="<?php echo _P('action-button', 'Create'); ?>">
        <input type="reset" name="reset" class="btn_sm btn-default" value="<?php echo __('Reset'); ?>">
        <input type="button" name="cancel" class="btn_sm btn-danger" value="<?php echo __('Cancel'); ?>" onclick="javascript:
        $('.richtext').each(function() {
            var redactor = $(this).data('redactor');
            if (redactor && redactor.opts.draftDelete)
                redactor.deleteDraft();
        });
        window.location.href='tickets.php';
    ">
    </p>
</form>
<script>
    var getRank_timeout = 0;
    var last_value = [];
    var customer_number = '';
    function getRank(elm) {
        document.getElementById("point_history").innerHTML = "";
        document.getElementById("result_point").innerHTML = "";
        if (getRank_timeout) clearTimeout(getRank_timeout);

        value = elm.value.trim();
        _desc = $(elm).parents('tr').find('.rank_description');

        if (value.length !== 10) {
            _desc.text('Số điện thoại khách hàng phải gồm 10 con số');
            return false;
        }

        if (!value) {
            _desc.text('');
            return false;
        }

        if (isNaN(value)) {
            _desc.text('Số điện thoại khách hàng phải gồm 10 con số');
            return false;
        }

        $(elm).val(value);

        getRank_timeout = setTimeout(function () {
            get_rank(value, elm);
        }, 500);
    }
    function get_rank(value, elm) {
        _desc = $(elm).parents('tr').find('.rank_description');
        _desc.text('Đang tải...');
        let uuid = '';
        $.get('<?php echo $cfg->getUrl(); ?>scp/ajax.php/users/' + value + '/getInfo', function (data, status) {

            if ('success' === status && data) {
                _desc.html('<p>Khách hàng: <strong>' + data.pax.customer_name
                    + '</strong> - Sđt: <em>' + data.pax.phone_number + '</em>'
                    + ' - Tổng điểm: <big><strong>' + data.pax.point + '</strong></big>'
                    + '</p>'
                );
                uuid = data.pax.uuid;
                customer_number = data.pax.customer_number;
                $("#savePoint").prop('disabled', false);
            } else {
                _desc.text('Không tìm thấy thông tin');
            }
            $(elm).focus();
        }).done(function() {
            showHistory(uuid);
        });
    }

    function updatePoint() {
        var type = $("#type").val();
        var point = $("#point").val();
        var reason = $("#reason").val();
        booking_code = $('[data-name=booking_code]').val();
        if(booking_code === '')
        {
            $("#description").html('<p class="error">Chưa nhập mã booking phía trên</p>');
            return;
        }
        if (customer_number !== '') {
            $.post('<?php echo $cfg->getUrl(); ?>scp/ajax.php/users/' + customer_number + '/updatePointCustomer', {
                type: type,
                subject: reason,
                booking_code: booking_code,
                change_point: point,
            }).done(function (data) {
                $("#description").html('<p style="color:darkgreen">Cập nhật điểm thành công</p>');
                $("#point_update_success").html(data);
                $("#savePoint").prop('disabled', true);
                id = $(".userpoint").attr("data-id");
                if(id !== null && id !== '') {
                    $("#id_history").val(id);
                }
            }).fail(function (response) {
                $("#description").html('<p class="error">'+response.responseText+'</p>');
            });
        }
    }

    function showHistory(uuid) {
        if(uuid === '')
            return;
        _result = $('#result_point');
        $.get('<?php echo $cfg->getUrl(); ?>scp/tickets.php?action=getPoint&uuid='+uuid, function (data, status) {
            if(data.length === 2) { //empty
                _result.text('Không có lịch sử điểm');
                return;
            }

            var table = document.getElementById("point_history");
            arr_point = JSON.parse(data);
            _result.html("Lịch sử điểm gần nhất - <b><i>Vui lòng kiểm tra kĩ trước khi cập nhật điểm</i></b>");

            var row = table.insertRow(0);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            cell1.innerHTML = "Mã booking";
            cell2.innerHTML = "Điểm";
            cell3.innerHTML = "Thời gian";
            for(let i = (arr_point.length - 1) ; i >= 0; i-- ){
                row = table.insertRow(0);
                cell1 = row.insertCell(0);
                cell2 = row.insertCell(1);
                cell3 = row.insertCell(2);
                cell1.innerHTML = arr_point[i].booking_code;
                cell2.innerHTML = arr_point[i].point;
                cell3.innerHTML = arr_point[i].date;
            }
        });

    }

<?php
if($_GET['receipt_id'] && $_GET['from_view'] = 'receipt'):
$receipt = Receipt::lookup((int)$_GET['receipt_id']);
if($receipt) :
    ?>
    (function($) {
        $('[data-name=booking_code]').val('<?php echo trim($receipt->booking_code) ?>');
        $('[data-name=receipt_code]').val('<?php echo trim($receipt->receipt_code) ?>');
        $('[data-name=amount]').val('<?php echo $receipt->amount_received ?>');
        $('[data-name=quantity]').val('<?php echo (int)$receipt->quantity_nl + (int)$receipt->quantity_te ?>');
        $('[data-name=time]').val('<?php echo date('d/m/Y') ?>');
        note = '<?php
                    if(isset($receipt->surcharge_note) && $receipt->surcharge_note)
                        echo "Phụ thu: ".str_replace(array("\r\n", "\\n", "\n"), "<br>",$receipt->surcharge_note) . "<br>";
                    if(isset($receipt->public_note) && $receipt->public_note)
                        echo "Khách: ".str_replace(array("\r\n", "\\n", "\n"), "<br>",$receipt->public_note) . "<br>";
                    if(isset($receipt->internal_note) && $receipt->internal_note)
                        echo "Nội bộ: ".str_replace(array("\r\n", "\\n", "\n"), "<br>",$receipt->internal_note) . "<br>";
                ?>';
        $("textarea").val(note);
    })(jQuery);
    <?php endif;?>
<?php endif;?>
</script>

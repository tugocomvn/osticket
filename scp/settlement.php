<?php
/*********************************************************************
users.php

Peter Rotich <peter@osticket.com>
Jared Hancock <jared@osticket.com>
Copyright (c)  2006-2014 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');
require(INCLUDE_DIR.'class.account.php');

global $thisstaff, $cfg;

if (!$thisstaff
    || !($thisstaff->canViewSettlementList() || $thisstaff->canEditSettlement()))
    die('Access Denied');

function __account_validate_spend_item(&$errors) {
    if (!isset($_POST['name']) || empty(trim($_POST['name']))) {
        $errors['name'] = 'Name is required.';
    }

    if (isset($_POST['default_quantity']) && !empty(trim($_POST['default_quantity'])) && !is_numeric(trim($_POST['default_quantity']))) {
        $errors['default_quantity'] = 'Default quantity must be a number.';
    }

    if (isset($_POST['default_value'])) {
        $_POST['default_value'] = str_replace(',', '', $_POST['default_value']);

        if (!empty(trim($_POST['default_value'])) && !is_numeric(trim($_POST['default_value']))) {
            $errors['default_value'] = 'Default value must be a number.';
        }
    }

    if ($errors) return false;


    $name = trim($_POST['name']);
    $default_quantity = isset($_POST['default_quantity']) ? floatval(trim($_POST['default_quantity'])) : null;
    $default_value = isset($_POST['default_value']) ? floatval(trim($_POST['default_value'])) : null;
    $description = isset($_POST['description']) ? trim($_POST['description']) : '';
    $views = isset($_POST['views']) ? trim($_POST['views']) : '';
    $status = isset($_POST['status']) && $_POST['status'] ? 1 : 0;

    return [
        'name' => $name,
        'default_quantity' => $default_quantity,
        'default_value' => $default_value,
        'description' => $description,
        'status' => $status,
        'views' => $views,
    ];
}

function __account_add_spend_item(&$errors) {
    global $thisstaff;

    $data = __account_validate_spend_item($errors);
    if (false === $data) return false;

    $string_views = get_view($data);

    $item = SettlementSpendItem::create(
        [
            'name' => $data['name'],
            'default_quantity' => $data['default_quantity'],
            'default_value' => $data['default_value'],
            'description' => $data['description'],
            'status' => $data['status'],
            'views' => $string_views,
            'created_at' => new SqlFunction('NOW'),
            'created_by' => $thisstaff->getId(),
        ]
    );

    return $item->save();
}

function get_view($data) {
    $views = explode(',', $data['views']);
    if (is_array($views) && $views) {
        foreach ($views as $_key => $_value) {
            $views[$_key] = trim($_value);
        }
    }
    if (is_array($views) && $views) {
        $views = array_unique(array_filter($views));
    }

    $string_views = '';
    if (is_array($views) && $views) {
        foreach ($views as $view) {
            $settlement_view = SettlementView::lookup($view);
            if (!$settlement_view) {
                $settlement_view = SettlementView::create(['name' => $view]);
                $settlement_view->save();
            }
        }

        $string_views = implode(',', $views);
    }

    return $string_views;
}

function __account_update_spend_item(&$errors) {
    global $thisstaff;

    $data = __account_validate_spend_item($errors);
    if (false === $data) return false;

    $id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? floatval($_REQUEST['id']) : null;
    if (!$id) {
        $errors['id'] = 'Invalid item';
        return false;
    }

    $item = SettlementSpendItem::lookup($id);
    if (!$item) {
        $errors['id'] = 'Invalid item';
        return false;
    }

    $string_views = get_view($data);

    $item->setAll(
        [
            'name' => $data['name'],
            'default_quantity' => $data['default_quantity'],
            'default_value' => $data['default_value'],
            'description' => $data['description'],
            'status' => $data['status'],
            'views' => $string_views,
            'updated_at' => new SqlFunction('NOW'),
            'updated_by' => $thisstaff->getId(),
        ]
    );

    return $item->save();
}

// POST
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST) {
    $errors = [];
    if (!$thisstaff->canEditSettlement()) {
        header('Location: '. $cfg->getBaseUrl().'/scp/settlement.php');
        exit;
    }

    if (isset($_POST['action']) && 'new' === $_POST['action']) {
        __account_add_spend_item($errors);
    }

    if (isset($_POST['action']) && 'edit' === $_POST['action']) {
        __account_update_spend_item($errors);
    }

    if (!$errors) {
        FlashMsg::set('ticket_create', 'Save successfully');
        header('Location: '. $cfg->getBaseUrl().'/scp/settlement.php');
        exit;
    }
}
// END POST

$nav->setTabActive('operator');

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
switch ($action) {
    case 'new':
        $inc_page = 'settlement-new.inc.php';
        break;
    case 'edit':
        $inc_page = 'settlement-edit.inc.php';

        if (!isset($_REQUEST['id']) || !intval(trim($_REQUEST['id']))) {
            header('Location: '.$cfg->getBaseUrl().'/scp/settlement.php');
            exit;
        }

        $id = intval(trim($_REQUEST['id']));
        $spend_item = SettlementSpendItem::lookup($id);

        if (!$spend_item) {
            header('Location: '.$cfg->getBaseUrl().'/scp/settlement.php');
            exit;
        }

        break;
    default:
        $inc_page = 'settlement-lists.inc.php';
        $name = isset($_REQUEST['name']) && !empty(trim($_REQUEST['name'])) ? trim($_REQUEST['name']) : null;
        $description = isset($_REQUEST['description']) && !empty(trim($_REQUEST['description'])) ? trim($_REQUEST['description']) : null;
        $params = [
            'name' => $name,
            'description' => $description,
        ];
        $total = 0;
        $page = isset($_REQUEST['p']) && intval($_REQUEST['p']) ? intval($_REQUEST['p']) : 1;
        $offset = ($page-1)*PAGE_LIMIT;
        $list = SettlementSpendItem::getPagination($params, $offset, PAGE_LIMIT, $total);

        $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
        $pageNav->setURL('settlement.php');
        $showing=$pageNav->showing().' '._N('items', 'items', $total);

        if ($offset > $total) {
            $page = ceil($offset/PAGE_LIMIT);
            header('Location: '.$cfg->getBaseUrl().'/scp/settlement.php?page='.$page);
            exit;
        }

        break;
}

require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$inc_page);
include(STAFFINC_DIR.'footer.inc.php');

ALTER TABLE osticket_dev.ost_ticket_thread MODIFY thread_type VARCHAR(32) NOT NULL;
CREATE INDEX ost_ticket_thread_thread_type_index ON osticket_dev.ost_ticket_thread (thread_type);
update ost_sms_log set `type`='Overdue' where content like 'nhan vien%';


CREATE INDEX ost_sms_log_type_index ON osticket_dev.ost_sms_log (type);
ALTER TABLE osticket_dev.ost_sms_log ADD ticket_id INT NULL;
CREATE INDEX ost_sms_log_ticket_id_index ON osticket_dev.ost_sms_log (ticket_id);
CREATE INDEX ost_sms_log_phone_number_index ON osticket_dev.ost_sms_log (phone_number);
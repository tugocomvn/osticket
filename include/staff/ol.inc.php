<style>
    .alert td {
        background-color: #ffded8 !important;
    }
</style>
<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') ||  !$thisstaff || !@$thisstaff->isStaff()) die('Access Denied');

if(!$thisstaff->canViewOfflates()) die('Access Denied');

$ticket = $errors = null;

if($_REQUEST['id']) {
    if(!($ticket=Ticket::lookup($_REQUEST['id'])))
        $errors['err']=sprintf(__('%s: Unknown or invalid ID.'), __('ticket'));
    elseif(!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err']=__('Access denied. Contact admin if you believe this is in error');
        $ticket=null; //Clear ticket obj.
    }
}

$qs = array();

$select = 'SELECT o.`ticket_id`,o.`number`,`staff_offer`,o.`ol_status`,t.`staff_id`, ol_handover_name, ol_staff_offer_name,
                 `time_offer`,`ol_reason`,`time_end`,`ol_handover`,`time_accept`,s.`firstname`,CONCAT(s.`firstname`," ",s.`lastname`) as `staff_idx`';

$from = ' FROM '.STAFF_OL_TABLE.' o';

$join = ' LEFT JOIN ost_ticket t ON o.`ticket_id` = t.`ticket_id` LEFT JOIN ost_staff s ON t.`staff_id` = s.`staff_id`';

$where=' WHERE 1 ';

if (!isset($_REQUEST['list']) || empty($_REQUEST['list']))
    $_REQUEST['list'] = 'all';

switch ($_REQUEST['list']) {
    case 'my_requests':
        $ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.ol_my_requests" />',
                             "$('#content').data('tipNamespace', 'dayoff.ol_my_requests');");

        $where .= " AND o.staff_offer = ".db_input($thisstaff->getId()) . " ";

        break;
    case 'need_approval':
        $ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.need_approval" />',
                             "$('#content').data('tipNamespace', 'dayoff.need_approval');");

        $where .= " AND t.staff_id = ".db_input($thisstaff->getId()) . "
            AND ( (o.ol_status is null or o.ol_status like '') OR (o.ol_status < 2 AND o.ol_status > -1)) ";
        break;
    case 'all':
    default:
        $ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.ol_all" />',
                             "$('#content').data('tipNamespace', 'dayoff.ol_all');");
        break;
}

if (isset($_REQUEST['query'])) {
    $_REQUEST['query'] = trim($_REQUEST['query']);
}
if (isset($_REQUEST['startDate'])) {
    $_REQUEST['startDate'] = trim($_REQUEST['startDate']);
}
if (isset($_REQUEST['endDate'])) {
    $_REQUEST['endDate'] = trim($_REQUEST['endDate']);
}
if ($_REQUEST['query']) {
    $search = db_input($_REQUEST['query'], false);
        $where .= ' AND (
                    staff_offer LIKE \'%' . $search . '%\'
                    OR ol_reason LIKE \'%' . $search . '%\'
                   
                )';
    $qs += array('query' => $_REQUEST['query']);
}
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):null;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):null;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){

        $where .= " AND date(from_unixtime(o.`time_offer`)) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $where .=" AND date(from_unixtime(o.`time_offer`)) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
$sortOptions=array();
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'created';
//Sorting options...
$order_column = $order = null;
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column ?: 'o.`created`';

if(isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order ?: 'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by=" $order_column $order ";

$qstr = '&amp;' . Http::build_query($qs);

//Staff::create_table();
//Staff::create_view();

$total = db_count("SELECT count(DISTINCT o.`ticket_id`) $from $join $where");
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('ol.php', $qs);
$query="$select $from $join $where ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
$showing = $search ? __('Search Results').': ' : '';
$res = db_query($query);

if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '."All Absence request";
else:
    $showing=__('No Absence request found!');
endif;?>

<h2><?php echo __('Requests for Leave of Absence');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>

<div class="pull-left">
    <form action="ol.php" method="get">
        <?php csrf_token(); ?>
        <table>
            <tr>
                <td><span>Date offer <strong>Between</strong></span></td>
                <td style="display: block"><input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF></td>
                <td style="display: block"><input class="dp input-field" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF></td>

                <td><span>All</strong></span></td>
                <td><input type="text" id="basic-user-search" name="query" size=30 value="<?php echo Format::htmlchars($_REQUEST['query']); ?>"
                           autocomplete="off" autocorrect="off" autocapitalize="off"></td>
                <td><input type="submit" name="basic_search" class="button" value="<?php echo __('Search'); ?>"></td>
                <!-- <td>&nbsp;&nbsp;<a href="" id="advanced-user-search">[advanced]</a></td> -->
            </tr>
        </table>
    </form>
</div>
<form id="ol_list" action="ol.php" method="POST" name="offlate">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="mass_process" >
    <input type="hidden" id="action" name="a" value="" >
    <input type="hidden" id="selected-count" name="count" value="" >
    <input type="hidden" id="org_id" name="org_id" value="" >
    <table id="tbdata" class="list"  border="0" cellspacing="1" cellpadding="0" width="1058">
        <caption>
            <?php
            if($row = mysqli_num_rows($res) ){
                echo "".$showing. "";
                }
            ?>
        </caption>
        <thead>
        <tr>
            <th><?php echo __('Ticket Number');?></th>
            <th><?php echo __('Người yêu cầu');?></th>
            <th><?php echo __('Người duyệt');?></th>
            <th><?php echo __('Thời gian bắt đầu');?></th>
            <th><?php echo __('Thời gian kết thúc');?></th>
            <th><?php echo __('Lý do');?></th>
            <th><?php echo __('Bàn giao');?></th>
            <th><?php echo __('Số ngày nghỉ/Trạng thái');?></th>
            <th><?php echo __('Ngày duyệt');?></th>
            <th><?php echo __('');?></th>
        </tr>
        </thead>
        <tbody >
        <?php
        if($res):
            $ids = ($errors && is_array($_POST['ids']))?$_POST['ids']:null;
            while (($row = db_fetch_array($res))) {
                $sel=false;
                if($ids && in_array($row['ticket_id'], $ids))
                    $sel=true;
                ?>
                <tr id="<?php echo $row['ticket_id']; ?>"
                    class="<?php if ($row['amount'] < 0) echo 'type_out' ?> <?php if (isset($row['ol_status']) && $row['ol_status'] == '-1') echo ' alert ' ?>">
                    <td>
                        <a class="no-pjax" target="_blank" href="/scp/tickets.php?id=<?php echo $row['ticket_id']; ?>"><?php echo Format::htmlchars($row['number']); ?></a>
                    </td>
                    <td><?php echo $row['ol_staff_offer_name']; ?></td>
                    <td><?php echo $row['staff_idx']; ?></td>
                    <td><?php echo Format::userdate('d/m/Y H:i', $row['time_offer']); ?></td>
                    <td><?php echo Format::userdate('d/m/Y H:i', $row['time_end']); ?></td>
                    <td><?php echo $row['ol_reason']; ?></td>
                    <td><?php echo $row['ol_handover_name']; ?></td>
                    <td style="text-align: center">
                        <?php echo Offlate::calc($row['time_offer'], $row['time_end']); ?>
                    </td>
                    <td>
                        <?php
                        if (isset($row['ol_status'])) echo Offlate::status($row['ol_status']);

                        if (isset($row['time_accept'])) {
                            ?><p><?php echo Format::userdate('d/m/Y', $row['time_accept']); ?></p><?php
                        }
                        ?>
                    </td>
                    <td>
                        <a class="no-pjax btn_sm btn-default" target="_blank" href="/scp/tickets.php?id=<?php echo $row['ticket_id']; ?>">Details</a>

                        <?php if((!isset($row['ol_status']) || !$row['ol_status'])
                            && isset($row['staff_offer']) && $thisstaff->getId() == $row['staff_offer']): ?>
                            <form id="del_form" action="ol.php" method="POST">
                                <p>
                                    <?php csrf_token(); ?>
                                    <input type="hidden" name="id" value="<?php echo $row['ticket_id']; ?>"/>
                                    <?php
                                    if($thisstaff->canEditOfflates()){
                                        ?>
                                        <button class="btn_sm btn-danger btn-xs del_btn" name="delete" value="1">Delete</button>
                                        <?php
                                    }?>
                                </p>
                            </form>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php
            } //end of while.
        endif; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6">
                <?php
                ?>
                <?php if($res && $num){ ?>

                <?php }else{
                    echo __('No off/late found');
                } ?>
            </td>
        </tr>
        </tfoot>
    </table>
</form>

<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>
<script>
    $(function() {
        $(".ol_btn").click(function(){
            if (confirm("Bạn có chắc chắn muốn duyệt?")){
                $('form#ol_form').submit();
            }else{
                return false;
            }
        });
        $(".boss_btn").click(function(){
            if (confirm("Bạn có chắc chắn muốn duyệt?")){
                $('form#ol_form').submit();
            }else{
                return false;
            }
        });
        $(".del_btn").click(function(){
            if(confirm("Bạn có chắc chắn muốn xoá?")){
                $('form#del_form').submit();
            }else{
                return false;
            }
        });
    });
</script>



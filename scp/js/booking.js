
function booking_calc() {
    $('[data-name^="retailprice"], [data-name^="quantity"], [data-name^="netprice"], [data-name^="discountamount"]')
        .off('change, keyup, keypress, blur')
        .on('change, keyup, keypress, blur', booking_calc_action)
}
function booking_calc_action() {
    total_retailprice = total_netprice = total_profit = 0;
    for (var i = 1; i <= 4; i++) {
        quantity_int_val = 0;
        retailprice_int_val = 0;
        netprice_int_val = 0;
        discountamount_int_val = 0;
        quantity_elm = $('[data-name^="quantity'+i+'"]');
        console.log('[data-name^="quantity'+i+'"]');
        console.log('quantity_elm', quantity_elm);
        if (!quantity_elm || !quantity_elm.length) continue;

        retailprice_elm = $('[data-name^="retailprice'+i+'"]');
        console.log('[data-name^="retailprice'+i+'"]');
        console.log('retailprice_elm', retailprice_elm);
        if (!retailprice_elm || !retailprice_elm.length) continue;

        netprice_elm = $('[data-name^="netprice'+i+'"]');
        console.log('[data-name^="netprice'+i+'"]');
        console.log('netprice_elm', netprice_elm);
        if (!netprice_elm || !netprice_elm.length) continue;

        quantity_val = quantity_elm.val().trim();
        if (!quantity_val) quantity_val = 0;
        try {
            quantity_int_val = parseInt(quantity_val);
            console.log('quantity_int_val', quantity_int_val);
        } catch (ex) {
            console.log('ex', ex);
            console.log('quantity_val', quantity_val);
            quantity_int_val = 0;
        }
        retailprice_val = retailprice_elm.val().trim();
        if (!retailprice_val) retailprice_val = 0;
        try {
            retailprice_int_val = parseInt(retailprice_val);
            console.log('retailprice_int_val', retailprice_int_val);
        } catch (ex) {
            console.log('ex', ex);
            console.log('retailprice_val', retailprice_val);
            retailprice_int_val = 0;
        }
        netprice_val = netprice_elm.val().trim();
        if (!netprice_val) netprice_val = 0;
        try {
            netprice_int_val = parseInt(netprice_val);
            console.log('netprice_int_val', netprice_int_val);
        } catch (ex) {
            console.log('ex', ex);
            console.log('netprice_val', netprice_val);
            netprice_int_val = 0;
        }

        discountamount_elm = $('[data-name^="discountamount'+i+'"]');
        if(!discountamount_elm || !discountamount_elm.length)
            discountamount_val = 0;
        else
            discountamount_val = discountamount_elm.val().trim();

        if(!discountamount_val) discountamount_val = 0;
        try{
            discountamount_int_val = parseInt(discountamount_val);
        }catch (ex) {
            console.log('ex', ex);
            discountamount_int_val = 0;
        }
        discountamount_total = $('[data-name^="discountamount'+i+'"]').nextAll('.discountamount_total'+i);
        if(!discountamount_total || !discountamount_total.length) {
            $('[data-name^="discountamount'+i+'"]').after('<span class="discountamount_total'+i+'"></span>');
            discountamount_total = $('[data-name^="discountamount'+i+'"]').nextAll('.discountamount_total'+i);
        }
        discountamount_total.text('='+(discountamount_int_val).formatMoney(0, '.', ',')+'đ');

        total_retailprice += (quantity_int_val*retailprice_int_val - discountamount_int_val);
        console.log('total_retailprice', total_retailprice);
        total_netprice += quantity_int_val*netprice_int_val;
        console.log('total_netprice', total_netprice);
        console.log('total_profit', total_profit);

        retailprice_total = $('[data-name="retailprice'+i+'"]').nextAll('.retailprice_total'+i);
        if (!retailprice_total || !retailprice_total.length) {
            $('[data-name="retailprice'+i+'"]').after('<span class="retailprice_total'+i+'"></span>');
            retailprice_total = $('[data-name="retailprice'+i+'"]').nextAll('.retailprice_total'+i);
        }
        retailprice_total.text('='+(quantity_int_val*retailprice_int_val).formatMoney(0, '.', ',')+'đ');
        //
        netprice_total = $('[data-name="netprice'+i+'"]').nextAll('.netprice_total'+i);
        if (!netprice_total || !netprice_total.length) {
            $('[data-name="netprice'+i+'"]').after('<span class="netprice_total'+i+'"></span>');
            netprice_total = $('[data-name="netprice'+i+'"]').nextAll('.netprice_total'+i);
        }
        netprice_total.text('='+(quantity_int_val*netprice_int_val).formatMoney(0, '.', ',')+'đ');
    }
    total_profit += total_retailprice-total_netprice;
    $('.total_retailprice').text( total_retailprice.formatMoney(0, '.', ',') + 'đ' );
    $('.total_netprice').text( total_netprice.formatMoney(0, '.', ',') + 'đ' );
    $('.total_profit').text( total_profit.formatMoney(0, '.', ',') + 'đ' );
}


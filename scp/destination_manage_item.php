<?php

require('staff.inc.php');
require_once INCLUDE_DIR.'class.tour-market.php';
require_once INCLUDE_DIR.'class.tour-destination.php';

$page = 'destination_manage_item.inc.php';
$market_id = 0;
$destination_id = 0;

function create($value, &$error){
    global $thisstaff;
    db_start_transaction();
    if(!$value || !trim($value['name'])) {
        $error = 'No data';
        return 0;
    }
    try {
        $data = [
            'name'           => trim($value['name']),
            'status'         => (int)$value['status'],
            'land_rate'      => (int)($value['land_rate']),
            'land_date'      => (int)$value['land_date'],
            'flight_rate'    => (int)$value['flight_rate'],
            'flight_date'    => (int)$value['flight_date'],
            'tour_market_id' => (int)$value['market'],
            'created_by'     => $thisstaff->getId(),
            'created_at'     => new SqlFunction('NOW')
        ];

        $new = TourDestination::create($data);
        $id = $new->save();
        db_commit();

    }catch (Exception $ex){
        db_rollback();
        $error = $ex->getMessage();
        return 0;
    }
    return (int)$id;
}

function edit($value, &$error){
    global $thisstaff;
    $market = TourMarket::lookup((int)$value['market']);
    $destination = TourDestination::lookup($value['destination']);

    if(!$value || !$value['market'] || !$value['destination'] || !($market) || !($destination)){
        $error = 'Không tìm thấy điểm đến của thị trường này';
        return 0;
    }

    db_start_transaction();
    try {
        $data = [
            'name'           => trim($value['name']),
            'status'         => (int)$value['status'],
            'land_rate'      => (int)($value['land_rate']),
            'land_date'      => (int)$value['land_date'],
            'flight_rate'    => (int)$value['flight_rate'],
            'flight_date'    => (int)$value['flight_date'],
            'tour_market_id' => (int)$value['market'],
            'updated_by'     => $thisstaff->getId(),
            'updated_at'     => new SqlFunction('NOW')
        ];

        $destination->setAll($data);
        $destination->save(true);

        db_commit();
    }catch (Exception $ex){
        db_rollback();
        $error = $ex->getMessage();
        return 0;
    }
    return (int)$destination->id;
}

if(isset($_REQUEST['market']) && $_REQUEST['market'])
    $market_id = (int)$_REQUEST['market'];

if(isset($_REQUEST['destination']) && $_REQUEST['destination'])
    $destination_id = (int)$_REQUEST['destination'];

$error = null;
if(isset($_REQUEST['action'])){
    switch ($_REQUEST['action']){
        case 'create':
            $title_create = 'Tạo mới điểm đến của thị trường';
            $page = 'destination_create.inc.php';
            break;
        case 'edit':
            $title_edit = 'Chỉnh sửa điểm điến';
            $result = TourDestination::lookup($destination_id);
            $page = 'destination_edit.inc.php';
            break;
    }
}

if($_POST){
    switch ($_POST['action']){
        case 'create':
            $id = create($_POST, $error);
            if($id && !$error)
            {
                $url = $cfg->getUrl().'scp/destination_manage_item.php?market='.$market_id;
                FlashMsg::set('ticket_create', 'Tạo mới điểm đến của thị trường thành công!');
                header('Location: '.$url);
                exit();
            }else
                $page = 'destination_create.inc.php';
            break;
        case 'edit':
            $id = edit($_POST, $error);
            if($id && !$error)
            {
                $url = $cfg->getUrl().'scp/destination_manage_item.php?action=edit&market='.$market_id.'&destination='.$destination_id;
                FlashMsg::set('ticket_create', 'Cập nhật điểm đến của thị trường thành công!');
                header('Location: '.$url);
                exit();
            }
            break;
    }
}
if($market_id)
    $tour_destination = TourDestination::objects()->filter(['tour_market_id' => $market_id])->order_by('name');
else $tour_destination = [];
$name_market = TourMarket::lookup($market_id);
if($name_market)
    $name_market =  $name_market->name;
else $name_market = '';

$nav->setTabActive('operator', 1);
require(STAFFINC_DIR . 'header.inc.php');
require(STAFFINC_DIR . $page);
include(STAFFINC_DIR . 'footer.inc.php');




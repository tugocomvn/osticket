<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
//include_once INCLUDE_DIR.'../scp/app_notification.send.php';
require('admin.inc.php');
include_once INCLUDE_DIR.'tugo.notification.php';


$errors = [];
$page='app_notification.inc.php';
$referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/app_notification.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/app_notification.php';

if (isset($_REQUEST['a']) && $_REQUEST['a']) {
    switch (strtolower($_REQUEST['a'])) {
        case 'add':
            $page = 'app_notification.add.inc.php';
            break;
        case 'send':
            global $thisstaff;

            if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
                $errors['err']=__('Invalid method');
                break;
            }

            if (!isset($_REQUEST['titile']) || empty(trim($_REQUEST['titile']))) $_REQUEST['titile'] = 'Du Lịch Tugo';
            $title = trim($_REQUEST['titile']);

            if (!isset($_REQUEST['content']) || empty(trim($_REQUEST['content']))) {
                $errors['err']=__('Content is required');
                break;
            }

            $content = trim($_REQUEST['content']);

            $params = [];

            if (isset($_REQUEST['url']) && !empty(trim($_REQUEST['url'])))
                $params['url'] = trim($_REQUEST['url']);

            if (isset($_REQUEST['user_uuid']) && !empty(trim($_REQUEST['user_uuid'])))
                $params['user_uuid'] = trim($_REQUEST['user_uuid']);

            if (isset($_REQUEST['phone_number']) && !empty(trim($_REQUEST['phone_number'])))
                $params['phone_number'] = trim($_REQUEST['phone_number']);

            if (isset($_REQUEST['customer_number']) && !empty(trim($_REQUEST['customer_number'])))
                $params['customer_number'] = trim($_REQUEST['customer_number']);

            if (isset($_REQUEST['cloud_message_token']) && !empty(trim($_REQUEST['cloud_message_token'])))
                $params['cloud_message_token'] = trim($_REQUEST['cloud_message_token']);

            if ($thisstaff)
                $params['staff_id'] = $thisstaff->getId();
            else
                $params['staff_id'] = 0;

            $_errors = [];
            if (!($res = \Tugo\Notification::send($title, $content, true, $params, $_errors)) || $_errors) {
                $errors['err'] = $_errors;
                break;
            }

            header('Location: '.$referer);
        default:
            break;
    }
}

if ($errors) {
    $page='app_notification.inc.php';
}

$nav->setTabActive('tugo_app');
$ost->addExtraHeader('<meta name="tip-namespace" content="tugo_app.app_notification" />',
                     "$('#content').data('tipNamespace', 'tugo_app.app_notification');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


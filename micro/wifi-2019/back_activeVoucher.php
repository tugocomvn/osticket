<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';
session_start();

if(isset($_SESSION['result'])) {
    $result['result'] = $_SESSION['result'];
    $result['mess'] = $_SESSION['mess'];

    unset($_SESSION['result']);
    unset($_SESSION['mess']);
}


if(isset($_REQUEST['name']) && $_REQUEST['name'] != null)
    $name = trim($_REQUEST['name']);
else
    $name = null;

if (isset($_REQUEST['email']) && $_REQUEST['email'] != null)
    $email = trim($_REQUEST['email']);
else
    $email = null;

if(isset($_REQUEST['sdt']) && $_REQUEST['sdt'] != null)
    $sdt = trim($_REQUEST['sdt']);
else
    $sdt = null;

if(isset($_REQUEST['voucher']) && $_REQUEST['voucher'] != null)
    $voucher = trim($_REQUEST['voucher']);
else
    $voucher = null;

if($_POST && isset($_POST['active']) && $_POST['active'] == 'active') {

    if (Awing::checkActiveVoucher($voucher) && !empty($name) && !empty($sdt) && !empty($voucher)) {

        //Tao ma booking_code
        $booking_code_new = Voucher::createBookingCodeVoucher();

        //cap nhat user
        $user_id = Awing::findUser($sdt)['user_id'];
        if(empty($user_id))
            $user_id = Awing::insertUser($name, $sdt, $email);

        //Active voucher and write history user active
        $success = Awing::redeemVoucher($booking_code_new,$voucher,$user_id);

        if($success){
            $_SESSION['result'] = true;
            $_SESSION['mess'] = 'Voucher đã được kích hoạt thành công!';

            header('Location: ./back_activeVoucher.php');
            exit;
        }else{
            $result['result'] = false;
            $result['mess'] = 'Kích hoạt không thành công, vui lòng kiểm tra lại thông tin và thử lại';
        }
    }else {
        $result['result'] = false;
        $result['mess'] = 'Mã voucher không tồn tại hoặc đã được kích hoạt';
    }
}
require_once (__DIR__.'/activeVoucher.php');
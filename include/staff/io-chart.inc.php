<?php
/**
 * Created by PhpStorm.
 * User: minhjoko
 * Date: 7/10/18
 * Time: 10:18 AM
 */
if(!$thisstaff || !$thisstaff->canViewPaymentsChart()) exit('Access Denied'); ?>
<form class="well form-inline" id="timeframe-form">
    <label>
        <i class="help-tip icon-question-sign" href="#report_timeframe"></i>&nbsp;&nbsp;<?php
        echo __('Date from'); ?>:
        <input autocomplete="off" type="text" class="dp input-medium search-query"
               name="start" placeholder="<?php echo __('Last month');?>"/>
    </label>
    <label>
        <?php echo __('Period');?>:
        <select name="period">
            <option value="now" selected="selected"><?php echo __('Up to today');?></option>
            <option value="+7 days"><?php echo __('One Week');?></option>
            <option value="+14 days"><?php echo __('Two Weeks');?></option>
            <option value="+1 month"><?php echo __('One Month');?></option>
            <option value="+3 months"><?php echo __('One Quarter');?></option>
            <option value="+6 months"><?php echo __('Six Months');?></option>
        </select>
    </label>
    <label>
        <?php echo __('Department');?>:
        <select name="dept">
            <?php
            $depts = Dept::get_cache(); $is_selected = false;
            ?>
            <?php foreach($depts as $_id => $_name): ?>
                <?php if (strpos(strtolower($_name), 'finance') === false) continue; ?>
                <option value="<?php echo $_id ?>"
                    <?php if(isset($_REQUEST['dept']) && $_id == $_REQUEST['dept']) {$is_selected = true; echo "selected"; } ?>><?php echo $_name?></option>
                    <?php $id_[] = $_id?>
            <?php endforeach; ?>
            <option value="<?php echo implode(",",$id_) ?>" <?php if (!$is_selected) echo 'selected'; ?>>ALL</option>
        </select>
    </label>
    <label>
        <?php echo __('Group');?>:
        <select name="group">
            <option value="1" selected="selected"><?php echo __('By Day');?></option>
            <option value="2"><?php echo __('By Month');?></option>
            <option value="3"><?php echo __('By Year');?></option>
            <option value="4"><?php echo __('By Quarter');?></option>
        </select>
    </label>
    <button class="btn" type="submit"><?php echo __('Refresh');?></button>
</form>
<div style="width:100%;height: auto" id="chart-container">
    <canvas id="mycanvas" width="700" height="500"></canvas>
</div>

<script>
    (function ($) {
        var barGraph;
        function refresh(e) {
            $.ajax({
                url: "ajax.php/report/overview/chart",
                dataType: 'json',
                data: $(this).serialize(),
                type: "GET",
                success: function (data) {
                    console.log(data);
                    var x = data.plots[<?php echo THU_TOPIC ?>];
                    var y = data.plots[<?php echo CHI_TOPIC ?>];
                    var z = [];
                    for (var i = 0; i < x.length; i++) {
                        z[i] = x[i] - y[i];
                    }
                    var chartData = {
                        labels: data.times,
                        datasets: [
                            {
                                label: 'Thu',
                                fill: false,
                                lineTension: 0.1,
                                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                                borderColor: 'rgba(255,99,132,1)',
                                data: data.plots[<?php echo THU_TOPIC ?>]
                            },
                            {
                                label: 'Chi',
                                fill: false,
                                lineTension: 0.1,
                                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                                borderColor: 'rgba(54, 162, 235, 1)',
                                data: data.plots[<?php echo CHI_TOPIC ?>]
                            },
                            {
                                label: 'Còn lại',
                                fill: false,
                                lineTension: 0.1,
                                backgroundColor: 'rgba(255, 159, 64, 0.2)',
                                borderColor: 'rgba(255, 159, 64, 1)',
                                data: z
                            }
                        ]
                    };
                    var ctx = $("#mycanvas")

                    if(barGraph){
                        barGraph.destroy();
                    }
                    barGraph = new Chart(ctx, {
                        type: 'line',
                        data: chartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Payment Chart',
                                fontSize: 25
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function (value, index, money) {
                                            if (parseInt(value) >= 1000) {
                                                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' VND';
                                            } else {
                                                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' VND';
                                            }
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function (t, d) {
                                        var xLabel = d.datasets[t.datasetIndex].label;
                                        var yLabel = t.yLabel >= 1000 ?
                                            t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' VND' :
                                            t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' VND';
                                        return xLabel + ': ' + yLabel;
                                    }
                                }
                            }
                        }
                    });
                },
                error: function (data) {
                    console.log(data);
                }
            });
            if (this.start) build_table.apply(this);
            return false;
        }
        var start, stop;
        function build_table() {
            if (this.start) {
                start = this.start.value || 'last month';
                stop = this.period.value || 'now';
            }else {
                return false;
            }
        }
        $(function() {
            var form = $("#timeframe-form");
            form.submit(refresh);
            //Trigger submit now...init.
            form.submit();
        });
    })(window.jQuery);
</script>

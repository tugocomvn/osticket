<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

$result_staff = Staff::getAllName();
$allStaff = [];
while ($result_staff && ($row = db_fetch_array($result_staff))){
    $allStaff[$row['staff_id']] = $row['name'];
}
?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl().'scp/market_manage_item.php' ?>">Back</a>
<form action="<?php echo $cfg->getUrl().'scp/market_manage_item.php' ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $error ?></div>
    <?php endif;?>

    <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
    <input type="hidden" name="action" value="edit">

    <?php if($result): ?>
        <h2 class="centered"><?php echo $title_edit.': '.$result->name ?></h2>
        <table style="margin-left: 25%; margin-top: 10px">
            <tr>
                <td width="180">Tên<span class="error">*&nbsp;</span></td>
                <td>
                    <input  class="input-field" type="text" size="28" name="name" value="<?php echo $result->name ?>">
                </td>
            </tr>
            <tr>
                <td width="180">Trạng thái</td>
                <td>
                    <select name="status">
                        <option value="0" <?php if(!$result->status) echo 'selected' ?>>Hide</option>
                        <option value="1" <?php if($result->status) echo 'selected' ?>>Show</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="180">Nhân viên tạo</td>
                <td>
                    <span><?php echo $allStaff[$result->created_by] ?></span>
                </td>
            </tr>
            <tr>
                <td width="180">Ngày tạo</td>
                <td>
                    <span><?php if($result->created_at !== null && $result->created_at !== 0)
                        echo date('d/m/Y H:i:s',strtotime($result->created_at)) ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td width="180">Nhân viên chỉnh sửa</td>
                <td>
                    <span><?php echo $allStaff[$result->updated_by] ?></span>
                </td>
            </tr>
            <tr>
                <td width="180">Ngày chỉnh sửa</td>
                <td>
                    <span><?php if($result->updated_at !== null && $result->updated_at !== 0 )
                        echo  date('d/m/Y H:i:s',strtotime($result->updated_at)) ?>
                    </span>
                </td>
            </tr>
        </table>
        <p class="centered">
            <button class="btn_sm btn-primary" name="edit" value="edit">Save</button>
        </p>
    <?php else: ?>
        <div id="msg_error">Không tìm thấy thị trường này</div>
    <?php endif;?>
</form>

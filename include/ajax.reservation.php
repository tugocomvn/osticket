<?php
require_once(INCLUDE_DIR . 'class.reservation.php');
require_once INCLUDE_DIR.'class.booking.php';

class ReservationAjaxAPI extends AjaxController{
    public static function format_values() {
        if(empty($_POST)) return null;
        $arr_data = $_POST['data']; 
        //convert 2d array to 1d
        $one_d_array = self::arrayFlatten($arr_data);
        if(empty($one_d_array) || !isset($one_d_array)) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa có nội dung.',
                ]
            );
            exit;
        }
        $new_arr_data = array_chunk($one_d_array, 3); 
        //change name key array
        $_arr_data = self::changeNameKey2dArray($new_arr_data);
        foreach ($_arr_data as $key => $value) {
            $result[] = self::get_value_format($value);
        }
        //load view with data
        $_one_d_array = self::arrayFlatten($result);
        $_arr_data = array_chunk($_one_d_array, 9);
        $load_view_data = self::changeNameKey2dArrayLoadView($_arr_data);
        $out = array(
            'all_values' => $load_view_data,
            'result' => 1,
            'message' => 'Dữ liệu đã được xử lý từ hệ thống. Vui lòng kiểm tra và điều chỉnh kỹ thông tin trước khi nhấn nút "Confirm & Save"',
            'index' => range(-1,10),
        );
        echo json_encode($out);
        exit;
    }

    public static function arrayFlatten(array $array)
    {
        $flat = array(); 
        $stack = array_values($array); 
        while($stack) 
        {
            $value = array_shift($stack);
            if (is_array($value)){
                $stack = array_merge(array_values($value), $stack);
            }
            else{
            $flat[] = $value;
            }
        }
        return $flat;
    } 

    public static function changeNameKey2dArrayLoadView($arr)
    {
        $second_level = array('giu', 'sure', 'name','giu_', 'sure_', 'name_','giu__', 'sure__', 'name__');
        for($d = 0; $d < count($arr); $d++ )
        {
            for($s = 0; $s < count($arr[$d]); $s++ )
            {
                $output[$d][$second_level[$s]] = $arr[$d][$s];
            }
        }
        return $output;
    }

    public static function get_value_format($data)
    {
        $_data = [];
        if(empty($data)) return null;
        //name content
        $str_name = $data['name'];
        $str_name = _String::khongdau($str_name);
        //booking code
        $booking_code = self::get_booking_code_from_str($str_name);
        if(isset($booking_code) && !empty($booking_code)) {
            $str_name = str_replace($booking_code, '', $str_name);
        }
        //phone number
        $phone_number = self::get_phone_number_from_str($str_name);
        if(isset($phone_number) && !empty($phone_number)) {
            $str_name = str_replace($phone_number, '', $str_name);
        }
        //due date
        $due_date = self::get_due_date_from_str($str_name);
        $_due_date = (!empty($due_date))?date('d/m', strtotime($due_date)):'';
        if(isset($phone_number) && !empty($due_date));
        //staff_id && staff_name
        $staff_id = self::get_staff_id_from_str($str_name);
        $staff_name = self::get_staff_name($staff_id);
        if(isset($staff_id) && !empty($staff_id));
        $_data = [
            'giu' => (!empty($data['giu']))?(int)$data['giu']:null,
            'sure' => (!empty($data['sure']))?(int)$data['sure']:null,
            'content' => $booking_code." ".$phone_number." ".$_due_date." ".$staff_name
        ];
        if(empty($_data)) return null;
        return $_data;
    }

    public static function get_booking_code_from_str($str)
    {
        if(empty($str)) return null;
        preg_match_all('/\bTG123-\d+/', $str, $matches);
        $booking_code = $matches[0];
        if(empty($booking_code)) return null;
        return $booking_code[0];
    }

    public static function get_phone_number_from_str($str)
    {
        if(empty($str)) return null;
        $phone_number = _String::getPhoneNumbers($str);
        if(!isset($phone_number)) return null;
        return $phone_number[0];
    }

    public static function get_staff_id_from_str($str)
    {
        if(empty($str) || is_null($str)) return null;
        preg_match_all('/\b(\w*.\w*)\b/', $str, $matches);
        $staff_name = $matches[0][0];
        if(!isset($staff_name)) return null;
        $sql = "SELECT staff_id FROM ost_staff WHERE username LIKE ".db_input($staff_name)."";
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row['staff_id'];
    }

    public static function get_due_date_from_str($str)
    {
        if(empty($str)) return null;
        preg_match_all('/\b(\d*\/\d*)\b/', $str, $matches);
        $date_dd_mm = array_shift(array_values($matches[0]));
        if($date_dd_mm == NULL) return null;
        $date_string = $date_dd_mm."/".date("Y");
        $date = !empty($date_string)?date('Y-m-d', strtotime(str_replace('/', '-',$date_string))):'';
        if(!isset($date)) return null;
        return $date;
    }

    public static function get_staff_name($staff_id)
    {
        $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
            .' FROM '.STAFF_TABLE.' WHERE staff_id ='.db_input($staff_id)
            .' ORDER by name';
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row['name'];
    }

	public static function save_reservation()
    {   
        global $thisstaff;
        if(empty($_POST)) return null;
        $data_array = $_POST['data'];  
        $tour_id = $_POST['tour_id'];
        $one_d_array = self::arrayFlatten($data_array);
        $one_d_array = array_filter($one_d_array, function($var){return ($var !== NULL && $var !== FALSE && $var !== '');} );
        if(empty($one_d_array) || !isset($one_d_array)) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa có nội dung.',
                ]
            );
            exit;
        }
        $new_arr_data = array_chunk($one_d_array, 3); 
        //change name key array
        $_arr_data = self::changeNameKey2dArray($new_arr_data);
        $content_log = [];
        foreach ($_arr_data as $key => $value) {
            $result = self::get_value_format_save($value);

            $sql = "INSERT INTO ".BOOKING_RESERVATION_TABLE." (hold_qty, sure_qty, phone_number ,booking_code, staff_id, note, created_at, tour_id, created_by) 
            VALUES (".db_input($result['hold_qty'])."
                ,".db_input($result['sure_qty'])."
                ,".db_input($result['phone_number'])."
                ,".db_input($result['booking_code'])."
                ,".db_input($result['staff_id'])."
                ,".db_input($result['note'])."
                ,NOW()
                ,".db_input($tour_id)."
                ,".$thisstaff->getId()."
            )";
            $res = db_query($sql);
            if ($res) {
                $id = db_insert_id();
                $content_log[$id] = $result;
            }
        }

        if(!empty($content_log)) {
            $log = BookingReservationImportLog::create([
                'tour_id'    => $tour_id,
                'content'    => json_encode($content_log),
                'created_by' => $thisstaff->getId(),
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $log->save();
        }

        if(isset($res) && $res){
            echo json_encode(
                [
                    'result' => 1,
                    'message' => 'Đã lưu thành công trang sẽ tự chuyển về trang danh sách',
                ]
                );
            exit;
        }            
    }

    public static function changeNameKey2dArray($arr)
    {
        $second_level = array('giu', 'sure', 'name');
        for($d = 0; $d < count($arr); $d++ )
        {
            for($s = 0; $s < count($arr[$d]); $s++ )
            {
                $output[$d][$second_level[$s]] = $arr[$d][$s];
            }
        }
        return $output;
    }

    public static function get_value_format_save($data)
    {
        $_data = [];
        if(empty($data)) return null;
        //name content
        $str_name = $data['name'];
        $str_name = _String::khongdau($str_name);
        //booking code
        $booking_code = self::get_booking_code_from_str($str_name);
        if(isset($booking_code) && !empty($booking_code))
        $str_name = str_replace($booking_code, '', $str_name);
        //phone number
        $phone_number = self::get_phone_number_from_str($str_name);
        if(isset($phone_number) && !empty($phone_number))
        $str_name = str_replace($phone_number, '', $str_name);
        //due date
        $due_date = self::get_due_date_from_str($str_name);
        $_due_date = (!empty($due_date))?date('d/m', strtotime($due_date)):'';
        if(isset($phone_number) && !empty($due_date));
        //staff_id && staff_name
        $staff_id = self::get_staff_id_from_str($str_name);
        $_data = [
            'hold_qty' => (!empty($data['giu']))?(int)$data['giu']:null,
            'sure_qty' => (!empty($data['sure']))?(int)$data['sure']:null,
            'phone_number' => (!empty($phone_number))?$phone_number:null,
            'booking_code' => (!empty($booking_code))?$booking_code:null,
            'staff_id' => (!empty($staff_id))?$staff_id:null,
            'note' => trim($data['name']),
        ];
        if(empty($_data)) return null;
        return $_data;
    }
}  
?>

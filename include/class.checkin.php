<?php
class Checkin {
    function __construct() {
        global  $thisstaff;
        if (!$thisstaff) return false;
        // kiểm tra checkin chưa
        // nếu chưa redirect về trang checkin
        if (!self::getToday($thisstaff->getId())) {
            $page = $_SERVER['PHP_SELF'];
            $_test_ip = !empty($_REQUEST['test_ip']) ? $_REQUEST['test_ip'] : '';
            $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : getenv('REMOTE_ADDR');
            if ($_test_ip) $ip = $_test_ip;

            if (is_array(config('dayoff')))
                $day_off = config('dayoff');
            else
                $day_off = [config('dayoff')];

            if (!in_array($page, ['/scp/checkin.php', '/scp/logout.php', '/scp/profile.php'])
                && !in_array(date('N'), $day_off)
//                && !$thisstaff->isAdmin()
                && !in_array($thisstaff->getId(), config('no_checkin'))
                && in_array($ip, config('tugo_ip'))
            ) {
                header('Location: /scp/checkin.php');
                exit;
            }
        } else {
            return false;
        }
        // rồi thì thôi
    }

    public static function getToday($staff_id) {
         $sql = sprintf(
             "SELECT COUNT(id) as count FROM %s WHERE staff_id=%s AND DATE(checkin_time)=DATE(NOW())"
             , CHECKIN_TABLE
             , db_input($staff_id)
         );

         $res = db_query($sql);
         if (!$res) return false;
         $row = db_fetch_array($res);
         if (!$row) return false;
         if (!isset($row['count']) || $row['count'] < 1) return false;

         return true;
    }

    public static function getByStaff($staff_id = 0, $from = null, $to = null, $limit = 32, $export = 0) {
        $where = ' ';
        if ($staff_id) $where .= ' AND s.staff_id= '. db_input($staff_id);
        if (!$from) $from = date('Y-m-01');
        if (!$to) $to = date('Y-m-d');

        $_limit = " LIMIT ".$limit;
        $where .= ' AND DATE(c.checkin_time) BETWEEN '.db_input($from)
            .' AND '.db_input($to);

        $order = $export ? " c.staff_id, c.checkin_time ASC " : " c.checkin_time DESC ";
        $sql = sprintf(
            "SELECT DISTINCT s.firstname, s.lastname, s.staff_id, s.username, c.checkin_time
            FROM %s c
            JOIN %s s ON s.staff_id=c.staff_id
            WHERE 1 $where ORDER BY $order $_limit"
            , CHECKIN_TABLE
            , STAFF_TABLE
        );

        $res = db_query($sql);
        if ($export) {
            self::_export($res);
            exit;
        }
        return $res;
    }

    private static function _export($res) {
        require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
        $dir = INCLUDE_DIR.'../public_file/';
        $file_name = 'Checkin_' . Format::userdate("Y-m-d_H-i-s", time()).".xlsx";

        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
        $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $objPHPExcel = new PHPExcel();
        $properties = $objPHPExcel->getProperties();
        $properties->setCreator(NOTIFICATION_TITLE);
        $properties->setLastModifiedBy("System");
        $properties->setTitle(NOTIFICATION_TITLE. " - Checkin Report");
        $properties->setSubject(NOTIFICATION_TITLE." - Checkin Report");
        $properties->setDescription(NOTIFICATION_TITLE." - Checkin Report - ".Format::userdate("Y-m-d H:i:s", time()));
        $properties->setKeywords(NOTIFICATION_TITLE." - Checkin Report");
        $properties->setCategory(NOTIFICATION_TITLE . " - Checkin Report");

        $sheet = $objPHPExcel->getActiveSheet();

        $header = [
            'Firstname',
            'Lastname',
            'Username',
            'Time',
        ];

        $excel_row = 1;
        $col = 'A';
        foreach ($header as $title) {
            $sheet->setCellValue($col++.$excel_row, $title);
        }
        // Set cell A1 with a string value
        $excel_row = 2;
        if ($res) {
            while (($row = db_fetch_array($res))) {
                if (preg_match('/[0-9]/', $row['username']))
                    continue;

                $dto = new DateTime($row['checkin_time']);
                $dateVal = PHPExcel_Shared_Date::PHPToExcel($dto);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$excel_row)
                    ->getNumberFormat()
                    ->setFormatCode("dd/mm/yyyy hh:mm");

                $data = [
                    $row['firstname'],
                    $row['lastname'],
                    $row['username'],
                    $dateVal,
                ];
                $sheet->fromArray($data, NULL, 'A'.$excel_row++);
            } //end of while.
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($dir.$file_name);

        $objPHPExcel->disconnectWorksheets();
        unset($objWriter);
        unset($objPHPExcel);

        $file = $dir.$file_name;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
        exit;
    }

    public static function checkin($staff_id) {
        if (self::getToday($staff_id)) return true;

        $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : getenv('REMOTE_ADDR');

        if (!in_array($ip, config('tugo_ip')) && $_SERVER['SERVER_NAME'] !== 'localhost')
            return false;

        $cookie = _UUID::getCookie();
        $sql = sprintf(
            "INSERT INTO %s SET staff_id=%s , checkin_time=NOW() , ip_address=%s, uuid=%s"
            , CHECKIN_TABLE
            , db_input($staff_id)
            , db_input($ip)
            , db_input($cookie)
        );

        return db_query($sql);
    }
}

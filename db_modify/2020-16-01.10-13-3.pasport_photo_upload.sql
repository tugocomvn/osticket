/*branch: visa */

create table passport_photo_upload
(
    id int unsigned auto_increment,
    passport_no varchar(32) null,
    filename varchar(64) null,
    upload_at datetime null,
    upload_by int null,
    constraint passport_photo_upload_pk
        primary key (id)
);

create unique index passport_photo_upload_filename_uindex
    on passport_photo_upload (filename);

create unique index passport_photo_upload_passport_no_uindex
    on passport_photo_upload (passport_no);


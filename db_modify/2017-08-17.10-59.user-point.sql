ALTER TABLE ost_user ADD uuid VARCHAR(16) NULL;
CREATE UNIQUE INDEX ost_user_uuid_uindex ON ost_user (uuid);

ALTER TABLE api_user_point_history CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE api_device ADD cloud_message_token VARCHAR(255) NULL;
CREATE INDEX api_device_cloud_message_token_index ON api_device (cloud_message_token);
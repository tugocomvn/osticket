<?php

require('staff.inc.php');
require_once INCLUDE_DIR.'class.tour-market.php';
$page = 'market_manage_item.inc.php';


function create($value, &$error){
    global $thisstaff;
    db_start_transaction();
    try {
        if (!$value || !trim($value['name'])) {
            $error = 'No data';
            return 0;
        }
        $data = [
            'name'       => trim($value['name']),
            'status'     => (int)$value['status'],
            'created_by' => $thisstaff->getId(),
            'created_at' => new SqlFunction('NOW')
        ];

        $new = TourMarket::create($data);
        $id = $new->save();
        db_commit();
    }catch (Exception $ex){
        db_rollback();
        $error = $ex->getMessage();
        return 0;
    }
    return (int)$id;
}

function edit($value, &$error){
    global $thisstaff;
    db_start_transaction();
    if(!$value || !$value['id'] || !( $market = TourMarket::lookup((int)$value['id']))) {
        $error = 'Không tìm thấy thị trường này';
        return 0;
    }
    try {
        $data = [
            'name'       => trim($value['name']),
            'status'     => (int)$value['status'],
            'updated_by' => $thisstaff->getId(),
            'updated_at' => new SqlFunction('NOW')
        ];

        $market->setAll($data);
        $market->save(true);
        db_commit();

    }catch (Exception $ex){
        db_rollback();
        $error = $ex->getMessage();
        return 0;
    }
    return (int)$market->id;
}
$error = null;
if(isset($_REQUEST['action'])){
    switch ($_REQUEST['action']){
        case 'create':
            $title_create = 'Tạo mới thị trường';
            $page = 'market_create.inc.php';
            break;
        case  'edit':
            $title_edit = 'Chỉnh sửa thị trường';
            $result = TourMarket::lookup((int)$_REQUEST['id']);
            $page = 'market_edit.inc.php';
            break;
    }
}

if($_POST){
    switch ($_POST['action']){
        case 'create':
            $id = create($_POST, $error);
            if($id && !$error)
            {
                $url = $cfg->getUrl().'scp/market_manage_item.php';
                FlashMsg::set('ticket_create', 'Tạo mới thị trường thành công!');
                header('Location: '.$url);
                exit();
            }else
                $page = 'market_create.inc.php';

            break;
        case 'edit':
            $id = edit($_POST, $error);
            if($id && !$error)
            {
                $url = $cfg->getUrl().'scp/market_manage_item.php?action=edit&id='.$id;
                FlashMsg::set('ticket_create', 'Cập nhật thị trường thành công!');
                header('Location: '.$url);
                exit();
            }
            break;
    }
}

$tour_market = TourMarket::objects()->order_by('name')->all();

$nav->setTabActive('operator', 1);

require(STAFFINC_DIR . 'header.inc.php');
require(STAFFINC_DIR . $page);
include(STAFFINC_DIR . 'footer.inc.php');



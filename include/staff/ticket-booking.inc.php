<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canCreateBookings()) die('Access Denied');

if (defined("DEV_ENV") && DEV_ENV === 0) {
    $sheet = new GoogleSheet(GOOGLE_SHEET_BOOKING, CREDENTIALS_BOOKING_PATH);
    $sheet->getClient();
}

$info=array();
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);

if (!$info['topicId'])
    $info['topicId'] = $cfg->getDefaultTopicId();

$form = null;
if ($info['topicId'] && ($topic=Topic::lookup($info['topicId']))) {
    $form = $topic->getForm();
    if ($_POST && $form) {
        $form = $form->instanciate();
        $form->isValid();
    }
}

if ($_POST)
    $info['duedate'] = Format::date($cfg->getDateFormat(),
        strtotime($info['duedate']));
?>

<form action="tickets.php?a=booking" method="post" id="save"  enctype="multipart/form-data" class="repeater">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="create">
    <input type="hidden" name="a" value="booking">
    <h2><?php echo __('Create a New Booking');?></h2>
    <table class="form_table fixed" width="940" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <!-- This looks empty - but beware, with fixed table layout, the user
             agent will usually only consult the cells in the first row to
             construct the column widths of the entire toable. Therefore, the
             first row needs to have two cells -->
        <tr><td></td><td></td></tr>
        <tr>
            <th colspan="2">
                <h4><?php echo __('New Booking');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Booking Information and Options');?></strong>:</em>
            </th>
        </tr>
        <tr>
            <td width="160" class="required">
                <?php echo __('Type'); ?>:
            </td>
            <td>
                <select name="topicId" onchange="javascript:
                        var data = $(':input[name]', '#dynamic-form').serialize();
                        $('input, button').prop('disabled', true);
                        $.ajax(
                          'ajax.php/form/help-topic/' + this.value,
                          {
                            data: data,
                            dataType: 'json',
                            success: function(json) {
                              if (json.type && json.type == 'io') { // form ke toan

                              } else {

                              }
                              $('input, button').prop('disabled', false);
                              $('#dynamic-form').empty().append(json.html);
                              $(document.head).append(json.media);
                              booking_calc();
                            }
                          });">
                    <?php $selected = null;
                    if ($topics=Topic::getHelpTopics()) {
                        if (count($topics) == 1)
                            $selected = 'selected="selected"';
                        else { ?>
                        <?php                   }
                        foreach($topics as $id =>$name) {
                            if (!in_array($id, [BOOKING_TOPIC])) continue;
                            echo sprintf('<option value="%d" %s %s>%s</option>',
                                $id, ($info['topicId']==$id)?'selected="selected"':'',
                                $selected, $name);
                        }
                        if (count($topics) == 1 && !$form) {
                            if (($T = Topic::lookup($id)))
                                $form =  $T->getForm();
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>
        </tr>

        </tbody>
        <tbody id="dynamic-form">
        <?php
        if ($form) {
            print $form->getForm()->getMedia();
            include(STAFFINC_DIR .  'templates/dynamic-form-io.tmpl.php');
        }
        ?>
        </tbody>
        <tbody>
            <tr>
                <td><strong>Tổng giá bán: </strong></td>
                <td><span class="total_retailprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng giá net: </strong></td>
                <td><span class="total_netprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng lợi nhuận: </strong></td>
                <td><span class="total_profit"></span></td>
            </tr>
        </tbody>
    </table>
    <p style="text-align:center;">
        <input type="submit" id="submit_btn" class="btn_sm btn-primary"
               value="<?php echo _P('action-button', 'Create');?>" >
        <input type="submit" id="check_submit" style="display: none;">
        <input type="reset"  name="reset"  class="btn_sm btn-default"
               value="<?php echo __('Reset');?>">
        <input type="button" name="cancel" class="btn_sm btn-danger"
               value="<?php echo __('Cancel');?>" onclick="javascript:
        $('.richtext').each(function() {
            var redactor = $(this).data('redactor');
            if (redactor && redactor.opts.draftDelete)
                redactor.deleteDraft();
        });
        window.location.href='tickets.php';
    ">
    </p>
</form>
<script>

    $(document).ready(function () {
        $('.repeater').repeater({
            show: function () {
                $(this).slideDown('fast');
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp('fast', deleteElement);
                }
            },
            isFirstItemUndeletable: false
        });

        $('#submit_btn').off('click').on('click', function(e) {
            e.preventDefault();

            if (!$('#save')[0].checkValidity()) {
                $('#check_submit').click();
                return false;
            } else {
                $('input[type="button"], input[type="submit"], button').not('#check_submit').prop('disabled', true);
                $('#check_submit').click();
            }
        })
    });

</script>

<script>
    (function($) {
       setTimeout(function() {
           $('[name="topicId"]').change();
       }, 1000)
    })(jQuery);

</script>

<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

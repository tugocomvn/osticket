<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col">
                <label for="datetime_main">Main</label>
                <input id="datetime_main" class="form-control"/>
            </div>
            <div class="form-group col">
                <label for="compare_to"><input type="checkbox" id="compare_to" name="compare_to" value="1">
                    Compare to</label>
                <input id="datetime_compare" class="form-control" style="display: none;"/>
            </div>
        </div>
        <button class="btn btn-default">Apply</button>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<script>
    (function($) {
        $('#datetime_main').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            "locale": {
                "format": "DD MMM YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "showCustomRangeLabel": true,
            "alwaysShowCalendars": true,
            "parentEl": "body",
            "opens": "left"
        }, function(start, end, label) {
            //do something, like clearing an input
            main_start = $('#datetime_main').data('daterangepicker').startDate.clone();
            xstart = $('#datetime_main').data('daterangepicker').startDate.clone();
            main_end = $('#datetime_main').data('daterangepicker').endDate.clone();

            $('#datetime_compare').data('daterangepicker')
                .setStartDate(main_start.subtract((main_end - xstart)/1000, 'seconds').format('DD MMM YYYY'));
            $('#datetime_compare').data('daterangepicker')
                .setEndDate(xstart.subtract(1, 'days').format('DD MMM YYYY'));
        });

        $('#datetime_compare').daterangepicker({
            ranges: {
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            "locale": {
                "format": "DD MMM YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "showCustomRangeLabel": true,
            "alwaysShowCalendars": true,
            "parentEl": "body",
            "opens": "left"
        }, function(start, end, label) {
        });

        $('#compare_to').on('change', function() {
            if ($('#compare_to').prop('checked'))
                $('#datetime_compare').show();
            else
                $('#datetime_compare').hide();
        })
    })(jQuery);
</script>
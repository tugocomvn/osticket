<?php

if(!defined('INCLUDE_DIR')) die('!');
/**
 * Created by PhpStorm.
 * User: cosmos
 * Date: 7/20/16
 * Time: 9:09 AM
 * @author: buupham
 * Khi cần download file, thay vì Apache trả file về theo cách thông thường - cần cấu hình trong virtualhost
 * cách này dùng PHP đọc file rồi truyền nội dung file xuống client (gồm cả các header)
 */
class Download
{
    public static function file($path, $name) {
        $file = implode(DIRECTORY_SEPARATOR, [ROOT_DIR, 'public_file', $path, $name]);

        if (file_exists($file) && is_file($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: '.mime_content_type($file));
            header('Content-Disposition: attachment; filename="'.basename($name).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        } else {
            Http::response(404, __('File Not Found'));
        }
        // end - don't do anything here
    }

    public static function verifyPath($path, $name, $hash)
    {
        $calc_hash = hash('sha256', $path.$name.md5($name).$name);
        if (!empty($hash) && !empty($path) && !empty($name) && !strcasecmp($calc_hash, $hash))
            return true;
        return false;
    }
}
<?php
require_once(INCLUDE_DIR . 'class.orm.php');

// City Address
class CityAddressModel extends \VerySimpleModel {
    static $meta = array(
        'table' => CITY_TABLE,
        'pk' => array('id'),
    );
}
class CityAddress extends CityAddressModel{
    public static function getAll(){
        $sql = "SELECT `id`,`name` FROM ". self::$meta['table']. " ORDER BY name";
        return db_query($sql);
    }
}

//District Address
class DistrictAddressModel extends \VerySimpleModel {
    static $meta = array(
        'table' => DISTRICT_TABLE,
        'pk' => array('id'),
    );
}
class DistrictAddress extends DistrictAddressModel{
    public static function getItem($id){
        $sql = "SELECT `id`,`name` FROM ". self::$meta['table']. " WHERE matp=".$id." ORDER BY name ASC";
        return db_query($sql);
    }
}

//Ward Address
class WardAddressModel extends \VerySimpleModel {
    static $meta = array(
        'table' => WARD_TABLE,
        'pk' => array('id'),
    );
}
class WardAddress extends WardAddressModel{
    public static function getItem($id){
        $sql = "SELECT `id`,`name` FROM ". self::$meta['table']. " WHERE maqh=".$id." ORDER BY name ASC";
        return db_query($sql);
    }
}

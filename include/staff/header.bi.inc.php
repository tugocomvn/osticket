<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>/node_modules/chart.js/dist/Chart.min.css">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap4.min.css">
    <title><?php echo $ost->getPageTitle() ?> | <?php echo $cfg->getTitle() ?></title>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/node_modules/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>/vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include INCLUDE_DIR.'bi.menu.aside.data.php' ?>
<?php include  STAFFINC_DIR.'bi.navbar.inc.php' ?>
<?php include  STAFFINC_DIR.'bi.aside.inc.php' ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<?php include STAFFINC_DIR.'bi.pageheader.inc.php' ?>

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
<?php 
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canCreateReceipt()) exit('Access Denied');
?>
<style>
.time_picker {
    background-color: white;
    display: inline-flex;
    border: 1px solid #ccc;
    color: #555;
}                    
.time_picker input {
    border: none;
    color: #555;
    text-align: center;
    width: 60px;
    height :20px;
}

.receipt_form {
    background-color: #ffffff;
}

.input-field{
    width: 250px;
    margin: 3px;
}

.border-bottom td {
    border-bottom: 1px solid #eee;
    padding-bottom: 1em;
}

.border-bottom+tr td {
    padding-top: 1em;
}

textarea.input-field {
    width: 90%;
    height: 4em;
    resize: none;
}

.form_receipt {
    margin: auto;
}
</style>    
<h2><?php echo __('Receipt Online');?></h2>
<div>
    <h1 style="text-align: center; margin-bottom: 10px;">Phiếu Xác Nhận Dịch Vụ</h1>
    <form action="<?php echo $cfg->getUrl() ?>scp/receipt.php" method="POST">
        <?php csrf_token(); ?>
            <div>
                <table class="form_receipt">
                    <tr>
                        <td><input type="hidden" id="receipt_code" name="receipt_code" value="<?php echo $newReceipt ?>"></td>
                        <td><input type="hidden" id="ticket_id" name="ticket_id" value=""></td>
                        <td><input type="hidden" id="booking_type_id" name="booking_type_id" value=""></td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Mã booking:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" name="booking_code"
                                value="<?php if(isset($_REQUEST['booking_code']) && trim($_REQUEST['booking_code']))
                                    echo trim($_REQUEST['booking_code']) ?>" placeholder="TG123-#######" required>
                            <font class="error">*</font>
                        </td>
                        <tr>
                            <td></td>
                            <td>
                                <em>Điền đầy đủ, ví dụ TG123-1234567</em>
                            </td>
                        </tr>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Lộ trình:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" name="tour_name"
                                value="<?php if(isset($_REQUEST['tour_name']) && trim($_REQUEST['tour_name']))
                                    echo trim($_REQUEST['tour_name']) ?>">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Ngày khởi hành:'); ?></b></td>
                        <td>
                            <input class="dp input-field" name="departure_date"
                                value="<?php if(strtotime($_REQUEST['departure_date']))
                                    echo date('d/m/Y', strtotime($_REQUEST['departure_date'])) ?>" autocomplete="OFF">    
                            <font class="error">*</font>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><b><?php echo __('Ngày tập trung:'); ?></td>
                        <td>
                            <input type="text" class="dp input-field"
                                   id="time_to_get_in" name="time_to_get_in"
                        value="<?php if(strtotime($_REQUEST['time_to_get_in']))
                            echo date('d/m/Y', strtotime($_REQUEST['time_to_get_in'])) ?>"
                                   autocomplete="OFF">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Địa điểm:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="place_to_get_in" name="place_to_get_in"
                                    value="<?php if (isset($_REQUEST['place_to_get_in']) && trim($_REQUEST['place_to_get_in']))
                                        echo trim($_REQUEST['place_to_get_in']) ?>">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Giá tiền 1 khách (NL):'); ?></b></td>
                        <td>
                            <input type="number" class="input-field" id="unit_price_nl" name="unit_price_nl"
                                    value="<?php if(isset($_REQUEST['unit_price_nl']) && trim($_REQUEST['unit_price_nl']))
                                        echo trim($_REQUEST['unit_price_nl']) ?>" min="1000" required>
                            <span id="format_unit_price_nl"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                        $('#unit_price_nl').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Số lượng (NL):'); ?></b></td>
                        <td>
                            <input style="width: 100px;" type="number" class="input-field" id="quantity_nl" min="0" max="9999"
                                size="10" name="quantity_nl"
                                value="<?php if(isset($_REQUEST['quantity_nl']) && trim($_REQUEST['quantity_nl']))
                                    echo trim($_REQUEST['quantity_nl']) ?>">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td>Giá tiền 1 khách (TE):</td>
                        <td>
                            <input type="number" class="input-field" id="unit_price_te" min="0" name="unit_price_te"
                                value="<?php if(isset($_REQUEST['unit_price_te']) && trim($_REQUEST['unit_price_te']))
                                    echo trim($_REQUEST['unit_price_te']) ?>">
                            <span id="format_unit_price_te"></span>
                        </td>
                        <script>
                        $('#unit_price_te').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    <tr class="border-bottom">
                        <td>Số lượng (TE):</td>
                        <td>
                            <input style="width: 100px;" type="number" class="input-field"
                                    min="0" size="10" max="9999" id="quantity_te" name="quantity_te"
                                    value="<?php if(isset($_REQUEST['quantity_te']) && trim($_REQUEST['quantity_te']))
                                    echo trim($_REQUEST['quantity_te']) ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Phí phụ thu:</td>
                        <td>
                            <input type="number" class="input-field" name="surcharge" min="0" id="surcharge"
                                value="<?php if(isset($_REQUEST['surcharge']) && trim($_REQUEST['surcharge']))
                            echo trim($_REQUEST['surcharge']) ?>">
                            <span id="format_surcharge"></span>
                        </td>
                        <script>
                        $('#surcharge').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    </tr>
                    <tr class="border-bottom">
                        <td>Nội dung phụ thu:</td>
                        <td>
                            <textarea type="text" class="input-field" id="surcharge_note"
                                    name="surcharge_note"><?php
                                if(isset($_REQUEST['surcharge_note']) && trim($_REQUEST['surcharge_note']))
                                    echo trim($_REQUEST['surcharge_note'])
                                ?></textarea>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Tổng số tiền khách cần đóng:'); ?></b></td>
                        <td>
                            <input type="number" class="input-field"
                                   min="1000" id="amount_to_be_received"
                                   name="amount_to_be_received"
                                value="<?php if(isset($_REQUEST['amount_to_be_received']) && trim($_REQUEST['amount_to_be_received']))
                                    echo trim($_REQUEST['amount_to_be_received']) ?>" minlength="4" required>
                            <span id="format_amount_to_be_received"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                        $('#amount_to_be_received').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    </tr>
                    <tr>
                        <td><b>Hình thức thanh toán:</b></td>
                        <td>
                        <select name="payment_method" id="payment_method">
                            <?php foreach(PaymentMethod::caseTitleName() as $_id => $_name): ?>
                                    <option value="<?php echo $_id ?>"
                                    <?php if (isset($_REQUEST['payment_method']) && $_REQUEST['payment_method'] == $_id)
                                    echo "selected" ?>
                                    >
                                    <?php echo $_name ?></option>
                                <?php endforeach; ?>
                        </select>
                        <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="bank_transfer_note_" style="display: none;">
                        <td>Ghi chú: </td>
                        <td>
                            <textarea class="input-field" id="bank_transfer_note" name="bank_transfer_note"
                                name="bank_transfer_note" placeholder="Nhập đầy đủ thông tin chuyển khoản bao gồm cả nội dung tin nhắn..."><?php if(isset($_REQUEST['bank_transfer_note']) && trim($_REQUEST['bank_transfer_note']))
                                    echo trim($_REQUEST['bank_transfer_note'])
                            ?></textarea><font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Số tiền khách đóng:'); ?></b></td>
                        <td>
                            <input type="number" class="input-field" min="1000" id="amount_received" name="amount_received"
                                value="<?php if(isset($_REQUEST['amount_received']) && trim($_REQUEST['amount_received']))
                                    echo trim($_REQUEST['amount_received']) ?>" minlength="4" required>
                            <span id="format_amount_received"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                        $('#amount_received').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    </tr>
                    <tr>
                        <td>Số tiền còn lại:</td>
                        <td>
                            <input type="number" class="input-field" min="0" id="balance_due" minlength="4" name="balance_due"
                                value="<?php if(isset($_REQUEST['balance_due']) && trim($_REQUEST['balance_due']))
                                    echo trim($_REQUEST['balance_due']) ?>">
                            <span id="format_balance_due"></span>
                        </td>
                        <script>
                        $('#balance_due').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                        </script>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Ngày hoàn tất thanh toán số tiền còn lại:'); ?></b></td>
                        <td>
                            <select class="input-field due" name="due" style="height:30px" required>
                                <?php foreach(DueStatus::caseTitleName() as $_id => $_name): ?>
                                    <option value="<?php echo $_id ?>"
                                    <?php if (isset($_REQUEST['due']) && $_REQUEST['due'] == $_id)
                                    echo "selected" ?>
                                    >
                                    <?php echo $_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="due_date" style="display: none">
                            <input style="width:100px;" class="dp input-field" id="due_date" size=12 name="due_date"
                                value="<?php if(strtotime($_REQUEST['due_date']))
                                    echo date('d/m/Y', strtotime($_REQUEST['due_date'])) ?>"
                                autocomplete="OFF">
                        </td>
                        <td class="due_date_text" style="display: none;">
                            <textarea class="input-field" id="due_date_text" name="due_date_text"
                                name="due_date_text"><?php if(isset($_REQUEST['due_date_text']) && trim($_REQUEST['due_date_text']))
                                    echo trim($_REQUEST['due_date_text'])
                            ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Tên khách đại diện:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="customer_name" name="customer_name"
                                value="<?php if(isset($_REQUEST['customer_name']) && trim($_REQUEST['customer_name']))
                                    echo trim($_REQUEST['customer_name']) ?>" required>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Số điện thoại khách:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="customer_phone_number" name="customer_phone_number"
                                value="<?php if(isset($_REQUEST['customer_phone_number']) && trim($_REQUEST['customer_phone_number']))
                                    echo trim($_REQUEST['customer_phone_number']) ?>" required>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <em>Ghi chú: Sử dụng số điện thoại khách đại diện để nhận phiếu thu online</em>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                    <tr class="border-bottom">
                        <td>Ghi chú cho khách:</td>
                        <td>
                            <textarea class="input-field" id="public_note" name="public_note"
                                name="public_note"><?php if(isset($_REQUEST['public_note']) && trim($_REQUEST['public_note']))
                                    echo trim($_REQUEST['public_note'])
                            ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Tên nhân viên kinh doanh:');?></b></td>
                        <td>
                            <select name='staff_id' class="staff_id" id="staff_id">
                               <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                                <?php while ($staff_list && ($row = db_fetch_array($staff_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'], ($thisstaff->getId() == $row['staff_id']?'selected="selected"':''), $row['name']);
                                }?>
                            </select>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Số điện thoại nhân viên kinh doanh:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="staff_phone_number" name="staff_phone_number"
                                value="<?php if(isset($_REQUEST['staff_phone_number']) && trim($_REQUEST['staff_phone_number'])){
                                    echo trim($_REQUEST['staff_phone_number']);
                                }else {
                                    echo Staff::lookup($thisstaff->getId())->getMobilePhone();
                                }
                                ?>">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                    <td><b>Nhân viên thu tiền:</b></td>
                        <td>
                            <select name='staff_cashier_id' class="staff_id" id="staff_cashier_id">
                               <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                                <?php while ($finance_staff_list && ($row = db_fetch_array($finance_staff_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'], (!empty($cookie_staff_cashier_id) || $_REQUEST['staff_cashier_id'] == $row['staff_id']?'selected="selected"':''), $row['name']);
                                }?>
                            </select>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Số điện thoại nhân viên thu tiền:</b></td>
                        <td>
                            <input type="text" class="input-field" id="staff_cashier_phone_number" name="staff_cashier_phone_number"
                                value="<?php if(isset($_REQUEST['staff_cashier_phone_number']) && trim($_REQUEST['staff_cashier_phone_number'])){
                                    echo trim($_REQUEST['staff_cashier_phone_number']); 
                                }elseif(!empty($cookie_staff_cashier_phone_number)){
                                    echo trim($cookie_staff_cashier_phone_number);
                                }                                                                       
                                    ?>">
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td>Ghi chú nội bộ:</td>
                        <td>
                            <textarea class="input-field" id="internal_note"
                                      name="internal_note"><?php if(isset($_REQUEST['internal_note']) && trim($_REQUEST['internal_note']))
                                          echo trim($_REQUEST['internal_note'])
                                ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Thời gian cần hoàn tất hồ sơ:</td>
                        <td>
                            <input type="text" class="dp input-field" id="document_due" name="document_due"
                                value="<?php if(strtotime($_REQUEST['document_due']))
                                    echo date('d/m/Y', strtotime($_REQUEST['document_due'])) ?>"
                                autocomplete="OFF">
                        </td>
                    </tr>
                    <tr>
                        <td>Thời gian DỰ KIẾN có kết quả visa:</td>
                        <td>
                            <input type="text" class="dp input-field" id="visa_result_estimate_due" name="visa_result_estimate_due"
                                value="<?php if(strtotime($_REQUEST['visa_result_estimate_due']))
                                    echo date('d/m/Y', strtotime($_REQUEST['visa_result_estimate_due'])); ?>" 
                                autocomplete="OFF">
                        </td>
                    </tr>
                </table>
                <p class="centered">
                    <button class="btn_sm btn-success" type="submit" name="action" value="save">Save</button>
                    <input type="reset" class="btn_sm btn-default"
                           value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
                </p>
            <script>
                function reset_form(event) {
                    console.log(event);
                    form = $(event).parents('form');
                    form.find('input[type="text"],input[type="number"],input[type="time"], select, .hasDatepicker, textarea').val('');
                    form.submit();
                }
            </script>
    </form>    
</div>
<script>
    (function($){
        $('.staff_id').select2(); 
        $("#payment_method").change(function() {
            if ($(this).val() != 1) {
                $('.bank_transfer_note_').show();
            }else{
                $('.bank_transfer_note_').hide();
                $('.bank_transfer_note').val("");
            }
        }).change();
        //choose 2 option due_date/due_date_text
        $( ".due" ).change(function() {
            if($(this).val() === 'due_date') {
                $('.due_date').show();
                $('.due_date_text').hide();
                $("#due_date_text").val("");
            }else{
                $('.due_date_text').show();
                $('.due_date').hide();
                $("#due_date").val("");
            }
        }).change();
        function readJsonReceipt() {
            var booking_code = $("input[name=booking_code]").val().trim();
            $.get("<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/booking", { booking_code : booking_code } )
            .done(function( data ) {  
                var data = data.data;
                $('input[name="tour_name"]').val(data.tour_name);
                $('input[name="time_to_get_in"]').val(data.time_to_get_in);
                $('input[name="departure_date"]').val(data.departure_date);
                $('input[name="place_to_get_in"]').val(data.place_to_get_in);
                $('input[name="customer_name"]').val(data.customer_name);
                $('input[name="customer_phone_number"]').val(data.customer_phone_number);
                $('input[name="quantity_nl"]').val(data.quantity_nl);
                $('input[name="unit_price_nl"]').val(data.unit_price_nl);
                $('input[name="quantity_te"]').val(data.quantity_te);
                $('input[name="unit_price_te"]').val(data.unit_price_te);
                $('input[name="surcharge"]').val(data.surcharge);
                $('#surcharge_note').val(data.surcharge_note);
                $('input[name="amount_to_be_received"]').val(data.total_retail_price);
                $('#internal_note').val(data.internal_notes);
                $('input[name="ticket_id"]').val(data.ticket_id);
                $('input[name="booking_type_id"]').val(data.booking_type_id);
                money_format();
            });
        }
        $('input[name="booking_code"]').change(function() {
            readJsonReceipt(this);
        });
        $('#staff_cashier_id').on('change', function() {
            $.ajax({
                url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/load',
                data: { "value": $("#staff_cashier_id").val() },
                type : 'POST',
                dataType : 'json',
                success: function(res){
                    if(res.result === 1){
                        $('#staff_cashier_phone_number').val(res.phone_number);      
                    }else{
                        $('#staff_cashier_phone_number').val('');  
                    }                                  
                }
            });
        });
        $('#staff_id').on('change', function() {
            $.ajax({
                url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/load',
                data: { "value": $("#staff_id").val() },
                type : 'POST',
                dataType : 'json',
                success: function(res){
                    if(res.result === 1){
                        $('#staff_phone_number').val(res.phone_number);      
                    }else{
                        $('#staff_phone_number').val('');  
                    }                                  
                }
            });
        });
    })(jQuery);  
    function money_format() {
        $('#format_unit_price_nl').text(parseInt($('#unit_price_nl').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_unit_price_te').text(parseInt($('#unit_price_te').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_surcharge').text(parseInt($('#surcharge').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_amount_to_be_received').text(parseInt($('#amount_to_be_received').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_amount_received').text(parseInt($('#amount_received').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_balance_due').text(parseInt($('#balance_due').val()).formatMoney(0, '.', ',')+'đ');
    }
    money_format();
    <?php
        if($_GET['booking_code'] && $_GET['from_view'] = 'booking'):
        $booking = Booking::lookup(['booking_code' => $_GET['booking_code']]);
        $ticket_id = Booking::getIdFromBookingCode($booking->booking_code);
        if(isset($ticket_id)) {
            $data_booking_view = Booking::get_booking_view($ticket_id);
        }
        $phone = Staff::lookup($booking->staff_id)->getMobilePhone();
        if($booking && $data_booking_view) :
            ?>
            (function($) {
                $('input[name="tour_name"]').val('<?php echo trim(_String::json_decode($booking->booking_type)) ?>');
                $('input[name="time_to_get_in"]').val('<?php echo trim(date('d/m/Y',($booking->dept_date))) ?>');
                $('input[name="booking_type_id"]').val('<?php echo ($booking->booking_type_id) ?>');
                $('input[name="ticket_id"]').val('<?php echo ($booking->ticket_id) ?>');
                $('input[name="place_to_get_in"]').val('<?php echo (PLACE_TO_GET_IN) ?>');
                $('input[name="departure_date"]').val('<?php echo (isset($booking->dept_date))?date('d/m/Y',$booking->dept_date):NULL ?>');
                $('input[name="unit_price_nl"]').val('<?php if(!empty($booking->retailprice1)) echo $booking->retailprice1 ?>');
                $('input[name="quantity_nl"]').val('<?php if(!empty($booking->quantity1)) echo $booking->quantity1 ?>');
                $('input[name="unit_price_te"]').val('<?php if(!empty($booking->retailprice2)) echo $booking->retailprice2 ?>');
                $('input[name="quantity_te"]').val('<?php if(!empty($booking->quantity2)) echo $booking->quantity2 ?>');
                $('input[name="surcharge"]').val('<?php if(!empty($booking->retailprice3)) echo abs($booking->retailprice3) ?>');
                $('#surcharge_note').val('<?php if(!empty($booking->note3)) echo preg_replace("/\r|\n/", "",$booking->note3) ?>');
                $('input[name="surcharge"]').val('<?php if(!empty($booking->retailprice3)) echo abs($booking->retailprice3) ?>');
                $("#internal_note").val('<?php if(!empty($booking->note)) echo preg_replace("/\r|\n/", "",$booking->note) ?>');
                $('input[name="total_retail_price"]').val('<?php if(!empty($data_booking_view['total_retail_price'])) echo $data_booking_view['total_retail_price'] ?>');
                $('input[name="amount_to_be_received"]').val('<?php if(!empty($data_booking_view['total_retail_price'])) echo $data_booking_view['total_retail_price'] ?>');
                $('input[name="customer_name"]').val('<?php if(!empty($booking->customer)) echo preg_replace( "/\r|\n/", " ", $booking->customer ) ?>');
                $('input[name="customer_phone_number"]').val('<?php if(!empty($booking->phone_number)) echo _String::formatPhoneNumber($booking->phone_number) ?>');
                $("#public_note").val('<?php
                    if(isset($booking->discounttype1) && (int)$booking->discountamount1)
                        echo 'Giảm giá 1: '.number_format($booking->discountamount1).'đ'.' ('. $booking->discounttype1 . ') \\n';
                    if(isset($booking->discounttype2) && (int)$booking->discountamount2)
                        echo 'Giảm giá 2: '.number_format($booking->discountamount2).'đ'.' ('. $booking->discounttype2 . ') \\n';
                    if(isset($booking->discounttype3) && (int)$booking->discountamount3)
                        echo 'Giảm giá 3: '.number_format($booking->discountamount3).'đ'.' ('. $booking->discounttype3 . ') \\n';
                    ?>');
                money_format();
            })(jQuery);
        <?php endif;?>
    <?php endif;?>     
</script>
CREATE TABLE IF NOT EXISTS `fnd_flight` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_code` VARCHAR(10) NULL,
  `airline_id` INT NULL,
  `airport_code_from` VARCHAR(6) NULL,
  `airport_code_to` VARCHAR(6) NULL,
  `departure_at` DATETIME NULL,
  `arrival_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  `updated_at` DATETIME NULL,
  `updated_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_code` ASC),
  INDEX `index3` (`airline_id` ASC),
  INDEX `index4` (`airport_code_from` ASC),
  INDEX `index5` (`airport_code_to` ASC),
  INDEX `index6` (`departure_at` ASC),
  INDEX `index7` (`arrival_at` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_ticket` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_ticket_code` VARCHAR(16) NULL,
  `net_price` DECIMAL(12,0) NULL,
  `retail_price` DECIMAL(12,0) NULL,
  `total_quantity` MEDIUMINT(3) NULL,
  `adult_quantity` MEDIUMINT(3) NULL,
  `children_quantity` MEDIUMINT(3) NULL,
  `infant_quantity` MEDIUMINT(3) NULL,
  `flight_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_ticket_code` ASC),
  INDEX `index3` (`net_price` ASC),
  INDEX `index4` (`retail_price` ASC),
  INDEX `index5` (`total_quantity` ASC),
  INDEX `index6` (`adult_quantity` ASC),
  INDEX `index7` (`children_quantity` ASC),
  INDEX `index8` (`infant_quantity` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_ticket_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_ticket_id` INT NULL,
  `content` LONGTEXT NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_ticket_id` ASC),
  INDEX `index3` (`created_at` ASC),
  INDEX `index4` (`created_by` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fnd_flight_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_id` INT NULL,
  `content` LONGTEXT NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_id` ASC),
  INDEX `index3` (`created_at` ASC),
  INDEX `index4` (`created_by` ASC))
ENGINE = InnoDB;
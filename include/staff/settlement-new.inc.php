<?php
global $cfg;
?>

<style>
    form textarea {
        width: 80%;
        height: 3em;
        resize: none;
    }
</style>
<h2>Settlement <small>Add New</small></h2>
<p><a href="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php" class="btn_sm btn-default">Back</a></p>
<form action="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php" method="post">
    <input type="hidden" name="action" value="new">
    <?php csrf_token() ?>
    <table width="50%">
        <tr>
            <td><label for="">Name</label></td>
            <td>
                <input type="text" name="name" maxlength="64" class="input-field" value="<?php
                if (isset($_POST['name']) && $_POST['name']) echo trim($_POST['name']) ?>">
                <span class="error"><?php if (isset($errors['name']) && $errors['name']) echo $errors['name'] ?></span>
            </td>
        <tr>
            <td><label for="">Default Quantity</label></td>
            <td>
                <input type="number" name="default_quantity" class="input-field" value="<?php
                if (isset($_POST['default_quantity']) && $_POST['default_quantity']) echo trim($_POST['default_quantity']) ?>">
                <span class="error"><?php if (isset($errors['default_quantity']) && $errors['default_quantity']) echo $errors['default_quantity'] ?></span>
            </td>
        </tr>
        <tr>
            <td><label for="">Default Value</label></td>
            <td>
                <input type="text" name="default_value" class="input-field" value="<?php
                if (isset($_POST['default_value']) && $_POST['default_value']) echo trim($_POST['default_value']) ?>"> USD per unit
                <span class="error"><?php if (isset($errors['default_value']) && $errors['default_value']) echo $errors['default_value'] ?></span>
            </td>
        </tr>
        <tr>
            <td><label for="">Description</label></td>
            <td colspan="3">
                <textarea class="input-field" maxlength="128" name="description" id="" cols="30" rows="10"><?php
                    if (isset($_POST['description']) && $_POST['description']) echo trim($_POST['description']) ?></textarea>
                <span class="error"><?php if (isset($errors['description']) && $errors['description']) echo $errors['description'] ?></span>
            </td>
        </tr>
        <tr>
            <td><label for="">Status</label></td>
            <td><input type="checkbox" name="status" class="input-field" value="1" <?php
                if (isset($_POST['status']) && $_POST['status']) echo 'checked'?>> check = display</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a href="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php" class="btn_sm btn-default">Cancel</a>
                <button class="btn_sm btn-primary">Save</button>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>
<?php
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-php-quickstart.json
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS
)));

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if(!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * @param $service Google_Service_Sheets
 * @param $spreadsheetId (GOOGLE_SHEET_ID_CALL_LOG)
 * @param $type: inbound | outbound
 */
function updateCallLogData($service, $spreadsheetId, $type, $detail = false) {
    if (!in_array($type, ['inbound', 'outbound'])) {
        echo date("Y-m-d H:i:s")." - Wrong direction type: $type.\r\n";
    }

    $sheetname = $type . ($detail ? '-detail' : '');
    echo date("Y-m-d H:i:s")." - Start getting $sheetname data ------------------\r\n";
    //
    echo  date("Y-m-d H:i:s")." - Getting the cell address containing the latest date in the sheet\r\n";
    $range = $sheetname.'!$I$1';
    $response = $service->spreadsheets_values->get($spreadsheetId, $range);
    $values = $response->getValues();
    echo  date("Y-m-d H:i:s")." - DONE Getting the cell address containing the latest date in the sheet\r\n";

    //
    $cell = $date = $date_str = null;

    if (count($values) == 0) {
        print "No data found.\n";
    } else {
        print "Cell Address: ";
        foreach ($values as $row) {
            // Print columns A and E, which correspond to indices 0 and 4.
            printf("%s\n", $row[0]);
            $cell = $row[0];
        }
    }

    if ($cell && $cell !== '#N/A') {
        echo date("Y-m-d H:i:s") . " - Getting latest date from the above address\r\n";
        $range = $sheetname.'!' . $cell;
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();
        echo date("Y-m-d H:i:s") . " - DONE Getting latest date from the above address\r\n";

        if (count($values) == 0) {
            print "No data found.\n";
        } else {
            print "Date: ";
            foreach ($values as $row) {
                // Print columns A and E, which correspond to indices 0 and 4.
                printf("%s\n", $row[0]);
                $date = $row[0];
            }
        }
    }

    // format date
    $sql_format = $detail ? 'Y-m-d H:i' : 'Y-m-d';
    if ($date) {
        $google_sheet_format = $detail ? 'm/d/Y H:i' : 'm/d/Y';
        $date_obj = date_create_from_format($google_sheet_format, $date);
        $date_str = $date_obj->format($sql_format);
    }

    echo  date("Y-m-d H:i:s")." - Query data\r\n";

    $sql_date_format = '%m/%d/%Y';
    if ($detail) $sql_date_format = '%m/%d/%Y %H:%i';

    $sql = sprintf(
        "SELECT DATE_FORMAT(c.starttime, %s) as date,
      COUNT(c.calluuid) as total,
      SUM(IF(c.disposition LIKE 'NO ANSWER', 1, 0)) AS no_answer,
      SUM(IF(c.disposition LIKE 'ANSWERED', 1, 0)) AS answered,
      SUM(IF(c.disposition LIKE 'FAILED', 1, 0)) AS failed,
      SUM(IF(c.disposition LIKE 'BUSY', 1, 0)) AS busy
    FROM %s c
      WHERE DATE(c.starttime) >= '2016-01-01'
        AND c.direction LIKE %s
        %s
    GROUP BY DATE(c.starttime) %s
    ORDER BY  DATE(c.starttime) ASC"
        , db_input($sql_date_format)
        , CALL_LOG_TABLE
        , db_input($type)
        , $date_str
            ? sprintf(
                " AND c.starttime >= %s "
                , db_input($date_str)
            )
            : ' '
        , $detail ? " , HOUR(c.starttime), MINUTE(c.starttime) " : " "
    );
    echo $sql."\r\n";
    $res = db_query($sql);
    if ($res) {
        $values = [];

        while (($row = db_fetch_array($res))) {
            $values[] = [
                $row['date'],
                $row['total'],
                $row['answered'],
                $row['no_answer'],
                $row['busy'],
                $row['failed'],
            ];
        }

        echo  date("Y-m-d H:i:s")." - Pushing data into the sheet\r\n";

        if (!$cell || $cell === '#N/A')
            $range = $sheetname.'!$A$2';
        else {
            $range = $sheetname.'!'.$cell;
        }

        $body = new Google_Service_Sheets_ValueRange(array(
            'values' => $values
        ));
        $params = array(
            'valueInputOption' => "USER_ENTERED"
        );
        $result = $service->spreadsheets_values->update($spreadsheetId, $range,
            $body, $params);
        echo  date("Y-m-d H:i:s")." - DONE Pushing data into the sheet\r\n";
    }

    echo date("Y-m-d H:i:s")." - DONE getting $sheetname data ------------------\r\n";
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);
$spreadsheetId = GOOGLE_SHEET_ID_CALL_LOG;
updateCallLogData($service, $spreadsheetId, 'inbound');
updateCallLogData($service, $spreadsheetId, 'outbound');
updateCallLogData($service, $spreadsheetId, 'inbound', true);
updateCallLogData($service, $spreadsheetId, 'outbound', true);

<?php
if(!file_exists(__DIR__.'/../../main.inc.php')) die('Fatal error... get technical support');

require_once(__DIR__.'/../../main.inc.php');
$res = null;
$result = [];
$errors = [];

if ( ! ( isset($_REQUEST['param5']) && $_REQUEST['param5']) )
    die('fuck you');

if ( ! ( isset($_REQUEST['param7']) && $_REQUEST['param7']) )
    die('fuck you');

if ($_REQUEST['param5'].$_REQUEST['param7'] !== 'VN71BfFVgWnyq52M2tnf7ZQZ3k2Hw54nJJd5es4S4f64x6u59PxEMxMy594SbEMS')
    die('fuck you');

// kiếm tra PHP
try {
    if ( ! ( isset($_REQUEST['param1']) && isset($_REQUEST['param2']) ) )
        throw new Exception("Missing params");

    $_REQUEST['param1'] = trim($_REQUEST['param1']);
    $_REQUEST['param2'] = trim($_REQUEST['param2']);

    $result['params'] = md5(implode('_', [$_REQUEST['param1'], $_REQUEST['param2']]));
} catch (Exception $e) {
    $errors['params'] = $e->getMessage();
}

// kiếm tra DB NOW()
try {
    $res = db_query("SELECT NOW() as time");

    if (!$res) throw new Exception("DB error, cannot fetch NOW() from MySQL");
    $row = db_fetch_array($res);
    if (isset($row['time'])) $result['time'] = $row['time'];
} catch (Exception $e) {
    $errors['time'] = $e->getMessage();
}

// kiếm tra số ticket
try {
    $res = db_query("SELECT COUNT(*) as total FROM ".TICKET_TABLE);

    if (!$res) throw new Exception("DB error, cannot count total ticket from MySQL");
    $row = db_fetch_array($res);
    if (isset($row['total'])) $result['total'] = $row['total'];
} catch (Exception $e) {
    $errors['total'] = $e->getMessage();
}

header('Content-Type: application/json;charset=utf-8');
echo json_encode([
    'result' => $result,
    'errors' => $errors,
]);
exit;



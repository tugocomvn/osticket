CREATE TABLE IF NOT EXISTS `tugo_osticket_customer_age` (
             `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
             `date` VARCHAR(8) NULL COMMENT 'YYYYMMDD',
             `day_of_month` VARCHAR(2) NULL COMMENT 'DD',
             `day_of_week` VARCHAR(1) NULL COMMENT 'D DAY_OF_WEEK',
             `week_of_the_year` VARCHAR(6) NULL COMMENT 'WEEK',
             `week_of_year` VARCHAR(2) NULL COMMENT 'YEAR_WEEK',
             `month_of_the_year` VARCHAR(2) NULL COMMENT 'MONTH',
             `month_of_year` VARCHAR(6) NULL COMMENT 'YEAR_MONTH',
             `year` VARCHAR(4) NULL COMMENT 'YEAR',
             `day_of_week_name` VARCHAR(10) NULL COMMENT 'DAY_OF_WEEK_NAME',
             `age_pax` INT UNSIGNED NULL,
             `amount_pax` INT UNSIGNED NULL,
             PRIMARY KEY (`id`),
             UNIQUE INDEX `index2` (`day_of_week_name` ASC, `year` ASC, `month_of_year` ASC, `month_of_the_year` ASC, `week_of_year` ASC, `week_of_the_year` ASC, `day_of_week` ASC, `day_of_month` ASC, `date` ASC, `age_pax` ASC)
);

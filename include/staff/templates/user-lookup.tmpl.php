<div id="the-lookup-form">
<h3><?php echo $info['title']; ?></h3>
<b><a class="close" href="#"><i class="icon-remove-circle"></i></a></b>
<hr/>
<?php
if (!isset($info['lookup']) || $info['lookup'] !== false) { ?>
<div><p id="msg_info"><i class="icon-info-sign"></i>&nbsp; <?php echo __(
'Search existing guests or add a new guest.'
); ?></p></div>
<div style="margin-bottom:10px;">
    <input type="text" class="search-input" style="width:100%;"
    placeholder="<?php echo __('Search by email, phone or name'); ?>" id="user-search"
    autocorrect="off" autocomplete="off"/>
</div>
<?php
}

if ($info['error']) {
    echo sprintf('<p id="msg_error">%s</p>', $info['error']);
} elseif ($info['warn']) {
    echo sprintf('<p id="msg_warning">%s</p>', $info['warn']);
} elseif ($info['msg']) {
    echo sprintf('<p id="msg_notice">%s</p>', $info['msg']);
} ?>
<div id="selected-user-info" style="display:<?php echo $user ? 'block' :'none'; ?>;margin:5px;">
<form method="post" class="user" action="<?php echo $info['action'] ?  $info['action'] : '#users/lookup'; ?>">
    <input type="hidden" id="user-id" name="id" value="<?php echo $user ? $user->getId() : 0; ?>"/>
    <i class="icon-user icon-4x pull-left icon-border"></i>
    <a class="action-button pull-right btn_sm btn-info" style="overflow:inherit; color:white !important;"
        id="unselect-user"  href="#"><i class="icon-remove"></i>
        <?php echo __('Add New Guest'); ?></a>
<?php if ($user) { ?>
    <div><strong id="user-name"><?php echo Format::htmlchars($user->getName()->getOriginal()); ?></strong></div>
    <div>&lt;<span id="user-email"><?php echo $user->getEmail(); ?></span>&gt;</div>
    <?php
    if ($org=$user->getOrganization()) { ?>
    <div><span id="user-org"><?php echo $org->getName(); ?></span></div>
    <?php
    } ?>
    <table style="margin-top: 1em;">
<?php foreach ($user->getDynamicData() as $entry) { ?>
    <tr><td colspan="2" style="border-bottom: 1px dotted black"><strong><?php
         echo $entry->getForm()->get('title'); ?></strong></td></tr>
<?php foreach ($entry->getAnswers() as $a) { ?>
    <tr style="vertical-align:top"><td style="width:30%;border-bottom: 1px dotted #ccc"><?php echo Format::htmlchars($a->getField()->get('label'));
         ?>:</td>
    <td style="border-bottom: 1px dotted #ccc"><?php echo $a->display(); ?></td>
    </tr>
<?php }
}
?>
</table>
<?php } ?>
    <div class="clear"></div>
    <hr>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" name="cancel" class="close btn_sm btn-default"  value="<?php
            echo __('Cancel'); ?>">
        </span>
        <span class="buttons pull-right">
            <input type="submit" class="btn_sm btn-primary" value="<?php echo __('Continue'); ?>">
        </span>
     </p>
</form>
</div>
<div id="new-user-form" style="display:<?php echo $user ? 'none' :'block'; ?>;">
<form method="post" class="user" action="<?php echo $info['action'] ?: '#users/lookup/form'; ?>">
    <table width="100%" class="fixed">
    <?php
        if(!$form) $form = UserForm::getInstance();
        $form->render(true, __('Create New Guest')); ?>
    </table>
    <hr>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="reset" class="btn_sm btn-default" value="<?php echo __('Reset'); ?>">
            <input type="button" class="close btn_sm btn-danger" name="cancel" class="<?php echo $user ?  'cancel' : 'close' ?>"  value="<?php echo __('Cancel'); ?>">
        </span>
        <span class="buttons pull-right">
            <input type="submit" class="btn_sm btn-primary" value="<?php echo __('Add Guest'); ?>">
        </span>
     </p>
</form>
</div>
<div class="clear"></div>
</div>
<script type="text/javascript">
$(function() {
    var last_req;
    $('#user-search').typeahead({
        source: function (typeahead, query) {
            $('.typeahead-searching').remove();

            if (!query.trim().length || query.trim().length < 3) return false;
            if (phone_number = get_phone_number(query))
                query = phone_number;
            else if (query.search('@') > 0)
                query = format_email_address(query);

            $('#user-search').val(query.trim());
            $('#user-search').before('<span class="typeahead-searching">Đang tìm...</span>');

            if (last_req) {
                window.clearTimeout(last_req);
                last_req = undefined;
                console.log('clearTimeout');
            }
            last_req = setTimeout(function() {
                console.log('setTimeout')
                $.ajax({
                    url: "ajax.php/users<?php
                        echo $info['lookup'] ? "/{$info['lookup']}" : '' ?>?q="+query,
                    dataType: 'json',
                    success: function (data) {
                        $('.typeahead-searching').remove();
                        if (!data.length) {
                            $('#user-search').before('<span class="typeahead-searching">Không tìm thấy kết quả</span>');
                        } else {
                            $('#user-search').before('<span class="typeahead-searching">Có '+data.length+' kết quả</span>');
                        }
                        typeahead.process(data);
                    }
                });
            }, 600);
        },
        onselect: function (obj) {
            $('#the-lookup-form').load(
                '<?php echo $info['onselect']? $info['onselect']: "ajax.php/users/select/"; ?>'+encodeURIComponent(obj.id)
            );
        },
        property: "/bin/true"
    });

    $('a#unselect-user').click( function(e) {
        e.preventDefault();
        $("#msg_error, #msg_notice, #msg_warning").fadeOut();
        $('div#selected-user-info').hide();
        $('div#new-user-form').fadeIn({start: function(){ $('#user-search').focus(); }});
        return false;
     });

    $(document).on('click', 'form.user input.cancel', function (e) {
        e.preventDefault();
        $('div#new-user-form').hide();
        $('div#selected-user-info').fadeIn({start: function(){ $('#user-search').focus(); }});
        return false;
     });

    $("[placeholder='name']").on('blur', function(e) {
        phone_number = $("[placeholder='phone']").val().trim();
        if (phone_number = get_phone_number(phone_number)) {
            $("[placeholder='phone']").val(phone_number);
        }

        if (!$("[placeholder='email']").val().length && $("[placeholder='phone']").val().trim()) {
            $("[placeholder='email']").val(phone_number + '@tugo.com.vn');
        } else {
            $("[placeholder='email']").val( format_email_address($("[placeholder='email']").val()) );
        }
    });

    $("[placeholder='phone']").on('blur', function(e) {
        phone_number = $("[placeholder='phone']").val().trim();
        if (phone_number = get_phone_number(phone_number)) {
            $("[placeholder='phone']").val(phone_number);
        }

        $("[placeholder='email']").val( format_email_address($("[placeholder='email']").val()) );
    });

    $("[placeholder='email']").on('blur', function(e) {
        $("[placeholder='email']").val( format_email_address($("[placeholder='email']").val()) );
    });
});
</script>

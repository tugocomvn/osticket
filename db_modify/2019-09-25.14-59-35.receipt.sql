CREATE TABLE `receipt_log` (
  `id` int AUTO_INCREMENT primary key,
  `receipt_id` int(11) NULL,
  `staff_id` int(11) NULL,
  `content` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `receipt_action_log` (
  `id` int AUTO_INCREMENT primary key,
  `receipt_id` int(11) NULL,
  `staff_id` int(11) NULL,
  `action_id` int(11) NULL,
  `status_id` int(11) NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
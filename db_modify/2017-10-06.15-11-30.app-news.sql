CREATE TABLE api_news
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(128),
  content TEXT,
  user_uuid VARCHAR(16)
);
CREATE INDEX api_news_title_index ON api_news (title);
CREATE INDEX api_news_user_uuid_index ON api_news (user_uuid);

ALTER TABLE api_news ADD created_by INT NULL;
ALTER TABLE api_news ADD updated_by INT NULL;
ALTER TABLE api_news ADD created_at DATETIME NULL;
CREATE INDEX api_news_created_at_index ON api_news (created_at);
ALTER TABLE api_news ADD updated_at DATETIME NULL;
CREATE INDEX api_news_updated_at_index ON api_news (updated_at);
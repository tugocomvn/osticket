<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewPayments()) die('Access Denied');

require_once(INCLUDE_DIR . '../scp/export.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$booking_type_list = TourDestination::loadList($group_des);

//function check booking coode in table booking_tmp
function check_booking($code)
{
    $sql = "SELECT booking_tmp.staff,booking_tmp.booking_code_trim FROM `booking_tmp` WHERE ".$code." 
            ORDER BY booking_tmp.`created` DESC, booking_tmp.ticket_id DESC";
    return db_query($sql);
}

//function convert string array to type ('string1','string2') use IN sql
function convert_IN_condition($arr)
{
    if (empty($arr))
        return "1=0";
    return "booking_tmp.booking_code_trim IN ".'(' . implode(',', $arr) .')';
}

//function arr['booking_code_trim'] = 'nameStaff'
function convert_index_booking($result)
{
    $arr = array();
    while ($result && ($row = db_fetch_array($result)))
    {
        $arr[$row['booking_code_trim']] = $row['staff'];
    }
    return $arr;
}

$res = db_query($sql . " LIMIT ".$pageNav->getStart().",".$pageNav->getLimit());

if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '.$title;
else:
    $showing=__('No payment found!');
endif;
?>

<?php //check và get booking in table booking_tmp
$code_booking = array();
while($res && $row = db_fetch_array($res))
{
    if(!empty($row['booking_code'])) {
        array_push($code_booking,"'".$row['booking_code_trim']."'");
    }
}
//convert string array use IN sql
$code_booking = convert_IN_condition($code_booking);

//get code in table booking_tmp
$result_check = check_booking($code_booking); //return type:db_fetch

if (db_num_rows($result_check) > 0)
{
    $result_check = convert_index_booking($result_check); //return type:array
}
else {
    $result_check = array(); //return array empty
}

$res->data_seek(0);
?>

<?php $flash_hash = FlashMsg::get('payment_hash'); ?>
<?php if($flash_hash): ?>
<div>
    <h1>Hash: <span style="color:red;"><?php echo substr($flash_hash, 0, 6) ?></span></h1>
    <p><small>Ghi chú: số 0 cao, chữ o thấp</small></p>
</div>
<?php endif; ?>


<h2><?php echo __('Payment List');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>
<div id='filter' >
 <form action="io.php" method="get">
    <div style="padding-left:2px;">
        <table>
            <tr>
                <td><b><?php echo __('Date Span'); ?></b> <?php echo __('Between'); ?></i></td>
                <td>
                    <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
                    <input class="dp input-field" id="ed" size=15  name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>
                </td>
                <td><?php echo __('Mã Booking'); ?>:</td>
                <td><input type="text" class="input-field" name="booking_code" value="<?php echo $_REQUEST['booking_code'] ?>"></td>
                <td><?php echo __('Mã Receipt'); ?>:</td>
                <td><input type="text" class="input-field" name="receipt_code" value="<?php echo $_REQUEST['receipt_code'] ?>"></td>
            </tr>
            <tr>
                <td><?php echo __('Thu/Chi'); ?>:</td>
                <td>
                    <select name='type' class="input-field">
                        <option value="" selected><?php echo __('-- All --');?></option>
                        <option value="in" <?php echo ($type=='in')?'selected="selected"':''; ?>><?php echo __('Thu');?></option>
                        <option value="out" <?php echo ($type=='out')?'selected="selected"':''; ?>><?php echo __('Chi');?></option>
                    </select>
                </td>

                <td>Phương Thức</td>
                <td>
                    <select name="method" class="input-field">
                        <option value=><?php echo __('-- All --') ?></option>
                        <?php
                        $sql = "SELECT DISTINCT `value` as name FROM ".LIST_ITEM_TABLE." li WHERE li.list_id IN (".THU_METHOD_LIST.",".CHI_METHOD_LIST.") ORDER BY name";
                        $res_list = db_query($sql);
                        if ($res_list) {
                            while(($row = db_fetch_array($res_list))) {
                                ?>
                                <option value="<?php echo $row['name'] ?>"
                                    <?php if (isset($_REQUEST['method']) && $_REQUEST['method']==$row['name']) echo 'selected' ?>
                                    ><?php echo $row['name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>

                <td><?php echo __('Số tiền'); ?>:</td>
                <td><input type="text" class="input-field" name="amount" value="<?php echo $_REQUEST['amount'] ?>"></td>
            </tr>
            <tr>
                <td>Loại Chi</td>
                <td>
                    <select name="outcome_type_id" id="">
                        <option value=>-- All --</option>
                        <?php foreach(ListUtil::getList(OUTCOME_TYPE_LIST_ID) as $_id => $_name): ?>
                            <option value="<?php echo $_id ?>" <?php if ( isset($_REQUEST['outcome_type_id']) && $_id == $_REQUEST['outcome_type_id']) echo 'selected'; ?>><?php echo $_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>Tour</td>
                <td>
                    <select name="tour" class="input-field">
                        <option value=>-- All --</option>
                        <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                            <?php foreach($booking_type_list as  $item): ?>
                                <option value="<?php echo $item['id'] ?>"
                                        <?php if (isset($_REQUEST['tour']) && $_REQUEST['tour'] == $item['id']) echo 'selected' ?>
                                ><?php echo $item['name'] ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </td>
                <td>Ghi Chú</td>
                <td><input type="text" class="input-field" name="note" value="<?php echo $_REQUEST['note'] ?>"></td>
            </tr>
            <tr>
                <td>Loại Thu</td>
                <td>
                    <select name="income_type_id" id="">
                        <option value=>-- All --</option>
                        <?php foreach(ListUtil::getList(INCOME_TYPE_LIST_ID) as $_id => $_name): ?>
                            <option value="<?php echo $_id ?>" <?php if ( isset($_REQUEST['income_type_id']) && $_id == $_REQUEST['income_type_id']) echo 'selected'; ?>><?php echo $_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td><?php echo __('Khu vực'); ?>:</td>
                <td>
                    <select name="dept_id" id="dept_id" class="input-field">
                        <option value=>-- All --</option>
                        <?php $depts = Dept::get_cache(); ?>
                        <?php foreach($depts as $_id => $_name): ?>
                            <?php if (strpos(strtolower($_name), 'finance') === false) continue; ?>
                            <option value="<?php echo $_id ?>"
                                <?php if(isset($_REQUEST['dept_id']) && $_id == $_REQUEST['dept_id']) echo "selected" ?>><?php echo $_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td><?php echo __('Agent'); ?></td>
                <td>
                    <select name='agent' class="input-field">
                        <option value><?php echo __('All');?></option>
                        <?php
                        $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                            .' FROM '.STAFF_TABLE.' staff '
                            .' ORDER by name';
                        if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                            while(list($id,$name)=db_fetch_row($agent_res)){
                                $selected=($_REQUEST['agent'] && $name==$_REQUEST['agent'])?'selected="selected"':'';
                                echo sprintf('<option value="%s" %s>%s</option>',$name,$selected,$name);
                            }
                        }
                        ?>
                    </select>
                </td>

            </tr>
            <tr>
                <td colspan="6">
                    <button type="submit" Value="" class="btn_sm btn-primary"><?php echo __('Search');?></button>
                    <button type="reset" Value="" class="btn_sm btn-default" onclick="reset_form(this)"><?php echo __('Reset');?></button>
                    <button type="submit" formaction="/scp/export.php" name="export" value="list" class="btn_sm btn-info">Export</button>
                </td>
            </tr>
        </table>
        <script>
            function reset_form(event) {
                console.log(event);
                form = $(event).parents('form');
                form.find('input[type="text"], select, .hasDatepicker').val('');
                form.submit();
            }
        </script>
    </div>
 </form>
</div>
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <caption>Tổng thu: <?php echo number_format($sum_in); ?> - Tổng chi: <?php echo number_format(-1*$sum_out); ?> - <strong style="color:yellow;">Số dư: <?php echo number_format($sum_in - $sum_out) ?></strong></caption>
    <thead>
        <tr>
            <th><?php echo __('Ticket');?></th>
            <th><?php echo __('Mã Booking');?></th>
            <th><?php echo __('Mã Receipt');?></th>
            <th><?php echo __('Số tiền');?></th>
            <th><?php echo __('Số lượng');?></th>
            <th><?php echo __('Loại');?></th>
            <th><?php echo __('Phương thức');?></th>
            <th><?php echo __('Người thu/chi');?></th>
            <th><?php echo __('Thời gian');?></th>
            <th><?php echo __('Ghi chú');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $total=0;
        if($res):
            $flash_id = FlashMsg::get('payment_id');
            $dept_cache = Dept::get_cache();
            while (($row = db_fetch_array($res))) {
                ?>
                <?php
                $class_name = "";
                if ( $row['ticket_id'] == $flash_id ) {
                    $class_name = "new-line";
                }
                ?>
            <tr id="<?php echo $row['ticket_id']; ?>"
                class="<?php if ($row['amount'] < 0) echo 'type_out' ?> <?php echo $class_name ?>">
                <td>
                    <a class="no-pjax" target="_blank" href="<?php echo $cfg->getUrl(); ?>scp/tickets.php?id=<?php echo $row['ticket_id']; ?>"><?php echo Format::htmlchars($row['number']); ?></a>
                    <br>
                    <?php if (isset($dept_cache[$row['dept_id']])) echo $dept_cache[$row['dept_id']] ?>
                </td>
                <td><a href="<?php echo $cfg->getUrl(); ?>scp/io.php?booking_code=<?php echo $row['booking_code']; ?>"><?php echo $row['booking_code']; ?></a></td>
                <td><?php echo $row['receipt_code']; ?></td>
                <td><?php echo  ( ($row['income_type'] || $row['topic_id']==THU_TOPIC) ? number_format($row['amount']) : number_format(-1 * $row['amount']) ); ?></td>
                <td><?php echo $row['quantity']; ?></td>
                <td><?php echo _String::json_decode($row['income_type'] ?: $row['outcome_type']); ?></td>
                <td><?php echo _String::json_decode($row['method']); ?></td>
                <td><?php echo $row['agent']; ?></td>
                <td><?php echo Format::userdate('d/m/Y', $row['time']); ?></td>
                <td><?php echo $row['note'];
                    //has booking_code, has THU or income_type
                    if ((!empty($row['booking_code'])) && (($row['income_type'] || $row['topic_id']==THU_TOPIC))):
                        //compare name staff
                        if (!empty($result_check[$row['booking_code_trim']]) &&  ($result_check[$row['booking_code_trim']] !== $row['agent'])):?>
                            <p class="label label-danger">Sai nhân viên (<?php echo $result_check[$row['booking_code_trim']];?>)</p>
                        <?php endif; ?>

                        <?php
                        //code booking NOT exist in booking_tmp
                        if (empty($result_check[$row['booking_code_trim']]) ):?>
                            <p class="label label-info">Mã <?php echo $row['booking_code']?> không tồn tại</p>
                        <?php endif; ?>
                    <?php
                    endif; ?>
                </td>

            </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
    <tfoot>
     <tr>
        <td colspan="6">
            <?php if($res && $num){ ?>
            <?php }else{
                echo __('No payment found');
            } ?>
        </td>
     </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>
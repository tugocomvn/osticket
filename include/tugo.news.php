<?php
namespace Tugo;

use FacebookAds\Exception\Exception;

require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'tugo.user.php');

class NewsModel extends \VerySimpleModel {
    static $meta = [
        'table' => NEWS_TABLE,
        'pk' => ['id'],
        'ordering' => ['created_at'],
    ];
}

class News extends NewsModel {
    public static function insert($title, $content, $params) {
        if (!isset($title) || empty(trim($title))) throw new Exception('Title is requied');
        if (!isset($content) || empty(trim($content))) throw new Exception('Content is requied');

        if (!isset($params['user_uuid']) || empty ($params['user_uuid'])) {
            $params['user_uuid'] = null;
            $user = null;

            if (isset($params['phone_number']) && !empty($params['phone_number'])) {
                $user = \Tugo\User::get(['phone_number' => $params['phone_number']]);
            }

            if (isset($params['customer_number']) && !empty($params['customer_number'])) {
                $user = \Tugo\User::get(['customer_number' => $params['customer_number']]);
            }

            if (isset($params['uuid']) && !empty($params['uuid'])) {
                $user = \Tugo\User::get(['uuid' => $params['uuid']]);
            }

            if ($user) $params['user_uuid'] = $user['uuid'];
        }

        if (!isset($params['created_by']) || empty($params['created_by']))
            $params['created_by'] = null;

        $news = self::create(
            [
                'title' => trim($title),
                'content' => trim($content),
                'user_uuid' => $params['user_uuid'],
                'created_by' => $params['created_by'],
                'created_at' => new \SqlFunction('NOW'),
            ]
        );

        return $news->save(true);
    }

    function getPagination($params, $offset, $limit = 30, &$total) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY created_at DESC ";
        $where = "  ";

        if (isset($params['uuid']) && !empty(trim($params['uuid']))) {
            $where .= " AND user_uuid LIKE ".db_input(trim($params['uuid']));
        }

        if (isset($params['app_uuid']) && !empty(trim($params['app_uuid']))) {
            $where .= " AND ((user_uuid IS NOT NULL AND user_uuid LIKE ".db_input(trim($params['app_uuid'])).') OR user_uuid LIKE \'\' OR user_uuid IS NULL) ';
        }

        if (isset($params['title']) && !empty(trim($params['title']))) {
            $where .= " AND user_uuid LIKE ".db_input('%'.trim($params['title']).'%');
        }

        if (isset($params['content']) && !empty(trim($params['content']))) {
            $where .= " AND user_uuid LIKE ".db_input('%'.trim($params['content']).'%');
        }

        $sql = " SELECT * FROM api_news WHERE 1 $where $order $limit ";

        $res = db_query($sql);
        $total = db_num_rows($res);
        return $res;
    }
}

<?php

if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

?>



<style>

    textarea {

        width: 98%;

        resize: vertical;

        min-height: 300px;

    }

</style>



<form action="contracts.php" method="post" id="save">

 <?php csrf_token(); ?>

<input type="hidden" name="do" value="save">

<input type="hidden" name="t" value="contract_content">

 <h2><?php echo __('Contract Template');?></h2>

    <p>

       <textarea name="content"><?php echo $content; ?></textarea>

    </p>

<p style="text-align:center">

   <button class="btn_sm btn-primary">Save</button>
   <button class="btn_sm btn-default" onclick='window.location.href="contracts.php"'>Cancel</button>

</p>

</form>
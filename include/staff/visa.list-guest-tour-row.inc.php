<?php if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path'); ?>
<tr>
    <td>
       <input type="checkbox" name="ticks[]" value="<?php echo $row['id'].'_'.$row['tour_id'];?>">
        <?php echo ++$i; ?>
    </td>
    <td>
        <?php if(!empty($row['passport_no'])): ?>
            <p><?php echo $row['passport_no'] ?></p>
        <?php endif; ?>

        <?php if (isset($row['doe']) && strtotime($row['doe'])): ?>
            <p>
            <?php if ($row['doe_after'] < 0): ?>
                <span class="label label-small label-danger">
            <?php endif; ?>
                <small><?php echo date('d/m/Y', strtotime($row['doe'])) ?></small>
            <?php if ($row['doe_after'] < 0): ?>
                </span>
            <?php endif; ?>
            </p>
        <?php endif; ?>

    </td>
    <td>
        <p>
            <?php if($row['gender'] == 0): ?>
                <span class="label label-female"><i class="icon-female"></i></span>
            <?php else: ?>
                <span class="label label-primary"><i class="icon-male"></i></span>
            <?php endif; ?>

            <?php if(!empty($row['full_name'])): ?>
                <?php echo $row['full_name'];?>
            <?php endif; ?>
        </p>

        <?php if(!empty($row['phone_number'])): ?>
            <p><?php echo $row['phone_number'];?></p>
        <?php endif; ?>
    </td>
    <td>
        <p><?php echo Format::date('d/m/Y', strtotime($row['dob']))?></p>
        <?php if (isset($row['nationality']) && isset($country_list[ $row['nationality'] ])): ?>
        <p>
            <?php if('VNM' !== $row['nationality']): ?>
                <span class="label label-small label-warning">
            <?php endif; ?>
                <?php echo $country_list[ $row['nationality'] ] ?>
            <?php if('VNM' !== $row['nationality']): ?>
                </span>
            <?php endif; ?>
        </p>
        <?php endif; ?>
    </td>
    <td>
        <?php if(!empty($row['booking_code'])): ?>
            <p>
                <a class="no-pjax" target="_blank"
                   href="<?php echo $cfg->getUrl();?>scp/new_booking.php?id=<?php
                   echo $row['ticket_id'] ?>">
                    <?php echo _String::formatBookingCode($row['booking_code']) ?>
                </a>
            </p>
        <?php endif; ?>

        <?php if(!empty($row['staff'])): ?>
            <p><small><?php echo $row['staff'] ?></small></p>
        <?php endif; ?>
    </td>
    <?php if(!isset($_REQUEST['tour']) || (int)$_REQUEST['tour'] === 0): ?>
        <td>
            <?php if(!isset($tour_list[ $row['tour_id'] ]) || !$tour_list[ $row['tour_id'] ]): ?>
            <?php
            $tour_ = TourNew::lookup($row['tour_id']);
            if ($tour_) {
                echo $tour_->name;
                $tour_list[ $row['tour_id'] ] = $tour_->name;
            }
                ?>
            <?php else: ?>
            <?php echo $tour_list[ $row['tour_id'] ]; ?>
            <?php endif; ?>
        </td>
    <?php endif; ?>
    <td>
        <?php $profile_pax= VisaTourGuest::getProfileVisaPax($row['id'], $row['tour_id']);?>

        <?php if($profile_pax && $profile_pax->visa_status_id) echo \Tugo\VisaStatusList::caseTitleName($profile_pax->visa_status_id)?>

        <?php if($profile_pax && $profile_pax->passport_location_id):
                $passport = \Tugo\PassportLocation::lookup($profile_pax->passport_location_id); ?>
                <p><i class="icon-flag-alt"> </i><?php echo $passport->name?></p>
        <?php endif;?>

            <?php if ((int)$profile_pax->visa_status_id === \Tugo\VisaStatusList::VISA_APPROVED): ?>
                <span class="label label-primary"><i class="icon-check"></i></span>
            <?php endif; ?>

            <?php if ((int)$profile_pax->visa_status_id === \Tugo\VisaStatusList::VISA_REJECTED): ?>
                <span class="label label-small label-danger"><i class="icon-remove"></i></span>
            <?php endif; ?>

            <?php if ((int)$profile_pax->visa_status_id === \Tugo\VisaStatusList::VISA_APPROVED_NOT_GO): ?>
                <span class="label label-inverse"><i class="icon-mail-forward"></i></span>
            <?php endif; ?>
    </td>
    <td>
        <?php if($profile_pax): ?>
            <?php $name_appointment = VisaTourGuest::getAppointment($profile_pax->id);?>
            <p><?php echo $name_appointment['name']?></p>
            <?php if($name_appointment['date_event'] !=NULL): ?>
                <p>
                <?php echo date('d/m/Y H:i', strtotime($name_appointment['date_event'])); ?>
                </p>
            <?php endif; ?>
            <?php if(($count_appointment = VisaTourGuest::countAppointments($profile_pax->id, true))): ?>
                <p><span class="label label-warning"><?php echo $count_appointment ?></span></p>
            <?php endif; ?>
        <?php endif; ?>
    </td>
    <td>
        <?php $deadline = VisaTourGuest::getDeadline($row['profile_id']);?>
        <?php if($deadline): ?>
            <?php if($time < strtotime($row['gather_date'])): ?>
                <?php if ($time >  strtotime($row['gather_date']) - VISA_DEADLINE_DANGER): ?>
                    <span class="label label-danger">
                        <?php echo date('d/m', strtotime($deadline)) ?>
                    </span>
                <?php elseif ($time >  strtotime($row['gather_date']) - VISA_DEADLINE_WARNING): ?>
                    <span class="label label-warning">
                        <?php echo date('d/m', strtotime($deadline)) ?>
                    </span>
                <?php endif; ?>
            <?php else: ?>
                <?php echo date('d/m', strtotime($deadline)) ?>
            <?php endif; ?>
        <?php endif; ?>
    </td>
    <td class="autohide-buttons">
        <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
            <a href="<?php echo $cfg->getUrl().'scp/visa_pax_tour_item.php?id='
                .$row['id'].'&tour='.$row['tour_id'] ?>"
                target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                data-title="Edit Profile" ><i class="icon-edit"></i></a>
        <?php endif;?>
            <a href="<?php echo $cfg->getUrl().'scp/visa-tour-guest.php?id='
                .$row['id'].'&tour_id='.$row['tour_id'].'&layout=sms&send=send'?>"
                target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                data-title="Cuộc hẹn" ><i class="icon-comments"></i></a>
        <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
            <a href="<?php echo $cfg->getUrl();
            ?>scp/tour-guest.php?tour=<?php echo $row['tour_id'] ?>"
               target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
               data-title="Manual Add/Edit" ><i class="icon-user"></i></a>
        <?php endif;?>
        <a href="<?php echo $cfg->getUrl(); ?>scp/visa-tour-list.php?keyword_tour=<?php
            $tour = TourNew::lookup((int)($row['tour_id']));
            if ($tour)
                echo $tour->name;
            else
                echo ''; ?>"
            target="_blank"  class="btn_sm btn-xs btn-default no-pjax" data-title="View Tour" ><i class="icon-info"></i></a>
    </td>
</tr>
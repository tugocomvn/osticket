<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config) die('Access Denied');
?>
<h2><?php echo __('Call &amp; SMS Settings');?></h2>
<form action="settings.php?t=sms" method="post" id="save">
    <?php csrf_token(); ?>
    <input type="hidden" name="t" value="sms" >
    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo __('Limit Settings');?></h4>
<!--                <em>--><?php //echo __('');?><!--</em>-->
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('Limit per ticket per day'); ?>:</td>
            <td>
                <input type="number" step="1" max="10" min="0" name="sms_per_ticket" value="<?php echo $config['sms_per_ticket'] ?>">&nbsp;<font class="error">*&nbsp;<?php echo $errors['sms_per_ticket']; ?></font>
                SMS/ticket/day
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Limit total SMS a staff can use per day'); ?>:</td>
            <td>
                <input type="number" step="1" max="100" min="0" name="sms_per_staff_per_day" value="<?php echo $config['sms_per_staff_per_day'] ?>">&nbsp;<font class="error">*&nbsp;<?php echo $errors['sms_per_staff_per_day']; ?></font>
                SMS/staff/day
            </td>
        </tr>
        </tbody>
    </table>

    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="3">
                <h4><?php echo __('Auto Send SMS');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('ACTIVE time'); ?>:</td>
            <td>

            </td>
            <td>
                <p>From <input type="text" class="" name="sms_auto_active_time_from" value="<?php
                    if(isset($config['sms_auto_active_time_from']) && $config['sms_auto_active_time_from'])
                        echo $config['sms_auto_active_time_from'] ?>">&nbsp;<font class="error">&nbsp;<?php
                        echo $errors['sms_auto_active_time_from']; ?></font>
                </p>

                <p>
                    To <input type="text" class="" name="sms_auto_active_time_to" value="<?php
                    if(isset($config['sms_auto_active_time_to']) && $config['sms_auto_active_time_to'])
                        echo $config['sms_auto_active_time_to'] ?>">&nbsp;<font class="error">&nbsp;<?php
                        echo $errors['sms_auto_active_time_to']; ?></font>
                </p>
                <p><em>Chỉ gửi SMS <strong>TRONG</strong> khoảng thời gian này</em>. Định dạng: <em><strong>h:m</strong></em></p>
            </td>
        </tr>

        <tr>
            <td width="180" class="required"><?php echo __('Send if NO ANSWER'); ?>:</td>
            <td>
                <input type="checkbox" name="auto_send_no_answer" <?php
                if(isset($config['auto_send_no_answer']) && $config['auto_send_no_answer'])
                    echo 'checked' ?>>&nbsp;<font class="error">&nbsp;<?php
                    echo $errors['auto_send_no_answer']; ?></font>
            </td>
            <td>
                <textarea name="auto_send_no_answer_content" id="auto_send_no_answer_content"
                          cols="30" rows="3" style="width:90%;resize:none;"><?php
                    if (isset($config['auto_send_no_answer_content']))
                        echo trim($config['auto_send_no_answer_content']);
                ?></textarea>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Alert missed calls for agents'); ?>:</td>
            <td>
                <input type="checkbox" name="missed_call_sms_agent" <?php
                if(isset($config['missed_call_sms_agent']) && $config['missed_call_sms_agent'])
                    echo 'checked' ?>>&nbsp;<font class="error">&nbsp;<?php
                    echo $errors['missed_call_sms_agent']; ?></font>
            </td>
            <td>
                <textarea name="missed_call_sms_agent_numbers" id="missed_call_sms_agent_numbers"
                          cols="30" rows="3" style="width:90%;resize:none;"><?php
                    if (isset($config['missed_call_sms_agent_numbers']))
                        echo trim($config['missed_call_sms_agent_numbers']);
                ?></textarea>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Send after 50s calls'); ?>:</td>
            <td>
                <input type="checkbox" name="auto_send_call" <?php
                if(isset($config['auto_send_call']) && $config['auto_send_call'])
                    echo 'checked' ?>>&nbsp;<font class="error">&nbsp;<?php
                    echo $errors['auto_send_call']; ?></font>
            </td>
            <td><textarea name="auto_send_call_content" id="auto_send_call_content"
                          cols="30" rows="3" style="width:90%;resize:none;"><?php
                    if (isset($config['auto_send_call_content']))
                        echo trim($config['auto_send_call_content']);
                ?></textarea>
                <br>
                <p>
                    Available tags: AGENT_PHONE_NUMBER, AGENT_NAME
                </p>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Send after FAILED call from Agent'); ?>:</td>
            <td>
                <input type="checkbox" name="auto_send_after_failed_call" <?php
                if(isset($config['auto_send_after_failed_call']) && $config['auto_send_after_failed_call'])
                    echo 'checked' ?>>&nbsp;<font class="error">&nbsp;<?php
                    echo $errors['auto_send_after_failed_call']; ?></font>
            </td>
            <td><textarea name="auto_send_call_after_failed_content" id="auto_send_call_after_failed_content"
                          cols="30" rows="3" style="width:90%;resize:none;"><?php
                    if (isset($config['auto_send_call_after_failed_content']))
                        echo trim($config['auto_send_call_after_failed_content']);
                ?></textarea>
                <br>
                <p>
                    Available tags: AGENT_PHONE_NUMBER, AGENT_NAME
                </p>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Passport expiration reminder'); ?>:</td>
            <td>
                <input type="checkbox" name="passport_expiration_reminder" <?php
                if(isset($config['passport_expiration_reminder']) && $config['passport_expiration_reminder'])
                    echo 'checked' ?>>&nbsp;<font class="error">&nbsp;<?php
                    echo $errors['passport_expiration_reminder']; ?></font>
            </td>
            <td>
                Remind 1st time before
                <input width="80px" style="width:80px;resize:none;" placeholder="30"
                 type="number" name="passport_expiration_reminder_1st_before" value="<?php
                    if (isset($config['passport_expiration_reminder_1st_before']))
                        echo trim($config['passport_expiration_reminder_1st_before']);
                ?>"> days;
                Remind 2nd time before
                <input width="80px" style="width:80px;resize:none;" placeholder="30"
                 type="number" name="passport_expiration_reminder_2nd_before" value="<?php
                    if (isset($config['passport_expiration_reminder_2nd_before']))
                        echo trim($config['passport_expiration_reminder_2nd_before']);
                ?>"> days;
                <br>
                <textarea name="passport_expiration_reminder_content" id="passport_expiration_reminder_content"
                          cols="30" rows="3" style="width:90%;resize:none;"><?php
                    if (isset($config['passport_expiration_reminder_content']))
                        echo trim($config['passport_expiration_reminder_content']);
                ?></textarea>
                <br>
                <p>
                    Available tags: DAY_TO_EXPIRE
                </p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="3">
                <h4><?php echo __('Call Center');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('Send email after Missed Calls'); ?>:</td>
            <td>
                <input type="checkbox" name="send_email_for_missed_calls" <?php
                if(isset($config['send_email_for_missed_calls']) && $config['send_email_for_missed_calls'])
                    echo 'checked' ?>>&nbsp;<font class="error">*&nbsp;<?php
                    echo $errors['send_email_for_missed_calls']; ?></font>
            </td>
            <td>
                Send email to: <input type="text" name="email_send_email_for_missed_calls"
                       value="<?php if (isset($config['email_send_email_for_missed_calls']))
                    echo trim($config['email_send_email_for_missed_calls']); ?>">
            </td>
        </tr>
        </tbody>
    </table>

    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="3">
                <h4><?php echo __('Overdue Tickets');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('Filter tickets from'); ?>:</td>
            <td>

            </td>
            <td>
                <input type="text" class="" placeholder="Y-m-d" name="send_overdue_alert_from" value="<?php if (isset($config['send_overdue_alert_from']))
                    echo trim($config['send_overdue_alert_from']); ?>" >
            </td>
        </tr>

        <tr>
            <td width="180" class="required"><?php echo __('Send email for overdue tickets'); ?>:</td>
            <td>
                <input type="checkbox" name="send_email_overdue_tickets" <?php
                if(isset($config['send_email_overdue_tickets']) && $config['send_email_overdue_tickets'])
                    echo 'checked' ?>>&nbsp;<font class="error">*&nbsp;<?php
                    echo $errors['send_email_overdue_tickets']; ?></font>
            </td>
            <td>
                Email Title: <input style="width:50%;resize:none;" type="text" name="send_email_overdue_tickets_title"
                       value="<?php if (isset($config['send_email_overdue_tickets_title']))
                    echo trim($config['send_email_overdue_tickets_title']); ?>" /> [TK-#######]
                <font class="error"><br/><?php echo $errors['send_email_overdue_tickets_title']; ?></font>
                <br>
                <br>
                Email Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="send_email_overdue_tickets_content"
                       ><?php if (isset($config['send_email_overdue_tickets_content']))
                        echo trim($config['send_email_overdue_tickets_content']); ?></textarea>
                <font class="error"><br/><?php echo $errors['send_email_overdue_tickets_content']; ?></font>
                <br>
                <p>
                    Available tags: AGENT_PHONE_NUMBER, AGENT_NAME
                </p>
            </td>
        </tr>
        <tr>
            <td width="180" class="required"><?php echo __('Send SMS for overdue tickets'); ?>:</td>
            <td>
                <input type="checkbox" name="send_sms_overdue_tickets" <?php
                if(isset($config['send_sms_overdue_tickets']) && $config['send_sms_overdue_tickets'])
                    echo 'checked' ?>>&nbsp;<font class="error">*&nbsp;<?php
                    echo $errors['send_sms_overdue_tickets']; ?></font>
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="send_sms_overdue_tickets_content"
                       ><?php if (isset($config['send_sms_overdue_tickets_content']))
                        echo trim($config['send_sms_overdue_tickets_content']); ?></textarea>
                <font class="error"><br/><?php echo $errors['send_sms_overdue_tickets_content']; ?></font>
                <br>
                <p>
                    Available tags: AGENT_PHONE_NUMBER, AGENT_NAME
                </p>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="3">
                <h4><?php echo __('After Sales SMS');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td width="180" class="required"><?php echo __('Hàn Quốc'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_hanquoc" size="4" value="<?php
                if(isset($config['after_sms_country_id_hanquoc']) && $config['after_sms_country_id_hanquoc'])
                    echo $config['after_sms_country_id_hanquoc'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_hanquoc_content"
                ><?php if (isset($config['after_sms_country_id_hanquoc_content']))
                        echo trim($config['after_sms_country_id_hanquoc_content']); ?></textarea>
            </td>
        </tr>


        <tr>
            <td width="180" class="required"><?php echo __('Nhật Bản'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_nhatban" size="4" value="<?php
                if(isset($config['after_sms_country_id_nhatban']) && $config['after_sms_country_id_nhatban'])
                    echo $config['after_sms_country_id_nhatban'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_nhatban_content"
                ><?php if (isset($config['after_sms_country_id_nhatban_content']))
                        echo trim($config['after_sms_country_id_nhatban_content']); ?></textarea>
            </td>
        </tr>


        <tr>
            <td width="180" class="required"><?php echo __('Anh Quốc'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_anhquoc" size="4" value="<?php
                if(isset($config['after_sms_country_id_anhquoc']) && $config['after_sms_country_id_anhquoc'])
                    echo $config['after_sms_country_id_anhquoc'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_anhquoc_content"
                ><?php if (isset($config['after_sms_country_id_anhquoc_content']))
                        echo trim($config['after_sms_country_id_anhquoc_content']); ?></textarea>
            </td>
        </tr>


        <tr>
            <td width="180" class="required"><?php echo __('Châu Âu'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_chauau" size="4" value="<?php
                if(isset($config['after_sms_country_id_chauau']) && $config['after_sms_country_id_chauau'])
                    echo $config['after_sms_country_id_chauau'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_chauau_content"
                ><?php if (isset($config['after_sms_country_id_chauau_content']))
                        echo trim($config['after_sms_country_id_chauau_content']); ?></textarea>
            </td>
        </tr>


        <tr>
            <td width="180" class="required"><?php echo __('Úc'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_uc" size="4" value="<?php
                if(isset($config['after_sms_country_id_uc']) && $config['after_sms_country_id_uc'])
                    echo $config['after_sms_country_id_uc'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_uc_content"
                ><?php if (isset($config['after_sms_country_id_uc_content']))
                        echo trim($config['after_sms_country_id_uc_content']); ?></textarea>
            </td>
        </tr>


        <tr>
            <td width="180" class="required"><?php echo __('Canada'); ?>:</td>
            <td>Tour ID
                <input type="text" name="after_sms_country_id_canada" size="4" value="<?php
                if(isset($config['after_sms_country_id_canada']) && $config['after_sms_country_id_canada'])
                    echo $config['after_sms_country_id_canada'] ?>" class="input-field">
            </td>
            <td>
                SMS Content: <textarea style="width:90%;resize:none;" cols="30" rows="3" type="text" name="after_sms_country_id_canada_content"
                ><?php if (isset($config['after_sms_country_id_canada_content']))
                        echo trim($config['after_sms_country_id_canada_content']);?></textarea>

            </td>
        </tr>
        </tbody>
    </table>
    <table class="form_table settings_table" width="1050" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <th colspan="3">
                <h4><?php echo __('Hợp đồng điện tử');?></h4>
            </th>
        </tr>
        </thead>
    	<tbody>
        <tr>
            <td>
            Email Content:
            <textarea style="width:98%;resize:none;margin-left:5px;" cols="30" rows="7" type="text"name="sms_contract_content" id="sms_contract_content">
                <?php if (isset($config['sms_contract_content']))
                        echo trim($config['sms_contract_content']);?></textarea>
            </td>
        </tr>
        <tr>
            <td>
            SMS Content: <textarea style="width:98%;resize:none;margin-left:5px;" cols="30" rows="3" type="text" name="">
             <?php if (isset($config['']))
                        echo trim($config['']);?></textarea>
            </td>
        </tr>
    	</tbody>
    </table>
    <p style="padding-left:250px;">
        <input class="button" type="submit" name="submit" value="<?php echo __('Save Changes');?>">
        <input class="button" type="reset" name="reset" value="<?php echo __('Reset Changes');?>">
    </p>
</form>

<?php
/*********************************************************************
    http.php

    HTTP controller for the osTicket API

    Jared Hancock
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
// Use sessions — it's important for SSO authentication, which uses
// /api/auth/ext
define('DISABLE_SESSION', false);

require 'api.inc.php';

# Include the main api urls
require_once INCLUDE_DIR."class.dispatcher.php";

$dispatcher = patterns('',
    url_post("^/tickets\.(?P<format>xml|json|email)$", array('api.tickets.php:TicketApiController','create')),
    url('^/tasks/', patterns('',
        url_post("^cron$", array('api.cron.php:CronApiController', 'execute'))
    )),
    url('^/call/', patterns('',
        url_post("^add$", array('api.call.php:CallApiController', 'add'))
    )),
    url('^/v1/', patterns('',
        url('^app/', patterns('',
            url_get("^test$", array('api.app.php:AppApiController', 'test')),
            url_post("^test$", array('api.app.php:AppApiController', 'testpost')),
            url_post("^login$", array('api.app.php:AppApiController', 'login')),
            url_post("^logout$", array('api.app.php:AppApiController', 'logout')),
            url_post("^register$", array('api.app.php:AppApiController', 'register')),
            url_post("^refresh$", array('api.app.php:AppApiController', 'refresh')),
            url_get("^qrcode$", array('api.app.php:AppApiController', 'qrcode')),
            url_post("^feedback$", array('api.app.php:AppApiController', 'feedback')),
            url_post("^request$", array('api.app.php:AppApiController', 'request')),
            url_post("^otp$", array('api.app.php:AppApiController', 'otp')),
            url_get("^point-history$", array('api.app.php:AppApiController', 'pointHistory')),
            url_get("^news$", array('api.app.php:AppApiController', 'news')),
            url_get("^me$", array('api.app.php:AppApiController', 'profile')),
            url_post("^me$", array('api.app.php:AppApiController', 'updateProfile')),
            url_post("^notification$", array('api.app.php:AppApiController', 'notification')),
            url_post("^verify-otp$", array('api.app.php:AppApiController', 'verifyOTP')),
            url_post("^point-history-otp$", array('api.app.php:AppApiController', 'pointHistoryOTP')),
            url_get("^membership$", array('api.app.php:AppApiController', 'membership')),
            url_get("^total-point$", array('api.app.php:AppApiController', 'totalPointOTP')),
            url_get("^address$", array('api.app.php:AppApiController', 'getAddress'))
        ))
    ))
);

Signal::send('api', $dispatcher);

# Call the respective function
print $dispatcher->resolve($ost->get_path_info());
?>

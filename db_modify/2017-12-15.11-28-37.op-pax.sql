ALTER TABLE ost_pax_tour ADD booking_date DATETIME NULL;
ALTER TABLE ost_pax_tour ADD apply_date DATETIME NULL;
ALTER TABLE ost_pax_tour ADD visa_estimated_due_date DATETIME NULL;

ALTER TABLE ost_pax_tour CHANGE booking_date receiving_date DATETIME;
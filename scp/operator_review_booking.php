<?php
require('staff.inc.php');

include_once INCLUDE_DIR.'class.booking.php';
include_once INCLUDE_DIR.'class.pax.php';

$opReview = BookingReservation::opReview();
$opReviewed = BookingReservation::opReviewed();


$nav->setTabActive('operator');
$nav->setActiveSubMenu(2,'operator');

$page = "operator_review_booking.inc.php";
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


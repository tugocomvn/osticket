<?php
/*********************************************************************
users.php

Peter Rotich <peter@osticket.com>
Jared Hancock <jared@osticket.com>
Copyright (c)  2006-2014 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');

require_once(INCLUDE_DIR.'class.list.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once INCLUDE_DIR.'class.tour-destination.php';
global $thisstaff;

if (!$thisstaff
    || !($thisstaff->canViewOperatorLists() || $thisstaff->canEditOparetorLists() || $thisstaff->canCreateOperatorListItem()))
    die('Access Denied');


if($thisstaff->canViewOperatorLists() || $thisstaff->canEditOparetorLists() || $thisstaff->canCreateOperatorListItem())
    $nav->addSubMenu([
         'desc'      => __('Operator Lists'),
         'title'     => __('View All Lists'),
         'href'      => 'operator.php',
         'iconclass' => 'newTicket',
         'id'        => 'operator',
     ], (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
$nav->setTabActive('operator');

$item_info = $list = $keyword = null;
$qs = $item_ids = $item_ids_pax = $item_ids_tour = $keyword_pax = $keyword_tour = [];

$criteria = array('allow_user_update' => 1);
if ($_REQUEST['id']) {
    $criteria['id'] = trim($_REQUEST['id']);
    $qs['id'] = trim($_REQUEST['id']);
} elseif ($_REQUEST['type']) {
    $criteria['type'] = trim($_REQUEST['type']);
    $qs['type'] = trim($_REQUEST['type']);
}

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$destination_list = TourDestination::getList($group_des);
if (isset($criteria['id']) && defined("TOUR_PAX_LIST_ID") && TOUR_PAX_LIST_ID == $criteria['id']) {

    $flag = false;
    $tour_sql=" ";
    if (!$thisstaff->isAdmin()){
        $flag=true;
        $destination = 'OR (destination is NULL or destination = 0)';
        if(isset($group_des) && is_array($group_des) && count(array_unique(array_filter($group_des)))) {
            $tour_sqlc .= " AND (destination IN (" . implode(",", $group_des) . ") $destination) ";
        }else{
            $tour_sqlc .= " AND 1 = 0";
        }
    }


    if (isset($_REQUEST['keyword']) && !empty($_REQUEST['keyword'])) {
        $flag=true;
        $keyword_tour['keyword'] = $qs['keyword'] = trim($_REQUEST['keyword']);
        $tour_sqlc .= " AND name LIKE  ".db_input('%'.$_REQUEST['keyword'].'%');
    }

    if (isset($_REQUEST['tour']) && !empty($_REQUEST['tour'])) {
        $flag=true;
        $keyword_tour['tour'] = $qs['tour'] = trim($_REQUEST['tour']);
        $tour_sqlc .= " AND destination =  ".db_input($_REQUEST['tour']);
    }

    if (isset($_REQUEST['from']) && !empty($_REQUEST['from'])) {
        $flag=true;
        $keyword_tour['from'] = $qs['from'] = trim($_REQUEST['from']);
        $tour_sqlc .= " AND departure_date IS NOT NULL AND departure_date <> '' AND date(departure_date) >= ".db_input($_REQUEST['from']);
    }

    if (isset($_REQUEST['to']) && !empty($_REQUEST['to'])) {
        $flag=true;
        $keyword_tour['to'] = $qs['to'] = trim($_REQUEST['to']);
        $tour_sqlc .= " AND departure_date IS NOT NULL AND departure_date <> '' AND date(departure_date) <= ".db_input($_REQUEST['to']);
    }

    if($flag) {
        $tour_sql = "SELECT id FROM ost_tour WHERE 1 $tour_sqlc ";
    }else{
        $tour_sql = "SELECT id FROM ost_tour WHERE 1 = 1";
    }

    $tour_res = db_query($tour_sql);
    while ($tour_res && ($row = db_fetch_array($tour_res))) {
        $item_ids_tour[] = intval($row['id']);
    }

    if (isset($_REQUEST['pax_name']) && !empty($_REQUEST['pax_name'])) {
        $keyword_pax['pax_name'] = $qs['pax_name'] = trim($_REQUEST['pax_name']);
    }

    if (isset($_REQUEST['pax_phone_number']) && !empty($_REQUEST['pax_phone_number'])) {
        $qs['pax_phone_number'] = $qs['pax_phone_number'] = trim($_REQUEST['pax_phone_number']);
        $keyword_pax['pax_phone_number'] = trim($_REQUEST['pax_phone_number']);
    }

    if (isset($_REQUEST['pax_passport_no']) && !empty($_REQUEST['pax_passport_no'])) {
        $keyword_pax['pax_passport_no'] = $qs['pax_passport_no'] = trim($_REQUEST['pax_passport_no']);
    }

    if (isset($_REQUEST['pax_booking_code']) && !empty($_REQUEST['pax_booking_code'])) {
        $keyword_pax['pax_booking_code'] = $qs['pax_booking_code'] = trim($_REQUEST['pax_booking_code']);
    }

    if ($keyword_pax) {
        $item_ids_pax = Pax::search($keyword_pax);
    }

    if ($keyword_pax && !$item_ids_pax) $item_ids_pax = null;
    if ($keyword_tour && !$item_ids_tour) $item_ids_tour = null;

    if (is_null($item_ids_pax) || is_null($item_ids_tour))
        $item_ids = null;
    elseif ($item_ids_pax && !$item_ids_tour)
        $item_ids = $item_ids_pax;
    elseif (!$item_ids_pax && $item_ids_tour)
        $item_ids = $item_ids_tour;
    else
        $item_ids = array_intersect($item_ids_pax, $item_ids_tour);
} // END IF TOUR_PAX_LIST_ID
else {
    if (isset($_REQUEST['item_info']) && !empty($_REQUEST['item_info'])) {
        $item_info = trim($_REQUEST['item_info']);
    }
}

if ($criteria) {
    $list = DynamicList::lookup($criteria);

    if ($list)
        $form = $list->getForm();
    else
        $errors['err']=sprintf(__('%s: Unknown or invalid ID.'),
                               __('custom list'));
}

$errors = array();
$max_isort = 0;

if (isset($_REQUEST['do']) && $_REQUEST['do'] === 'reload') {
    if ($list->getId() == TOUR_LIST_ID) {
        foreach ($list->getItems() as $item) {
            $item->push(TOUR_PROERTIES_FORM_ID);
        }
    }
}

if($_POST) {
    switch(strtolower($_POST['do'])) {
        case 'update':
            if (!$list)
                $errors['err']=sprintf(__('%s: Unknown or invalid ID.'),
                                       __('custom list'));
            else {
                // Update items
                $items = array();
                $page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
                $count = $list->getNumItems($item_info ?: $keyword);
                $pageNav = new Pagenate($count, $page, PAGE_LIMIT);
                foreach ($list->getAllItems() as $item) {
                    if (!isset($_POST["value-".$item->getId()])) continue;

                    $id = $item->getId();
                    $ht = array(
                        'value' => $_POST["value-$id"],
                        'abbrev' => $_POST["abbrev-$id"],
                        'sort' => $_POST["sort-$id"],
                    );
                    $value = mb_strtolower($ht['value']);
                    if (!$value)
                        $errors["value-$id"] = __('Value required');
                    elseif (in_array($value, $items) && 'delete' !== strtolower($value))
                        $errors["value-$id"] = __('Value already in-use');
                    elseif ($item->update($ht, $errors)) {
                        if ($_POST["disable-$id"] == 'on')
                            $item->disable();
                        elseif(!$item->isEnabled() && $item->isEnableable())
                            $item->enable();

                        $item->save();
                        $items[] = $value;

                        if ($list->getId() == TOUR_LIST_ID) {
                            $item = $list->getItem( (int) $id);
                            $item->push(TOUR_PROERTIES_FORM_ID);
                        }
                    }

                    $max_isort = max($max_isort, $_POST["sort-$id"]);
                }

                if ($errors)
                    $errors['err'] = $errors['err'] ?: sprintf(__('Unable to update %s. Correct error(s) below and try again!'),
                                                               __('custom list items'));
                else {
                    $list->_items = null;
                    $msg = sprintf(__('Successfully updated %s'),
                                   __('this custom list'));
                }

            }

            break;
        case 'add':
            if ($list=DynamicList::add($_POST, $errors)) {
                $form = $list->getForm();
                $msg = sprintf(__('Successfully added %s'),
                               __('this custom list'));
            } elseif ($errors) {
                $errors['err']=sprintf(__('Unable to add %s. Correct error(s) below and try again.'),
                                       __('this custom list'));
            } else {
                $errors['err']=sprintf(__('Unable to add %s.'), __('this custom list'))
                    .' '.__('Internal error occurred');
            }
            break;
    }

    if ($list && $list->allowAdd()) {
        for ($i=0; isset($_POST["sort-new-$i"]); $i++) {
            if (!$_POST["value-new-$i"])
                continue;

            $item = $list->addItem(array(
                               'value' => $_POST["value-new-$i"],
                               'abbrev' =>$_POST["abbrev-new-$i"],
                               'sort' => $_POST["sort-new-$i"] ?: ++$max_isort,
                           ), $errors);

            if ($list->getId() == TOUR_LIST_ID) {
                $item->push(TOUR_PROERTIES_FORM_ID);
            }
        }


    }

    if ($form) {
        for ($i=0; isset($_POST["prop-sort-new-$i"]); $i++) {
            if (!$_POST["prop-label-new-$i"])
                continue;
            $field = DynamicFormField::create(array(
                                                  'form_id' => $form->get('id'),
                                                  'sort' => $_POST["prop-sort-new-$i"] ?: ++$max_sort,
                                                  'label' => $_POST["prop-label-new-$i"],
                                                  'type' => $_POST["type-new-$i"],
                                                  'name' => $_POST["name-new-$i"],
                                              ));
            if ($field->isValid()) {
                $field->form = $form;
                $field->save();
            }
            else
                $errors["new-$i"] = $field->errors();
        }
        // XXX: Move to an instrumented list that can handle this better
        if (!$errors)
            $form->_dfields = $form->_fields = null;
    }

    header('Location: '.$_SERVER['HTTP_REFERER']);
    exit;
}
$_SESSION['op_referer'] = isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/scp/operator.php') !== false
    ? $_SERVER['REQUEST_URI'] : '/scp/operator.php';

$page='view-lists.inc.php';
if(($list && $criteria['id']) || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add'))) {
    $page='view-list.inc.php';
    $ost->addExtraHeader('<meta name="tip-namespace" content="operator.all_list" />',
                         "$('#content').data('tipNamespace', 'operator.all_list');");
}

$nav->setTabActive('operator');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

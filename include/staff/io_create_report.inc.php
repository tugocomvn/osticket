<?php
require('staff.inc.php');
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canCreatePayments()) die('Access Denied');
?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/report_commission.php?action=list_report"?>">Back</a>

<form  action="<?php echo $cfg->getUrl()."scp/report_commission.php?action=create_report"?>" method="post" >
    <?php csrf_token(); ?>
    <table style="margin-left: 25%">
        <tr>
            <td>Tháng</td>
            <td>
                <select name="month">
                    <?php for($i=1; $i<=12; $i++):;
                        $month = $i;
                        echo sprintf('<option value="%d">%s</option>',$month,$month);
                    endfor;?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Năm</td>

            <td>
                <select name="year" class="input-field">
                    <?php for ($i=0; $i < 2; $i++):
                        $year = date('Y')-$i;
                        echo sprintf('<option value="%d">%s</option>',$year,$year);
                     endfor;?>
                </select>
            </td>
        </tr>
        <tr>
        <td>Nhân viên</td>
            <td>
                <select name='agent' class="input-field">
                    <?php foreach($allStaff as $id => $name):
                        echo sprintf('<option value="%s">%s</option>',$id,$name);
                    endforeach; ?>
                </select>
            </td>
        </tr>

         <tr>
            <td>Ghi chú</td>
             <td>
                 <textarea rows="4" cols="50" name="note" value="<?php if(isset($_REQUEST['note'])) echo $_REQUEST['note'] ?>"></textarea>
             </td>
         </tr>
         <tr >
             <td></td>
            <td>
                <button type="submit" class="btn_sm btn-primary align-center" name="create">Tạo báo cáo</button>
            </td>
         </tr>
    </table>
</form>


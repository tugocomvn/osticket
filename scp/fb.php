<?php
require_once('staff.inc.php');
global $cfg;
?>

<html>
<head>
    <title>Tugo - Facebook Ticket</title>
    <meta charset="utf8">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>scp/editor/ui/trumbowyg.min.css">
    <link rel="stylesheet" href="<?php echo $cfg->getUrl() ?>scp/css/fb.css">
    <script src="<?php echo $cfg->getUrl() ?>scp/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo $cfg->getUrl() ?>scp/editor/trumbowyg.min.js"></script>
</head>
<body>
<div>
    <?php csrf_token() ?>
    <p><input type="text" autofocus name="title" id="title" placeholder="tour name" tabindex="1"> <input type="checkbox" name="hn" id="hn" value="1" tabindex="3"> Tugo HN</p>
    <p><textarea name="body" id="body" cols="30" rows="10" placeholder="content" tabindex="99"></textarea></p>
    <p><button type="button" id="send_btn" tabindex="4">Send</button></p>
</div>

<ol class="list" reversed>

</ol>

<script src="<?php echo $cfg->getUrl() ?>scp/js/fb.js"></script>
</body>
</html>


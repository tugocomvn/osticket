        </div>
    </div>
    <div id="footer">
        <?php  require_once __DIR__.'/../../version.php';?>
        <p>Copyright &copy; <?php echo date('Y'); ?> <?php echo (string) $ost->company ?: 'Tugo.com.vn'; ?> - All rights reserved.</p>
        <a id="" href="http://tugo.com.vn" target="_blank"><?php echo __('Helpdesk software - powered by Tugo'); ?></a>
        <p>Version <?php echo TUGO_OST_VERSION ?></p>
    </div>
<div id="overlay"></div>
<div id="loading">
    <h4><?php echo __('Chờ chút nhé!');?></h4>
    <p><?php echo __('Hệ thống đang xử lý!');?></p>
</div>
<?php
if (($lang = Internationalization::getCurrentLanguage()) && $lang != 'en_US') { ?>
    <script type="text/javascript" src="ajax.php/i18n/<?php
        echo $lang; ?>/js"></script>
<?php } ?>
</body>
</html>

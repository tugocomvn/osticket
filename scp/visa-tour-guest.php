<?php
/*********************************************************************
    visa-tour-guest.php

    Emails

    Peter Rotich <peter@osticket.com>
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');
require_once(INCLUDE_DIR.'class.visa-tour.php');
require_once(INCLUDE_DIR.'class.visa.php');
include_once INCLUDE_DIR.'data/country.list.php';
require_once(INCLUDE_DIR.'class.pax.php');
include_once(INCLUDE_DIR.'class.staff.php');

function title_visa_info($departure_flight_code,$itinerary_go,$tour_date_a, $tour_date_b){
    //info tour
    $date_go = date('dM',strtotime($tour_date_a));
    $date_go = strtoupper($date_go);
    $time_go_from = date('Hi',strtotime($tour_date_a));
    $time_go_to = date('Hi',strtotime($tour_date_b));
    $itinerary_go = str_replace('-', '', $itinerary_go);

    $info_tour_go = $departure_flight_code.' '.$date_go.' '.$itinerary_go.' '.$time_go_from.' '.$time_go_to;
    return $info_tour_go;
}

function saveAppointment($data){
    global $thisstaff;
    if (!$data) return false;
    $id_profile  = \Tugo\VisaTourPaxProfile::lookup((int)$data['tour_pax_visa_profile_id']);
    if(!$id_profile)
        return false;
    $id_visa_appointment = \Tugo\VisaAppointment::lookup((int)$data['id-visa-appointment']);
    if(!$id_visa_appointment) return false;

    $date_event = date("Y-m-d", strtotime($data['date-appointment']));
    $time_event = date("H:i", strtotime($data['time-appointment']));
    $date_time_event = $date_event .' '.$time_event;
    $note_appointment = trim($data['note-appointment']);

    //----------CREATE------------
    $data_create = [
        'tour_pax_visa_profile_id' => $id_profile->id,
        'visa_appointment_id' => $id_visa_appointment->id,
        'date_event' => $date_time_event,
        'note' => $note_appointment
    ];
    $news = \Tugo\VisaProfileAppointment::create($data_create);
    $id = $news->save(true);
    if(!$id) return false;

    //add log action create visa appointment

    if(isset($data['time_action_log']) && $data['time_action_log'] !== '')
        $time_action_log = $data['time_action_log'];
    else $time_action_log = date('Y-m-d H:i:s');
    \Tugo\VisaFollowAction::addLogAction($id_profile->id, $news->ht,
        \Tugo\VisaFollowAction::VISA_CREATED_VISA_APPOINTMENT, $time_action_log);

    $log = \Tugo\VisaProfileAppointmentAction::create(
        [
            'visa_profile_appointment_action_log_id' => $id,
            'snapshot' => json_encode($news->ht),
            'time' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId(),
        ]
    );
    $log_id = $log->save();
    $news->set('log_id_visa_appointment', $log_id);
    $news->save(true);

    return true;
}

function createProfileVisa($id_pax, $tour_id, &$mess){
    global $thisstaff;
    $tour = TourNew::lookup($tour_id);

    if (!$id_pax || !$tour) {
        $mess[] = 'Không tồn tại khách hàng này';
        return 0;
    }
    db_start_transaction();

    try {
        $data = [
            'paper_require'        => 0,
            'finance_status'       => 0,
            'job_status_id'        => 0,
            'marital_status_id'    => 0,
            'visa_status_id'       => 0,
            'passport_location_id' => 0,
            'visa_type_id'         => 0,
            'staff_id'             => (int)$thisstaff->getId(),
            'deadline'             => null,
            'visa_code'            => null,
            'street_address'       => null,
            'city_id'              => 0,
            'district_id'          => 0,
            'ward_id'              => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $thisstaff->getId(),
        ];
        $news = \Tugo\VisaTourPaxProfile::create($data);
        $news->save(true);

        //add log action auto create visa_profile
        \Tugo\VisaFollowAction::addLogAction($news->id, $news->ht,
                    \Tugo\VisaFollowAction::VISA_AUTO_CREATE_PROFILE);

        $id = $news->ht['id'];
        VisaTourGuest::insert_visa_profile($id_pax, $tour_id, $id);
        $log = \Tugo\TourPaxVisaProfileHistory::create(
            [
                'tour_pax_visa_profile_id' => $id,
                'snapshot'                         => json_encode($news->ht),
                'time'                             => date('Y-m-d H:i:s'),
                'created_by' => $thisstaff->getId()
            ]
        );
        $log_id = $log->save();
        $news->set('log_id_profile_history', $log_id);
        $news->save(true);
        db_commit();

    }catch (exception $ex){
        $mess[] = $ex->getMessage();
        db_rollback();
        return 0;
    }
    return $id;
}

$name = $phone_number = $booking_code = $passport_code =  null;
$qs = [];
if(isset($_REQUEST['name'])){
    $name = trim($_REQUEST['name']);
    $qs += [ 'name' => trim($_REQUEST['name']) ];
}
if(isset($_REQUEST['phone_number'])){
    $phone_number = trim($_REQUEST['phone_number']);
    $qs += [ 'phone_number' => trim($_REQUEST['phone_number']) ];
}
if(isset($_REQUEST['booking_code'])){
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += [ 'booking_code' => trim($_REQUEST['booking_code']) ];
    }
if(isset($_REQUEST['passport_code'])){
    $passport_code = trim($_REQUEST['passport_code']);
    $qs += [ 'passport_code' => trim($_REQUEST['passport_code']) ];
}
$dob  = (isset($_REQUEST['dob']) && (strlen($_REQUEST['dob'])>=8))
    ? trim($_REQUEST['dob']) : null;

if ($dob) {
    $qs += [ 'dob' => trim($_REQUEST['dob']) ];
}

$doe  = (isset($_REQUEST['doe']) && (strlen($_REQUEST['doe'])>=8))
    ? trim($_REQUEST['doe']) : null;

if ($doe) {
    $qs += [ 'doe' => trim($_REQUEST['doe']) ];
}

$visa_apply_date  =(isset($_REQUEST['visa_apply']) && (strlen($_REQUEST['visa_apply'])>=8))
    ? trim($_REQUEST['visa_apply']) : null;

if ($visa_apply_date) {
    $qs += [ 'visa_apply' => trim($_REQUEST['visa_apply']) ];
}

$est_due_date  =(isset($_REQUEST['est_due_date']) && (strlen($_REQUEST['est_due_date'])>=8))
    ? trim($_REQUEST['est_due_date']) : null;

if ($est_due_date) {
    $qs += [ 'est_due_date' => trim($_REQUEST['est_due_date']) ];
}

if(isset($_REQUEST['visa_result']) && $_REQUEST['visa_result']){
    $visa_result = trim($_REQUEST['visa_result']);
    $qs += [ 'visa_result' => trim($_REQUEST['visa_result']) ];
}

if(isset($_REQUEST['staff']) && $_REQUEST['staff']){
    $staff = trim($_REQUEST['staff']);
    $qs += [ 'staff' => trim($_REQUEST['staff']) ];
}
$deadline =(isset($_REQUEST['dl']) && (strlen($_REQUEST['dl'])>=8))
    ?  trim($_REQUEST['dl'])  : null;

if($deadline) {
    $qs += ['deadline' => trim($_REQUEST['dl']) ];
}

if(isset($_REQUEST['destination']) && $_REQUEST['destination'])
    $destination = $_REQUEST['destination'];

if(isset($_REQUEST['keyword_tour']) && $_REQUEST['keyword_tour'])
    $keyword_tour = trim($_REQUEST['keyword_tour']);
else
    $keyword_tour = '';

$tour_id = $_REQUEST['tour'];
$tour = TourNew::lookup($tour_id);
$country_list = \Tugo\Data\Country::getList();
$listVisaStatus = \Tugo\StatusVisa::getItemExist();

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$booking_type_list = TourDestination::loadList($group_des);


//only access when have permission
if(!in_array($tour->destination,$group_des) || empty($group_des))
    $tour_id = $_REQUEST['tour'] = 0;

if(isset($destination) && !empty(array_filter($destination)))
{
    $group_des = array_intersect($group_des,$destination);
}

//var_dump($group_des);
$searchDataGuest = array(
    'keyword_tour'    => $keyword_tour,
    'destination'     => $group_des,
    'name'            => $name,
    'phone_number'    => $phone_number,
    'booking_code'    => $booking_code,
    'passport_code'   => $passport_code,
    'dob'             => $dob,
    'doe'             => $doe,
    'visa_apply_date' => $visa_apply_date,
    'est_due_date'    => $est_due_date,
    'visa_result'     => $visa_result,
    'deadline'        => $deadline,
    'staff'           => $staff
);

$listVisaAppointment = \Tugo\VisaAppointment::getItemExist();
//export file EXCEL


if(isset($_REQUEST['ticks']) && !empty($_REQUEST['ticks']) && ($_REQUEST['action'] === 'export'))
{
    $arr_export_pax = [];
    $datas_tour_id = [];
    $content_export = [];
    $pax_tour = explode(",",$_REQUEST['ticks']);
    foreach ($pax_tour as $value){
        $info = explode("_",$value);
        $arr_export_pax[] = $info[0]; //id guest
        $datas_tour_id[] = $info[1]; //tour id
    }

    if (!$tour_id)
        exit('Tour not found');

    $tour_pax_list = PaxTour::getData($tour_id, true);
    $tour = TourNew::lookup($tour_id);

    require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
    $dir = INCLUDE_DIR.'../public_file/';

    //create folder
    if(!file_exists($dir)){
        if (!mkdir($dir, 0777) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
    }

    $file_name = 'Tour - '
        . preg_replace('/[^a-zA-Z0-9-_]/', '-', _String::khongdau($tour->name))
        .' - Pax List - ' . Format::userdate("Y-m-d_H-i-s", time()).".xlsx";
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
    $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    $objPHPExcel = new PHPExcel();
    $sheet = $objPHPExcel->getActiveSheet();

    //INSERT LOGO
    if(!empty($tour->airline)) {
        //Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
        $objDrawing = new PHPExcel_Worksheet_Drawing();    //create object for Worksheet drawing

        $objDrawing->setName('Customer Signature');        //set name to image

        $signature = '../images/airline/' . $tour->airline . '.jpg';    //Path to signature .jpg file
        if (file_exists($signature)) {
            $objDrawing->setPath($signature);

            $objDrawing->setOffsetX(60);                       //setOffsetX works properly
            $objDrawing->setOffsetY(0);                       //setOffsetY works properly

            $objDrawing->setCoordinates('A1');        //set image to cell

            $objDrawing->setWidth(600);                 //set width, height
            $objDrawing->setHeight(70);

            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());  //save
        }
    }

    $excel_row = 5;
    $sheet->setCellValue('A'.$excel_row++, $tour->name);
    $objPHPExcel->getActiveSheet()->getStyle("A5:F5")->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS)));

    $departure_info = '';
    $return_info = '';
    $airport_code = FlightOP::searchAirportCodeFromTour((int)$tour->id); //array

    if (count($airport_code) === 2) {
        $departure_info = date('d M', strtotime($airport_code[0]['departure_at'])) . ' - '
            . $airport_code[0]['airport_code_from'] . ' ' . $airport_code[0]['airport_code_to'] . ' '
            . date('Hi', strtotime($airport_code[0]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[0]['arrival_at']));

        $return_info = date('d M', strtotime($airport_code[1]['departure_at'])) . ' - '
            . $airport_code[1]['airport_code_from'] . ' ' . $airport_code[1]['airport_code_to'] . ' '
            . date('Hi', strtotime($airport_code[1]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[1]['arrival_at']));
    } elseif (count($airport_code) === 4) {
        $departure_info = date('d M', strtotime($airport_code[0]['departure_at'])) . ' - '
            . $airport_code[0]['airport_code_from'] . ' ' . $airport_code[0]['airport_code_to'] . ' ' . $airport_code[1]['airport_code_to'] . ' - '
            . date('Hi', strtotime($airport_code[0]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[1]['arrival_at']));

        $return_info = date('d M', strtotime($airport_code[2]['departure_at'])) . ' - '
            . $airport_code[2]['airport_code_from'] . ' ' . $airport_code[2]['airport_code_to'] . ' ' . $airport_code[3]['airport_code_to'] . ' - '
            . date('Hi', strtotime($airport_code[2]['departure_at'])) . ' - ' . date('Hi',
                strtotime($airport_code[3]['arrival_at']));
    }

    if(!empty($airport_code)) {
        $sheet->setCellValue('A' . $excel_row, strtoupper($departure_info));
        $objPHPExcel->getActiveSheet()->getStyle("A" . $excel_row . ":F" . $excel_row)->applyFromArray(
            array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS)));
        $excel_row++;

        $sheet->setCellValue('A' . $excel_row, strtoupper($return_info));
        $objPHPExcel->getActiveSheet()->getStyle("A" . $excel_row . ":F" . $excel_row)->applyFromArray(
            array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS)));
        $excel_row++;
    }

    $objPHPExcel->getActiveSheet()->getStyle("A5:B".$excel_row)->getFont()->setSize(17);
    $excel_row ++;

    $header = [
        'NO',
        'NAME',
        'SEX',
        'DOB',
        'PPNo',
        'DOE',
    ];

    $col = 'A';
    foreach ($header as $title) {
        $sheet->setCellValue($col++.$excel_row, $title);
    }
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);

    $start_data = $excel_row;
    $excel_row++;
    $content_pax_list =[];
    // ghi data vao
    if ($tour_pax_list) {
        $count = 1;
        while ( $tour_pax_list && ($row = db_fetch_array($tour_pax_list))) {
            if(array_search($row['id'],$arr_export_pax) === false)
                continue;
            $content_pax_list[] = $row;
            $dob = date('d/m/Y',strtotime($row['dob']));
            $doe = date('d/m/Y',strtotime($row['doe']));

            $data = [
                $count++,
                $row['full_name'],
                (isset($row['gender']) && $row['gender']) ? 'M' : 'F',
                $dob,
                $row['passport_no'],
                $doe,
            ];
            $sheet->fromArray($data, NULL, 'A' . $excel_row++);
        }
    }

    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font' => [
            'size' => 13
        ]
    );

    $objPHPExcel->getActiveSheet()->getStyle('A'.$start_data.':F'.($excel_row-1))->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()
                ->getStyle('B' . ($start_data + 1) . ':B' . ($excel_row - 1))
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objWriter->save($dir.$file_name);
    $content_export['tour'] = $tour;
    $content_export['pax'] = $content_pax_list ;
    $content_export['flight'] = $airport_code;
    $content_export = json_encode($content_export);
    //log export
    $log = TourExportLog::create([
        'tour_id'    => $tour->id,
        'content'    => $content_export,
        'filename'   => $file_name,
        'created_by' => $thisstaff->getId(),
        'created_at' => date('Y-m-d H:i:s')
    ]);
    $log->save();

    $objPHPExcel->disconnectWorksheets();
    unset($objWriter);
    unset($objPHPExcel);

    $file = $dir.$file_name;
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }
    exit;
}
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['tour_id']) && !empty($_REQUEST['tour_id']))
{
    $guest_id= $_REQUEST['id'];
    $tour_id = $_REQUEST['tour_id'];
    $tour = TourNew::lookup($tour_id);
    $tour_datas_ = VisaTourGuest::getPaxFromId($guest_id,$tour_id);
    $tour_datas = db_fetch_array($tour_datas_);
}

if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['phone_number']) && !empty($_POST['phone_number'])){
    $vars = $_POST;
    if($vars){
        try{
            if (!isset($vars['phone_number']))
                throw new Exception('Chưa có số điện thoại');

            if (!\_String::isMobileNumber($vars['phone_number']))
                throw new Exception('Số điện thoại không đúng');

            if(!isset($vars['id-visa-appointment']) || !$vars['id-visa-appointment'])
                throw new Exception('Chưa chọn cuộc hẹn!');


            if(!isset($vars['date-appointment']) || empty($vars['date-appointment'])
                    || !isset($vars['time-appointment']) || empty($vars['time-appointment']))
                throw new Exception('Chưa chọn thời gian cuộc hẹn!');

            if(empty($vars['sms_content']))
                throw new Exception('Chưa có nội dung sms');

            if(!isset($vars['tour_pax_visa_profile_id']) || !$vars['tour_pax_visa_profile_id'])
                throw new Exception('Khách chưa có hồ sơ, vui lòng cập nhật!');

            $time_action_log = date('Y-m-d H:i:s');
            $vars['time_action_log'] = $time_action_log;
            $check_save = saveAppointment($vars);

            if($check_save) {
                $mess = 'Lưu cuộc hẹn thành công';

                if($_POST['send_sms'] === 'send_sms')
                    $send_sms = VisaTourGuest::_sendSMSVisa($vars,$errors,$_POST['id']);

                if(isset($send_sms) && $send_sms)
                    $mess .= ' && Gửi SMS thành công';

                $data_action = \Tugo\VisaFollowAction::countProfileVisa($time_action_log);
                //add data to tugo_osticket_visa
                while ($data_action && ($row = db_fetch_array($data_action))){
                    try{
                        \Tugo\AnlysisVisa::insert($row);
                    }catch (Exception $ex) {
                        echo $ex->getMessage();
                    }
                }

                FlashMsg::set('ticket_create', $mess);
                $referer = isset($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'], '/scp/visa-tour-guest.php') !== false)
                    ? $_SERVER['HTTP_REFERER'] : $cfg->getBaseUrl().'/scp/visa-tour-guest.php';
                header('Location: '. $referer);
                exit();
            }else
                $mess_error = 'Thao tác không thành công, vui lòng thử lại';

        } catch (Exception $ex) {
            $mess_error = $ex->getMessage();
        }
    }
}
if(isset($_POST['sms_phone_number_group']) && !empty($_POST['sms_phone_number_group']) ){
    $vars = $_POST;
    if(!(isset($vars['id-visa-appointment']) && $vars['id-visa-appointment'])) {
        $error_SMS = 'Bạn chưa chọn nội dung cuộc hẹn';
    }
    if(!isset($vars['date-appointment']) || empty($vars['date-appointment'])
        || !isset($vars['time-appointment']) || empty($vars['time-appointment']))
        $error_SMS = 'Chưa chọn thời gian cuộc hẹn!';

    if(empty($vars['sms_phone_number_group']))
        $error_SMS = 'Chưa có nội dung sms';

    if(!isset($error_SMS)) {
        $errors = [];
        $mess = [];
        $send_sms_group = VisaTourGuest::_sendSMSProfileGroup($vars,$errors, $mess);

        if(empty($errors) && empty($errors)) {
        FlashMsg::set('ticket_create', 'Gửi SMS thành công');
        $referer = isset($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'], '/scp/visa-tour-guest.php') !== false)
            ? $_SERVER['HTTP_REFERER'] : $cfg->getBaseUrl() . '/scp/visa-tour-guest.php';
        header('Location: '. $referer);
        exit();
        }
    }
}

$listStaff = Staff::getAll();

if (isset($_REQUEST['layout']) && 'list' === $_REQUEST['layout']
    && isset($_REQUEST['tour']) && $_REQUEST['tour']) {

    $tour_id = $_REQUEST['tour'];
    $tour = TourNew::lookup($tour_id);
    $results = PaxTour::getData($tour_id, true);
    $total = db_num_rows($results);
    $pageNav = new Pagenate($total, 1, 999);
    $pageNav->setURL('visa-tour-guest.php?layout=list&tour=' . $tour_id, $qs ?: null);
    $showing = $pageNav->showing() . ' ' . _N('guest', 'Guests', $total);
    $page = 'visa-tour-guest.inc.php';
    
}
elseif (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && ($_REQUEST['layout'] === 'sms') && ($_REQUEST['send'] === 'send')) {

    $page = 'visa-profile-sms.inc.php';
    $guest_id = $_REQUEST['id'];;
    $tour_id = $_REQUEST['tour_id'];
    $mess = [];
    if(isset($guest_id) && isset($tour_id)){
        $tour = TourNew::lookup($tour_id);
        $paxInfo = VisaTourGuest::getPaxFromId($guest_id,$tour_id);
        if(db_num_rows($paxInfo) === 0)
            $mess[] = 'Không tìm thấy khách';
        else {
            $paxInfo = db_fetch_array($paxInfo);
            if (!$paxInfo['tour_pax_visa_profile_id'])
                $paxInfo['tour_pax_visa_profile_id'] = createProfileVisa($guest_id, $tour_id, $mess);

            $profile_sms_logs = ProfileSMS::getValuesProfileSms($guest_id);
        }
    }
  
    
} elseif (isset($_REQUEST['ticks']) && !empty($_REQUEST['ticks']) && ($_REQUEST['layout'] === 'sms') && ($_REQUEST['send'] === 'send_group')){

    $page = 'visa-profile-group-sms.inc.php';
    $datas_guest_id = [];
    $datas_tour_id = [];
    $pax_tour = explode(",",$_REQUEST['ticks']);
    foreach ($pax_tour as $value){
        $info = explode("_",$value);
        $datas_guest_id[] = $info[0]; //id guest
        $datas_tour_id[] = $info[1]; //tour id

    }

    if ($datas_guest_id && $datas_tour_id){
        $N = count($datas_guest_id);
        $datas_tour_id = array_unique($datas_tour_id);
        $results = VisaTourGuest::getValuesSendGroupSms($datas_guest_id, $datas_tour_id);
    }   

}
elseif (isset($_REQUEST['action']) && $_REQUEST['action'] === 'view_visa_image' && $_REQUEST['file_name'])
{
    echo "<img style=\" display:block  ;margin: 0 auto \" src=\"../visa_upload/".$_REQUEST['file_name'].".png\" alt= \"No find image \" id=\"profile-img-tag\" width= \"50%\ \"/>";
    exit();
}
else {
    $total = 0;
    $p = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
    $offset = ($p - 1) * PAGE_LIMIT;
    $count = DynamicList::objects()->count();
    $results = VisaTourGuest::getPaginationTourGuest($searchDataGuest, $offset, PAGE_LIMIT, $total);
    $pageNav = new Pagenate($total, $p, PAGE_LIMIT);
    $pageNav->setURL('visa-tour-guest.php', $qs ?: null);
    $showing = $pageNav->showing() . ' ' . _N('guest', 'Guests', $total);

    $all_staff = Staff::getAll();
    $all_staff_list = [];
    while ($all_staff && ($row = db_fetch_array($all_staff))) {
        $all_staff_list[ $row['staff_id'] ] = $row['username'];
    }

    $page = "visa-tour-guest-list.inc.php";
}

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);
$booking_type_list = TourDestination::loadList($group_des);

$nav->setTabActive('visa');
//$nav->setActiveSubMenu(4,'visa');
$ost->addExtraHeader('<meta name="tip-namespace" content="visa.visa" />',
    "$('#content').data('tipNamespace', 'visa.visa_tour_list');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

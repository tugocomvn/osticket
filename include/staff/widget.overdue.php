<?php $upcoming = Ticket::getUpcomingOverdue($thisstaff->getId(), UPCOMING_OVERDUE) ?>
<?php $remind = Ticket::getRemindOverdue($thisstaff->getId(), REMIND_OVERDUE) ?>

<?php if($upcoming || $remind): ?>
    <div class="todo-widget side-left-container">
        <div class="side-left">
            <div>
                <span class="widget-title">Overdues <em>[Bấm để thu gọn]</em></span>
            </div>
            <div>
                <h3>Upcoming overdue</h3>
                <ol>
                    <?php while(($row = db_fetch_array($upcoming))): ?>
                        <li><a href="<?php echo $cfg->getUrl()?>scp/new_booking.php?id=<?php echo $row['ticket_id'] ?>"><?php echo $row['number'] ?></a>
                            <?php
                            $time = isset($row['time']) ? floor(intval($row['time'])/3600) : 0;
                            $label = isset($row['time']) && $row['time']/3600 > 12 ? 'warning' : 'danger';
                            ?>
                            <span class="label label-small label-<?php echo $label ?>"><?php echo $time ?></span></li>
                    <?php endwhile; ?>
                </ol>
                <hr>
                <h3 style="color:darkred">Overdue reminder</h3>
                <ol class="reminder">
                    <?php while(($row = db_fetch_array($remind))): ?>
                        <li><a href="<?php echo $cfg->getUrl()?>scp/new_booking.php?id=<?php echo $row['ticket_id'] ?>"><?php echo $row['number'] ?></a></li>
                    <?php endwhile; ?>
                </ol>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            if ($(".side-left-container")) {
                show_expander = 1;

                if (typeof Cookies !== 'undefined' && typeof Cookies.get('show_expander') !== 'undefined')
                    show_expander = Cookies.get('show_expander');

                $(".side-left").jqxExpander({
                    width: '250px',
                    expanded: show_expander
                });

                $('.side-left').on('expanded', function () {
                    Cookies.set('show_expander', 1, {expires: 1/4});
                });

                $('.side-left').on('collapsed', function () {
                    Cookies.set('show_expander', 0, {expires: 1/4});
                });
            }
        });
    </script>
<?php endif; ?>


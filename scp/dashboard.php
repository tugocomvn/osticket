<?php
/*********************************************************************
    dashboard.php

    Staff's Dashboard - basic stats...etc.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.dashboard" />',
    "$('#content').data('tipNamespace', 'dashboard.dashboard');");
require(STAFFINC_DIR.'header.inc.php');
?>

<script type="text/javascript" src="js/bootstrap-tab.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/dashboard.css?v=1.1"/>
<style>
    iframe {
        width: 100%;
    }
</style>

<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffaaff&amp;ctz=Asia%2FHo_Chi_Minh&amp;src=cnFhcDlmOG1la2FyMTk2M2Y2MzNqaG9jNXNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23871111&amp;title=S%E1%BB%B1%20ki%E1%BB%87n%20Tugo&amp;showPrint=0&amp;showTz=0" style="border:solid 1px #777" width="800" height="600" frameborder="0" scrolling="no"></iframe></body>

<?php if($thisstaff->can_do_checkin()): ?>
    <?php include INCLUDE_DIR.'staff/dashboard.staff-checkin.inc.php'; ?>
<?php endif; ?>

<?php if($thisstaff->can_view_all_checkin()): ?>
    <?php include INCLUDE_DIR.'staff/dashboard.all-staff-checkin.inc.php'; ?>
<?php endif; ?>

<?php
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
class TourDestinationModel extends \VerySimpleModel {
    static $meta = [
        'table' => TOUR_DESTINATION_TABLE,
        'pk' => ['id'],
    ];
}

class TourDestination extends TourDestinationModel {
    public static function loadList($access_destination = []) {
        $where = " WHERE 1 ";
        if(isset($access_destination) && is_array($access_destination))
            if(count($access_destination) > 0)
                $where .= " AND id IN (" . implode(",", $access_destination) . ") ";
            else
                $where .= " AND 1 = 0 ";

        $sql = "SELECT * FROM ". self::$meta['table'].$where." ORDER BY name ASC";
        $res = db_query($sql);
        $data = [];
        while($res && ($row = db_fetch_array($res))) {
            $data[ $row['id'] ] = $row;
        }
        return $data;
    }

    public static function getList($access_destination = []) {
        $data = [];
        $where = " WHERE 1 ";
        if(isset($access_destination) && is_array($access_destination))
            if(count($access_destination) > 0)
                $where .= " AND id IN (" . implode(",", $access_destination) . ") ";
            else
                $where .= " AND 1 = 0 ";

        $sql = "SELECT * FROM ". self::$meta['table'].$where." ORDER BY name ASC";
        $res_list = db_query($sql);
        if ($res_list) {
            while(($row = db_fetch_array($res_list))) {
                $data[$row['id']] = $row['name'];
            }
        }
        return $data;
    }


    public static function getDestinationFromMarket ($tour_market = []){
        $destination = [];
        $tour_market[] = 0; //load destination have tour_market_id = 0
        if($tour_market && is_array($tour_market) && count($tour_market) > 0){
            $sql = "SELECT * FROM ". self::$meta['table']." WHERE tour_market_id IN (" . implode(",", $tour_market) . ") ";
            $res = db_query($sql);
            while($res && ($row = db_fetch_array($res))) {
                $destination[] = $row['id'];
            }
        }
        return $destination;
    }

    public static function loadAllDestination($access_destination = []) {

        $sql = "SELECT * FROM ". self::$meta['table']." ORDER BY name ASC";
        $res = db_query($sql);
        $data = [];
        while($res && ($row = db_fetch_array($res))) {
            $data[ $row['id'] ] = $row;
        }
        return $data;
    }
}
?>

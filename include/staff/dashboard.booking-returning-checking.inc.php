<?php global $cfg; ?>
<h2>Checking</h2>
<form method="post" action="<?php echo trim($cfg->getUrl(), '/') ?>/scp/booking-returning-check.php" target="frame">
    <?php csrf_token() ?>
    <input type="text" name="phone_number" class="input-field">
    <button class="btn_sm btn-primary">Search</button>
</form>

<iframe name="frame" frameborder="0" width="100%" height="200px" style="border:none;"></iframe>
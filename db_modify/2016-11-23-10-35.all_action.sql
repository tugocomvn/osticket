CREATE TABLE osticket_dev.ost_all_action
(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    source_id INT,
    source_name ENUM('staff', 'user', 'system', 'api', 'other'),
    action_name VARCHAR(64),
    info TEXT,
    ip VARCHAR(16),
    action_time DATETIME,
    action_time_unix INT
);
CREATE INDEX ost_all_action_action_name_index ON osticket_dev.ost_all_action (action_name);
CREATE INDEX ost_all_action_source_name_index ON osticket_dev.ost_all_action (source_name);
CREATE INDEX ost_all_action_source_id_index ON osticket_dev.ost_all_action (source_id);
CREATE INDEX ost_all_action_action_time_unix_index ON osticket_dev.ost_all_action (action_time_unix);
CREATE INDEX ost_all_action_ip_index ON osticket_dev.ost_all_action (ip);
ALTER TABLE osticket_dev.ost_all_action COMMENT = 'log everything';
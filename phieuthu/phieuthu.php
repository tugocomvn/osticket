<?php
include_once 'get-data.php';
?>
<head>
	<title>Phiếu Thu Online</title>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<style>
*{
	margin: 4px 0px 5px 0px;
	padding: 0px;

}

.logo {
	height: 180px;
	display: flex;
	align-items: center;
}
</style>
<body>
<div class="container">
	<div class="row" >
	  	<div class="col-sm-4 logo" >
			<img src="/tugo-logo-2024.png" width="200" height="100">
		</div>
		<div class="col-sm-8">
		<p><b>CÔNG TY CP DỊCH VỤ VÀ THƯƠNG MẠI TUGO</b></p>
		<p>Địa chỉ:</p>
		<p>&circledast; 219 Lê Thánh Tôn, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh</p>
		<p>&circledast; 121 Lý Tự Trọng, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh</p>
		<p>Giấy phép lữ hành số: 79-726/2017/TCDL-GP LHQT</p>
		<p>Điện thoại: 028 7106 5543. Email: <a href="http://support.tugo.com.vn/">support@tugo.com.vn</a> Website: <a href="https://www.tugo.com.vn/">www.tugo.com.vn</a></p>
		</div>
	</div>
	<hr>
	<div class="receipt_info">
		<div class="row">
			<div class="col-12" style="text-align: center; margin-bottom: 25px;">
				<h3><b>PHIẾU XÁC NHẬN DỊCH VỤ</b></h3>
				<?php $create_at = date('d-m-Y', strtotime($results->created_at)) ?>
				<p><i><?php echo "Ngày ".date('d',strtotime($create_at))." Tháng ".date('m',strtotime($create_at))." Năm ".date('Y',strtotime($create_at)) ?></i></p>
			</div>
			<div class="col-12">
				<p>Mã phiếu thu: <b><?php if(isset($results->receipt_code)) echo $results->receipt_code ?></b></p>
				<p>Mã Booking: <b><?php if(isset($results->booking_code)) echo $results->booking_code ?></b></p>
			</div>
			<div class="col-6">
				<p>Lộ trình: <b><?php if(isset($results->tour_name)) echo $results->tour_name ?></b></p>
			</div>
			<div class="col-6">
				<p>Ngày khởi hành: <b><?php if(isset($results->departure_date) && strtotime($results->departure_date))  echo date('d/m/Y', strtotime($results->departure_date))?></b></p>
			</div>
			<div class="col-6">
				<p>Giá tiền 1 khách (NL): <b><?php if(!empty($results->unit_price_nl)) echo number_format($results->unit_price_nl).'đ' ?></b></p>
			</div>
			<div class="col-6">
				<p>Số lượng: <b><?php if(!empty($results->quantity_nl)) echo $results->quantity_nl ?></b></p>
			</div>
			<div class="col-6">
				<p>Giá tiền 1 khách (TE): <b><?php if(!empty($results->unit_price_te)) echo number_format($results->unit_price_te).'đ' ?></b></p>
			</div>
			<div class="col-6">
				<p>Số lượng: <b><?php if(!empty($results->quantity_te)) echo $results->quantity_te ?></b></p>
			</div>
			<div class="col-6">
				<p>Phí phụ thu: <b><?php if(!empty($results->surcharge)) echo number_format($results->surcharge).'đ' ?></b></p>
			</div>
			<div class="col-6">
				<p>Nội dung phụ thu: <b><?php if(!empty($results->surcharge_note)) echo $results->surcharge_note ?></b></p>
			</div>
			<div class="col-6">
				<p>Tổng số tiền khách cần đóng: <b><?php if(!empty($results->amount_to_be_received)) echo number_format($results->amount_to_be_received). 'đ' ?></b></p>
			</div>
			<div class="col-6">
				<p>Số tiền khách đóng: <b><?php if(!empty($results->amount_received)) echo number_format($results->amount_received). 'đ' ?></b></p>
			</div>
			<div class="col-6">
				<p>Số tiền còn lại: <b><?php if(!empty($results->balance_due)) { echo number_format($results->balance_due).'đ'; } else { echo "0 đồng" ;} ?></b></p>
			</div>
			<div class="col-6">
				<p>Hình thức thanh toán: <b><?php if(!empty($results->payment_method)) echo PaymentMethod::caseTitleName($results->payment_method) ?></b></p>
			</div>
			<div class="col-6">
				<p>Tên khách đại diện: <b><?php if(!empty($results->customer_name)) echo $results->customer_name ?></b></p>
			</div>
			<div class="col-6">
				<p>Số điện thoại: <b><?php if(!empty($results->customer_phone_number)) echo $results->customer_phone_number ?></b></p>
			</div>
			<div class="col-6">
				<p><i>Tên nhân viên kinh doanh: <b><?php if(!empty($results->staff_name)) echo $results->staff_name ?></b></i></p>
			</div>
			<div class="col-6">
				<p><i>Số điện thoại nhân viên kinh doanh: <b><?php if(!empty($results->staff_phone_number)) echo $results->staff_phone_number ?></b></i></p>
			</div>
			<div class="col-6">
				<p><i>Tên nhân viên thu tiền: <b><?php if(!empty($results->staff_cashier_name)) echo $results->staff_cashier_name ?></b></i></p>
			</div>
			<div class="col-6">
				<p><i>Số điện thoại nhân viên thu tiền: <b><?php if(!empty($results->staff_cashier_phone_number)) echo $results->staff_cashier_phone_number ?></b></i></p>
			</div>
			<div class="col-6">
				<p><i>Ngày hoàn tất thanh toán số tiền còn lại: <b>
                <?php if(!empty($results->due_date) && empty($results->due_date_text)): ?>
                    <?php if (strtotime($results->due_date)) echo date('d/m/Y', strtotime($results->due_date)) ?>
                <?php endif; ?>
                <?php if(!empty($results->due_date_text) && empty($results->due_date)): ?>
                    <?php echo trim($results->due_date_text) ?>
                <?php endif; ?>
                </b></i></p>
			</div>
			<div class="col-6">
				<p><b><i>Hotline hỗ trợ: 1900 555 543(24/24)</i></b></p>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
	<div class="col-12">
		<p><i><b><u>** Lưu ý:</u></b></i></p>
			<p><?php echo $results->public_note ?>s</p>
		</div>
		<div class="col-12">
			<p><b>*</b> Quý khách vui lòng thanh toán phí còn lại đúng hạn (nếu có), trường hợp quý khách thanh toán trễ hạn, Tugo sẽ không thể giữ dịch vụ cho quý khách và bắt buộc phải hủy tour mất phí theo quy định.</p>
		</div>
		<div class="col-4">
			<p><b>Ngày tập trung: <?php if(isset($results->time_to_get_in) && strtotime($results->time_to_get_in))  echo date('d/m/Y', strtotime($results->time_to_get_in)) ?> </b></p>
		</div>
		<div class="col-4">
			<p><b>Địa điểm: <?php echo $results->place_to_get_in ?> </b></p>
		</div>
		<div class="col-12">
			<p><b>*</b> Thời gian cần hoàn tất hồ sơ (nếu có): <b><?php if(isset($results->document_due) && strtotime($results->document_due))  echo date('d/m/Y', strtotime($results->document_due)) ?></b></p>
			<p><b>*</b> Thời gian có kết quả visa (đối với các tour cần xin visa): <b><?php if(isset($results->visa_result_estimate_due) && strtotime($results->visa_result_estimate_due))  echo date('d/m/Y', strtotime($results->visa_result_estimate_due)) ?></b></p>
			<p><b>*</b> Trước 1 ngày khởi hành (buổi chiều), HDV sẽ sẽ liên lạc với khách để xác nhận lại toàn bộ thông tin cần thiết.</p>
		</div>
	</div>
	<div class="row">
		<div>
			<p><i><b><u>CHÍNH SÁCH HỦY ĐỔI TOUR:</u></b></i></p>
		</div>
		<div>
			<p><i>Tour khuyến mãi tại Tugo không áp dụng chính sách hủy tour, dời ngày hoặc đổi người đi. Khách sẽ mất 100% phí tour trong các trường hợp trên.</i></p>
		</div>
	</div>
	<div>
	</div>
	<div class="row">
		<div class="col-4" style="text-align:center;">
            <p><b>Người nộp tiền</b></p>
            <?php if ($results->status === Receipt::APPROVED || $results->status === Receipt::WAITING_VERIFYING): ?>
				<span>Đã xác nhận lúc: <?php if(!empty($results->customer_confirmed_at)) echo date('d-m-Y H:i', strtotime($results->customer_confirmed_at)) ?></span>
			<?php else: ?>
				<?php echo "Chưa xác nhận" ?>
			<?php endif ?>
        </div>
		<div class="col-4" style="text-align:center;">
            <p><b>Nhân viên thu tiền</b></p>
            <?php if ($results->status === Receipt::APPROVED || $results->status === Receipt::WAITING_VERIFYING || $results->status === Receipt::MONEY_RECEIVED || $results->status === Receipt::SEND_CODE): ?>
                <span>Đã xác nhận lúc: <?php if(!empty($results->sales_cashier_confirmed_at)) echo date('d-m-Y H:i', strtotime($results->sales_cashier_confirmed_at)) ?></span>
			<?php elseif($results->status === Receipt::REJECTED): ?>
				<span>Đã hủy lúc: <?php if(!empty($results->finance_staff_confirmed_at)) echo date('d-m-Y H:i', strtotime($results->finance_staff_confirmed_at)) ?></span>
			<?php else: ?>
				<?php echo "Chưa xác nhận" ?>
			<?php endif ?>
        </div>
        <div class="col-4" style="text-align:center;">
            <p><b>Kế toán</b></p>
            <?php if ($results->status === Receipt::APPROVED): ?>
				<span>Đã xác nhận lúc: <?php if(!empty($results->finance_staff_confirmed_at)) echo date('d-m-Y H:i', strtotime($results->finance_staff_confirmed_at)) ?></span>
			<?php elseif($results->status === Receipt::REJECTED): ?>
				<span>Đã hủy lúc: <?php if(!empty($results->finance_staff_confirmed_at)) echo date('d-m-Y H:i', strtotime($results->finance_staff_confirmed_at)) ?></span>
			<?php else: ?>
				<?php echo "Chưa xác nhận" ?>
			<?php endif ?>
		</div>
	</div>
	<hr>
</div>
</body>

/*branch: receipt*/

CREATE OR REPLACE VIEW booking_view AS
SELECT *,
       (quantity1 * netprice1 + quantity2 * netprice2 + quantity3 * netprice3 +
        quantity4 * netprice4)                         AS total_net_price,
       ((quantity1 * retailprice1 + quantity2 * retailprice2 + quantity3 * retailprice3 + quantity4 * retailprice4) -
        ((case when discountamount1 is null then 0 else discountamount1 end)
            + (case when discountamount2 is null then 0 else discountamount2 end)
            + (case when discountamount3 is null then 0 else discountamount3 end)
            ))                                         AS total_retail_price,
       ((quantity1 * retailprice1 + quantity2 * retailprice2 + quantity3 * retailprice3 + quantity4 * retailprice4) -
        (quantity1 * netprice1 + quantity2 * netprice2 + quantity3 * netprice3 + quantity4 * netprice4) -
        ((case when discountamount1 is null then 0 else discountamount1 end)
            + (case when discountamount2 is null then 0 else discountamount2 end)
            + (case when discountamount3 is null then 0 else discountamount3 end)
            ))                                         AS total_profit,
       (quantity1 + quantity2 + quantity3 + quantity4) AS total_quantity
FROM booking_tmp;


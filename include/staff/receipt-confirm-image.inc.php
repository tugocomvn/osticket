<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canApproveReceipt() || !$thisstaff->canViewReceipt()) exit('Access Denied');
?>

<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>">Back</a>
<h2 class="centered">Ảnh phiếu thu</h2>
    <?php csrf_token(); ?>
    <?php if(isset($errors) && $errors): ?>
        <div id="msg_error"><?php echo $errors ?></div>
    <?php endif; ?>
    <input type="hidden" name="id" value="<?php echo $result->id ?>">
    <p class="centered">Mã phiếu thu: <b><?php echo $result->receipt_code ?></b></p>
    <p class="centered">Mã booking code: <b><?php echo $result->booking_code ?></b></p>
    <p class="centered">
        <?php if (isset($result->status)): ?>  
            <?php if ($result->status == 5): ?>
                <span class="label label-small label-success">Kế toán xác nhận phiếu thu</i></span>
            <?php endif; ?>
            <?php if ($result->status == 6): ?>
                <span class="label label-small label-danger">Đã hủy</i></span>
            <?php endif; ?>
        <?php endif; ?>
    </p>
    <div class="centered">
        <img src="<?php if(isset($name_image)) echo '../public_file/'.$name_image?>" alt="No find image" id="profile-img-tag" width="80%"/>
    </div>

    <?php if($thisstaff->canApproveReceipt() && !empty($name_image) && ($result->status !== Receipt::APPROVED)): ?>
        <p class="centered"><button class="btn_sm btn-success image" type="submit" id="confirm_image">Xác nhận</button></p>
    <?php endif;?>

<script>
    $(document).ready(function(){
        $('#confirm_image').click(function()
        {
            if (!confirm('Bạn đã kiểm tra kĩ thông tin chưa ?')) {
                e.preventDefault();
                return false;
            }else{
                var id = $('input[name=id]').val();
                $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/confirm/confirmImage',
                    data: {"receipt_id":id},
                    success : function (res){
                        if(res.result == 1){
                            alert('Xác nhận hình ảnh phiếu thu thành công');
                            <?php $url = $cfg->getUrl().'scp/receiptlist.php'; ?>
                            parent.setTimeout(function() { parent.window.location = "<?php echo $url  ?>"; }, 1500);
                        }else{
                            alert('Xác nhận không thành công');
                        }
                    },
                    error : function (){
                        alert('Có lỗi xảy ra trong quá trình xử lý');
                    }
                });
            }
        });
    });
</script>

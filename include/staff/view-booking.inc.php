<?php
//Note that ticket obj is initiated in tickets.php.
if (!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) {
    die('Invalid path');
}
//Make sure the staff is allowed to access the page.
if (!@$thisstaff->isStaff() || !$ticket->checkStaffAccess($thisstaff) || !$thisstaff->canViewBookings()) {
    die('Access Denied');
}

//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info = ($_POST && $errors) ? Format::input($_POST) : array();

//Auto-lock the ticket if locking is enabled.. If already locked by the user then it simply renews.
if ($cfg->getLockTime() && !$ticket->acquireLock($thisstaff->getId(), $cfg->getLockTime())) {
    $warn .= __('Unable to obtain a lock on the ticket');
}

//Get the goodies.
$dept = $ticket->getDept();  //Dept
$staff = $ticket->getStaff(); //Assigned or closed by..
$user = $ticket->getOwner(); //Ticket User (EndUser)
$team = $ticket->getTeam();  //Assigned team.
$sla = $ticket->getSLA();
$lock = $ticket->getLock();  //Ticket lock obj
$_id = $ticket->getId();    //Ticket ID.

$partner = DynamicListItem::objects()->filter(['list_id' => PARTNER_LIST])->order_by('value');
$status = DynamicListItem::objects()->filter(['list_id' => BOOKING_STATUS_LIST])->order_by('value');
$code = DynamicListItem::objects()->filter(['list_id' => NO_ANSWER_1ST_STATUS])->order_by('value');

$list_code = [];
foreach ($code as $data){
    $list_code[$data->id] = $data->value;
}

$list_partner = [];
foreach ($partner as $data){
    $list_partner[$data->id] = $data->value;
}

$list_status = [];
foreach ($status as $data){
    $list_status[$data->id] = $data->value;
}
$allStaff = Staff::getAllName();
$StaffName = array();

//convert staff[id] = name
while (($row = db_fetch_array($allStaff)) && $allStaff)
{
    $StaffName[$row['staff_id']] = $row['name'];
}
//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable()) {
    $warn = sprintf(
        __('Current ticket status (%s) does not allow the end guest to reply.'),
        $ticket->getStatus());
} elseif ($ticket->isAssigned()
    && (($staff && $staff->getId() != $thisstaff->getId())
        || ($team && !$team->hasMember($thisstaff))
    )) {
    $warn .= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
        sprintf(__('Ticket is assigned to %s'),
            implode('/', $ticket->getAssignees())
        ));
}

if (!$errors['err']) {

    if ($lock && $lock->getStaffId() != $thisstaff->getId()) {
        $errors['err'] = sprintf(__('This ticket is currently locked by %s'),
            $lock->getStaffName());
    } elseif (($emailBanned = TicketFilter::isBanned($ticket->getEmail()))) {
        $errors['err'] = __('Email is in banlist! Must be removed before any reply/response');
    }
//    elseif (!Validator::is_valid_email($ticket->getEmail()))
//        $errors['err'] = __('EndGuest email address is not valid! Consider updating it before responding');
}

$unbannable = ($emailBanned) ? BanList::includes($ticket->getEmail()) : false;
//$booking = Booking::get_booking_view($_id);
$booking = Booking::lookup((int)$_REQUEST['id']);
$booking_list_type = TourDestination::loadAllDestination();

if(isset($booking_list_type[$booking->booking_type_id]))
    $booking_type = $booking_list_type[$booking->booking_type_id]['name'];
else $booking_type = '';
$booking_code = Booking::fromTicketId($ticket->getId());
$points = UserPoint::fromBookingCode($booking_code);
$booking_action = BookingAction::lookup($ticket->getId());
$total_quantity = (int)$booking->quantity1 + (int)$booking->quantity2 + (int)$booking->quantity3 + (int)$booking->quantity4;
$total_net_price = (int)$booking->quantity1*$booking->netprice1 + (int)$booking->quantity2*$booking->netprice2
                        + (int)$booking->quantity3*$booking->netprice3 + (int)(int)$booking->quantity4*$booking->netprice4;
$total_retail_price = (int)$booking->quantity1*$booking->retailprice1 + (int)$booking->quantity2*$booking->retailprice2
                        + (int)$booking->quantity3*$booking->retailprice3 + (int)$booking->quantity4*$booking->retailprice4
                        - ((int)$booking->discountamount1 + (int)$booking->discountamount2 
                            + (int)$booking->discountamount3 );

?>
<table width="1058" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td width="20%" class="has_bottom_border">
            <h2><a href="<?php echo $cfg->getUrl(); ?>scp/new_booking.php?id=<?php echo $ticket->getId(); ?>"
                   title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
                    <?php echo 'Booking: '.$booking_code ?></a></h2>
        </td>

        <td class="has_bottom_border">
            <?php if ($booking_action): ?>
                <em>Đã gửi email booking <i class="icon-ok"></i></em>
            <?php endif; ?>
        </td>

        <td width="auto" class="flush-right has_bottom_border">
            <?php
            if ($thisstaff->canBanEmails()
                || $thisstaff->canEditTickets()
                || ($dept && $dept->isManager($thisstaff))) { ?>
                <span class="action-button pull-right" data-dropdown="#action-dropdown-more">
                <i class="icon-caret-down pull-right"></i>
                <span><i class="icon-cog"></i> <?php echo __('More'); ?></span>
            </span>
                <?php
            }

            if ($thisstaff->canEditBookings()) { ?>
                <a class="action-button pull-right no-pjax" target="_blank"
                   href="<?php echo $cfg->getUrl() ?>scp/new_booking.php?id=<?php echo $ticket->getId(); ?>&action=edit"><i
                            class="icon-edit"></i> <?php
                    echo __('Edit'); ?></a>
                <?php
            }
            if ($ticket->isOpen()
                && !$ticket->isAssigned()
                && $thisstaff->canAssignTickets()
                && $ticket->getDept()->isMember($thisstaff)) { ?>
                <a id="ticket-claim" class="action-button pull-right confirm-action" href="#claim"><i
                            class="icon-user"></i> <?php
                    echo __('Claim'); ?></a>

                <?php
            } ?>
            <span class="action-button pull-right" data-dropdown="#action-dropdown-print">
                <i class="icon-caret-down pull-right"></i>
                <a id="ticket-print" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print"><i
                            class="icon-print"></i> <?php
                    echo __('Print'); ?></a>
            </span>
            <div id="action-dropdown-print" class="action-dropdown anchor-right">
                <ul>
                    <li><a class="no-pjax" target="_blank"
                           href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=0"><i
                                    class="icon-file-alt"></i> <?php echo __('Ticket Thread'); ?></a>
                    <li><a class="no-pjax" target="_blank"
                           href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=1"><i
                                    class="icon-file-text-alt"></i> <?php echo __('Thread + Internal Notes'); ?></a>
                </ul>
            </div>
            <div id="action-dropdown-more" class="action-dropdown anchor-right">
                <ul>
                    <?php
                    if ($thisstaff->canDeleteTickets()) {
                        ?>
                        <li><a class="ticket-action" href="#tickets/<?php
                            echo $ticket->getId(); ?>/status/delete"
                               data-href="tickets.php"><i class="icon-trash"></i> <?php
                                echo __('Delete Ticket'); ?></a></li>
                        <?php
                    } ?>
                    <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                        ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                        ><i class="icon-paste"></i> <?php echo __('Manage Forms'); ?></a></li>
                    <?php
                    if (!$booking_action) {
                        ?>
                        <li>
                            <a href="#ajax.php/bookings/<?php echo $ticket->getId();
                            ?>/finish" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                            ><i class="icon-mail-reply"></i> <?php echo __('Finish and Send email'); ?></a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li><a href="#"><em>Đã gửi email booking</em></a></li><?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="1058" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Status'); ?>:</th>
                    <td><?php echo $ticket->getStatus(); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Priority'); ?>:</th>
                    <td><?php echo $ticket->getPriority(); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Department'); ?>:</th>
                    <td><?php echo Format::htmlchars($ticket->getDeptName()); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Date'); ?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getCreateDate()); ?></td>
                </tr>
            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <tr>
                    <th width="100"><?php echo __('Help Topic'); ?>:</th>
                    <td><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Message'); ?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastMsgDate()); ?></td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Response'); ?>:</th>
                    <td><?php echo Format::db_datetime($ticket->getLastRespDate()); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="4" width="1058" border="0">
    <tbody>
    <tr>
        <th width="200px">Mã Booking:</th>
        <td><?php echo $booking->booking_code ?></td>
    </tr>
    <tr>
        <th>Mã Receipt:</th>
        <td><?php echo $booking->receipt_code ?></td>
    </tr>
    <tr>
        <th>Thông tin khách:</th>
        <td><?php echo '<span style="white-space: pre-line">'.$booking->customer.'</span>' ?></td>
    </tr>
    <tr>
        <th>Số điện thoại:</th>
        <td><?php echo '<span style="white-space: pre-line">'.$booking->phone_number.'</span>' ?></td>
    </tr>
    <tr>
        <th>Loại Booking:</th>
        <td><?php echo $booking_type ?></td>
    </tr>
    <tr>
        <th>Ngày khởi hành:</th>
        <td><?php echo date('d/m/Y', $booking->dept_date) ?></td>
    </tr>
    <tr>
        <th>Đối tác:</th>
        <td><?php
            $_partner='';
            if(is_numeric($booking->partner))
                $_partner = $list_partner[(int)$booking->partner];
            elseif(strlen(($_partner=_String::json_decode($booking->partner))))
                $_partner=_String::json_decode($booking->partner);
            echo $_partner;
            ?>
        </td>
    </tr>
    <tr>
        <th>Nhân viên phụ trách:</th>
        <td><?php echo $booking->staff ?></td>
    </tr>
    <tr>
        <th>Ngày full CỌC:</th>
        <td><?php echo date('d/m/Y', $booking->fullcoc_date) ?></td>
    </tr>
    <tr>
        <th>Ngày full PAY:</th>
        <td><?php echo date('d/m/Y', $booking->fullpay_date) ?></td>
    </tr>

    <?php if(isset($booking->quantity1) && (int)$booking->quantity1>0): ?>
        <tr>
            <th>Số lượng người lớn:</th>
            <td><?php echo $booking->quantity1 ?></td>
        </tr>
        <tr>
            <th>Giá bán 1 người lớn:</th>
            <td><?php echo number_format($booking->retailprice1).'đ'; ?></td>
        </tr>
        <tr>
            <th>Giá net 1 người lớn:</th>
            <td><?php echo number_format($booking->netprice1).'đ'; ?></td>
        </tr>
        <tr>
            <th>Ghi chú giá người lớn</th>
            <td><?php echo $booking->note1 ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->quantity2) && (int)$booking->quantity2>0): ?>
        <tr>
            <th>Số lượng trẻ em:</th>
            <td><?php echo $booking->quantity2 ?></td>
        </tr>
        <tr>
            <th>Giá bán 1 trẻ em:</th>
            <td><?php echo number_format($booking->retailprice2).'đ'; ?></td>
        </tr>
        <tr>
            <th>Giá net 1 trẻ em:</th>
            <td><?php echo number_format($booking->netprice2).'đ'; ?></td>
        </tr>
        <tr>
            <th>Ghi chú giá trẻ em</th>
            <td><?php echo $booking->note2 ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->quantity3) && (int)$booking->quantity3>0): ?>
        <tr>
            <th>Số lượng phụ thu:</th>
            <td><?php echo $booking->quantity3 ?></td>
        </tr>
        <tr>
            <th>Giá bán 1 khách phụ thu:</th>
            <td><?php echo number_format($booking->retailprice3).'đ'; ?></td>
        </tr>
        <tr>
            <th>Giá net 1 khách phụ thu:</th>
            <td><?php echo number_format($booking->netprice3).'đ'; ?></td>
        </tr>
        <tr>
            <th>Ghi chú phụ thu</th>
            <td><?php echo $booking->note3 ?></td>
        </tr>
    <?php endif; ?>


    <?php if(isset($booking->quantity4) && (int)$booking->quantity4>0): ?>
        <tr>
            <th>Số lượng giá khác :</th>
            <td><?php echo $booking->quantity4 ?></td>
        </tr>
        <tr>
            <th>Giá bán 1 khách giá khác:</th>
            <td><?php echo number_format($booking->retailprice4).'đ'; ?></td>
        </tr>
        <tr>
            <th>Giá net 1 khách giá khác:</th>
            <td><?php echo number_format($booking->netprice4).'đ'; ?></td>
        </tr>
        <tr>
            <th>Ghi chú giá khác</th>
            <td><?php echo $booking->note4 ?></td>
        </tr>
    <?php endif; ?>
    <?php if(isset($booking->discountamount1) && (int)$booking->discountamount1 > 0): ?>
        <tr>
            <th>Loại giảm 1</th>
            <td><?php echo $booking->discounttype1 ?></td>
        </tr>
        <tr>
            <th>Giá giảm 1</th>
            <td><?php echo number_format($booking->discountamount1).'đ' ?></td>
        </tr>
    <?php endif; ?>
    <?php if(isset($booking->discountamount2) && (int)$booking->discountamount2 > 0): ?>
        <tr>
            <th>Loại giảm 2</th>
            <td><?php echo $booking->discounttype2 ?></td>
        </tr>
        <tr>
            <th>Giá giảm 2</th>
            <td><?php echo number_format($booking->discountamount2).'đ' ?></td>
        </tr>
    <?php endif; ?>
    <?php if(isset($booking->discountamount3) && (int)$booking->discountamount3 > 0): ?>
        <tr>
            <th>Loại giảm 3</th>
            <td><?php echo $booking->discounttype3 ?></td>
        </tr>
        <tr>
            <th>Giá giảm 3</th>
            <td><?php echo number_format($booking->discountamount3).'đ' ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->twin_room) && (int)$booking->twin_room>0): ?>
        <tr>
            <th>Twin:</th>
            <td><?php echo $booking->twin_room ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->double_room) && (int)$booking->double_room>0): ?>
        <tr>
            <th>Double:</th>
            <td><?php echo $booking->double_room ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->single_room) && (int)$booking->single_room>0): ?>
        <tr>
            <th>Single:</th>
            <td><?php echo $booking->single_room ?></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($booking->triple_room) && (int)$booking->triple_room>0): ?>
        <tr>
            <th>Triple:</th>
            <td><?php echo $booking->triple_room ?></td>
        </tr>
    <?php endif; ?>

    <?php if(!empty($booking->other_room)): ?>
        <tr>
            <th>Khác:</th>
            <td><?php echo $booking->other_room ?></td>
        </tr>
    <?php endif; ?>

    <tr>
        <th>Trạng thái:</th>
        <td><?php $status_booking = '';
            if(is_numeric($booking->status))
                $status_booking = $list_status[$booking->status];
            elseif(strlen(($_partner=_String::json_decode($booking->status))))
                $status_booking =_String::json_decode($booking->status);
            echo $status_booking;
            ?>
        </td>
    </tr>
    <tr>
        <th>Ghi chú</th>
        <td>
            <?php echo $booking->note ?>
        </td>
    </tr>
    <?php if(isset($fileNameCommitment) && $fileNameCommitment): ?>
        <tr>
            <th>File cam kết</th>
            <td><a class="no-pjax btn_sm btn-success btn-xs" target="_blank"
                   href="<?php echo $cfg->getUrl().'scp/new_booking.php?id='.$booking->ticket_id.'&action=down_commitment'?>">
                    Download</a>
            </td>
        </tr>
    <?php endif;?>
    <?php if(isset($fileNameContract) && $fileNameContract): ?>
        <tr>
            <th>File hợp đồng</th>
            <td><a class="no-pjax btn_sm btn-success btn-xs" target="_blank"
                   href="<?php echo $cfg->getUrl().'scp/new_booking.php?id='.$booking->ticket_id.'&action=down_contract'?>">
                    Download</a>
            </td>
        </tr>
    <?php endif;?>
    <tr>
        <table cellspacing="5" cellpadding="4" width="100%" border="0">
            <tr>
                <td><strong>Tổng giá bán: </strong> <?php echo number_format($total_retail_price).'đ'; ?>
                </td>
                <td><strong>Tổng giá net: </strong> <?php echo number_format($total_net_price).'đ'; ?>
                </td>
                <td><strong>Tổng lợi nhuận: </strong> <?php echo number_format($total_retail_price - $total_net_price).'đ'; ?>
                </td>
            </tr>
        </table>
    </tr>
    </tbody>
</table>
<div class="clear"></div>
<h2 style="padding:10px 0 5px 0; font-size:11pt;"><?php echo Format::htmlchars($ticket->getSubject()); ?></h2>
<?php
$thread_new = BookingLog::objects()->filter(['ticket_id' => (int)$_REQUEST['id']])->order_by('time');
$tcount = $ticket->getThreadCount();
$tcount += $ticket->getNumNotes();
?>
<ul id="threads">
    <li><a class="active" id="toggle_ticket_thread" href="#"><?php echo sprintf(__('Booking Logs (%d)'),
                $thread_new->count() + $tcount); ?></a></li>
</ul>
<div id="ticket_thread">
    <?php
    $count_reply = 0;
    $threadTypes = array('M' => 'message', 'R' => 'response', 'N' => 'note', 'S' => 'sms');
    /* -------- Messages & Responses & Notes (if inline)-------------*/
    $types = array('M', 'R', 'N', 'S');

    if(($thread=$ticket->getThreadEntries($types))) {
        foreach($thread as $entry) { ?>
            <?php if('R' == $entry['thread_type']) $count_reply++; ?>
            <table class="thread-entry <?php echo $threadTypes[$entry['thread_type']]; ?>" cellspacing="0" cellpadding="1" width="1058" border="0">
                <tr>
                    <th colspan="4" width="100%">
                        <div>
                    <span class="pull-left">
                    <span style="display:inline-block"><?php
                        echo Format::db_datetime($entry['created']);?></span>
                    <span style="display:inline-block;padding:0 1em" class="faded title"><?php
                        echo Format::truncate($entry['title'], 100); ?></span>
                    </span>
                            <span class="pull-right" style="white-space:no-wrap;display:inline-block">
                        <span style="vertical-align:middle;" class="textra"></span>
                        <span style="vertical-align:middle;"
                              class="tmeta faded title"><?php
                            echo Format::htmlchars($entry['name'] ?: $entry['poster']); ?></span>
                    </span>
                        </div>
                    </th>
                </tr>
                <tr><td colspan="4" class="thread-body" id="thread-id-<?php
                    echo $entry['id']; ?>"><div><?php
                            echo $entry['body']->toHtml(); ?></div></td></tr>
                <?php
                if($entry['attachments']
                    && ($tentry = $ticket->getThreadEntry($entry['id']))
                    && ($urls = $tentry->getAttachmentUrls())
                    && ($links = $tentry->getAttachmentsLinks())) {?>
                    <tr>
                        <td class="info" colspan="4"><?php echo $tentry->getAttachmentsLinks(); ?></td>
                    </tr> <?php
                }
                if ($urls) { ?>
                    <script type="text/javascript">
                        $('#thread-id-<?php echo $entry['id']; ?>')
                            .data('urls', <?php
                                echo JsonDataEncoder::encode($urls); ?>)
                            .data('id', <?php echo $entry['id']; ?>);
                    </script>
                    <?php
                } ?>
            </table>
            <?php
            if($entry['thread_type']=='M')
                $msgId=$entry['id'];
        }
    }

    if ($thread_new):
        foreach ($thread_new as $data):
            $content = json_decode($data->content, true);
//            $total_quantity = (int)$content['quantity1'] + (int)$content['quantity2'] + (int)$content['quantity3'] + (int)$content['quantity4'];
//            $total_net_price = (int)$content['quantity1']*$content['netprice1'] + (int)$content['quantity2']*$content['netprice2']
//                + (int)$content['quantity3']*$content['netprice3'] + (int)$content['quantity4']*$content['netprice4'];
//            $total_retail_price = (int)$content['quantity1']*$content['retailprice1'] + (int)$content['quantity2']*$content['retailprice2']
//                + (int)$content['quantity3']*$content['retailprice3'] + (int)$content['quantity4']*$content['retailprice4'];

            ?>
            <table class="thread-entry note" cellspacing="0" cellpadding="1" width="1058" border="0">
                <tr>
                    <th colspan="4" width="100%">
                        <div>
                            <span class="pull-left">
                                <?php echo date('d/m/Y H:i:s', strtotime($data->time)) ?>
                            </span>
                            <span class="pull-right">
                                <?php echo $StaffName[$data->staff_id] ?>
                            </span>
                        </div>
                    </th>
                </tr>
                <tbody class="booking-log">
                <tr>
                    <td width="200px">Mã Booking:</td>
                    <td><?php echo $content['booking_code'] ?></td>
                </tr>
                <tr>
                    <td>Mã Ticket:</td>
                    <td><?php echo $content['ticket_number'] ?></td>
                </tr>
                <tr>
                    <td>Mã Receipt:</td>
                    <td><?php echo $content['receipt_code'] ?></td>
                </tr>
                <tr>
                    <td>Thông tin khách:</td>
                    <td><?php echo '<span style="white-space: pre-line">'.$content['customer'].'</span>' ?></td>
                </tr>
                <tr>
                    <td>Số điện thoại:</td>
                    <td><?php echo '<span style="white-space: pre-line">'.$content['phone_number'].'</span>' ?></td>
                </tr>
                <tr>
                    <td>Loại Booking:</td>
                    <td><?php echo (isset($booking_list_type[$content['booking_type_id']]) && $booking_list_type[$content['booking_type_id']]['name'])
                            ?$booking_list_type[$content['booking_type_id']]['name']:'' ?></td>
                </tr>
                <tr>
                    <td>Ngày khởi hành:</td>
                    <td><?php echo date('d/m/Y', $content['dept_date']) ?></td>
                </tr>
                <tr>
                    <td>Đối tác:</td>
                    <td><?php echo $list_partner[$content['partner']] ?></td>
                </tr>
                <tr>
                    <td>Nhân viên phụ trách:</td>
                    <td><?php echo $content['staff'] ?></td>
                </tr>
                <tr>
                    <td>Ngày full CỌC:</td>
                    <td><?php echo date('d/m/Y', $content['fullcoc_date']) ?></td>
                </tr>
                <tr>
                    <td>Ngày full PAY:</td>
                    <td><?php echo date('d/m/Y', $content['fullpay_date']) ?></td>
                </tr>
                <tr>
                    <td>Số lượng người lớn:</td>
                    <td><?php echo $content['quantity1'] ?></td>
                </tr>
                <tr>
                    <td>Giá bán 1 người lớn:</td>
                    <td><?php echo number_format($content['retailprice1']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Giá net 1 người lớn:</td>
                    <td><?php echo number_format($content['netprice1']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Ghi chú giá người lớn</td>
                    <td><?php echo $content['note1']?></td>
                </tr>
                <tr>
                    <td>Số lượng trẻ em:</td>
                    <td><?php echo $content['quantity2'] ?></td>
                </tr>
                <tr>
                    <td>Giá bán 1 trẻ em:</td>
                    <td><?php echo number_format($content['retailprice2']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Giá net 1 trẻ em:</td>
                    <td><?php echo number_format($content['netprice2']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Ghi chú giá trẻ em</td>
                    <td><?php echo $content['note2']?></td>
                </tr>
                <tr>
                    <td>Số lượng phụ thu:</td>
                    <td><?php echo $content['quantity3'] ?></td>
                </tr>
                <tr>
                    <td>Giá bán 1 khách phụ thu:</td>
                    <td><?php echo number_format($content['retailprice3']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Giá net 1 khách phụ thu:</td>
                    <td><?php echo number_format($content['netprice3']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Ghi chú phụ thu</td>
                    <td><?php echo $content['note3']?></td>
                </tr>
                <tr>
                    <td>Số lượng giá khác :</td>
                    <td><?php echo $content['quantity4'] ?></td>
                </tr>
                <tr>
                    <td>Giá bán 1 khách giá khác:</td>
                    <td><?php echo number_format($content['retailprice4']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Giá net 1 khách giá khác:</td>
                    <td><?php echo number_format($content['netprice4']).'đ'; ?></td>
                </tr>
                <tr>
                    <td>Ghi chú giá khác</td>
                    <td><?php echo $content['note4']?></td>
                </tr>
                <tr>
                    <td>Loại giảm 1</td>
                    <td><?php echo $content['discounttype1']?></td>
                </tr>
                <tr>
                    <td>Giá giảm 1</td>
                    <td><?php echo number_format($content['discountamount1']).'đ' ?></td>
                </tr>
                <tr>
                    <td>Loại giảm 2</td>
                    <td><?php echo $content['discounttype2']?></td>
                </tr>
                <tr>
                    <td>Giá giảm 2</td>
                    <td><?php echo number_format($content['discountamount2']).'đ' ?></td>
                </tr>
                <tr>
                    <td>Loại giảm 3</td>
                    <td><?php echo $content['discounttype3']?></td>
                </tr>
                <tr>
                    <td>Giá giảm 3</td>
                    <td><?php echo number_format($content['discountamount3']).'đ' ?></td>
                </tr>
                <tr>
                    <td>Twin:</td>
                    <td><?php echo $content['twin_room'] ?></td>
                </tr>
                <tr>
                    <td>Double:</td>
                    <td><?php echo $content['double_room'] ?></td>
                </tr>
                <tr>
                    <td>Khác:</td>
                    <td><?php echo $content['other_room'] ?></td>
                </tr>
                <tr>
                    <td>Trạng thái:</td>
                    <td><?php echo $list_status[$content['status']] ?></td>
                </tr>
                <tr>
                    <td>Ghi chú:</td>
                    <td><?php echo $content['note'] ?></td>
                </tr>
                <tr>
                    <td>Mã giới thiệu:</td>
                    <td><?php echo $list_code[$content['referral_code']] ?></td>
                </tr>
                </tbody>
            </table>
        <?php endforeach;?>
    <?php endif;

    ?>
</div>
<div class="clear" style="padding-bottom:10px;"></div>
<script>
    (function ($) {
        $('.thread-body>div,.booking-log').readmore({
            speed: 20,
            collapsedHeight: 200,
            moreLink: '<a href="#" class="read_more_link">Read more</a>',
            lessLink: '<a href="#" class="read_more_link">Close</a>',
            blockCSS: 'display: block; width: 100%; margin-bottom: 1em !important;'
        });
    })(jQuery);
</script>

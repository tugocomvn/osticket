CREATE TABLE IF NOT EXISTS `fnd_flight_ticket_flight` (
  `flight_ticket_id` INT NOT NULL,
  `flight_id` INT NOT NULL,
  PRIMARY KEY(flight_ticket_id, flight_id),
  INDEX `index1` (`flight_ticket_id` ASC),
  INDEX `index2` (`flight_id` ASC))
ENGINE = InnoDB;

<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');
$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');
?>
<div id="landing_page">
    <?php
    if($cfg && ($page = $cfg->getLandingPage()))
        echo $page->getBodyWithImages();
    else
        echo  '<h1>'.__(COMPANY_TITLE.' kính chào quý khách').'</h1>';
    ?>
    <div id="new_ticket" class="pull-left">
        <h3><?php echo __('Tôi cần được hỗ trợ!');?></h3>
        <br>
        <div><?php echo __('Hãy bấm nút bên dưới. Để dễ dàng theo dõi và được hỗ trợ tốt nhất, quý khách vui lòng đăng nhập theo thông tin do nhân viên '.COMPANY_TITLE.' cung cấp.');?></div>
    </div>

    <div id="check_status" class="pull-right">
        <h3><?php echo __('Nhân viên đã xử lý đến đâu?');?></h3>
        <br>
        <div><?php echo __('Lịch sử các lần hỗ trợ, trạng thái và nội dung xử lý của từng trường hợp.');?></div>
    </div>

    <div class="clear"></div>
    <div class="front-page-button pull-left">
        <p>
            <a href="open.php" class="btn_sm btn-primary"><span class="icon-plus"></span> <?php echo __('Tạo phiếu hỗ trợ');?></a>
        </p>
    </div>
    <div class="front-page-button pull-right">
        <p>
            <a href="<?php if(is_object($thisclient)){ echo 'tickets.php';} else {echo 'tickets.php';}?>" class="btn_sm btn-success"><span class="icon-search"></span> <?php echo __('Kiếm tra');?></a>
        </p>
    </div>
</div>
<div class="clear"></div>
<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p><?php echo sprintf(
    __('Be sure to browse our %s before opening a ticket'),
    sprintf('<a href="kb/index.php">%s</a>',
        __('Frequently Asked Questions (FAQs)')
    )); ?></p>
</div>
<?php
} ?>
<?php require(CLIENTINC_DIR.'footer.inc.php'); ?>

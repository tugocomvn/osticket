<?php

use \koolreport\widgets\google\ColumnChart;
use \koolreport\widgets\google\PieChart;
$check_empty = 0;
?>

<?php
if(!empty($this->dataStore('statistic_age_pax')->data())){

   $results =  $this->dataStore('statistic_age_pax')->data()[0];
   $arr_data=[];
   foreach ($results as $key=>$value){
       if($value)
            $arr_data[] = array ("group_age"=>$key,"quantity"=>$value);
   }
   if(!empty($arr_data))
   {
       ColumnChart::create(array(
           "title" => "Age",
           "width" => "100%",
           "height" => "500px",
           "dataSource" => $arr_data,

           "columns" => array(
               "group_age",
               "quantity"
           ),

       ));
   }else{
       $check_empty +=1;
   }

}
?>

<?php
if(!empty($this->dataStore('statistic_gender_pax')->data())) {
    PieChart::create(array(
        "title" => "Gender",
        "dataSource" => $this->dataStore("statistic_gender_pax"),
        "width" => "100%",
        "height" => "500px",
        "columns" => array(
            "gender",
            "amount" => array("type" => "number")
        ),
    ));
}else{
    $check_empty +=1;
}
?>

<?php if($check_empty === 2): ?>
    <h2>No data in datetime you choose</h2>
<?php endif;?>

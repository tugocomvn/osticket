alter table ost_groups drop column can_view_approve;
ALTER TABLE `ost_groups` ADD `can_view_approve_leader` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_view_approve_operator` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ost_groups` ADD `can_view_approve_visa` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
<div class="card card-outline card-purple">
    <div class="card-body">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-info active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> Day
            </label>
            <label class="btn btn-info">
                <input type="radio" name="options" id="option2" autocomplete="off"> Week
            </label>
            <label class="btn btn-info">
                <input type="radio" name="options" id="option3" autocomplete="off"> Month
            </label>
            <label class="btn btn-info">
                <input type="radio" name="options" id="option4" autocomplete="off"> Year
            </label>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php
class GeneralObject {
    public static function getValue($const) {
        return constant(__CLASS__.'::'.$const);
    }

    public static function getConstants() {
        $oClass = new \ReflectionClass(get_called_class());
        return $oClass->getConstants();
    }

    public static function caseTitleName($id = null) {
        $list = static::getConstants();
        $result = [];
        foreach($list as $index => $item) {
            $index = str_replace('_', ' ', $index);
            $tmp = mb_convert_case($index, MB_CASE_TITLE);

            if (!is_null($id) && $id == $item) {
                return $tmp;
            }

            $result[$item] = $tmp;
        }

        return $result;
    }
}

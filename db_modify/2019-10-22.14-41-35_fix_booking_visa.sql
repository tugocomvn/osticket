create table tour_flight
(
   id int auto_increment primary key,
   tour_id int null,
   flight_id int null,
   flight_ticket_code varchar(20) null
);

create table flight
(
  id int auto_increment primary key,
  departure_flight_code   varchar(10) null,
  return_flight_code   varchar(10) null,
  departure_flight_code_b   varchar(10) null,
  return_flight_code_b   varchar(10) null,
  flight_time_stage_1 datetime null,
  flight_time_arrives_stage_1 datetime null,
  flight_time_stage_2 datetime null,
  flight_time_arrives_stage_2 datetime null,
  flight_time_stage_1_b datetime null,
  flight_time_arrives_stage_1_b datetime null,
  flight_time_stage_2_b datetime null,
  flight_time_arrives_stage_2_b datetime null
);
ALTER TABLE ost_tour ADD color_codes varchar(11) NULL;

ALTER TABLE receipt ADD internal_notes text null;

alter table receipt
	add image_name text null;


ALTER TABLE booking_reservation ADD leader_approve_hold_qty smallint(2) NULL;
ALTER TABLE booking_reservation ADD leader_approve_sure_qty smallint(2) NULL;
ALTER TABLE ost_tour ADD tl_quantity tinyint(4) NULL;

<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<table>
    <caption>Current reservation of <?php echo $all_staff[$id_staffReservation]?></caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Customer / <br>Phone number</th>
        <th>Hold</th>
        <th>Sure</th>
        <th>Booking Code</th>
        <th>Update Time</th>
        <th>Due</th>
        <th>Note</th>
        <th>Tour/Country</th>
        <th>#</th>
    </tr>
    </thead>
    <tbody>
    <?php $count_item = 0; $i=1; ?>
    <?php while($staffReservation && ($history = db_fetch_array($staffReservation))): $count_item++; ?>
        <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue' ?>">
            <td><?php echo $i++; ?></td>
            <td><?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
                <br><?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?></td>
            <td><?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?></td>
            <td><?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?></td>
            <td><?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?></td>
            <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?></td>
            <td>
                <?php
                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                    echo date('d/m/Y', strtotime($history['due_at'])); ?>
            </td>
            <td>
                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                    <label class="label label-default label-small" for="">Visa Only</label>
                <?php endif; ?>
                <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                    <label class="label label-default label-small" for="">Security Deposit</label>
                <?php endif; ?>
                <?php if (isset($history['fe']) && $history['fe']): ?>
                    <label class="label label-default label-small" for="">FE</label>
                <?php endif; ?>
                <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                    <label class="label label-default label-small" for="">Visa Ready</label>
                <?php endif; ?>
                <?php if (isset($history['one_way']) && $history['one_way']): ?>
                    <label class="label label-default label-small" for="">One Way</label>
                <?php endif; ?>

                <?php if(isset($history['infant']) && $history['infant']): ?>
                    [ Infant: <?php echo $history['infant'] ?> ]
                <?php endif; ?>
            </td>
            <td>
                <?php
                if (isset($history['tour_id']) && $history['tour_id']) {
                    $tour = TourNew::lookup($history['tour_id']);
                    if ($tour) echo $tour->name;
                } elseif (isset($history['country']) && $history['country']) {
                    $country = TourMarket::lookup($history['country']);
                    if($country)
                        echo 'Visa '.$country->name;
                }

                ?>
            </td>
            <td>
                <?php if( (isset($history['hold_qty']) && $history['hold_qty']) || (isset($history['sure_qty']) && $history['sure_qty']) ): ?>
                    <a href="<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id=<?php echo $history['id'] ?>"
                       class="btn_sm btn-default btn-xs" data-title="Jump to reservation"><i class="icon-eye-open"></i></a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$count_item): ?>
        <tr><td>No Item</td></tr>
    <?php endif; ?>
    </tbody>
</table>

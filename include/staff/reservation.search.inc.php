
<form class="search">
    <table>
        <caption>Search</caption>
        <tr>
            <td>Tour Name</td>
            <td>
                <input type="text" class="input-field" name="tour_name" value="<?php if (isset($_REQUEST['tour_name'])) echo trim($_REQUEST['tour_name']); ?>">
            </td>
            <td>Country</td>
            <td>
                <select name="country" id="country" class="input-field">
                <option value=>-- All --</option>
                    <?php if(is_array($country_list) && $country_list): ?>
                        <?php foreach($country_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                    <?php if (isset($_REQUEST['country']) && $_REQUEST['country'] == $item['id']) echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Departure FROM</td>
            <td>
                <input type="text" class="input-field dp" name="from" value="<?php if (isset($_REQUEST['from']) && strtotime($_REQUEST['from']))
                    echo date('d/m/Y', strtotime($_REQUEST['from']))?>">
            </td>

            <td>Departure TO</td>
            <td>
                <input type="text" class="input-field dp" name="to" value="<?php if (isset($_REQUEST['to']) && strtotime($_REQUEST['to']))
                    echo date('d/m/Y', strtotime($_REQUEST['to']))?>">
            </td>
        </tr>
        <tr>
            <td>Airline</td>
            <td>
                <?php
                if ($airlines_list_) {
                    $airline_items = $airlines_list_->getItems();
                    ?>
                    <select name="airline" id="airline" class="input-field">
                        <option value>- Select -</option>
                        <?php foreach($airline_items as $_item): ?>
                            <option value="<?php echo $_item->getId() ?>"
                                <?php if (isset($_REQUEST['airline']) && $_REQUEST['airline'] == $_item->getId()) echo "selected" ?>
                            ><?php echo $_item->getValue() ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php } ?>
            </td>
            <td>Quantity</td>
            <td>
                <input type="number" min="0" max="99" class="input-field" name="quantity">
            </td>
        </tr>

        <tr>
            <td>Customer Name</td>
            <td>
                <input type="text" class="input-field" name="customer_name" value="<?php if (isset($_REQUEST['customer_name'])) echo trim($_REQUEST['customer_name']); ?>">
            </td>
            <td>Customer Phone Number</td>
            <td>
                <input type="text" class="input-field" name="customer_phonenumber" value="<?php if (isset($_REQUEST['customer_phonenumber'])) echo trim($_REQUEST['customer_phonenumber']); ?>">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php if(isset($_REQUEST['reservation_id']) && $_REQUEST['reservation_id']): ?>
                    <p class="warning-banner">Đang tìm theo mục giữ chỗ. Để quay về trang đầy đủ, bấm nút RESET bên dưới.</p>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Staff Owner</td>
            <td>
                <select name="staff_id_search" id="" class="input-field">
                    <option value="0">- TUGO -</option>
                    <?php
                    if($users) {
                        foreach($users as $id => $name) {
                            $s = " ";
                            if ($id === intval($_REQUEST['staff_id_search']))
                                $s="selected";
                            echo sprintf('<option value="%s" %s>%s</option>', $id, $s, $name);
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><button class="btn_sm btn-primary" type="submit">Search</button></td>
            <td><button class="btn_sm btn-default" type="button" onclick="window.location='<?php echo $cfg->getUrl() ?>scp/reservation.php'">Reset</button></td>
            <td></td>
        </tr>
    </table>
</form>
<?php

$display_info_list = [
    'airline',
    'gather_date',
];
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <h4>Operator <small>Upload Passport Main Page</small></h4>
        <h5>Tour: <?php echo $tour->name ?></h5>
    </div>

    <div class="col-md-6">
        <div>
            <h3>Tour Info:</h3>
            <p>Airline: <?php if(isset($tour->airline)) echo $tour->airline ?></p>
            <p>Departure Flight Code: <?php if(isset($tour->departure_flight_code)) echo $tour->departure_flight_code ?></p>
            <p>Return Flight Code: <?php if(isset($tour->return_flight_code)) echo $tour->return_flight_code ?></p>
            <p>Tour Leader: <?php
                if (isset($tour->tour_leader)) {
                    $tour_leader = DynamicListItem::lookup($tour->tour_leader);
                    if ($tour_leader) {
                        echo $tour_leader->getValue();
                    }
                }
                ?></p>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card mb-4">
            <div class="row no-gutters">
                <div class="col-md-8">
                    <div class="card-body">
                        <p>Điền mã booking, sau đó chọn ảnh chụp passport.</p>
                        <p>Chú ý: Mỗi lần chụp 01 mặt passport. Chụp gần, rõ nét, chú ý lấy đủ phần code ở cuối trang (2 dòng có dấu &lt;&lt;&lt;&lt;&lt;&lt;&lt;)</p>
                        <div class="form-group">
                            <label for="" class="d-block d-sm-none">Booking Code</label>
                            <div class="input-group">
                                <div class="input-group-prepend d-none d-sm-block">
                                    <div class="input-group-text">Booking Code</div>
                                </div>
                                <input type="text" class="form-control booking_code" id="" placeholder="" value="<?php
                                    if($passport && $passport->booking_code !== null) echo $passport->booking_code ; else echo 'TG123-'?>" readonly>
                                <input type="hidden" class="form-control tour_id" id="" value="<?php echo $tour->id ?>">
                                <input type="hidden" class="form-control passport_id" id="" value="<?php  if($passport) echo $passport->id; else echo 0 ?>">
                            </div>
                        </div>
                        <p><small class="booking_quantity"></small></p>
                        <div class="form-group">
                            <label for="" class="d-block d-sm-none">Passport Photo</label>
                            <div class="input-group">
                                <div class="input-group-prepend d-none d-sm-block">
                                    <div class="input-group-text">Passport Photo</div>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <span class="passport_photo_status"></span>
                        <div class="progress" style="display: none;">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="passport_photo_preview">
                        <img src alt="" class="card-img">
                    </div>
                    <?php csrf_token(); ?>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    (function($) {

        $('.custom-file-input').on('change',function(e){
            //get the file name
            _self = e.target;
            var fileName = e.target.files[0].name;
            fontsize = getComputedStyle($(_self).next('label')[0]).fontSize;
            fontsize = fontsize.replace('px', '');
            em = $(_self).width()/fontsize;
            em = Math.round(em)-5;

            if (fileName.length > 20)
                fileName = '...'+fileName.substr(fileName.length-em);
            $(this).next('.custom-file-label').text(fileName);

            parent = $(this).parents('.card');
            parent.find('.progress').show();
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);

                reader.onload = function(e) {
                    booking_code = $('.booking_code').val().trim();
                    tour_id = $('.tour_id').val().trim();
                    passport_id = $('.passport_id').val().trim();
                    parent = $(input).parents('.card');
                    _status = parent.find('.passport_photo_status');

                    if (!booking_code) {
                        _status.text('Booking Code is required!');
                        return false;
                    }

                    _status.text('Uploading...');
                    parent.find('.passport_photo_preview img').attr('src', '');
                    var jqxhr = $.post( '<?php echo $cfg->getUrl() ?>scp/ajax.php/pax/passport_re_upload',
                        {
                            data: e.target.result,
                            name: input.files[0].name,
                            booking_code: booking_code,
                            tour_id: tour_id,
                            passport_id: passport_id,
                            __CSRFToken__: $('[name=__CSRFToken__]').val()
                        },
                        function() {
                        })
                        .done(function(data) {
                            var response = jQuery.parseJSON(data);
                            if ('1' === response.result+'') {
                                parent.find('.passport_photo_preview img').attr('src', e.target.result);
                            }

                            _status.text(response.message);
                        })
                        .fail(function(err) {
                            _status.text(err);
                        })
                        .always(function() {
                            parent.find('.progress').hide();
                            $('[type=file]').val('').prop('disabled', true);
                            $('.custom-file-label').text('Choose file');
                            $('.booking_quantity').text('');
                        });
                }
            }
        }
    })(jQuery);
</script>

<?php

if (!defined('OSTSCPINC') || !$thisstaff) {
    die('Invalid path');
}


?>
<style>
    table.list tbody td {
        padding: 0.75em;
    }
</style>
<h2>Quản lí thị trường</h2>
<div class="pull-right flush-right" style="padding-right:5px;height: 20px"><b><a href="<?php echo $cfg->getUrl()?>scp/market_manage_item.php?action=create" class="Icon newstaff">Add new</a></b></div>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <thead>
        <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Chỉnh sửa</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach ($tour_market as $market):?>
        <tr>
            <td style="width: 50px" class="centered"><?php echo ++$i ?></td>
            <td >
                <a href="<?php echo $cfg->getUrl().'scp/destination_manage_item.php?market='.(int)$market->id ?>">
                    <?php echo $market->name ?>
                </a>
                <?php if(!$market->status):?>
                    <span class="label label-default label-small">Hide</span>
                <?php endif;?>
            </td>
            <td class="autohide-buttons">
                <a href="<?php echo $cfg->getUrl().'scp/market_manage_item.php?action=edit&id='.$market->id?>"
                   class="btn_sm btn-xs btn-default no-pjax" data-title="Edit market" ><i class="icon-edit"></i></a>
            </td>
        </tr>
    <?php endforeach;?>
    <?php if($i === 0):?>
        <tr><td>No Item</td></tr>
    <?php endif;?>
    </tbody>
</table>



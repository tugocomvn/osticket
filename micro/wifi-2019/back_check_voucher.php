<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once  __DIR__.'/functions.php';

$code = null;
$str_result = null;

if (isset($_REQUEST['code']) && !empty(trim($_REQUEST['code']))) {
    $code = trim($_REQUEST['code']);
    $voucher = Awing::getVoucher(null, null, $code);
    $code = "\"" . $code . "\"";

    if ($voucher)
        $str_result = Awing::getResultContent($voucher);
    else
        $str_result = "Không tìm thấy mã voucher $code";
}

require_once ("check_voucher.php");

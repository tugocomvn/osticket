<?php
include_once INCLUDE_DIR.'class.ticket-analytics.php';
include_once INCLUDE_DIR.'class.tag.php';
// --------------------------------------------------------------------
function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

define('DATE_FORMAT', 'M-d D');

if (!$_REQUEST['from'])
    $_REQUEST['from'] = date('Y-m-d', time()-24*3600*30);
if (!$_REQUEST['to'])
    $_REQUEST['to'] = date('Y-m-d');

$returning_data = TicketAnalytics::bookingReturning($_REQUEST['from'], $_REQUEST['to'], 'returning');
$returning_month_data = TicketAnalytics::bookingReturning($_REQUEST['from'], $_REQUEST['to'], 'returning_month');
$returning_data_arr = [];
$returning_data_date = [];

while ($returning_data && ($row = db_fetch_array($returning_data))) {
    $returning_data_arr[] = $row['total'];
    $returning_data_date[] = date(DATE_FORMAT, strtotime($row['date']));
}

$returning_month_data_arr = [];
$returning_month_data_date = [];

while ($returning_month_data && ($row = db_fetch_array($returning_month_data))) {
    $returning_month_data_arr[] = $row['total'];
    $returning_month_data_date[] = $row['x_month'].'/'.$row['x_year'];
}
?>

<h1>Analytics</h1>

<form action="">
    <table width="95%">
        <tr>
            <td>From: </td>
            <td>
                <input type="text" name="from" class="dp input-field"
                       value="<?php if(isset($_REQUEST['from'])) echo trim(date('d/m/Y', strtotime($_REQUEST['from']))); else echo date('d/m/Y', time()-24*3600*30) ?>">
            </td>
            <td>To: </td>
            <td>
                <input type="text" name="to" class="dp input-field"
                       value="<?php if(isset($_REQUEST['to'])) echo trim(date('d/m/Y', strtotime($_REQUEST['to']))); else echo date('d/m/Y', time()-24*3600*0) ?>">
            </td>
            <td><button class="btn_sm btn-primary">Apply</button></td>
        </tr>
    </table>
</form>
<table width="100%">
    <tr>
        <td>
            <h2>Booking Returning <small>by day</small></h2>
            <canvas id="booking_returning" width="1000" height="400"></canvas>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Booking Returning <small>by month</small></h2>
            <canvas id="booking_returning_month" width="1000" height="400"></canvas>
        </td>
    </tr>

</table>
<script>
    var poolColors = function (a) {
        var pool = [];
        for(i=0;i<a;i++){
            pool.push(dynamicColors());
        }
        return pool;
    }

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }

    var booking_returning_elm = document.getElementById("booking_returning").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(1);
    new Chart(booking_returning_elm, {
        type: 'bar',
        data:
        {
            labels: $.parseJSON('<?php echo json_encode($returning_data_date) ?>'),
            datasets: [
                {
                    label: 'Returning by day',
                    data: $.parseJSON('<?php echo json_encode($returning_data_arr) ?>'),
                    fill: false,
                    backgroundColor: colors[0]
                }
            ],
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
        }
    });

    var booking_returning_month_elm = document.getElementById("booking_returning_month").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(1);
    new Chart(booking_returning_month_elm, {
        type: 'bar',
        data:
        {
            labels: $.parseJSON('<?php echo json_encode($returning_month_data_date) ?>'),
            datasets: [
                {
                    label: 'Returning group by month',
                    data: $.parseJSON('<?php echo json_encode($returning_month_data_arr) ?>'),
                    fill: false,
                    backgroundColor: colors[0]
                }
            ],
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
        }
    });
</script>
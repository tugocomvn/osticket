<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canViewReceipt()) die('Access Denied');
?>
<style>
input[type=text]
{
    border: 0;
    border-bottom: 1px dotted #000;
    text-decoration: none;
    margin-bottom: 8px;
}
input[type=time]
{
    border: 0;
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
.receipt-box {
    margin: auto;
    padding: 30px;
    height: 1460px;
    border: 1px solid #eee;
    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
    line-height: 24px;
    color: #555;
}
.receipt-box table {
    width: 100%;
    line-height: inherit;
    text-align: left;
}
.receipt-box table td {
    padding: 5px;
    width: 200px;
    vertical-align: top;
    padding-left: 50px;
}
.stamp {
	border: 0.25rem solid #D23;
	display: inline-block;
	padding: 0.25rem 1rem;
	text-transform: uppercase;
	font-family: 'Courier';
	-webkit-mask-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png');
    -webkit-mask-size: 944px 604px;
    mix-blend-mode: multiply;
}
.is-confirm {
	color: #D23;
	-webkit-mask-position: 13rem 6rem;
    border-radius: 0;
}  
.code {
    font-size: 30px;
}
.general-info {
    border-left: 2px solid #cdd0d4; 
    width : 100px;
}
</style>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>">Back</a>
<h2><?php echo __('View Receipt');?></h2>
<table cellpadding="1" cellspacing="2" width="1028">
    <thead>
        <tr>
            <th>SALES</th>
            <th>KHÁCH</th>
            <th>KẾ TOÁN</th>
        </tr>
    </thead>
    <tr>
        <td class="general-info" >
            <?php if (isset($results->created_by) && isset($results->created_at)): ?>
                <?php $created_by = Staff::lookup($results->created_by)->getName(); ?>
                <p><b>Lập bởi: </b><?php if(isset($created_by)) echo $created_by ?></p>
                <p><b>Lập lúc: </b><?php echo date('d/m/Y H:i',strtotime($results->created_at)) ?></p>
            <?php endif; ?>
        </td>
        <td class="general-info" >
            <p><b>Tên: </b><?php echo $results->customer_name ?><p>
                <p><b>Xác nhận lúc: </b>
                <?php if (!empty($results->customer_confirmed_at) && isset($results->customer_confirmed_at)): ?>
                    <?php echo date('d/m/Y H:i',strtotime($results->customer_confirmed_at)) ?>
                <?php else: ?>
                    <span>Chưa xác nhận</span>
                <?php endif ?>
            </p>
        </td>
        <td class="general-info" >
            <p><b>Nhận tiền bởi: </b> 
                    <?php if(!empty($results->sales_cashier_id_confirmed) && $results->sales_cashier_id_confirmed != null):?>
                        <?php 
                            $staff = Staff::lookup($results->sales_cashier_id_confirmed);
                            if(isset($staff)) echo $staff->getName();
                        ?>
                    <?php else: ?>
                        <span>Chưa nhận</span>
                    <?php endif ?>
            </p>
            <p><b>Nhận tiền lúc: </b> 
                <?php if(!empty($results->sales_cashier_confirmed_at) && $results->sales_cashier_confirmed_at != null):?>
                    <?php echo date('d/m/Y H:i',strtotime($results->sales_cashier_confirmed_at)) ?>
                <?php else: ?>
                    <span>Chưa nhận</span>
                <?php endif ?>
            </p>
            <?php if($results->status != Receipt::REJECTED): ?>
                <p><b>Xác nhận phiếu thu:</b>
                    <?php if($results->status === Receipt::APPROVED || $results->status === Receipt::REJECTED):?>
                        <?php if(isset($results->finance_staff_confirmed_at)) echo date('d/m/Y H:i',strtotime($results->finance_staff_confirmed_at)) ?>
                    <?php else: ?>
                        <span>Chưa xác nhận</span>
                    <?php endif ?>
                    </p>
                <p><b>Xác nhận phiếu thu bởi:</b> 
                <?php if($results->status === Receipt::APPROVED || $results->status === Receipt::REJECTED):?>
                        <?php 
                        $staff = Staff::lookup($results->finance_staff_id_confirmed);
                        if(isset($staff)) echo $staff->getName();
                        ?>
                    <?php else: ?>
                        <span>Chưa xác nhận</span>
                <?php endif ?>
                </p>
            <?php endif; ?>  
            <?php if($results->status === Receipt::REJECTED): ?>
                <p>Hủy lúc: 
                    <?php if($results->status === Receipt::APPROVED || $results->status === Receipt::REJECTED):?>
                        <?php if(isset($results->finance_staff_confirmed_at)) echo date('d/m/Y H:i',strtotime($results->finance_staff_confirmed_at)) ?>
                    <?php endif ?>
                    </p>
                <p>Hủy bởi: 
                <?php if($results->status === Receipt::APPROVED || $results->status === Receipt::REJECTED):?>
                        <?php $staff = Staff::lookup($results->finance_staff_id_confirmed); ?>
                        <?php if(isset($staff)) echo $staff->getName(); ?>
                <?php endif ?>
                </p>
            <?php endif; ?>   
        </td>
    </tr>
</table>
<div class="receipt-box">
    <table>
        <tr>   
            <td>
                <b><i><?php echo __('Mã phiếu thu:'); ?></i></b>
                <input style="width: 150px;" type="text" class="input-field" name="receipt_code" value="<?php echo $results->receipt_code ?>" readonly>
            </td>     
            <td>
                <?php if (isset($results->status)): ?>
                    <?php if((int)$results->status === 0): ?>
                        <span class="label label-default pull-right">Chưa xử lý</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::MONEY_RECEIVED): ?>
                        <span class="label label-default pull-right">Đã nhận tiền</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::SEND_CODE): ?>
                        <span class="label label-primary pull-right">Đã gửi mã xác nhận</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::WAITING_VERIFYING): ?>
                        <span class="label label-primary pull-right">Khách xác nhận</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::APPROVED): ?>
                        <span class="label label-default pull-right">Kế toán approved</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::REJECTED): ?>
                        <span class="label label-danger pull-right">Đã hủy</span>
                    <?php endif; ?>
                    <?php if((int)$results->status === Receipt::UPLOAD_IMAGE): ?>
                        <span class="label label-success pull-right">Upload hình phiếu thu</span>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b><i><?php echo __('Booking code:'); ?></i></b>
                <input style="width: 150px;" type="text" class="input-field" name="booking_code" value="<?php echo $results->booking_code ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Lộ trình:</label>
                <input type="text" class="input-field" name="tour_name" value="<?php echo $results->tour_name ?>" readonly>
            </td>
            <td>
                <label>Ngày khởi hành:</label>
                <input type="text" class="input-field" name="departure_date" 
                    value="<?php if (strtotime($results->departure_date)) echo date('d/m/Y', strtotime($results->departure_date)) ?>" readonly> 
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Giá tiền 1 khách (NL):</b></label>
                <input type="text" class="input-field" name="unit_price_nl" value="<?php echo number_format($results->unit_price_nl) ?>" readonly>
            </td>      
            <td>
                <label><b>Số lượng (NL):</b></label>
                <input style="width: 60px;" type="text" class="input-field" name="quantity_nl" value="<?php echo $results->quantity_nl ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Giá tiền 1 khách (TE):</label>
                <input type="text" class="input-field" name="unit_price_te" value="<?php if(isset($results->unit_price_te)) echo number_format($results->unit_price_te) ?>" readonly>
            </td>      
            <td>
                <label>Số lượng (TE):</label>
                <input style="width: 60px;" type="text" class="input-field" name="quantity_te" value="<?php if(isset($results->quantity_te)) echo $results->quantity_te ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Phí phụ thu:</label>
                <input type="text" class="input-field" name="surcharge" value="<?php echo number_format($results->surcharge) ?>" readonly>
            </td>
            <td>
                <label>Nội dung phụ thu:</label>
                <input type="text" class="input-field" name="surcharge_note" value="<?php echo $results->surcharge_note ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Tổng số tiền khách cần đóng:</b></label>
                <input type="text"class="input-field" name="amount_to_be_received" value="<?php echo number_format($results->amount_to_be_received) ?>" readonly>
            </td>
            <td>
                <label><b>Số tiền khách đóng:</b></label>
                <input type="text" class="input-field" name="amount_received" value="<?php echo number_format($results->amount_received) ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Số tiền còn lại:</b></label>
                <input type="text" class="input-field" name="balance_due" value="<?php if(isset($results->balance_due)) echo number_format($results->balance_due) ?>" readonly>
            </td>
            <td>
                <label><b>Hình thức thanh toán:</b></label>   
                <input type="text" class="input-field" name="payment_method" value="<?php if(!empty($results->payment_method)) echo PaymentMethod::caseTitleName($results->payment_method)?>" readonly>      
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Tên khách đại diện:</b></label>
                <input type="text" class="input-field" name="customer" value="<?php echo $results->customer_name ?>" readonly>
            </td>
            <td>
                <label><b>Số điện thoại khách:</b></label>
                <input type="text" class="input-field" name="customer_phone_number" value="<?php echo $results->customer_phone_number ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Tên nhân viên kinh doanh:</label>
                <input type="text" class="input-field" name="staff_name" value="<?php echo $results->staff_name ?>" readonly>
            </td>
            <td>
                <label>Số điện thoại nhân viên kinh doanh:</label>
                <input type="text" class="input-field" name="staff_phone_number" value="<?php echo $results->staff_phone_number ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label>Tên nhân viên thu tiền:</label>
                <input type="text" class="input-field" name="staff_cashier_name" value="<?php echo $results->staff_cashier_name ?>" readonly>
            </td>
            <td>
                <label>Số điện thoại nhân viên thu tiền:</label>
                <input type="text" class="input-field" name="staff_cashier_phone_number" value="<?php echo $results->staff_cashier_phone_number ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>Số lần in: </b></label>
                <input type="text" class="input-field" value="<?php echo (int)$results->print_count ?>" readonly>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <label><b>Ngày hoàn tất thanh toán số tiền còn lại:</b></label>
                <?php if(!empty($results->due_date) && empty($results->due_date_text)): ?>
                    <input type="text" class="input-field" name="due_date" 
                    value="<?php if (strtotime($results->due_date)) echo date('d/m/Y', strtotime($results->due_date)) ?>" readonly> 
                <?php endif; ?>
                <?php if(!empty($results->due_date_text) && empty($results->due_date)): ?>
                    <textarea style="resize: none; border: none" class="due_date_text" rows="2" cols="107" name="due_date_text" readonly><?php echo trim($results->due_date_text) ?></textarea>
                <?php endif; ?>
            </td>
        </tr>
        <?php if(!empty($results->bank_transfer_note)): ?>
        <tr>
            <td>   
                <p><label><b>Ghi chú hình thức thanh toán chuyển khoản: </b></label></p>
                <textarea style="border: none; resize: none;" class="bank_transfer_note" rows="2" cols="107" name="bank_transfer_note" readonly><?php echo $results->bank_transfer_note ?></textarea>
              
            </td>         
        </tr>
        <?php endif; ?>
        <tr>
            <td>
                <p><label><b>Lưu ý cho khách:</b></label></p>
                <textarea style="border: none; resize: none;" class="public_note" rows="3" cols="105" name="public_note" readonly><?php echo $results->public_note ?></textarea>
                <p><b>*</b> Quý khách vui lòng thanh toán phí còn lại đúng hạn (nếu có), trường hợp quý khách thanh toán trễ hạn,
                Tugo sẽ không thể giữ dịch vụ cho quý khách và bắt buộc phải hủy tour mất phí theo quy định.</p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <label><b>Ngày tập trung:</b></label>
                <input type="text" class="input-field" name="time_to_get_in" value="<?php if(strtotime($results->time_to_get_in)) echo date('d/m/Y', strtotime($results->time_to_get_in)) ?>" readonly>
                <label><b>Địa điểm:</b></label>
                <input type="text" class="input-field" name="place_to_get_in" value="<?php echo $results->place_to_get_in ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label><b>*</b> Thời gian cần hoàn tất hồ sơ (nếu có):</label>
                <input type="text" class="input-field" name="file_date" value="<?php if(strtotime($results->document_due)) echo date('d/m/Y', strtotime($results->document_due)) ?>" readonly>
            </td>
        </tr>
        <tr>
            <td>
            <label><b>*</b> Thời gian DỰ KIẾN có kết quả visa (đối với các tour cần xin visa):</label>
                <input type="text" class="input-field" name="visa_date" value="<?php if(strtotime($results->visa_result_estimate_due)) echo date('d/m/Y', strtotime($results->visa_result_estimate_due)) ?>" readonly>
                <p><b>*</b> Trước 1 ngày khởi hành (buổi chiều), HDV sẽ liên lạc với khách để xác nhận lại toàn bộ thông tin cần
                thiết.</p>
                <p><i><b><u>CHÍNH SÁCH HỦY ĐỔI TOUR:</u></b></i>
                <i>Tour khuyến mãi tại Tugo không áp dụng chính sách hủy tour, dời ngày hoặc đổi người đi. Khách sẽ mất 100%
                        phí tour trong các trường hợp trên.</i></p>
            </td>
        </tr>
    </table>
    <div class="receipt-bottom">
        <table>
            <tr>
                <td align="center">
                    <label><b>Khách xác nhận</b></label>
                    <p>
                        <?php if($results->status === Receipt::WAITING_VERIFYING || $results->status === Receipt::APPROVED || $results->status === Receipt::UPLOAD_IMAGE): ?>
                            <span class="stamp is-confirm">Đã xác nhận</span>
                        <?php endif; ?>
                    </p>
                </td>
                <td align="center">
                    <label><b>Kế toán xác nhận</b></label>
                    <p>
                        <?php if($results->status === Receipt::APPROVED): ?>
                            <span class="stamp is-confirm">Đã xác nhận</span>
                        <?php endif; ?>
                        <?php if($results->status === Receipt::REJECTED): ?>
                            <span class="stamp is-confirm">Đã hủy</span>
                        <?php endif; ?>
                    </p>
                </td>
                <td align="center">
                    <label><b>Nhân viên xác nhận</b></label>
                    <p>
                        <?php if($results->status === Receipt::APPROVED || $results->status === Receipt::WAITING_VERIFYING || $results->status === Receipt::UPLOAD_IMAGE || $results->status === Receipt::MONEY_RECEIVED || $results->status === Receipt::SEND_CODE): ?>
                            <span class="stamp is-confirm">Đã xác nhận</span>
                        <?php endif; ?>
                    </p>
                </td>                
            </tr>
        </table>
    </div>
    <div>
        <?php if($results->status === Receipt::APPROVED): ?>
            <?php if(isset($results->qr_code)): ?>
                <div class="centered">
                    <img src="<?php if(isset($results->qr_code)) echo '../public_file/qrcode_receipt/'.$results->qr_code?>" alt="No Qr" id="profile-img-tag"/>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
<table cellpadding="0" cellspacing="2" width="1028">
    <tr> 
        <h2 style="margin-top: 15px;">Ghi chú nội bộ</h2>
    </tr>
    <form action="<?php echo $cfg->getUrl() ?>scp/receipt.php" method="POST"> 
    <input type="hidden" name="receipt_id" value="<?php echo $results->id ?>">  
    <?php csrf_token(); ?>                    
    <tr>
        <?php if($results->status == 0): ?>
        <td>
            <textarea placeholder="Thêm ghi chú nội bộ tại đây..." style="resize: none;" class="internal_note" rows="3" cols="128" name="internal_note"><?php echo $results->internal_note ?></textarea>
        </td>
        <?php elseif($results->status != 0): ?>
        <td>
            <textarea style="resize: none; border: none;" class="internal_note" rows="3" cols="128" name="internal_note" readonly><?php echo $results->internal_note ?></textarea>
        </td>
        <?php endif; ?>
    </tr>
    <tr>
        <?php if($results->status == Receipt::WAITING_VERIFYING || $results->status == Receipt::MONEY_RECEIVED || $results->status == Receipt::SEND_CODE || $results->status == Receipt::UPLOAD_IMAGE): ?>
        <td>
            <textarea style="resize: none;" class="add_internal_note" placeholder="Bổ sung ghi chú nội bộ tại đây..." class="add_internal_note" rows="1" cols="128" name="add_internal_note"></textarea>
        </td>
        <?php endif; ?>
    </tr>
    <tr>
        <?php if($results->status == 0): ?>
        <td class="centered">
            <button class="btn save_internal_note" type="submit" name="action" value="new_internal_note">Save</button>
        </td>
        <?php endif; ?>
        <?php if($results->status == Receipt::WAITING_VERIFYING || $results->status == Receipt::MONEY_RECEIVED || $results->status == Receipt::SEND_CODE || $results->status == Receipt::UPLOAD_IMAGE): ?>
        <td class="centered">
            <button class="btn save_internal_note" type="submit" name="action" value="add_internal_note">Save</button>
        </td>
        <?php endif; ?>
    </tr>
    </form> 
</table>
<?php
$log = ReceiptLog::getAllLog((int)$_REQUEST['id']);
?>
<h2><?php echo __('Receipt History');?></h2>
<div id="ticket_thread">
    <?php
    if ($log):
        foreach ($log as $data):
            $content = json_decode($data['content'], true);?>
            <table class="thread-entry note" cellspacing="0" cellpadding="1" width="1058" border="0">
                <tbody>
                    <tr>
                        <th colspan="4" width="100%">
                            <div>
                                <span class="pull-left">
                                    <?php if(isset($data['created_at'])): ?>
                                        <?php echo date('d/m/Y H:i:s', strtotime($data['created_at'])) ?>
                                    <?php endif; ?>
                                </span>
                                <span class="pull-right">
                                    <?php if(isset($data['created_at'])): ?>
                                        <?php $_staff = Staff::lookup($data['created_by']) ?>
                                        <?php if(!empty($_staff)) echo $_staff->getName() ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4" width="100%">
                            <?php if (isset($content['status']) && !empty($data['id'])): ?>
                                <?php if ($content['status'] == 0): ?>
                                <div>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-default">Chưa xử lý</span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                </div>
                                <?php endif; ?>
                                <?php if ($content['status'] == 1): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-default">Đã nhận tiền</span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                                <?php if ($content['status'] == 2): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-primary">Chờ xác nhận code</span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                                <?php if ($content['status'] == 3): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-success">Khách đã xác nhận phiếu thu</i></span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                                <?php if ($content['status'] == 4): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-success">Upload image</i></span>
                                    <?php if($thisstaff->canEditBookings() && $content['status'] == Receipt::UPLOAD_IMAGE || $content['status'] == Receipt::APPROVED): ?>
                                        <a href="<?php echo $cfg->getUrl();
                                        ?>scp/receipt.php?id=<?php echo $content['id'] ?>&action=confirm_image"
                                        target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                                        data-title="View Image Receipt" >
                                            <i class="icon-eye-open"></i></a>
                                    <?php endif;?>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                                <?php if ($content['status'] == 5): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-success">Kế toán xác nhận phiếu thu</i></span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                                <?php if ($content['status'] == 6): ?>
                                    <span>Trạng thái: </span>
                                    <span class="label label-small label-danger">Đã hủy</i></span>
                                    <span class="pull-right">
                                        <a href="<?php echo $cfg->getUrl().'scp/receipt.php?id='.$data['id'].'&action=detailed_information'?>">Xem chi tiết</a></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php endforeach;?>
    <?php endif;?>
</div>
<script>
    (function($){
        var internal_note = $('.internal_note').val();
        $(".add_internal_note").keyup(function() {
            var add_internal_note = $('.add_internal_note').val();
            var internal_notes = internal_note + "\r\n" +add_internal_note;
            $('.internal_note').val(internal_notes);
        });
        $('.save_internal_note').off('click').on('click', function(e) {
            if (!confirm('Bạn đã kiểm tra kĩ thông tin chưa ?')) {
                e.preventDefault();
                return false;
            }
        });
    })(jQuery); 
</script>

var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

// gulp.task('default', function() {
//     // place code for your default task here
// });

gulp.task('default', function() {
    return gulp.src([
        './scp/js/bootstrap-typeahead.js',
        './scp/js/jquery.dropdown.js',
        './scp/js/readmore.min.js',
        './scp/js/jqxcore.js',
        './scp/js/jqxexpander.js',
        './scp/js/jqxbuttons.js',
        './scp/js/jqxscrollbar.js',
        './scp/js/jqxdata.js',
        './scp/js/jqxdate.js',
        './scp/js/jqxscheduler.js',
        './scp/js/jqxscheduler.api.js',
        './scp/js/jqxdatetimeinput.js',
        './scp/js/jqxmenu.js',
        './scp/js/jqxcalendar.js',
        './scp/js/jqxloader.js',
        './scp/js/jqxtooltip.js',
        './scp/js/jqxwindow.js',
        './scp/js/jqxcheckbox.js',
        './scp/js/jqxlistbox.js',
        './scp/js/jqxdropdownlist.js',
        './scp/js/jqxnumberinput.js',
        './scp/js/jqxradiobutton.js',
        './scp/js/jqxinput.js'
    ])
        .pipe(concat('all.js'))
        .pipe(minify({
            ext:{
                src:'.js',
                min:'.min.js'
            },
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest('./scp/js/'));
});

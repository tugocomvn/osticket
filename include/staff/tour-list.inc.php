<?php
if (!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
    <style>
        table.list tbody td,
        table.list thead th {
            padding-top: 0.75em;
            padding-bottom: 0.75em;
            vertical-align: middle;
        }

        table.list {
            margin-left: -50px;
        }
    </style>
     <div class="pull-right">
            <a href="<?php echo $cfg->getUrl() ?>scp/tour.php" target="_blank"
                class="no-pjax btn_sm btn btn-success btn-xs"><i class="icon-list-alt"></i> Fast Create</a>
    </div>
    <h2>Tour
        <small>List</small>
    </h2>
    <div class="clearfix"></div>
    <form action="<?php echo $cfg->getUrl(); ?>scp/tour-list.php" method="get" id="search">
        <table>
            <tr>
                <td>Tour name</td>
                <td>
                    <input type="text" class="input-field" name="tour_name"
                           value="<?php if (isset($_REQUEST['tour_name']) && !empty($_REQUEST['tour_name'])) echo trim($_REQUEST['tour_name']); ?>">
                </td>
                <td>Điểm đến</td>
                <td>
                    <select name="booking_type_id" class="input-field" style="height:30px;">
                    <option value=>- All -</option>
                        <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                            <?php foreach($booking_type_list as  $item): ?>
                                <option value="<?php echo $item['id'] ?>"
                                        <?php if (isset($_REQUEST['booking_type_id']) && $_REQUEST['booking_type_id'] == $item['id']) echo 'selected' ?>
                                ><?php echo $item['name'] ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </td>
                <td>Trạng thái</td>
                <td>
                    <select class="input-field tour_status" name="tour_status">
                        <option value=>- All -</option>
                        <?php foreach(TourStatus::caseTitleName() as $_id => $_name): ?>
                            <option value="<?php echo $_id ?>"
                                    <?php if (isset($_REQUEST) && $_REQUEST['tour_status'] == $_id)
                                    echo "selected" ?>
                                    >
                            <?php echo $_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>Ngày tập trung Từ</td>
                <td>
                    <input type="text" style="width: 100px" class="input-field dp" name="from"
                        value="<?php if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from'])) echo date('d/m/Y', strtotime($_REQUEST['from'])) ?>">
                </td>
            </tr>
            <tr>
                <td>Tên khách</td>
                <td>
                    <input type="text" name="customer_name" value="<?php
                    if (isset($_REQUEST['customer_name']) && !empty($_REQUEST['customer_name']))
                        echo trim($_REQUEST['customer_name']);
                    ?>" class="input-field" placeholder="" />
                </td>
                <td>Số hộ chiếu</td>
                <td>
                    <input type="text" name="passport_no" value="<?php
                        if (isset($_REQUEST['passport_no']) && !empty($_REQUEST['passport_no']))
                            echo trim($_REQUEST['passport_no']);
                    ?>" class="input-field" placeholder="" />
                </td>
                <td>Phone Number</td>
                <td>
                    <input type="text" name="customer_phone_number" value="<?php
                    if (isset($_REQUEST['customer_phone_number']) && !empty($_REQUEST['customer_phone_number']))
                        echo trim($_REQUEST['customer_phone_number']);
                    ?>" class="input-field" placeholder="" />
                </td>
                <td>Đến</td>
                <td>
                    <input type="text" style="width: 100px" class="input-field dp" name="to"
                        value="<?php if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to'])) echo date('d/m/Y', strtotime($_REQUEST['to'])) ?>">
                </td>
            </tr>
            <tr>
                <td>Mã Booking</td>
                <td>
                    <input type="text" name="booking_code" class="input-field"
                    value="<?php if (isset($_REQUEST['booking_code']) && !empty($_REQUEST['booking_code'])) echo trim($_REQUEST['booking_code']); ?>">
                </td>
                <td>Mã Chuyến bay</td>
                <td>
                    <input type="text" name="flight_code" class="input-field"
                           value="<?php if (isset($_REQUEST['flight_code']) && !empty($_REQUEST['flight_code'])) echo trim($_REQUEST['flight_code']); ?>">
                </td>
                <td>Mã vé</td>
                <td>
                    <input type="text" name="flight_ticket_code" class="input-field"
                           value="<?php if (isset($_REQUEST['flight_ticket_code']) && !empty($_REQUEST['flight_ticket_code'])) echo trim($_REQUEST['flight_ticket_code']); ?>">
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td colspan="6">
                    <button class="btn_sm btn-primary" name="action" value="search">Search</button>
                    <button type="reset" class="btn_sm btn-default" Value="<?php echo __('Reset'); ?>"
                            onclick="reset_form(this)">Reset
                    </button>
                </td>
            </tr>
        </table>
    </form>

    <script>
        function reset_form(event) {
            console.log(event);
            form = $(event).parents('form');
            form.find('input[type="text"], select, .hasDatepicker').val('');
            form.submit();
        }
    </script>
    <table class="list" border="0" cellspacing="1" cellpadding="2" width="1158">
        <caption><?php echo $pageNav->showing().' '._N('tours', 'tours', $total);?></caption>
        <thead>
        <tr>
            <th>No.</th>
            <th>Tên Tour</th>
            <th>Điểm Đến</th>
            <th>Ngày Tập Trung</th>
            <th>Ngày về</th>
            <th>Notes</th>
            <th>Số Lượng Vé</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($results && ($row = db_fetch_array($results))): ?>
            <tr>
                <td><?php echo ++$offset; ?></td>
                <td>
                    <?php echo $row['name'] ?>
                </td>
                <td>
                    <?php echo isset($booking_type_list[$row['destination']]['name'])?$booking_type_list[$row['destination']]['name']:'' ?>
                </td>
                <td>
                    <?php if (!empty($row['gateway_present_time']) && $row['gateway_present_time'] != '0000-00-00 00:00:00'): ?>
                        <?php _Format::dateInSameYear($row['gateway_present_time'], null); ?>
                    <?php endif; ?>     
                </td>
                <td>
                    <?php if (!empty($row['return_time']) && $row['return_time'] != '0000-00-00 00:00:00'): ?>
                        <?php echo date("d/m", strtotime($row['return_time'])) ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?php $decode_note = json_decode($row['note'], true);?>
                    <?php if($decode_note == NULL) :?>
                        <?php
                            $len = 18;
                            $note = mb_substr($row['note'], 0, $len, 'UTF-8').(strlen($row['note']) > $len
                                    ? "[...]":"");
                            $_note = strip_tags($row['note']);
                        ?>
                        <a href="#" data-title="<?php echo $_note ?>">
                            <?php echo $note; ?>
                        </a>
                    <?php else: ?>
                        <?php 
                            $len = 18;
                            $decode_note = implode(" ",$decode_note);
                            $note = mb_substr($decode_note, 0, $len, 'UTF-8').(strlen($decode_note) > $len
                                    ? "[...]":"");
                            $_note = strip_tags($decode_note);
                        ?>
                        <a href="#" data-title="<?php echo $_note ?>">
                            <?php echo $note; ?>
                        </a>
                    <?php endif;?>
                </td>
                <td>
                    <?php if (!empty($row['pax_quantity'])) echo $row['pax_quantity'] ?>
                </td>
                <td class="autohide-buttons">
                    <?php if((int)$row['hold_qty_current'] === 0
                            || (int)$row['status_edit_tour'] === TourNew::APPROVAL_EDIT_TOUR):?>
                        <a href="<?php echo $cfg->getUrl() ?>scp/tour-list.php?id=<?php echo $row['id'] ?>&layout=edit"
                           target="_blank" class="btn_sm btn-primary no-pjax btn-xs"
                           data-title="Edit Tour"><i class="icon-edit"></i></a>
                    <?php elseif($thisstaff->canRequestEditTour() && $row['status_edit_tour'] == 0): ?>
                        <button class="btn_sm btn-xs btn-primary request_edit_tour"
                                data-tour-id="<?php echo $row['id'] ?>">Request edit tour</button>
                    <?php elseif($thisstaff->canRequestEditTour() && $row['status_edit_tour'] == TourNew::REQUEST_EDIT_TOUR): ?>
                        <button class="btn_sm btn-xs btn-primary request_edit_tour" disabled>Sent request</button>
                    <?php elseif($thisstaff->canApproveEditTour() && $row['status_edit_tour'] == TourNew::REQUEST_EDIT_TOUR): ?>
                        <button class="btn_sm btn-xs btn-primary approval_edit_tour"
                                data-tour-id="<?php echo $row['id'] ?>">Approval edit tour</button>
                    <?php endif;?>
                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>"
                       target="_blank" class="no-pjax btn_sm btn-default  btn-xs"
                       data-title="Update Pax List"><i class="icon-user"></i></a>

                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>&layout=list"
                       target="_blank" class="no-pjax btn_sm btn-warning  btn-xs"
                       data-title="View Pax List"><i class="icon-list"></i></a>

                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>&layout=print"
                       target="_blank" class="no-pjax btn_sm btn-default  no-pjax btn-xs"
                       data-title="Print"><i class="icon-print"></i></a>

                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>&action=export"
                       target="_blank" class="no-pjax btn_sm btn-success no-pjax btn-xs"
                       data-title="Export"><i class="icon-file-text"></i></a>

                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-guest.php?tour=<?php echo $row['id'] ?>&layout=import"
                       target="_blank" class="no-pjax btn_sm btn-info  btn-xs"
                       data-title="Import Pax List"><i class="icon-list-alt"></i></a>

                    <a href="<?php echo $cfg->getUrl() ?>scp/tour-list.php?tour=<?php echo $row['id'] ?>&layout=import_reservation"
                        target="_blank" class="no-pjax btn_sm btn-primary  btn-xs"
                        data-title="Import Reservation"><i class="icon-list-alt"></i></a>
                </td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
    <div> Page:
    <?php echo $pageNav->getPageLinks(); ?>
</div>
<script>
    (function($) {
        $('.request_edit_tour').on('click', function(e) {
            _self = $(e.target);
            tour_id = _self.data('tour-id');
            text = 'Bạn có thật sự muốn gửi yêu cầu chỉnh sửa thông tin tour không? ';
            check_confirm = confirm(text);
            if ( !check_confirm ) return false;
            $.post('<?php echo $cfg->getUrl() ?>scp/ajax.php/tour/request_edit_tour', {
                __CSRFToken__: $('[name=__CSRFToken__]').val(),
                tour_id: tour_id,
            }, function(data) {
                data = JSON.parse(data);
                console.log(data.result);
                if (1 === data.result) {
                    _self.text('Sent request').prop('disabled', true);
                    _self.after("<div id='msg_notice'>"+data.message+"</div>");
                    return true;
                }else
                    _self.after("<div id='msg_error'>"+data.message+"</div>");

                return false;
            });
        });

        $('.approval_edit_tour').on('click', function(e) {
            _self = $(e.target);
            tour_id = _self.data('tour-id');
            text = 'Bạn có thật sự muốn phê duyệt chỉnh sửa thông tin tour không? ';
            check_confirm = confirm(text);
            if ( !check_confirm ) return false;
            $.post('<?php echo $cfg->getUrl() ?>scp/ajax.php/tour/approval_edit_tour', {
                __CSRFToken__: $('[name=__CSRFToken__]').val(),
                tour_id: tour_id,
            }, function(data) {
                data = JSON.parse(data);
                console.log(data.result);
                if (1 === data.result) {
                    _self.text('Approved').prop('disabled', true);
                    _self.after("<div id='msg_notice'>"+data.message+"</div>");
                    return true;
                }else
                    _self.after("<div id='msg_error'>"+data.message+"</div>");

                return false;
            });
        })
    })(jQuery);
</script>


CREATE TABLE ost_user_tracking
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  uuid VARCHAR(32),
  username VARCHAR(32),
  user_id INT,
  is_staff TINYINT(1) DEFAULT 1,
  useragent VARCHAR(255),
  ip_address VARCHAR(16),
  add_time DATETIME,
  last_login_time DATETIME
);
CREATE INDEX ost_user_tracking_uuid_index ON ost_user_tracking (uuid);
CREATE INDEX ost_user_tracking_username_index ON ost_user_tracking (username);
CREATE INDEX ost_user_tracking_user_id_index ON ost_user_tracking (user_id);
CREATE INDEX ost_user_tracking_is_staff_index ON ost_user_tracking (is_staff);
CREATE INDEX ost_user_tracking_add_time_index ON ost_user_tracking (add_time);
CREATE INDEX ost_user_tracking_last_login_time_index ON ost_user_tracking (last_login_time);
ALTER TABLE ost_user_tracking COMMENT = 'track staff and user devices';

CREATE UNIQUE INDEX ost_user_tracking_uindex ON osticket_dev.ost_user_tracking (username, user_id, is_staff, ip_address, useragent, uuid);
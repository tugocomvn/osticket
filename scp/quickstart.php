<?php
/**
 * Copyright 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// [START drive_quickstart]
require '../vendor/autoload.php';
//if (php_sapi_name() != 'cli') {
  //  throw new Exception('This application must be run on the command line.');
//}
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
//$ID_link = $_REQUEST['id'];
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');


	
    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }
    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));
            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);
            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}
// Get the API client and construct the service object.
$client = getClient();

$service = new Google_Service_Drive($client);
// Print the names and IDs for up to 10 files.
//$optParams = array(
//	'q' => '"1OkUexNYYgKn5aKbjicnCK_JljjoYVaG2" in parents',
//  'pageSize' => 10,
//  'fields' => 'nextPageToken, files(id, name)'
//);
//$results = $service->files->listFiles($optParams);
//if (count($results->getFiles()) == 0) {
//    print "No files found.\n";
//} else {
//    print "Files:\n";
//    foreach ($results->getFiles() as $file) {
//        printf("%s (%s)\n", $file->getName(), $file->getId());
//    }
//}
function printFile($service, $fileId) {
    try {
        $response = $service->files->export($fileId, 'application/pdf', array(
            'alt' => 'media'));
        var_dump($response);
        $content = $response->getBody()->getContents();
        writeFile($content);

        require("read_document.php");
        return 1;
    } catch (Exception $e) {
        return 0;
    }
}
function getTypeFile($service, $fileId){
    try {
        $file = $service->files->get($fileId);

        return $file->getMimeType();
    } catch (Exception $e) {
        return 0;
    }
}

function writeFile($content){
    $myfile = fopen("newfile.pdf", "w") or die("Unable to open file!");
    fwrite($myfile, $content);
    fclose($myfile);
}
$fileId = "1mpulTy2YXDMo2bY7l7FiEB6X_1T4S1SrfOUFaGOKwGs";
//printFile($service,$fileId);
getTypeFile($service,$fileId);
//// [END drive_quickstart]
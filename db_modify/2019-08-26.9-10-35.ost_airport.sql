CREATE TABLE `ost_airport` (
  `id` int(11) NOT NULL,
  `code_airport` varchar(32) DEFAULT NULL,
  `name_airport` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
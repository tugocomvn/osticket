<?php
namespace Tugo;
include_once ROOT_DIR.'vendor/autoload.php';
include_once INCLUDE_DIR.'class.pax.php';
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\ImageContext;
use mysql_xdevapi\Exception;

class Vision {
    public static function read($id, $tour_id, $booking_code, $image) {

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . ROOT_DIR . 'tugo-vision.json');
        try {
            $imageAnnotator = new ImageAnnotatorClient();
            echo 'Image path: '.$image."\r\n";
            $image = file_get_contents($image);
            $context = new ImageContext();
            $context->setLanguageHints(['en']);
            $response = $imageAnnotator->documentTextDetection($image, ['imageContext' => $context]);
            $annotation = $response->getFullTextAnnotation();
            if (!$annotation) {
                print('No text found' . PHP_EOL);
                throw new \Exception('No text found');
            }

            $block_text = static::getLastBlock($annotation);
            $imageAnnotator->close();
            if (!$block_text || !(strpos($block_text, '<<') !== false || strpos($block_text, '< <') !== false)) {
                print('No << chars' . PHP_EOL);
                throw new \Exception('No << chars');
            }
        } catch (\Exception $ex) {
            $sql = "Update pax_passport_photo set status=".db_input('-1')
                .", result=".db_input('')
                .", read_at=NOW() WHERE id=".db_input($id);
            db_query($sql);
            print($ex->getMessage() . PHP_EOL);
            return false;
        }

        $fullname_start = 5;
        $block_text_no_space = str_replace(' ', '', $block_text);
        $block_text_no_space = str_replace('«', '<<', $block_text_no_space);
        echo $block_text_no_space;
        //echo $block_text_no_space;
        $_check = '<<<<';
        $fullname_end = strpos($block_text_no_space, $_check);
        $firtname_start = strpos($block_text_no_space, '<<');
        $firstname_string = substr($block_text_no_space, $firtname_start, $fullname_end-$firtname_start);
        $firstname_string = trim(str_replace('<', ' ', $firstname_string));
        //echo $firtname_string;exit;

        $last_name_string = substr($block_text_no_space, $fullname_start, $firtname_start-$fullname_start);
        $last_name_string = trim(str_replace('<', ' ', $last_name_string));
        //echo $first_name_string;exit;
        $full_name_string = $last_name_string.' '.$firstname_string;
        ///
        ///
        //P<VNMNGO<<NGUYET<HANG<<<<<<<<<<<<<<<<<<<<<<<C4162095<3VNM6504061F2712077068165000043«<10
        $check_ = '<<<<';
        $passport_start = strrpos($block_text_no_space, $check_, -20);
        $passport_end = strpos($block_text_no_space, '<', $passport_start+strlen($check_));
        $passport_string = substr($block_text_no_space, $passport_start+strlen($check_), $passport_end-$passport_start-strlen($check_));
        $passport_string = trim(str_replace('<', ' ', $passport_string));
        $passport_string = str_replace('O', '0', $passport_string);
        //echo $passport_string;exit;

        $nationality = substr($block_text_no_space, $passport_end+2, 3);
        //echo $nationality;exit;

        $dob = substr($block_text_no_space, $passport_end+5, 6);
        $dob = str_replace('O', '0', $dob);
        //echo $dob."\r\n";
        $sex = substr($block_text_no_space, $passport_end+12, 1);
        //echo $sex."\r\n";
        $doe = substr($block_text_no_space, $passport_end+13, 6);
        $doe = str_replace('O', '0', $doe);
        //echo $doe."\r\n";

        $matches = [];

        preg_match('/[0-9][^0-9]+[0-9]+$/', $block_text_no_space, $matches, PREG_OFFSET_CAPTURE);
        if ($matches[0][1]) {
            $id_card_end = $matches[0][1];
            $id_card = substr($block_text_no_space, $passport_end+20, $id_card_end+1-$passport_end-20);
            $id_card = str_replace('O', '0', $id_card);
        //    echo $id_card;
        } else {
            $id_card = '';
        }

        $list = array(
            'id'           => $id,
            'result'       => $block_text_no_space,
            'fullname'     => $full_name_string,
            'dob'          => $dob,
            'gender'       => $sex,
            'dayofex'      => $doe,
            'passportno'   => $passport_string,
            'uuid'         => $id_card,
            'nationality'  => $nationality,
            'tour_id'      => $tour_id,
            'booking_code' => $booking_code,
        );
        var_dump($list);

        try {
            db_start_transaction();
            static::update($list);
            db_commit();
        } catch(\Exception $ex) {
            echo date('Y-m-d H:i:s').": Update ID $id failed. ".$ex->getMessage()."::".$ex->getCode()."\r\n";
            if ($ex->getCode() != MYSQL_ERROR_DUPLICATE_CODE) {
                db_rollback();
            } else {
                db_commit();
            }
        }
    }

    private static function getLastBlock($annotation) {
        foreach ($annotation->getPages() as $page) {
            foreach ($page->getBlocks() as $block) {
                $block_text = '';
                foreach ($block->getParagraphs() as $paragraph) {

                    foreach ($paragraph->getWords() as $word) {
                        foreach ($word->getSymbols() as $symbol) {
                            $block_text .= $symbol->getText();
                        }
                        $block_text .= ' ';
                    }
                    $block_text .= "\n";
                }
            }
        }

        return $block_text ?: null;
    }

    private static function update($params) {
        $id = (isset($params["id"]) ? $params["id"] : '');
        if (!$id) return false;
        $result = (isset($params["result"]) ? $params["result"] : '');
        if (!$result) {
            $code = (isset($params["code"]) ? $params["code"] : '');
            $status = (isset($params["status"]) ? $params["status"] : '');
            $sql = "Update pax_passport_photo set status=".db_input($code)
                .", result=".db_input($status)
                .", read_at=NOW() WHERE id=".db_input($id);
            db_query($sql);
            return false;
        }
        $tour_id = (isset($params["tour_id"]) ? $params["tour_id"] : '');
        if (!$tour_id) return false;
        $booking_code = (isset($params["booking_code"]) ? $params["booking_code"] : '');
        if (!$booking_code) return false;

        $fullname = (isset($params["fullname"]) ? $params["fullname"] : '');

        $dob = (isset($params["dob"]) ? $params["dob"] : '');
        $dob_year = substr($dob, 0, 2);
        echo "DOB Y: $dob_year\r\n";
        if ((int)$dob_year > (int)date('y')) {
            $dob_year = '19'.$dob_year;
        } else {
            $dob_year = '20'.$dob_year;
        }
        $dob = $dob_year.'-'.substr($dob, 2, 2)
            .'-'.substr($dob, 4, 2);
        echo "DOB: ".$dob."\r\n";

        $gender = (isset($params["gender"]) && $params["gender"] === 'M' ? 1 : 0);
        $passportno = (isset($params["passportno"]) ? $params["passportno"] : '');

        $dayofex = (isset($params["dayofex"]) ? $params["dayofex"] : '');
        $dayofex = '20'.substr($dayofex, 0, 2)
            .'-'.substr($dayofex, 2, 2).'-'.substr($dayofex, 4, 2);
        echo "DOE: ".$dayofex."\r\n";

        $uuid = (isset($params["uuid"]) ? $params["uuid"] : '');
        $nationality = (isset($params["nationality"]) ? $params["nationality"] : '');

        $sql = "Update pax_passport_photo set status=1, result=".db_input($result)
            .", read_at=NOW() WHERE id=".db_input($id);
        db_query($sql);
        echo "updated pax_passport_photo status\r\n";

        $where_pax_info = [ 'passport_no' => $passportno ];
        $pax_info_check = \PaxInfo::lookup($where_pax_info);
        $date = date('Y-m-d H:i:s');
        // kiểm tra mã passport để lấy thông tin khách có sẵn

        if ($pax_info_check) {
            $pax_id = $pax_info_check->pax_id;
            $pax_info_id = $pax_info_check->id;
            echo "Pax Info exists, ID: $pax_info_id, Pax ID: $pax_id\r\n";
        } else {
            // kiểm tra thông tin pax
            $where_pax = [
                "full_name" => $fullname,
                "dob" => $dob,
                "gender" => $gender,
            ];

            $pax = \Pax::lookup($where_pax);
            if (!$pax) {
                $pax_data = [
                    "full_name"  => $fullname,
                    "dob"        => $dob,
                    "gender"     => $gender,
                    "uuid"       => $uuid,
                    "created_at" => $date,
                ];

                $pax = \Pax::create($pax_data);
                $pax_id = $pax->save();
                $pax = \Pax::lookup($pax_id);
                if (!$pax) throw new \Exception('Cannot add new pax');
                echo "Pax ID: $pax_id\r\n";
            }

            // kiểm tra thông tin pax info
            $pax_info_date = [
                "pax_id"      => $pax_id,
                "passport_no" => $passportno,
                "doe"         => $dayofex,
                "nationality" => $nationality,
                "created_at"  => $date,
            ];

            $pax_info = \PaxInfo::create($pax_info_date);
            $pax_info_id = $pax_info->save();
            $pax_info = \PaxInfo::lookup($pax_info_id);
            if (!$pax_info) throw new \Exception('Cannot add new pax info');
            echo "Pax Info ID: $pax_info_id\r\n";
        }

        // kiểm tra thông tin pax tour
        $where_pax_tour = [
            "pax_id"       => $pax_id,
            "tour_id"      => $tour_id,
            "pax_info"     => $pax_info_id,
            "booking_code" => $booking_code,
        ];

        $pax_tour_check = \PaxTour::lookup($where_pax_tour);
        if ($pax_tour_check) {
            echo "Pax Tour exists\r\n";
            echo "Pax ID: $pax_tour_check->pax_id\r\n";
            echo "Tour ID: $pax_tour_check->tour_id\r\n";
            return false;
        }

        // kiểm tra thông tin pax tour
        $where_pax_tour = [
            "pax_id"       => $pax_id,
            "tour_id"      => $tour_id,
        ];

        $pax_tour_check = \PaxTour::lookup($where_pax_tour);
        if ($pax_tour_check) {
            echo "Pax Tour exists\r\n";
            echo "Pax ID: $pax_tour_check->pax_id\r\n";
            echo "Tour ID: $pax_tour_check->tour_id\r\n";
            return false;
        }

        $pax_tour_data = [
            "pax_id"            => $pax_id,
            "tour_id"           => $tour_id,
            "pax_info"          => $pax_info_id,
            "booking_code"      => $booking_code,
            "booking_code_trim" => \_String::trimBookingCode($booking_code),
            "receiving_date"    => $date,
            "created_at"        => $date,
        ];

        $pax_tour = \PaxTour::create($pax_tour_data);
        $pax_tour_id = $pax_tour->save();
        $pax_tour = \PaxTour::lookup($pax_tour_id);
        if (!$pax_tour) throw new \Exception('Cannot add new pax tour');
        echo "Pax Tour ID: $pax_tour_id\r\n";
    }
}

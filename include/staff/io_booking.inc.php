<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewPayments()) die('Access Denied');

if(!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
    function staffLoginPage($msg) {
        global $ost, $cfg;
        $_SESSION['check_session_request'] = 1;
        $_SESSION['_staff']['auth']['dest'] =
            '/' . ltrim($_SERVER['REQUEST_URI'], '/');
        $_SESSION['_staff']['auth']['msg']=$msg;
        require(SCP_DIR.'login.php');
        exit;
    }
}

if (!isset($_SESSION['check_session']) || !$_SESSION['check_session']
    || !isset($_SESSION['check_session_time'])
    || (isset($_SESSION['check_session_time']) && time() - $_SESSION['check_session_time'] > 1000)) {
    $_SESSION['check_session_time'] = -10000;
    $_SESSION['check_session'] = false;
    $_SESSION['check_session_request'] = true;
    $_SESSION['_staff']['auth']['finance']=true;
    unset($_SESSION['check_session_time']);
    unset($_SESSION['check_session']);
    staffLoginPage(__('Enter password for access'));
    exit;
}

$qs = array();
$type = $booking_code = null;
if($_REQUEST['type']) {
    $qs += array('type' => $_REQUEST['type']);
    switch(strtolower($_REQUEST['type'])){
        case 'in':
            $title=__('Thu');
            $type=$_REQUEST['type'];
            break;
        case 'out':
            $title=__('Chi');
            $type=$_REQUEST['type'];
            break;
        default:
            $type=null;
            $title=__('All bookings');
    }
}

if (isset($_REQUEST['booking_code'])) {
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += ['booking_code' => $_REQUEST['booking_code']];
}

$qwhere = ' WHERE 1 ';
$qwhere .= " AND  booking_code IS NOT NULL AND booking_code NOT LIKE ''  ";
//Type
if($type)
    $qwhere .= ' AND t.topic_id='.db_input($type == 'in' ? THU_TOPIC : CHI_TOPIC);

if($booking_code) {
    $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $booking_code);
    $_booking_code_trim = ltrim($_booking_code_trim, '0');
    $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) LIKE '.db_input('%'.$_booking_code_trim.'%');
}

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):0;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere .= " AND DATE(FROM_UNIXTIME(`time`)) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere .=" AND DATE(FROM_UNIXTIME(`time`)) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}

$sortOptions=array();
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'time';
//Sorting options...
$order_column = $order = null;
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column ?: '`time`';

if(isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order ?: 'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by=" $order_column $order ";
///
// SUMMMMMMMMMMMMMMMMMMMMMMMMMM
$sum_in = 0;
$sum_out = 0;

$sql_sum = "SELECT sum(ABS(amount)) as total FROM payment_tmp $qwhere AND topic_id = %s";

$sum=db_fetch_row(db_query(sprintf($sql_sum, THU_TOPIC)));
if ($sum && isset($sum[0]))
    $sum_in = $sum[0];

$sum=db_fetch_row(db_query(sprintf($sql_sum, CHI_TOPIC)));
if ($sum && isset($sum[0]))
    $sum_out = $sum[0];
// SUMMMMMMMMMMMMMMMMMMMMMMMMMM
//pagenate

$qs += array('order' => $order);
$qstr = '&amp;'. Http::build_query($qs);

$sql_sum = " SUM( CASE WHEN topic_id = %s THEN amount ELSE 0 END ) AS %s ";
$sql = " SELECT booking_code, MAX(`time`) as `time`,
  ".sprintf($sql_sum, THU_TOPIC, 'income').",
  ".sprintf($sql_sum, CHI_TOPIC, 'outcome').",
  ".sprintf(" SUM( CASE WHEN topic_id = %s THEN amount ELSE -1*amount END ) AS %s ", THU_TOPIC, 'balance')."
  FROM payment_tmp $qwhere GROUP BY booking_code ORDER BY $order_by ";

$total = db_count("SELECT COUNT(*) FROM ($sql) AS A");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('io-booking.php',$qs);
$res=db_query($sql . " LIMIT ".$pageNav->getStart().",".$pageNav->getLimit());
$rows = [];

if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '.$title;
else:
    $showing=__('No payment found!');
endif;
?>

<h2><?php echo __('Booking Balance');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>
<div id='filter' >
 <form action="io-booking.php" method="get">
    <div style="padding-left:2px;">
        <b><?php echo __('Date Span'); ?></b>
        <?php echo __('Between'); ?>:
        <input class="dp" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo Format::userdate('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
        &nbsp;&nbsp;
        <input class="dp" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo Format::userdate('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>

        &nbsp;<?php echo __('Mã Booking'); ?>:&nbsp
        <input type="text" name="booking_code" value="<?php echo $_REQUEST['booking_code'] ?>">
        <input type="submit" Value="<?php echo __('Search');?>" />
        <input type="reset" Value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
        <script>
            function reset_form(event) {
                console.log(event);
                form = $(event).parents('form');
                form.find('input[type="text"], select, .hasDatepicker').val('');
                form.submit();
            }
        </script>
    </div>
 </form>
</div>
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <caption><?php echo $showing; ?></caption>
    <caption>Tổng thu: <?php echo number_format($sum_in); ?> - Tổng chi: <?php echo number_format(-1*$sum_out); ?> - <strong style="color:yellow;">Số dư: <?php echo number_format($sum_in - $sum_out) ?></strong></caption>
    <thead>
        <tr>
            <th><a  <?php ?>href="io-booking.php?<?php echo $qstr; ?>&sort=date"><?php echo __('Mã Booking');?></a></th>
            <th><a  <?php ?>href="io-booking.php?<?php echo $qstr; ?>&sort=date"><?php echo __('Tổng thu');?></a></th>
            <th><a  <?php ?>href="io-booking.php?<?php echo $qstr; ?>&sort=date"><?php echo __('Tổng chi');?></a></th>
            <th><a  <?php ?>href="io-booking.php?<?php echo $qstr; ?>&sort=date"><?php echo __('Số dư');?></a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $total=0;
        if($res):
            while ($row = db_fetch_array($res)) {
                ?>
            <tr id="<?php echo $row['booking_code']; ?>">
                <td>&nbsp;<a class="no-pjax" target="_blank" href="/scp/io.php?booking_code=<?php echo Format::htmlchars($row['booking_code']); ?>"><?php echo Format::htmlchars($row['booking_code']); ?></a></td>
                <td><?php echo number_format($row['income']); ?></td>
                <td><?php echo number_format($row['outcome']); ?></td>
                <td><?php echo number_format($row['balance']); ?></td>
            </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
?>
<?php
endif;
?>
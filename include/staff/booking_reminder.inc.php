<?php
if($res && ($num = db_num_rows($res))):
$showing=$pageNav->showing();
else:
$showing=__('No payment found!');
endif;
?>
<style>
    .row .add_booking_code {
        visibility: hidden;
    }
    .row:hover .add_booking_code {
        visibility: visible;
    }
</style>
<h2><?php echo __('Booking Reminder');?>
    <small>&bullet; Bổ sung mã booking cho phiếu thu</small>
</h2>
<div id='filter' >
    <form action="booking_reminder.php" method="get">
        <div style="padding-left:2px;">
            <table>
                <tr>
                    <td><b><?php echo __('Date Span'); ?></b> <?php echo __('Between'); ?></i></td>
                    <td>
                        <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
                        <input class="dp input-field" id="ed" size=15  name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>
                    </td>
                    <td><?php echo __('Mã Receipt'); ?>:</td>
                    <td><input type="text" class="input-field" name="receipt_code" value="<?php echo $_REQUEST['receipt_code'] ?>"></td>
                </tr>
                <tr>
                    <td>Phương Thức</td>
                    <td>
                        <select name="method" class="input-field">
                            <option value=><?php echo __('-- All --') ?></option>
                            <?php
                            $sql = "SELECT DISTINCT `value` as name FROM ".LIST_ITEM_TABLE." li WHERE li.list_id IN (".THU_METHOD_LIST.",".CHI_METHOD_LIST.") ORDER BY name";
                            $res_list = db_query($sql);
                            if ($res_list) {
                                while(($row = db_fetch_array($res_list))) {
                                    ?>
                                    <option value="<?php echo $row['name'] ?>"
                                        <?php if (isset($_REQUEST['method']) && $_REQUEST['method']==$row['name']) echo 'selected' ?>
                                    ><?php echo $row['name'] ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>

                    <td><?php echo __('Số tiền'); ?>:</td>
                    <td><input type="text" class="input-field" name="amount" value="<?php echo $_REQUEST['amount'] ?>"></td>
                </tr>
                <tr>
                    <td>Tour</td>
                    <td>
                        <select name="tour" class="input-field">
                        <option value=>- All -</option>
                            <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                                <?php foreach($booking_type_list as  $item): ?>
                                    <option value="<?php echo $item['id'] ?>"
                                            <?php if (isset($_REQUEST['tour']) && $_REQUEST['tour'] == $item['id']) echo 'selected' ?>
                                    ><?php echo $item['name'] ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                    <td>Ghi Chú</td>
                    <td><input type="text" class="input-field" name="note" value="<?php echo $_REQUEST['note'] ?>"></td>
                </tr>
                <tr>
                    <td><?php echo __('Khu vực/Văn Phòng'); ?>:</td>
                    <td>
                        <select name="dept_id" id="dept_id" class="input-field">
                            <option value=>-- All --</option>
                            <?php $depts = Dept::get_cache(); ?>
                            <?php foreach($depts as $_id => $_name): ?>
                                <?php if (strpos(strtolower($_name), 'finance') === false) continue; ?>
                                <option value="<?php echo $_id ?>"
                                    <?php if(isset($_REQUEST['dept_id']) && $_id == $_REQUEST['dept_id']) echo "selected" ?>><?php echo $_name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td><?php echo __('Agent/Staff'); ?></td>
                    <td>
                        <select name='agent' class="input-field">
                            <option value><?php echo __('All');?></option>
                            <?php
                            $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                                .' FROM '.STAFF_TABLE.' staff '
                                .' ORDER by name';
                            if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                                while(list($id,$name)=db_fetch_row($agent_res)){
                                    $selected=($_REQUEST['agent'] && $name==$_REQUEST['agent'])?'selected="selected"':'';
                                    echo sprintf('<option value="%s" %s>%s</option>',$name,$selected,$name);
                                }
                            }
                            ?>
                        </select>
                    </td>

                </tr>
                <tr>
                    <td colspan="3">
                        <button type="submit" Value="" class="btn_sm btn-primary"><?php echo __('Search');?></button>
                        <button type="reset" Value="" class="btn_sm btn-default" onclick="reset_form(this)"><?php echo __('Reset');?></button>
                    </td>
                    <td colspan="3">
                        Danh sách chỉ tính các phiếu thu từ ngày 01/01/2019 trở về sau này.
                    </td>
                </tr>
            </table>
            <script>
                function reset_form(event) {
                    console.log(event);
                    form = $(event).parents('form');
                    form.find('input[type="text"], select, .hasDatepicker').val('');
                    form.submit();
                }
            </script>
        </div>
    </form>
</div>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <thead>
    <tr>
        <th style="width: 10%;"><?php echo __('Mã Receipt');?></th>
        <th style="width: 10%;"><?php echo __('Số tiền');?></th>
        <th><?php echo __('Số lượng');?></th>
        <th style="width: 10%;"><?php echo __('Loại');?></th>
        <th style="width: 10%;"><?php echo __('Phương thức');?></th>
        <th style="width: 10%;"><?php echo __('Người thu/chi');?></th>
        <th style="width: 10%;"><?php echo __('Thời gian');?></th>
        <th><?php echo __('Ghi chú');?></th>
        <th>Bổ sung mã booking</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $total=0;
    if($res):
        $dept_cache = Dept::get_cache();
        while (($row = db_fetch_array($res))) {
            $booking_code = ObjectUpdate::getValue($row['ticket_id'], TObject::PAYMENT_BOOKING_CODE);
            ?>
            <tr id="<?php echo $row['ticket_id']; ?>"
                class="<?php if ($row['amount'] < 0) echo 'type_out' ?> row" data-ticket-id="<?php echo $row['ticket_id']; ?>">
                <td class="receipt_code">
                    <?php if($booking_code && $thisstaff->canEditPayments()): ?>
                    <a href="<?php echo $cfg->getUrl() ?>scp/tickets.php?id=<?php echo $row['ticket_id'] ?>&a=edit-io&booking_code=<?php echo $booking_code['update_value'] ?>" target="_blank" class="no-pjax">
                        <strong>
                    <?php endif; ?>
                    <?php echo $row['receipt_code']; ?>
                    <?php if($booking_code && $thisstaff->canEditPayments()): ?>
                        </strong>
                    </a>
                    <?php endif; ?>
                </td>
                <td class="amount"><?php if(isset($row['amount']) && is_numeric($row['amount'])) echo number_format((int)$row['amount']) ?></td>
                <td class="quantity"><?php echo $row['quantity']; ?></td>
                <td><?php echo _String::json_decode($row['income_type']); ?></td>
                <td><?php echo _String::json_decode($row['method']); ?></td>
                <td><?php echo $row['agent']; ?></td>
                <td><?php echo Format::userdate('d/m/Y', $row['time']); ?></td>
                <td><?php echo $row['note']; ?></td>
                <td>
                    <?php if ($booking_code): ?>
                        <?php if ($thisstaff->canEditPayments()) { ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/tickets.php?id=<?php echo $row['ticket_id'] ?>&a=edit-io&booking_code=<?php echo $booking_code['update_value'] ?>" target="_blank" class="no-pjax">
                            <strong>
                        <?php } ?>

                        <span class="label label-info"><?php echo $booking_code['update_value'] ?></span>

                        <?php if ($thisstaff->canEditPayments()) { ?>
                            </strong>
                            </a>
                        <?php } ?>

                    <?php else: ?>
                        <button class="btn_sm btn-xs btn-primary add_booking_code">Update</button>
                    <?php endif; ?>
                </td>
            </tr>
            <?php
        } //end of while.
    endif; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="8">
            <?php if($res && $num){ ?>
            <?php }else{
                echo __('No payment found');
            } ?>
        </td>
    </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>
<?php csrf_token(); ?>
<script>
    (function($) {
        $('.add_booking_code').on('click', function(e) {
            _self = $(e.target);
            parent = _self.parents('.row');
            text = 'Bổ sung mã booking cho phiếu thu: '+parent.find('.receipt_code').text().trim()
                + '; Số tiền: '+parent.find('.amount').text().trim()
                + '; Số khách: '+parent.find('.quantity').text().trim();
            booking_code = prompt(text);
            payment_ticket_id = parent.data('ticket-id');
            if ( !(booking_code && booking_code.trim() && payment_ticket_id) ) return false;

            $.post('/scp/ajax.php/bookings/addToPayment', {
                __CSRFToken__: $('[name=__CSRFToken__]').val(),
                payment_ticket_id: payment_ticket_id,
                booking_code: booking_code
            }, function(data) {
                if ('1' === data) {
                    _self.text('Saved').prop('disabled', true);
                    _self.before('DONE');
                    return true;
                }

                return false;
            });
        })
    })(jQuery);
</script>

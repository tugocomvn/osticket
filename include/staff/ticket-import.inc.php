


<form action="ticket-import.php" method="POST" name="keys" target="post_iframe" enctype="multipart/form-data">
    <div class="pull-left" style="width:100%;padding-top:5px;">
        <h2><?php echo __('Tickets');?> <small><?php echo __('mass upload') ?></small></h2>
    </div>
    <hr />
    <?php csrf_token(); ?>
    <p>
        File (CSV format): <input type="file" name="import">
    </p>
    <p>
        <button type="submit" name="do" value="import-ticket">Import Ticket</button>
    </p>

    <div class="pull-right flush-right" style="padding-top:5px;padding-right:5px;">
    </div>
    <div class="clear"></div>
    <div class="pull-left" style="width:100%;padding-top:5px;">
        <h2><?php echo __('Payments');?> <small><?php echo __('mass upload') ?></small></h2>
    </div>
    <hr />
    <p>
        File (CSV format): <input type="file" name="import">
    </p>
    <p>
        <button type="submit" name="do" value="import-payment">Import Payment</button>
    </p>

    <div class="clear"></div>
    <div class="pull-left" style="width:100%;padding-top:5px;">
        <h2><?php echo __('Bookings');?> <small><?php echo __('mass upload') ?></small></h2>
    </div>
    <hr />
    <p>
        File (CSV format): <input type="file" name="import">
    </p>
    <p>
        <button type="submit" name="do" value="import-booking">Import Booking</button>
    </p>
</form>

<p>
    <iframe frameborder="0" name="post_iframe" class="post_iframe"></iframe>
</p>

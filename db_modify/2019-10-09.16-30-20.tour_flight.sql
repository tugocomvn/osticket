create table tour_flight
(
   id int auto_increment primary key,
   tour_id int null,
   flight_id int null,
   flight_ticket_code varchar(20) null
);
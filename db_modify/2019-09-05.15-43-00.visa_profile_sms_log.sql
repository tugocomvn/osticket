CREATE TABLE `visa_profile_sms_log` (
  `profile_id` int(10) NOT NULL,
  `phone_number` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sms_content` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `staff_id_create` int(10) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
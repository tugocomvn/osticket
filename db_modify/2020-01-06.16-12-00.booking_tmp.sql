/*branch: receipt-booking*/
alter table booking_tmp drop column discounttype4;

alter table booking_tmp drop column discountamount4;

CREATE OR REPLACE VIEW booking_view AS
SELECT *,
       (quantity1 * netprice1 + quantity2 * netprice2 + quantity3 * netprice3 +
        quantity4 * netprice4)                                                   AS total_net_price,
       ((quantity1 * retailprice1 + quantity2 * retailprice2 + quantity3 * retailprice3 + quantity4 * retailprice4) -
        (discountamount1 + discountamount2 + discountamount3)) AS total_retail_price,
       ((quantity1 * retailprice1 + quantity2 * retailprice2 + quantity3 * retailprice3 + quantity4 * retailprice4) -
        (quantity1 * netprice1 + quantity2 * netprice2 + quantity3 * netprice3 + quantity4 * netprice4) -
        (discountamount1 + discountamount2 + discountamount3)) AS total_profit,
       (quantity1 + quantity2 + quantity3 + quantity4)                           AS total_quantity
FROM booking_tmp;

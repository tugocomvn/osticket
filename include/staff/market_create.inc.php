<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');


?>

<form action="<?php echo $cfg->getUrl().'scp/market_manage_item.php' ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $error ?></div>
    <?php endif;?>

    <input type="hidden" name="action" value="create">

    <h2 class="centered"><?php echo $title_create ?></h2>
    <table style="margin-left: 25%; margin-top: 10px">
        <tr class="required">
            <td width="150">Tên<span class="error">*&nbsp;</span></td>
            <td >
                <input class="input-field" type="text" size="28" name="name" required value="<?php if(isset($_REQUEST['name'])) echo $_REQUEST['name'] ?>">
                <br><em style="color:gray;display:inline-block">Tên thị trường là duy nhất</em>
            </td>
        </tr>
        <tr>
            <td width="180">Trạng thái</td>
            <td>
                <select name="status">
                    <option value="1" selected>Show</option>
                    <option value="0" >Hide</option>
                </select>
            </td>
        </tr>
    </table>
    <p class="centered">
        <a class="btn_sm btn-default" href="<?php echo $cfg->getUrl().'scp/market_manage_item.php' ?>">Cancel</a>
        <button class="btn_sm btn-primary" name="create" value="create">Save</button>
    </p>
</form>

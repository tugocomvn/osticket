CREATE TABLE ost_ticket_calendar
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    ticket_id INT,
    start_date DATETIME NOT NULL,
    title VARCHAR(255),
    extra LONGTEXT COMMENT 'JSON encode'
);

ALTER TABLE ost_ticket_calendar ADD created_by INT NULL;
ALTER TABLE ost_ticket_calendar ADD created_at DATETIME NULL;
ALTER TABLE ost_ticket_calendar ADD updated_by INT NULL;
ALTER TABLE ost_ticket_calendar ADD updated_at DATETIME NULL;

ALTER TABLE ost_ticket_calendar ADD end_date DATETIME NULL;
ALTER TABLE ost_ticket_calendar
  MODIFY COLUMN end_date DATETIME AFTER start_date;
<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/visa-tour-guest.php"?>">Back</a>
<h2>Visa - Send SMS to Passenger <small>GROUP</small> </h2>
<div class="pull-right">
    <p >
        Tour <strong><?php echo $tour->name; ?></strong>
    </p>
    <p><?php
        echo $booking_type_list[(int)$tour->destination]['name']?:'';
        echo ' - ';
        if($tour->gateway_present_time) echo date('d/m/Y', strtotime($tour->gateway_present_time)) ?>
    </p>
</div>
<!--<div class="clearfix"></div>-->
<form action="<?php echo $cfg->getUrl().'scp/visa-tour-guest.php?layout=sms&ticks='.$_REQUEST['ticks'].'&send=send_group'?>" method="POST">
    <?php csrf_token(); ?>
    <?php if(isset($error_SMS)):?>
        <p class="mt-1"><span class="label label-danger"><?php echo $error_SMS?></span></>
    <?php endif; ?>
    <?php if(!empty($errors)):?>
        <p><span class="label label-success"><?php echo 'Có '.(int)($N - count($errors)).' khách được gửi tin nhắn thành công' ?></span></p>
        <p class="mt-1"><span class="label label-danger"><?php echo 'Có '.count($errors).' khách không thể gửi tin nhắn, vui lòng kiểm tra lại'?></span></>
    <?php endif; ?>
    <p><?php echo("You selected $N guest(s): "); ?></p>
    <table class="form_table" width="1050" cellspacing="0" cellpadding="2" border="0">
        <thead>
            <tr>
                <th><?php echo __('STT');?></th>
                <th><?php echo __('Mã Booking');?></th>
                <th><?php echo __('Khách Hàng');?></th>
                <th><?php echo __('Tên Tour');?></th>
                <th><?php echo __('Phone Number');?></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php $i = 0; while($results && ($row = db_fetch_array($results))): ?>
            <tr>
                <input type="hidden" name="id[]" value="<?php echo $row['id']; ?>">
                <td><?php echo ($i +1)  ?></td>
                <td><?php echo $row['booking_code']?></td>
                <td><?php echo $row['full_name']?></td>
                <td>
                    <?php echo $row['name']?>
                </td>
                <td>
                    <input style="<?php if( !_String::isMobileNumber($row['phone_number'])) echo "color: red"?>" name="sms_phone_number_group[]" type="text" maxlength="10" value="<?php
                    if(isset($_POST['sms_phone_number_group'][$i])) echo _String::formatPhoneNumber($_POST['sms_phone_number_group'][$i]);
                    elseif(isset($row['phone_number']) && !empty($row['phone_number'])) echo _String::formatPhoneNumber($row['phone_number']);
                    ?>" class="input-field" readonly/>
                </td>
                <td class="autohide-buttons">
                    <?php if($thisstaff->canMangeVisaPaxDocuments()): ?>
                        <a href="<?php echo $cfg->getUrl();
                        ?>scp/tour-guest.php?tour=<?php echo $row['tour_id'] ?>"
                           target="_blank"  class="btn_sm btn-default btn-xs no-pjax"
                           data-title="Edit Phone Number" ><i class="icon-user"></i></a>
                    <?php endif;?>
                </td>
                <td>
                    <?php if(isset($mess[_String::formatPhoneNumber($_POST['sms_phone_number_group'][$i])])):?>
                        <span class="error"><?php echo $mess[_String::formatPhoneNumber($_POST['sms_phone_number_group'][$i])] ?></span>
                    <?php endif; ?>
                </td>
                <?php $i++; ?>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
    <table border="0" cellspacing="" cellpadding="4">
        <caption><h2>Soạn tin nhắn</h2></caption>
        <tbody>
        <tr>
            <td>Cuộc hẹn<font class="error"><b>*</b>&nbsp;</font></td>
            <td>
                <select name="id-visa-appointment" id="content_appointment" required>
                    <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($listVisaAppointment && ($row = db_fetch_array($listVisaAppointment))): ?>
                        <option value="<?php echo $row['id'] ?>" ><?php echo $row['name'] ?></option>
                    <?php endwhile; ?>
                </select>
                <em>Chọn nội dung cuộc hẹn nhé!</em>
            </td>
        </tr>
        <tr>
            <td>Ngày<font class="error"><b>*</b>&nbsp;</font></td>
            <td>
                <input placeholder="Ngày" style="width: 100px;" type="text" class="input-field dp" style="width: 180px"
                       name="date-appointment" id="date_appointment"
                       value="<?php echo date('d/m/Y', time() + 1*24*3600) ?>">
            </td>
        </tr>
        <tr>
            <td>Giờ<font class="error"><b>*</b>&nbsp;</font></td>
            <td>
                <input type="time" name="time-appointment" id="time_appointment" required
                       value="<?php echo date('H:i', time()) ?>">
            </td>
        </tr>
        <tr>
            <td>Ghi chú</td>
            <td>
                    <textarea style="overflow:auto;resize:none" cols="20" rows="2" type="text" id="note_appointment"
                              name="note-appointment" value=""></textarea>
            </td>
        </tr>
        <tr>
            <td><b><?php echo __('Nội dung SMS :'); ?><font class="error"><b>*</b>&nbsp;</font></b></td>
            <td><textarea class="input-field" id="sms" maxlength="320" minlength="10" name="sms_content_group" style="width:100%" cols="60" rows="3" required></textarea></td>
        </tr>
        </tbody>
    </table>

    <p class="centered">
        <button class="btn_sm btn-primary submit_btn" name="send" value="send_group">Send SMS</button>
    </p>
</form>
<script>
    var appointment = '';
    var day = '';
    var time = '';
    var note = '';
    var name = '';
    var tour = '';

    $(document).ready(function(){
        day = $("#date_appointment").val();
        time = $("#time_appointment").val();
        //name = "<?php echo $tour_datas['full_name'] ?>";
        //tour = "<?php echo $booking_type_list[(int)$tour->destination]['name']?:'' ?>"
        setTextSMS();
    });

    function setTextSMS(){
        let content = "<?php echo SMS_APPOINTMENT_START ?>";
        //content += name;
        content += ' lich hen ' + appointment;

        if(tour !== '')
            content += '(tour:' + tour + ')';

        content += ' vao luc '+ time;
        content += ' ngay '+ day;

        if(note !== ''){
            content += ' (' + note + ')';
        }

        $("#sms").val(content);
    }

    $("select#content_appointment").change(function(){
        appointment = $("select#content_appointment option:checked").html();
        setTextSMS();
    });

    $("#date_appointment").change(function(){
        day = $("#date_appointment").val();
        setTextSMS();
    });

    $("#time_appointment").change(function(){
        time = $("#time_appointment").val();
        setTextSMS();
    });

    $("textarea#note_appointment").change(function(){
        note = $("textarea#note_appointment").val();
        setTextSMS();
    });

    $('.submit_btn').off('click').on('click', function(e) {
        if (!confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {
            e.preventDefault();
            return false;
        }
    });

</script>

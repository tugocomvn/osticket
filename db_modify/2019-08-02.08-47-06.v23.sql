CREATE TABLE `ost_ticket_count`
(
  `id`           int auto_increment
    primary key,

  `ticket_id`    int(11) NOT NULL,
  `count_ticket` int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
create index ost_phone_number_index_object_id_object_type_index
  on ost_phone_number_index (object_id, object_type);

create index ost_ticket_count_ticket_id_index
  on ost_ticket_count (ticket_id);

CREATE TABLE `commission_checking` (
                                     `id` int(11) NOT NULL,
                                     `staff_create_id` int(11) NOT NULL,
                                     `staff_id_of_report` int(11) NOT NULL,
                                     `month_of_report` tinyint(4) NOT NULL,
                                     `year_of_report` int(4) NOT NULL,
                                     `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                     `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `commission_checking`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `commission_checking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

CREATE TABLE `commission_checking_booking` (
                                             `booking_id` int(11) NOT NULL,
                                             `booking_code` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                             `total_retail_price` double NOT NULL,
                                             `total_quantity` int(11) NOT NULL,
                                             `user_id` int(11) NOT NULL,
                                             `report_id` int(11) NOT NULL,
                                             `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `commission_checking_booking`
  ADD PRIMARY KEY (`booking_id`);

ALTER TABLE `commission_checking_booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
-- --------
-- --------
-- --------
-- --------
CREATE TABLE `receipt`
(
  `id`                 int(11)      NOT NULL,
  `receipt_code`       varchar(64)  NOT NULL,
  `booking_code`       varchar(64)  NOT NULL,
  `booking_type_id`    int(11)      NOT NULL,
  `departure_day`      datetime     NOT NULL,
  `price_nl`           int(255)     NOT NULL,
  `quantity_nl`        mediumint(2) NOT NULL,
  `price_te`           int(255)     NOT NULL,
  `quantity_te`        mediumint(2) NOT NULL,
  `surcharge`          int(255)     NOT NULL,
  `note_surcharge`     varchar(255) NOT NULL,
  `total_retail_price` int(255)     NOT NULL,
  `staked`             int(255)     NOT NULL,
  `remaining_charge`   int(255)     NOT NULL,
  `completed_date`     date         NOT NULL,
  `customer`           varchar(255) NOT NULL,
  `phone_number`       varchar(16)  NOT NULL,
  `staff`              varchar(255) NOT NULL,
  `phone`              varchar(16)  NOT NULL,
  `note`               varchar(255) NOT NULL,
  `created_receipt`    datetime    DEFAULT CURRENT_TIMESTAMP,
  `middle_date`        date        DEFAULT NULL,
  `place_air`          varchar(64) DEFAULT NULL,
  `file_date`          date        DEFAULT NULL,
  `visa_date`          date        DEFAULT NULL,
  `hash_id`            varchar(7)  DEFAULT NULL,
  `ticket_id`          int(11)     DEFAULT NULL,
  `code_`              varchar(11) DEFAULT NULL,
  `confirm_date`       datetime    DEFAULT NULL,
  `code_hidden`        varchar(11) DEFAULT NULL,
  `tracking`           tinyint(1)  DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 806;
-- --------
-- --------
-- --------
-- --------
CREATE TABLE `demographic_age` (
                                 `gather_date` date NOT NULL,
                                 `age` int(11) NOT NULL,
                                 `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_age`
  ADD PRIMARY KEY (`gather_date`,`age`);

CREATE TABLE `demographic_age_group` (
                                       `gather_date` date NOT NULL,
                                       `0_25` int(11) NOT NULL,
                                       `25_35` int(11) NOT NULL,
                                       `35_55` int(11) NOT NULL,
                                       `older_55` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_age_group`
  ADD PRIMARY KEY (`gather_date`);

CREATE TABLE `demographic_gender` (
                                    `gather_date` date NOT NULL,
                                    `gender` tinyint(1) NOT NULL,
                                    `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `demographic_gender`
  ADD PRIMARY KEY (`gather_date`,`gender`);

-- --------
-- --------

CREATE TABLE `paper_require` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `status_job` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `status_marital` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `status_visa` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `passport_location` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `visa_type` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `staff_id_create` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_edit` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `visa_history` ( `id` INT(10) NOT NULL AUTO_INCREMENT ,`visa_profile_id` INT(10) NOT NULL , `pax_id` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_create` INT(10) NOT NULL , `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `date_event` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `visa_content` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,PRIMARY KEY (`id`) ) ENGINE = InnoDB;
CREATE TABLE `visa_information` ( `id` INT(10) NOT NULL AUTO_INCREMENT ,`visa_profile_id` INT(10) NOT NULL , `pax_id` INT(10) NOT NULL , `date_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_create` INT(10) NOT NULL , `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `staff_id_edit` INT(10) NOT NULL , `visa_content` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,PRIMARY KEY (`id`) ) ENGINE = InnoDB;
ALTER TABLE `paper_require` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_job` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_marital` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `status_visa` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `passport_location` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;
ALTER TABLE `visa_type` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;

CREATE TABLE `tour_pax_visa_profile` ( `id` INT NOT NULL AUTO_INCREMENT , `paper_require` TINYINT NOT NULL , `job_status_id` TINYINT NOT NULL , `marital_status_id` TINYINT NOT NULL , `visa_status_id` TINYINT NOT NULL , `passport_location_id` TINYINT NOT NULL , `visa_type_id` TINYINT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `ost_pax_tour` ADD `tour_pax_visa_profile_id` INT NOT NULL AFTER `tl`;

ALTER TABLE `paper_require` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_job` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_marital` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `status_visa` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `passport_location` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `visa_type` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `tour_pax_visa_profile` CHANGE `paper_require` `paper_require` INT NOT NULL;
alter table visa_information drop column pax_id;
ALTER TABLE `visa_information` CHANGE `visa_profile_id` `tour_pax_visa_profile_id` INT(10) NOT NULL;
alter table visa_history drop column pax_id;
alter table visa_history change visa_profile_id `​​tour_pax_visa_profile_id` int(10) not null;
alter table visa_history change `​​tour_pax_visa_profile_id` tour_pax_visa_profile_id int(10) not null;

ALTER TABLE `visa_history` CHANGE `date_event` `date_event` DATETIME NOT NULL;
ALTER TABLE `status_job` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `status_marital` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `status_visa` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `passport_location` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `visa_type` CHANGE `date_edit` `date_edit` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `tour_pax_visa_profile` CHANGE `paper_require` `paper_require` BIGINT UNSIGNED NOT NULL;
ALTER TABLE `visa_history` CHANGE `date_event` `date_event` DATE NOT NULL;

CREATE TABLE `visa_profile_appointment` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `tour_pax_visa_profile_id` INT(10) NOT NULL , `date_event` DATETIME NOT NULL , `visa_appointment_id` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `visa_appointment` (
                                  `id` int(10) NOT NULL,
                                  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                  `status` tinyint(1) NOT NULL,
                                  `staff_id_create` int(10) NOT NULL,
                                  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `staff_id_edit` int(10) NOT NULL,
                                  `date_edit` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------
-- --------

create table pax_passport_photo
(
  id int auto_increment,
  filename varchar(64) null,
  booking_code varchar(16) null,
  upload_at datetime null,
  upload_by int null,
  status tinyint null,
  constraint pax_passport_photo_pk
    primary key (id)
);

create index pax_passport_photo_booking_code_index
  on pax_passport_photo (booking_code);

create unique index pax_passport_photo_filename_uindex
  on pax_passport_photo (filename);

create index pax_passport_photo_status_index
  on pax_passport_photo (status);

create index pax_passport_photo_upload_at_index
  on pax_passport_photo (upload_at);

create index pax_passport_photo_upload_by_index
  on pax_passport_photo (upload_by);

alter table pax_passport_photo
  add result varchar(255) null;

alter table pax_passport_photo
  add upload_filename varchar(255) null;

alter table pax_passport_photo
  add read_at datetime null;

alter table pax_passport_photo
  add tour_id int null after booking_code;

create index pax_passport_photo_tour_id_index
  on pax_passport_photo (tour_id);


<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');

$page='calendar.inc.php';
$nav->setTabActive('tickets');
$nav->addSubMenu(
    [
        'desc'=>__('Back to Tickets'),
        'title'=>__('Back to Tickets'),
        'href'=>'tickets.php',
        'iconclass'=>'Ticket'
    ],
    ($_REQUEST['status']=='unassigned')
);
$nav->addSubMenu(
    [
        'desc'=>__(' Calendar'),
        'title'=>__('Calendar'),
        'href'=>'calendar.php',
        'iconclass'=>'icon-calendar'
    ],
    ($_REQUEST['status']=='unassigned')
);
$ost->addExtraHeader('<meta name="tip-namespace" content="tickets.calendar" />',
                     "$('#content').data('tipNamespace', 'tickets.calendar');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

?>
<div class="pull-left" style="width:700;padding-top:5px;">
    <h2><?php echo __('App News'); ?></h2>
</div>
<div class="pull-right flush-right" style="padding-top:5px;padding-right:5px;">
    <b><a href="app_news.php?a=add" class="Icon list-add"><?php
            echo __('Add News'); ?></a></b></div>
<div class="clear"></div>

<?php
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$count = 0;
$params = [];
$offset = ($page-1) * PAGE_LIMIT;
$news_res = \Tugo\News::getPagination($params, $offset, PAGE_LIMIT, $count);
$pageNav = new Pagenate($count, $page, PAGE_LIMIT);
$pageNav->setURL('app_news.php');
$showing=$pageNav->showing().' '._N('news', 'news', $count);

?>
<form action="app_news.php" method="POST" name="lists">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="mass_process" >
    <input type="hidden" id="action" name="a" value="" >
    <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
        <caption>News List</caption>
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th><?php echo __('Title'); ?></th>
            <th><?php echo __('Content') ?></th>
            <th><?php echo __('User UUID') ?></th>
            <th><?php echo __('Created') ?></th>
            <th><?php echo __('Last Updated'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if ($news_res): ?>
            <?php $i = 1; ?>
            <?php while(($row = db_fetch_array($news_res))): ?>
                <tr>
                    <td><?php echo ($offset + $i++) ?> <input type="checkbox" class="ckb" name="ids[]" value="<?php echo $row['id']; ?>"
                            <?php echo $sel?'checked="checked"':''; ?>> </td>
                    <td><a href="/scp/app_news.php?a=edit&id=<?php echo $row['id'] ?>"><?php echo $row['title'] ?></a></td>
                    <td title="<?php echo $row['content'] ?>"><?php echo substr($row['content'], 0, 100).(strlen($row['content']) > 100?' [...]':'') ?></td>
                    <td><?php echo $row['user_uuid'] ?></td>
                    <td><?php if ($row['created_at']) echo date('d/m/Y H:i:s', strtotime($row['created_at'])) ?></td>
                    <td><?php if ($row['updated_at']) echo date('d/m/Y H:i:s', strtotime($row['updated_at'])) ?></td>
                </tr>
            <?php endwhile; ?>
        <?php endif; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4">
                <?php if($count){ ?>
                    <?php echo __('Select'); ?>:&nbsp;
                    <a id="selectAll" href="#ckb"><?php echo __('All'); ?></a>&nbsp;&nbsp;
                    <a id="selectNone" href="#ckb"><?php echo __('None'); ?></a>&nbsp;&nbsp;
                    <a id="selectToggle" href="#ckb"><?php echo __('Toggle'); ?></a>&nbsp;&nbsp;
                <?php } else {
                    echo sprintf(__('No news defined yet &mdash; %s add one %s!'),
                                 '<a href="app_news.php?a=add">','</a>');
                } ?>
            </td>
        </tr>
        </tfoot>
    </table>
    <?php
    if ($count) //Show options..
        echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
    ?>

    <p class="centered" id="actions">
        <input class="btn_sm btn-danger button" type="submit" name="delete" value="<?php echo __('Delete'); ?>">
    </p>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm'); ?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo sprintf(
                    __('Are you sure you want to DELETE %s?'),
                    _N('selected custom list', 'selected custom lists', 2)); ?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered.'); ?>
    </p>
    <div><?php echo __('Please confirm to continue.'); ?></div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons pull-left">
            <input type="button" value="No, Cancel" class="btn_sm btn-default close">
        </span>
        <span class="buttons pull-right">
            <input type="button" value="Yes, Do it!" class="btn_sm btn-danger confirm">
        </span>
    </p>
    <div class="clear"></div>
</div>

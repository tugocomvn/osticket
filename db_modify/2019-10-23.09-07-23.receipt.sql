ALTER TABLE receipt CHANGE COLUMN gather_date gather_date datetime null;
ALTER TABLE receipt CHANGE COLUMN departure_day departure_date datetime null;
ALTER TABLE receipt ADD staff_receive int(11) null;
ALTER TABLE receipt ADD phone_staff_receive varchar(11) null;

create table receipt_sms_code
(
  id int auto_increment primary key,
  receipt_id varchar(10) null,
  sms_code varchar(10) null,
  created_at datetime null,
  expired_at datetime null,
  confirmed_at int(11) null,
  receipt_log_id int(11) null,
  phone_number int(11) null
);
<?php
namespace Tugo;

include_once __DIR__.'/../lib/Firebase-Cloud-Message/ClientInterface.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/ClientInterface.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/Message.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/Notification.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/Recipient/Device.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/Recipient/Recipient.php';
include_once __DIR__.'/../lib/Firebase-Cloud-Message/Recipient/Topic.php';

use FacebookAds\Exception\Exception;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

class CloudMessage {
    public static function sendToDevices($devices, $title, $body, array $payload) {
        $title = trim($title);
        if (empty($title)) throw new Exception('Title is required');

        $body = trim($body);
        if (empty($body)) throw new Exception('Body is required');

        $server_key = config('cloud_message_server_key');
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        if (is_string($devices))
            $message->addRecipient(new Device($devices));
        elseif (is_array($devices)) {
            foreach ($devices as $token)
                $message->addRecipient(new Device($token));
        }

        $message->setNotification(new Notification($title, $body));

        if ($payload) $message->setData($payload);

        $response = $client->send($message);
        var_dump($response->getStatusCode());
        var_dump($response->getBody()->getContents());

    }

    public static function sendToTopic($topics, $title, $body, array $payload) {
        $title = trim($title);
        if (empty($title)) throw new Exception('Title is required');

        $body = trim($body);
        if (empty($body)) throw new Exception('Body is required');

        $server_key = config('cloud_message_server_key');
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        if (is_string($topics))
            $message->addRecipient(new Topic($topics));
        elseif (is_array($topics)) {
            foreach ($topics as $topic)
                $message->addRecipient(new Topic($topic));
        }

        $message->setNotification(new Notification($title, $body));

        if ($payload) $message->setData($payload);

        $response = $client->send($message);
        var_dump($response->getStatusCode());
        var_dump($response->getBody()->getContents());
    }

    public static function subscribe($topic, array $tokens) {
        $topic = trim($topic);
        if (empty($topic)) throw new Exception('Topic is required');

        $server_key = config('cloud_message_server_key');
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $response = $client->addTopicSubscription($topic, $tokens);
        var_dump($response->getStatusCode());
        var_dump($response->getBody()->getContents());
    }

    public static function remove($topic, array $tokens) {
        $topic = trim($topic);
        if (empty($topic)) throw new Exception('Topic is required');

        $server_key = config('cloud_message_server_key');
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $response = $client->removeTopicSubscription($topic, $tokens);
        var_dump($response->getStatusCode());
        var_dump($response->getBody()->getContents());
    }
}

<?php
require_once(__DIR__ . '/staff.inc.php');
require_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/../lib/FilterTranscode.php');
use League\Csv\Writer;

if(!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
    function staffLoginPage($msg) {
        global $ost, $cfg;
        $_SESSION['_staff']['auth']['dest'] =
            '/' . ltrim($_SERVER['REQUEST_URI'], '/');
        $_SESSION['_staff']['auth']['msg']=$msg;
        require(SCP_DIR.'login.php');
        exit;
    }
}

if (!isset($_SESSION['check_session']) || !$_SESSION['check_session']
    || !isset($_SESSION['check_session_time'])
    || (isset($_SESSION['check_session_time']) && time() - $_SESSION['check_session_time'] > 1000)) {
    $_SESSION['check_session_time'] = -10000;
    $_SESSION['check_session'] = false;
    $_SESSION['check_session_request'] = true;
    $_SESSION['_staff']['auth']['finance']=true;
    unset($_SESSION['check_session_time']);
    unset($_SESSION['check_session']);
    staffLoginPage(__('Enter password for access'));
    exit;
}

$sql = $title = $type = $sum_out = $sum_in = '';
$pageNav = null;
get_payment_list($sql, $pageNav, $title, $type, $sum_in, $sum_out);

function get_payment_list(&$sql, &$pageNav, &$title, &$type, &$sum_in, &$sum_out) {
    global $thisstaff;
    $qs = array();
    $agent = $type = $booking_code = $amount = $receipt_code = $method = $dept_id = $tour = $note = $outcome_type_id = $income_type_id = null;
    if ($_REQUEST['type']) {
        $qs += array('type' => $_REQUEST['type']);
        switch (strtolower($_REQUEST['type'])) {
            case 'in':
                $title = __('Thu');
                $type = $_REQUEST['type'];
                break;
            case 'out':
                $title = __('Chi');
                $type = $_REQUEST['type'];
                break;
            default:
                $type = null;
                $title = __('All bookings');
        }
    }

    if (isset($_REQUEST['agent'])) {
        $agent = trim($_REQUEST['agent']);
        $qs += ['agent' => $_REQUEST['agent']];
    }

    if (isset($_REQUEST['booking_code'])) {
        $booking_code = trim($_REQUEST['booking_code']);
        $qs += ['booking_code' => $_REQUEST['booking_code']];
    }
    if (isset($_REQUEST['receipt_code'])) {
        $receipt_code = trim($_REQUEST['receipt_code']);
        $qs += ['receipt_code' => $_REQUEST['receipt_code']];
    }
    if (isset($_REQUEST['amount'])) {
        $amount = preg_replace('/[^0-9]/', '', trim($_REQUEST['amount']));
        $qs += ['amount' => $_REQUEST['amount']];
    }
    if (isset($_REQUEST['method'])) {
        $method = trim(json_encode(trim($_REQUEST['method'])), '"');
        $qs += ['method' => $_REQUEST['method']];
    }

    if (isset($_REQUEST['dept_id'])) {
        $dept_id = trim($_REQUEST['dept_id']);
        $qs += ['dept_id' => $dept_id];
    }

    if (isset($_REQUEST['income_type_id'])) {
        $income_type_id = trim($_REQUEST['income_type_id']);
        $qs += ['income_type_id' => $income_type_id];
    }

    if (isset($_REQUEST['outcome_type_id'])) {
        $outcome_type_id = trim($_REQUEST['outcome_type_id']);
        $qs += ['outcome_type_id' => $outcome_type_id];
    }

    if (isset($_REQUEST['note'])) {
        $note = trim($_REQUEST['note']);
        $qs += ['note' => $note];
    }

    if (isset($_REQUEST['tour'])) {
        $tour = trim(json_encode(trim($_REQUEST['tour'])), '"');
        $qs += ['tour' => $tour];
    }

    switch (strtolower($_REQUEST['type'])) {
        case 'in':
            $title = __('Thu');
            $type = $_REQUEST['type'];
            break;
        case 'out':
            $title = __('Chi');
            $type = $_REQUEST['type'];
            break;
        default:
            $type = null;
            $title = __('All payments');
    }

    $qwhere = ' WHERE 1 ';

    if ($agent) {
        $qwhere .= " AND agent = ".db_input($agent);
    }

    if ($type)
        $qwhere .= ' AND payment_tmp.topic_id=' . db_input($type == 'in' ? THU_TOPIC : CHI_TOPIC);

    if ($dept_id)
        $qwhere .= sprintf(" AND (  payment_tmp.dept_id = %s ) ", db_input("$dept_id"));

    if ($outcome_type_id)
        $qwhere .= sprintf(" AND (  payment_tmp.outcome_type_id = %s ) ", db_input("$outcome_type_id"));

    if ($income_type_id)
        $qwhere .= sprintf(" AND (  payment_tmp.income_type_id = %s ) ", db_input("$income_type_id"));

    if ($tour) {
        $join = " JOIN booking_tmp b ON trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM b.booking_code))
               LIKE trim(LEADING '0' FROM trim(LEADING '".BOOKING_CODE_PREFIX."' FROM payment_tmp.booking_code)) ";
        $qwhere .= sprintf(" AND (  b.booking_type LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $tour)."%'");
    }

    if ($booking_code) {
        $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $booking_code);
        $_booking_code_trim = ltrim($_booking_code_trim, '0');
        $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM payment_tmp.booking_code)) LIKE '.db_input($_booking_code_trim);
    }

    if ($receipt_code)
        $qwhere .= sprintf(" AND (  payment_tmp.receipt_code LIKE %s ) ", db_input("%$receipt_code%"));

    if ($method)
        $qwhere .= sprintf(" AND (  payment_tmp.method LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $method)."%'");

    if (!empty($amount))
        $qwhere .= sprintf(" AND (  payment_tmp.amount = %s ) ", db_input("$amount"));

    if ($note && !empty($note)) {
        if ( _String::isMobileNumber($note) ) {
            $qwhere .= sprintf(" AND (  payment_tmp.note LIKE %s OR payment_tmp.note LIKE %s ) "
                , db_input("%$note%"), db_input("%"._String::convertMobilePhoneNumber($note)."%"));
        } else {
            $qwhere .= sprintf(" AND (  payment_tmp.note LIKE %s ) ", db_input("%$note%"));
        }
    }

//dates
    $startTime = ($_REQUEST['startDate'] && (strlen($_REQUEST['startDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])) : 0;
    $endTime = ($_REQUEST['endDate'] && (strlen($_REQUEST['endDate']) >= 8)) ? Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])) : 0;
    if ($startTime > $endTime && $endTime > 0) {
        $errors['err'] = __('Entered date span is invalid. Selection ignored.');
        $startTime = $endTime = 0;
    } else {
        if ($startTime) {
            $qwhere .= " AND DATE(FROM_UNIXTIME(`payment_tmp`.`time`)) >= date(" . (db_input($startTime)) . ")";
            $qs += array('startDate' => $_REQUEST['startDate']);
        }
        if ($endTime) {
            $qwhere .= " AND DATE(FROM_UNIXTIME(`payment_tmp`.`time`)) <= date(" . (db_input($endTime)) . ")";
            $qs += array('endDate' => $_REQUEST['endDate']);
        }
    }

    $staff_dept = $thisstaff->getGroup()->getDepartments();
    if (!$staff_dept) {
        $qwhere .= '  AND 1 = 0 ';
    } else {
        $qwhere .= ' AND payment_tmp.dept_id IN ('.implode(',', $staff_dept).') ';
    }

    $sortOptions = array();
    $orderWays = array('DESC' => 'DESC', 'ASC' => 'ASC');
    $sort = ($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])]) ? strtolower($_REQUEST['sort']) : 'time';
//Sorting options...
    $order_column = $order = null;
    if ($sort && $sortOptions[$sort]) {
        $order_column = $sortOptions[$sort];
    }
    $order_column = $order_column ?: '`time`';

    if (isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
        $order = $orderWays[strtoupper($_REQUEST['order'])];
    }
    $order = $order ?: 'DESC';

    if ($order_column && strpos($order_column, ',')) {
        $order_column = str_replace(',', " $order,", $order_column);
    }
    $x = $sort . '_sort';
    $$x = ' class="' . strtolower($order) . '" ';
    $order_by = " payment_tmp.$order_column $order ";
///
// SUMMMMMMMMMMMMMMMMMMMMMMMMMM
    $sum_in = 0;
    $sum_out = 0;

    $sql_sum = "SELECT sum(ABS(payment_tmp.amount)) as total FROM payment_tmp  " .( $join ?: " " ) .str_replace('%', '%%', $qwhere)." AND payment_tmp.topic_id = %s";

    $sum = db_fetch_row(db_query(sprintf($sql_sum, THU_TOPIC)));
    if ($sum && isset($sum[0]))
        $sum_in = $sum[0];

    $sum = db_fetch_row(db_query(sprintf($sql_sum, CHI_TOPIC)));
    if ($sum && isset($sum[0]))
        $sum_out = $sum[0];
// SUMMMMMMMMMMMMMMMMMMMMMMMMMM
//pagenate

    $qs += array('order' => $order);
    $sql = " SELECT payment_tmp.* FROM payment_tmp " .( $join ?: " " ) . " $qwhere ORDER BY $order_by, payment_tmp.ticket_id DESC ";
    
    $total = db_count("SELECT COUNT(*) FROM ($sql) AS A");
    $page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
    $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
    $pageNav->setURL('io.php', $qs);

    if (isset($_REQUEST['export']) && $_REQUEST['export']) {
        switch ($_REQUEST['export']) {
            case 'list':
                payment_export_list($sql);
                break;
            default:
                break;
        }

        exit;
    }
}

function payment_export_list($sql) {
    $file_name = 'Payment_list_' . time() . '.csv';

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$file_name.'";');

    $path = '/public_file/' . $file_name;
    $file = INCLUDE_DIR . '..' . $path;
    $csv = Writer::createFromFileObject(new SplTempFileObject());

    $header = [
        'Ngày',
        'Booking Code',
        'Receipt Code',
        'Loại',
        'Số lượng',
        'Tổng tiền',
        'Phương thức',
        'Note/Agent',
    ];
    $csv->insertOne($header);
    $res = db_query($sql);
    if ($res) {
        while (($row = db_fetch_array($res))) {
            $data = [
                Format::userdate('d/m/Y', $row['time']),
                $row['booking_code'],
                $row['receipt_code'],
                _String::json_decode($row['income_type'] ?: $row['outcome_type']),
                $row['quantity'],
                ($row['income_type'] || $row['topic_id']==THU_TOPIC) ? number_format($row['amount']) : number_format(-1 * $row['amount']),
                _String::json_decode($row['method']),
                $row['note'] . $row['agent'],
            ];
            $csv->insertOne($data);
        } //end of while.
    }

    $csv->output();
    die;
}

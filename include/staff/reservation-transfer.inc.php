<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<style>
    .reservation_list {
        margin: 10px;
    }

    .tour_item {
        margin: 10px 10px 30px 10px;
        border: 1px solid #0A568E;
        border-bottom: 2px solid #0A568E;
        border-radius: 4px;
    }

    .tour_header,
    .tour_content,
    .tour_footer {
        padding: 10px;
    }

    .tour_header {
        border-bottom: 1px solid #ddd;
    }

    .tour_header:hover {
        cursor: pointer;
    }

    .tour_header.c_blue {
        background: #1a4580;
    }

    .tour_header.c_yellow {
        background: #747304;
    }

    .tour_header.c_green {
        background: #0e8001;
    }

    .tour_header.c_red {
        background: #800f0b;
    }

    .tour_header.c_orange {
        background: #806108;
    }

    .tour_header.c_black {
        background: #3d3c42;
    }
    .tour_header.c_grey {
        background: #c3c3c3;
    }

    .tour_header.c_purple {
        background: #500a80;
    }

    .tour_header.c_pink {
        background: #800780;
    }

    .tour_header.c_cyan {
        background: darkcyan;
    }

    .tour_name,
    .tour_time,
    .tour_price {
        width: 30%;
        margin: 0 5px;
        vertical-align: middle;
        height: 30px;
        line-height: 30px;
        display: inline-block;
        padding: 5px;
        text-align: center;
        color: white;
        float: left;
    }

    .tour_name {
        font-size: 18px;
        font-weight: bold;
    }

    .tour_name small {
        margin-left: 2em;
    }

    .tour_time {
        float: left;
        text-align: center;
    }

    .tour_price {
        float: right;
        font-size: 18px;
    }

    .tour_content {

    }

    .tour_footer {
        border-top: 1px solid #ddd;
    }

    .tour_progress {
        width: 100%;
        height: 5px;
        background: #b90500;
        margin-top: 10px;
    }

    .tour_progress .bar {
        height: 5px;
        background: #0e8001;
    }

    .bf_label {
        padding: 0.2em;
        color: white;
    }

    .bf_label .before,
    .bf_label .after {
        padding: 0.25em;
        font-size: 12px;
    }

    .bf_label:first-child .before {
        background: #3bb749;
    }

    .bf_label:first-child .after {
        background: #4d698e;
    }

    .bf_label:last-child .before {
        background: #db9d22;
    }

    .bf_label:last-child .after {
        background: #3e338e;
    }

    .bf_label:last-child .before,
    .bf_label:last-child .after {
        font-weight: bold;
        font-size: 16px;
    }

    .bf_label .before {
        border-radius: 4px 0 0 4px;
        padding-right: 1em;
        background: #00aa00;
    }

    .bf_label .after {
        border-radius: 0 4px 4px 0;
        padding-left: 0.5em;
        background: #0A568E;
    }

    .quantity {
        float: left;
    }

    .buttons {
        float: right;
    }

    .tour_tl {
        float: right;
        font-style: italic;
    }


    .table_list,
    .action {
        margin-top: 2em;
    }

    table caption {
        font-size: 16px;
        font-weight: bold;
        margin-bottom: 1em;
    }

    .search table,
    .upcoming_list table,
    .table_list table.booking_table,
    .update_table {
        border-collapse: collapse;
        width: 100%;
    }

    .upcoming_list thead th,
    .table_list thead th {
        border-bottom: 2px solid #1a1a1a;
        padding-bottom: 0.5em;
        margin-bottom: 1em;
    }

    .upcoming_list tbody td,
    .table_list tbody td {
        padding: 0.5em;
    }

    .upcoming_list tr:nth-child(2n) td,
    .table_list tr:nth-child(2n) td {
        background: #dff5f3;
    }

    .upcoming_list tr.overdue td,
    .table_list tr.overdue td {
        background: #f5bdba;
    }
    .upcoming_list tr.near_overdue td,
    .table_list tr.near_overdue td {
        background: #f5e0ec;
    }

    .hold_frame {
        width: 100%;
        height: 0;
        border: none;
    }

    td textarea {
        width: 90%;
        height: 3em;
        resize: none;
    }

    td input[type=text] {
        width: 70%;
    }

    .tour_item tr td:nth-child(3){
        padding-left: 3em;
    }

    .dp_due.new {
        width: 6em;
    }

    .upcoming_list tr:hover td,
    .table_list tr:hover td {
        background: #bfdffc;
    }

    td input[name=quantity],
    td input[name=hold_qty],
    td input[name=sure_qty] {
        width: 3em;
    }

    .history_highlight td {
        background: #4a1e7f !important;
        color: white;
    }

    .search,
    .hold__visa_wrapper,
    .upcoming_list {
        /*width: 100%;*/
        margin: 20px;
        padding: 20px;
        border: 1px solid #0A568E;
        border-bottom: 2px solid #0A568E;
        border-radius: 4px;
    }

    .hold__visa_wrapper #hold__visa {
        margin: auto;
        width: 90%;
    }

    .search caption,
    .hold__visa_wrapper #hold__visa caption {
        font-weight: bold;
        margin-bottom: 1em;
    }

    .hold__visa_wrapper #hold__visa td:nth-child(2) {
        padding-right: 3em;
    }

    .label input[type=checkbox] {
        border: none;
        background: none;
        vertical-align: middle;
    }

    .search td:nth-child(2n) {
        padding-right: 40px;
    }

    .search td input[type=text] {
        width: 90%;
    }

    .goto {
        cursor: pointer;
    }

    .strong {
        font-weight: bold;
        text-decoration: underline;
    }

    .confirm_frame {
        border: none;
        display: none;
        overflow: hidden;
    }

    .current td {
        margin-top: 1px;
        border-top: 1px solid red;
        border-bottom: 1px solid red;
    }

    .current td:first-child {
        border-right: none;
        border-left: 1px solid red;
    }

    .current td:last-child {
        border-left: none;
        border-right: 1px solid red;
    }
</style>
<h2>Reservation</h2>
<div class="search">
    <form action="<?php echo $cfg->getUrl();?>scp/reservation.php" method="get">
    <input type="hidden" name="transfer_date" value="1">
    <table>
        <caption>Search</caption>
        <tr>
            <td>Tour Name</td>
            <td>
                <input type="text" class="input-field" name="tour_name" value="<?php if (isset($_REQUEST['tour_name'])) echo trim($_REQUEST['tour_name']); ?>">
            </td>
            <td>Country</td>
            <td>
                <select name="country" id="country" class="input-field">
                <option value=>- Select -</option>
                    <?php if(is_array($country_list) && $country_list): ?>
                        <?php foreach($country_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                    <?php if (isset($_REQUEST['country']) && $_REQUEST['country'] == $item['id']) echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Departure FROM</td>
            <td>
                <input type="text" class="input-field dp" name="from" value="<?php if (isset($_REQUEST['from']) && strtotime($_REQUEST['from']))
                    echo date('d/m/Y', strtotime($_REQUEST['from']))?>">
            </td>

            <td>Departure TO</td>
            <td>
                <input type="text" class="input-field dp" name="to" value="<?php if (isset($_REQUEST['to']) && strtotime($_REQUEST['to']))
                    echo date('d/m/Y', strtotime($_REQUEST['to']))?>">
            </td>
        </tr>
        <tr>
            <td>Airline</td>
            <td>
                <?php
                if ($airlines_list_) {
                    $airline_items = $airlines_list_->getItems();
                    ?>
                    <select name="airline" id="airline" class="input-field">
                        <option value>- Select -</option>
                        <?php foreach($airline_items as $_item): ?>
                            <option value="<?php echo $_item->getId() ?>"
                                <?php if (isset($_REQUEST['airline']) && $_REQUEST['airline'] == $_item->getId()) echo "selected" ?>
                            ><?php echo $_item->getValue() ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php } ?>
            </td>
            <td>Quantity</td>
            <td>
                <input type="number" min="0" max="99" class="input-field" name="quantity">
            </td>
        </tr>

        <tr>
            <td>Customer Name</td>
            <td>
                <input type="text" class="input-field" name="customer_name" value="<?php if (isset($_REQUEST['customer_name'])) echo trim($_REQUEST['customer_name']); ?>">
            </td>
            <td>Customer Phone Number</td>
            <td>
                <input type="text" class="input-field" name="customer_phonenumber" value="<?php if (isset($_REQUEST['customer_phonenumber'])) echo trim($_REQUEST['customer_phonenumber']); ?>">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php if(isset($_REQUEST['reservation_id']) && $_REQUEST['reservation_id']): ?>
                    <p class="warning-banner">Đang tìm theo mục giữ chỗ. Để quay về trang đầy đủ, bấm nút RESET bên dưới.</p>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Staff Owner</td>
            <td>
                <select name="staff_id_search" id="" class="input-field">
                    <option value="0">- TUGO -</option>
                    <?php
                    if($users) {
                        foreach($users as $id => $name) {
                            $s = " ";
                            if ($id === intval($_REQUEST['staff_id_search']))
                                $s="selected";
                            echo sprintf('<option value="%s" %s>%s</option>', $id, $s, $name);
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><button class="btn_sm btn-primary" type="submit">Search</button></td>
            <td><button class="btn_sm btn-default" type="button" onclick="window.location='<?php echo $cfg->getUrl() ?>scp/reservation.php'">Reset</button></td>
            <td></td>
        </tr>
    </table>
</form>
</div>

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs"  id="myTab">
        <li class="active">
            <a href="#tourlist" data-toggle="tab">Tour List</a>
        </li>
    </ul>
    <?php if(db_num_rows($tours) === 0):?>
        <div id="msg_warning">
            Không tìm thấy tour phù hợp
        </div>
    <?php else:?>
        <div class="tab-content">
            <div class="tab-pane active" id="tourlist">
                <div class="reservation_list">
                    <?php while($tours && ($tour = db_fetch_array($tours))): ?>
                        <?php
                        $reservation_cache = [];
                        $_tour_available = intval($tour['quantity']) - intval($tour['sure_quantity']) - intval($tour['hold_quantity']);
                        ?>
                        <div class="tour_item">
                            <div>
                                <form id="<?php echo $tour['id'] ?>">
                                    <button type="button" class="btn_sm btn-primary choose_tour_btn pull-right">CHOOSE TOUR</button>
                                    <div class="clearfix"></div>
                                    <?php csrf_token(); ?>
                                    <input type="hidden" name="tour_id" value="<?php echo (int)$_GET['tour_id']?>">
                                    <input type="hidden" name="reservation_current" value="<?php echo (int)$_GET['reservation_id']?>">
                                    <input type="hidden" name="tour_transfer" value="<?php echo $tour['id'] ?>">
                                </form>
                            </div>

                            <div class="tour_header
                                <?php if (!$tour['status'] || (date('Y-m-d',strtotime($tour['departure_date'])) < date('Y-m-d'))) echo "c_grey";
                                    elseif($_tour_available <= 0) echo "c_purple";
                                    elseif($tour['status']) echo "c_blue";?> ">
                                <span class="tour_name"> <?php echo $tour['name'] ?></span>
                                <span class="tour_time"> Ra sân bay: <?php echo date('d/m/Y H:i',strtotime($tour['gather_date'])) ?></span>
                                <small><?php echo itinerary(trim($tour['itinerary_a']),trim($tour['itinerary_b']),trim($tour['itinerary_c']),trim($tour['itinerary_d'])) ?></small>
                                <p class="tour_price"><?php echo number_format($tour['retail_price'], 0, ',', '.') ?> đ</p>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tour_content" style="display:none;">
                                <div class="tour_info">
                                    <p>Bay đi: <?php if($tour['tour_date_a']) echo date('d/m/Y H:i',strtotime($tour['tour_date_a'])) ?></p>
                                    <p>Bay về: <?php if($tour['tour_date_c']) echo date('d/m/Y H:i',strtotime($tour['tour_date_c'])) ?></p>
                                    <hr>
                                    <?php echo $tour['notes'] ?>
                                </div>
                                <p class="tour_tl">
                                    TL:
                                    <?php
                                    $tour_leader_item = DynamicListItem::lookup($tour['tour_leader']);
                                    ?><?php if ($tour_leader_item) echo $tour_leader_item->getValue(); else echo 'N/A' ?>
                                </p>
                                <div class="clearfix"></div>
                                <p>
                                    <?php if($thisstaff->canCancelTour()): ?>
                                        <?php if($tour['status']): ?>
                                            <button class="btn_sm btn-xs btn-danger btn_cancel_tour">Cancel Tour</button>
                                        <?php else: ?>
                                            <button class="btn_sm btn-xs btn-primary btn_cancel_tour">Reopen Tour</button>
                                        <?php endif;?>
                                    <?php endif; ?>
                                </p>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tour_footer">
                                <div class="function">
                                    <div class="quantity">
                                        <!--                <span class="bf_label"><span class="before"><strong>Total</strong></span><span class="after">31</span></span>-->
                                        <span class="bf_label"><span class="before">Hold</span><span class="after"><?php echo $tour['hold_quantity'] ?: 0 ?></span></span>
                                        <span class="bf_label"><span class="before">Sure</span><span class="after"><?php echo $tour['sure_quantity'] ?: 0 ?></span></span>
                                        <span class="bf_label"><span class="before">Available</span><span class="after"><?php echo $_tour_available ?></span></span>
                                    </div>
                                    <div class="buttons">
                                        <button class="btn_sm btn-xs btn-success btn_details <?php if (isset($_REQUEST['reservation_id']) && (int)$_REQUEST['reservation_id']) echo ' strong' ?>">List</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="tour_progress">
                                    <?php
                                    $total = isset($tour['quantity']) ? (int)$tour['quantity'] : 0;
                                    ?>
                                    <div class="bar" style="width: <?php echo $total ? ($total-$_tour_available)*100/$total : 0 ?>%;"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="table_list details" style="display: none">
                                    <?php
                                    $reservations = BookingReservation::getByTour($tour['id'], false);
                                    $i = 1;
                                    ?>
                                    <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
                                    <table class="booking_table">
                                        <caption>Current Reservations</caption>
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Sale</th>
                                            <th>Hold</th>
                                            <th>Sure</th>
                                            <th>Customer / <br>Phone number</th>
                                            <th>Booking Code</th>
                                            <th>Update Time</th>
                                            <th>Due</th>
                                            <th>Note</th>
                                            <th>Created By</th>
                                            <th>Updated By</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $count_item = 0; ?>
                                        <?php while ($reservations && ($history = db_fetch_array($reservations))): $count_item++; ?>
                                            <input type="hidden" name="reservation_id" value="<?php echo $history['id'] ?>">
                                            <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()):
                                                    echo 'overdue';
                                                elseif (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= (time() + TIME_NEAR_DUE_RESERVATION * 3600)):
                                                    echo "near_overdue";
                                                endif;?>
                                                <?php if (isset($_REQUEST['reservation_id']) && (int)$_REQUEST['reservation_id'] && $_REQUEST['reservation_id'] == $history['id']) echo ' current' ?>">
                                                <td><?php echo $i++; ?></td>
                                                <td><?php if (isset($all_staff[$history['staff_id']])):
                                                        echo $all_staff[$history['staff_id']];
                                                    else:
                                                        echo '- Tugo -';
                                                    endif;?>
                                                </td>
                                                <td>
                                                    <span class="<?php if ($history['over'] && $history['hold_qty']) echo "label label-danger" ?>">
                                                        <?php if (isset($history['hold_qty']) && $history['hold_qty']):
                                                            echo $history['hold_qty'];
                                                        endif; ?>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?php if (isset($history['sure_qty']) && $history['sure_qty']):
                                                        echo $history['sure_qty'];
                                                    endif;?>
                                                </td>
                                                <td>
                                                    <?php if (isset($history['customer_name']) && $history['customer_name']):
                                                        echo $history['customer_name'];
                                                        endif; ?>
                                                    <br>
                                                    <?php if (isset($history['phone_number']) && $history['phone_number']):
                                                        echo $history['phone_number'];
                                                    endif; ?>
                                                </td>
                                                <td><?php if (isset($history['booking_code']) && $history['booking_code']):
                                                        $booking_check = Booking::lookup(['booking_code' => $history['booking_code']]); ?>
                                                        <a class="no-pjax" target="_blank"
                                                           href="<?php echo $cfg->getUrl() . 'scp/new_booking.php?id=' . $booking_check->ticket_id; ?>"><?php echo $history['booking_code']; ?></a>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if (isset($history['created_at']) && strtotime($history['created_at'])):
                                                        echo date('d/m/y H:i', strtotime($history['created_at']));
                                                    endif;?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (isset($history['due_at']) && strtotime($history['due_at']) > 0):
                                                        echo date('d/m', strtotime($history['due_at']));
                                                    endif?>
                                                </td>
                                                <td>
                                                    <?php if (isset($history['note']) && $history['note']):
                                                        echo $history['note'];
                                                    endif; ?>
                                                    <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                                                        <label class="label label-default label-small" for="">Visa
                                                            Only</label>
                                                    <?php endif; ?>
                                                    <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                                                        <label class="label label-default label-small" for="">Security
                                                            Deposit</label>
                                                    <?php endif; ?>
                                                    <?php if (isset($history['fe']) && $history['fe']): ?>
                                                        <label class="label label-default label-small" for="">FE</label>
                                                    <?php endif; ?>
                                                    <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                                                        <label class="label label-default label-small" for="">Visa
                                                            Ready</label>
                                                    <?php endif; ?>
                                                    <?php if (isset($history['one_way']) && $history['one_way']): ?>
                                                        <label class="label label-default label-small" for="">One
                                                            Way</label>
                                                    <?php endif; ?>

                                                    <?php if (isset($history['infant']) && $history['infant']): ?>
                                                        [ Infant: <?php echo $history['infant'] ?> ]
                                                    <?php endif; ?>
                                                </td>

                                                <td><?php if (isset($all_staff[$history['created_by']])):
                                                        echo $all_staff[$history['created_by']];
                                                     else:
                                                        echo 'Tugo';
                                                     endif;?>
                                                </td>
                                                <td>
                                                    <?php if (isset($all_staff[$history['updated_by']])):
                                                        echo $all_staff[$history['updated_by']];
                                                    endif;?>
                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                        <?php if (!$count_item): ?>
                                            <tr>
                                                <td>No Item</td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>
<script>
    (function($) {
        $('.btn_details').off('click').on('click', function(e) {
            $('.btn_details').not(this).removeClass('strong');
            $('.btn_history').not(this).removeClass('strong');
            $('.btn_action').not(this).removeClass('strong');
            _self = $(e.target);
            _self.toggleClass('strong');
            _item = _self.parents('.tour_item');
            _item.find('.action').hide();
            _item.find('.table_list.history').hide();
            _item.find('.table_list.upcoming').hide();
            _item.find('.table_list.details').toggle();
        });

        $('.choose_tour_btn').off('click').on('click', function(e) {
            _self = $(e.target);
            _form = _self.parents('form');
            reservation_current = _form.find('[name=reservation_current]').val().trim();
            tour_id = _form.find('[name=tour_id]').val().trim();
            tour_transfer = _form.find('[name=tour_transfer]').val().trim();
            if (!tour_id || !reservation_current || !tour_transfer) return false;

            $.post('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php',
                {action:"transfer_date",reservation_current:reservation_current,tour_id:tour_id,tour_transfer:tour_transfer},
                function(data) {
                    if (!data) return false;
                    _self.after('<div>' +
                        data+
                        '</div>');
            });
            $('.choose_tour_btn').hide();
        });

    })(jQuery);

    function _reload(reservation_id) {
        setTimeout(function() {
                console.log(reservation_id);
                if (reservation_id) {
                    window.location.href = '<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id='+reservation_id;
                } else {
                    location.reload();
                }
            }, 3000
        );
    }
</script>
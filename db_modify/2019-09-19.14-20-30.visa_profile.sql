CREATE TABLE `visa_information_action_log` (
  `id` int AUTO_INCREMENT primary key,
  `visa_information_action_log_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `visa_profile_appointment_action_log` (
  `id` int AUTO_INCREMENT primary key,
  `visa_profile_appointment_action_log_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `visa_history_action_log` (
  `id` int AUTO_INCREMENT primary key,
  `visa_history_action_log_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tour_pax_visa_profile_history` (
  `id` int AUTO_INCREMENT primary key,
  `tour_pax_visa_profile_history_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ost_pax_tour_history` (
  `id` int AUTO_INCREMENT primary key,
  `ost_pax_tour_history_id` int(11) NULL,
  `snapshot` longtext NULL,
  `time` DATETIME NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ost_pax_tour ADD log_id_ost_pax_tour int(10);
ALTER TABLE tour_pax_visa_profile ADD log_id_profile_history int(10);
ALTER TABLE visa_history ADD log_id_visa_history int(10);
ALTER TABLE visa_information ADD log_id_visa_information int(10);
ALTER TABLE visa_profile_appointment ADD log_id_visa_appointment int(10);
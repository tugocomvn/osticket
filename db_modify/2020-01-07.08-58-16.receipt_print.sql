/*branch: receipt*/

create table receipt_print
(
    id int unsigned null,
    receipt_id int null,
    log_id int unsigned null,
    created_by int null,
    created_at datetime null,
    constraint Receipt_print_pk
        primary key (id)
);

create index receipt on Receipt_print (receipt_id,log_id);

alter table receipt
	add print_count int unsigned null;
	
alter table receipt
	add log_id int unsigned null;


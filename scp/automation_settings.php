<?php
/*********************************************************************
settings.php

Handles all admin settings.

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/

require('admin.inc.php');
include_once INCLUDE_DIR.'class.auto_action.php';

$ost->addExtraHeader('<meta name="tip-namespace" content="automation_settings" />',
                     "$('#content').data('tipNamespace', 'automation_settings');");
$nav->setTabActive('settings', ('automation_settings.php'));

$trigger_list = TriggerList::caseTitleName();

function __validate_utm(&$data) {
    if (!isset($data['campaign']) || empty(trim($data['campaign'])))
        throw new Exception('Campaign is required');
    if (!isset($data['utm_source']) || empty(trim($data['utm_source'])))
        throw new Exception('UTM Source is required');
    if (!isset($data['utm_medium']) || empty(trim($data['utm_medium'])))
        throw new Exception('UTM Medium is required');
    if (!isset($data['utm_content']) || empty(trim($data['utm_content'])))
        throw new Exception('UTM Content is required');

    $data['aac_campaign_id'] = AutoActionCampaign::createIfNotExists('campaign', $data['campaign']);
    $data['aac_utm_source_id'] = AutoActionCampaign::createIfNotExists('source', $data['utm_source']);
    $data['aac_utm_content_id'] = AutoActionCampaign::createIfNotExists('content', $data['utm_content']);
    $data['aac_utm_medium_id'] = AutoActionCampaign::createIfNotExists('medium', $data['utm_medium']);

    if (!empty(trim($data['utm_term'])))
        $data['aac_utm_term_id'] = AutoActionCampaign::createIfNotExists('term', $data['utm_term']);
    else
        $data['aac_utm_term_id'] = null;
}

function __validate_recursive_time(&$data)  {
    if (empty($data['schedule'])) {
        throw new Exception("Please choose schedule type");
    }

    switch ($data['schedule']) {
        case "one_time":
            if (empty($data['run_one_time_date']) &&
                empty($data['run_one_time_hour']) &&
                empty($data['run_one_time_minute'])
            ) throw new Exception("Time to send one time is required");

            $date = $data['run_one_time_date']
                .' '.sprintf("%02s", $data['run_one_time_hour'])
                .':'.sprintf("%02s", $data['run_one_time_minute']);

            if (!date_create_from_format('Y-m-d H:i', $date))
                throw new Exception("Invalid  TIME date time");

            $data['send_one_time_at'] = $date.":00";
            break;

        case "recursion":
            $data[ 'recursion_hour'] = (empty($data[ 'recursion_hour'])
                ? '-1' : implode(',', $data[ 'recursion_hour']));

            $data[ 'recursion_minute'] = (empty($data[ 'recursion_minute'])
                ? '-1' : implode(',', $data[ 'recursion_minute']));

            $data[ 'recursion_date_in_month'] = (empty($data[ 'recursion_date_in_month'])
                ? '-1' : implode(',', $data[ 'recursion_date_in_month']));

            $data[ 'recursion_month'] = (empty($data[ 'recursion_month'])
                ? '-1' : implode(',', $data[ 'recursion_month']));

            $data[ 'recursion_day_of_week'] = (empty($data[ 'recursion_day_of_week'])
                ? '-1' : implode(',', $data[ 'recursion_day_of_week']));
            break;

        case "now":
            break;

        default:
            throw new Exception("Please choose schedule type");
            break;
    }
}

function __validation(&$data) {
    global $trigger_list;
    $auto = null;
    if (isset($data['id']) && (int)trim($data['id'])) {
        $data['id'] = (int)trim($data['id']);
        $auto = AutoActionContent::lookup(['id' => $data['id']]);
        if (!$auto) throw new \Exception('Invalid automation content. Wrong ID');

        $_search = ['id__noteq' => $data['id'], 'name' => $data["name"]];
    } else {
        $_search = ['name' => $data["name"]];
    }

    $_auto = AutoActionContent::lookup($_search);
    if ($_auto) throw new \Exception('Duplicate name.');

    if (!isset($data['trigger_id']) || !(int)trim($data['trigger_id']))
        throw new Exception('Trigger is required');

    if (!isset($trigger_list[$data['trigger_id']]))
        throw new Exception('Invalid trigger, wrong ID');

    if (!isset($data['send_after']) || !(int)trim($data['send_after']))
        throw new Exception('Invalid send after, second unit');

    if (!isset($data['status']) || ($data['status'] != 1 && $data['status'] != 0))
        throw new Exception('Invalid status');

    switch ($data['type']) {
        case "sms":
            if (empty(trim($data["sms_content"])))
                throw new Exception('SMS content is required');
            break;
        case "email":
            if (empty($data['email_subject']))
                throw new Exception("Email title is required");
            if (empty($data['email_content']))
                throw new Exception("Email conmtent is required");
            break;
        case "notification":
            if (empty($data['notification_title']))
                throw new Exception("Notification title is required");

            if (empty($data['notification_content']))
                throw new Exception("Notification conmtent is required");
            break;
        default:
            break;
    }

    __validate_recursive_time($data);
    __validate_utm($data);

    foreach($data as $_name => $_value) {
        if (strpos($_name, "_send_recursion_") === false) continue;
        if (empty($_value)) $data[$_name] = "-1";
    }

    if ($auto) return $auto;
    return null;
}

function __save_automation_content($data) {
    global $thisstaff;
    $auto = __validation($data);

    $_data = [
        "name"                    => trim($data["name"]),
        "description"             => trim($data["description"]),
        "trigger_id"              => (int)$data["trigger_id"],
        "sms_content"             => trim($data["sms_content"]),
        "send_one_time_at"        => trim($data["send_one_time_at"]),
        "notification_title"      => trim($data["notification_title"]),
        "notification_content"    => trim($data["notification_content"]),
        "notification_url"        => trim($data["notification_url"]),
        "notification_image_name" => trim($data["notification_image_name"]),
        "email_subject"           => trim($data["email_subject"]),
        "email_content"           => trim($data["email_content"]),
        "status"                  => (int)$data["status"],
        "send_after"              => (int)$data["send_after"],
        "recursion_minute"        => trim($data["recursion_minute"]),
        "recursion_hour"          => trim($data["recursion_hour"]),
        "recursion_day_of_week"   => trim($data["recursion_day_of_week"]),
        "recursion_date_in_month" => trim($data["recursion_date_in_month"]),
        "recursion_month"         => trim($data["recursion_month"]),
        "schedule"                => trim($data["schedule"]),
        "recursion_from"          => trim($data["recursion_from"]),
        "recursion_to"            => trim($data["recursion_to"]),
        "aac_campaign_id"         => (int)$data["aac_campaign_id"],
        "aac_utm_source_id"       => (int)$data["aac_utm_source_id"],
        "aac_utm_term_id"         => (int)$data["aac_utm_term_id"],
        "aac_utm_content_id"      => (int)$data["aac_utm_content_id"],
        "aac_utm_medium_id"       => (int)$data["aac_utm_medium_id"],
        "type"                    => trim($data["type"]),
    ];

    if ($auto) {
        $_data["updated_at"] = date('Y-m-d H:i:s');
        $_data["updated_by"] = $thisstaff->getId();
        $auto->setAll($_data);
        $id = $auto->save(true);
    } else {
        $_data["created_at"] = date('Y-m-d H:i:s');
        $_data["created_by"] = $thisstaff->getId();
        $auto = AutoActionContent::create($_data);
        if (!$auto) throw new Exception('Create error. Please check data again.');
        $id = $auto->save(true);
    }

    $log = AutoActionContentLog::create(
        [
            'auto_action_content_id' => $id,
            'snapshot' => json_encode($auto->ht),
            'time' => date('Y-m-d H:i:s'),
        ]
    );
    $log_id = $log->save();

    $auto->set('last_log_id', $log_id);
    return $auto->save(true);
}

function __delete($data) {
    if (isset($data['id']) && (int)trim($data['id'])) {
        $data['id'] = (int)trim($data['id']);
        $auto = AutoActionContent::lookup(['id' => $data['id']]);
        if (!$auto) throw new \Exception('Invalid automation content. Wrong ID');
        $auto->delete();

        return true;
    } else {
        throw new \Exception('Invalid automation content. Wrong ID');
    }
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    if ($_POST['action'] === 'save') {
        $errors = array();
        try {
            db_start_transaction();
            $data = $_POST;
            if (($id = __save_automation_content($data))) {
                db_commit();
                FlashMsg::set('ticket_create', 'Save Successfully.');
                header('Location: '.$cfg->getUrl().'scp/automation_settings.php?action=edit&id='.$id);
                exit;
            } else {
                db_rollback();
            }
        } catch (Exception $ex) {
            db_rollback();
            $auto = AutoActionContent::create($data);
            $errors[] = $ex->getMessage();
        }
    }

    if ($_POST['action'] === 'delete') {
        try {
            $data = $_POST;
            if (__delete($data)) {
                FlashMsg::set('ticket_create', 'Delete Successfully.');
                header('Location: '.$cfg->getUrl().'scp/automation_settings.php');
                exit;
            }
        } catch (Exception $ex) {
            $errors[] = $ex->getMessage();
        }
    }
}


$_REQUEST['action'] = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if (isset($_REQUEST['action'])) {
    switch ($_REQUEST['action']) {
        case 'add':
            $page_inc = "automation_settings_add.inc.php";
            break;
        case 'save':
            $page_inc = "automation_settings_add.inc.php";
            break;
        case 'edit':
            if (isset($_REQUEST['id']) && (int)trim($_REQUEST['id'])) {
                $_REQUEST['id'] = (int)trim($_REQUEST['id']);
                $auto = AutoActionContent::lookup(['id' => $_REQUEST['id']]);
                $utm_campaign = $utm_source = $utm_medium = $utm_content = $utm_term = null;
                $utm = AutoActionCampaign::fetchAll();

                if (!$auto) {
                    $errors['id'] = 'Content not found';
                } else {
                    $utm_campaign = AutoActionCampaign::lookup(['type' => 'campaign', 'id' => $auto->aac_campaign_id]);
                    $utm_source = AutoActionCampaign::lookup(['type' => 'source', 'id' => $auto->aac_utm_source_id]);
                    $utm_medium = AutoActionCampaign::lookup(['type' => 'medium', 'id' => $auto->aac_utm_medium_id]);
                    $utm_content = AutoActionCampaign::lookup(['type' => 'content', 'id' => $auto->aac_utm_content_id]);

                    if ($auto->aac_utm_term_id)
                        $utm_term = AutoActionCampaign::lookup(['type' => 'content', 'id' => $auto->aac_utm_term_id]);
                }

            } else {
                $errors['id'] = 'No ID';
            }

            $page_inc = "automation_settings_add.inc.php";
            break;
        default:
            $page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
            $total = 0;
            $offset = ($page-1) * PAGE_LIMIT;
            $params = $qs = [];
            if (isset($_REQUEST['name']) && trim($_REQUEST['name'])) {
                $params['name'] = trim($_REQUEST['name']);
                $qs += ['name' => trim($_REQUEST['name'])];
            }
            if (isset($_REQUEST['description']) && trim($_REQUEST['description'])) {
                $params['description'] = trim($_REQUEST['description']);
                $qs += ['description' => trim($_REQUEST['description'])];
            }
            $auto_action_content = AutoActionContent::getPagination($params, $offset, $total, PAGE_LIMIT);
            $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
            $page_inc = "automation_settings.inc.php";
            $pageNav->setURL($page_inc, $qs);

            break;
    }
}

require_once(STAFFINC_DIR.'header.inc.php');
include_once(STAFFINC_DIR.$page_inc);
include_once(STAFFINC_DIR.'footer.inc.php');
?>

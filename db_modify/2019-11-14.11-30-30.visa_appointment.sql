alter table visa_appointment drop column id;

alter table visa_appointment
    add id INT unsigned not null auto_increment primary key ;
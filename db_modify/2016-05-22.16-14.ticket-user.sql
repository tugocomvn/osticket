create table ost_ticket_user
(
    id int auto_increment
      primary key,
    ticket_id int null,
    user_id int null,
    add_at datetime null,
    remove_at datetime null,
    constraint ost_ticket_user_ticket_id_user_id_uindex
    unique (ticket_id, user_id)
)
;


CREATE UNIQUE INDEX ost_ticket_user_ticket_id_user_id_uindex ON osticket_dev.ost_ticket_user (ticket_id, user_id);
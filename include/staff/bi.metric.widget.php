<div class="card card-outline card-info">
    <div class="card-body">
        <div class="form-group">
            <select class="js-metric form-control">
                <option value=>-</option>
                <optgroup label="Tickets">
                    <option>Total</option>
                    <option>Cancel</option>
                    <option>Success</option>
                    <option>Returning</option>
                </optgroup>
                <optgroup label="Bookings">
                    <option>Total</option>
                    <option>Returning</option>
                </optgroup>
                <optgroup label="Customers">
                    <option>Total</option>
                    <option>Returning</option>
                </optgroup>
                <optgroup label="Call">
                    <option>Total In</option>
                    <option>Missed Call In</option>
                    <option>Total Out</option>
                    <option>Missed Call Out</option>
                </optgroup>
                <optgroup label="SMS">
                    <option>Birthday</option>
                    <option>After Sales</option>
                    <option>Ticket Cancel</option>
                    <option>Ticket Cancel-2Week</option>
                    <option>New Booking</option>
                </optgroup>
                <optgroup label="Email">
                    <option>Birthday</option>
                    <option>After Sales</option>
                    <option>Ticket Cancel</option>
                    <option>Ticket Cancel-2Week</option>
                    <option>New Booking</option>
                </optgroup>
                <optgroup label="Notification">
                    <option>Birthday</option>
                    <option>After Sales</option>
                    <option>Ticket Cancel</option>
                    <option>Ticket Cancel-2Week</option>
                    <option>New Booking</option>
                </optgroup>
            </select>
        </div>
        <button class="btn btn-default" type="button">
            Add a metric
        </button>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php
/*********************************************************************
    class.staff.php

    Everything about staff.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
include_once(INCLUDE_DIR.'class.ticket.php');
include_once(INCLUDE_DIR.'class.dept.php');
include_once(INCLUDE_DIR.'class.error.php');
include_once(INCLUDE_DIR.'class.team.php');
include_once(INCLUDE_DIR.'class.group.php');
include_once(INCLUDE_DIR.'class.passwd.php');
include_once(INCLUDE_DIR.'class.user.php');
include_once(INCLUDE_DIR.'class.auth.php');

class Staff extends AuthenticatedUser
implements EmailContact {

    var $ht;
    var $id;

    var $dept;
    var $departments;
    var $destinations;
    var $group;
    var $teams;
    var $timezone;
    var $stats;
    const TABLE_NAME = 'offlate_tmp';
    const VIEW_NAME = 'offlate_view';
    public static function get_inner_sql() {
        return " (SELECT DISTINCT
             vv.value
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', [OFFLATE_FORM]) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id LIMIT 1
          ) AS %s ";
    }

    public static function get_inner_sql_id() {
        return " (SELECT DISTINCT
             vv.value_id
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (" . implode(',', [OFFLATE_FORM]) . ")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }

    public static function get_inner_sql_null() {
        return " (SELECT DISTINCT
             IFNULL(vv.value, 0)
           FROM ost_ticket tt
             JOIN ost_form_entry ee ON tt.ticket_id = ee.object_id AND ee.object_type = 'T' AND ee.form_id IN (".implode(',', [OFFLATE_FORM]).")
             JOIN ost_form_entry_values vv ON ee.id = vv.entry_id
             JOIN ost_form_field ff ON ff.id = vv.field_id
           WHERE 1 AND ff.`name` LIKE '%s' AND tt.ticket_id=t.ticket_id
          ) AS %s ";
    }
    public static function get_replace_sql($ticket_id) {
        $select = self::get_select_sql();
        $ticket_id = db_input($ticket_id);
        $table_name = self::TABLE_NAME;
        return " REPLACE INTO $table_name $select WHERE t.ticket_id = $ticket_id LIMIT 1";
    }

    public static function get_create_table_sql() {
        $select = self::get_select_sql();
        $table_name = self::TABLE_NAME;
        $sql = " CREATE TABLE IF NOT EXISTS $table_name
            (UNIQUE INDEX ticket_id_ix(`ticket_id`)) 
            $select ";
        return $sql;
    }

    public static function get_create_view_sql() {
        $table_name = self::TABLE_NAME;
        $view_name = self::VIEW_NAME;
        return "CREATE OR REPLACE  VIEW $view_name AS SELECT * FROM $table_name";
    }
    public static function cre_after_del(){
        $table_name = self::TABLE_NAME;
        return "DROP TABLE IF EXISTS $table_name";
    }
    public static function get_offlate_view($ticket_id) {
        $view_name = self::VIEW_NAME;
        $sql = " SELECT * FROM $view_name WHERE ticket_id = $ticket_id LIMIT 1 ";
        $res = db_query($sql);
        if (!$res) return null;
        return db_fetch_array($res);
    }

    public static function select_ticket_from_offlate_view($ticket_number) {
        $view_name = self::VIEW_NAME;
        return db_query(" SELECT * FROM $view_name WHERE ticket_number LIKE '$ticket_number' ");
    }

    public static function create_view() {
        $sql = self::get_create_view_sql();
        return db_query($sql);
    }
    public static function drop_table(){
        $sql = self::cre_after_del();
        return db_query($sql);
    }
    public static function create_table() {
        $sql = self::get_create_table_sql();
        return db_query($sql);
    }

    public static function get_select_sql() {
        $_inner_sql = self::get_inner_sql();
        $_inner_sql_id = self::get_inner_sql_id();
        $str_time_offer=sprintf($_inner_sql,'time_offer','time_offer');
        $str_time_end=sprintf($_inner_sql,'time_end','time_end');
        $str_staff_offer=sprintf($_inner_sql_id,'staff_offer','staff_offer');
        $str_staff_offer_name=sprintf($_inner_sql,'staff_offer','ol_staff_offer_name');
        $str_time_accept=sprintf($_inner_sql,'time_accept','time_accept');
        $str_ol_reason=sprintf($_inner_sql,'ol_reason','ol_reason');
        $str_handover=sprintf($_inner_sql_id,'ol_handover','ol_handover');
        $str_handover_name=sprintf($_inner_sql,'ol_handover','ol_handover_name');
        $str_ol_status = sprintf($_inner_sql,'ol_status','ol_status');

        $offlate_form = implode(',', [OFFLATE_FORM]);

        return " SELECT DISTINCT
              t.ticket_id,
              t.number,
              t.topic_id,
              t.created,
              $str_time_offer,
              $str_time_end,
              $str_time_accept,
              $str_staff_offer,
              $str_staff_offer_name,
              $str_ol_reason,
              $str_handover,
              $str_handover_name,
              $str_ol_status
            FROM ost_ticket t
                JOIN ost_form_entry e ON t.ticket_id = e.object_id AND e.object_type = 'T' 
                    AND e.form_id IN ($offlate_form)
                JOIN ost_form_entry_values v ON e.id = v.entry_id
                JOIN ost_form_field f ON f.id = v.field_id";
    }
    public static function push($ticket_id = 0) {
        $ticket_id = intval($ticket_id);
        $sql_tmp_table = self::get_replace_sql($ticket_id);
        db_query($sql_tmp_table);
    }

    public static function getAll($sale_cs = false) {
        $sql = "SELECT staff_id, username FROM ".STAFF_TABLE." WHERE 1 ";
        if ($sale_cs) {
            $sql .= " AND group_id IN (".implode(',', unserialize(SALE_CS__GROUPIDs)).")";
        }
        $sql .= " ORDER BY firstname ";
        return db_query($sql);
    }

    public static function getAllName()
    {
        $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
            .' FROM '.STAFF_TABLE.' staff '
            .' ORDER by name';
        return db_query($sql);
    }

    public static function getAllNameFinance()
    {
        $sql = 'SELECT staff_id, CONCAT_WS(" ", firstname, lastname) as name 
            FROM '.STAFF_TABLE.' staff 
            LEFT JOIN '.DEPT_TABLE.' dept ON(staff.dept_id=dept.dept_id) 
            WHERE dept.dept_name LIKE "finance%"';    
        return db_query($sql);
    }

    public static function getNameStaff($staff_id)
    {
        $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
            .' FROM '.STAFF_TABLE.' staff  WHERE staff_id='.db_input($staff_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        if(!$row) return null;
        return $row['name'];
    }

    function getAllStaff(){
        $allStaff = self::getAll();
        $arr = [];
        while (($row = db_fetch_array($allStaff)) && $allStaff)
        {
            $arr[$row['staff_id']] = $row['username'];
        }
        return $arr;
    }

    public static function getAllUsername($sale_cs = false) {
        $sql = "SELECT staff_id, username FROM ".STAFF_TABLE." WHERE 1 ";
        if ($sale_cs) {
            $sql .= " AND group_id IN (".implode(',', unserialize(SALE_CS__GROUPIDs)).")";
        }
        $sql .= " ORDER BY username ";
        return db_query($sql);
    }

    public static function getAllwPhone() {
        $sql = "Select staff_id, mobile from ".STAFF_TABLE . " WHERE isactive = 1  ";
        $res = db_query($sql);
        if (!$res) return [];
        $data = [];
        while ($res && ($row = db_fetch_array($res))) {
            $phone = _String::formatPhoneNumber($row['mobile']);
            $phone_ctry = _String::formatPhoneNumber($row['mobile'], true);
            $convert = _String::convertMobilePhoneNumber($phone);
            $convert_ctry = _String::formatPhoneNumber($convert, true);
            $data[ $row['staff_id'] ] = [
                $phone,
                $phone_ctry,
                '+'.$phone_ctry,
                $convert,
                $convert_ctry,
                '+'.$convert_ctry,
            ];
        }

        return $data;
    }

    public static function findAllByPhone($search) {
        $search = preg_replace('/[^0-9]/', '', $search);
        $data = static::getAllwPhone();
        foreach ($data as $id => $phone_numbers) {
            foreach ($phone_numbers as $phone) {
                if (strcasecmp($search, $phone) === 0)
                    return $id;
            }
        }

        return null;
    }

    public static function findByEmail($search) {
        $search = str_replace(' ', '', $search);
        $sql = "Select staff_id from ".STAFF_TABLE." WHERE isactive = 1 AND email LIKE ".db_input($search);
        $res = db_query($sql);
        if (!$res) return null;
        $row = db_fetch_array($res);
        if (!$row && !isset($row['staff_id']) || !$row['staff_id']) return null;
        return $row['staff_id'];
    }

    function Staff($var) {
        $this->id =0;
        return ($this->load($var));
    }

    function load($var='') {

        if(!$var && !($var=$this->getId()))
            return false;

        $sql='SELECT staff.created as added, grp.*, staff.* '
            .' FROM '.STAFF_TABLE.' staff '
            .' LEFT JOIN '.GROUP_TABLE.' grp ON(grp.group_id=staff.group_id)
               WHERE ';

        if (is_numeric($var))
            $sql .= 'staff_id='.db_input($var);
        elseif (is_string($var) && strpos($var, '@') !== false && Validator::is_email($var))
            $sql .= 'email='.db_input($var);
        elseif (is_string($var))
            $sql .= 'username='.db_input($var);
        elseif (is_array($var)) {
            $cond = [];
            foreach ($var as $key => $val)
                $cond[] = $key.'='.db_input($val);

            if ($cond)
                $sql .= implode(' AND ', $cond) . ' LIMIT 1 ';
        }
        else
            return null;

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return NULL;


        $this->ht=db_fetch_array($res);
        $this->id  = $this->ht['staff_id'];
        $this->teams = $this->ht['teams'] = array();
        $this->group = $this->dept = null;
        $this->departments = $this->stats = array();
        $this->config = new Config('staff.'.$this->id);

        //WE have to patch info here to support upgrading from old versions.
        if(($time=strtotime($this->ht['passwdreset']?$this->ht['passwdreset']:$this->ht['added'])))
            $this->ht['passwd_change'] = time()-$time; //XXX: check timezone issues.

        if($this->ht['timezone_id'])
            $this->ht['tz_offset'] = Timezone::getOffsetById($this->ht['timezone_id']);
        elseif($this->ht['timezone_offset'])
            $this->ht['tz_offset'] = $this->ht['timezone_offset'];

        return ($this->id);
    }

    function reload() {
        return $this->load();
    }

    function __toString() {
        return (string) $this->getName();
    }

    function asVar() {
        return $this->__toString();
    }

    function getHashtable() {
        return $this->ht;
    }

        function getInfo() {
        return $this->config->getInfo() + $this->getHashtable();
    }

    // AuthenticatedUser implementation...
    // TODO: Move to an abstract class that extends Staff
    function getRole() {
        return 'staff';
    }

    function getAuthBackend() {
        list($authkey, ) = explode(':', $this->getAuthKey());
        return StaffAuthenticationBackend::getBackend($authkey);
    }

    /*compares user password*/
    function check_passwd($password, $autoupdate=true) {

        /*bcrypt based password match*/
        if(Passwd::cmp($password, $this->getPasswd()))
            return true;

        //Fall back to MD5
        if(!$password || strcmp($this->getPasswd(), MD5($password)))
            return false;

        //Password is a MD5 hash: rehash it (if enabled) otherwise force passwd change.
        $sql='UPDATE '.STAFF_TABLE.' SET passwd='.db_input(Passwd::hash($password))
            .' WHERE staff_id='.db_input($this->getId());

        if(!$autoupdate || !db_query($sql))
            $this->forcePasswdRest();

        return true;
    }

    function cmp_passwd($password) {
        return $this->check_passwd($password, false);
    }

    function hasPassword() {
        return (bool) $this->ht['passwd'];
    }

    function forcePasswdRest() {
        return db_query('UPDATE '.STAFF_TABLE.' SET change_passwd=1 WHERE staff_id='.db_input($this->getId()));
    }

    /* check if passwd reset is due. */
    function isPasswdResetDue() {
        global $cfg;
        return ($cfg && $cfg->getPasswdResetPeriod()
                    && $this->ht['passwd_change']>($cfg->getPasswdResetPeriod()*30*24*60*60));
    }

    function isPasswdChangeDue() {
        return $this->isPasswdResetDue();
    }

    function getTZoffset() {
        return $this->ht['tz_offset'];
    }

    function observeDaylight() {
        return $this->ht['daylight_saving']?true:false;
    }

    function getRefreshRate() {
        return $this->ht['auto_refresh_rate'];
    }

    function getPageLimit() {
        return $this->ht['max_page_size'];
    }

    function getId() {
        return $this->id;
    }
    function getUserId() {
        return $this->getId();
    }

    function getEmail() {
        return $this->ht['email'];
    }

    function getAvatar() {
        return $this->ht['avatar'];
    }

    function getCCEmail() {
        return $this->ht['cc_email'];
    }

    function getUserName() {
        return $this->ht['username'];
    }

    function getPasswd() {
        return $this->ht['passwd'];
    }

    function getName() {
        return new PersonsName(array('first' => $this->ht['firstname'], 'last' => $this->ht['lastname']));
    }

    function getFirstName() {
        return $this->ht['firstname'];
    }

    function getLastName() {
        return $this->ht['lastname'];
    }

    function getSignature() {
        return $this->ht['signature'];
    }

    function getDefaultSignatureType() {
        return $this->ht['default_signature_type'];
    }

    function getDefaultPaperSize() {
        return $this->ht['default_paper_size'];
    }

    function forcePasswdChange() {
        return ($this->ht['change_passwd']);
    }

    function getDepartments() {

        if($this->departments)
            return $this->departments;

        //Departments the staff is "allowed" to access...
        // based on the group they belong to + user's primary dept + user's managed depts.
        $sql='SELECT DISTINCT d.dept_id FROM '.STAFF_TABLE.' s '
            .' LEFT JOIN '.GROUP_DEPT_TABLE.' g ON(s.group_id=g.group_id) '
            .' INNER JOIN '.DEPT_TABLE.' d ON(d.dept_id=s.dept_id OR d.manager_id=s.staff_id OR d.dept_id=g.dept_id) '
            .' WHERE s.staff_id='.db_input($this->getId());

        $depts = array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id)=db_fetch_row($res))
                $depts[] = $id;
        } else { //Neptune help us! (fallback)
            $depts = array_merge($this->getGroup()->getDepartments(), array($this->getDeptId()));
        }

        $this->departments = array_filter(array_unique($depts));


        return $this->departments;
    }

    function getDepts() {
        return $this->getDepartments();
    }

    function getDestinations() {

        if($this->destinations)
            return $this->destinations;

        $sql='SELECT DISTINCT destination FROM '.STAFF_TABLE.' s '
            .' INNER JOIN '.GROUP_DES_TABLE.' g ON(s.group_id=g.group_id) '
            .' WHERE s.staff_id='.db_input($this->getId());

        $dess = array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id)=db_fetch_row($res))
                $dess[] = $id;
        }
        $this->destinations = array_filter(array_unique($dess));


        return $this->destinations;
    }

    function getDess() {
        return $this->getDestinations();
    }

    function getManagedDepartments() {

        return ($depts=Dept::getDepartments(
                    array('manager' => $this->getId())
                    ))?array_keys($depts):array();
    }

    function getGroupId() {
        return $this->ht['group_id'];
    }

    function getGroup() {

        if(!$this->group && $this->getGroupId())
            $this->group = Group::lookup($this->getGroupId());

        return $this->group;
    }

    function getDeptId() {
        return $this->ht['dept_id'];
    }

    function getPhone() {
        return $this->ht['phone'];
    }

    function getMobilePhone() {
        return $this->ht['mobile'];
    }

    function getZaloUid() {
        return $this->ht['zalo_uid'];
    }

    function getDept() {

        if(!$this->dept && $this->getDeptId())
            $this->dept= Dept::lookup($this->getDeptId());

        return $this->dept;
    }

    function getLanguage() {
        static $cached = false;
        if (!$cached) $cached = &$_SESSION['staff:lang'];

        if (!$cached) {
            $cached = $this->config->get('lang');
            if (!$cached)
                $cached = Internationalization::getDefaultLanguage();
        }
        return $cached;
    }

    function isManager() {
        return (($dept=$this->getDept()) && $dept->getManagerId()==$this->getId());
    }

    function isStaff() {
        return TRUE;
    }

    function isGroupActive() {
        return ($this->ht['group_enabled']);
    }

    function isactive() {
        return ($this->ht['isactive']);
    }

    function isVisible() {
         return ($this->ht['isvisible']);
    }

    function onVacation() {
        return ($this->ht['onvacation']);
    }

    function isAvailable() {
        return ($this->isactive() && $this->isGroupActive() && !$this->onVacation());
    }

    function showAssignedOnly() {
        return ($this->ht['assigned_only']);
    }

    function isAccessLimited() {
        return $this->showAssignedOnly();
    }

    function isAdmin() {
        return ($this->ht['isadmin']);
    }

    function isTeamMember($teamId) {
        return ($teamId && in_array($teamId, $this->getTeams()));
    }

    function canAccessDept($deptId) {
        return ($deptId && in_array($deptId, $this->getDepts()) && !$this->isAccessLimited());
    }

    function canCreateTickets() {
        return ($this->ht['can_create_tickets']);
    }

    function can_view_personal_checkin() {
        return $this->ht['can_view_personal_checkin'];
    }

    function can_view_all_checkin() {
        return $this->ht['can_view_all_checkin'];
    }

    function can_do_checkin() {
        return $this->ht['can_do_checkin'];
    }

    function can_view_ticket_activity() {
        return $this->ht['can_view_ticket_activity'];
    }

    function can_view_general_statistics() {
        return $this->ht['can_view_general_statistics'];
    }

    function can_view_agent_directory() {
        return $this->ht['can_view_agent_directory'];
    }

    function can_view_ticket_analytics() {
        return $this->ht['can_view_ticket_analytics'];
    }

    function can_view_booking_returning() {
        return $this->ht['can_view_booking_returning'];
    }

    function can_view_agent_statistics() {
        return $this->ht['can_view_agent_statistics'];
    }

    function can_export_tickets() {
        return $this->ht['can_export_tickets'];
    }

    function can_view_guest_directory() {
        return $this->ht['can_view_guest_directory'];
    }

    function can_edit_guest_info() {
        return $this->ht['can_edit_guest_info'];
    }

    function can_view_organizations() {
        return $this->ht['can_view_organizations'];
    }

    function can_create_organizations() {
        return $this->ht['can_create_organizations'];
    }

    function can_edit_organizations() {
        return $this->ht['can_edit_organizations'];
    }

    function canEditTickets() {
        return ($this->ht['can_edit_tickets']);
    }

    function canDeleteTickets() {
        return ($this->ht['can_delete_tickets']);
    }

    function canCloseTickets() {
        return ($this->ht['can_close_tickets']);
    }

    function canPostReply() {
        return ($this->ht['can_post_ticket_reply']);
    }

    function canCreatePayments() {
        return ($this->ht['can_create_payment']);
    }

    function canViewPayments() {
        return ($this->ht['can_view_payment']);
    }

    function canViewPaymentsChart(){
        return ($this->ht['can_view_payment_chart']);
    }

    function canEditPayments() {
        return ($this->ht['can_edit_payment']);
    }

    function canCreateBookings() {
        return ($this->ht['can_create_booking']);
    }

    function canViewBookings() {
        return ($this->ht['can_view_booking']);
    }
    function canEditBookings() {
        return ($this->ht['can_edit_booking']);
    }

    function canExportBookings() {
        return ($this->ht['can_export_booking']);
    }

    function canChangeBookingStatus() {
        return ($this->ht['can_change_booking_status']);
    }

    function canAcceptOfflates(){
        return ($this->ht['can_accept_offlate']);
    }

    function canCreateOfflates() {
        return ($this->ht['can_create_offlate']);
    }

    function canViewOfflates() {
        return ($this->ht['can_view_offlate']);
    }

    function canEditOfflates() {
        return ($this->ht['can_edit_offlate']);
    }

    function canViewOperatorLists() {
        return ($this->ht['can_view_operator_list']);
    }

    function canEditOparetorLists() {
        return ($this->ht['can_edit_operator_list']);
    }

    function canCreateOperatorListItem() {
        return ($this->ht['can_create_operator_list_item']);
    }

    function canOpEditPax() {
        return ($this->ht['op_edit_pax']);
    }

    function canEditSettlement() {
        return ($this->ht['op_edit_settlement']);
    }

    function canViewSettlementList() {
        return ($this->ht['op_view_settlement_list']);
    }

    function canViewExpectedSettlement() {
        return ($this->ht['op_view_expected_settlement']);
    }

    function canViewActualSettlement() {
        return ($this->ht['op_view_actual_settlement']);
    }

    function canViewStaffStats() {
        return ($this->ht['can_view_staff_stats']);
    }

    function canAssignTickets() {
        return ($this->ht['can_assign_tickets']);
    }

    function canTransferTickets() {
        return ($this->ht['can_transfer_tickets']);
    }

    function canBanEmails() {
        return ($this->ht['can_ban_emails']);
    }

    function canManageTickets() {
        return ($this->isAdmin()
                 || $this->canDeleteTickets()
                    || $this->canCloseTickets());
    }

    function canManagePremade() {
        return ($this->ht['can_manage_premade']);
    }

    function canManageCannedResponses() {
        return $this->canManagePremade();
    }

    function canManageFAQ() {
        return ($this->ht['can_manage_faq']);
    }

    function canManageFAQs() {
        return $this->canManageFAQ();
    }

    function showAssignedTickets() {
        return ($this->ht['show_assigned_tickets']);
    }

    function canViewTourList() {
        return ($this->ht['can_view_tour_list']);
    }

    function canViewMemberList() {
        return ($this->ht['can_view_member_list']);
    }

    function canEditPointLoyalty() {
        return ($this->ht['can_edit_point_loyalty']);
    }

    //-----------------permission access visa--------------
    function canViewVisaDocuments() {
        return ($this->ht['can_view_visa_documents']);
    }
    function canManageVisaItems() {
        return ($this->ht['can_manage_visa_items']);
    }
    function canMangeVisaPaxDocuments() {
        return ($this->ht['can_manage_visa_pax_documents']);
    }
    function canCancelTour(){
        return ($this->ht['can_cancel_tour']);
    }
    function canViewRetailPrice(){
        return ($this->ht['can_view_retail_price']);
    }
    function canViewNetPrice(){
        return ($this->ht['can_view_net_price']);
    }
    function canEditNetPrice(){
        return ($this->ht['can_edit_net_price']);
    }
    function canEditRetailPrice(){
        return ($this->ht['can_edit_retail_price']);
    }
    function canRequestEditTour(){
        return ($this->ht['can_request_edit_tour']);
    }
    function canApproveEditTour(){
        return ($this->ht['can_approve_edit_tour']);
    }
    function canHoldReservation(){
        return ($this->ht['can_hold_reservation']);
    }
    function canCancelMemberReservation(){
        return ($this->ht['can_cancel_member_reservation']);
    }
    function canCancelLeaderReservation(){
        return ($this->ht['can_cancel_leader_reservation']);
    }
    function canCancelSuperReservation(){
        return ($this->ht['can_cancel_super_reservation']);
    }
    function canApproveLeaderReservation(){
        return ($this->ht['can_approve_leader']);
    }
    function canApproveOperatorReservation(){
        return ($this->ht['can_approve_operator']);
    }
    function canApproveVisaReservation(){
        return ($this->ht['can_approve_visa']);
    }
    function canViewApproveLeader(){
        return ($this->ht['can_view_approve_leader']);
    }
    function canViewApproveOperator(){
        return ($this->ht['can_view_approve_operator']);
    }
    function canViewApproveVisa(){
        return ($this->ht['can_view_approve_visa']);
    }
    //---permission receipt---
    function canRejectReceipt(){
        return ($this->ht['can_reject_receipt']);
    }
    function canCreateReceipt(){
        return ($this->ht['can_create_receipt']);
    }
    function canEditReceipt(){
        return ($this->ht['can_edit_receipt']);
    }
    function canViewReceipt(){
        return ($this->ht['can_view_receipt']);
    }
    function canViewReceiptList(){
        return ($this->ht['can_view_receipt_list']);
    }
    function canApproveReceipt(){
        return ($this->ht['can_approve_receipt']);
    }
    function canMangeAllReceipt(){
        return ($this->ht['can_manage_all_receipt']);
    }
    function getTeams() {

        if(!$this->teams) {
            $sql='SELECT team_id FROM '.TEAM_MEMBER_TABLE
                .' WHERE staff_id='.db_input($this->getId());
            if(($res=db_query($sql)) && db_num_rows($res))
                while(list($id)=db_fetch_row($res))
                    $this->teams[] = $id;
        }

        return $this->teams;
    }

    function getRandomTeamMembers($limit = 3) {
        if (!$limit) return null;

        $teams = $this->getTeams();
        $team = isset($teams[0]) ? $teams[0] : null;
        if (!$team) return null;

        $team = new Team($team);
        $members = $team->getMembers();
        $count = count($members);
        if ($count <= $limit) return $members;

        $res = [];
        while (count($res) < $limit) {
            $index = rand(0, $count-1);
            $res[ $index ] = $members[ $index ];
        }
        return $res;
    }

    /* stats */

    function resetStats() {
        $this->stats = array();
    }

    /* returns staff's quick stats - used on nav menu...etc && warnings */
    function getTicketsStats($topic_id=1) {//Minh Bao: add param $topic_id

        if(!$this->stats['tickets'])
            $this->stats['tickets'] = Ticket::getStaffStats($this,$topic_id);

        return  $this->stats['tickets'];
    }

    function getNumAssignedTickets() {
        return ($stats=$this->getTicketsStats())?$stats['assigned']:0;
    }

    function getNumClosedTickets() {
        return ($stats=$this->getTicketsStats())?$stats['closed']:0;
    }

    //Staff profile update...unfortunately we have to separate it from admin update to avoid potential issues
    function updateProfile($vars, &$errors) {
        global $cfg;

        $vars['firstname']=Format::striptags($vars['firstname']);
        $vars['lastname']=Format::striptags($vars['lastname']);

        if($this->getId()!=$vars['id'])
            $errors['err']=__('Internal error occurred');

        if(!$vars['firstname'])
            $errors['firstname']=__('First name is required');

        if(!$vars['lastname'])
            $errors['lastname']=__('Last name is required');

        if(!$vars['email'] || !Validator::is_valid_email($vars['email']))
            $errors['email']=__('Valid email is required');
        elseif(Email::getIdByEmail($vars['email']))
            $errors['email']=__('Already in-use as system email');
        elseif(($uid=Staff::getIdByEmail($vars['email'])) && $uid!=$this->getId())
            $errors['email']=__('Email already in-use by another agent');

        $vars['cc_email'] = trim($vars['cc_email']);
        if($vars['cc_email'] && !Validator::is_valid_email($vars['cc_email']))
            $errors['email']=__('Valid cc email is required');

        if($vars['phone'] && !Validator::is_phone($vars['phone']))
            $errors['phone']=__('Valid phone number is required');

        if($vars['mobile'] && !Validator::is_phone($vars['mobile']))
            $errors['mobile']=__('Valid phone number is required');

        if(!$vars['skype'])
            $errors['skype']=__('Điền username Skype');

        if(!$vars['relative'])
            $errors['relative']=__('Điền tên người thân để liên lạc truòng hợp khẩn cấp');

        if(!$vars['relative_mobile'])
            $errors['relative_mobile']=__('Điền số điện thoại của người thân để liên lạc truòng hợp khẩn cấp');

        if(!$vars['address'])
            $errors['address']=__('Điền địa chỉ nơi lưu trú');

        if($vars['passwd1'] || $vars['passwd2'] || $vars['cpasswd']) {

            if(!$vars['passwd1'])
                $errors['passwd1']=__('New password is required');
            elseif($vars['passwd1'] && strlen($vars['passwd1'])<6)
                $errors['passwd1']=__('Password must be at least 6 characters');
            elseif($vars['passwd1'] && strcmp($vars['passwd1'], $vars['passwd2']))
                $errors['passwd2']=__('Passwords do not match');

            if (($rtoken = $_SESSION['_staff']['reset-token'])) {
                $_config = new Config('pwreset');
                if ($_config->get($rtoken) != $this->getId())
                    $errors['err'] =
                        __('Invalid reset token. Logout and try again');
                elseif (!($ts = $_config->lastModified($rtoken))
                        && ($cfg->getPwResetWindow() < (time() - strtotime($ts))))
                    $errors['err'] =
                        __('Invalid reset token. Logout and try again');
            }
            elseif(!$vars['cpasswd'])
                $errors['cpasswd']=__('Current password is required');
            elseif(!$this->cmp_passwd($vars['cpasswd']))
                $errors['cpasswd']=__('Invalid current password!');
            elseif(!strcasecmp($vars['passwd1'], $vars['cpasswd']))
                $errors['passwd1']=__('New password MUST be different from the current password!');
        }

        if(!$vars['timezone_id'])
            $errors['timezone_id']=__('Time zone selection is required');

        if($vars['default_signature_type']=='mine' && !$vars['signature'])
            $errors['default_signature_type'] = __("You don't have a signature");

        // avatar
        $uploads_dir = ROOT_DIR.'staff_avatar/';
        $ext_type = array('image/jpg','image/jpeg','image/png');
        $avatar = null;

        if (isset($_FILES["avatar"]) && file_exists($_FILES["avatar"]["tmp_name"])) {
            $tmp_name = $_FILES["avatar"]["tmp_name"];
            $file_info = new finfo(FILEINFO_MIME_TYPE);
            $mime_type = $file_info->buffer(file_get_contents($tmp_name));

            if (!in_array($mime_type, $ext_type))
                $errors['avatar'] = 'Invalid file type';
            else {
                $ext = explode('/', $mime_type);
                if (isset($ext[1])) {
                    $ext = $ext[1];
                    $avatar = md5('staff' . $this->getId() . 'tugo_avatar').'.'.$ext;
                    @move_uploaded_file($tmp_name, "$uploads_dir$avatar");
                } else {
                    $errors['avatar'] = 'Invalid file type';
                }
            }
        }

        if (isset($_REQUEST['delete_avatar']) && $_REQUEST['delete_avatar'])
            $avatar = '';

        if($errors) return false;

        $this->config->set('lang', $vars['lang']);
        $_SESSION['staff:lang'] = null;
        TextDomain::configureForUser($this);

        $sql='UPDATE '.STAFF_TABLE.' SET updated=NOW() '
            .' ,firstname='.db_input($vars['firstname'])
            .' ,lastname='.db_input($vars['lastname'])
            .' ,email='.db_input($vars['email'])
            .' ,cc_email='.db_input($vars['cc_email'])
            .' ,phone="'.db_input(Format::phone($vars['phone']),false).'"'
            .' ,phone_ext='.db_input($vars['phone_ext'])
            .' ,mobile="'.db_input(Format::phone($vars['mobile']),false).'"'
            .' ,skype='.db_input(trim($vars['skype']))
            .' ,relative='.db_input(trim($vars['relative']))
            .' ,relative_mobile="'.db_input(Format::phone($vars['relative_mobile']), false).'"'
            .' ,address='.db_input(trim($vars['address']))
            .' ,signature='.db_input(Format::sanitize($vars['signature']))
            .' ,timezone_id='.db_input($vars['timezone_id'])
            .' ,daylight_saving='.db_input(isset($vars['daylight_saving'])?1:0)
            .' ,show_assigned_tickets='.db_input(isset($vars['show_assigned_tickets'])?1:0)
            .' ,max_page_size='.db_input($vars['max_page_size'])
            .' ,auto_refresh_rate='.db_input($vars['auto_refresh_rate'])
            .' ,default_signature_type='.db_input($vars['default_signature_type'])
            . (!is_null($avatar) ? ' ,avatar='.db_input($avatar) : ' ')
            .' ,default_paper_size='.db_input($vars['default_paper_size']);


        if($vars['passwd1']) {
            $sql.=' ,change_passwd=0, passwdreset=NOW(), passwd='.db_input(Passwd::hash($vars['passwd1']));
            $info = array('password' => $vars['passwd1']);
            Signal::send('auth.pwchange', $this, $info);
            $this->cancelResetTokens();
        }

        $sql.=' WHERE staff_id='.db_input($this->getId());

        //echo $sql;

        return (db_query($sql));
    }


    function updateTeams($teams) {

        if($teams) {
            foreach($teams as $k=>$id) {
                $sql='INSERT IGNORE INTO '.TEAM_MEMBER_TABLE.' SET updated=NOW() '
                    .' ,staff_id='.db_input($this->getId()).', team_id='.db_input($id);
                db_query($sql);
            }
        }

        $sql='DELETE FROM '.TEAM_MEMBER_TABLE.' WHERE staff_id='.db_input($this->getId());
        if($teams)
            $sql.=' AND team_id NOT IN('.implode(',', db_input($teams)).')';

        db_query($sql);

        return true;
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->updateTeams($vars['teams']);
        $this->reload();

        Signal::send('model.modified', $this);

        return true;
    }

    function delete() {
        global $thisstaff;

        if (!$thisstaff || $this->getId() == $thisstaff->getId())
            return 0;

        $sql='DELETE FROM '.STAFF_TABLE
            .' WHERE staff_id = '.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {
            // DO SOME HOUSE CLEANING
            //Move remove any ticket assignments...TODO: send alert to Dept. manager?
            db_query('UPDATE '.TICKET_TABLE.' SET staff_id=0 WHERE staff_id='.db_input($this->getId()));

            //Update the poster and clear staff_id on ticket thread table.
            db_query('UPDATE '.TICKET_THREAD_TABLE
                    .' SET staff_id=0, poster= '.db_input($this->getName()->getOriginal())
                    .' WHERE staff_id='.db_input($this->getId()));

            //Cleanup Team membership table.
            db_query('DELETE FROM '.TEAM_MEMBER_TABLE.' WHERE staff_id='.db_input($this->getId()));

            // Destrory config settings
            $this->config->destroy();
        }

        Signal::send('model.deleted', $this);

        return $num;
    }

    /**** Static functions ********/
    function getStaffMembers($availableonly=false) {
        global $cfg;

        $sql = 'SELECT s.staff_id, s.firstname, s.lastname FROM '
            .STAFF_TABLE.' s ';

        if($availableonly) {
            $sql.=' INNER JOIN '.GROUP_TABLE.' g ON(g.group_id=s.group_id AND g.group_enabled=1) '
                 .' WHERE s.isactive=1 AND s.onvacation=0';
        }

        switch ($cfg->getDefaultNameFormat()) {
        case 'last':
        case 'lastfirst':
        case 'legal':
            $sql .= ' ORDER BY s.lastname, s.firstname';
            break;

        default:
            $sql .= ' ORDER BY s.firstname, s.lastname';
        }

        $users=array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id, $fname, $lname) = db_fetch_row($res))
                $users[$id] = new PersonsName(
                    array('first' => $fname, 'last' => $lname));
        }

        return $users;
    }
    function getSaleOnly($include_cs = false, $status = null) {
        global $cfg;

        $sql = 'SELECT s.staff_id, s.firstname, s.lastname, s.username FROM '
            .STAFF_TABLE.' s ';

        $sql.=' WHERE 1
            '.(!is_null($status) ? ' AND s.isactive='. ($status ? '1':'0') : ' ').' 
            AND s.group_id IN 
            ('.implode(',', unserialize($include_cs ? SALE_CS__GROUPIDs : SALE_GROUPIDs)).')';

        switch ($cfg->getDefaultNameFormat()) {
        case 'last':
        case 'lastfirst':
        case 'legal':
            $sql .= ' ORDER BY s.lastname, s.firstname';
            break;

        default:
            $sql .= ' ORDER BY s.firstname, s.lastname';
        }

        $users=array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id, $fname, $lname, $username) = db_fetch_row($res))
                $users[$id] = new PersonsName(
                    array('first' => $fname, 'last' => $lname)) .' | '. $username;
        }

        return $users;
    }
    public static function getEmailSaleOnly($include_cs = false, $status = null) {
        global $cfg;

        $sql = 'SELECT s.staff_id, s.email FROM '
            .STAFF_TABLE.' s ';

        $sql.=' WHERE 1
            '.(!is_null($status) ? ' AND s.isactive='. ($status ? '1':'0') : ' ').' 
            AND s.group_id IN 
            ('.implode(',', unserialize($include_cs ? SALE_CS__GROUPIDs : SALE_GROUPIDs)).')';



        $users=array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id, $email) = db_fetch_row($res))
                $users[$id] = $email;
        }

        return $users;
    }

    function getAvailableStaffMembers() {
        return self::getStaffMembers(true);
    }

    function getIdByUsername($username) {
        $sql='SELECT staff_id FROM '.STAFF_TABLE.' WHERE username='.db_input($username);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function getIdByEmail($email) {
        $sql='SELECT staff_id FROM '.STAFF_TABLE.' WHERE email='.db_input($email);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function getIdByMobilePhone($phone_number) {
        $sql='SELECT staff_id FROM '.STAFF_TABLE.' WHERE mobile='.db_input($phone_number);
        $id = null;

        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        if ($id) return $id;

        $sql='SELECT staff_id, mobile FROM '.STAFF_TABLE;
        $res = db_query($sql);
        if (!$res) return null;

        while((list($id, $mobile) = db_fetch_array($res))) {
            $mobile = preg_replace('/[^0-9]/', '', $mobile);
            if ($mobile == $phone_number) return $id;
        }

        return null;
    }

    function lookup($id) {
        return ($id && ($staff= new Staff($id)) && $staff->getId()) ? $staff : null;
    }

    function create($vars, &$errors) {
        if(($id=self::save(0, $vars, $errors)) && ($staff=Staff::lookup($id))) {
            if ($vars['teams'])
                $staff->updateTeams($vars['teams']);
            if ($vars['welcome_email'])
                $staff->sendResetEmail('registration-staff', false);
            Signal::send('model.created', $staff);
        }

        return $id;
    }

    function cancelResetTokens() {
        // TODO: Drop password-reset tokens from the config table for
        //       this user id
        $sql = 'DELETE FROM '.CONFIG_TABLE.' WHERE `namespace`="pwreset"
            AND `value`='.db_input($this->getId());
        db_query($sql, false);
        unset($_SESSION['_staff']['reset-token']);
    }

    function sendResetEmail($template='pwreset-staff', $log=true) {
        global $ost, $cfg;

        $content = Page::lookup(Page::getIdByType($template));
        $token = Misc::randCode(48); // 290-bits

        if (!$content)
            return new Error(/* @trans */ 'Unable to retrieve password reset email template');

        $vars = array(
            'url' => $ost->getConfig()->getBaseUrl(),
            'token' => $token,
            'staff' => $this,
            'recipient' => $this,
            'reset_link' => sprintf(
                "%s/scp/pwreset.php?token=%s",
                $ost->getConfig()->getBaseUrl(),
                $token),
        );
        $vars['link'] = &$vars['reset_link'];

        if (!($email = $cfg->getAlertEmail()))
            $email = $cfg->getDefaultEmail();

        $info = array('email' => $email, 'vars' => &$vars, 'log'=>$log);
        Signal::send('auth.pwreset.email', $this, $info);

        if ($info['log'])
            $ost->logWarning(_S('Agent Password Reset'), sprintf(
             _S('Password reset was attempted for agent: %1$s<br><br>
                Requested-User-Id: %2$s<br>
                Source-Ip: %3$s<br>
                Email-Sent-To: %4$s<br>
                Email-Sent-Via: %5$s'),
                $this->getName(),
                $_POST['userid'],
                $_SERVER['REMOTE_ADDR'],
                $this->getEmail(),
                $email->getEmail()
            ), false);

        $msg = $ost->replaceTemplateVariables(array(
            'subj' => $content->getName(),
            'body' => $content->getBody(),
        ), $vars);

        $_config = new Config('pwreset');
        $_config->set($vars['token'], $this->getId());

        $email->send($this->getEmail(), Format::striptags($msg['subj']),
            $msg['body']);
    }

    function save($id, $vars, &$errors) {

        $vars['username']=Format::striptags($vars['username']);
        $vars['firstname']=Format::striptags($vars['firstname']);
        $vars['lastname']=Format::striptags($vars['lastname']);

        if($id && $id!=$vars['id'])
            $errors['err']=__('Internal Error');

        if(!$vars['firstname'])
            $errors['firstname']=__('First name required');
        if(!$vars['lastname'])
            $errors['lastname']=__('Last name required');

        $error = '';
        if(!$vars['username'] || !Validator::is_username($vars['username'], $error))
            $errors['username']=($error) ? $error : __('Username is required');
        elseif(($uid=Staff::getIdByUsername($vars['username'])) && $uid!=$id)
            $errors['username']=__('Username already in use');

        if(!$vars['email'] || !Validator::is_valid_email($vars['email']))
            $errors['email']=__('Valid email is required');
        elseif(Email::getIdByEmail($vars['email']))
            $errors['email']=__('Already in use system email');
        elseif(($uid=Staff::getIdByEmail($vars['email'])) && $uid!=$id)
            $errors['email']=__('Email already in use by another agent');

        if($vars['phone'] && !Validator::is_phone($vars['phone']))
            $errors['phone']=__('Valid phone number is required');

        if($vars['mobile'] && !Validator::is_phone($vars['mobile']))
            $errors['mobile']=__('Valid phone number is required');

        if($vars['passwd1'] || $vars['passwd2'] || !$id) {
            if($vars['passwd1'] && strcmp($vars['passwd1'], $vars['passwd2'])) {
                $errors['passwd2']=__('Passwords do not match');
            }
            elseif ($vars['backend'] != 'local' || $vars['welcome_email']) {
                // Password can be omitted
            }
            elseif(!$vars['passwd1'] && !$id) {
                $errors['passwd1']=__('Temporary password is required');
                $errors['temppasswd']=__('Required');
            } elseif($vars['passwd1'] && strlen($vars['passwd1'])<6) {
                $errors['passwd1']=__('Password must be at least 6 characters');
            }
        }

        if(!$vars['dept_id'])
            $errors['dept_id']=__('Department is required');

        if(!$vars['group_id'])
            $errors['group_id']=__('Group is required');

        if(!$vars['timezone_id'])
            $errors['timezone_id']=__('Time zone selection is required');

        // Ensure we will still have an administrator with access
        if ($vars['isadmin'] !== '1' || $vars['isactive'] !== '1') {
            $sql = 'select count(*), max(staff_id) from '.STAFF_TABLE
                .' WHERE isadmin=1 and isactive=1';
            if (($res = db_query($sql))
                    && (list($count, $sid) = db_fetch_row($res))) {
                if ($count == 1 && $sid == $id) {
                    $errors['isadmin'] = __(
                        'Cowardly refusing to remove or lock out the only active administrator'
                    );
                }
            }
        }

        if($errors) return false;


        $sql='SET updated=NOW() '
            .' ,isadmin='.db_input($vars['isadmin'])
            .' ,isactive='.db_input($vars['isactive'])
            .' ,isvisible='.db_input(isset($vars['isvisible'])?1:0)
            .' ,onvacation='.db_input(isset($vars['onvacation'])?1:0)
            .' ,assigned_only='.db_input(isset($vars['assigned_only'])?1:0)
            .' ,dept_id='.db_input($vars['dept_id'])
            .' ,group_id='.db_input($vars['group_id'])
            .' ,timezone_id='.db_input($vars['timezone_id'])
            .' ,daylight_saving='.db_input(isset($vars['daylight_saving'])?1:0)
            .' ,username='.db_input($vars['username'])
            .' ,firstname='.db_input($vars['firstname'])
            .' ,lastname='.db_input($vars['lastname'])
            .' ,email='.db_input($vars['email'])
            .' ,backend='.db_input($vars['backend'])
            .' ,phone="'.db_input($vars['phone'],false).'"'
            .' ,phone_ext='.db_input($vars['phone_ext'])
            .' ,mobile="'.db_input($vars['mobile'],false).'"'
            .' ,skype='.db_input(trim($vars['skype']))
            .' ,relative='.db_input(trim($vars['relative']))
            .' ,relative_mobile="'.db_input(Format::phone($vars['relative_mobile']), false).'"'
            .' ,address='.db_input(trim($vars['address']))
            .' ,zalo_uid="'.db_input($vars['zalo_uid'],false).'"'
            .' ,signature='.db_input(Format::sanitize($vars['signature']))
            .' ,notes='.db_input(Format::sanitize($vars['notes']));

        if($vars['passwd1']) {
            $sql.=' ,passwd='.db_input(Passwd::hash($vars['passwd1']));

            if(isset($vars['change_passwd']))
                $sql.=' ,change_passwd=1';
        }
        elseif (!isset($vars['change_passwd']))
            $sql .= ' ,change_passwd=0';

        if($id) {
            $sql='UPDATE '.STAFF_TABLE.' '.$sql.' WHERE staff_id='.db_input($id);
            if(db_query($sql) && db_affected_rows())
                return true;

            $errors['err']=sprintf(__('Unable to update %s.'), __('this agent'))
               .' '.__('Internal error occurred');
        } else {
            $sql='INSERT INTO '.STAFF_TABLE.' '.$sql.', created=NOW()';
            if(db_query($sql) && ($uid=db_insert_id()))
                return $uid;

            $errors['err']=sprintf(__('Unable to create %s.'), __('this agent'))
               .' '.__('Internal error occurred');
        }

        return false;
    }

    /**
     * @author buupham
     * @param $ticket_id
     * Check SMS quota. If under the limit, staff can send SMS to guest.
     * The quota contains quota per ticket and quota per staff
     * This uses the TICKET_THREAD_TABLE as a log for checking
     */
    function checkSMSQuota($staff, $ticket, &$errors) {
        global $cfg;
        // get quota settings
        $quota = $cfg->getSMSQuota();

        if (is_numeric($staff)) {
            if (!($staff = Staff::lookup($staff))) {
                $errors['staff'] = 'Invalid staff info';
                return false;
            }
        }

        if (!(is_object($staff) && $staff instanceof Staff)) {
            $errors['staff'] = 'Invalid staff info';
            return false;
        }

        $staff_today = $staff->getSMSSendToday();

        if (is_numeric($ticket)) {
            if (!($ticket = Ticket::lookup($ticket))) {
                $errors['ticket'] = 'Invalid ticket info';
                return false;
            }
        }

        if (!(is_object($ticket) && $ticket instanceof Ticket)) {
            $errors['ticket'] = 'Invalid ticket info';
            return false;
        }

        $ticket_today = $ticket->getSMSSendToday();

        if ($staff->isAdmin()) return true; // quyền lực vô biên :v

        if (!(isset($quota['per_staff']) && $quota['per_staff'] > $staff_today))
            $errors['quota'] = 'Quota limit reached: ' .  $quota['per_staff'] . ' SMSs/staff/day';

        if (!(isset($quota['per_ticket']) && $quota['per_ticket'] > $ticket_today))
            $errors['quota'] = 'Quota limit reached: ' .  $quota['per_ticket'] . ' SMSs/ticket/day';

        if ($errors) return false;
        return true;
    }

    function getSMSSendToday() {
        $sql = sprintf(
            "SELECT COUNT(DISTINCT id) FROM %s 
              WHERE staff_id = %d 
              AND DATE(created) = DATE(NOW())
              AND thread_type LIKE 'S'
            ",
            TICKET_THREAD_TABLE,
            $this->getId()
        );

        return db_count($sql);
    }
}

class StaffSigninTokenModel extends VerySimpleModel {
    static $meta = array(
        'table' => STAFF_SIGNIN_TOKEN_TABLE,
        'pk' => array('id')
    );
}

class StaffSigninToken extends StaffSigninTokenModel{
    public static function checkStaff($token){
        $sql = 'select user_name from '.static::$meta['table']. ' where signin_token = '.db_input($token). ' and expired_at >= now()';
        $result = db_query($sql);
        if(!$result)
            return '';
        else
            return db_fetch_array($result)['user_name'];

    }
}
?>

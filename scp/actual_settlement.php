<?php
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.booking.php');
require_once(INCLUDE_DIR.'class.pax.php');
require_once(INCLUDE_DIR.'class.account.php');
require_once(INCLUDE_DIR.'class.tour-destination.php');
require_once(INCLUDE_DIR.'class.list.php');

global $thisstaff, $cfg;

if (!$thisstaff
    || !($thisstaff->canViewActualSettlement() || $thisstaff->canEditSettlement()))
    die('Access Denied');

$errors = [];

function __account_update_actual_settlement(&$errors) {
    global $thisstaff;

    if (!($tour_id = isset($_REQUEST['id']) && (int)trim($_REQUEST['id']) ? (int)trim($_REQUEST['id']) : null)
        || !($tour = TourNew::lookup($tour_id))) {
        $errors['tour'] = 'Invalid tour';
    }

    $rate = $income = 0;
    if (!isset($_POST['rate']) || empty(trim($_POST['rate'])) || !is_numeric( ($rate = trim(str_replace([','], '', $_POST['rate']))) )) {
        $errors['rate'] = 'Invalid rate, number required';
    }

    if (isset($_POST['income']) && !empty(trim($_POST['income'])) && !is_numeric( $income = trim(str_replace([','], '', $_POST['income'])) )) {
    }

    if ($errors) return false;

    $note = isset($_POST['note']) ? trim($_POST['note']) : '';

    try {
        db_start_transaction();
        $outcome = 0;

        $check_list_obj = SettlementActualDetail::getPagination([ 'tour_id' => $tour_id ], 0, 999, $total);
        $check_list_arr = [];
        foreach($check_list_obj as $_item) {
            $check_list_arr[(int)$_item['account_item_id']] = false;
        }

        if (isset($_REQUEST['item']) && is_array($_REQUEST['item'])) {
            $list_item = $_REQUEST['item'];
            $list_quantity = $_REQUEST['quantity'];
            $list_value = $_REQUEST['value'];

            foreach($list_item as $_index => $_item_id) {
                if (!(int)$_item_id) {
                    unset($list_quantity[$_index]);
                    unset($list_value[$_index]);
                    unset($list_item[$_index]);
                }
            }

            if (count($list_item) != count(array_unique($list_item)))
                throw new Exception('Duplicated items');

            foreach($list_item as $_index => $_item_id) {
                $list_value[ $_index ] = str_replace(',', '', $list_value[ $_index ]);
                $_data = [
                    'tour_id' => $tour_id,
                    'account_item_id' => (int)$_item_id,
                    'quantity' => (int)$list_quantity[$_index],
                    'value' => (float)$list_value[$_index],
                    'total' => (int)$list_quantity[$_index] * (float)$list_value[$_index],
                ];

                $outcome += $_data['total'];

                $this_item = SettlementActualDetail::lookup(
                    [
                        'tour_id' => $tour_id,
                        'account_item_id' => (int)$_item_id,
                    ]
                );
                if ($this_item) {
                    $_data['updated_at'] = new SqlFunction('NOW');
                    $_data['updated_by'] = $thisstaff->getId();
                    $this_item->setAll($_data);
                } else {
                    $_data['created_at'] = new SqlFunction('NOW');
                    $_data['created_by'] = $thisstaff->getId();
                    $this_item = SettlementActualDetail::create($_data);
                }

                $this_item->save();

                if (isset($check_list_arr[(int)$_item_id]))
                    $check_list_arr[(int)$_item_id] = true;
            } // END foreach
        } // END big IF

        foreach($check_list_arr as $_item_id => $_check) {
            if (!$_check) {
                $delete_item = SettlementActualDetail::lookup(
                    [
                        'tour_id' => $tour_id,
                        'account_item_id' => (int)$_item_id,
                    ]
                );

                if ($delete_item) $delete_item->delete();
            }
        }

        $settlment = SettlementGeneral::lookup($tour_id);
        $_data = [
            'tour_id' => $tour_id,
            'rate' => (int)$rate,
            'actual_income' => (float)$income,
            'actual_outcome' => (float)$outcome,
            'actual_profit' => (float)($income - $outcome),
            'note' => $note,
        ];
        if ($settlment) {
            $_data['updated_at'] = new SqlFunction('NOW');
            $_data['updated_by'] = $thisstaff->getId();
            $settlment->setAll($_data);
        } else {
            $_data['created_at'] = new SqlFunction('NOW');
            $_data['created_by'] = $thisstaff->getId();
            $settlment = SettlementGeneral::create($_data);
        }

        if ($settlment)
            $settlment->save();

        db_commit();
        return true;
    } catch (Exception $ex) {
        $errors[] = $ex->getMessage();
        db_rollback();

        return false;
    }
}

function __clone_spend_item(&$errors) {
    global $thisstaff;

    if (!($from = isset($_REQUEST['from']) && (int)trim($_REQUEST['from']) ? (int)trim($_REQUEST['from']) : null) || !($tour_from= TourNew::lookup($from))
        || !($to = isset($_REQUEST['to']) && (int)trim($_REQUEST['to']) ? (int)trim($_REQUEST['to']) : null) || !($tour_to= TourNew::lookup($to))
    ) {
        $errors['tour'] = 'Invalid tour';
    }

    try {
        db_start_transaction();

        $from_settlment = SettlementGeneral::lookup($from);
        $rate = 0;
        if ($from_settlment)
            $rate = $from_settlment->rate;

        SettlementActualDetail::cloneItems($from, $to);
        $settlment = SettlementGeneral::lookup($to);

        if (!$rate && $settlment->rate)
            $rate = $settlment->rate;

        $income = 0;
        if ($rate)
            $income = PaxTour::calcIncome($to)/$rate;
        $outcome = SettlementActualDetail::calc_outcome($to);
        $rate = str_replace(',', '', $rate);
        $outcome = str_replace(',', '', $outcome);
        $income = str_replace(',', '', $income);

        $_data = [
            'tour_id' => $to,
            'rate' => $rate,
            'actual_income' => floatval($income),
            'actual_outcome' => floatval($outcome),
            'actual_profit' => floatval($income-$outcome),
            'note' => '',
        ];

        if ($settlment) {
            $_data['updated_at'] = new SqlFunction('NOW');
            $_data['updated_by'] = $thisstaff->getId();
            $settlment->setAll($_data);
        } else {
            $_data['created_at'] = new SqlFunction('NOW');
            $_data['created_by'] = $thisstaff->getId();
            $settlment = SettlementGeneral::create($_data);
        }

        if ($settlment)
            $settlment->save();

        db_commit();
        return true;
    } catch (Exception $ex) {
        db_rollback();
        $errors['err'] = $ex->getCode().': '.$ex->getMessage();
    }

    return false;
}

$referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/actual_settlement.php') !== false
    ? $_SERVER['HTTP_REFERER'] : $cfg->getBaseUrl().'/scp/actual_settlement.php';

// POST
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST) {
    $errors = [];
    if (!$thisstaff->canEditSettlement()) {
        $referer = isset($_REQUEST['referer']) && strpos($_REQUEST['referer'], '/scp/actual_settlement.php') !== false
            ? $_REQUEST['referer'] : $cfg->getBaseUrl().'/scp/actual_settlement.php';
        header('Location: '. $referer);
        exit;
    }

    if (isset($_POST['action']) && 'edit' === $_POST['action'] && __account_update_actual_settlement($errors)) {
        FlashMsg::set('ticket_create', 'Save successfully');
        header('Location: '. $referer);
        exit;
    }

    if (isset($_POST['action']) && 'clone' === $_POST['action'] && __clone_spend_item($errors)) {
        FlashMsg::set('ticket_create', 'Clone successfully');
        header('Location: '. $referer);
        exit;
    }
}
// END POST

$nav->setTabActive('operator');

$_inc_page = 'settlement-actual-lists.inc.php';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$destination_list = TourDestination::getList($group_des);
$airlines_list = ListUtil::getList(AIRLINE_LIST);
$settlement = null;

switch ($action) {
    case 'edit':
        $_inc_page = 'settlement-actual-edit.inc.php';
        if (!($tour_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null)
            || !($tour = TourNew::lookup($tour_id))) {
            header('Location: '.$referer);
            exit;
        }

        $payment_list = PaxTour::getPayments($tour_id, 0);
        while($payment_list && ($payment = db_fetch_array($payment_list))) {
            Payment::updateTypeId($payment['ticket_id']);
        }

        $settlement = SettlementGeneral::lookup($tour_id);
        $spend_items_res = SettlementSpendItem::getAll();
        $spend_items = [];
        while($spend_items_res && ($row = db_fetch_array($spend_items_res))) {
            $spend_items[] = $row;
        }

        $total = 0;
        $spend_details = SettlementActualDetail::getPagination([ 'tour_id' => $tour_id ], 0, 999, $total);

        $_related_total = 0;
        $params = [
            'from' => date('Y-m-d', strtotime($tour->gateway_present_time)),
            'to' => date('Y-m-d', strtotime($tour->gateway_present_time)),
        ];
        $list = TourNew::getPagination($params, 0, $_related_total,99);

        break;
    case 'details':
        $_inc_page = 'settlement-actual-view.inc.php';
        if (!($tour_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null)
            || !($tour = TourNew::lookup($tour_id))) {
            header('Location: '.$referer);
            exit;
        }

        $payment_list = PaxTour::getPayments($tour_id, 0);
        while($payment_list && ($payment = db_fetch_array($payment_list))) {
            Payment::updateTypeId($payment['ticket_id']);
        }

        $settlement = SettlementGeneral::lookup($tour_id);
        $spend_items_res = SettlementSpendItem::getAll();
        $spend_items = [];
        while($spend_items_res && ($row = db_fetch_array($spend_items_res))) {
            $spend_items[] = $row;
        }

        $total = 0;
        $spend_details = SettlementActualDetail::getPagination([ 'tour_id' => $tour_id ], 0, 999, $total);

        $_related_total = 0;
        $params = [
            'from' => date('Y-m-d', strtotime($tour->gateway_present_time)),
            'to' => date('Y-m-d', strtotime($tour->gateway_present_time)),
        ];
        $list = TourNew::getPagination($params, 0, $_related_total, 99);

        break;
    default:
        $params = $_REQUEST;
        $params['join_account_general'] = 1;

        $total = 0;
        $page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
        $offset = ($page-1)*PAGE_LIMIT;
        $list = TourNew::getPagination($params, $offset, $total,PAGE_LIMIT);

        unset($params['p']);
        $pageNav = new Pagenate($total, $page, PAGE_LIMIT);
        $pageNav->setURL('actual_settlement.php', $params);
        $showing=$pageNav->showing().' '._N('tours', 'tours', $total);

        $view = isset($_REQUEST['view']) && !empty($_REQUEST['view']) ? trim($_REQUEST['view']) : null;
        $view_list = SettlementView::getAll();
        if ($view) {
            $view_obj = SettlementView::lookup($view);
            if (!$view_obj) {
                $view = null;
            }  else {
                $spend_items_res = SettlementSpendItem::getAll($view);
                $spend_items = [];
                while($spend_items_res && ($row = db_fetch_array($spend_items_res))) {
                    $spend_items[ $row['id'] ] = $row;
                }
            }
        }

        if ($offset > $total) {
            $page = ceil($offset/PAGE_LIMIT);
            header('Location: '.$cfg->getBaseUrl().'/scp/actual_settlement.php?page='.$page);
            exit;
        }
        break;
}

require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$_inc_page);
include(STAFFINC_DIR.'footer.inc.php');

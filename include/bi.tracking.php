<?php
namespace BI;
require_once(INCLUDE_DIR . 'class.orm.php');

class TrackingCookieUrlparamEventModel extends \VerySimpleModel {
    static $meta = [
        'table' => TRACKING_COOKIE_URLPARAM_EVENT_TABLE,
        'pk' => ['id'],
    ];
}

class TrackingCookieUrlparamEvent extends TrackingCookieUrlparamEventModel {

}

class TrackingCookieUrlparamModel extends \VerySimpleModel {
    static $meta = [
        'table' => TRACKING_COOKIE_URLPARAM_TABLE,
        'pk' => ['id'],
    ];
}

class TrackingCookieUrlparam extends TrackingCookieUrlparamModel {

}

class TrackingCookieModel extends \VerySimpleModel {
    static $meta = [
        'table' => TRACKING_COOKIE_TABLE,
        'pk' => ['id'],
    ];
}

class TrackingCookie extends TrackingCookieModel {

}


class TrackingUrlparamModel extends \VerySimpleModel {
    static $meta = [
        'table' => TRACKING_URLPARAM_TABLE,
        'pk' => ['id'],
    ];
}

class TrackingUrlparam extends TrackingUrlparamModel {
    static $param_name = '_tgt_';

    public static function generateCode($object_id, $object_type) {
        $rnd = \_String::generateRandomString(10);
        return hash('md5',$object_id.$object_type.microtime(1).$rnd);
    }

    public static function getCode($object_id, $object_type, &$id) {
        $obj = static::create(['object_id' => $object_id, 'object_type' => $object_type]);
        $id = $obj->id;
        return static::$param_name . '=' . $obj->code;
    }

    public static function getUTM($campaign, $medium, $source, $content, $term = null) {
        $params = "utm_source=$source&utm_medium=$medium&utm_campaign=$campaign&utm_content=$content";
        if ($term)
            $params .= "$params&utm_term=$term";
        return $params;
    }

    public static function create($data = false) {
        $obj = parent::create($data);
        $code = static::generateCode($data['object_id'], $data['object_type']);
        $time = date('Y-m-d H:i:s');
        $obj->setAll(
            [
                'time' => $time,
                'code' => $code,
            ]
        );

        $obj->save(1);
        return $obj;
    }
}


<?php
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-php-quickstart.json
define('SCOPES', implode(' ', array(
    Google_Service_Sheets::SPREADSHEETS
)));

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {

    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory(CREDENTIALS_AGENT_PATH);
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if(!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * @param $service Google_Service_Sheets
 * @param $spreadsheetId (GOOGLE_SHEET_ID_CALL_LOG)
 * @param $type: inbound | outbound
 */
function updateAgentList($service, $spreadsheetId) {
    echo date("Y-m-d H:i:s")." - Start update agent list  ------------------\r\n";

    $sql = sprintf(
        "SELECT username FROM %s WHERE dept_id=1 AND isactive=1 ORDER BY username "
        , STAFF_TABLE
    );
    echo $sql."\r\n";
    $res = db_query($sql);
    if ($res) {
        $values = [];

        while (($row = db_fetch_array($res))) {
            $values[] = [ $row['username'] ];
        }

        echo  date("Y-m-d H:i:s")." - Pushing data into the sheet\r\n";
        $range = 'Sheet1!$A$1';
        $body = new Google_Service_Sheets_ValueRange(array('values' => $values));
        $params = array('valueInputOption' => "USER_ENTERED");
        $result = $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);
        echo  date("Y-m-d H:i:s")." - DONE Pushing data into the sheet\r\n";
    }

    echo date("Y-m-d H:i:s")." - DONE update agent list ------------------\r\n";
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);
$spreadsheetId = GOOGLE_SHEET_AGENT_LIST;

updateAgentList($service, $spreadsheetId);

CREATE TABLE `ost_call_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `calluuid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direction` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `callernumber` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destinationnumber` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `answertime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `billduration` int(11) DEFAULT NULL,
  `totalduration` int(11) DEFAULT NULL,
  `disposition` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `destinationnumber` (`destinationnumber`),
  KEY `callernumber` (`callernumber`),
  KEY `starttime` (`starttime`),
  KEY `totalduration` (`totalduration`),
  KEY `disposition` (`disposition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
<?php
namespace Tugo;

class Config {
    protected static $instance;
    protected static $data;

    /**
     * @return mixed
     */
    public static function getInstance($path = null) {
        if (is_null(static::$instance))
            static::$instance = new static;

        if (!is_null($path) && is_string($path) && file_exists($path))
            static::$data = parse_ini_file($path) ?: [];

        if (!static::$data)
            static::$data = [];

        return static::$instance;
    }

    public function get($key) {
        if (isset(static::$data[$key]))
            return static::$data[$key];
        return null;
    }

    public function set($key) {
        foreach($key as $_index => $_value)
            static::$data[$_index] = $_value;
    }
}

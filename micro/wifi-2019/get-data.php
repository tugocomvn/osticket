<?php
include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/config.php';
include_once __DIR__.'/functions.php';

date_default_timezone_set('Asia/Ho_Chi_Minh');

try {
    $api_campaign_id = isset($argv[1]) ? $argv[1] : null;
    $from = isset($argv[2]) ? $argv[2] : date('d/m/Y', time()-3600*24);
    $to = isset($argv[3]) ? $argv[3] : date('d/m/Y', time()+3600*24);

    if (!empty($api_campaign_id))
        define('API_CAMPAIGN_ID', $api_campaign_id);
    else
        exit(date('Y-m-d H:i:s').": No campaign\r\n");

    $request_data = [
        'username'   => API_USERNAME,
        'password'   => API_PASSWORD,
        'campaignId' => API_CAMPAIGN_ID,
        'fromDate'   => $from,
        'toDate'     => $to,
    ];

    echo date('Y-m-d H:i:s').": Request - ".json_encode($request_data)."\r\n";

    Awing::requestData($request_data);
} catch (Exception $ex) {
    echo $ex->getMessage();
}

<?php
$prefix = [
    "0123" => "083",
    "0124" => "084",
    "0125" => "085",
    "0127" => "081",
    "0129" => "082",
    "0120" => "070",
    "0121" => "079",
    "0122" => "077",
    "0126" => "076",
    "0128" => "078",
    "0162" => "032",
    "0163" => "033",
    "0164" => "034",
    "0165" => "035",
    "0166" => "036",
    "0167" => "037",
    "0168" => "038",
    "0169" => "039",
    "0186" => "056",
    "0188" => "058",
    "0199" => "059",
];

$query = [
    "callernumber" => "UPDATE ost_call_log SET callernumber = CONCAT('%s', RIGHT(callernumber, length(callernumber)-locate('%s', callernumber)-1))
    where ((callernumber like '%s%%' and length(callernumber) = 11)
          or (concat('0', callernumber) like '%s%%' and length(callernumber) = 10)
          )
    ",
    "destinationnumber" => "UPDATE ost_call_log SET destinationnumber = CONCAT('%s', RIGHT(destinationnumber, length(destinationnumber)-locate('%s', destinationnumber)-1))
    where ((destinationnumber like '%s%%' and length(destinationnumber) = 11)
          or (concat('0', destinationnumber) like '%s%%' and length(destinationnumber) = 10)
          )
    ",
    "ost_sms_log" => "UPDATE ost_sms_log SET phone_number=CONCAT('%s', RIGHT(phone_number, 8))
        where phone_number like '%s%%' and length(phone_number) = 11
    ",

];

<?php
require_once __DIR__.'/tuthien.cnf.php';

// ini_set('display_errors', 0);
// error_reporting(~E_ALL);

// Make the connnection and then select the database.
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT);

/* check connection */
if (!$mysqli || $mysqli->connect_errno) {
    exit();
}

$mysqli->query("SET NAMES 'utf8'");
$mysqli->query("SET CHARACTER SET utf8");
$mysqli->query("USE " . DB_NAME);

$sql = "SELECT * FROM mkt_tuthien ORDER BY gather_date DESC";
$res = $mysqli->query($sql);
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#660066">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#660066">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="tugo.com.vn">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-tugo-negative-500-122x122.png" type="image/x-icon">
    <meta name="description" content="Tugo, Ước nguyện cho bé, Nuôi heo đất cùng Tugo giúp trẻ em thoát khỏi bệnh ROP">
    <title>ƯỚC NGUYỆN CHO BÉ | Nuôi heo đất cùng Tugo giúp trẻ em thoát khỏi bệnh ROP</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" href="css/simplelightbox.min.css" type="text/css">

    <style>
        @import url('https://fonts.googleapis.com/css?family=Baloo+Paaji|Barlow:600,700|Roboto:300');


        .display-2 {
            font-family: 'Baloo Paaji', cursive;
        }

        .display-1,
        .display-4,
        .display-7 {
            font-family: 'Roboto', sans-serif;
        }


        .display-5 {
            font-family: 'Barlow', sans-serif;
        }

        .btn-primary {
            background-color: #606 !important;
            border-color: #606 !important;
        }

        .btn-primary:hover,
        .mbr-section-btn a.btn:not(.btn-form):hover,
        .btn-primary:active,
        .btn-primary:focus,
        .mbr-section-btn a.btn:not(.btn-form):focus {
            background-color: rgba(100, 2, 102, 0.8) !important;
            border-color: #606 !important;
        }

        .paginate_button {
            margin: 0.25em;
            padding: 0.5em 1em;
            background-color: #606;
            border: 2px solid #606;
            color: white;
        }

        .paginate_button.disabled {
            cursor: not-allowed;
            background-color: rgba(100, 2, 102, 0.8);
        }

        .paginate_button:hover {
            background-color: rgba(100, 2, 102, 0.8);
            color: white;
        }

        .paginate_button:active {
            background-color: #606;
        }

        .paginate_button.current {
            background-color: rgba(100, 2, 102, 0.54);
            color: #606;
        }

        a {
            color: white;
        }

        #footer1-a {
            background: #606;
        }
        h5 {
            font-weight: bold;
        }

        .title h2 {
            margin-top: 2em;
        }

        .mobile_only {
            display: none;
        }

        @media only screen and (max-width: 600px) {
            .mobile_only {
                display: block;
            }
        }

        .mbr-section-title,
        .head-item {
            color: #606;
        }

        .gallery_container {
            max-width: 1170px;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        /* line 32, ../sass/demo.scss */
        .gallery_container .gallery a img {
            float: left;
            width: 20%;
            height: auto;
            border: 2px solid #fff;
            -webkit-transition: -webkit-transform .15s ease;
            -moz-transition: -moz-transform .15s ease;
            -o-transition: -o-transform .15s ease;
            -ms-transition: -ms-transform .15s ease;
            transition: transform .15s ease;
            position: relative;
        }
        /* line 46, ../sass/demo.scss */
        .gallery_container .gallery a:hover img {
            -webkit-transform: scale(1.05);
            -moz-transform: scale(1.05);
            -o-transform: scale(1.05);
            -ms-transform: scale(1.05);
            transform: scale(1.05);
            z-index: 5;
        }
        /* line 57, ../sass/demo.scss */
        .gallery_container .gallery a.big img {
            width: 40%;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-62783484-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-62783484-6');
    </script>
</head>
<body>
<section class="menu cid-riGukg09B9" once="menu" id="menu1-b">



    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://www.tugo.com.vn/?utm_source=tugo&utm_medium=landingpage&utm_campaign=tu_thien_2019&utm_term=click&utm_content=logo">
                         <img src="assets/images/logo-tugo-negative-500-122x122.png" alt="Tugo.com.vn" title="" style="height: 3.8rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="https://www.tugo.com.vn/?utm_source=tugo&utm_medium=landingpage&utm_campaign=tu_thien_2019&utm_term=click&utm_content=brandname">
                        Du Lịch Tugo</a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">


        </div>
    </nav>
</section>

<section class="mbr-section info3 cid-riGqPZMPXL" id="info3-9">



    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="media-container-column title col-12 col-md-10">
                <h2 class="align-left mbr-bold mbr-white pb-3 mbr-fonts-style display-2">ƯỚC NGUYỆN CHO BÉ</h2>
                <h3 class="mbr-section-subtitle align-left mbr-light mbr-white pb-3 mbr-fonts-style display-5">
                    Nuôi heo đất cùng Tugo giúp trẻ em thoát khỏi bệnh ROP
                </h3>
                <p class="mbr-text align-left mbr-white mbr-fonts-style display-7">
                    Từ tháng 1/2019, khi đăng ký tour đường dài Châu Âu, Anh Quốc, Mỹ, Canada, Úc, New Zealand là bạn đã đóng góp 100.000 đồng vào quỹ heo đất từ thiện.
                    Quỹ heo đất được dùng để nâng cấp thiết bị y tế và chi trả phí phẫu thuật cho những em nhỏ bị bệnh hiểm nghèo tại bệnh viện Nhi Đồng 2.
                </p>


                <div class="mbr-section-btn align-left py-4">
                    <a class="btn btn-primary display-4" href="#" onclick="ga('send', 'event', 'tu_thien_2019', 'click', 'list');window.scrollBy(0, 500);">DANH SÁCH</a>
                    <a class="btn btn-primary display-4" href="callto:1900555543" onClick="ga('send', 'event', 'tu_thien_2019', 'click', 'call');">ĐẶT TOUR</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-table cid-riGqyxeuqg" id="table1-7">


    <div class="container container-table">
        <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2">
            DANH SÁCH MẠNH THƯỜNG QUÂN
        </h2>
        <h3 class="mbr-section-subtitle mbr-fonts-style align-center pb-5 mbr-light display-5">
            Những đóng góp của quý khách đã tiếp thêm động lực sống cho những em nhỏ bị bệnh hiểm nghèo.
            Tugo trân trọng cảm ơn quý khách đã đồng hành nuôi "Heo Đất".
        </h3>
        <div class="table-wrapper">
            <div class="container">
                <div class="row search">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="dataTables_filter">
                            <label class="searchInfo mbr-fonts-style display-7">Tìm kiếm:</label>
                            <input class="form-control input-sm" disabled="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container scroll">
                <table class="table isSearch" cellspacing="0">
                    <thead>
                    <tr class="table-heads ">
                        <th class="head-item mbr-fonts-style display-7">KHÁCH HÀNG</th>
                        <th class="head-item mbr-fonts-style display-7">TOUR</th>
                        <th class="head-item mbr-fonts-style display-7">NGÀY KHỞI HÀNH</th>
                        <th class="head-item mbr-fonts-style display-7">SỐ TIỀN ĐÓNG GÓP</th>
                    </thead>

                    <tbody>
                    <?php $count = 0; ?>
                    <?php while($res && ($row = $res->fetch_assoc())): $count++; ?>
                        <tr>
                            <?php
//                            $name = $row['full_name'];
//                            $name = explode(' ', $row['full_name']);
//                            $name = $name[0].' ** '.$name[ count($name)-1 ];
                            ?>
                            <td class="body-item mbr-fonts-style display-7"><?php echo $row['full_name'] ?></td>
                            <td class="body-item mbr-fonts-style display-7"><?php echo $row['tour_name'] ?></td>
                            <td class="body-item mbr-fonts-style display-7"><?php echo date('d/m/Y', strtotime($row['gather_date'])) ?></td>
                            <td class="body-item mbr-fonts-style display-7">100.000đ</td>
                        </tr>
                    <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
            <div class="container table-info-container">
                <div class="row info">
                    <div class="col-md-6">
                        <div class="dataTables_info mbr-fonts-style display-7">
                        </div>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="media-container-column title col-12 col-md-10">
                        <div class=" align-center py-4">
                            <h5>Tính đến ngày <?php echo date('d/m/Y') ?>, tổng số tiền trong quỹ Heo Đất là
                                <strong>
                                    <?php
                                    echo number_format($count*100000, 0, ',', '.').'đ';
                                    ?>
                                </strong></h5>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2">
                HÌNH ẢNH THỰC TẾ
            </h2>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="title col-12 col-md-10">
                        <div class="gallery_container">
                            <div class="gallery">
                                <a href="em/1.jpg" class=""><img src="em/1.jpg" alt="" title="" /></a>
                                <a href="em/2.jpg"><img src="em/2.jpg" alt="" title=""/></a>
                                <a href="em/3.jpg" class="big"><img src="em/3.jpg" alt="" title=""/></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row justify-content-center">
                    <div class="media-container-column title col-12 col-md-10">
                        <div class="mbr-section-btn align-left py-4">
                            <a class="btn btn-primary display-4 mobile_only" href="callto:1900555543" onClick="ga('send', 'event', 'tu_thien_2019', 'click', 'call_footer_button');">ĐẶT TOUR</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<a name="info"></a>
<section class="cid-riGufB6pw8" id="footer1-a">
    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="https://www.tugo.com.vn/?utm_source=tugo&utm_medium=landingpage&utm_campaign=tu_thien_2019&utm_term=click&utm_content=footer_logo">
                        <img src="assets/images/logo-tugo-negative-500-122x122.png" alt="Tugo.com.vn">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-5 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Văn Phòng Tugo
                </h5>
                <p class="mbr-text">
                    118 Lê Thánh Tôn, P. Bến Thành, Quận 1, TP.HCM
                    <br>27 Thủ Khoa Huân, P. Bến Thành, Quận 1, TP.HCM
                    <br>123 Lý Tự Trọng, P. Bến Thành, Quận 1, TP.HCM
                    <br>259 Lê Thánh Tôn, P. Bến Thành, Quận 1, TP.HCM
                </p>
            </div>
            <div class="col-12 col-md-4 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Đặt Tour và Hỗ Trợ Khách Hàng
                </h5>
                <p class="mbr-text">
                    Tổng đài: <a href="callto:1900555543" onClick="ga('send', 'event', 'tu_thien_2019', 'click', 'call_footer_text');">1900555543</a>
                    <br>Email: <a href="mailto:support@tugo.com.vn" onClick="ga('send', 'event', 'tu_thien_2019', 'click', 'send_email');">support@tugo.com.vn</a>
                    <br>Website: <a href="https://www.tugo.com.vn/?utm_source=tugo&utm_medium=landingpage&utm_campaign=tu_thien_2019&utm_term=click&utm_content=footer">tugo.com.vn</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2019 Tugo - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/datatables/jquery.data-tables.min.js?v=1"></script>
<script src="assets/theme/js/script.js"></script>
<script type="text/javascript" src="js/simple-lightbox.min.js"></script>
<script>
    $(function(){
        var $gallery = $('.gallery_container a').simpleLightbox();

    });
</script>

</body>
</html>

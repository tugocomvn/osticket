<?php
require('staff.inc.php');
include_once INCLUDE_DIR.'class.booking.php';
include_once INCLUDE_DIR.'class.tour-destination.php';

if (!isset($_REQUEST['from']) || !$_REQUEST['from'])
    $_REQUEST['from'] = date('Y-m-d', time()-24*3600*10);

if (!isset($_REQUEST['to']) || !$_REQUEST['to'])
    $_REQUEST['to'] = date('Y-m-d', time()+24*3600*60);

$nav->setTabActive('finance');
$ost->addExtraHeader('<meta name="tip-namespace" content="finance.expenses_forecast" />',
                     "$('#content').data('tipNamespace', 'finance.expenses_forecast');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.'expenses-forecast.inc.php');

$page='report/expenses-forecast';
$forecast = new BookingExpenseForecast(
    $_REQUEST
);
$forecast->run();
$forecast->render($page);

include(STAFFINC_DIR.'footer.inc.php');
?>
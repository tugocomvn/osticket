<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class FlightModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TR_TABLE,
        'pk' => ['id'],
    );
}
class Flight extends FlightModel {
    public static function getNameAirline($airline_id) {
        if(empty($airline_id)) return null;
        $sql = "SELECT DISTINCT id, `value` as airline_name FROM " . LIST_ITEM_TABLE . " 
            li WHERE list_id = ".AIRLINE_LIST." AND id=".db_input($airline_id);
        $res = db_query($sql);
        if(!$res) return null;
        $row = db_fetch_array($res);
        return $row['airline_name'];
    }
}

class FlightTicketFlightTR extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_FLIGHT_TR_TABLE,
        'pk' => ['flight_id', 'flight_ticket_flight'],
    );
}

class FlightLogTR extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TR_LOG_TABLE,
        'pk' => ['id'],
    );
}

class FlightTicketLogTR extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_TR_LOG_TABLE,
        'pk' => ['id'],
    );
}

class FlightTicketModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_TR_TABLE,
        'pk' => ['id'],
    );
}

class FlightTicket extends FlightTicketModel {
    public static function getPagination(array $params, $offset, &$total, $limit = PAGE_LIMIT)
    {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY f.created_at DESC";
        $order = "";
        $where = "  ";
        $total = 0;

        if (isset($params['flight_ticket_code']) && !empty($params['flight_ticket_code']))
            $where .= " AND ft.flight_ticket_code LIKE " . db_input('%' . ($params['flight_ticket_code']) . '%') . " ";

        if(isset($params['airline']) && !empty($params['airline']))
            $where .= " AND f.airline_id = ".db_input($params['airline']);

        if(isset($params['flight_code']) && !empty($params['flight_code']))
            $where .= " AND f.flight_code LIKE " . db_input('%' . ($params['flight_code']) . '%') . " ";

        if(isset($params['departure_at']) && !empty($params['departure_at']))
            $where .=" AND DATE(`f.departure_at`) = date(".(db_input($params['departure_at'])).")";

        if(isset($params['arrival_at']) && !empty($params['arrival_at']))
            $where .=" AND DATE(`f.arrival_at`) = date(".(db_input($params['arrival_at'])).")";

        if(isset($params['total_quantity']) && !empty($params['total_quantity']))
            $where .= " AND ft.total_quantity = ".db_input($params['total_quantity']);

        $sql = "SELECT  ft.flight_ticket_code, 
                        ft.total_quantity, f.airline_id,
                        f.flight_code, f.airport_code_from, f.airport_code_to, f.departure_at, f.arrival_at
                FROM " .static::$meta['table']." ft 
                    LEFT JOIN ".FLIGHT_TICKET_FLIGHT_TR_TABLE." ftf ON ftf.flight_ticket_id = ft.id  
                    LEFT JOIN ".FLIGHT_TR_TABLE." f ON f.id = ftf.flight_id  
                    WHERE 1 $where $order";
        $sql_count = "SELECT COUNT(*) as total FROM ($sql) as A";
        $res_total = db_query($sql_count);
        if ($res_total) {
            $total_row = db_fetch_array($res_total);
            if ($total_row && isset($total_row['total'])) {
                $total = $total_row['total'];
            }
        }
        $res = db_query("$sql $limit");
        return $res;
    }

    public static function getAllAirline()
    {
         $sql = "SELECT DISTINCT id, `value` as airline_name FROM " . LIST_ITEM_TABLE . " li WHERE list_id = ".AIRLINE_LIST."";
        return db_query($sql);
    }
}
//OP
class FlightTicketOPModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_TABLE,
        'pk' => ['id'],
    );
}

class FlightTicketOP extends FlightTicketOPModel{
    public static function searchTourFromCode($code){
        $sql = 'select ticket_tour.tour_id from '.FLIGHT_TICKET_TABLE.' as ticket
                join '.FLIGHT_TICKET_TOUR_TABLE.' as ticket_tour on ticket.id = ticket_tour.flight_ticket_id '
            .' where ticket.flight_ticket_code like  '.db_input('%'.$code.'%');

        $result = db_query($sql);
        $tour_id = [];
        while ($result && ($row = db_fetch_array($result))){
            $tour_id[] = $row['tour_id'];
        }
        return $tour_id;
    }
}

class FlightTicketLogOPModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_LOG_TABLE,
        'pk' => ['id'],
    );
}
class FlightTicketLogOP extends FlightTicketLogOPModel {

}

class FlightOPModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TABLE,
        'pk' => ['id'],
    );
}
class FlightOP extends FlightOPModel{
    public static function searchTourFromCode($code){
        $sql = 'select ticket_tour.tour_id from '.FLIGHT_TABLE.' as flight 
                join '.FLIGHT_TICKET_FLIGHT_OP_TABLE.' as flight_ticket_flight on flight_ticket_flight.flight_id = flight.id 
                join '.FLIGHT_TICKET_TOUR_TABLE.' as ticket_tour on flight_ticket_flight.flight_ticket_id = ticket_tour.flight_ticket_id 
                where flight.flight_code like '.db_input('%'.$code.'%');

        $result = db_query($sql);
        $tour_id = [];
        while ($result && ($row = db_fetch_array($result))){
            $tour_id[] = $row['tour_id'];
        }
        return $tour_id;
    }

    public static function searchFlightCodeFromTour($tour_id){
        $sql = 'select DISTINCT flight.flight_code 
                from '.FLIGHT_TICKET_TOUR_TABLE.' as ticket_tour 
                join '.FLIGHT_TICKET_FLIGHT_OP_TABLE.' as flight_ticket_flight on flight_ticket_flight.flight_ticket_id = ticket_tour.flight_ticket_id 
                join '.FLIGHT_TABLE. ' as flight on flight_ticket_flight.flight_id = flight.id 
                where ticket_tour.tour_id = '.(int)$tour_id.' and flight.flight_code is not null 
                order by flight_ticket_flight.flight_ticket_id,flight.departure_at, flight.arrival_at';

        $result = db_query($sql);
        $flight_code = [];
        while ($result && ($row = db_fetch_array($result))){
            $flight_code[] = $row['flight_code'];
        }
        return $flight_code;
    }


    public static function searchAirportCodeFromTour($tour_id){
        $sql = 'select DISTINCT flight.airport_code_from, flight.airport_code_to, flight.departure_at, flight.arrival_at 
                from '.FLIGHT_TICKET_TOUR_TABLE.' as ticket_tour 
                join '.FLIGHT_TICKET_FLIGHT_OP_TABLE.' as flight_ticket_flight on flight_ticket_flight.flight_ticket_id = ticket_tour.flight_ticket_id 
                join '.FLIGHT_TABLE. ' as flight on flight_ticket_flight.flight_id = flight.id 
                where ticket_tour.tour_id = '.(int)$tour_id.'
                order by flight_ticket_flight.flight_ticket_id,flight.departure_at, flight.arrival_at';

        $result = db_query($sql);
        $airport_code = [];

        while ($result && ($row = db_fetch_array($result))){
            $airport_code[] = $row;
        }
        return $airport_code;
    }

}
class FlightLogOP extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_LOG_TABLE,
        'pk' => ['id'],
    );
}

class FlightTicketFlightOP extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_FLIGHT_OP_TABLE,
        'pk' => ['flight_id', 'flight_ticket_flight'],
    );
}
class FLightTicketFlight extends FlightTicketFlightOP {
    public static function getFLightId($arr_flight_ticket_id)
    {
        $sql = "SELECT * FROM " .static::$meta['table']." ftf WHERE ftf.flight_ticket_id IN (".implode(',', db_input($arr_flight_ticket_id)).")";
        $result = db_query($sql);
        $flight_id = [];
        while ($result && ($row = db_fetch_array($result))){
            $flight_id[] = $row['flight_id'];
        }
        return $flight_id;
    }

    public static function getPagination($offset, &$total, $limit = PAGE_LIMIT)
    {
        $limit = " LIMIT $limit OFFSET $offset ";
        $where = "  ";
        $total = 0;
        $sql = "SELECT *
                FROM (SELECT ft.total_quantity, ft.flight_ticket_code, f.departure_at, f.arrival_at FROM ".FLIGHT_TABLE." f 
                        LEFT JOIN ".FLIGHT_TICKET_FLIGHT_OP_TABLE." ftf ON f.id = ftf.flight_id
                        LEFT JOIN ".FLIGHT_TICKET_TABLE." ft ON ft.id = ftf.flight_ticket_id 
                        WHERE NOT EXISTS ( 
                                SELECT fnd_ft.total_quantity, fnd_ft.flight_ticket_code, fnd_f.departure_at, fnd_f.arrival_at FROM ".FLIGHT_TR_TABLE." fnd_f
                                LEFT JOIN ".FLIGHT_TICKET_FLIGHT_TR_TABLE." fnd_ftf ON fnd_f.id = fnd_ftf.flight_id
                                LEFT JOIN ".FLIGHT_TICKET_TR_TABLE." fnd_ft ON fnd_ft.id = fnd_ftf.flight_ticket_id
                                WHERE  ft.total_quantity = fnd_ft.total_quantity AND ft.flight_ticket_code = fnd_ft.flight_ticket_code 
                                AND f.departure_at = fnd_f.departure_at
                                AND f.arrival_at = fnd_f.arrival_at
                            )
                        UNION
                            SELECT fnd_ft.total_quantity, fnd_ft.flight_ticket_code, fnd_f.departure_at, fnd_f.arrival_at FROM ".FLIGHT_TR_TABLE." fnd_f
                            LEFT JOIN ".FLIGHT_TICKET_FLIGHT_TR_TABLE." fnd_ftf ON fnd_f.id = fnd_ftf.flight_id
                            LEFT JOIN ".FLIGHT_TICKET_TR_TABLE." fnd_ft ON fnd_ft.id = fnd_ftf.flight_ticket_id
                            WHERE NOT EXISTS (
                                SELECT ft.total_quantity, ft.flight_ticket_code, f.departure_at, f.arrival_at  FROM ".FLIGHT_TABLE." f 
                                LEFT JOIN ".FLIGHT_TICKET_FLIGHT_OP_TABLE." ftf ON f.id = ftf.flight_id
                                LEFT JOIN ".FLIGHT_TICKET_TABLE." ft ON ft.id = ftf.flight_ticket_id 
                                WHERE  ft.total_quantity = fnd_ft.total_quantity AND ft.flight_ticket_code = fnd_ft.flight_ticket_code 
                                AND f.departure_at = fnd_f.departure_at
                                AND f.arrival_at = fnd_f.arrival_at
                            )
                    ) AS T $where ";
        $sql_count = "SELECT COUNT(*) as total FROM ($sql) as A";
        $res_total = db_query($sql_count);
        if ($res_total) {
            $total_row = db_fetch_array($res_total);
            if ($total_row && isset($total_row['total'])) {
                $total = $total_row['total'];
            }
        }
        $res = db_query("$sql $limit");
        return $res;
    }
}
class TourLogOP extends VerySimpleModel {
    static $meta = array(
        'table' => TOUR_LOG_TABLE,
        'pk' => ['id'],
    );
}
class FlightTicketTourOPModel extends VerySimpleModel {
    static $meta = array(
        'table' => FLIGHT_TICKET_TOUR_TABLE,
        'pk' => ['id'],
    );
}
class FlightTicketTourOP extends FlightTicketTourOPModel {
    public static function getFlightTicketFLight($tour_id){
        $sql = "SELECT * FROM ". self::$meta['table']." WHERE tour_id = ".$tour_id;
        return db_query($sql);
    }

    public static function deleteItem($id)
    {
        $flight_ticket_tour = self::lookup(['flight_ticket_id' => $id]);
        if($flight_ticket_tour) {
            $flight_ticket = FlightTicketOP::lookup($id);
            if($flight_ticket) {
                $flight_ticket_flight = FlightTicketFlightOP::lookup(['flight_ticket_id' => $flight_ticket->id]);
                if($flight_ticket_flight) {
                    $flight = FlightOP::lookup($flight_ticket_flight->flight_id);
                    $flight->delete();
                }
                self::deleteFlightTicketId($flight_ticket_flight->flight_ticket_id);
                self::deleteFlightId($flight_ticket_flight->flight_id);
            }
            $flight_ticket->delete();
        }
        $flight_ticket_tour->delete();
    }

    public static function deleteFlightTicketId($flight_ticket_id)
    {
        $sql = 'DELETE FROM '.FLIGHT_TICKET_FLIGHT_OP_TABLE.' WHERE flight_ticket_id='.db_input($flight_ticket_id);
        return db_query($sql);
    }

    public static function deleteFlightId($flight_id)
    {
        $sql = 'DELETE FROM '.FLIGHT_TICKET_FLIGHT_OP_TABLE.' WHERE flight_id='.db_input($flight_id);
        return db_query($sql);
    }
}

?>

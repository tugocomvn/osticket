<?php
$all_call_by_day = TicketAnalytics::callAnalytics($_REQUEST['from'], $_REQUEST['to'], 'all_call_by_day', $_REQUEST);
$all_call_by_day_arr = [];
$dates_source = [];
while($all_call_by_day && ($row = db_fetch_array($all_call_by_day))) {
    $type = _String::json_decode($row['type']);
    if (!isset($all_call_by_day_arr[ $type ]))
        $all_call_by_day_arr[ $type ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $dates_source[] = $row['week'].'-'.$row['year'];
            $all_call_by_day_arr[ $type ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $dates_source[] = $row['month'].'-'.$row['year'];
            $all_call_by_day_arr[ $type ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $all_call_by_day_arr[ $type ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}

$dates_source = array_filter(array_unique($dates_source));

foreach ($all_call_by_day_arr as $_type => $_date_data) {
    foreach ($dates_source as $_date) {
        if (!isset($all_call_by_day_arr[ $_type ][ $_date ]))
            $all_call_by_day_arr[ $_type ][ $_date ] = 0;
    }

    uksort($all_call_by_day_arr[ $_type ], "___date_compare");
}
?>
<td colspan="2">
    <h2>All calls by Day</h2>
    <canvas id="all_call" width="1000" height="300"></canvas>
</td>
<script>
    var all_call_elm = document.getElementById("all_call").getContext('2d');
    <?php $i = 0; ?>
    var colors = poolColors(<?php echo count($all_call_by_day_arr) ?>);
    var all_call = new Chart(all_call_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($all_call_by_day_arr as $_type => $_date): ?>
                {
                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                    backgroundColor:  colors[<?php echo $i++ ?>],
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>

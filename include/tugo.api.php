<?php
namespace Tugo;

include_once INCLUDE_DIR.'../vendor/autoload.php';
include_once INCLUDE_DIR.'class.sms.php';

use FacebookAds\Exception\Exception;
use \Firebase\JWT\JWT;

class Token {
    public static function getToken($data) {
        if ($token = static::getTokenFromHeaders())
            return $token;

        return static::getTokenFromRequest($data);
    }

    public static function getTokenFromRequest($data) {
        if (!isset($data['token']) || empty(trim($data['token'])))
            return null;

        return trim($data['token']);
    }

    public static function getTokenFromHeaders() {
        $head = $all_head = null;
        if (isset($_SERVER["HTTP_AUTHORIZATION"]) && !empty(trim($_SERVER["HTTP_AUTHORIZATION"])))
            $head = $_SERVER["HTTP_AUTHORIZATION"];
        elseif ( ($all_head = getallheaders()) ) {
            foreach ($all_head as $_key => $_val) {
                if (strtolower($_key) === 'authorization')
                    $head = trim($_val);
            }
        } else {
            return null;
        }

        list($type, $data) = explode(" ", $head, 2);
        if (strcasecmp($type, "Bearer") != 0)
            return null;

        return trim($data);
    }

    public static function getNewToken($user, $device_id) {
        $token = JWT::encode(
            [
                'exp' => time() + config('token_expiration'),
                'iss' => 'Tugo',
                'nbf' => time() - 60,
                'iat' => time(),
                'customer_name' => $user['customer_name'],
                'phone_number' => $user['phone_number'],
                'customer_number' => $user['customer_number'],
                'uuid' => $user['uuid'],
                'device_id' => trim($device_id),
            ],
            config('signing_secret'),
            config('signing_algorithm')
        );

        return $token;
    }

    public static function getNewRefreshToken($user_uuid, $device_id) {
        $user_uuid = trim($user_uuid);
        if (empty($user_uuid)) throw new Exception('Thiếu mã khách', 1601);

        $device_id = trim($device_id);
        if (empty($device_id)) throw new Exception('Thiếu mã thiết bị', 1602);

        $token = md5(uniqid('Tugo_App', true) . $user_uuid . $device_id . microtime(true));

        if (!$token) return false;

        $user_uuid = db_input($user_uuid);
        $device_id = db_input($device_id);
        $sql_token = db_input($token);
        $sql = "REPLACE INTO api_refresh_token SET 
            user_uuid = $user_uuid, 
            device_id = $device_id, 
            refresh_token = $sql_token,
            issued_at = NOW(),
            expired_at = DATE_ADD(NOW(), INTERVAL 30 DAY)
        ";

        db_query($sql);

        return $token;
    }

    public static function tokenValidate($token) {
        $decoded = JWT::decode($token, config('signing_secret'), array(config('signing_algorithm')));
        return $decoded;
    }

    public static function verifyRefreshToken($user_uuid, $device_id, $refresh_token) {
        $user_uuid = trim($user_uuid);
        if (empty($user_uuid)) throw new Exception('Thiếu mã khách', 1601);

        $device_id = trim($device_id);
        if (empty($device_id)) throw new Exception('Thiếu mã thiết bị', 1602);

        $refresh_token = trim($refresh_token);
        if (empty($device_id)) throw new Exception('Thiếu mã refresh token', 1603);

        return static::_verify_refresh_token($user_uuid, $device_id, $refresh_token);
    }

    public static function verifyToken($data) {
        $token = static::getToken($data);
        if (!$token) throw new Exception('Chưa có Token', 403);

        $payload = static::tokenValidate($token);
        if (! $payload ) throw new Exception('Token không hợp lệ', 403);
        return $payload;
    }

    private static function _verify_refresh_token($user_uuid, $device_id, $refresh_token) {
        $user_uuid = trim($user_uuid);
        $user_uuid = db_input($user_uuid);

        $device_id = trim($device_id);
        $device_id = db_input($device_id);

        $refresh_token = trim($refresh_token);
        $refresh_token = db_input($refresh_token);

        $sql = "SELECT COUNT(*) as total FROM api_refresh_token
            WHERE user_uuid LIKE $user_uuid 
            AND device_id LIKE $device_id 
            AND refresh_token LIKE $refresh_token 
            AND issued_at < NOW()
            AND expired_at > NOW()
            LIMIT 1
        ";
        $res = db_query($sql);
        if (!$res) return false;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['total']) || $row['total'] != 1) return false;
        return true;
    }

    public static function removeRefreshToken($user_uuid, $device_id) {
        $user_uuid = trim($user_uuid);
        $user_uuid = db_input($user_uuid);

        $device_id = trim($device_id);
        $device_id = db_input($device_id);
        $sql = "DELETE FROM api_refresh_token
            WHERE user_uuid LIKE $user_uuid 
            AND device_id LIKE $device_id
        ";

        return db_query($sql);
    }

    public static function decode($jwt, $key, array $allowed_algs = array()) {
        $jwt = static::getToken($jwt);
        if (!$jwt) throw new Exception('Chưa có Token', 403);

        if (empty($key)) {
            throw new Exception('Key may not be empty');
        }
        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
            throw new Exception('Wrong number of segments');
        }
        list($headb64, $bodyb64, $cryptob64) = $tks;
        if (null === ($header = JWT::jsonDecode(JWT::urlsafeB64Decode($headb64)))) {
            throw new Exception('Invalid header encoding');
        }
        if (null === $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64))) {
            throw new Exception('Invalid claims encoding');
        }

        return $payload;
    }
}

class App {
    public static function sendOTP($phonenumber) {
        $phonenumber = trim($phonenumber);
        if (empty($phonenumber))
            throw new Exception('Chưa có số điện thoại', 1700);

        if ('0000000009' == $phonenumber) return true;

        if (!\_String::isMobileNumber($phonenumber))
            throw new Exception('Số điện thoại không đúng', 1701);

        $phonenumber = \_String::formatPhoneNumber($phonenumber);

        if (!static::_check_send_time($phonenumber))
            throw new Exception('Số đang spam', 1702);

        $otp = '';
        for ($i = 0; $i < 4; $i++) {
            $otp .= strval(rand(1, 9));
        }
        if (!static::_log_OTP($phonenumber, $otp))
            throw new Exception('Lỗi lưu OTP', 1703);

        $error = [];
        $message = 'Tugo chao ban, ma OTP dang nhap Tugo Reward App cua ban la: '.$otp;
        return \_SMS::send($phonenumber, $message, $error, 'OTP');
    }

    private static function _log_OTP($phonenumber, $otp) {
        $phonenumber = \_String::formatPhoneNumber($phonenumber);
        $phonenumber = db_input($phonenumber);
        $otp = db_input(trim($otp));
        $sql = "INSERT INTO api_otp SET phone_number = $phonenumber, otp=$otp, expires=DATE_ADD(NOW(), INTERVAL ".OTP_EXPIRED." HOUR)";
        return db_query($sql);
    }

    public static function verifyOTP($phonenumber, $otp) {
        $phonenumber = \_String::formatPhoneNumber($phonenumber);
        $_phonenumber = db_input($phonenumber);
        $_otp = db_input(trim($otp));

        static::offOTP($phonenumber, $otp); //delete OTP when expired

        //check otp has used and active
        $sql = "SELECT COUNT(*) as otp FROM api_otp 
            WHERE phone_number LIKE $_phonenumber 
              AND otp LIKE 'verify' 
              AND expires > NOW()
              LIMIT 1";
        $res = db_query($sql);
        if($res)
        {
            $row = db_fetch_array($res);
            if($row['otp']) return true;
        }

        $sql = "SELECT COUNT(*) as otp FROM api_otp 
            WHERE phone_number LIKE $_phonenumber 
              AND otp LIKE $_otp 
              AND expires > NOW()
              LIMIT 1";
        
        $res = db_query($sql);
        if (!$res) return false;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['otp']) || $row['otp'] != 1) return false;

        //sign otp active
        $sql = "UPDATE api_otp SET otp = ".db_input(OTP_VERIFY)." WHERE phone_number LIKE $_phonenumber 
              AND otp LIKE $_otp 
              AND expires > NOW() ";
        db_query($sql);

        return true;
    }

    public static function offOTP($phonenumber, $otp) {
        $_phonenumber = db_input(trim($phonenumber));
        $_otp = db_input(trim($otp));

        $sql = "DELETE FROM api_otp WHERE phone_number LIKE $_phonenumber AND (otp LIKE $_otp OR otp LIKE ".db_input(OTP_VERIFY)." ) AND expires <= NOW() ";
        db_query($sql);
    }

    private static function _check_send_time($phonenumber) {
        if (!$phonenumber) return null;
        $phonenumber = \_String::formatPhoneNumber($phonenumber);
        if (empty($phonenumber)) return null;

        $phonenumber = db_input($phonenumber);
        $type = db_input('OTP');

        $sql = "SELECT ((SELECT COUNT(*) <= 3
             FROM ost_sms_log
             WHERE phone_number LIKE $phonenumber
                   AND type LIKE $type
                   AND send_time >= DATE_SUB(NOW(), INTERVAL 5 MINUTE)
            ) +
            (SELECT COUNT(*) <= 5
             FROM ost_sms_log
             WHERE phone_number LIKE $phonenumber
                   AND type LIKE $type
                   AND send_time >= DATE_SUB(NOW(), INTERVAL 1 HOUR)
            ) +
            (SELECT COUNT(*) <= 6
             FROM ost_sms_log
             WHERE phone_number LIKE $phonenumber
                   AND type LIKE $type
                   AND send_time >= DATE_SUB(NOW(), INTERVAL 1 DAY)
            ) +
            (SELECT COUNT(*) <= 10
             FROM ost_sms_log
             WHERE phone_number LIKE $phonenumber
                   AND type LIKE $type
                   AND send_time >= DATE_SUB(NOW(), INTERVAL 7 DAY)
            ) = 4) AS checktime
        ";
        $res = db_query($sql);
        if (!$res) return false;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['checktime']) || $row['checktime'] != 1) return false;
        return true;
    }
}

class Api {
    public static function checkRoute() {

    }
}

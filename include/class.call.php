<?php

require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'class.sms.php');
require_once(INCLUDE_DIR . 'class.staff.php');
require_once(INCLUDE_DIR . 'tugo.notification.php');

class CallLog extends VerySimpleModel {
    static $meta = array(
        'table' => CALL_LOG_TABLE,
        'pk' => array('id'),
        'ordering' => array('add_time')
    );
    function getId() {
        return  $this->id;
    }

    function getNumber() {
        return $this->number;
    }

    function getOwnerId() {
        return $this->ht['user_id'];
    }

    function getOwner() {

        if (!isset($this->owner)
            && ($u=User::lookup($this->getOwnerId())))
            $this->owner = new TicketOwner(new EndUser($u), $this);

        return $this->owner;
    }
    function getStaffId() {
        return $this->ht['staff_id'];
    }

    function getStaff() {

        if(!$this->staff && $this->getStaffId())
            $this->staff= Staff::lookup($this->getStaffId());

        return $this->staff;
    }
    function getUserId() {
        return $this->getOwnerId();
    }

    function getUser() {

        if(!isset($this->user) && $this->getOwner())
            $this->user = new EndUser($this->getOwner());

        return $this->user;
    }
    function getAuthToken() {
        # XXX: Support variable email address (for CCs)
        return md5($this->getId() . strtolower($this->getEmail()) . SECRET_SALT);
    }

    function getName(){
        if ($o = $this->getOwner())
            return $o->getName();
        return null;
    }

    function getSubject() {
        return (string) $this->_answers['subject'];
    }
    function getPhoneNumber() {
        if (($owner = $this->getOwner()))
            return (string)$owner->getPhoneNumber();
        else
            return '';
    }
    function save() {
        global $cfg;

        try {
            if (parent::save())
                echo '1';
            else
                echo '3';
        } catch (Exception $ex) {
            if ($ex->getCode() == MYSQL_ERROR_DUPLICATE_CODE) {
                echo '1';
                return true;
            } else {
                echo '0';
            }

        } finally {

        }

        $config = $cfg->getSmsAutoSettings();
        if ($this->canSendSms($config)) {
            $this->sendSms($config);
        }

        try {
            if (strtolower('NO ANSWER') == strtolower($this->disposition)
                && $config['send_email_for_missed_calls']
                && $config['email_send_email_for_missed_calls']
                && 'inbound' === strtolower($this->direction)
            ) $this->sendEmailMissedCall($config['email_send_email_for_missed_calls']);
        } catch (Exception $ex) { }

        try {
            $this->alertAgent();
        } catch (Exception $ex) { }

        return true;
    }

    function sendSms($config) {
        // gửi sms cho khách khi khách gọi vào không ai nghe máy
        if (strtolower('NO ANSWER') == strtolower($this->disposition)
            && $config['auto_send_no_answer']
            && $config['auto_send_no_answer_content']
            && 'inbound' === strtolower($this->direction)
        ) $this->sendMissCallSms($config['auto_send_no_answer_content']);

        // gửi sms cho khách khi khách gọi vào và nói chuyện hơn 50s
        if (strtolower('ANSWERED') == strtolower($this->disposition)
            && $config['auto_send_call']
            && $config['auto_send_call_content']
            && 'inbound' === strtolower($this->direction)
        ) $this->sendAgentInfoSms($config['auto_send_call_content']);

        // gửi sms thông báo cho khách khi nhân viên gọi khách không được
        if (in_array(strtolower($this->disposition), ['failed', 'no answer', 'busy'])
            && $config['auto_send_after_failed_call']
            && trim($config['auto_send_call_after_failed_content'])
            && 'outbound' === strtolower($this->direction)
            && ( self::countMissedCallToday() < 500 - round(  ( (time()- TODAY_SMS_500)/(3600*24*10) ) ) )
            // cứ 10 ngày trôi qua thì limit giảm đi 1 SMS
        ) $this->sendFailedCallNotifySms($config['auto_send_call_after_failed_content']);
    }

    function countMissedCallToday() {
        $sql = "SELECT COUNT(calluuid) as total FROM ost_call_log
            WHERE direction LIKE 'outbound'
              AND disposition LIKE 'NO ANSWER'
              AND DATE(starttime) = CURDATE()";
        $res = db_query($sql);
        if (!$res) return 0;
        $row = db_fetch_array($res);
        if (!$row || !isset($row['total'])) return 0;
        return intval($row['total']);
    }

    function alertAgent() {
        global $cfg;
        global $ost;

        $config = $cfg->getSmsAutoSettings();

        if (strtolower('NO ANSWER') == strtolower($this->disposition)
            && strtolower('inbound') == strtolower($this->direction)
        ) {
            // báo zalo cho nhân viên khi nhân viên trực tổng đài
            if ($config['missed_call_sms_agent']
                && $config['missed_call_sms_agent_numbers']
            ) {
                $list = explode("\r\n", $config['missed_call_sms_agent_numbers']);
                foreach ($list as $k => $v)
                    $list[$k] = trim($v);

                if (!in_array($this->destinationnumber, $list)
                    && preg_match('/^0?[^2][0-9]{8}$/', $this->destinationnumber)
                ) $this->sendMissCallAlertAgentSms();
            }

            // gửi email báo cho nhân viên đang xử lý ticket khi khách gọi vào mà không ai nghe máy
            $phone = $this->callernumber;
            if (!preg_match('/^0/', $phone))
                $phone = '0'.$phone;

            $sql = "SELECT
              t.ticket_id,
              t.number,
              t.staff_id
            FROM ost_user u join ost_form_entry e ON u.id=e.object_id and e.object_type LIKE 'U'
              JOIN ost_form_entry_values v on e.id=v.entry_id
              JOIN ost_ticket t ON t.user_id=u.id
              JOIN ost_ticket_status s ON s.id = t.status_id AND s.state LIKE 'open'
            WHERE t.staff_id IS NOT NULL AND t.staff_id <> '' AND t.staff_id <> 0 AND trim(v.value) LIKE '".$phone."'
            ORDER BY IFNULL(t.updated, t.created) DESC
            LIMIT 1";

            if ( ! (
                ($res = db_query($sql))
                && ($row = db_fetch_array($res))
                && isset($row['staff_id']) && $row['staff_id']
            ) ) {
                return false;
            }

            $staff = Staff::lookup($row['staff_id']);
            if (!$staff) return;

            $phone_number = $staff->getMobilePhone();
            if (!$phone_number) return false;

            $phone_number = preg_replace('/[^\d]/', '', $phone_number);
            $ticket = Ticket::lookup($row['ticket_id']);
            if (!$ticket) return false;

            $user = $ticket->getUser();
            if (!$user) return false;

            $url = $cfg->getBaseUrl().'/scp/tickets.php?id='.$row['ticket_id'];
            $subject = 'URGENT - Ticket '.$row['number'].' yêu cầu gọi lại gấp - '. $this->starttime;
            $message = sprintf("<p>Khách của ticket <a href=\"%s\">%s</a></p><p>Cuộc gọi nhỡ từ số <strong>%s</strong>, vào lúc <em>%s</em></p>
            <p><strong>Vui lòng gọi lại gấp!</strong></p>",
                               trim($cfg->getUrl(), '/').'/scp/tickets.php?id='.$row['ticket_id'],
                               $row['number'],
                               $phone,
                               $this->starttime);
            $params = [];
            $params['phone_number'] = $phone_number;
            $params['url'] = $url;
            $params['created_by'] = 0;
            $params['save_as_news'] = true;
            $noti = strip_tags($message);

            $title = 'URGENT - Ticket '.$row['number'].' yêu cầu gọi lại gấp';
            $_errors = [];
            $notification_send = \Tugo\Notification::send($title, $noti, true, $params, $_errors);
            $email=null;
            if(!($email=$ost->getConfig()->getAlertEmail()))
                $email=$ost->getConfig()->getDefaultEmail(); //will take the default email.

            $to_email = $staff->getEmail();
            if($email) {
                $email->sendAlert($to_email, $subject, $message, null, array('reply-tag'=>false));
            } else {//no luck - try the system mail.
                Mailer::sendmail($to_email, $subject, $message, '"' . __('osTicket Alerts') . sprintf('" <%s>', $to_email));
            }
        } // END if (strtolower('NO ANSWER') == strtolower($this->disposition)
    }

    function sendMissCallSms($pattern) {
        $text = $pattern;
        $error = [];
        _SMS::send($this->callernumber, _String::khongdau($text), $error, 'Missed Call');
    }

    function sendAgentInfoSms($pattern) {
        $staff = Staff::lookup(['phone_ext' => $this->destinationnumber]);
        if (!$staff) return false;

        $name = $staff->getFirstName();
        $info = $staff->getInfo();

        $text = $pattern;
        $text = str_replace('AGENT_PHONE_NUMBER', preg_replace('/[^0-9]/', '', $info['mobile']), $text);
        $text = str_replace('AGENT_NAME', $name, $text);

        $error = [];
        _SMS::send($this->callernumber, _String::khongdau($text), $error, '50s Call');
    }

    function sendFailedCallNotifySms($pattern) {
        $staff = Staff::lookup(['phone_ext' => $this->callernumber]);
        if (!$staff) return false;

        $name = $staff->getFirstName();
        $info = $staff->getInfo();

        $text = $pattern;
        $text = str_replace('AGENT_PHONE_NUMBER', preg_replace('/[^0-9]/', '', $info['mobile']), $text);
        $text = str_replace('AGENT_NAME', $name, $text);

        $error = [];
        _SMS::send($this->destinationnumber, _String::khongdau($text), $error, 'Notify Call');
    }

    function sendMissCallAlertAgentSms() {
        $text = date('d M Y H:i:s').' - Missed call: ' . $this->callernumber;
        $error = [];

        $notification_send = false;

        $params = [];
        $params['phone_number'] = $this->destinationnumber;
        $params['created_by'] = 0;
        $params['save_as_news'] = true;

        $title = NOTIFICATION_TITLE;
        $_errors = [];
        $notification_send = \Tugo\Notification::send($title, $text, true, $params, $_errors);

        if (!$notification_send) {
            $zalo_uid = null;
            $staff = \Staff::getIdByMobilePhone($this->destinationnumber);
            $staff = Staff::lookup($staff);
            if ($staff) $zalo_uid = $staff->getZaloUid();
            if ($zalo_uid) _Zalo::sendTextToZalo($text, $zalo_uid, $error);
        }
    }

    function sendEmailMissedCall($to) {
        global $ost;

        $subject = 'Tugo osTicket - Missed Call ['.$this->callernumber.']';
        $message = sprintf("<p>Tổng đài: <strong>%s</strong>.</p><p>Cuộc gọi nhỡ từ số <strong>%s</strong>, vào lúc <em>%s</em></p>",
            $this->destinationnumber, $this->callernumber, $this->starttime);
        $email=null;
        if(!($email=$ost->getConfig()->getAlertEmail()))
            $email=$ost->getConfig()->getDefaultEmail(); //will take the default email.

        if($email) {
            $email->sendAlert($to, $subject, $message, null, array('reply-tag'=>false));
        } else {//no luck - try the system mail.
            Mailer::sendmail($to, $subject, $message, '"'.__('osTicket Alerts').sprintf('" <%s>',$to));
        }
    }

    function canSendSms($config) {
        // check gửi SMS theo khung giờ
        if (isset($config['sms_auto_active_time_from']) && $config['sms_auto_active_time_from']
            && isset($config['sms_auto_active_time_to']) && $config['sms_auto_active_time_to']
        ) {
            $from = date_create_from_format('H:i', trim($config['sms_auto_active_time_from']), new DateTimeZone('+0700'));
            $to = date_create_from_format('H:i', trim($config['sms_auto_active_time_to']), new DateTimeZone('+0700'));
            if ($from->getTimestamp() > time() || $to->getTimestamp() < time()) {
                return false;
            }
        }

        if ('outbound' === strtolower($this->direction)) {
            if (!_String::isMobileNumber($this->destinationnumber)) return false;
            if (!in_array($this->disposition, ['FAILED', 'NO ANSWER', 'BUSY'])) return false;

            return $this->firstCallIn(3, false, $this->destinationnumber);
        }

        if ('inbound' !== strtolower($this->direction))
            return false;

        if (!_String::isMobileNumber($this->callernumber)) return false;

        if ('ANSWERED' == $this->disposition
            && abs(strtotime($this->answertime) - strtotime($this->endtime)) < 50)
            return false;

        if (!in_array($this->disposition, ['NO ANSWER', 'ANSWERED']))
            return false;

        if (!$this->firstCallThisWeek())
            return false;

        return true;
    }

    function firstCallThisWeek() {
        $sql = 'SELECT * FROM '. CALL_LOG_TABLE . ' WHERE 1 '
            . ' AND callernumber LIKE ' . db_input('%'.$this->callernumber.'%')
            . ' AND direction = ' . db_input('inbound')
            . ' AND starttime >= ' . db_input(date('Y-m-d 00:00:00', time()-3600*24*7))
        ;
        if(($res=db_query($sql)) && db_num_rows($res) > 0)
            return false;

        return true;
    }

    function firstCallIn($hours = 6, $callernumber = false, $destinationnumber = false) {
        if (!$callernumber && !$destinationnumber) return false;

        $sql = 'SELECT * FROM '. CALL_LOG_TABLE . ' WHERE 1 '
            . ' AND disposition <> '. db_input('NO ANSWER')
                . ' AND disposition <> '. db_input('BUSY')
                . ' AND disposition <> '. db_input('FAILED')
            . ($callernumber ? (' AND callernumber LIKE ' . db_input('%'.$callernumber) . ' AND direction = ' . db_input('inbound')) : ' ')
            . ($destinationnumber ? (' AND destinationnumber LIKE ' . db_input('%'.$destinationnumber). ' AND direction = ' . db_input('outbound')) : ' ')
            . ' AND starttime >= ' . db_input(date('Y-m-d 00:00:00', time()-3600*$hours))
        ;
        if(($res=db_query($sql)) && db_num_rows($res) > 0)
            return false;

        return true;
    }
}

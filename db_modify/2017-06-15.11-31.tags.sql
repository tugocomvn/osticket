CREATE TABLE ost_tag
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  content VARCHAR(32)
);
CREATE UNIQUE INDEX ost_tag_content_uindex ON ost_tag (content);
ALTER TABLE ost_tag COMMENT = 'ticket tags';

CREATE TABLE ost_ticket_tag
(
  ticket_id INT,
  tag_id INT
);
CREATE UNIQUE INDEX ost_ticket_tag_ticket_id_tag_id_uindex ON ost_ticket_tag (ticket_id, tag_id);
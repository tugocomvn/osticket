<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');

$country_item = DynamicListItem::lookup($_country['country']);
?>
<style>
    .current_reservation td:nth-child(3) {
        border-right: 1px solid  #ccc;
    }
</style>
<div class="tour_item">
    <div class="tour_header c_blue">
        <h3 class="tour_name">
            <span class="label label-default">Visa</span> <?php echo $country_item->getValue() ?>
            <small><?php echo $_country['year'].'-'.sprintf('%02d', $_country['month']) ?></small>
        </h3>
        <div class="clearfix"></div>
    </div>
    <div class="tour_footer">
        <div class="function">
            <div class="buttons">
                <?php
                $reservation_history = BookingReservationHistory::getByTour(0, $_country['country']);
                $reservations = BookingReservation::getByTour(0, $_country['country']);
                ?>
                <?php if(db_num_rows($reservation_history) > 0): ?>
                    <button class="btn_sm btn-xs btn-default btn_history">History</button>
                <?php endif; ?>
                <?php if(db_num_rows($reservations) > 0): ?>
                    <button class="btn_sm btn-xs btn-success btn_details">List</button>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="table_list history" style="display: none">
            <?php
            $i = 1;
            ?>
            <table class="booking_table">
                <caption>History</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale</th>
                    <th>Action</th>
                    <th>Qty</th>
                    <th>Customer / <br>Phone number</th>
                    <th>Booking Code</th>
                    <th>Update Time</th>
                    <th>Due</th>
                    <th>Note</th>
                    <th>Updated By</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php $count_item = 0; ?>
                <?php while($reservation_history && ($history = db_fetch_array($reservation_history))): $count_item++; ?>
                    <tr class="history_item" data-id="<?php echo $history['booking_reservation_id'] ?>">
                        <td><?php echo $i++; ?></td>
                        <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
                        <td>
                            <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo 'Giữ'; ?>
                            <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo 'Sure'; ?>
                            <?php if(!$history['hold_qty'] && !$history['sure_qty']): ?>
                                Hủy
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?>
                            <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?>
                        </td>
                        <td><?php echo $history['customer_name'] ?>
                            <br><?php echo $history['phone_number'] ?></td>
                        <td><?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?></td>
                        <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?></td>
                        <td>
                            <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at']));
                            ?>
                        </td>
                        <td>
                            <?php echo $history['note'] ?>
                        </td>
                        <td><?php if (isset($all_staff[ $history['created_by'] ]) && $history['created_by']) echo $all_staff[ $history['created_by'] ]; else echo 'Tugo'; ?></td>
                        <td>
                            <?php if( (isset($history['hold_qty']) && $history['hold_qty']) || (isset($history['sure_qty']) && $history['sure_qty']) ): ?>
                                <a href="#" class="btn_sm btn-default btn-xs" data-title="Edit"><i class="icon-edit"></i></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endwhile; ?>
                <?php if(!$count_item): ?>
                    <tr><td>No Item</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="table_list upcoming" style="display: none">

        </div>
        <div class="clearfix"></div>
        <div class="table_list details" >
            <?php
            $i = 1;
            ?>
            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
            <table class="booking_table current_reservation">
                <caption>Current Reservations</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale</th>
                    <th>Hold</th>
                    <th>Sure</th>
                    <th>Customer</th>
                    <th>Phone number</th>
                    <th>Booking Code</th>
                    <th>Update Time</th>
                    <th>Due</th>
                    <th>Note</th>
                    <th>Created By</th>
                    <th>Updated By</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php $count_item = 0; ?>
                <?php while($reservations && ($history = db_fetch_array($reservations))): $count_item++; ?>
                    <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue' ?>">
                        <td><?php echo $i++; ?></td>
                        <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
                        <td>
                            <?php if((int)$history['hold_qty'] === 0) echo '-' ?>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['hold_qty'] ?>" style="display: none" form="action_form_<?php echo $history['id'] ?>" type="number" name="hold_qty" class="input-field" width="2" step="1" min="0" max="50" value="<?php
                                if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?><?php
                                ?>">
                            <span><?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if((int)$history['sure_qty'] === 0) echo '-' ?>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['sure_qty'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="number"
                                       name="sure_qty" class="input-field" width="2" step="1" min="0" max="50" value="<?php
                                if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty'];
                                ?>">
                            <span><?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['customer_name'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       name="customer_name" class="input-field" value="<?php
                                if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?><?php
                                ?>">
                                <span><?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['phone_number'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       name="phone_number" class="input-field" width="11" value="<?php
                                if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?><?php
                                ?>">
                                <span><?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?>
                            <?php endif; ?>
                        </td>
                        <td><?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?></td>
                        <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/Y H:i:s', strtotime($history['created_at'])); ?></td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php if (isset($history['due_at']) && strtotime($history['due_at']))
                                    echo date('d/m/Y', strtotime($history['due_at'])); ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       class="dp_due new input-field" name="due_at" value="<?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>">
                            <span>
                                <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>
                            </span>
                            <?php else: ?>
                                <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <textarea data-ori="<?php echo $history['note'] ?>" style="display: none"
                                          form="action_form_<?php echo $history['id'] ?>" name="note" class="input-field"><?php
                                    if (isset($history['note']) && $history['note']) echo $history['note']; ?></textarea>
                                <span><?php if (isset($history['note']) && $history['note']) echo $history['note']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                            <?php endif; ?>
                        </td>

                        <td><?php if (isset($all_staff[ $history['created_by'] ]) && $history['created_by']) echo $all_staff[ $history['created_by'] ]; else echo 'Tugo'; ?></td>
                        <td><?php if (isset($all_staff[ $history['updated_by'] ]) && $history['updated_by']) echo $all_staff[ $history['updated_by'] ]; ?></td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <form action="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php" method="post" name="action_form_<?php echo $history['id'] ?>">
                                    <input type="hidden" name="reservation_id" value="<?php echo $history['id'] ?>">
                                    <button type="button" class="btn_sm btn-default btn-xs edit_btn">Edit</button>
                                    <button style="display: none" type="submit" value="detail_action" class="btn_sm btn-primary btn-xs update_btn">Save</button>
                                    <button style="display: none" type="button" class="btn_sm btn-danger btn-xs reset_btn">Cancel</button>
                                </form>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endwhile; ?>
                <?php if(!$count_item): ?>
                    <tr><td>No Item</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
namespace Tugo;

require_once(INCLUDE_DIR . 'class.orm.php');
require_once(INCLUDE_DIR . 'tugo.user.php');
require_once(INCLUDE_DIR . 'tugo.news.php');

class NotificationModel extends \VerySimpleModel {
    static $meta = [
        'table' => NOTIFICATION_TABLE,
        'pk' => ['id'],
        'ordering' => ['sent_at'],
    ];
}

class Notification extends NotificationModel {
    function save($refetch=false) {
        if (count($this->dirty)) //XXX: doesn't work??
            $this->set('sent_at', new \SqlFunction('NOW'));
        return parent::save($refetch);
    }

    public static function getPagination(array $params, $offset, $limit = 30, &$total) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY sent_at DESC ";
        $where = "  ";
//
//        if (isset($params['uuid']) && !empty(trim($params['uuid']))) {
//            $where .= " AND user_uuid LIKE ".db_input(trim($params['uuid']));
//        }
//
//        if (isset($params['app_uuid']) && !empty(trim($params['app_uuid']))) {
//            $where .= " AND ((user_uuid IS NOT NULL AND user_uuid LIKE ".db_input(trim($params['app_uuid'])).') OR user_uuid LIKE \'\' OR user_uuid IS NULL) ';
//        }
//
//        if (isset($params['title']) && !empty(trim($params['title']))) {
//            $where .= " AND user_uuid LIKE ".db_input('%'.trim($params['title']).'%');
//        }
//
//        if (isset($params['content']) && !empty(trim($params['content']))) {
//            $where .= " AND user_uuid LIKE ".db_input('%'.trim($params['content']).'%');
//        }

        $sql = " SELECT * FROM ".NOTIFICATION_TABLE." WHERE 1 $where $order $limit ";

        $res = db_query($sql);
        $total = db_num_rows($res);
        return $res;
    }

    public static function send($title, $content, $sound_on = true, array $params, &$errors) {
        $cloud_message_token = null;
        $res = false;
        $save_as_news = false;

        if (isset($params['save_as_news']) && $params['save_as_news'])
            $save_as_news = true;

        if (isset($params['cloud_message_token']) && $params['cloud_message_token'])
            $cloud_message_token = trim($params['cloud_message_token']);

        if ($cloud_message_token) {
            $res = self::_send($cloud_message_token, $title, $content, $sound_on, $params, $errors);

            if ($res && $save_as_news && isset($params['phone_number'])) {
                \Tugo\News::insert($title, $content, ['phone_number' => $params['phone_number']]);
            }

            return $res;
        }

        $user = null;
        if (isset($params['user_uuid']) && $params['user_uuid']) {
            $user = \Tugo\User::get(['uuid' => $params['user_uuid']]);
        } elseif (isset($params['phone_number']) && $params['phone_number']) {
            $user = \Tugo\User::get(['phone_number' => $params['phone_number']]);
        } elseif (isset($params['customer_number']) && $params['customer_number']) {
            $user = \Tugo\User::get(['customer_number' => $params['customer_number']]);
        }

        if (!$user) {
            $errors[] = 'User not found';
            return false;
        }

        $tokens = \Tugo\Device::get(['uuid' => $user['uuid']]);

        if (is_array($tokens) && count($tokens)) {
            foreach ($tokens as $_token) {
                $res = self::_send($_token, $title, $content, $sound_on, $params, $errors);

                if ($res && $save_as_news && isset($params['phone_number'])) {
                    \Tugo\News::insert($title, $content, ['phone_number' => $params['phone_number']]);
                }
            }

            return true;
        }

        return false;
    }

    private static function _send($cloud_message_token, $title, $content, $sound_on, array $params, &$errors) {
        $curl = curl_init();

        $postfields = '{
                "to" : "'.$cloud_message_token.'",
                "notification": {
                    "title": "'.$title.'",
                    "body": "'.$content.'",
                    "sound": "'.($sound_on?'on':'off').'"
                }'.
            (isset($params['url']) && $params['url'] ? '
                ,"data": {
                    "type": "url",
                    "url": "'.trim($params['url']).'"
                }
            ' : ' ')
            .'
            }';

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => array(
                "authorization: ".FIREBASE_SERVER_TOKEN,
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $errors = $err;
            return false;
        } else {
            $response = json_decode($response);
            if (isset($response->multicast_id) && $response->multicast_id
                && isset($response->success) && $response->success
                && isset($response->results) && isset($response->results[0])
                && isset($response->results[0]->message_id) && $response->results[0]->message_id
            ) {
                $notificaiton = \Tugo\Notification::create(
                    [
                        'sender_id' => $params['staff_id'],
                        'sent_at' => new \SqlFunction('NOW'),
                        'title' => $title,
                        'content' => $content,
                        'sound_on' => 1,
                        'phone_number' => isset($params['phone_number']) ? $params['phone_number'] : '',
                        'customer_number' => isset($params['customer_number']) ? $params['customer_number'] : '',
                        'cloud_message_token' => isset($params['cloud_message_token']) ? $params['cloud_message_token'] : '',
                        'user_uuid' => isset($params['user_uuid']) ? $params['user_uuid'] : '',
                        'multicast_id' => $response->multicast_id,
                        'message_id' => $response->results[0]->message_id,
                        'failure' => $response->failure,
                        'status' => 1,
                    ]
                );

                $notificaiton->save(true);

                return true;
            }

            return false;
        }
    }
}

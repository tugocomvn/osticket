<?php
/*********************************************************************
    logs.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once  INCLUDE_DIR.'class.bonus.php';
require_once  INCLUDE_DIR.'class.staff.php';

$id = $thisstaff->getId();
$allStaff = Staff::getAlLStaff();

function string_to_arrayCode($data){
    $arr = preg_split('/\r\n|\r|\n/', $data);
    $arrCode = array();
    foreach ($arr as $code)
    {
        $code = trim($code);

        if(!empty($code)) // mã không rỗng
        {
            array_push($arrCode,$code);
        }
    }
    return $arrCode;
}

function convert_booking_code_trim($arrCode)
{
    foreach ($arrCode as &$code)
    {
        $partern = sprintf("/^%s/",BOOKING_CODE_PREFIX);
        $code = preg_replace($partern,'',$code);
        $code = ltrim($code,'0');
        $code = db_input($code);
    }
    if(is_string($arrCode))
    {
        $partern = sprintf("/^%s/",BOOKING_CODE_PREFIX);
        $arrCode = preg_replace($partern,'',$arrCode);
        $arrCode = ltrim($arrCode,'0');
    }
    return $arrCode;
}

function convert_IN_condition($arr) //return string ('a,'b');
{
    if (empty($arr))
        return '';
    return '(' . implode(',', $arr) .')';
}

function convert_report_info($arr_data)
{
    $arr = [];
    foreach ($arr_data as $data)
    {

        $arr[$data['id']][0] = $data['month_of_report'].'/'.$data['year_of_report']; //time
        $arr[$data['id']][1] = $data['staff_id_of_report']; //staff
    }
    return $arr;
}


//variable

if (isset($_REQUEST['action']) && 'list_report' === $_REQUEST['action'])
{
    $results = BonusCheck::get_report();
    $page='report_commission.inc.php';
}

if (isset($_REQUEST['action']) && 'create_report' === $_REQUEST['action'])
{

    if (!empty($_REQUEST['month'] && is_numeric($_REQUEST['month']) && $_REQUEST['month'] >= 1 && $_REQUEST['month'] <= 12))
    {
        $month = $_REQUEST['month'];
    }else $month = null;

    if(!empty($_REQUEST['year']) && is_numeric($_REQUEST['year']))
    {
        $year = $_REQUEST['year'];
    }else $year = null;

    if(!empty($_REQUEST['agent']) && is_numeric($_REQUEST['agent']))
    {
        $agent = $_REQUEST['agent'];
    }else $agent = null;

    if(!empty($_REQUEST['note']))
    {
        $note = $_REQUEST['note'];
    }else $note = null;

    $data = [];

    if(isset($_REQUEST['create']))
    {
        $id_report = BonusCheck::insert_report($month,$year,$agent,$id,$note);
        $link = 'report_commission.php?action=check_booking&id='.$id_report;
        header('Location:'.$link );
        exit;
    }

    $nav->setActiveSubMenu(6);
    $page='io_create_report.inc.php';
}

if (isset($_REQUEST['action']) && 'check_booking' === $_REQUEST['action'])
{
    $id_report = 0;
    $add_note = "none";
    $form_check = true;

    $timeReport = BonusCheck::get_report();
    $timeReport = convert_report_info($timeReport);
    $results_from_code = array();
    $results_from_id = array();

    if (isset($_REQUEST['id']))
    {
        $id_report = $_REQUEST['id'];
        $results_from_id = BonusCheck::get_Booking_from_Report($id_report);

        //An form nhap ma booking
        if (!empty($results_from_id))
        {
            $form_check = false;
        }
    }
    if(isset($_REQUEST['code']) && !empty($_REQUEST['code']))
    {
        $code = $_REQUEST['code'];
        $code = string_to_arrayCode($code);

    }elseif (isset($_SESSION['code']))
    {
        $code = $_SESSION['code'];

    }
    if (!empty($code))
    {
        $code_trim = convert_booking_code_trim($code);
        $code_IN_sql = convert_IN_condition($code_trim);
        if (!empty($code_IN_sql)) {
            $results_from_code = BonusCheck::get_Booking($code_IN_sql);
        }
    }

    if (!empty($results_from_id))
    {
        $results = $results_from_id;
    }else $results = $results_from_code;

    if (isset($_REQUEST['save']) && $_REQUEST['save'] === "save")
    {
        unset($_SESSION['code']);

        if (!empty($results_from_id) && empty($results_from_code)) {
            foreach ($results_from_id as $data) {
                $note = 'note_' . $data['booking_code'];

                if (isset($_REQUEST[$note])) {
                    $note = $_REQUEST[$note];
                } else $note = "";

                if (!empty($note)) {
                    //cap nhat dong du lieu in report_booking
                    BonusCheck::update_note_booking($data['booking_id'], $note);
                }
            }
        }

        //insert
        if (empty($results_from_id) && !empty($results_from_code)) {
            foreach ($results as $data) {
                $note = 'note_' . $data['booking_code'];

                if (isset($_REQUEST[$note])) {
                    $note = $_REQUEST[$note];
                } else $note = "";

                if(!isset($data['report_id'])) { //record chua có trong DB
                    //Them dong du lieu booking in report_booking
                    BonusCheck::insert_booking($data['booking_code'], $data['total_retail_price'], $data['total_quantity'], $data['staff_id'], $id_report, $note);
                }
            }
        }

        //get data from database again
        $results = BonusCheck::get_Booking_from_Report($id_report);
        $results_from_id = BonusCheck::get_Booking_from_Report($id_report); //get data from id after insert
        if (!empty($results_from_id))
        {
            $form_check = false;
        }
    }

    if (isset($_REQUEST['check']) && $_REQUEST['check'] === "check")
    {
        if (!empty($code)) {
            $_SESSION['code'] = $code;
        }
    }
    $page='io_check_bonus.inc.php';
}

$nav->setTabActive('finance');
$ost->addExtraHeader('<meta name="tip-namespace" content="finance.io" />',
    "$('#content').data('tipNamespace', 'finance.io');");
require(STAFFINC_DIR.'header.inc.php');
?>
<?php
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>


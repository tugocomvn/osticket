CREATE TABLE ost_tour
(
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  destination INT(11),
  description TEXT,
  status TINYINT(2),
  created_at DATETIME,
  created_by INT(11),
  last_updated_at DATETIME,
  last_updated_by INT(11)
);
CREATE INDEX ost_tour_name_index ON ost_tour (name);
CREATE INDEX ost_tour_destination_index ON ost_tour (destination);

CREATE TABLE ost_tour_group
(
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  tour_id INT(11),
  departure_date DATETIME,
  arrival_date DATETIME,
  carrier_id INT(11),
  return_departure_date DATETIME,
  return_arrival_date DATETIME,
  price DECIMAL(12),
  promotion DECIMAL(12),
  promo_code VARCHAR(32),
  promo_condition TEXT,
  tour_leader_id INT(11),
  tour_guide_id INT(11),
  total_seat SMALLINT(3),
  sure_seat SMALLINT(3),
  hold_seat SMALLINT(3),
  available_seat SMALLINT(3)
);

create index ost_tour_group_available_seat_index on ost_tour_group (available_seat);
create index ost_tour_group_carrier_id_index on ost_tour_group (carrier_id);
create index ost_tour_group_departure_date_index on ost_tour_group (departure_date);
create index ost_tour_group_tour_guide_id_index on ost_tour_group (tour_guide_id);
create index ost_tour_group_tour_id_index on ost_tour_group (tour_id);
create index ost_tour_group_tour_leader_id_index on ost_tour_group (tour_leader_id);

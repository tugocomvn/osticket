<?php
/**
 * Created by PhpStorm.
 * User: minhjoko
 * Date: 7/10/18
 * Time: 10:17 AM
 */
require('staff.inc.php');

$page='io-chart.inc.php';
$nav->setTabActive('finance');
$ost->addExtraHeader('<meta name="tip-namespace" content="finance.chart" />',
    "$('#content').data('tipNamespace', 'finance.chart');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
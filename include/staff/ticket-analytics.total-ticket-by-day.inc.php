<?php
$total_chart = $x = [];
TicketAnalytics::analytics($_REQUEST['from'], $_REQUEST['to'], 'total', $_REQUEST, $total_chart, $x);
$total_chart_arr = [];
while($total_chart && ($row = db_fetch_array($total_chart))) {
    switch ($_REQUEST['group']) {
        case 'week':
            $total_chart_arr[ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $total_chart_arr[ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $total_chart_arr[ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}
?>
<td colspan="">
    <h2>Total Ticket by Day</h2>
    <canvas id="total" width="500" height="300"></canvas>
</td>
<script>
    var total_elm = document.getElementById("total").getContext('2d');
    i = 0;
    var sources = new Chart(total_elm, {
        type: 'line',
        data: {
            datasets: [
                {
                    borderColor: '#55f',
                    label: 'Ticket By Day',
                    data: $.parseJSON('<?php echo json_encode(array_values($total_chart_arr)) ?>')
                }
            ],
            labels: $.parseJSON('<?php echo json_encode(array_keys($total_chart_arr)) ?>'),
        },
        options: {
            responsive: false,
            maintainAspectRatio: false
        }
    });
</script>

<?php
require_once('../client.inc.php');
require_once('../include/class.receipt.php');
require_once('../include/class.tracking_receipt.php');

$_REQUEST['passkey'] = isset($_REQUEST['passkey']) ? $_REQUEST['passkey'] : '';
$pass_key = trim($_REQUEST['passkey']);
if($pass_key){
	$values = Receipt::lookup(['hash_url' => $pass_key]);
	$results = Receipt::lookup($values->id);
	if(!isset($results)) {
		echo "Receipt not found";
		exit;
	}
	$track = _this_track_receipt($values->id);
}

function _this_track_receipt($receipt_id) {
	$receipt_id = (int)($receipt_id);
	$receipt = Receipt::lookup($receipt_id);
    if($receipt){
        Receipt_Tracking::update([
			'receipt_id' => $receipt_id,
			'status'	=> 1,
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'add_time' => date('Y-m-d H:i:s'),
		]);				
    }
}
?>

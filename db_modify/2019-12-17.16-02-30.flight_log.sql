DROP TABLE IF EXISTS `flight_log`;
CREATE TABLE IF NOT EXISTS `flight_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_id` INT NULL,
  `content` LONGTEXT NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_id` ASC),
  INDEX `index3` (`created_at` ASC),
  INDEX `index4` (`created_by` ASC))
ENGINE = InnoDB;
DROP TABLE IF EXISTS `flight`;
CREATE TABLE IF NOT EXISTS `flight` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_code` VARCHAR(10) NULL,
  `airline_id` INT NULL,
  `airport_code_from` VARCHAR(6) NULL,
  `airport_code_to` VARCHAR(6) NULL,
  `departure_at` DATETIME NULL,
  `arrival_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  `updated_at` DATETIME NULL,
  `updated_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_code` ASC),
  INDEX `index3` (`airline_id` ASC),
  INDEX `index4` (`airport_code_from` ASC),
  INDEX `index5` (`airport_code_to` ASC),
  INDEX `index6` (`departure_at` ASC),
  INDEX `index7` (`arrival_at` ASC))
ENGINE = InnoDB;
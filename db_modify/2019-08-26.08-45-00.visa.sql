CREATE TABLE `status_finance` (
  `id` int(10) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `staff_id_create` int(10) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staff_id_edit` int(10) NOT NULL,
  `date_edit` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `status_finance` ADD PRIMARY KEY (`id`);
ALTER TABLE `status_finance` MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

ALTER TABLE `tour_pax_visa_profile` ADD `finance_status` INT(11) NOT NULL;
ALTER TABLE `tour_pax_visa_profile` ADD `staff_id` INT(11) NOT NULL;
ALTER TABLE `tour_pax_visa_profile` ADD `deadline` DATE NULL DEFAULT NULL;
ALTER TABLE `tour_pax_visa_profile` ADD `visa_code` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `visa_profile_appointment` ADD `note` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

/*2019-27-08*/
ALTER TABLE `tour_pax_visa_profile` ADD `household_address` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL

/*2019-29-08*/
ALTER TABLE `tour_pax_visa_profile` ADD `city_id` INT(5) NOT NULL, 
									ADD `district_id` INT(5) NOT NULL, 
									ADD `ward_id` INT(5) NOT NULL;
ALTER TABLE `tour_pax_visa_profile` CHANGE `household_address` `street_address` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

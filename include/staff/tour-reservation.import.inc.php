<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/tour-list.php"?>">Back</a></p>
<h2>Import Reservation to Tour </h2>
<div class="centered">
    <h3><?php echo $results->name ?></h3>
    <input type="hidden" id="tour" name="tour" value="<?php if(isset($results->id)) echo $results->id ?>">
    <p>Điểm đến: <b><?php echo TourNew::getBookingTypeName($results->destination) ?></b></p>
    <p>Ngày khởi hành - về
        <?php if (!empty($results->gateway_present_time) && !empty($results->return_time)): ?>
        <?php _Format::dateInSameYear($results->gateway_present_time, $results->return_time); ?>
        <?php endif; ?>
    </p>
</div>
<body>
    <div class="wrapper">
        <div class="rowLayout">
            <p>
                <button name="view" id="view">Xem trước nội dung</button>
            </p>
            <div id="message" class="message">Click "Xem trước nội dung" to load data format</div>
            <div id="main_reservation"></div>
            <h2>Xem trước nội dung</h2>
            <div id="view_reservation"></div>
            <p class="centered">
                <button class="btn_sm btn-primary" id="save" name="save">Confirm & Save</button>
                <a href="<?php echo $cfg->getUrl()."scp/tour-list.php"?>" class="btn_sm btn-danger">Cancel</a>
            </p>
        </div>
    </div>
</body>
<script>
    var tour_id = $('#tour').val();
    var
        $container = $("#main_reservation"),
        $console = $("#message"),
        NumberValidator,
        NumberValidator = function(value, callback) {
            if (/^[0-9]*$/.test(value)) {
                callback(true);
                $("#save").removeAttr("disabled");
                $console.text('');
            } else {
                callback(false);
                $console.text('Nhập sai định dạng . Vui lòng nhập lại!!!');
                $("#save").prop("disabled",true);
            }
        },
        FormatValidatorData,
        FormatValidatorData = function(value, callback) {
            if (/([a-zA-Z]+)([\.])([a-zA-Z]+)/.test(value)) {
                callback(true);
                $("#save").removeAttr("disabled");
                $console.text('');
            } else {
                callback(false);
                $console.text('Tên sale không đúng định dạng ten.ho Ví dụ: quy.nguyen!!!');
                $("#save").prop("disabled",true);
            }
        },
        $parent = $container.parent(),    
        hot;
    hot = new Handsontable($container[0], {
        rowHeaders: true,
        startRows: 10,
        startCols: 7,
        minSpareRows: 1,
        colHeaders: true,
        maxCols: 20,
        contextMenu: true,
        width: '100%',
        colWidths: [50 ,50 ,230 ,50 ,50 ,230 ,50 ,50 ,230 ],
            licenseKey: 'non-commercial-and-evaluation',
            nestedHeaders: [
            ['GIU', 'SURE', 'TÊN', 'GIU', 'SURE', 'TÊN', 'GIU', 'SURE', 'TÊN'],
            ],
            columns: [
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: FormatValidatorData,
            allowInvalid: true
            },
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: FormatValidatorData,
            allowInvalid: true
            },
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: NumberValidator,
            allowInvalid: true
            },
            {
            validator: FormatValidatorData,
            allowInvalid: true
            }
            ],
        minSpareCols: 0,
        minSpareRows: 1,
        contextMenu: true,
        cells: function (row, col, prop, value) {
            var cellProperties = {};
            cellProperties.renderer = fisrtRowRenderer;
            return cellProperties;
        }
    });
    function fisrtRowRenderer(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        if (!value || value === '' || value == null) {
            td.innerHTML = "";
        }        
    }
    $parent.find('button[name=view]').click(function () {
        $.ajax({
        url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/reservation/load',
        data: {data: hot.getData()}, // returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
            if (res.result === 1) {
                var data = [], row;
                for (var i = 0, ilen = res.all_values.length; i < ilen; i++) {
                row = [];
                row[0] = res.all_values[i].giu;
                row[1] = res.all_values[i].sure;
                row[2] = res.all_values[i].name;
                row[3] = res.all_values[i].giu_;
                row[4] = res.all_values[i].sure_;
                row[5] = res.all_values[i].name_;
                row[6] = res.all_values[i].giu__;
                row[7] = res.all_values[i].sure__;
                row[8] = res.all_values[i].name__;
                data[res.index[i] + 1] = row;
                }
                _hot.loadData(data);
                $console.text(res.message);
                messageSuccess();
            }else{
                $console.text(res.message);
                messageError();
            }
            }
        });
    });
    function messageError(){
        document.getElementById("message").style.color = "Red";
    }

    function messageSuccess(){
        document.getElementById("message").style.color = "#4CAF50";
    }
    
    $parent.find('button[name=save]').click(function () {
        if (confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {           
            $.ajax({
            url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/reservation/save',
            data: { tour_id : tour_id, data: hot.getData()},
            dataType: 'json',
            type: 'POST',
            success: function (res) {
                if (res.result === 1) {
                    $console.text(res.message);
                    messageSuccess();
                    <?php $url = $cfg->getUrl().'scp/tour-list.php'; ?>
                    parent.setTimeout(function() { parent.window.location = "<?php echo $url ?>"; }, 1500);
                }else{
                    $console.text(res.message);
                    messageError();
                }
                }
            });
        }
    });
    var
        $container = $("#view_reservation"),
        $console = $("#message"),
        $parent = $container.parent(),    
        _hot;
    _hot = new Handsontable($container[0], {
        rowHeaders: true,
        startRows: 10,
        startCols: 7,
        minSpareRows: 1,
        colHeaders: true,
        maxCols: 20,
        contextMenu: true,
        width: '100%',
        colWidths: [50 ,50 ,230 ,50 ,50 ,230 ,50 ,50 ,230 ],
            licenseKey: 'non-commercial-and-evaluation',
            nestedHeaders: [
                ['GIU', 'SURE', 'TÊN', 'GIU', 'SURE', 'TÊN', 'GIU', 'SURE', 'TÊN'],
            ],
            columns: [
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            }
            ],
        minSpareCols: 0,
        minSpareRows: 1,
        contextMenu: true,
    });
</script>

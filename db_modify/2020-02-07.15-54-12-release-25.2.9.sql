/*branch booking*/
create table booking_document
(
    id int unsigned auto_increment,
    ticket_id int unsigned null,
    action_id tinyint null,
    filename text null comment 'name+extend',
    created_by int null,
    created_at datetime null,
    constraint booking_document_log_pk
        primary key (id)
);
alter table booking_document modify filename varchar(64) null comment 'name+extend';

create index booking_document_ticket_id_index
	on booking_document (ticket_id);


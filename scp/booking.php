<?php
/*********************************************************************
    logs.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once INCLUDE_DIR.'class.pax.php';
require_once(INCLUDE_DIR.'class.tour-destination.php');

global $thisstaff;
$booking_type_list = TourDestination::loadAllDestination();

if (!isset($_REQUEST['export']) || !$_REQUEST['export'] || !$thisstaff->canExportBookings())
    $_REQUEST['export'] = 0;

$qs = array();
$agent = $type = $booking_code = $info = $status = $partner = $customer_number = $tour = null;
if (isset($_REQUEST['booking_code'])) {
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += ['booking_code' => $_REQUEST['booking_code']];
}

if (isset($_REQUEST['info'])) {
    $info = trim($_REQUEST['info']);
    $qs += ['info' => $_REQUEST['info']];
}

if (isset($_REQUEST['agent'])) {
    $agent = trim($_REQUEST['agent']);
    $qs += ['agent' => $_REQUEST['agent']];
}

if (isset($_REQUEST['tour'])) {
    $tour = $_REQUEST['tour'];
    $qs += ['tour' => $tour];
}

if (isset($_REQUEST['customer_number'])) {
    $customer_number = trim($_REQUEST['customer_number']);
    $qs += ['customer_number' => $_REQUEST['customer_number']];
}

if (isset($_REQUEST['status'])) {
    $status = trim(json_encode(trim($_REQUEST['status'])), '"');
    $qs += ['status' => $_REQUEST['status']];
}

if (isset($_REQUEST['partner'])) {
    $partner = trim(json_encode(trim($_REQUEST['partner'])), '"');
    $qs += ['partner' => $_REQUEST['partner']];
}

$qwhere = ' WHERE 1 ';

if($booking_code) {
    $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $booking_code);
    $_booking_code_trim = ltrim($_booking_code_trim, '0');
    $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) LIKE '.db_input(strval($_booking_code_trim));
}

if($customer_number) {
    $customer_number_booking_code_list = [];
    $customer_number_sql = "SELECT booking_code FROM api_user u JOIN api_user_point_history p ON u.uuid=p.user_uuid WHERE customer_number LIKE ".db_input($customer_number);

    $res_customer_number = db_query($customer_number_sql);
    if ($res_customer_number) {
        while (($row = db_fetch_array($res_customer_number))) {
            $_tmp_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $row['booking_code']);
            $_tmp_booking_code_trim = ltrim($_tmp_booking_code_trim, '0');
            $customer_number_booking_code_list[] = db_input($_tmp_booking_code_trim);
        }

        $customer_number_booking_code_list = array_unique(array_filter($customer_number_booking_code_list));
    }

    if (count($customer_number_booking_code_list))
        $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) IN ('.implode(',', $customer_number_booking_code_list).') ';
    else
        $qwhere .= ' AND 1 = 0 ';
}

$is_mobile = false;
$total_amount = 0;

if ($info) {
    $is_mobile = _String::isMobileNumber($info);
    $qwhere .= ' AND ( '
        . ' customer LIKE '.db_input('%'.$info.'%')
        . ' OR phone_number LIKE '.db_input('%'.$info.'%')

        . ( $is_mobile ? (' OR phone_number LIKE '.db_input('%'._String::convertMobilePhoneNumber($info).'%')) : ' ' )

        . ' OR receipt_code LIKE '.db_input('%'.$info.'%')
        . ' OR note LIKE '.db_input('%'.$info.'%')
        . ' )';
}

if ($agent) {
    $qwhere .= " AND staff_id = ".db_input($agent);
}

if ($status)
    $qwhere .= sprintf(" AND (  status LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $status)."%'");

if ($partner)
    $qwhere .= sprintf(" AND (  partner LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $partner)."%'");

if ($tour && is_numeric($tour) && intval($tour)) {
    $qwhere .= " AND booking_type_id = ".db_input(intval($tour));
} elseif ($tour && is_array($tour) && ( $tour = array_unique(array_filter($tour)) )) {
    $qwhere .= " AND booking_type_id IN (".implode(',', $tour).")";
}

if(isset($group_des) && is_array($group_des))
    if(count($group_des) > 0 )
        $qwhere .= " AND booking_type_id IN (".implode(',', $group_des).")";
    else
        $qwhere .= " AND 1=0 ";

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):null;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):null;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere .= " AND DATE(`created`) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere .=" AND DATE(`created`) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}

$sortOptions=array();
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'created';
//Sorting options...
$order_column = $order = null;
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column ?: '`created`';

if(isset($_REQUEST['order']) && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order ?: 'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by=" $order_column $order ";
///
//Booking::create_table();
//Booking::create_view();
//pagenate

$qs += array('order' => $order);
$qstr = '&amp;'. Http::build_query($qs);
$view_name = Booking::VIEW_NAME;
$sql = " SELECT * FROM $view_name $qwhere ORDER BY $order_by ";
$total = 0;

if (!(isset($_REQUEST['export']) && $_REQUEST['export']))
    $total = db_count("SELECT COUNT(*) FROM ($sql) AS A");

$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('booking.php',$qs);

if (!isset($_REQUEST['export']) || !$_REQUEST['export'])
    $sql .= " LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

$res=db_query($sql);

// get payment log
$res_booking_id = $res;
$booking_ids = [];
while ($res_booking_id && ($row = db_fetch_array($res_booking_id)))
    $booking_ids[] = $row['ticket_id'];
$payemnt_logs = [];
$booking_total_amount = [];
if (is_array($booking_ids) && ( $booking_ids = array_unique(array_filter($booking_ids)) )) {
    $sql_log = " SELECT
          b.ticket_id,
          b.booking_code_trim,
          p.receipt_code,
          p.amount * (IF(p.topic_id = ".THU_TOPIC.", 1, -1)) as amount
        FROM `booking_tmp` b
          JOIN payment_tmp p
            on b.booking_code_trim=p.booking_code_trim
        WHERE b.ticket_id IN (".implode(',', $booking_ids).") ";
    $res_log = db_query($sql_log);
    while ($res_log && ($row = db_fetch_array($res_log))) {
        if (!isset($payemnt_logs[ $row['ticket_id'] ]))
            $payemnt_logs[ $row['ticket_id'] ] = [];

        $payemnt_logs[ $row['ticket_id'] ][] = [
            'receipt_code' => $row['receipt_code'],
            'amount' => $row['amount']
        ];

        if (!empty($row['receipt_code']) && $row['amount'] > 0) {
            if ($is_mobile) {
                $total_amount += $row['amount'];
            }

            if (!isset($booking_total_amount[ $row['ticket_id'] ])) {
                $booking_total_amount[ $row['ticket_id'] ] = 0;
            }

            $booking_total_amount[ $row['ticket_id'] ] += $row['amount'];
        }
    }
}
//
db_data_reset($res);

if (isset($_REQUEST['export']) && $_REQUEST['export'] && $thisstaff->canExportBookings()) {
    require_once(INCLUDE_DIR.'../lib/PHPExcel.php');
    $dir = INCLUDE_DIR.'../public_file/';
    $file_name = 'Booking_list_' . Format::userdate("Y-m-d_H-i-s", time()).".xlsx";

    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
    $cacheSettings = [ 'memoryCacheSize' => '16MB' ];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    $objPHPExcel = new PHPExcel();
    $properties = $objPHPExcel->getProperties();
    $properties->setCreator(NOTIFICATION_TITLE);
    $properties->setLastModifiedBy("System");
    $properties->setTitle(NOTIFICATION_TITLE." - Booking List");
    $properties->setSubject(NOTIFICATION_TITLE." - Booking List");
    $properties->setDescription(NOTIFICATION_TITLE." - Booking List - ".Format::userdate("Y-m-d H:i:s", time()));
    $properties->setKeywords(NOTIFICATION_TITLE." - Booking List");
    $properties->setCategory(NOTIFICATION_TITLE." - Booking List");

    $sheet = $objPHPExcel->getActiveSheet();

    $header = [
        'STT',
        'Mã Booking',
        'Mã Phiếu thu',
        'Khách hàng',
        'Số điện thoại',
        'Email',
        'Nhân viên',
        'Loại Booking',
        'Số lượng',
        'Ngày khởi hành',
        'Đối tác',
        'Giá Bán',
        'Giá Net',
        'Lợi nhuận',
        'Ghi chú',
        'Ngày tạo',
        'Trạng thái',
    ];

    $excel_row = 1;
    $col = 'A';
    foreach ($header as $title) {
        $sheet->setCellValue($col++.$excel_row, $title);
    }
    // Set cell A1 with a string value
    $excel_row = 2;
    if ($res) {
        $count = 1;
        while (($row = db_fetch_array($res))) {
            $dto = new DateTime('@'.$row['dept_date']);
            $dateVal = PHPExcel_Shared_Date::PHPToExcel($dto);
            $objPHPExcel->getActiveSheet()->getStyle('J'.$excel_row)
                ->getNumberFormat()
                ->setFormatCode("dd/mm/yyyy");
            $dto = new DateTime($row['created']);
            $created_dateVal = PHPExcel_Shared_Date::PHPToExcel($dto);
            $objPHPExcel->getActiveSheet()->getStyle('P'.$excel_row)
                ->getNumberFormat()
                ->setFormatCode("dd/mm/yyyy hh:mm");

            $data = [
                $count++,
                strip_tags($row['booking_code']),
                strip_tags($row['receipt_code']),
                strip_tags($row['customer']),
                strip_tags($row['phone_number']),
                strip_tags($row['email']),
                _String::json_decode($row['staff']),
                _String::json_decode($row['booking_type']),
                $row['total_quantity'],
                $dateVal,
                _String::json_decode($row['partner']),
                $row['total_retail_price'],
                $row['total_net_price'],
                $row['total_profit'],
                strip_tags($row['note']),
                $created_dateVal,
                _String::json_decode($row['status'])
            ];
            $sheet->fromArray($data, NULL, 'A'.$excel_row++);
        } //end of while.
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    $objWriter->save($dir.$file_name);

    $objPHPExcel->disconnectWorksheets();
    unset($objWriter);
    unset($objPHPExcel);

    $file = $dir.$file_name;
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }
    exit;
}
//load reservation of leader
$leaderReview = BookingReservation::leaderReview();
$leaderReviewed = BookingReservation::leaderReviewed();

$arr_booking_review = [];
while($leaderReview && ($data = db_fetch_array($leaderReview)))
    $arr_booking_review[] = _String::trimBookingCode($data['booking_code']);
db_data_reset($leaderReview);

if(!empty($arr_booking_review)) {
    $sql = 'select booking_code, customer, phone_number, total_quantity, total_retail_price, booking_type_id, note, staff_id  from '
        . Booking::VIEW_NAME
        . ' where booking_code_trim IN (' . implode(',', db_input($arr_booking_review)) . ')';

    $res_booking_review = db_query($sql);

    while ($res_booking_review && ($row = db_fetch_array($res_booking_review))) {
        $arr_booking_review[$row['booking_code']] = $row;

        $booking_type = $booking_type_list[$row['booking_type_id']]['name']?:'';
        $arr_booking_review[$row['booking_code']]['booking_type'] = $booking_type;
    }
}

$page='booking.inc.php';
$nav->setTabActive('booking', 'booking');
$ost->addExtraHeader('<meta name="tip-namespace" content="booking.booking" />',
    "$('#content').data('tipNamespace', 'booking.booking');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
require('staff.inc.php');
require_once(INCLUDE_DIR . 'class.json.php');
require_once(INCLUDE_DIR . 'class.booking.php');
require_once INCLUDE_DIR . 'class.uuid.php';
require_once INCLUDE_DIR . 'class.reservation.php';
require_once INCLUDE_DIR . 'class.pax.php';
require_once INCLUDE_DIR . 'class.tour-destination.php';

$page = '';
$ticket = $user = $errors = null; //clean start.

$errors = null;
$mess = null;
define('COMMITMENT_FILE', 'commitment_file');
define('CONTRACT_FILE', 'contract_file');

function saveCommitmentFile($file, $ticket_id, &$error, &$mess)
{
    global $thisstaff;

    if ((int)$file['error'] > 0) {
        $error = true;
        $mess[] = 'có lỗi xảy ra khi upload ảnh này!';
        return;
    }

    if (isset($file)) {
        $file_size = $file['size'];
        $file_tmp = $file['tmp_name'];
        $file_ext = strtolower(end(explode('.', $file['name'])));
        $extensions = array("doc", "docx", "pdf", "jpeg", "jpg", "png");

        if (in_array($file_ext, $extensions) === false) {
            $error = true;
            $mess[] = "định dạng không cho phép ";
        }

        if ($file_size > 50 * 1024 * 1024) {
            $error = true;
            $mess[] = 'file phải có kích thước không quá 50 mb';
        }

        $file_name = md5(mt_rand(0, 9999) . time()) . '.' . $file_ext;
        $directory = __dir__ . '/../' . commitment_file . '/';

        if (!is_dir($directory) && !mkdir($directory) && !is_dir($directory)) {
            $error = true;
            $mess[] = 'directory ' . $directory . ' was not created';
        }
        if ($error)
            return;

        $destination = $directory . $file_name;
        $res = move_uploaded_file($file_tmp, $destination);
        if ($res) {
            $new = bookingdocument::create([
                'ticket_id' => $ticket_id,
                'action_id' => bookingdocument::commitment_file_upload,
                'filename' => $file_name,
                'created_by' => $thisstaff->getid(),
                'created_at' => date('y-m-d h:i:s')
            ]);
            $new->save();
        } else {
            $error = true;
            $mess[] = "tải lên tài liệu không thành công!";
        }
    }
}

function saveContractFile($file, $ticket_id, &$error, &$mess)
{
    global $thisstaff;

    if ((int)$file['error'] > 0) {
        $error = true;
        $mess[] = 'Có lỗi xảy ra khi upload ảnh này!';
        return;
    }

    if (isset($file)) {
        $file_size = $file['size'];
        $file_tmp = $file['tmp_name'];
        $file_ext = strtolower(end(explode('.', $file['name'])));
        $extensions = array("doc", "docx", "pdf", "jpeg", "jpg", "png");

        if (in_array($file_ext, $extensions) === false) {
            $error = true;
            $mess[] = "Định dạng không cho phép ";
        }

        if ($file_size > 50 * 1024 * 1024) {
            $error = true;
            $mess[] = 'File phải có kích thước không quá 50 MB';
        }

        $file_name = md5(mt_rand(0, 9999) . time()) . '.' . $file_ext;
        $directory = __DIR__ . '/../' . CONTRACT_FILE . '/';

        if (!is_dir($directory) && !mkdir($directory) && !is_dir($directory)) {
            $error = true;
            $mess[] = 'Directory ' . $directory . ' was not created';
        }
        if ($error)
            return;

        $destination = $directory . $file_name;
        $res = move_uploaded_file($file_tmp, $destination);
        if ($res) {
            $new = BookingDocument::create([
                'ticket_id' => $ticket_id,
                'action_id' => BookingDocument::CONTRACT_FILE_UPLOAD,
                'filename' => $file_name,
                'created_by' => $thisstaff->getId(),
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $new->save();
        } else {
            $error = true;
            $mess[] = "Tải lên tài liệu không thành công!";
        }
    }
}

function downFileDocument($filepath)
{
    // Process download
    if (file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        die();
    }
    http_response_code(404);
    die();
}

function getNameDocument($ticket_id, $type)
{
    $result = BookingDocument::objects()->filter(
        array('ticket_id' => (int)$ticket_id, 'action_id' => $type))->order_by('-created_at')->one();
    if ($result && $result->filename) {
        return $result->filename;
    }
    return null;
}

function bookingReview($data)
{
    $token = REVIEW_ADMIN_TOKEN ?? '';
    $url = 'https://api.review.tugo.com.vn/api/v1/booking-notification';

    $data = [
        "admin_token" => $token,
        "booking_id" => (string) $data['booking_code'],
        "sale_name" => $data['sale_name'],
        "total_amount" => (string) $data['total_amount'],
        "tour_name" => $data['tour_name'],
        "tour_price_per_guest" => (string) $data['tour_price_per_guest'],
        "customer_name" => $data['customer_name'],
        "customer_phone" => (string) $data['customer_phone'],
    ];

    $headers = [
        'Content-Type: application/json',
//        'Cookie: session=9f1a0439-d48a-4ea0-907b-e42fe53c42bb.y1DDheRoalcyREasabJS1NBMTIw'
    ];

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => $headers,
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    if (curl_errno($ch)) {
        return 'Curl error: ' . curl_error($ch);
    } else {
        if (isset($response->message))
            return $response->message;
        else
            return '';
    }

    curl_close($ch);
}

if ($_REQUEST['id']) {
    if (!($ticket = Ticket::lookup($_REQUEST['id'])))
        $errors['err'] = sprintf(__('%s: Unknown or invalid ID.'), __('ticket'));
    elseif (!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err'] = __('Access denied. Contact admin if you believe this is in error');
        $ticket = null; //Clear ticket obj.
    }
    $ticket_id = (int)$_REQUEST['id'];
    $fileNameCommitment = getNameDocument($ticket_id, BookingDocument::COMMITMENT_FILE_UPLOAD);
    $fileNameContract = getNameDocument($ticket_id, BookingDocument::CONTRACT_FILE_UPLOAD);
}
$page = 'view-booking.inc.php';
if ($_REQUEST['action'] && !$errors) {
    $booking_type_list = TourDestination::loadAllDestination();

    switch ($_REQUEST['action']) {
        case 'create':
            if ($_POST) {
                //$_POST['booking_code'] = BOOKING_CODE_PREFIX . UUID::generate('booking_code');
                $_POST['dept_date'] = strtotime($_POST['dept_date']);
                $_POST['fullcoc_date'] = strtotime($_POST['fullcoc_date']);
                $_POST['fullpay_date'] = strtotime($_POST['fullpay_date']);
                $id = Ticket::createBookingtmp($_POST, $errors);

                if (isset($_FILES['commitment_file']) && $_FILES['commitment_file'] && $_FILES['commitment_file']['name'] !== '')
                    saveCommitmentFile($_FILES['commitment_file'], $id, $errors, $mess);

                if (isset($_FILES['contract_file']) && $_FILES['contract_file'] && $_FILES['contract_file']['name'] !== '')
                    saveContractFile($_FILES['contract_file'], $id, $errors, $mes);

                if ($id && !$errors) {
                    $url = $cfg->getUrl() . 'scp/new_booking.php?id=' . $id;

                    if (isset($_POST['reservation_id']) && $_POST['reservation_id'] > 0) {
                        $reservation_id = (int)$_POST['reservation_id'];
                        $reservation = Reservation::lookup($reservation_id);
                        if ($reservation) {
                            $reservation->set('booking_code', $_POST['booking_code']);
                            $reservation->save();
                            $url = $cfg->getUrl() . 'scp/reservation.php?reservation_id=' . $reservation_id; // redirect to reservation
                        }
                    }

                    $message = '';
                    try {
                        $tour = TourDestination::lookup((int)$_POST['booking_type']);
                        $sale = Staff::lookup($_POST['staff']);
                        $booking = BookingView::lookup($id);

                        $data_to_review = [
                            'booking_code' => $_POST['booking_code'],
                            'sale_name' => $booking->staff,
                            'total_amount' => $booking->total_retail_price,
                            'tour_name' => $tour->name,
                            'tour_price_per_guest' => $booking->retailprice1 ?? 0 / $booking->quantity1 ?? 1,
                            'customer_name' => $_POST['customer'],
                            'customer_phone' => $_POST['phone_number'],
                        ];

                        $message = bookingReview($data_to_review);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }

                    FlashMsg::set('ticket_create', 'Tạo booking thành công! '.$message);
                    header('Location: ' . $url);
                    exit;
                }
            }
            if ($_REQUEST['from_view'] && ($_REQUEST['from_view'] === 'reservation') && $_REQUEST['reservation_id']) {
                $data_load = [];

                $reservation = Reservation::lookup((int)$_REQUEST['reservation_id']);

                if ($reservation) {
                    $temp = [
                        'customer_name' => $reservation->customer_name,
                        'phone_number' => $reservation->phone_number,
                        'staff_id' => (int)$reservation->staff_id,
                        'quantity_pax_old' => (int)$reservation->hold_qty + (int)$reservation->sure_qty - (int)$reservation->infant,
                        'quantity_infant' => (int)$reservation->infant,
                    ];
                    $data_load = array_merge($data_load, $temp);
                }

                $tour = TourNew::lookup((int)$reservation->tour_id);
                if ($tour) {
                    $temp = [
                        'destination_id' => (int)$tour->destination,
                        'departure_date' => $tour->departure_date,
                        'retail_price' => $tour->retail_price,
                        'net_price' => $tour->net_price,
                    ];
                    $data_load = array_merge($data_load, $temp);
                }
            }

            $page = 'create-booking.inc.php';
            break;
        case 'edit':
            if ($_POST) {
                $_POST['dept_date'] = strtotime($_POST['dept_date']);
                $_POST['fullcoc_date'] = strtotime($_POST['fullcoc_date']);
                $_POST['fullpay_date'] = strtotime($_POST['fullpay_date']);
                $id = $ticket->new_updateBooking($_POST, $errors);

                if (isset($_FILES['commitment_file']) && $_FILES['commitment_file'] && $_FILES['commitment_file']['name'] !== '')
                    saveCommitmentFile($_FILES['commitment_file'], $id, $errors, $mess);

                if (isset($_FILES['contract_file']) && $_FILES['contract_file'] && $_FILES['contract_file']['name'] !== '')
                    saveContractFile($_FILES['contract_file'], $id, $errors, $mes);

                if ($id && !$errors) {
                    $url = $cfg->getUrl() . 'scp/new_booking.php?id=' . $id . '&action=edit';
                    FlashMsg::set('ticket_create', 'Cập nhật booking thành công!');
                    header('Location: ' . $url);
                    exit;
                }
            }
            $page = 'edit-booking.inc.php';
            break;
        case 'down_commitment':
            if (isset($fileNameCommitment) && $fileNameCommitment) {
                $filePath = __DIR__ . '/../' . COMMITMENT_FILE . '/' . $fileNameCommitment;
                downFileDocument($filePath);
            } else {
                echo 'FILE NOT FOUND';
                exit();
            }
            break;
        case 'down_contract':
            if (isset($fileNameContract) && $fileNameContract) {
                $filePath = __DIR__ . '/../' . CONTRACT_FILE . '/' . $fileNameContract;
                downFileDocument($filePath);
            } else {
                echo 'FILE NOT FOUND';
                exit();
            }
            break;
        default:
            break;
    }
}
$nav->setTabActive('booking');
$nav->setActiveSubMenu(0, 'booking');
require_once(STAFFINC_DIR . 'header.inc.php');
require_once(STAFFINC_DIR . $page);
require_once(STAFFINC_DIR . 'footer.inc.php');

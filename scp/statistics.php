<?php
/*************************************************************************
    statistics.php

    thong ke nhung ticket agent duoc assign & trang thai hien tai cua ticket trong 1 khoang thoi gian xac dinh

    Minh Bao <beckerbao29@gmail.com>    
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.log.php');

if(!defined('OSTSCPINC') || !$thisstaff || !@$thisstaff->isStaff()) die('Access Denied');
//Navigation
$nav->setTabActive('dashboard');
$total = 0;
$page = isset($_REQUEST['p']) && is_numeric($_REQUEST['p']) ? intval($_REQUEST['p']) : 1;
$pagelimit = PAGE_LIMIT;
$offset = ($page-1)*$pagelimit;
$params = [];

if (isset($_REQUEST['staff_id']) && is_numeric($_REQUEST['staff_id'])) {
    $params['staff_id'] = intval(trim($_REQUEST['staff_id']));
}

if (isset($_REQUEST['action_id']) && is_numeric($_REQUEST['action_id'])) {
    $params['action_id'] = intval(trim($_REQUEST['action_id']));
}


if (isset($_REQUEST['customer_name']) && !empty($_REQUEST['customer_name'])) {
    $params['customer_name'] = trim($_REQUEST['customer_name']);
}

if (isset($_REQUEST['phone_number']) && !empty($_REQUEST['phone_number'])) {
    $params['phone_number'] = trim($_REQUEST['phone_number']);
}

if (isset($_REQUEST['from']) && date_create_from_format('Y-m-d', $_REQUEST['from'])) {
    $params['from'] = trim($_REQUEST['from']);
} else {
    $params['from'] = $_REQUEST['from'] = date('Y-m-d', time()-31*3600*24);
}

if (isset($_REQUEST['to']) && date_create_from_format('Y-m-d', $_REQUEST['to'])) {
    $params['to'] = trim($_REQUEST['to']);
} else {
    $params['to'] = $_REQUEST['to'] = date('Y-m-d', time());
}


$year_from = explode('-', $_REQUEST['from']);
$year_from = $year_from[0];
$year_to = explode('-', $_REQUEST['to']);
$year_to = $year_to[0];

$two_year = false;
if ($year_from != $year_to) {
    $two_year = true;
}

switch ($_REQUEST['group']) {
    case 'week':
        define('DATE_FORMAT', 'W-Y');
        break;
    case 'month':
        define('DATE_FORMAT', 'm-Y');
        break;
    case 'day':
    default:
        if ($two_year)
            define('DATE_FORMAT', 'Y-M-d D');
        else
            define('DATE_FORMAT', 'M-d D');
        break;
}

switch ($_REQUEST['group']) {
    case 'week':
    case 'month':
        function ___date_compare($a, $b) {
            $a = explode('-', $a);
            $b = explode('-', $b);

            if ($a[1] > $b[1]) return 1;
            if ($a[1] < $b[1]) return -1;
            return $a[0] - $b[0];
        }
        break;
    case 'day':
    default:
        function ___date_compare($a, $b) {
            $_a = date_create_from_format(DATE_FORMAT, $a)->getTimestamp();
            $_b = date_create_from_format(DATE_FORMAT, $b)->getTimestamp();

            return $_a - $_b;
        }
        break;
}

$res = LogAction::getPagination($params, $offset, $total, $pagelimit);

$all_res = [];
$users = [];
$users_id = [];
$ticket_id = [];
$thread_id = [];
$threads = [];
$ticket_numbers = [];
while($res && ($row = db_fetch_array($res))) {
    $users_id[] = $row['user_id'];
    $ticket_id[] = $row['ticket_id'];
    $thread_id[] = $row['thread_id'];
    $all_res[] = $row;
}

$users_id = array_filter(array_unique($users_id));
if ($users_id) {
    $sql = 'SELECT DISTINCT u.id, u.name, v.value FROM ost_user u 
                JOIN ost_form_entry e ON e.object_id=u.id AND e.object_type LIKE \'U\'
                JOIN ost_form_entry_values v ON v.entry_id=e.id AND v.field_id='.USER_PHONE_NUMBER_FIELD.' 
                WHERE u.id IN ('.implode(',', $users_id).')';
    $res = db_query($sql);
    while ($res && ($row = db_fetch_array($res))) {
        $users[ $row['id'] ] = [
            'name' => $row['name'],
            'phone_number' => $row['value'],
        ];
    }
}

$ticket_id = array_filter(array_unique($ticket_id));
if ($ticket_id) {
    $sql = 'SELECT DISTINCT ticket_id, number FROM ost_ticket WHERE ticket_id IN ('.implode(',', $ticket_id).')';
    $res = db_query($sql);
    while ($res && ($row = db_fetch_array($res))) {
        $ticket_numbers[ $row['ticket_id'] ] = $row['number'];
    }
}

$thread_id = array_filter(array_unique($thread_id));
if ($thread_id) {
    $sql = 'SELECT DISTINCT id, body FROM ost_ticket_thread WHERE id IN ('.implode(',', $thread_id).')';
    $res = db_query($sql);
    while ($res && ($row = db_fetch_array($res))) {
        $threads[ $row['id'] ] = $row['body'];
    }
}

$list_staff= Staff::getAllUsername();
$all_staff = [];

$StaffNumberTicketSummaryTable = LogAction::getStaffNumberTicketSummaryTable($params);
$StaffNumberTicketChart = LogAction::getStaffNumberTicketChart($params);
$StaffActivitySummaryTable = LogAction::getStaffActivitySummaryTable($params);
$StaffActivityChart = LogAction::getStaffActivityChart($params);
$ActivitySummaryTable = LogAction::getActivitySummaryTable($params);
$ActivityChart = LogAction::getActivityChart($params);
// ------------------------------------------------------ //
// ------------------------------------------------------ //

$StaffNumberTicketChart_data = [];
$Agents = [];
while($StaffNumberTicketChart && ($row = db_fetch_array($StaffNumberTicketChart))) {
    $StaffNumberTicketChart_data[ $row['username'] ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
    $Agents[] = $row['username'];
}

if ($StaffNumberTicketChart_data) {
    for ($date = $_REQUEST['from'];
            strtotime($date) <= strtotime($_REQUEST['to']);
            $date = date('Y-m-d', strtotime($date) + 24 * 3600)) {

        foreach ($Agents as $_agent) {
            if (!isset($StaffNumberTicketChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ])) {
                $StaffNumberTicketChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ] = 0;
            }
        }
    }
}


foreach ($StaffNumberTicketChart_data as $_key => $_data) {
    uksort($StaffNumberTicketChart_data[ $_key ], "___date_compare");
}

// ------------------------------------------------------ //
// ------------------------------------------------------ //
//
$StaffActivityChart_data = $StaffActivityChart_date = $Agents = [];
while($StaffActivityChart && ($row = db_fetch_array($StaffActivityChart))) {
    $date_string = date(DATE_FORMAT, strtotime($row['date']));
    $StaffActivityChart_data[ $row['username'].'/'.LogAction::getName($row['action_id']) ][ $date_string ] = $row['total'];

    $Agents[] = $row['username'].'/'.LogAction::getName($row['action_id']);
}

$Agents = array_unique($Agents);

if ($StaffActivityChart_data) {
    for ($date = $_REQUEST['from'];
            strtotime($date) <= strtotime($_REQUEST['to']);
            $date = date('Y-m-d', strtotime($date) + 24 * 3600)) {

        $StaffActivityChart_date[] = $date;
        foreach ($Agents as $_agent) {
            if (!isset($StaffActivityChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ])) {
                $StaffActivityChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ] = 0;
            }
        }
    }
}

foreach ($StaffActivityChart_data as $_key => $_data) {
    uksort($StaffActivityChart_data[ $_key ], "___date_compare");
}

// ------------------------------------------------------ //
// ------------------------------------------------------ //

$ActivityChart_data = $ActivityChart_date = $Agents = [];
while($ActivityChart && ($row = db_fetch_array($ActivityChart))) {
    $date_string = date(DATE_FORMAT, strtotime($row['date']));
    $ActivityChart_data[ LogAction::getName($row['action_id']) ][ $date_string ] = $row['total'];
    $Agents[] = LogAction::getName($row['action_id']);
}
$Agents = array_unique($Agents);

if ($ActivityChart_data) {
    for ($date = $_REQUEST['from'];
         strtotime($date) <= strtotime($_REQUEST['to']);
         $date = date('Y-m-d', strtotime($date) + 24 * 3600)) {

        $ActivityChart_date[] = $date;
        foreach ($Agents as $_agent) {
            if (!isset($ActivityChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ])) {
                $ActivityChart_data[ $_agent ][ date(DATE_FORMAT, strtotime($date)) ] = 0;
            }
        }
    }
}
foreach ($ActivityChart_data as $_key => $_data) {
    uksort($ActivityChart_data[ $_key ], "___date_compare");
}

// ------------------------------------------------------ //
// ------------------------------------------------------ //

$pageNav = new Pagenate($total,$page,$pagelimit);
$pageNav->setURL('statistics.php', $params);

$showing = $total ? ' &mdash; '.$pageNav->showing():"";

$ACTION = LogAction::ACTIONS;
ksort($ACTION);

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.'agent-statistics.inc.php');
require_once(STAFFINC_DIR.'footer.inc.php');
?>
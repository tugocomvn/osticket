<?php
include_once INCLUDE_DIR.'class.general-object.php';

class TObject extends GeneralObject {
    const TICKET = 1;
    const BOOKING = 2;
    const PAYMENT_BOOKING_CODE = 3;
}

class ObjectUpdate {
    public static function update($object_id, $object_type_id, $value) {
        global $thisstaff;
        $sql = sprintf(
            "REPLACE INTO object_update SET 
            object_id=%s, 
            object_type_id=%s, 
            update_value=%s, 
            created_at=%s,
            created_by=%s",
            db_input($object_id),
            db_input($object_type_id),
            db_input($value),
            'NOW()',
            $thisstaff ? db_input($thisstaff->getId()) : null
        );

        return db_query($sql);
    }

    public static function getValue($object_id, $object_type_id) {
        $sql = "SELECT * FROM object_update WHERE object_id=".db_input($object_id)." AND object_type_id=".db_input($object_type_id)." LIMIT 1";
        $res = db_query($sql);
        if (!$res) return null;
        return db_fetch_array($res);
    }

    public static function approve($object_id, $object_type_id) {
        global $thisstaff;
        $sql = sprintf(
            "UPDATE object_update SET 
            approved_at=%s,
            approved_by=%s
            WHERE object_id=%s AND 
            object_type_id=%s",
            'NOW()',
            $thisstaff ? db_input($thisstaff->getId()) : null,
            db_input($object_id),
            db_input($object_type_id)
        );

        return db_query($sql);
    }
}

(function($) {
    $('#body').trumbowyg({
        tabindex: 2
    });


    document.addEventListener('keydown', function(event) {
        if (event.metaKey && event.which === 13) {
            console.log('OK');
            send();
        }
    });

    $('#send_btn').on('click', send);

    function send() {
        var to = 'support@tugo.com.vn';
        var title = $('#title').val().trim(),
            hn = $('#hn').prop('checked'),
            body = $('#body').trumbowyg('html');
            __CSRFToken__ = $('[name=__CSRFToken__]').val();

        if (hn) to = 'support-hn@tugo.com.vn';

        if (!title || !body || !to || !__CSRFToken__) return false;

        $('.list').prepend('<li class="new">'+title+' ...sending...</li>');

        $('#title').val('')
        $('#body').trumbowyg('empty');
        $('#title').focus();
        $('#hn').prop('checked', false);

        $.post('/scp/send.php', {
            title: title,
            body: body,
            to: to,
            __CSRFToken__: __CSRFToken__
        }, function(data) {
            if (data && data.length === 40) {
                $('[name=__CSRFToken__]').val(data);
                $('.new').text( $('.new').text().replace(' ...sending...', '') ).removeClass('new');
            }
        });
    }
})(jQuery);


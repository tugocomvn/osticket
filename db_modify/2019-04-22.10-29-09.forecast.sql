create table booking_type_list
(
    id int not null,
    name varchar(64) null,
    country int null,
    land_rate decimal null comment 'price per pax',
    flight_rate decimal null comment 'price per pax',
    paid_date tinyint null comment 'days',
    constraint booking_type_list_pk
        primary key (id)
);

create index booking_type_list_name_index
    on booking_type_list (name);

create index booking_type_list_paid_date_index
    on booking_type_list (land_date);

alter table booking_type_list modify land_rate decimal(10,2) null comment 'price per pax';

alter table booking_type_list modify flight_rate decimal(10,2) null comment 'price per pax';


create table booking_paid_forecast
(
    pay_date date not null,
    land decimal(12,2) null,
    flight decimal(12,2) null,
    constraint booking_paid_forecast_pk
        primary key (pay_date)
);

alter table booking_paid_forecast
    add booking_type_id int null after pay_date;

create index booking_paid_forecast_booking_type_id_index
    on booking_paid_forecast (booking_type_id);

create index booking_paid_forecast_pay_date_index
    on booking_paid_forecast (pay_date);

alter table booking_paid_forecast drop key pay_date;

alter table booking_paid_forecast
    add constraint pay_date
        primary key (pay_date, booking_type_id);

alter table booking_paid_forecast
    add qty int null;

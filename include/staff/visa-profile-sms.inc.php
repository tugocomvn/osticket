<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/visa-tour-guest.php"?>">Back</a></p>
<?php if(isset($mess_error)):?>
    <p class="mt-1"><span class="label label-danger"><?php echo $mess_error?></span></>
<?php endif; ?>
<h2>Visa - Send SMS to Passenger</h2>
<form action="<?php echo $cfg->getUrl().'scp/visa-tour-guest.php?id='.$_REQUEST['id'].'&tour_id='.$_REQUEST['tour_id'].'&layout=sms&send=send'?>" method="POST">
    <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
        <input type="hidden" name="tour" value="<?php echo $_REQUEST['tour_id']; ?>">
        <input type="hidden" name="tour_pax_visa_profile_id" value="<?php if(isset($paxInfo['tour_pax_visa_profile_id']) && $paxInfo['tour_pax_visa_profile_id'])
                                                            echo $paxInfo['tour_pax_visa_profile_id'];
                                                        else echo 0; ?>">
        <?php if(isset($mess) && is_array($mess)):?>
            <?php foreach ($mess as $value): ?>
                <div id="warning"><?php echo $value ?></div>
            <?php endforeach; ?>
        <?php endif;?>

    <div class="pull-right">
        <p >
            <span class="label label-info">TG123-<?php echo _String::trimBookingCode($tour_datas['booking_code']) ?></span>
            &bull; <?php echo $tour->name; ?>
        </p>
        <p><?php
            echo $booking_type_list[(int)$tour->destination]['name']?:'';
            echo ' - ';
            if($tour->gateway_present_time) echo date('d/m/Y', strtotime($tour->gateway_present_time)) ?>
        </p>
    </div>
    <p><big><?php echo $tour_datas['full_name'] ?></big></p>
    <div class="clearfix"></div>
    <ul class="tabs" id="myTab">
        <li><a href="#appointment" class="active">
                <i class="icon-comment"></i> Cuộc hẹn</a></li>
        <li><a href="#history">
                <i class="icon-list"></i> Lịch sử</a></li>
    </ul>
    <div id="appointment" class="tab_content">
        <table>
            <tr>
                <td>
                    <h3 class="centered">Tạo cuộc hẹn</h3>
                </td>
                <td>
                    <h3 class="centered">Tin nhắn</h3>
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <table border="0" cellspacing="" cellpadding="4" width="100%">
                        <tbody>
                        <tr>
                            <td>Số điện thoại</td>
                            <td>
                                <input name="phone_number" class="input-field" type="text"
                                       value=" <?php echo _String::formatPhoneNumber($tour_datas['phone_number']) ?>"
                                       readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Cuộc hẹn</td>
                            <td>
                                <select name="id-visa-appointment" id="content_appointment" required>
                                    <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                                    <?php while ($listVisaAppointment && ($row = db_fetch_array($listVisaAppointment))):
                                        echo sprintf('<option value="%d" >%s</option>', $row['id'], $row['name']);
                                    endwhile;
                                    db_data_seek($listVisaAppointment, 0); ?></select>
                            </td>
                        </tr>
                        <tr>
                            <td>Ngày</td>
                            <td>
                                <input placeholder="Ngày" type="text" class="input-field dp"
                                       name="date-appointment" id="date_appointment"
                                       value="<?php echo date('d/m/Y', time() + 1 * 24 * 3600) ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Giờ:</td>
                            <td>
                                <input type="time" name="time-appointment" id="time_appointment" required
                                       value="<?php echo date('H:i', time()) ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Ghi chú</td>
                            <td>
                                <textarea class="input-field" cols="40" rows="4" type="text" id="note_appointment"
                                  name="note-appointment" value=""></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td width="50%">
                    <table width="100%">
                        <tbody>
                        <tr>Đây là nội dung tin nhắn được tạo tự động, bạn có thể gửi cho khách</tr>
                        <tr>
                            <td><textarea class="input-field" id="sms" maxlength="320" minlength="10" name="sms_content"
                                          style="width:90%" cols="100" rows="7" required></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        <tr>
            <td>
                <p class="centered">
                    <button type="submit" class="btn_sm btn-primary submit_btn" name="save_appointment" value="save_appointment">Lưu cuộc hẹn</button>
                </p>
            </td>
            <td>
                <p class="centered">
                    <button type="submit" class="btn_sm btn-primary submit_btn" name="send_sms" value="send_sms">Gửi tin nhắn</button> và lưu cuộc hẹn
                </p>
            </td>
        </tr>
    </table>
    </div>
    <div id="history" class="tab_content" style="display: none">
        <div style="width: 50%" class="pull-left">
            <h3 class="centered">Cuộc hẹn</h3>
            <?php
            $paxInfo = VisaTourGuest::getPaxFromId($_REQUEST['id'], $_REQUEST['tour_id']);
            $paxInfo = db_fetch_array($paxInfo);
            $id = $paxInfo['tour_pax_visa_profile_id'];
            $appointment_pax_list = \Tugo\VisaProfileAppointment::getAppointmentPax($id); ?>
            <table class="list" border="0" cellspacing="" cellpadding="4" style="width: 90%; margin: auto;">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Thời gian</th>
                        <th>Cuộc hẹn</th>
                        <th>Ghi chú</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; while ($appointment_pax_list && ($row = db_fetch_array($appointment_pax_list))): ?>
                        <tr>
                            <td><?php echo ++$i ?></td>
                            <td>
                                <?php echo date("d/m/Y H:i", strtotime($row['date_event'])) ?>
                            </td>
                            <td>
                                <?php echo \Tugo\VisaAppointment::lookup($row['visa_appointment_id'])->name; ?>
                            </td>
                            <td>
                                <?php echo $row['note'] ?>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
        <div style="width: 50%" class="pull-left">
            <h3 class="centered"> Tin nhắn đã gửi</h3>
            <?php if (db_num_rows($profile_sms_logs) > 0): ?>
                <table class="list" border="0" cellspacing="1" cellpadding="1" style="width: 90%; margin: auto;">
                    <tr>
                        <th>STT</th>
                        <th>Time</th>
                        <th>Staff</th>
                        <th>SMS Content</th>
                    </tr>
                    <?php $i = 1; ?>
                <?php while ($profile_sms_logs && ($data = db_fetch_array($profile_sms_logs))): ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td>
                            <?php echo date("d/m/Y H:i", strtotime($data['send_time'])) ?>
                        </td>
                        <td>
                            <?php $name_staff = Staff::lookup($data['created_by']); ?>
                            <?php echo $name_staff; ?>
                        </td>
                        <td><?php echo $data['content']; ?></td>
                    </tr>
                <?php endwhile; ?>
                </table>
            <?php else: ?>
                <span>Không có lịch sử nào</span>
            <?php endif; ?>
        </div>
    </div>

    <div class="clearfix"></div>
</form>
<script>
    var appointment = '';
    var day = '';
    var time = '';
    var note = '';
    var name = '';
    var tour = '';

    $(document).ready(function(){
        day = $("#date_appointment").val();
        time = $("#time_appointment").val();
        name = "<?php echo $tour_datas['full_name'] ?>";
        tour = "<?php echo $booking_type_list[(int)$tour->destination]['name']?:''.' - '; ?>"
        setTextSMS();
    });

    function setTextSMS(){
        let content = "<?php echo SMS_APPOINTMENT_START ?>";
        content += name;
        content += ' lich hen ' + appointment;

        if(tour !== '')
            content += '(tour:' + tour + ')';

        content += ' vao luc '+ time;
        content += ' ngay '+ day;

        if(note !== ''){
            content += ' (' + note + ')';
        }

        $("#sms").val(content);
    }

    $("select#content_appointment").change(function(){
        appointment = $("select#content_appointment option:checked").html();
        setTextSMS();
    });

    $("#date_appointment").change(function(){
        day = $("#date_appointment").val();
        setTextSMS();
    });

    $("#time_appointment").change(function(){
        time = $("#time_appointment").val();
        setTextSMS();
    });

    $("textarea#note_appointment").change(function(){
        note = $("textarea#note_appointment").val();
        setTextSMS();
    });

    $('.submit_btn').off('click').on('click', function(e) {
        if (!confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {
            e.preventDefault();
            return false;
        }
    });

</script>

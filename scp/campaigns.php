<?php 
include ("request_curl.php"); 
 
if(isset($_POST['submit'])){ 
    $schedule_time = $_POST['schedule_time']; 
    if(!empty($schedule_time)){ 
        $apiKey = '7090ec095d3256042a961b4b799c7895-us16'; 
        $campaign_id = '093f3e102d'; 
        $data_center = substr($apiKey,strpos($apiKey,'-')+1); 
 
        $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/campaigns/'. $campaign_id .'/actions/schedule'; 
 
        $json = json_encode([ 
            'schedule_time' => $schedule_time 
        ]); 
        RequestUrl::CallCurl($url,$apiKey,$json); 
    } 
} 
?>
 <?php
require('admin.inc.php');
//include_once(INCLUDE_DIR.'class.template.php');
if($_POST){
    switch(strtolower($_POST['do'])){
        case 'save':
            if($_POST && !$errors) {
                if($cfg && $cfg->updateSettings($_POST,$errors)) {
                    $msg = 'Successfully updated';
                } elseif(!$errors['err']) {
                    $errors['err']=__('Unable to update settings - correct errors below and try again');
                }
            }
            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
}
$page='contract-template.inc.php';
$tip_namespace = 'contract.template';
$content = '';
if ($cfg)
    $content = $cfg->get('contract_content');
$nav->setTabActive('emails');
$ost->addExtraHeader('<meta name="tip-namespace" content="' . $tip_namespace . '" />',
                     "$('#content').data('tipNamespace', '".$tip_namespace."');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');

?>
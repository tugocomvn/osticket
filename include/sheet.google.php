<?php
require_once __DIR__.'/../vendor/autoload.php';

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-php-quickstart.json
define('SCOPES', implode(' ', array(
    Google_Service_Sheets::SPREADSHEETS
)));

class GoogleSheet {
    private $service = null;
    private $client = null;
    private $spreadsheetId = null;
    private $credentials_path = null;

    function __construct($spreadsheetId, $credentials_path) {
        $this->spreadsheetId = $spreadsheetId;
        $this->credentials_path = $credentials_path;
    }

    function getService() {
        if (!is_null($this->service))
            return $this->service;

        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);
        $this->service = $service;
        return $this->service;
    }

    function getClient() {
        if (!is_null($this->client))
            return $this->client;

        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH_BOOKING);
        $client->setRedirectUri("http://".$_SERVER["HTTP_HOST"].$_SERVER['PHP_SELF'].'?a=booking');
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->expandHomeDirectory($this->credentials_path);
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            if (isset($_GET['code'])) { // we received the positive auth callback, get the token and store it in session
                $authCode = trim($_GET['code']);
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                if(!file_exists(dirname($credentialsPath))) {
                    mkdir(dirname($credentialsPath), 0700, true);
                }
                file_put_contents($credentialsPath, json_encode($accessToken));
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                header('Location: ' . $authUrl);
                exit;
            }
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }

        $this->client = $client;
        return $this->client;
    }

    /**
     * Expands the home directory alias '~' to the full path.
     * @param string $path the path to expand.
     * @return string the expanded path.
     */
    function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }

    function push($sheetname, $range, $values) {
        $range = $sheetname.'!'.$range;
        $service = $this->getService();
        $body = new Google_Service_Sheets_ValueRange(array('values' => $values));
        $params = array('valueInputOption' => "USER_ENTERED");
        $result = $service->spreadsheets_values->update($this->spreadsheetId, $range, $body, $params);
        return $result;
    }

    function get($sheetname, $range) {
        $range = $sheetname.'!'.$range;
        $service = $this->getService();
        $response = $service->spreadsheets_values->get($this->spreadsheetId, $range);
        $values = $response->getValues();
        return $values;
    }
}



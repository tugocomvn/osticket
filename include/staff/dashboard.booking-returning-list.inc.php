<?php
include_once INCLUDE_DIR.'class.ticket-analytics.php';

if (!$_REQUEST['from'])
    $_REQUEST['from'] = date('Y-m-d', time()-24*3600*30);
if (!$_REQUEST['to'])
    $_REQUEST['to'] = date('Y-m-d');

$res = TicketAnalytics::bookingReturingPhoneNumber($_REQUEST['from'], $_REQUEST['to']);
?>
<h2>Detail list</h2>
<p>Bookings return in this time span</p>
<div style="height: 500px; overflow: scroll;">
    <table class="table list">
        <thead>
        <tr>
            <th>#</th>
            <th>Phone Number</th>
            <th>Count</th>
            <th>Booking Code</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = 0;
        while ($res && ($row = db_fetch_array($res))) {
            $codes = explode(',', $row['booking_code']);
            $dates = explode(',', $row['date']);
            $new_date = [];
            if (is_array($codes) && is_array($dates)) {
                foreach ($dates as $i => $date) {
                    $new_date[ $date ] = $codes[$i];
                }
            }

            if (is_array($codes) && is_array($new_date)) {
                $rowspan = count($new_date);
                $check = $rowspan;
                foreach ($new_date as $_date => $_code) {
                    ?>
                    <tr>
                        <?php if ($check == $rowspan): $check--; ?>
                            <td rowspan="<?php echo $rowspan ?>"><?php echo (++$count) ?></td>
                            <td rowspan="<?php echo $rowspan ?>"><?php echo $row['phone_number'] ?></td>
                            <td rowspan="<?php echo $rowspan ?>"><?php echo $rowspan ?></td>
                        <?php endif; ?>

                        <td><?php echo $_code ?></td>
                        <td><?php echo $_date ?></td>
                    </tr>
                    <?php
                }
            }
        }
        ?>
        </tbody>
    </table>

</div>

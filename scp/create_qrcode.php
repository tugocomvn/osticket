<?php
require_once __DIR__."/../vendor/autoload.php";

use Endroid\QrCode\QrCode;

$token = '12345';
$url = 'https://www.google.com.vn?token='.$token;

$qrCode = new QrCode($url);

// Save it to a file
$qrCode->save(__DIR__.'/qrcode.png');

?>
<img src="./qrcode.png" alt="qrcode">

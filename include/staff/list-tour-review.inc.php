<?php 
if (!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<style>
    td br {
        margin-bottom: 0.5em;
        display: block;
        content: "";
    }

    table.list tbody td, table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }

    table.list {
        margin-left: -100px;
    }
</style>
<h2><?php echo __('List Tour Review');?></h2>
<div class="clearfix"></div>
    <table class="list" border="0" cellspacing="1" cellpadding="2" width="1258">
        <caption><?php echo $pageNav->showing() ?></caption>
        <thead>
            <tr>
                <th>No.</th>
                <th>Mã Vé</th>
                <th>Ngày bay đi</th>
                <th>Ngày bay về</th>
                <th>Số lượng khách</th>
            </tr>
        </thead>
        <tbody>
        <?php $no = $offset; ?>
            <?php while($results && ($row = db_fetch_array($results))): ?>
                <tr>
                    <td><?php echo ++$offset; ?></td>
                    <td><?php if(isset($row['flight_ticket_code'])) echo $row['flight_ticket_code'] ?></td>
                    <td><?php if(isset($row['departure_at'])) echo date('d/m/Y H:i', strtotime($row['departure_at'])) ?></td>
                    <td><?php if(isset($row['arrival_at'])) echo date('d/m/Y H:i', strtotime($row['arrival_at'])) ?></td>
                    <td><?php if(isset($row['total_quantity'])) echo $row['total_quantity'] ?></td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
<div> Page:
    <?php echo $pageNav->getPageLinks(); ?>
</div>;
<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canViewReceiptList()) die('Access Denied');
?>
<style>
    td br {
        margin-bottom: 0.5em;
        display: block;
        content: "";
    }

    table.list tbody td, table.list thead th {
        padding-top: 0.75em;
        padding-bottom: 0.75em;
        vertical-align: middle;
    }

    table.list {
        margin-left: -100px;
    }

    td, td span, td p {
        word-break: break-word;
    }
</style>
<h2><?php echo __('Receipt List');?></h2>
<div class="clearfix"></div>
    <form action="<?php echo $cfg->getUrl() ?>scp/receiptlist.php" method="get">
        <div style="padding-left:2px;">
            <table>
                <tr>
                    <td><b><?php echo __('Mã Phiếu Thu'); ?></b></td>
                    <td><input type="text" class="input-field" name="receipt_code"
                            value="<?php if(isset($_REQUEST['receipt_code']))
                                echo $_REQUEST['receipt_code'] ?>" placeholder="TGF123456"></td>
                    <td><b><?php echo __('Mã Booking'); ?></b></td>
                    <td><input type="text" class="input-field" name="booking_code_trim"
                            value="<?php if(isset($_REQUEST['booking_code_trim']))
                                echo $_REQUEST['booking_code_trim'] ?>"></td>
                </tr>
                <tr>
                    <td><?php echo __('Tên khách'); ?></td>
                    <td><input type="text" class="input-field" name="customer_name"
                            value="<?php if(isset($_REQUEST['customer_name']))
                                echo $_REQUEST['customer_name'] ?>"></td>
                    <td><?php echo __('Số điện thoại khách'); ?></td>
                    <td><input type="text" class="input-field" name="customer_phone_number"
                            value="<?php if(isset($_REQUEST['customer_phone_number']))
                                echo $_REQUEST['customer_phone_number'] ?>"></td>
                </tr>
                <tr>
                <td><?php echo __('Tên tour'); ?></td>
                    <td><input type="text" class="input-field" name="tour_name"
                            value="<?php if(isset($_REQUEST['tour_name']))
                                echo $_REQUEST['tour_name'] ?>">
                    </td>
                    <td>Hình thức thanh toán</td>
                    <td>
                        <select name='payment_method'>
                            <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                            <?php foreach($payment_method_list as $key => $value): ?>
                                <option value="<?php echo $key ?>"
                                    <?php if (!empty($_REQUEST['payment_method']) && $_REQUEST['payment_method'] == $key)
                                        echo "selected" ?>
                                ><?php echo $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Nhân viên thu tiền'); ?></td>
                    <td>
                        <select name='staff_cashier' class="staff">
                            <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                            <?php while ($finance_staff_list && ($row = db_fetch_array($finance_staff_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'],($_REQUEST['staff_cashier'] == $row['staff_id']?'selected="selected"':''),$row['name']);
                            }?>
                        </select>
                    </td>
                    <td><?php echo __('Số điện thoại nhân viên thu tiền'); ?></td>
                    <td><input type="text" class="input-field" name="staff_cashier_phone_number"
                            value="<?php if(isset($_REQUEST['staff_cashier_phone_number']))
                                echo $_REQUEST['staff_cashier_phone_number'] ?>"></td>
                </tr>
                <tr>
                    <td>Nhân viên kinh doanh</td>
                    <td>
                        <select name='staff_business' class="staff" >
                            <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                            <?php while ($staff_business_list && ($row = db_fetch_array($staff_business_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'],($_REQUEST['staff_business'] == $row['staff_id']?'selected="selected"':''),$row['name']);
                            }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Created Date'); ?> - <?php echo __('Between'); ?></td>
                    <td>
                        <input class="dp input-field" size=15 name="from"
                            value="<?php if (strtotime($_REQUEST['from']))
                                echo date('d/m/Y', strtotime($_REQUEST['from'])); ?>"
                            autocomplete=OFF>
                        <input class="dp input-field" size=15 name="to"
                            value="<?php if (strtotime($_REQUEST['to']))
                                echo date('d/m/Y', strtotime($_REQUEST['to'])); ?>"
                            autocomplete=OFF>
                    </td>
                    <td>Trạng thái</td>
                    <td>
                        <select name='receipt_status'>
                            <option value>&mdash; <?php echo __('All'); ?> &mdash;</option>
                            <?php foreach($receipt_status_list as $key => $value): ?>
                                <option value="<?php echo $key ?>"
                                    <?php if (!empty($_REQUEST['receipt_status']) && $_REQUEST['receipt_status'] == $key)
                                        echo "selected" ?>
                                ><?php echo $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" class="btn_sm btn-primary"
                            value="<?php echo __('Search');?>" />
                        <input type="reset" class="btn_sm btn-default"
                            value="<?php echo __('Reset');?>"
                            onclick="reset_form(this)" />
                    </td>
                </tr>
                <script>
                    function reset_form(event) {
                        console.log(event);
                        form = $(event).parents('form');
                        form.find('input[type="text"], select, .hasDatepicker').val('');
                        form.submit();
                    }
                </script>
            </table>
        </div>
    </form>
    <table class="list" border="0" cellspacing="1" cellpadding="2" width="1258">
        <caption>
            <?php echo $pageNav->showing() ?>
            <br><br>
            <?php echo "Tổng Tiền: ".number_format($sum_amount_received) ?>
        </caption>
        <thead>
            <tr>
                <th>No.</th>
                <th>Mã Receipt</th>
                <th>Mã Booking</th>
                <th>Tour</th>
                <th style="width: 15em;">Name/Phone</th>
                <th>Số Khách</th>
                <th>Thu vào</th>
                <th>Ngày Tạo</th>
                <th>Trạng Thái</th>
                <th style="width: 10em;">Internal Notes</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = $offset; ?>
                <?php while($results && ($row = db_fetch_array($results))): ?>
                    <tr>
                        <td><?php echo ++$offset; ?></td>
                        <td>
                            <a class="no-pjax" target="_blank"
                                href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=view">
                            <?php echo $row['receipt_code']; ?></a>
                            <br>
                            <?php $payment_check = Payment::lookup(['receipt_code'=>$row['receipt_code']]);
                            if ($row['status'] == Receipt::APPROVED && $thisstaff->canCreatePayments() && !$payment_check): ?>
                                <a class="btn_sm btn-xs btn-primary no-pjax"
                                    href="<?php echo $cfg->getUrl() ?>scp/tickets.php?a=io&receipt_id=<?php echo $row['id'] ?>&from_view=receipt"
                                        data-title="Create New Payment">
                                    <i class="icon-list"></i>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo $cfg->getUrl() ?>scp/booking.php?booking_code=<?php
                                echo $row['booking_code'] ?>"
                                target="_blank" class="no-pjax">
                                <?php echo $row['booking_code'] ?>
                            </a>
                            <br>
                            <small><?php echo $row['staff_name'] ?></small>
                        </td>
                        <td>
                            <?php echo $row['tour_name'] ?>
                        </td>
                        <td>
                            <?php echo $row['customer_name']?>
                            <br>
                            <i class="icon-phone"></i> <?php echo $row['customer_phone_number'] ?>
                        </td>
                        <td>
                            <?php if($row['quantity_nl']): ?>
                                <?php echo $row['quantity_nl'] ?>
                            <?php endif; ?>
                            <?php if($row['quantity_te']): ?>
                                <br>
                                TE: <?php echo $row['quantity_te'] ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo number_format($row['amount_received']).'đ' ?>
                        </td>
                        <td>
                            <?php if(isset($row['created_at']) && strtotime($row['created_at']) > 0):
                                echo date('d/m/Y H:i', strtotime($row['created_at'])); ?>
                                <br>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (isset($row['status'])): ?>
                                <?php if ((int)$row['status'] == 0): ?>
                                    <span class="label label-small label-default">Chưa xử lý</span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::MONEY_RECEIVED): ?>
                                    <span class="label label-small label-default">Đã nhận tiền</span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::SEND_CODE): ?>
                                    <span class="label label-small label-primary">Đã gửi mã xác nhận</span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::WAITING_VERIFYING): ?>
                                    <span class="label label-small label-success">Khách xác nhận</i></span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::APPROVED): ?>
                                    <span class="label label-small label-success">Kế toán approved</i></span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::UPLOAD_IMAGE): ?>
                                    <span class="label label-small label-success">Upload hình phiếu thu</i></span>
                                <?php endif; ?>
                                <?php if ((int)$row['status'] == Receipt::REJECTED): ?>
                                    <span class="label label-small label-danger">Đã hủy</i></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                           <?php
                            $len = 18;
                            $note = mb_substr($row['internal_note'], 0, $len, 'UTF-8').(strlen($row['note']) > $len
                                    ? "[...]":"");
                            $_note = strip_tags($row['internal_note']);
                            ?>
                            <a href="#" data-title="<?php echo $_note ?>">
                                <?php echo $note; ?>
                            </a>
                        </td>
                        <td class="autohide-buttons">
                        <?php if (isset($row['status'])): ?>
                            <?php if ($thisstaff->canEditReceipt() && $row['status'] == 0) : ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=edit"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Edit"><i class="icon-pencil"></i></a>
                            <?php endif;?>

                            <?php if($thisstaff->canRejectReceipt() && $row['status'] != Receipt::REJECTED && $row['status'] != Receipt::APPROVED): ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=rejected"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Hủy phiếu thu"><i class="icon-remove"></i></a>
                            <?php endif; ?>
                            <?php if($row['status'] == 0 && $thisstaff->canEditReceipt()
                                &&($thisstaff->getId() == $row['created_by']
                                    || $thisstaff->getId() == $row['staff_cashier_id']
                                    || $thisstaff->getId() == $row['staff_id']
                                    || $thisstaff->canRejectReceipt())): ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=sales_confirm"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Kế toán xác nhận thu tiền"><i class="icon-check"></i></a>
                            <?php endif;?>

                            <?php if ($row['status'] == Receipt::MONEY_RECEIVED && $thisstaff->canEditReceipt()
                                && ($thisstaff->getId() == $row['created_by']
                                    || $thisstaff->getId() == $row['staff_cashier_id']
                                    || $thisstaff->getId() == $row['staff_id']
                                    || $thisstaff->canRejectReceipt())) : ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=confirm"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Send verify code"><i class="icon-envelope"></i></a>
                            <?php endif;?>

                            <?php if($thisstaff->canEditReceipt() && $row['status'] <= Receipt::MONEY_RECEIVED): ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=print"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Print"><i class="icon-print"></i></a>
                            <?php endif;?>

                            <?php if ($row['status'] == Receipt::WAITING_VERIFYING && $thisstaff->canApproveReceipt()): ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=confirm_financial"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Kế Toán xác nhận"><i class="icon-check"></i></a>
                            <?php endif;?>

                            <?php if ($row['status'] == Receipt::SEND_CODE && ($row['status'] != Receipt::MONEY_RECEIVED) && $thisstaff->canApproveReceipt()) : ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=confirm_code"
                                target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                                data-title="Nhập code xác nhận từ SMS của khách hàng"><i class="icon-check"></i></a>
                            <?php endif;?>

                            <?php if($row['status'] == Receipt::MONEY_RECEIVED && $thisstaff->canEditReceipt()
                                && ($thisstaff->getId() == $row['created_by']
                                    || $thisstaff->getId() == $row['staff_cashier_id']
                                    || $thisstaff->getId() == $row['staff_id']
                                    || $thisstaff->canRejectReceipt())): ?>
                            <a href="<?php echo $cfg->getUrl() ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=receipt_upload"
                               target="_blank"  class="btn_sm btn-xs btn-default no-pjax"
                               data-title="Upload Receipt" >
                                <i class="icon-camera"></i></a>
                            <?php endif;?>

                            <?php if($row['status'] == Receipt::UPLOAD_IMAGE && $thisstaff->canApproveReceipt()): ?>
                            <a href="<?php echo $cfg->getUrl();
                            ?>scp/receipt.php?id=<?php echo $row['id'] ?>&action=confirm_image"
                               target="_blank" class="btn_sm btn-xs btn-default no-pjax"
                               data-title="Kế Toán xác nhận" >
                                <i class="icon-check"></i></a>
                            <?php endif;?>
                        <?php endif; ?>
                        </td>
                    </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
<div> Page:
    <?php echo $pageNav->getPageLinks(); ?>
</div>
<script>
    $(document).ready(function() {
        $('.staff').select2();
    });
</script>

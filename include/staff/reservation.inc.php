<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
?>
<link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/reservation.css">

<h2>Reservation</h2>
<?php include STAFFINC_DIR.'reservation.search.inc.php' ?>

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs"  id="myTab">
        <?php if($params['customer_name'] || $params['customer_phonenumber'] || $params['staff_id']):?>
            <li class="">
                <a href="#search_result" data-toggle="tab">Search Result</a>
            </li>
        <?php endif;?>
        <li class="">
            <a href="#tourlist" data-toggle="tab">Tour List</a>
        </li>
        <li>
            <a href="#visareservation" data-toggle="tab">Visa</a>
        </li>
        <li class="">
            <a href="#staffreservation" data-toggle="tab">Your Reservation</a>
        </li>
        <li class="">
            <a href="#recently" data-toggle="tab">Recently Update</a>
        </li>
        <li>
            <a href="#upcoming" data-toggle="tab">Upcoming
                <?php if($upcoming && ($rows = db_num_rows($upcoming))): ?>
                    <span class="label label-small label-danger"><?php echo $rows ?></span>
                <?php endif; ?>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <?php if($params['customer_name'] || $params['customer_phonenumber'] || $params['staff_id']):?>
            <div class="tab-pane" id="search_result">
                <div class="upcoming_list">
                    <?php include STAFFINC_DIR.'reservation.search_result.inc.php' ?>
                </div>
            </div>
        <?php endif;?>
        <div class="tab-pane" id="tourlist">
            <div class="reservation_list">
                <?php $tour_group = ''; while($tours && ($tour = db_fetch_array($tours))): ?>
                    <?php if(!$tour_group || $tour_group !== date('m/Y',strtotime($tour['gather_date']))): ?>
                        <h2><?php echo 'Tháng '.date('m/Y',strtotime($tour['gather_date'])); ?></h2>
                        <hr>
                        <?php $tour_group = date('m/Y',strtotime($tour['gather_date']));?>
                    <?php endif;?>
                    <?php include STAFFINC_DIR.'reservation.tour-item.inc.php' ?>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="tab-pane" id="visareservation">

            <div class="hold__visa_wrapper">
                <?php include STAFFINC_DIR.'reservation.visa-hold.inc.php' ?>
            </div>

            <div class="reservation_list">

                <?php while($visa_country && ($_country = db_fetch_array($visa_country))):
                    if ($_country && isset($_country['country']) && $_country['country'])
                        ?>
                    <?php include STAFFINC_DIR.'reservation.visa-item.inc.php' ?>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="tab-pane" id="staffreservation">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.staffhold.inc.php' ?>
            </div>
        </div>
        <div class="tab-pane" id="recently">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.recently.inc.php' ?>
            </div>
        </div>
        <div class="tab-pane" id="upcoming">
            <div class="upcoming_list">
                <?php include STAFFINC_DIR.'reservation.upcoming.inc.php' ?>
            </div>
        </div>
    </div>
</div>

<script>
(function($) {
    $(document).ready(function(){
        tab_search_result = $("a[href='#search_result']").attr("href");
        if(window.location.hash === ''|| (window.location.hash === "#search_result" && typeof tab_search_result === "undefined"))
            activeTab('staffreservation');
        else
            activeTab(window.location.hash.replace('#',''));

        //default SHOW if search: customer_name, customer_phone, staff
        if(typeof tab_search_result !== "undefined")
            activeTab('search_result')
    });

    function activeTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    }

    $('#myTab a').click(function (e) {
        e.preventDefault();
        window.location.hash = $(e.target).attr("href");
        $(this).tab('show');
    });

    $('.dp_due').datepicker({
        changeYear: true,
        yearRange: "<?php echo date('Y') ?>:<?php echo intval(date('Y'))+1 ?>",
        shortYearCutoff: 50,
        showButtonPanel: true,
        selectOtherMonths: false,
        numberOfMonths: 1,
        minDate: new Date(<?php echo (time())*1000 ?>),
        maxDate: new Date(<?php echo (time() + 4*24*3600)*1000 ?>),
        dateFormat: 'dd/mm/yy'
    });

    $('.tour_item .tour_header').off('click').on('click', function(e) {
        _self = $(e.target);
        _item = _self.parents('.tour_item');

        if (_item.find('.tour_content').length) {
            if ( _item.find('.tour_content').is(":visible") ) {
                _item.find('.tour_content, .table_list, .action').hide();

            _item.find('.btn_details').removeClass('strong');
            _item.find('.btn_history').removeClass('strong');
            _item.find('.btn_action').removeClass('strong');
            } else {
                _item.find('.tour_content').show();
            }
        } else {

            _item.find('.btn_details').removeClass('strong');
            _item.find('.btn_history').removeClass('strong');
            _item.find('.btn_action').removeClass('strong');
            _item.find('.table_list').hide();
        }


    });

    $('.btn_details').off('click').on('click', function(e) {
        $('.btn_details').not(this).removeClass('strong');
        $('.btn_history').not(this).removeClass('strong');
        $('.btn_action').not(this).removeClass('strong');
        _self = $(e.target);
        _self.toggleClass('strong');
        _item = _self.parents('.tour_item');
        _item.find('.action').hide();
        _item.find('.table_list.history').hide();
        _item.find('.table_list.upcoming').hide();
        _item.find('.table_list.details').toggle();
    });

    $('.btn_history').off('click').on('click', function(e) {
        $('.btn_details').not(this).removeClass('strong');
        $('.btn_history').not(this).removeClass('strong');
        $('.btn_action').not(this).removeClass('strong');
        _self = $(e.target);
        _self.toggleClass('strong');
        _item = _self.parents('.tour_item');
        _item.find('.action').hide();
        _item.find('.table_list.details').hide();
        _item.find('.table_list.upcoming').hide();
        _item.find('.table_list.history').toggle();
    });

    $('.btn_action').off('click').on('click', function(e) {
        $('.btn_details').not(this).removeClass('strong');
        $('.btn_history').not(this).removeClass('strong');
        $('.btn_action').not(this).removeClass('strong');
        _self = $(e.target);
        _self.toggleClass('strong');
        _item = _self.parents('.tour_item');
        _item.find('.table_list.history').hide();
        _item.find('.table_list.details').hide();
        _item.find('.table_list.upcoming').hide();
        _item.find('.action').toggle();
    });

    $('.submit_btn').off('click').on('click', function(e) {
        if (!confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {
            e.preventDefault();
            return false;
        } else {
            $(this).parents('form').submit();
            $(this).prop('disabled', true);
        }
    });

    $('.edit_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        if (!id) return false;
        $.get('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?reservation_update=1&reservation_id='+id, function(data) {
            if (!data) return false;

            _self.parents('tr').after('<tr>' +
                    '<td colspan="13">' +
                    data+
                '</td>'+
                '</tr>');
        });
        $('.edit_btn').hide();
    });

    $('.transfer_date_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        if (!id) return false;
        $.get('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?transfer_date=1&reservation_id='+id, function(data) {
            if (!data) return false;

            _self.parents('tr').after('<tr>' +
                '<td colspan="13">' +
                data+
                '</td>'+
                '</tr>');
        });
        $('.transfer_date_btn').hide();
    });

    $('.cancel_all_btn').off('click').on('click', function(e) {
        reason = prompt("Nhập lý do huỷ chỗ:", "");
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();

        if(reason !== null && reason !== "")
        {
            $.get('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?reservation_cancel=1&reservation_id='+id+'&reason='+reason, function(data) {
                if (!data) return false;

                _self.parents('tr').after('<tr>' +
                        '<td colspan="13">' +
                        data+
                    '</td>'+
                    '</tr>');
            });
        }
    });

    $('.cancel_hold_btn').off('click').on('click', function(e) {
        reason = prompt("Nhập lý do huỷ chỗ:", "");
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        if(reason !== null && reason !== "") {
            $.get('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?reservation_cancel=1&reservation_id=' + id +'&reason='+reason + '&type=hold', function (data) {
                if (!data) return false;

                _self.parents('tr').after('<tr>' +
                    '<td colspan="13">' +
                    data +
                    '</td>' +
                    '</tr>');
            });
        }
    });

    $('.cancel_sure_btn').off('click').on('click', function(e) {
        reason = prompt("Nhập lý do huỷ chỗ:", "");
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        if(reason !== null && reason !== "") {
            $.get('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?reservation_cancel=1&reservation_id=' + id +'&reason='+reason + '&type=sure', function (data) {
                if (!data) return false;

                _self.parents('tr').after('<tr>' +
                    '<td colspan="13">' +
                    data +
                    '</td>' +
                    '</tr>');
            });
        }
    });

    $('.sure_all_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _form = _self.parents('tr');
        hold_qty = _form.find('[name=hold_qty]').val();
        sure_qty = _form.find('[name=sure_qty]').val();
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        tour_id = _form.find('[name=tour_id]').val();
        if(sure_qty == null || sure_qty === ""){
            sure_qty = 0;
        }
        if(hold_qty === sure_qty)
        {
            alert("Chỗ đã sure tất cả");
            return;
        }

        confirm_ok = confirm("Chỗ đang giữ: "+hold_qty+"\nChỗ đang sure hiện tại: "+sure_qty+"\nBạn có muốn chuyển qua SURE tất cả?");
        if(confirm_ok){
            $.post('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php',{action:"sure_all",reservation_id:id,tour_id:tour_id}, function(data) {
                if (!data) return false;
                _self.parents('tr').after('<tr>' +
                    '<td colspan="13">' +
                    data+
                    '</td>'+
                    '</tr>');
            });
        }
    });

    $('.extend_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _form = _self.parents('form');
        id = _form.find('[name=reservation_id]').val().trim();
        tour_id = _form.find('[name=tour_id]').val();

        $.post('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php',{action:"extend",reservation_id:id,tour_id:tour_id}, function(data) {
            if (!data) return false;
            _self.parents('tr').after('<tr>' +
                '<td colspan="13">' +
                data+
                '</td>'+
                '</tr>');
        });
    });

    $('.reset_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _parent = _self.parents('tr');
        _parent.find('td span').show();
        _parent.find('td input, td textarea').hide();
        _parent.find('td input, td textarea').each(function(index, elm) {
            $(elm).val($(elm).data('ori'));
        });
        $('.edit_btn').show();
        _parent.find('.update_btn, .reset_btn').hide();
    });

    $('.history_item').off('click').on('click', function(e) {
        _self = $(e.target);
        if (!_self.hasClass('history_item'))
            _self = _self.parent('tr');

        if (_self.hasClass('history_highlight')) {
            $('.history_item').removeClass('history_highlight');
            return;
        }

        $('.history_item').removeClass('history_highlight');
        $('[data-id='+_self.data('id')+']').toggleClass('history_highlight');
    });

    $('.btn_cancel_tour').off('click').on('click', function(e) {
        if (!confirm('Bạn có thật sự muốn huỷ tour này?')) {
            e.preventDefault();
            return false;
        }
        _self = $(e.target);
        _item = _self.parents('.tour_item');
        tour_id = _item.find('[name=tour_id]').val();
        $.post('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php',{action:"cancel_tour",tour_id:tour_id}, function(data) {
            if (!data) return false;
            _self.parents('.tour_item').after('<div>' +
                data+
                '</div>');
        });
    });

    $('.approval_request_btn').off('click').on('click', function(e) {
        _self = $(e.target);
        _form = _self.parents('tr');
        id = _form.find('[name=reservation_id]').val().trim();
        tour_id = _form.find('[name=tour_id]').val();

        $.post('<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php',{action:"approval_request",reservation_id:id, tour_id:tour_id}, function(data) {
            if (!data) return false;
            _self.parents('tr').after('<tr>' +
                '<td colspan="13">' +
                data +
                '</td>'+
                '</tr>');
        });
    });

})(jQuery);

function _reload(reservation_id) {
    setTimeout(function() {
        console.log(reservation_id);
        if (reservation_id) {
            window.location.href = '<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id='+reservation_id+window.location.hash;
        } else {
           location.reload();
        }
        }, 3000
    );
}
</script>
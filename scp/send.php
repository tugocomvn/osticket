<?php
require_once('staff.inc.php');
require INCLUDE_DIR.'../vendor/autoload.php';
use Mailgun\Mailgun;
global $thisstaff, $ost;

try {
    # First, instantiate the SDK with your API credentials
    $mg = Mailgun::create('key-7e2c1a69507a22f26690363c1d60e972');

    # Now, compose and send your message.
    # $mg->messages()->send($domain, $params);
    $html = preg_replace('/style=\\"[^\\"]*\\"/', '', $_REQUEST['body']);
    $html = preg_replace('/data-bind=\\"[^\\"]*\\"/', '', $html);
    $html = str_replace('&nbsp;', '', $html);
    $text = trim(preg_replace('#<[^>]+>#', ' ',  $html));

    $res = $mg->messages()->send('m.buupq.com', [
        'from'    => 'Facebook-TUGO <postmaster@m.buupq.com>',
        'to'      => $_REQUEST['to'],
        'subject' => trim($_REQUEST['title']),
        'html'    => trim($html),
        'text'    => trim($text),
    ]);

    file_put_contents(__DIR__.'/../public_file/Facebook-Email.'.$thisstaff->getUserName().'.'.date('Y-m-d').'.txt',
                      date('Y-m-d H:i:s').': '.trim($_REQUEST['title'])."\r\n", FILE_APPEND);

    if ($res->getId() && $ost && $ost->getCSRF()) exit($ost->getCSRF()->getToken());
} catch(Exception $ex) {
    exit($ex->getMessage());
}

exit('-1');

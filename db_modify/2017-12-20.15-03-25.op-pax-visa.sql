ALTER TABLE ost_pax_tour ADD visa_result TINYINT(1) DEFAULT 0 NULL;
ALTER TABLE ost_pax_tour ADD room_type VARCHAR(10) NULL;
ALTER TABLE ost_pax_tour
  MODIFY COLUMN room_type VARCHAR(10) AFTER room;

ALTER TABLE ost_pax_info MODIFY nationality VARCHAR(2) NOT NULL DEFAULT 'VN';

ALTER TABLE ost_pax_tour ADD `order` MEDIUMINT(2) DEFAULT 0 NULL;

ALTER TABLE ost_pax_tour CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE booking_tmp CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE payment_tmp CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

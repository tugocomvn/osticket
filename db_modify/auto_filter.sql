CREATE TABLE ost_auto_filter
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  execorder INT(10) unsigned DEFAULT '99' NOT NULL,
  isactive TINYINT(1) unsigned DEFAULT '1' NOT NULL,
  status INT(11) unsigned DEFAULT '0' NOT NULL,
  match_all_rules TINYINT(1) unsigned DEFAULT '0' NOT NULL,
  stop_onmatch TINYINT(1) unsigned DEFAULT '0' NOT NULL,
  reject_ticket TINYINT(1) unsigned DEFAULT '0' NOT NULL,
  use_replyto_email TINYINT(1) unsigned DEFAULT '0' NOT NULL,
  disable_autoresponder TINYINT(1) unsigned DEFAULT '0' NOT NULL,
  canned_response_id INT(11) unsigned DEFAULT '0' NOT NULL,
  email_id INT(10) unsigned DEFAULT '0' NOT NULL,
  status_id INT(10) unsigned DEFAULT '0' NOT NULL,
  priority_id INT(10) unsigned DEFAULT '0' NOT NULL,
  dept_id INT(10) unsigned DEFAULT '0' NOT NULL,
  staff_id INT(10) unsigned DEFAULT '0' NOT NULL,
  team_id INT(10) unsigned DEFAULT '0' NOT NULL,
  sla_id INT(10) unsigned DEFAULT '0' NOT NULL,
  form_id INT(11) unsigned DEFAULT '0' NOT NULL,
  topic_id INT(11) unsigned DEFAULT '0' NOT NULL,
  ext_id VARCHAR(11),
  target ENUM('Any', 'Web', 'Email', 'API') DEFAULT 'Any' NOT NULL,
  name VARCHAR(32) DEFAULT '' NOT NULL,
  notes TEXT,
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL
);
CREATE TABLE ost_auto_filter_rule
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  filter_id INT(10) unsigned DEFAULT '0' NOT NULL,
  `when` VARCHAR(32) NOT NULL,
  how ENUM('equal', 'not_equal', 'contains', 'dn_contain', 'starts', 'ends', 'match', 'not_match') NOT NULL,
  val VARCHAR(255) NOT NULL,
  isactive TINYINT(1) unsigned DEFAULT '1' NOT NULL,
  notes TINYTEXT NOT NULL,
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL
);
CREATE INDEX email_id ON ost_auto_filter (email_id);
CREATE INDEX target ON ost_auto_filter (target);
CREATE UNIQUE INDEX filter ON ost_auto_filter_rule (filter_id, `when`, how, val);
CREATE INDEX filter_id ON ost_auto_filter_rule (filter_id);
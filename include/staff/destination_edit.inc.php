<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');


$result_staff = Staff::getAllName();
$allStaff = [];
while ($result_staff && ($row = db_fetch_array($result_staff))){
    $allStaff[$row['staff_id']] = $row['name'];
}
?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl().'scp/destination_manage_item.php?market='.(int)$_REQUEST['market'] ?>">Back</a>
<form action="<?php echo $cfg->getUrl().'scp/destination_manage_item.php' ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $error ?></div>
    <?php endif;?>

    <input type="hidden" name="action" value="edit">
    <input type="hidden" name="market" value="<?php echo (int)$_REQUEST['market'] ?>">
    <input type="hidden" name="destination" value="<?php echo (int)$_REQUEST['destination'] ?>">

    <?php if($result):?>
        <h2 class="centered">Thị trường <?php echo $name_market ?></h2>
        <h2 class="centered"><?php echo $title_edit.': '.$result->name ?></h2>
        <table style="margin-left: 25%; margin-top: 10px">
            <tr class="required">
                <td width="150">Tên<span class="error">*&nbsp;</span></td>
                <td >
                    <input class="input-field" type="text" size="28" name="name" required value="<?php echo $result->name?>">
                    <br><em style="color:gray;display:inline-block">Tên điểm điến là duy nhất</em>
                </td>
            </tr>
            <tr>
                <td>Land rate</td>
                <td >
                    <input class="input-field" type="number" size="28" name="land_rate" data-name="land_rate" value="<?php echo (int)$result->land_rate ?>">
                    <span class="amount_text_helper" id="land_rate">đ</span>
                </td>
            </tr>
            <tr>
                <td>Land date</td>
                <td >
                    <input class="input-field" type="number" size="20" name="land_date" value="<?php echo (int)$result->land_date?>"> <i>(days)<i>
                </td>
            </tr>
            <tr>
                <td>Flight rate</td>
                <td >
                    <input class="input-field" type="number" size="28" name="flight_rate" data-name="flight_rate" value="<?php echo (int)$result->flight_rate?>">
                    <span class="amount_text_helper" id="flight_rate">đ</span>
                </td>
            </tr>
            <tr>
                <td>Flight date</td>
                <td >
                    <input class="input-field" type="number" size="20" name="flight_date" value="<?php echo (int)$result->flight_date?>"> <i>(days)<i>
                </td>
            </tr>
            <tr>
                <td>Trạng thái</td>
                <td>
                    <select name="status">
                        <option value="0" <?php if(!$result->status) echo 'selected' ?>>Hide</option>
                        <option value="1" <?php if($result->status) echo 'selected' ?>>Show</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="180">Nhân viên tạo</td>
                <td>
                    <span><?php echo $allStaff[$result->created_by] ?></span>
                </td>
            </tr>
            <tr>
                <td width="180">Ngày tạo</td>
                <td>
                        <span><?php if($result->created_at !== null && $result->created_at !== 0)
                                echo date('d/m/Y H:i:s',strtotime($result->created_at)) ?>
                        </span>
                </td>
            </tr>
            <tr>
                <td width="180">Nhân viên chỉnh sửa</td>
                <td>
                    <span><?php echo $allStaff[$result->updated_by] ?></span>
                </td>
            </tr>
            <tr>
                <td width="180">Ngày chỉnh sửa</td>
                <td>
                        <span><?php if($result->updated_at !== null && $result->updated_at !== 0 )
                                echo  date('d/m/Y H:i:s',strtotime($result->updated_at)) ?>
                        </span>
                </td>
            </tr>
        </table>
        <p class="centered">
            <button class="btn_sm btn-primary" name="edit" value="edit">Save</button>
        </p>
    <?php else: ?>
        <div id="msg_error">Không tìm thấy điểm đến của thị trường này</div>
    <?php endif;?>
</form>

<script>
    $(document).ready(function() {
        money = $('[data-name^="land_rate"]').val().trim();
        money = parseInt(money);
        money = money.formatMoney(0, '.', ',');
        $('#land_rate').text(money + ' đ');

        money = $('[data-name^="flight_rate"]').val().trim();
        money = parseInt(money);
        money = money.formatMoney(0, '.', ',');
        $('#flight_rate').text(money + ' đ');
    });

    $('[data-name="land_rate"]')
        .off('change, keyup')
        .on('change, keyup', function () {
            money = $('[data-name^="land_rate"]').val().trim();
            money = parseInt(money);
            money = money.formatMoney(0, '.', ',');
            $('#land_rate').text(money + ' đ');
        });

    $('[data-name="flight_rate"]')
        .off('change, keyup')
        .on('change, keyup', function () {
            money = $('[data-name^="flight_rate"]').val().trim();
            money = parseInt(money);
            money = money.formatMoney(0, '.', ',');
            $('#flight_rate').text(money + ' đ');
        });
</script>


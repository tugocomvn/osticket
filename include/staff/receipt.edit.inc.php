<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canEditReceipt()) die('Access Denied');
?>
<style>
.time_picker {
    background-color: white;
    display: inline-flex;
    border: 1px solid #ccc;
    color: #555;
}          

.time_picker input {
    border: none;
    color: #555;
    text-align: center;
    width: 60px;
    height :20px;
}

.receipt_form {
    background-color: #ffffff;
}

.input-field{
    width: 250px;
    margin: 3px;
}

.border-bottom td {
    border-bottom: 1px solid #eee;
    padding-bottom: 1em;
}

.border-bottom+tr td {
    padding-top: 1em;
}

textarea.input-field {
    width: 90%;
    height: 4em;
    resize: none;
}

.form_receipt {
    margin: auto;
}
</style>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>">Back</a>
<?php if($receipt->status == 0): ?>
<h2><?php echo __('Edit Receipt')."-".$receipt->receipt_code;?></h2>
<div>
    <h1 style="text-align: center; margin-bottom: 10px;">Phiếu Xác Nhận Dịch Vụ</h1>
    <form action="<?php echo $cfg->getUrl()."scp/receipt.php?id=".$receipt->id."&action=edit"?>" method="POST">
        <?php csrf_token(); ?>
            <div>
                <table class="form_receipt">
                    <tr>
                        <td><input type="hidden" id="receipt_id" name="receipt_id" value="<?php echo $receipt->id ?>"></td>
                        <td><input type="hidden" name="receipt_code" value="<?php echo $receipt->receipt_code ?>"></td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Mã booking:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="booking_code" name="booking_code" 
                            <?php if(isset($receipt)): ?>
                                value="<?php if($receipt && isset($receipt->booking_code))
                                    echo trim($receipt->booking_code) ?>"
                            <?php endif; ?>
                            readonly>
                            <font class="error">*</font>
                        </td>
                        <tr>
                            <td></td>
                            <td>
                                <em>Điền đầy đủ, ví dụ TG123-1234567</em>
                            </td>
                        </tr>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Lộ trình:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="tour_name" name="tour_name"
                            <?php if(isset($receipt)): ?>
                                value="<?php if($receipt && isset($receipt->tour_name))
                                    echo trim($receipt->tour_name) ?>"
                            <?php endif; ?>
                            >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Ngày khởi hành:'); ?></b></td>
                        <td>
                            <input class="dp input-field" id="departure_date" name="departure_date"
                            <?php if(isset($receipt)): ?>
                                value="<?php if(strtotime($receipt->departure_date))
                                echo date('d/m/Y', strtotime($receipt->departure_date)) ?>"
                                    autocomplete="OFF"
                            <?php endif; ?>
                            >    
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Ngày tập trung:'); ?></td>
                        <td>
                            <input type="text" class="dp input-field" id="time_to_get_in" name="time_to_get_in"
                            <?php if(isset($receipt)): ?>
                                value="<?php if (strtotime($receipt->time_to_get_in))
                                echo date('d/m/Y', strtotime($receipt->time_to_get_in)) ?>"
                                    autocomplete="OFF"
                            <?php endif; ?>
                            >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Địa điểm:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field" id="place_to_get_in" name="place_to_get_in"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->place_to_get_in))
                                        echo trim($receipt->place_to_get_in) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Giá tiền 1 khách (NL):'); ?></b></td>
                        <td>
                            <input type="number" class="input-field" id="unit_price_nl" name="unit_price_nl" min="1000"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->unit_price_nl))
                                        echo trim($receipt->unit_price_nl) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_unit_price_nl"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                         $('#unit_price_nl').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Số lượng (NL):'); ?></b></td>
                        <td>
                            <input style="width: 100px;" type="number" class="input-field" id="quantity_nl" min="0" max="9999"
                                   size="10" name="quantity_nl"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->quantity_nl))
                                        echo trim($receipt->quantity_nl) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td>Giá tiền 1 khách (TE):</td>
                        <td>
                            <input type="number" class="input-field"
                                   id="unit_price_te" min="0" name="unit_price_te"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->unit_price_te))
                                        echo trim($receipt->unit_price_te) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_unit_price_te"></span>
                        </td>
                        <script>
                         $('#unit_price_te').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    <tr class="border-bottom">
                        <td>Số lượng (TE):</td>
                        <td>
                            <input style="width: 100px;" type="number" class="input-field"
                                   min="0" size="10" max="9999" id="quantity_te" name="quantity_te"
                            <?php if(isset($receipt)): ?>
                                value="<?php if ($receipt && isset($receipt->quantity_te))
                                    echo trim($receipt->quantity_te) ?>"
                            <?php endif; ?>
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>Phí phụ thu:</td>
                        <td>
                            <input type="number" class="input-field"
                                   name="surcharge" min="0" id="surcharge"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->surcharge))
                                        echo trim($receipt->surcharge) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_surcharge"></span>
                        </td>
                        <script>
                         $('#surcharge').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    </tr>
                    <tr class="border-bottom">
                        <td>Nội dung phụ thu:</td>
                        <td>
                            <textarea type="text" class="input-field" id="surcharge_note"
                            name="surcharge_note"><?php if(isset($receipt->surcharge_note) && isset($receipt->surcharge_note))
                                echo trim($receipt->surcharge_note)?></textarea>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Tổng số tiền khách cần đóng:'); ?></b></td>
                        <td>
                            <input type="number" class="input-field"
                                   min="1000" id="amount_to_be_received"
                                   name="amount_to_be_received" minlength="4" 
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->amount_to_be_received))
                                        echo trim($receipt->amount_to_be_received) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_amount_to_be_received"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                         $('#amount_to_be_received').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    </tr>
                    <tr>
                        <td><b>Hình thức thanh toán:</b></td>
                        <td>
                        <select name="payment_method" id="payment_method">
                            <?php foreach(PaymentMethod::caseTitleName() as $_id => $_name): ?>
                                    <option value="<?php echo $_id ?>"
                                    <?php if (isset($receipt->payment_method) && $receipt->payment_method == $_id)
                                    echo "selected" ?>
                                    >
                                    <?php echo $_name ?></option>
                                <?php endforeach; ?>
                        </select>
                        <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="bank_transfer_note_">
                        <td>Ghi chú: </td>
                        <td>
                            <textarea class="input-field" id="bank_transfer_note" name="bank_transfer_note"
                                name="bank_transfer_note"><?php if(isset($receipt->bank_transfer_note) && trim($receipt->bank_transfer_note))
                                    echo trim($receipt->bank_transfer_note)
                            ?></textarea><font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Số tiền khách đóng:'); ?></b></td>
                        <td>
                            <input type="number" class="input-field"
                                   min="1000" id="amount_received" name="amount_received" minlength="4" 
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->amount_received))
                                        echo trim($receipt->amount_received) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_amount_received"></span>
                            <font class="error">*</font>
                        </td>
                        <script>
                         $('#amount_received').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    </tr>
                    <tr>
                        <td>Số tiền còn lại:</td>
                        <td>
                            <input type="number" class="input-field" min="0"
                                   id="balance_due" minlength="4"
                                   name="balance_due"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->balance_due))
                                        echo trim($receipt->balance_due) ?>"
                                <?php endif; ?>
                                >
                            <span id="format_balance_due"></span>
                        </td>
                        <script>
                         $('#balance_due').off('change, keyup').on('change, keyup', function() {
                            money_format();
                            });
                        </script>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Ngày hoàn tất thanh toán số tiền còn lại:'); ?></b></td>
                        <td>
                            <?php if(!empty($receipt->due_date) && empty($receipt->due_date_text)): ?>
                            <input style="width: 90px;" class="dp input-field" id="due_date"
                                   size=12 name="due_date"
                                    value="<?php if (strtotime($receipt->due_date))
                                    echo date('d/m/Y', strtotime($receipt->due_date)) ?>"
                                >
                            <?php endif; ?>
                            <?php if(!empty($receipt->due_date_text) && empty($receipt->due_date)): ?>
                                <textarea class="input-field" id="due_date_text"
                                      name="due_date_text"><?php if(isset($receipt->due_date_text) && isset($receipt->due_date_text))
                                          echo trim($receipt->due_date_text)
                                ?></textarea>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Tên khách đại diện:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field"
                                   id="customer_name" name="customer_name"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->customer_name))
                                        echo trim($receipt->customer_name) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Số điện thoại khách:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field"
                                   id="customer_phone_number" name="customer_phone_number"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->customer_phone_number))
                                        echo trim($receipt->customer_phone_number) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <em>Ghi chú: Sử dụng số điện thoại khách đại diện để nhận phiếu thu online</em>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td>Ghi chú cho khách:</td>
                        <td>
                            <textarea class="input-field" id="public_note"
                                      name="public_note"><?php if(isset($receipt) && isset($receipt->public_note))
                                          echo trim($receipt->public_note)
                                ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?php echo __('Tên nhân viên kinh doanh:');?></b></td>
                        <td>
                            <select name='staff_id' class="staff_id" id="staff_id">
                               <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                                <?php while ($staff_list && ($row = db_fetch_array($staff_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'],($receipt->staff_id == $row['staff_id']?'selected="selected"':''),$row['name']);
                                }?>
                            </select>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td><b><?php echo __('Số điện thoại nhân viên kinh doanh:'); ?></b></td>
                        <td>
                            <input type="text" class="input-field"
                                   id="staff_phone_number" name="staff_phone_number"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if ($receipt && isset($receipt->staff_phone_number))
                                        echo trim($receipt->staff_phone_number) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                    <td><b>Nhân viên thu tiền:</b></td>
                        <td>
                            <select name='staff_cashier_id' class="staff_id" id="staff_cashier_id">
                               <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                                <?php while ($finance_staff_list && ($row = db_fetch_array($finance_staff_list))){
                                    echo sprintf('<option value="%d" %s>%s</option>',
                                    $row['staff_id'],($receipt->staff_cashier_id == $row['staff_id']?'selected="selected"':''),$row['name']);
                                }?>
                            </select>
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Số điện thoại nhân viên thu tiền:</b></td>
                        <td>
                            <input type="text" class="input-field"
                                   id="staff_cashier_phone_number" name="staff_cashier_phone_number"
                                <?php if(isset($receipt)): ?>
                                    value="<?php if($receipt && isset($receipt->staff_cashier_phone_number))
                                        echo trim($receipt->staff_cashier_phone_number) ?>"
                                <?php endif; ?>
                                >
                            <font class="error">*</font>
                        </td>
                    </tr>
                    <tr class="border-bottom">
                        <td>Ghi chú nội bộ:</td>
                        <td>
                            <textarea class="input-field" id="internal_note"
                                      name="internal_note"><?php if(isset($receipt->internal_note) && isset($receipt->internal_note))
                                          echo trim($receipt->internal_note)
                                ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Thời gian cần hoàn tất hồ sơ:</td>
                        <td>
                            <input type="text" class="dp input-field"
                                   id="document_due" name="document_due"
                            <?php if(isset($receipt)): ?>
                                value="<?php if (strtotime($receipt->document_due))
                                echo date('d/m/Y', strtotime($receipt->document_due)) ?>"
                                    autocomplete="OFF"
                            <?php endif; ?>
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>Thời gian DỰ KIẾN có kết quả visa:</td>
                        <td>
                            <input type="text" class="dp input-field"
                                   id="visa_result_estimate_due" name="visa_result_estimate_due"
                            <?php if(isset($receipt)): ?>
                                value="<?php if (strtotime($receipt->visa_result_estimate_due))
                                echo date('d/m/Y', strtotime($receipt->visa_result_estimate_due)) ?>"
                                    autocomplete="OFF"
                            <?php endif; ?>
                            >
                        </td>
                    </tr>
                </table>
                <p class="centered">
                    <?php if($thisstaff->canEditReceipt()): ?>
                        <button class="btn_sm btn-success edit" type="submit" name="action" value="edit">Save</button>
                    <?php endif; ?>
                    <a href="<?php echo $cfg->getUrl()."scp/receiptlist.php"?>" class="btn_sm btn-danger">Cancel</a>
                </p>
    </form>  
    <?php endif; ?>  
</div>
<script>
    (function($){
        $('.staff_id').select2();
        $("#payment_method").change(function() {
            if ($(this).val() != 1) {
                $('.bank_transfer_note_').show();
            }else{
                $('.bank_transfer_note_').hide();
                $('.bank_transfer_note').val("");
            }
        }).change();
        //choose 2 option due_date/due_date_text
        $( ".due" ).change(function() {
            if($(this).val() === 'due_date') {
                $('.due_date').show();
                $('.due_date_text').hide();
                $("#due_date_text").val("");
            }else{
                $('.due_date_text').show();
                $('.due_date').hide();
                $("#due_date").val("");
            }
        }).change();
        money_format();
        $('.edit').off('click').on('click', function(e) {
            if (!confirm('Bạn đã kiểm tra kĩ thông tin chưa ?')) {
                e.preventDefault();
                return false;
            }
        });
        $('#staff_cashier_id').on('change', function() {
            $.ajax({
                url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/load',
                data: { "value": $("#staff_cashier_id").val() },
                type : 'POST',
                dataType : 'json',
                success: function(res){
                    if(res.result === 1){
                        $('#staff_cashier_phone_number').val(res.phone_number);      
                    }                                  
                }
            });
        });
        $('#staff_id').on('change', function() {
            $.ajax({
                url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/receipt/load',
                data: { "value": $("#staff_id").val() },
                type : 'POST',
                dataType : 'json',
                success: function(res){
                    if(res.result === 1){
                        $('#staff_phone_number').val(res.phone_number);      
                    }                                  
                }
            });
        });
    })(jQuery); 
    function money_format() {
        $('#format_unit_price_nl').text(parseInt($('#unit_price_nl').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_unit_price_te').text(parseInt($('#unit_price_te').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_surcharge').text(parseInt($('#surcharge').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_amount_to_be_received').text(parseInt($('#amount_to_be_received').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_amount_received').text(parseInt($('#amount_received').val()).formatMoney(0, '.', ',')+'đ');
        $('#format_balance_due').text(parseInt($('#balance_due').val()).formatMoney(0, '.', ',')+'đ');
    }
    money_format();
</script>

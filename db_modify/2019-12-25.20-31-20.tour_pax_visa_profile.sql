/*branch: hotfix/24.4.1 */
alter table tour_pax_visa_profile
	add created_by int null;

alter table tour_pax_visa_profile
	add created_at datetime null;

alter table tour_pax_visa_profile
	add updated_by int null;

alter table tour_pax_visa_profile
	add updated_at datetime null;

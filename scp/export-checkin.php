<?php
require('staff.inc.php');
include_once INCLUDE_DIR.'class.checkin.php';

if (!isset($_REQUEST['from']) || empty($_REQUEST['from']))
    $_REQUEST['from'] = Format::userdate('Y-m-d', time());

if (!isset($_REQUEST['to']) || empty($_REQUEST['to']))
    $_REQUEST['to'] = Format::userdate('Y-m-d', time());

if (!isset($_REQUEST['export']) || !$_REQUEST['export'])
    $_REQUEST['export'] = 0;

$from = date_create_from_format('d/m/Y', $_REQUEST['from'])
    ? date_create_from_format('d/m/Y', $_REQUEST['from'])->format('Y-m-d') : date('Y-m-d');
$to = date_create_from_format('d/m/Y', $_REQUEST['to'])
    ? date_create_from_format('d/m/Y', $_REQUEST['to'])->format('Y-m-d') : date('Y-m-d');

if (!isset($_REQUEST['agent']) || empty($_REQUEST['agent']))
    $_REQUEST['agent'] = 0;

$data = Checkin::getByStaff($_REQUEST['agent'], $from, $to, $_REQUEST['export'] ? 999 : null, $_REQUEST['export']);


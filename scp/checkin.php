<?php
require('staff.inc.php');

$is_sunday = in_array(date('N'), config('dayoff') ?: []);
$is_checked_in = Checkin::getToday($thisstaff->getId());

if ($is_checked_in
    || !(
        !in_array(date('N'), config('dayoff') ?: [])
//        && !$thisstaff->isAdmin()
        && !in_array($thisstaff->getId(), config('no_checkin') ?: [])
    )
) {
    header('Location: /scp/dashboard.php');
    exit;
}
//

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST) && $_POST && !$is_sunday) {
    $csrf = new CSRF();
    $token = $csrf->getToken();
    if (!$token) {
        ?><p><strong>Invalid token!!!</strong></p><?php
        exit;
    }
    $valid = $csrf->validateToken($token);
    if (!$valid) {
        ?><p><strong>Invalid token!!!</strong></p><?php
        exit;
    }
    Checkin::checkin($thisstaff->getId());
}

$is_checked_in = Checkin::getToday($thisstaff->getId());

$inc = 'checkin.inc.php';
require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.$inc);
?>

</div>
</div>
<?php  require_once __DIR__.'/../version.php';?>
<div id="footer">
    <p>Copyright &copy; <?php echo date('Y'); ?> &bullet; <?php echo (string) $ost->company ?: 'osTicket.com'; ?></p>
    <p>Version <?php echo TUGO_OST_VERSION ?></p>
    <p><?php echo _String::getIp() ?></p>
    <p><?php echo date('Y-m-d H:i:s') ?></p>
</div>
</div>
<div class="footer-holder"></div>
</body>
</html>

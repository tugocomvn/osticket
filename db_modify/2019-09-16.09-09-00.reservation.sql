ALTER TABLE `booking_reservation` ADD `extend_due` SMALLINT UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `booking_reservation_history` ADD `extend_due` SMALLINT UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `ost_tour` DROP `status`
ALTER TABLE `ost_tour` ADD `status` TINYINT(1) NOT NULL DEFAULT '1'

ALTER TABLE `ost_groups` ADD `can_cancel_tour` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

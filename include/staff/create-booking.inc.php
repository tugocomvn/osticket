<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canCreateBookings()) die('Access Denied');
$status = DynamicListItem::objects()->filter(['list_id' => BOOKING_STATUS_LIST])->order_by('value');
$partner = DynamicListItem::objects()->filter(['list_id' => PARTNER_LIST])->order_by('value');
$code = DynamicListItem::objects()->filter(['list_id' => NO_ANSWER_1ST_STATUS])->order_by('value');
$allStaff = Staff::getAllName();
$booking_code = BOOKING_CODE_PREFIX.UUID::generate('booking_code');
?>
<style>
    input[type="text"],
    input[type="number"] {
        width: 12em !important;
    }

    textarea {
        width: 95%;
        height: 5em;
        resize: none;
    }

    table.fixed {
        table-layout: fixed;
        border-collapse: collapse;
        width: 70%;
        margin: auto;
    }

    table,
    .form_table,
    .form_table td,
    tr, td {
        border-top: none;
        border-bottom: none;
        border-right: none;
        border-left: none;
    }

    table.room input[type="text"] {
        width: 2em !important;
        margin-right: 1.5em;
    }

    table.room td:last-child input[type="text"] {
        width: 7em !important;
        margin-right: 1.5em;
    }

    .form_table th, div.section-break {
        border: none;
        margin-top: 1em;
        margin-bottom: 1em;
    }
</style>


<form action="<?php echo $cfg->getUrl(); ?>scp/new_booking.php?id=<?php echo $result['ticket_id'] ?>&action=create" method="post" id="save"  enctype="multipart/form-data" class="repeater">
    <?php csrf_token(); ?>
    <input type="hidden" name="action" value="create">
    <input type="hidden" name="reservation_id" value="<?php echo (int)$_REQUEST['reservation_id'] ?>">
    <h2><?php echo __('Create a New Booking');?></h2>
    <?php if(isset($errors) && $errors ): ?>
        <?php foreach ($mess as $value): ?>
            <div id="msg_warning"><?php echo $value ?></div>
        <?php endforeach; ?>
    <?php endif;?>
    <table class="form_table fixed" width="940" border="0" cellspacing="0" cellpadding="2">
        <thead>
            <tr><td></td><td></td></tr>
            <tr>
                <th colspan="2">
                    <h4><?php echo __('New Booking');?></h4>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:150px;"></td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">
                    <em><strong>Booking</strong>:
                    </em>
                </th>
            </tr>
            <tr>
                <td>
                    Mã Booking:
                </td>
                <td>
                    <input type="text" name="booking_code"  class="input-field" placeholder="Ma booking" readonly value="<?php echo $booking_code?>">
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Mã Ticket:
                </td>
                <td>
                    <input type="text" name="ticket_number" required  class="input-field">
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Tên khách:
                </td>
                <td>
                     <textarea class="input-field" cols="100" rows="4" required type="text" name="customer" ><?php if(isset($data_load) && $data_load['customer_name']) echo $data_load['customer_name'] ?></textarea>
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Số điện thoại:
                </td>
                <td>
                    <input class="input-field" cols="12" width="12" type="text" required name="phone_number"
                        pattern="^0[0-9]{9}$" aria-label="Điền một Số điện thoại"
                        value="<?php if(isset($data_load) && $data_load['phone_number']) echo $data_load['phone_number'] ?>" />
                    <font class="error">*</font>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <input type="text" name="email"  class="input-field">
                </td>
            </tr>

            <tr>
                <td>
                    Loại Booking:
                </td>
                <td>
                    <div style="position:relative">
                        <select name="booking_type" required  class="input-field">
                            <option value="">— Select —</option>
                            <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                                <?php foreach($booking_type_list as  $item): ?>
                                    <option value="<?php echo $item['id'] ?>"
                                        <?php if(isset($data_load) && $data_load['destination_id'] == $item['id']) echo 'selected' ?>
                                    ><?php echo $item['name'] ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <font class="error">*</font>
                    </div>
                </td>
            </tr>
            <tr>
                <td >
                    Ngày khởi hành:
                </td>
                <td>
                    <input class="input-field dp" name="dept_date"  required type="text" autocomplete="off"
                    value="<?php if($data_load && $data_load['departure_date'])
                        echo date('d/m/Y', strtotime($data_load['departure_date']))?>">
                    <font class="error">*</font>
                </td>

            </tr>
            <tr>
                <td >
                    Ngày full CỌC:
                </td>
                <td>
                    <input class="input-field dp" name="fullcoc_date"  required type="text" autocomplete="off">
                    <font class="error">*</font>
                </td>

            </tr>
            <tr>
                <td >
                    Ngày full PAY:
                </td>
                <td>
                    <input class="input-field dp" name="fullpay_date"  required type="text" autocomplete="off">
                    <font class="error">*</font>
                </td>

            </tr>

            <tr>
                <td>
                    Đối tác:
                </td>
                <td>
                    <div style="position:relative">
                        <select name="partner">
                            <option value="">— Select —</option>
                            <?php foreach ($partner as $data):?>
                                <option value="<?php echo $data->id ?>" <?php if($data->id == 92) echo 'selected' ?>><?php echo $data->value ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Nhân viên phụ trách:
                </td>
                <td>
                    <div style="position:relative">
                        <select name="staff" required  class="input-field">
                            <option value="">— Select —</option>
                            <?php while ($allStaff && ($row = db_fetch_array($allStaff))):?>
                                <option value="<?php echo $row['staff_id'] ?>" <?php if($row['staff_id'] == (int)$data_load['staff_id']
                                    || ($row['staff_id'] == $thisstaff->getId() && !isset($data_load)))  echo 'selected' ?>><?php echo $row['name'] ?></option>
                            <?php endwhile;?>
                        </select>
                        <font class="error">*</font>
                    </div>
                </td>
            </tr>

            <tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Người Lớn __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input type="number" name="quantity1" class="input-field" data-name="quantity1" required type="text"
                    value="<?php if($data_load && $data_load['quantity_pax_old']) echo $data_load['quantity_pax_old'];
                    else echo 0 ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice1" data-type='currency' data-name="retailprice1" required  class="input-field"
                        value="<?php if($data_load && $data_load['retail_price']) echo $data_load['retail_price']; else echo 0; ?>">
                        <font class="error">*</font>
                        <span id="retailprice1">đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice1"  data-type='currency' data-name="netprice1" required  class="input-field"
                       value="<?php if($data_load && $data_load['net_price']) echo $data_load['net_price']; else echo 0 ?>">
                        <font class="error">*</font>
                        <span id="netprice1">đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note1" rows="2"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Trẻ Em __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input type="number" name="quantity2"  required type="text" data-name="quantity2"  class="input-field"
                   value="<?php if($data_load && $data_load['quantity_infant']) echo $data_load['quantity_infant'];  else echo 0 ?>">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice2" data-type='currency' data-name="retailprice2" required  class="input-field"
                   value="<?php if($data_load && $data_load['retail_price']) echo $data_load['retail_price']; else echo 0?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice2" data-type='currency' data-name="netprice2" required  class="input-field"
                   value="<?php if($data_load && $data_load['net_price']) echo $data_load['net_price']; else echo 0 ?>">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note2" rows="2"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Phụ Thu __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input type="number" name="quantity3"  class="input-field" required type="text" data-name="quantity3" value="0">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice3"  class="input-field" data-type='currency' required data-name="retailprice3" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice3"  class="input-field" data-type='currency' required data-name="netprice3" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note3" rows="2"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__ Giá Khác __</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Số lượng:
                </td>
                <td>
                    <input type="number" name="quantity4"  class="input-field" data-type='currency' required data-name="quantity4" value="0">
                </td>
            </tr>
            <tr>
                <td>
                    Giá bán 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="retailprice4"   class="input-field" data-type='currency' required data-name="retailprice4" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá net 1 pax:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="netprice4"  class="input-field" data-type='currency' required data-name="netprice4" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr class="dy-form">
                <td class="multi-line " style="min-width:120px;">
                    Note:
                </td>
                <td>
                    <textarea style="resize: none" name="note4" rows="2"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 1__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype1" class="input-field" data-name="discounttype1" value="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount1"  class="input-field" data-type='currency' data-name="discountamount1" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 2__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype2" class="input-field" data-name="discounttype2" value="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount2" class="input-field" data-type='currency' data-name="discountamount2" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>__Giảm giá 3__</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Loại:
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discounttype3" class="input-field" data-name="discounttype3" value="">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Giá giảm
                </td>
                <td>
                    <div style="position:relative">
                        <input type="text" name="discountamount3" class="input-field" data-type='currency' data-name="discountamount3" value="0">
                        <font class="error">*</font>
                        <span>đ</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>Xếp Phòng</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="room">
                        <tr>
                            <td colspan="6">
                                <em style="color:gray;display:inline-block">Điền số lượng khách (để trống nếu không có) CÓ PHỤ
                                    THU</em>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Twin: <font class="error">*</font></strong>
                            </td>
                            <td>
                                <input type="text"  class="input-field" name="room_twin" required value="0">
                            </td>
                            <td>
                                <strong>Double: <font class="error">*</font></strong>
                            </td>
                            <td>
                                <input type="text"  class="input-field" name="room_double" required>
                            </td>
                            <td>
                                Triple:
                            </td>
                            <td>
                                <input type="text"  class="input-field" name="room_triple" >
                            </td>
                            <td>
                                Single:
                            </td>
                            <td>
                                <input type="text"  class="input-field" name="room_single">
                            </td>
                            <td>
                                Khác:
                            </td>
                            <td>
                                <input type="text"  class="input-field" name="room_other" placeholder="Lẻ nam/lẻ nữ">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>Tải lên tài liệu</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>Cam kết</td>
                <td width="150px" class="form_group">
                    <input type="file" name="commitment_file" class="input-field"/>
                </td>
            </tr>
            <tr>
                <td>Hợp đồng</td>
                <td width="150px" class="form_group">
                    <input type="file" name="contract_file" class="input-field"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-header section-break"><h3>____</h3><em></em></div>
                </td>
            </tr>
            <tr>
                <td>
                    Trạng thái:
                </td>
                <td>
                    <select name="status"  class="input-field">
                        <option value="">— Select —</option>
                        <?php foreach ($status as $data):?>
                            <option value="<?php echo $data->id ?>" <?php if($data->id == 53) echo 'selected'; ?>><?php echo $data->value ?></option>
                        <?php endforeach;?>
                    </select>

                </td>
            </tr>
            <tr>
                <td>
                    Ghi chú:
                </td>
                <td>
                    <textarea style="resize: none" rows="2" cols="70"  name="note" class="input-field"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Mã giới thiệu:
                </td>
                <td>
                    <select name="referral_code"  class="input-field">
                        <option value="">— Select —</option>
                        <?php foreach ($code as $data):?>
                            <option value="<?php echo $data->id ?>"><?php echo $data->value ?></option>
                        <?php endforeach;?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><strong>Tổng giá bán: </strong></td>
                <td><span class="total_retailprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng giá net: </strong></td>
                <td><span class="total_netprice"></span></td>
            </tr>
            <tr>
                <td><strong>Tổng lợi nhuận: </strong></td>
                <td><span class="total_profit"></span></td>
            </tr>
        </tbody>
    </table>
    <p style="text-align:center;">
        <input type="submit" id="submit_btn" class="btn_sm btn-primary"
               value="<?php echo _P('action-button', 'Create');?>" >
        <input type="submit" id="check_submit" style="display: none;">
        <input type="reset"  name="reset"  class="btn_sm btn-default"
               value="<?php echo __('Reset');?>">
        <input type="button" name="cancel" class="btn_sm btn-danger"
               value="<?php echo __('Cancel');?>" onclick="javascript:
        $('.richtext').each(function() {
            var redactor = $(this).data('redactor');
            if (redactor && redactor.opts.draftDelete)
                redactor.deleteDraft();
        });
        window.location.href='tickets.php';
    ">
    </p>
</form>

<script>
    $(document).ready(function () {
        $('#'+$(this).attr("name")).text(formatNumber($(this).val()));
        booking_calc_action();
    });
    $("input[data-type='currency']").on({
        keyup: function() {
            $('#'+$(this).attr("name")).text(formatNumber($(this).val()));
            booking_calc_action();
        },
    });


    function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

</script>
<script>
$(document).ready(function() {
    $('input[name="phone_number"]').on('input blur change keyup', function() {
        var phone_number = $(this).val();
        if(phone_number.match(/^0[0-9]{9}$/)) {
            $(this).css('border-color', 'green');
            this.setCustomValidity("");
        } else {
            $(this).css('border-color', 'red');
            this.setCustomValidity("Điền 01 số điện thoại đi bạn!");
        }
    });
});
</script>

<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

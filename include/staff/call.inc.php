<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

global $cfg;
$qs = array();
$qwhere =' WHERE 1 ';

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere.=' AND DATE(add_time)>='.db_input(date('Y-m-d', $startTime));
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere.=' AND DATE(add_time)<='.db_input(date('Y-m-d', $endTime));
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
$sortOptions=array(
    'id'=>'id',
    'callernumber'=>'callernumber',
    'destinationnumber'=>'destinationnumber',
    'starttime'=>'starttime',
    'totalduration'=>'totalduration',
    'add_time' => 'add_time'
);
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'id';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'log.add_time';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$qselect = 'SELECT * ';
$qfrom=' FROM '.CALL_LOG_TABLE.' log ';

$total=db_count("SELECT count(*) $qfrom $qwhere");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
//pagenate
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('call.php',$qs);
$qs += array('order' => ($order=='DESC' ? 'ASC' : 'DESC'));
$qstr = '&amp;'. Http::build_query($qs);
$query="$qselect $qfrom $join $qwhere ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' '.$title;
else
    $showing=__('No logs found!');
?>

<h2><?php echo __('Call Logs');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>

<div id='filter' >
    <form action="call.php" method="get">
        <div style="padding-left:2px;">
            <b><?php echo __('Date Span'); ?></b>&nbsp;<i class="help-tip icon-question-sign" href="#date_span"></i>
            <?php echo __('Between'); ?>:
            <input class="dp" id="sd" size=15 name="startDate" value="<?php echo Format::htmlchars($_REQUEST['startDate']); ?>" autocomplete=OFF>
            &nbsp;&nbsp;
            <input class="dp" id="ed" size=15 name="endDate" value="<?php echo Format::htmlchars($_REQUEST['endDate']); ?>" autocomplete=OFF>&nbsp;&nbsp;
            <input type="submit" Value="<?php echo __('Go!');?>" />
        </div>
    </form>
</div>
<table class="list" border="0" cellspacing="1" cellpadding="0" width="1050">
    <caption><?php echo $showing; ?></caption>
    <thead>
    <tr>
        <th>#</th>
        <th nowrap><a <?php echo $callernumber_sort; ?> href="call.php?<?php echo $qstr; ?>&sort=callernumber"><?php echo __('Phone Number');?></a></th>
        <th nowrap><a <?php echo $destinationnumber_sort; ?> href="call.php?<?php echo $qstr; ?>&sort=destinationnumber"><?php echo __('Call To');?></a></th>
        <th><a  <?php echo $starttime_sort; ?> href="call.php?<?php echo $qstr; ?>&sort=starttime"><?php echo __('Start');?></a></th>
        <th><a  <?php echo $answertime_sort; ?>href="call.php?<?php echo $qstr; ?>&sort=answertime"><?php echo __('Answer');?></a></th>
        <th><a  <?php echo $endtime_sort; ?>href="call.php?<?php echo $qstr; ?>&sort=endtime"><?php echo __('End');?></a></th>
        <th><a  <?php echo $totalduration_sort; ?>href="call.php?<?php echo $qstr; ?>&sort=totalduration"><?php echo __('Duration');?></a></th>
        <th><a  <?php echo $disposition_sort; ?>href="call.php?<?php echo $qstr; ?>&sort=disposition"><?php echo __('Disposition');?></a></th>
        <th><a  <?php echo $add_time_sort; ?>href="call.php?<?php echo $qstr; ?>&sort=add_time"><?php echo __('Add Time');?></a></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $num = 0;
    if($res && db_num_rows($res)):
        while ($row = db_fetch_array($res)) {?>
            <tr id="<?php echo $row['id']; ?>">
                <td>
                    <?php echo ($pageNav->getStart() + $num + 1);
                    $num++;   ?>
                    <?php if (isset($row['disposition']) && 'NO ANSWER' == $row['disposition']): ?>
                        <span>&#x219d;</span>
                    <?php elseif ('inbound' == $row['direction']): ?>
                        <span>&#x2198;</span>
                    <?php else: ?>
                        <span>&#x2196;</span>
                    <?php endif; ?>
                </td>
                <td style="<?php if (isset($row['disposition']) && 'NO ANSWER' == $row['disposition']) echo 'color:red;' ?>"><?php echo $row['callernumber'] ?></td>
                <td><?php echo $row['destinationnumber'] ?></td>
                <td><?php echo Format::date($cfg->getDateTimeFormat(), strtotime($row['starttime'])-($cfg->getDBTZoffset()*3600)); ?></td>
                <td><?php echo Format::date($cfg->getDateTimeFormat(), strtotime($row['answertime'])-($cfg->getDBTZoffset()*3600)); ?></td>
                <td><?php echo Format::date($cfg->getDateTimeFormat(), strtotime($row['endtime'])-($cfg->getDBTZoffset()*3600)); ?></td>
                <td><?php echo $row['totalduration']; ?></td>
                <td><?php echo $row['disposition'] ?></td>
                <td><?php echo Format::db_datetime($row['add_time']); ?></td>
            </tr>
            <?php
        } //end of while.
    endif; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="9">
            <?php if(!$res || !$num){
                echo __('No logs found');
            } ?>
        </td>
    </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>

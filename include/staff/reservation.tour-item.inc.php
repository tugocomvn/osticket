<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');
$reservation_cache = [];
$_tour_available = intval($tour['quantity']) - intval($tour['sure_quantity']) - intval($tour['hold_quantity']);

$color = $tour['color_codes']?:'';  //default
if (!$tour['status'] || (date('Y-m-d',strtotime($tour['gather_date'])) < date('Y-m-d')))
    $color = '#6c757d'; //het han
elseif($_tour_available <= 0) $color = '#800080'; //full doan
if($color === '') $color_font = '#000000';
else $color_font = '#ffffff';
?>
<style>
    .current_reservation td:nth-child(3) {
        border-right: 1px solid  #ccc;
    }
</style>
<div class="tour_item">
    <input type="hidden" name="tour_id" value="<?php echo $tour['id'] ?>">
    <div style="background-color: <?php echo $color ?>;" class="tour_header">
        <span style="color: <?php echo $color_font?>" class="tour_name"> <?php echo $tour['name'] ?></span>
        <span style="color: <?php echo $color_font?>" class="tour_time"> Ra sân bay: <?php echo date('d/m/Y H:i',strtotime($tour['gather_date'])) ?></span>
        <span style="color: <?php echo $color_font?>" class="tour_price"><?php echo number_format($tour['retail_price'], 0, ',', '.') ?> đ</span>
        <div class="clearfix"></div>
    </div>
    <div class="tour_content" style="display:none;">
        <div class="tour_info">
            <p>
                Bay đi:
                <?php
                $flight_code = FlightOP::searchFlightCodeFromTour((int)$tour['id']);
                $departure_flight_code = '';
                $return_flight_code = '';
                if(count($flight_code) === 4){
                    $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
                    $return_flight_code = $flight_code[2].'-'.$flight_code[3];
                }elseif(count($flight_code) === 2){
                    $departure_flight_code = $flight_code[0];
                    $return_flight_code = $flight_code[1];
                } ?>
                <?php if($tour['gateway_present_time']) echo date('d/m/Y H:i',strtotime($tour['gateway_present_time'])) ?> -
                <?php echo $departure_flight_code ?>
            </p>
            <p>
                Bay về: <?php if($tour['return_time']) echo date('d/m/Y H:i',strtotime($tour['return_time'])) ?> -
                <?php echo $return_flight_code ?>
            </p>
            <p>
                <?php echo itinerary(trim($tour['itinerary_a']),trim($tour['itinerary_b']),trim($tour['itinerary_c']),trim($tour['itinerary_d'])) ?>
            </p>
            <hr>
            <?php echo $tour['notes'] ?>
        </div>
        <p class="tour_tl">
            TL:
                <?php
                $tour_leader_item = DynamicListItem::lookup($tour['tour_leader']);
                ?><?php if ($tour_leader_item) echo $tour_leader_item->getValue(); else echo 'N/A' ?>
        </p>
        <div class="clearfix"></div>
        <p>
            <?php if($thisstaff->canCancelTour()): ?>
                <?php if($tour['status']): ?>
                    <button class="btn_sm btn-xs btn-danger btn_cancel_tour">Cancel Tour</button>
                <?php else: ?>
                    <button class="btn_sm btn-xs btn-primary btn_cancel_tour">Reopen Tour</button>
                <?php endif;?>
            <?php endif; ?>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="tour_footer">
        <div class="function">
            <div class="quantity">
<!--                <span class="bf_label"><span class="before"><strong>Total</strong></span><span class="after">31</span></span>-->
                <span class="bf_label"><span class="before">Hold</span><span class="after"><?php echo $tour['hold_quantity'] ?: 0 ?></span></span>
                <span class="bf_label"><span class="before">Sure</span><span class="after"><?php echo $tour['sure_quantity'] ?: 0 ?></span></span>
                <span class="bf_label"><span class="before">Available</span><span class="after"><?php echo $_tour_available ?></span></span>
            </div>
            <div class="buttons">
<!--                <button class="btn_sm btn-warning btn_upcoming">Upcoming</button>-->
                <?php
                $reservation_history = BookingReservationHistory::getByTour($tour['id'], false);
                $reservations = BookingReservation::getByTour($tour['id'], false);
                ?>
                <?php if(db_num_rows($reservation_history) > 0): ?>
                    <button class="btn_sm btn-xs btn-default btn_history">History</button>
                <?php endif; ?>
                <?php if(db_num_rows($reservations) > 0): ?>
                    <button class="btn_sm btn-xs btn-success btn_details <?php if (isset($_REQUEST['reservation_id']) && (int)$_REQUEST['reservation_id']) echo ' strong' ?>">List</button>
                <?php endif; ?>
                <?php if($tour['status'] && $thisstaff->canHoldReservation()): ?>
                    <?php if(!$tour['status'] || (date('Y-m-d',strtotime($tour['gather_date'])) < date('Y-m-d'))): ?>
                        <button class="btn_sm btn-xs btn-primary">Book</button>
                    <?php else: ?>
                        <button class="btn_sm btn-xs btn-primary btn_action">Book</button>
                    <?php endif;?>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="tour_progress">
            <?php
            $total = isset($tour['quantity']) ? (int)$tour['quantity'] : 0;
            ?>
            <div class="bar" style="width: <?php echo $total ? ($total-$_tour_available)*100/$total : 0 ?>%;"></div>
        </div>
        <div class="clearfix"></div>
        <div class="table_list history" style="display: none">
            <?php
            $i = 1;
            ?>
            <table class="booking_table">
                <caption class="tour_header c_grey">History</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale</th>
                    <th>Action</th>
                    <th>Qty</th>
                    <th>Customer / <br>Phone number</th>
                    <th>Booking Code</th>
                    <th>Update Time</th>
                    <th>Due</th>
                    <th>Note</th>
                    <th>Updated By</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php $count_item = 0; ?>
                <?php while($reservation_history && ($history = db_fetch_array($reservation_history))): $count_item++; ?>
                    <?php
                    $reservation_current = BookingReservation::lookup($history['booking_reservation_id']);
                    if ($reservation_current) {
                        $reservation_cache[ $reservation_current->id ] = true;
                    }
                    ?>
                    <tr class="history_item
                        <?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue';
                        elseif (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= (time()+ TIME_NEAR_DUE_RESERVATION*3600)) echo "near_overdue"?>
                        " data-id="<?php echo $history['booking_reservation_id'] ?>">
                        <td><?php echo $i++; ?></td>
                        <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
                        <td>
                            <?php if((!$history['hold_qty'] && !$history['sure_qty']) || (strpos($history['note'], 'reason') !== false)): ?>
                                Hủy
                            <?php else: ?>
                                <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo 'Giữ'; ?>
                                <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo 'Sure'; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if(($history['hold_qty'] || $history['sure_qty']) && (strpos($history['note'], 'reason') === false)):?>
                                <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?>
                                <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?>
                            <?php endif; ?>
                        </td>
                        <td><?php echo $history['customer_name'] ?>
                            <br><?php echo $history['phone_number'] ?></td>
                        <td><?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?></td>
                        <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?></td>
                        <td>
                            <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at']));
                            ?>
                            <?php if( (!(isset($history['hold_qty']) && $history['hold_qty']) && !(isset($history['sure_qty']) && $history['sure_qty']))
                                || (strpos($history['note'], 'reason') !== false)): ?>
                                (cancel)
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo $history['note'] ?>
                            <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                                <label class="label label-default label-small" for="">Visa Only</label>
                            <?php endif; ?>
                            <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                                <label class="label label-default label-small" for="">Security Deposit</label>
                            <?php endif; ?>
                            <?php if (isset($history['fe']) && $history['fe']): ?>
                                <label class="label label-default label-small" for="">FE</label>
                            <?php endif; ?>
                            <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                                <label class="label label-default label-small" for="">Visa Ready</label>
                            <?php endif; ?>
                            <?php if (isset($history['one_way']) && $history['one_way']): ?>
                                <label class="label label-default label-small" for="">One Way</label>
                            <?php endif; ?>
                            <?php if(isset($history['infant']) && $history['infant']): ?>
                                [ Infant: <?php echo $history['infant'] ?> ]
                            <?php endif; ?>
                        </td>
                        <td><?php if (isset($all_staff[ $history['created_by'] ])) echo $all_staff[ $history['created_by'] ]; else echo 'Tugo'; ?></td>
                        <td>
                            <?php if(isset($reservation_cache[ $history['booking_reservation_id'] ])
                                && $reservation_cache[ $history['booking_reservation_id'] ]
                                && ((isset($history['hold_qty']) && $history['hold_qty']) || (isset($history['sure_qty']) && $history['sure_qty']))): ?>
                                <a href="<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id=<?php echo $history['id'] ?>"
                                   class="btn_sm btn-default btn-xs" data-title="Jump to reservation"><i class="icon-eye-open"></i></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endwhile; ?>
                <?php if(!$count_item): ?>
                    <tr><td>No Item</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="table_list upcoming" style="display: none">

        </div>
        <div class="clearfix"></div>
        <div class="table_list details" style="<?php if ( !(isset($_REQUEST['reservation_id']) && (int)$_REQUEST['reservation_id']) ) echo 'display: none;' ?> ">
            <?php
            $i = 1;
            ?>
            <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
            <table class="booking_table current_reservation">
                <caption class="tour_header c_green">Current Reservations</caption>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale</th>
                    <th>Hold</th>
                    <th>Sure</th>
                    <th>Customer / <br>Phone number</th>
                    <th>Booking Code</th>
                    <th>Update Time</th>
                    <th>Due</th>
                    <th>Note</th>
                    <th>Created By</th>
                    <th>Updated By</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php $count_item = 0; ?>
                <?php while($reservations && ($history = db_fetch_array($reservations))): $count_item++; ?>
                    <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue';
                    elseif (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= (time()+ TIME_NEAR_DUE_RESERVATION*3600)) echo "near_overdue"?>
                    <?php if (isset($_REQUEST['reservation_id']) && (int)$_REQUEST['reservation_id'] && $_REQUEST['reservation_id'] == $history['id'])
                        echo ' current' ?>
                    ">
                        <td><?php echo $i++; ?></td>
                        <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
                        <td>
                            <?php if((int)$history['hold_qty'] === 0) echo '-' ?>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input  data-ori="<?php echo $history['hold_qty'] ?>" style="display: none" form="action_form_<?php echo $history['id'] ?>" type="number" name="hold_qty" class="input-field" width="2" step="1" min="0" max="50" value="<?php
                                if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?><?php
                                ?>">
                            <span class="<?php if($history['over'] && $history['hold_qty']) echo "label label-danger"?>">
                                <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?>
                            </span>
                            <?php else: ?>
                                <span class="<?php if($history['over'] && $history['hold_qty']) echo "label label-danger"?>">
                                    <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; ?>
                                </span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if((int)$history['sure_qty'] === 0) echo '-' ?>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['sure_qty'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="number"
                                       name="sure_qty" class="input-field" width="2" step="1" min="0" max="50" value="<?php
                                if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty'];
                                ?>">
                            <span><?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['customer_name'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       name="customer_name" class="input-field" value="<?php
                                if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?><?php
                                ?>">
                                <span><?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
                            <?php endif; ?>
                            <br>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php echo $history['phone_number'] ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       name="phone_number" class="input-field" width="11" value="<?php
                                if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?><?php
                                ?>">
                                <span><?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?>
                            <?php endif; ?>
                        </td>
                        <td><?php if (isset($history['booking_code']) && $history['booking_code']):
                            $booking_check = Booking::lookup(['booking_code' => $history['booking_code']]); ?>
                                <a class="no-pjax" target="_blank" href="<?php echo $cfg->getUrl().'scp/new_booking.php?id='.$booking_check->ticket_id;?>"><?php echo $history['booking_code']; ?></a>
                            <?php endif;?>
                        </td>
                        <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?></td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <input data-ori="<?php if (isset($history['due_at']) && strtotime($history['due_at']))
                                    echo date('d/m/Y', strtotime($history['due_at'])); ?>" style="display: none"
                                       form="action_form_<?php echo $history['id'] ?>" type="text"
                                       class="dp_due new input-field" name="due_at" value="<?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>">
                            <span>
                                <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>
                            </span>
                            <?php else: ?>
                                <?php
                                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                                    echo date('d/m', strtotime($history['due_at'])); ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                <textarea data-ori="<?php echo $history['note'] ?>" style="display: none"
                                          form="action_form_<?php echo $history['id'] ?>" name="note" class="input-field"><?php
                                    if (isset($history['note']) && $history['note']) echo $history['note']; ?></textarea>
                                <span><?php if (isset($history['note']) && $history['note']) echo $history['note']; ?></span>
                            <?php else: ?>
                                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                            <?php endif; ?>
                            <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                                <label class="label label-default label-small" for="">Visa Only</label>
                            <?php endif; ?>
                            <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                                <label class="label label-default label-small" for="">Security Deposit</label>
                            <?php endif; ?>
                            <?php if (isset($history['fe']) && $history['fe']): ?>
                                <label class="label label-default label-small" for="">FE</label>
                            <?php endif; ?>
                            <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                                <label class="label label-default label-small" for="">Visa Ready</label>
                            <?php endif; ?>
                            <?php if (isset($history['one_way']) && $history['one_way']): ?>
                                <label class="label label-default label-small" for="">One Way</label>
                            <?php endif; ?>

                            <?php if(isset($history['infant']) && $history['infant']): ?>
                                [ Infant: <?php echo $history['infant'] ?> ]
                            <?php endif; ?>
                        </td>

                        <td><?php if (isset($all_staff[ $history['created_by'] ])) echo $all_staff[ $history['created_by'] ]; else echo 'Tugo'; ?></td>
                        <td><?php if (isset($all_staff[ $history['updated_by'] ])) echo $all_staff[ $history['updated_by'] ]; ?></td>
                        <td>
                            <form action="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php" method="post" name="action_form_<?php echo $history['id'] ?>">
                                <input type="hidden" name="reservation_id" value="<?php echo $history['id'] ?>">
                                <input type="hidden" name="tour_id" value="<?php echo $history['tour_id'] ?>">
                                <?php if($thisstaff->getId() == $history['staff_id']): ?>
                                    <?php if($thisstaff->canHoldReservation()):?>
                                        <button type="button" class="btn_sm btn-default btn-xs edit_btn">Edit</button>
                                        <a class="btn btn-default btn_sm btn-xs no-pjax" target="_blank" href="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php?transfer_date=1&reservation_id=<?php echo $history['id'] ?>&tour_id=<?php echo $tour['id']?>">Transfer Date</a>
                                        <button type="button" class="btn_sm btn-default btn-xs sure_all_btn">Sure All</button>
                                        <button type="button" class="btn_sm btn-default btn-xs extend_btn">Extend</button>
                                    <?php endif;?>
                                    <?php if($thisstaff->canCancelMemberReservation()): ?>
                                        <?php if((int)$history['hold_qty'] > 0 && (int)$history['sure_qty'] > 0): ?>
                                            <button type="button" class="btn_sm btn-default btn-xs cancel_all_btn">Cancel All</button>
                                        <?php endif; ?>
                                        <?php if((int)$history['hold_qty'] > 0): ?>
                                            <button type="button" class="btn_sm btn-default btn-xs cancel_hold_btn">Cancel Hold</button>
                                        <?php endif;?>
                                        <?php if((int)$history['sure_qty'] > 0): ?>
                                            <button type="button" class="btn_sm btn-default btn-xs cancel_sure_btn">Cancel Sure</button>
                                        <?php endif; ?>
                                    <?php endif;?>
                                    <?php if((int)$history['hold_qty'] === 0 && (int)$history['sure_qty'] > 0 && $history['booking_code']):?>
                                        <button type="button" class="btn_sm btn-default btn-xs approval_request_btn">Approve Request</button>
                                    <?php endif;?>

                                <?php elseif(isset($all_member_staff[$history['staff_id']]) && $thisstaff->canHoldReservation()): ?>
                                    <button type="button" class="btn_sm btn-default btn-xs edit_btn">Edit</button>
                                <?php endif; ?>

                                <?php if($thisstaff->canCancelSuperReservation() || ($thisstaff->canCancelLeaderReservation() && $all_member_staff[$history['staff_id']])): ?>
                                    <?php if((int)$history['hold_qty'] > 0 && (int)$history['sure_qty'] > 0): ?>
                                        <button type="button" class="btn_sm btn-default btn-xs cancel_all_btn">Cancel All</button>
                                    <?php endif; ?>
                                    <?php if((int)$history['hold_qty'] > 0): ?>
                                        <button type="button" class="btn_sm btn-default btn-xs cancel_hold_btn">Cancel Hold</button>
                                    <?php endif;?>
                                    <?php if((int)$history['sure_qty'] > 0): ?>
                                        <button type="button" class="btn_sm btn-default btn-xs cancel_sure_btn">Cancel Sure</button>
                                    <?php endif; ?>
                                <?php endif;?>

                                <?php if($thisstaff->canCreateBookings() && !$history['booking_code']):?>
                                    <a href="<?php echo $cfg->getUrl().'scp/new_booking.php?action=create&reservation_id='.$history['id'].'&from_view=reservation' ?>"
                                       class="btn_sm btn-default btn-xs no-pjax" target="_blank">Create Booking</a>
                                <?php endif;?>
                            </form>
                        </td>
                    </tr>
                <?php endwhile; ?>
                <?php if(!$count_item): ?>
                    <tr><td>No Item</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="action" style="display: none">
            <form action="<?php echo $cfg->getBaseUrl() ?>/scp/reservation.php"
                  method="post" id="<?php echo $tour['id'] ?>"
                  target="hold_frame__<?php echo $tour['id'] ?>">
                <input type="hidden" name="tour_id" value="<?php echo $tour['id'] ?>">
                <input type="hidden" name="action" value="hold">
                <input type="hidden" name="code" value="<?php $code = md5(uniqid('__123321!@!@!@', true)); echo $code; ?>">
                <input type="hidden" name="check" value="<?php echo md5($tour['id'].$code) ?>">
                <?php csrf_token() ?>
                <table>
                    <caption>Book a Reservation</caption>
                    <tr>
                        <td><strong>Quantity *</strong></td>
                        <td>
                            Hold <input type="number" class="input-field" max="40" min="0" step="1" name="hold_qty" value="0">
                            - Sure <input type="number" class="input-field" max="40" min="0" step="1" name="sure_qty" value="0">
                        </td>

                        <td><strong>Staff *</strong></td>
                        <td>
                            <select name="staff_id" id="" class="input-field">
                                <option value=>- Select -</option>
                                <?php
                                if($users) {
                                    foreach($users as $id => $name) {
                                        $s = " ";
                                        if ($id == $thisstaff->getId())
                                            $s="selected";
                                        echo sprintf('<option value="%s" %s>%s</option>', $id, $s, $name);
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><strong>Customer Name *</strong></td>
                        <td>
                            <input type="text" class="input-field" name="customer_name">
                        </td>
                        <td><strong>Phone number *</strong></td>
                        <td>
                            <input type="text" class="input-field" name="phone_number">
                        </td>
                    </tr>

                    <tr>
                        <td><strong>Due date *</strong></td>
                        <td>
                            <input type="text" class="input-field" readonly
                                   name="due" value="<?php echo date('d/m/Y', time()+ MAX_TIME_EXTEND_RESERVATION*3600) ?>">
                        </td>
                        <td>Booking Code</td>
                        <td>
                            <input type="text" name="booking_code" value="" class="input-field">
                        </td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td>
                            <label class="label label-default" for=""><input name="fe" value="1" type="checkbox"> FE</label>
                            <label class="label label-default" for=""><input name="security_deposit" value="1" type="checkbox"> Security Deposit</label>
                            <label class="label label-default" for=""><input name="visa_ready" value="1" type="checkbox"> Visa Ready</label>
                            <label class="label label-default" for=""><input name="one_way" value="1" type="checkbox"> One way</label>
                        </td>
                        <td>Infant</td>
                        <td>
                            <input type="number" step="1" min="0" max="10" name="infant" class="input-field" value="0">
                        </td>
                    </tr>
                    <tr>


                        <td>Note</td>
                        <td>
                            <textarea name="note" id="" cols="30" rows="10" class="input-field"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><em>Các mục có dấu * là bắt buộc</em></td>
                        <td colspan="2">
                            <button class="btn_sm btn-primary submit_btn"
                                    name="action" value="hold" type="submit">Book</button>

                        </td>
                    </tr>
  
                    <?php $tour_net_price = TourNetPrice::get_tour_net_price($tour['id']); ?>
                    <?php if(!empty($tour_net_price)): ?>
                        <?php while($tour_net_price && ($row = db_fetch_array($tour_net_price))): ?>
                            <?php $max_quantity = TourNetPrice::max_quantity($tour['id']);?>
                            <?php if($row['quantity'] < $max_quantity): ?>
                                <tr>
                                    <td>Số lượng  <=</td>
                                    <td>
                                        <input type="text" style="width: 20px;" maxlength="3" class="input-field"  name="quantity" value ="<?php echo $row['quantity'] ?>" readonly>
                                        <span>Giá</span>
                                                <input type="text" style="width: 100px;" class="input-field retail_price" min="0" id="retail_price" name="retail_price" value="<?php echo $row['retail_price'] ?>" readonly>
                                    </td>
                                </tr>
                            <?php else: ?>
                            <tr>
                                <td>Trường hợp còn lại, Giá</td>
                                <td>                                      
                                    <input type="text" class="input-field" style="width : 160px;" min="0" id="retail_price" name="retail_price" value="<?php echo $row['retail_price'] ?>" readonly>
                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif?>
                </table>
            </form>
            <iframe class="hold_frame" src="" frameborder="0" name="hold_frame__<?php echo $tour['id'] ?>">

            </iframe>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
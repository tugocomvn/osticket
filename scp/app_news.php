<?php
/*********************************************************************
logs.php

System Logs

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('admin.inc.php');
include_once INCLUDE_DIR.'tugo.news.php';

$errors = [];
$page='app_news.inc.php';
$referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/app_news.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/app_news.php';

if (isset($_REQUEST['a']) && $_REQUEST['a']) {
    $news_id = $news = null;

    switch (strtolower($_REQUEST['a'])) {
        case 'add':
            $page='app_news.add.inc.php';
            break;
        case 'edit':
            if (isset($_REQUEST['id']) && intval($_REQUEST['id']))
                $news_id = intval($_REQUEST['id']);
            else {
                $errors['err']=__('Invalid ID');
                break;
            }

            $page='app_news.add.inc.php';
            $news = \Tugo\News::lookup($news_id);

            if (!$news) {
                $errors['err']=__('Invalid News');
                break;
            }
            break;
        case 'save':
            if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
                $errors['err']=__('Invalid method');
                break;
            }

            if (!isset($_REQUEST['title']) || empty(trim($_REQUEST['title'])))
                $errors['save'][] = 'Title is required';
            if (!isset($_REQUEST['content']) || empty(trim($_REQUEST['content'])))
                $errors['save'][] = 'Content is required';

            if ($errors) break;

            $data = [
                'title' => trim($_REQUEST['title']),
                'content' => trim($_REQUEST['content']),
                'user_uuid' => trim($_REQUEST['user_uuid']),
            ];
            if (isset($_REQUEST['id']) && intval($_REQUEST['id'])) {
                $news_id = intval($_REQUEST['id']);
                $news = \Tugo\News::lookup($news_id);

                if (!$news) {
                    $errors['err']=__('Invalid News');
                    break;
                } else {
                    $news->setAll($data);
                }
            } else {
                $data['created_at'] = new \SqlFunction('NOW');
                $news = \Tugo\News::create($data);
            }

            $news->save(true);
            FlashMsg::set('ticket_create', 'Save successfully');
            $referer = isset($_REQUEST['referer']) && strpos($_REQUEST['referer'], '/scp/app_news.php') !== false
                ? $_REQUEST['referer'] : '/scp/app_news.php';
            header('Location: '.$referer);
            exit;
        case 'delete':
            if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
                $errors['err']=__('Invalid method');
                break;
            }

            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = sprintf(__('You must select at least %s'),
                                         __('one news entry'));
                break;
            } else {
                $count=count($_POST['ids']);
                $news_ids = [];

                foreach ($_POST['ids'] as $_id) {
                    $news_ids[] = intval(trim($_id));
                }
                $news_ids = array_filter($news_ids);
                if (!$news_ids) {
                    $errors['err'] = sprintf(__('You must select at least %s'),
                                             __('one news entry'));
                    break;
                }
                $sql=' DELETE FROM api_news WHERE id IN ('.implode(',', db_input($news_ids)).')';

                if(db_query($sql) && ($num=db_affected_rows())){
                    if($num==$count)
                        $msg=sprintf(__('Successfully deleted %s'),
                                     _N('selected news entry', 'selected news entries', $count));
                    else
                        $warn=sprintf(__('%1$d of %2$d %3$s deleted'), $num, $count,
                                      _N('selected news entry', 'selected news entries', $count));
                } elseif(!$errors['err'])
                    $errors['err']=sprintf(__('Unable to delete %s'),
                                           _N('selected news entry', 'selected news entries', $count));
            }

            FlashMsg::set('ticket_create', $msg);

            header('Location: '.$referer);
            exit;
        default:
            break;
    }
}

if ($errors && isset($errors['save'])) {
    $page='app_news.add.inc.php';
} elseif ($errors) {
    $page='app_news.inc.php';
}

$nav->setTabActive('tugo_app');
$ost->addExtraHeader('<meta name="tip-namespace" content="tugo_app.app_news" />',
                     "$('#content').data('tipNamespace', 'tugo_app.app_news');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


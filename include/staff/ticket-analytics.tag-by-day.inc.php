<?php
$tag_dates = [];
while($chart_data && ($row = db_fetch_array($chart_data))) {
    if (!isset($tag_by_day_chart[ $row['content'] ]))
        $tag_by_day_chart[ $row['content'] ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $tag_dates[] = $row['week'].'-'.$row['year'];
            $tag_by_day_chart[ $row['content'] ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $tag_dates[] = $row['month'].'-'.$row['year'];
            $tag_by_day_chart[ $row['content'] ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $tag_dates[] = date(DATE_FORMAT, strtotime($row['date']));
            $tag_by_day_chart[ $row['content'] ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}
$tag_dates = array_filter(array_unique($tag_dates));



foreach ($tag_by_day_chart as $_source => $_date_data) {
    foreach ($tag_dates as $_date) {
        if (!isset($tag_by_day_chart[ $_source ][ $_date ]))
            $tag_by_day_chart[ $_source ][ $_date ] = 0;
    }

    uksort($tag_by_day_chart[ $_source ], "___date_compare");
}

?>
<td colspan="2">
    <h2>Tags by day <button class="btn_sm btn-xs btn-default" onclick="hide_all('tags_chart_by_day')">Hide All</button></h2>
    <canvas id="tags_by_day" width="1000" height="500"></canvas>
</td>

<script>
    <?php $i = 0; ?>
    var colors = poolColors(<?php echo count($tag_by_day_chart) ?>);
    var tags_chart_by_day_elm = document.getElementById("tags_by_day").getContext('2d');
    var tags_chart_by_day = new Chart(tags_chart_by_day_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($tag_by_day_chart as $_type => $_date): ?>
                {
                    label: '<?php echo htmlspecialchars($_type, ENT_QUOTES) ?>',
                    backgroundColor:  colors[<?php echo $i++ ?>],
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    fill: true
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>
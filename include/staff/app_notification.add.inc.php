<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

?>
<style>
    table input {
        width: 90%;
    }

    table textarea {
        width: 90%;
        height: 50px;
        resize: none;
    }

</style>

<div class="pull-left" style="width:700;padding-top:5px;">
    <h2><?php echo __('Push Notification'); ?></h2>
    <p><a href="app_notification.php" class="btn_sm btn-default">Back</a></p>
</div>

<div class="clear"></div>

<form action="app_notification.php" method="POST" name="lists">
    <?php csrf_token(); ?>
    <input type="hidden" name="a" value="send" >
    <input type="hidden" name="id" value="<?php echo $news_id ?>" >
    <?php
    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/app_notification.php') !== false
        ? $_SERVER['HTTP_REFERER'] : '/scp/app_notification.php';
    ?>
    <input type="hidden" name="referer" value="<?php echo $referer ?>">
    <table width="1058px">
        <tr>
            <td>Title</td>
            <td width="80%"><input class="input-field" required type="text" name="title" value="<?php
                if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['title'])) echo $_REQUEST['title'];
                elseif (isset($news) && $news) echo $news->title;
                else echo 'Du lịch Tugo';
                ?>"></td>
        </tr>
        <tr>
            <td>Content</td>
            <td><textarea class="input-field" name="content" required id="" cols="30" rows="5"><?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['content'])) echo $_REQUEST['content'];
                    elseif (isset($news) && $news) echo $news->content;
                    else echo 'Hàn Quốc mùa đông';
                ?></textarea></td>
        </tr>
        <tr>
            <td>URL</td>
            <td>
                <input class="input-field" type="text" name="url" value="<?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['url'])) echo $_REQUEST['url'];
                    elseif (isset($news) && $news) echo $news->url
                    ?>">
            </td>
        </tr>
        <tr>
            <td>User UUID</td>
            <td>
                <input class="input-field" type="text" name="user_uuid" value="<?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['user_uuid'])) echo $_REQUEST['user_uuid'];
                    elseif (isset($news) && $news) echo $news->user_uuid
                    ?>">
            </td>
        </tr>
        <tr>
            <td>Cloud Message Token</td>
            <td>
                <input class="input-field" type="text" name="cloud_message_token" value="<?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['cloud_message_token'])) echo $_REQUEST['cloud_message_token'];
                    elseif (isset($news) && $news) echo $news->cloud_message_token;
                    else echo 'feaT3kvtWqA:APA91bG1pxyKsiCr1waH38ssAz3rPtanBWN4qzu02GGvAACiajzhVnW7BQoSNNN1uKFym8hJPkKdEbS8Xs7Bk1cSeQJh0_j61dkZMEFeca8Mk-TJOFhwomdEc9lsOp5MKqpnCpc6P2GB';
                    ?>">
            </td>
        </tr>
        <tr>
            <td>Phone Number</td>
            <td>
                <input class="input-field" type="text" name="phone_number" value="<?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['phone_number'])) echo $_REQUEST['phone_number'];
                    elseif (isset($news) && $news) echo $news->phone_number
                    ?>">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a href="app_notification.php" class="btn_sm btn-default">Cancel</a> <button class="btn_sm btn-primary">Send</button>
            </td>
        </tr>
    </table>
</form>

<?php

class Tag {
    public static function getAllTags() {
        $sql = "SELECT DISTINCT id, content FROM ".TAG_TABLE." ORDER by content";
        $data = [];
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $data[ $row['id'] ] = $row['content'];
        }

        return $data;
    }

    public static function getAllTagIds() {
        $sql = "SELECT DISTINCT id, content FROM ".TAG_TABLE." ORDER by content";
        $data = [];
        $res = db_query($sql);
        while ($res && ($row = db_fetch_array($res))) {
            $data[ $row['content'] ] = $row['id'];
        }

        return $data;
    }

    public static function update($ticket_id, array $tags, $add = false) {
        db_start_transaction();
        try {
            $tag_string = [];
            foreach ($tags as $tag) {
                $tag_string[] = db_input($tag);
                $sql = sprintf(
                    "INSERT IGNORE INTO %s SET content=%s",
                    TAG_TABLE,
                    db_input($tag)
                );
                db_query($sql);
            }

            $sql = sprintf(
                "SELECT id FROM %s WHERE content IN(".implode(',', $tag_string).")",
                TAG_TABLE
            );

            $res = db_query($sql);
            $tag_id = [];

            if ($res) {
                while (($row = db_fetch_array($res))) {
                    $tag_id[] = $row['id'];
                }
            }

            if (!$add) {
                $sql = sprintf(
                    "DELETE FROM %s WHERE ticket_id = %s",
                    TICKET_TAG_TABLE,
                    db_input($ticket_id)
                );
                db_query($sql);
            }

            foreach ($tag_id as $tag) {
                $sql = sprintf(
                    "INSERT IGNORE INTO %s SET ticket_id=%s, tag_id=%s",
                    TICKET_TAG_TABLE,
                    db_input($ticket_id), db_input($tag)
                );
                db_query($sql);
            }

            db_commit();
        } catch(Exception $ex) {
            db_rollback();
        }
    }

    public static function get($ticket_id) {
        $sql = sprintf(
            "SELECT DISTINCT content FROM %s c JOIN %s t ON c.id=t.tag_id WHERE t.ticket_id=%s",
            TAG_TABLE,
            TICKET_TAG_TABLE,
            db_input($ticket_id)
        );
        return db_query($sql);
    }

    public static function getKeyword($keyword = null) {
        $where = $keyword ? sprintf( " WHERE t.content LIKE %s ", db_input('%'.$keyword.'%') ) : " ";
        $sql = sprintf(
            " SELECT DISTINCT content FROM %s t %s LIMIT 10 ",
            TAG_TABLE, $where
        );

        return db_query($sql);
    }

    public static function get_tag_label_class($length) {
        switch ($length%5) {
            case 1:
                return 'label label-primary';
            case 2:
                return 'label label-danger';
            case 3:
                return 'label label-success';
            case 4:
                return 'label label-default';
            case 5:
                return 'label label-warning';
            default:
                return 'label label-default';
        }
    }
}

class TagIndexing {
    public static $tag = null;
    public static function getTags() {
        if (!static::$tag) {
            static::$tag = require_once(INCLUDE_DIR.'myconfig/tags.keywords.php');
        }
        return static::$tag;
    }

    public static function getTokens($content) {

    }

    public static function getFirstTag($content) {
        $tags = self::getTags();
        $content = mb_strtolower($content);
        $content = trim($content);
        $content = preg_replace('/\s+/', ' ', $content);
        foreach ($tags as $tag => $list) {
            foreach ($list as $string) {
                $string = trim($string);
                if (mb_stripos($content, $string) !== false)
                    return $tag;
            }
        }

        return null;
    }

    public static function getAllTags($content) {
        $tags = self::getTags();
        $result = [];
        $content = mb_strtolower($content);
        $content = trim($content);
        $content = preg_replace('/\s+/', ' ', $content);

        if (isset($tags) && is_array($tags)) {
            foreach ($tags as $tag => $list) {
                foreach ($list as $string) {
                    $string = trim($string);
                    if (mb_stripos($content, $string) !== false)
                        $result[] = $tag;
                }
            }
        } else {
            @file_put_contents('/var/log/httpd/osticket/tag'
                               .date('Y-m-d-H-i-s').'.txt',
                               json_encode($tags)
                               ."\r\n".file_exists(INCLUDE_DIR.'myconfig/tags.keywords.php')
                               ."\r\n".file_get_contents(INCLUDE_DIR.'myconfig/tags.keywords.php'));
        }

        $result = array_filter($result);
        $result = array_unique($result);

        return $result;
    }
}

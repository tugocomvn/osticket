<?php
namespace Tugo;
class Token {
    public static function getToken() {
        if ($token = static::getTokenFromHeaders())
            return $token;
        return static::getTokenFromRequest();
    }

    public static function getTokenFromRequest() {
        if (!isset($_REQUEST['token']) || empty(trim($_REQUEST['token'])))
            return null;

        return trim($_REQUEST['token']);
    }

    public static function getTokenFromHeaders() {
        if (!isset($_SERVER["HTTP_AUTHORIZATION"]) || empty(trim($_SERVER["HTTP_AUTHORIZATION"])))
            return null;

        list($type, $data) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"], 2);
        if (strcasecmp($type, "Bearer") != 0)
            return null;

        return trim($data);
    }
}

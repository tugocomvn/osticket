CREATE TABLE ost_uuid
(
    `type` VARCHAR(64),
    uuid_value VARCHAR(128)
);
CREATE UNIQUE INDEX ost_uuid_type_uindex ON ost_uuid (`type`);

CREATE TABLE ost_uuid_tmp
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    `type` VARCHAR(64),
    uuid_value VARCHAR(128)
);

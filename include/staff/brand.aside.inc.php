<!-- Brand Logo -->
<a href="index3.html" class="brand-link">
    <img src="<?php echo $cfg->getUrl() ?>/images/icons/icon-512x512.png" alt="Tugo BI" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Tugo BI</span>
</a>
<?php
$____start = microtime(1);
/*************************************************************************
    tickets.php

    Handles all tickets related actions.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.dept.php');
require_once(INCLUDE_DIR.'class.filter.php');
require_once(INCLUDE_DIR.'class.canned.php');
require_once(INCLUDE_DIR.'class.json.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.export.php');       // For paper sizes
require_once(INCLUDE_DIR . 'class.signal.php');
require_once(INCLUDE_DIR . 'class.uuid.php');
require_once(INCLUDE_DIR . 'class.booking.php');
require_once(INCLUDE_DIR . 'class.offlate.php');
require_once(INCLUDE_DIR.'class.ticket-return.php');
require_once INCLUDE_DIR.'class.receipt.php';

$page='';
$ticket = $user = $errors = null; //clean start.
//LOCKDOWN...See if the id provided is actually valid and if the user has access.
if($_GET && $_GET['action'] == 'getPoint'){
    $total = 0;
    $arr_point = '';
    if(isset($_GET['uuid']) && $_GET['uuid']) {
        $results = Tugo\Point::get($_GET['uuid'], $total);
        if(!empty($results))
        {
            foreach ($results as &$data){
                $data['date'] = date('d/m/Y H:i:s',strtotime($data['date']));
            }
            $arr_point = $results;
        }
    }
    echo json_encode($arr_point);
    exit();
}
if($_REQUEST['id']) {
    if(!($ticket=Ticket::lookup($_REQUEST['id'])))
         $errors['err']=sprintf(__('%s: Unknown or invalid ID.'), __('ticket'));
    elseif(!$ticket->checkStaffAccess($thisstaff)) {
        $errors['err']=__('Access denied. Contact admin if you believe this is in error');
        $ticket=null; //Clear ticket obj.
    }
}

//Lookup user if id is available.
if ($_REQUEST['uid']) {
    $user = User::lookup($_REQUEST['uid']);
}
elseif (!isset($_REQUEST['advsid']) && @$_REQUEST['a'] != 'search'
    && !isset($_REQUEST['status']) && isset($_SESSION['::Q'])
) {
    $_REQUEST['status'] = $_SESSION['::Q'];
}
// Configure form for file uploads
$response_form = new Form(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:response',
        'configuration' => array('extensions'=>'')))
));
$note_form = new Form(array(
    'attachments' => new FileUploadField(array('id'=>'attach',
        'name'=>'attach:note',
        'configuration' => array('extensions'=>'')))
));

//At this stage we know the access status. we can process the post.
if($_POST && !$errors):
    if($ticket && $ticket->getId()) {
        $ticket_id = $ticket->getId();
        //More coffee please.
        $errors=array();
        $lock=$ticket->getLock(); //Ticket lock if any
        switch(strtolower($_POST['a'])):
        case 'reply':
            if(!$thisstaff->canPostReply())
                $errors['err'] = __('Action denied. Contact admin for access');
            else {

                if(!$_POST['response'])
                    $errors['response']=__('Response required');
                //Use locks to avoid double replies
                if($lock && $lock->getStaffId()!=$thisstaff->getId())
                    $errors['err']=__('Action Denied. Ticket is locked by someone else!');

                //Make sure the email is not banned
                if(!$errors['err'] && TicketFilter::isBanned($ticket->getEmail()))
                    $errors['err']=__('Email is in banlist. Must be removed to reply.');
            }

            //If no error...do the do.
            $vars = $_POST;
            $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();

            if(!$errors && ($response=$ticket->postReply($vars, $errors, $_POST['emailreply']))) {
                LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_REPLY'],
                               $ticket->getUserId(), $response && $response->getId() ? $response->getId() : null);

                $msg = sprintf(__('%s: Reply posted successfully'),
                        sprintf(__('Ticket #%s'),
                            sprintf('<a href="tickets.php?id=%d"><b>%s</b></a>',
                                $ticket->getId(), $ticket->getNumber()))
                        );

                // Clear attachment list
                $response_form->setSource(array());
                $response_form->getField('attachments')->reset();

                // Remove staff's locks
                TicketLock::removeStaffLocks($thisstaff->getId(),
                        $ticket->getId());

                // Cleanup response draft for this user
                Draft::deleteForNamespace(
                    'ticket.response.' . $ticket->getId(),
                    $thisstaff->getId());

                    
                header("Location: /scp/tickets.php?id=".$_REQUEST['id']);

            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to post the reply. Correct the errors below and try again!');
            }

            break;

        case 'handovered':
            if(!$thisstaff->canPostReply())
                $errors['err'] = __('Action denied. Contact admin for access');
            else {

                if(!$_POST['response'])
                    $errors['response']=__('Response required');
                //Use locks to avoid double replies
                if($lock && $lock->getStaffId()!=$thisstaff->getId())
                    $errors['err']=__('Action Denied. Ticket is locked by someone else!');

                //Make sure the email is not banned
                if(!$errors['err'] && TicketFilter::isBanned($ticket->getEmail()))
                    $errors['err']=__('Email is in banlist. Must be removed to reply.');
            }

            //If no error...do the do.
            $vars = $_POST;
            $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();
            $vars['handovered'] = true;

            if(!$errors && ($response=$ticket->postReply($vars, $errors, $_POST['emailreply']))) {
                LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_HANDOVER'],
                               $ticket->getUserId(), $response && $response->getId() ? $response->getId() : null);

                $msg = sprintf(__('%s: Reply posted successfully'),
                    sprintf(__('Ticket #%s'),
                        sprintf('<a href="tickets.php?id=%d"><b>%s</b></a>',
                            $ticket->getId(), $ticket->getNumber()))
                );

                // Clear attachment list
                $response_form->setSource(array());
                $response_form->getField('attachments')->reset();

                // Remove staff's locks
                TicketLock::removeStaffLocks($thisstaff->getId(),
                    $ticket->getId());

                // Cleanup response draft for this user
                Draft::deleteForNamespace(
                    'ticket.response.' . $ticket->getId(),
                    $thisstaff->getId());

                header("Location: /scp/tickets.php?id=".$_REQUEST['id']);

            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to post the reply. Correct the errors below and try again!');
            }

            break;
        
        case 'transfer': /** Transfer ticket **/
            //Check permission
            if(!$thisstaff->canTransferTickets())
                $errors['err']=$errors['transfer'] = __('Action Denied. You are not allowed to transfer tickets.');
            else {

                //Check target dept.
                if(!$_POST['deptId'])
                    $errors['deptId'] = __('Select department');
                elseif($_POST['deptId']==$ticket->getDeptId())
                    $errors['deptId'] = __('Ticket already in the department');
                elseif(!($dept=Dept::lookup($_POST['deptId'])))
                    $errors['deptId'] = __('Unknown or invalid department');

                //Transfer message - required.
                if(!$_POST['transfer_comments'])
                    $errors['transfer_comments'] = __('Transfer comments required');
                elseif(strlen($_POST['transfer_comments'])<5)
                    $errors['transfer_comments'] = __('Transfer comments too short!');

                //If no errors - them attempt the transfer.
                if(!$errors && $ticket->transfer($_POST['deptId'], $_POST['transfer_comments'])) {

                    LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_TRANSFER'], $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                    $msg = sprintf(__('Ticket transferred successfully to %s'),$ticket->getDeptName());
                    //Check to make sure the staff still has access to the ticket
                    if(!$ticket->checkStaffAccess($thisstaff))
                        $ticket=null;

                } elseif(!$errors['transfer']) {
                    $errors['err'] = __('Unable to complete the ticket transfer');
                    $errors['transfer']=__('Correct the error(s) below and try again!');
                }
            }
            break;
        case 'assign':

             if(!$thisstaff->canAssignTickets())
                 $errors['err']=$errors['assign'] = __('Action Denied. You are not allowed to assign/reassign tickets.');
             else {

                 $id = preg_replace("/[^0-9]/", "",$_POST['assignId']);
                 $claim = (is_numeric($_POST['assignId']) && $_POST['assignId']==$thisstaff->getId());
                 $dept = $ticket->getDept();

                 if (!$_POST['assignId'] || !$id)
                     $errors['assignId'] = __('Select assignee');
                 elseif ($_POST['assignId'][0]!='s' && $_POST['assignId'][0]!='t' && !$claim)
                     $errors['assignId']= sprintf('%s - %s',
                             __('Invalid assignee'),
                             __('get technical support'));
                 elseif ($_POST['assignId'][0]=='s'
                         && $dept->assignMembersOnly()
                         && !$dept->isMember($id)) {
                     $errors['assignId'] = sprintf('%s. %s',
                             __('Invalid assignee'),
                             __('Must be department member'));
                 } elseif($ticket->isAssigned()) {
                     if($_POST['assignId'][0]=='s' && $id==$ticket->getStaffId())
                         $errors['assignId']=__('Ticket already assigned to the agent.');
                     elseif($_POST['assignId'][0]=='t' && $id==$ticket->getTeamId())
                         $errors['assignId']=__('Ticket already assigned to the team.');
                 }

                 //Comments are not required on self-assignment (claim)
                 if(!$_POST['assign_comments'])
                     $_POST['assign_comments'] = sprintf(__('%s reassigned Ticket'),$thisstaff->getName());

                 if(!$errors && $ticket->assign($_POST['assignId'], $_POST['assign_comments'], !$claim)) {
                     if ($ticket->getStatus() == 1)
                         $ticket->setStatus(6);

                     LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_ASSIGN'],
                                    $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                     LogAction::log($id, $ticket->getId(), LogAction::ACTIONS['TICKET_OWN'],
                                    $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                     if($claim) {
                         LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_OWN'],
                                        $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                         $msg = __('Ticket is NOW assigned to you!');
                     } else {
                         $msg=sprintf(__('Ticket assigned successfully to %s'), $ticket->getAssigned());
                         TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());
                         $ticket=null;
                     }

                     header('Location: tickets.php?id=' . $ticket_id);
                     exit();
                 } elseif(!$errors['assign']) {
                     $errors['err'] = __('Unable to complete the ticket assignment');
                     $errors['assign'] = __('Correct the error(s) below and try again!');
                 }
             }
            break;
         case 'assign_ol':

             if(!$thisstaff->canEditOfflates() && !$thisstaff->canAcceptOfflates())
                 $errors['err']=$errors['assign'] = __('Action Denied. You are not allowed to approve/deny absence requests.');
             elseif ( $_POST['assignId'] != '-3' && $_POST['approve'] == '-1' ) {
                 $errors['assign'] = 'Nếu từ chối thì mục "Người tiếp theo sẽ duyệt" chọn là "Không duyệt"';
             }elseif ( $_POST['assignId'] == '-3' && $_POST['approve'] != '-1' ) {
                 $errors['assign'] = 'Nếu từ chối thì mục "Người tiếp theo sẽ duyệt" chọn là "Không duyệt" và bấm Deny';
             }
             elseif ( (!isset($_POST['assign_comments']) || empty(trim($_POST['assign_comments']))) && $_POST['approve'] == '-1' ) {
                 $errors['assign'] = 'Nếu từ chối thì phải ghi lý do vào mục comments';
             }
             else {

                 $id = preg_replace("/[^0-9]/", "",$_POST['assignId']);
                 $claim = (is_numeric($_POST['assignId']) && $_POST['assignId']==$thisstaff->getId());
                 $dept = $ticket->getDept();

                 if (!$_POST['assignId'] || !$id)
                     $errors['assignId'] = __('Select assignee');
                 elseif ($_POST['assignId'] == '-2' && $_POST['approve'] == '1') {
                     // TODO: trùm cuối
                 }
                 elseif ($_POST['assignId'] == '-3' && $_POST['approve'] == '-1') {
                     // TODO: đéo cho nghỉ
                 }
                 elseif ($_POST['assignId'][0]!='s' && $_POST['assignId'][0]!='t' && !$claim)
                     $errors['assignId']= sprintf('%s - %s',
                             __('Invalid assignee'),
                             __('get technical support'));
                 elseif ($_POST['assignId'][0]=='s'
                         && $dept->assignMembersOnly()
                         && !$dept->isMember($id)) {
                     $errors['assignId'] = sprintf('%s. %s',
                             __('Invalid assignee'),
                             __('Must be department member'));
                 }

                 $ol = Offlate::lookup($ticket->getId());
                 if (!$ol) {
                     $errors['approve'] = 'Invalid absence request';
                 }

                 //Comments are not required on self-assignment (claim)

                 $_comment = '';
                 if (isset($_POST['assign_comments'])) $_comment = $_POST['assign_comments'];

                 if ($_POST['approve'] == '1') {

                     if ($_POST['assignId'] == '-2') {
                         $_msg_approve = sprintf(ABSENCE_REQUEST_APPROVED_TITLE.'<hr />', $ticket->getNumber(), $thisstaff->getName(), $thisstaff->getUserName());
                         $_comment = $_msg_approve.$_comment;

                     } else {
                         $_staff = Staff::lookup($id);
                         $_msg_transfer = sprintf(
                             ABSENCE_REQUEST_TRANSFER_TITLE.'<hr />',
                             $ticket->getNumber(),
                             $thisstaff->getName(), $thisstaff->getUserName(),
                             $_staff->getName(), $_staff->getUserName()
                         );
                         $_comment = $_msg_transfer.$_comment;
                     }

                 } elseif ($_POST['approve'] == '-1') {
                     $_msg_deny = sprintf(ABSENCE_REQUEST_DENIED_TITLE.'<hr />', $ticket->getNumber(), $thisstaff->getName(), $thisstaff->getUserName());
                     $_comment = $_msg_deny.$_comment;

                 } else {
                     $errors['approve'] = 'Invalid information';
                 }

                 if(!$errors && $ticket->assign_ol($_POST['assignId'], $_comment)) {
                     TicketLock::removeStaffLocks($thisstaff->getId(), $ticket->getId());

                     $ol_status = $ol->get('ol_status');
                     if ($_POST['approve'] == '1') {
                         if (!$ol_status || $ol_status == -1) $ol_status = 0;
                         if ($ol_status == 0) $ol_status = 1;
                         if ($_POST['assignId'] == '-2') $ol_status = 2;
                         $ol->set('time_accept', time());
                         $ol->set('ol_status', $ol_status);
                     } elseif ($_POST['approve'] == '-1') {
                         $ol->set('ol_status', -1);
                         $ol->set('time_accept', '');
                     }

                     if ($ol) $ol->save();

                     $ticket=null;

                     $msg = sprintf("%s successfully", $_POST['approve'] == '1' ? 'Approve' : 'Deny');
                     FlashMsg::set('ticket_create', $msg);

                     header('Location: /scp/ol.php?list=need_approval');
                     exit();
                 } elseif(!$errors['assign']) {
                     $errors['err'] = __('Unable to complete the ticket assignment');
                     $errors['assign'] = __('Correct the error(s) below and try again!');
                 }
             }
            break;
        case 'postnote': /* Post Internal Note */
            $vars = $_POST;
            $attachments = $note_form->getField('attachments')->getClean();
            $vars['cannedattachments'] = array_merge(
                $vars['cannedattachments'] ?: array(), $attachments);

            $wasOpen = ($ticket->isOpen());
            if(($note=$ticket->postNote($vars, $errors, $thisstaff))) {
                LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_NOTE'],
                               $ticket->getUserId(), $note && $note->getId() ? $note->getId() : null);

                $msg=__('Internal note posted successfully');
                // Clear attachment list
                $note_form->setSource(array());
                $note_form->getField('attachments')->reset();

                if($wasOpen && $ticket->isClosed())
                    $ticket = null; //Going back to main listing.
                else
                    // Ticket is still open -- clear draft for the note
                    Draft::deleteForNamespace('ticket.note.'.$ticket->getId(),
                        $thisstaff->getId());

            } else {

                if(!$errors['err'])
                    $errors['err'] = __('Unable to post internal note - missing or invalid data.');

                $errors['postnote'] = __('Unable to post the note. Correct the error(s) below and try again!');
            }
            break;
        case 'sendsms': /* Send SMS */
            $vars = $_POST;

            if(($note=$ticket->sendSMS($vars, $errors, $thisstaff))) {
                LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_SMS'],
                               $ticket->getUserId(), $note && $note->getId() ? $note->getId() : null);

                $msg=__('SMS sent successfully');

            } else {
                if(!$errors)
                    $errors['err'] = __('Unable to send SMS - missing or invalid data.');
            }
            break;
        case 'edit-io':
            if(!$ticket || !$thisstaff->canEditPayments())
                $errors['err']=__('Permission Denied. You are not allowed to edit payments');
            elseif($ticket->updateIO($_POST,$errors)) {
                $msg=__('Payment updated successfully');
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffAccess($thisstaff))
                    $ticket=null;

                header('Location: /scp/tickets.php?id='.$_REQUEST['id']);
                exit;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the payment. Correct the errors below and try again!');
            }
            break;
        case 'edit-ol':
            if(!$ticket || !$thisstaff->canEditOfflates())
                $errors['err']=__('Permission Denied. You are not allowed to edit payments');
            elseif (!isset($_POST['assignId']) || !$_POST['assignId'])
                $errors['err']=__('Chọn người duyệt');
            elseif($ticket->updateOL($_POST,$errors)) {
                $msg=__('Payment updated successfully');
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffAccess($thisstaff))
                    $ticket=null;

                $msg = 'Update successfully.';
                FlashMsg::set('ticket_create', $msg);

                header('Location: /scp/tickets.php?id='.$_REQUEST['id']);
                exit;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the offlate. Correct the errors below and try again!');
            }
            break;
        case 'edit-booking':
            if(!$ticket || !$thisstaff->canEditBookings())
                $errors['err']=__('Permission Denied. You are not allowed to edit bookings');
            elseif($ticket->updateBooking($_POST,$errors)) {
                $msg=__('Booking updated successfully');
                $_REQUEST['a'] = null; //Clear edit action - going back to view.
                //Check to make sure the staff STILL has access post-update (e.g dept change).
                if(!$ticket->checkStaffAccess($thisstaff))
                    $ticket=null;

                header('Location: /scp/tickets.php?id='.$_REQUEST['id']);
                exit;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the booking. Correct the errors below and try again!');
            }
            break;
        case 'edit':
        case 'update':
            if(!$ticket || !$thisstaff->canEditTickets())
                $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
            elseif($ticket->update($_POST,$errors)) {
                LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_UPDATE'],
                               $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                $msg=__('Ticket updated successfully');
                $_REQUEST['a'] = null; //Clear edit action - going back to view.

                header("Location: /scp/tickets.php?id=".$_REQUEST['id']);
                exit;
            } elseif(!$errors['err']) {
                $errors['err']=__('Unable to update the ticket. Correct the errors below and try again!');
            }
            break;
        case 'process':
            switch(strtolower($_POST['do'])):
                case 'release':
                    if(!$ticket->isAssigned() || !($assigned=$ticket->getAssigned())) {
                        $errors['err'] = __('Ticket is not assigned!');
                    } elseif($ticket->release()) {
                        $msg=sprintf(__(
                            /* 1$ is the current assignee, 2$ is the agent removing the assignment */
                            'Ticket released (unassigned) from %1$s by %2$s'),
                            $assigned, $thisstaff->getName());
                        $ticket->logActivity(__('Ticket unassigned'),$msg);
                    } else {
                        $errors['err'] = __('Problems releasing the ticket. Try again');
                    }
                    break;
                case 'claim':
                    if(!$thisstaff->canAssignTickets()) {
                        $errors['err'] = __('Permission Denied. You are not allowed to assign/claim tickets.');
                    } elseif(!$ticket->isOpen()) {
                        $errors['err'] = __('Only open tickets can be assigned');
                    } elseif($ticket->isAssigned()) {
                        $errors['err'] = sprintf(__('Ticket is already assigned to %s'),$ticket->getAssigned());
                    } elseif ($ticket->claim()) {

                        LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_OWN'],
                                       $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                        $msg = __('Ticket is now assigned to you!');
                    } else {
                        $errors['err'] = __('Problems assigning the ticket. Try again');
                    }
                    break;
                case 'overdue':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets overdue');
                    } elseif($ticket->markOverdue()) {
                        $msg=sprintf(__('Ticket flagged as overdue by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Overdue'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket overdue. Try again');
                    }
                    break;
                case 'answered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markAnswered()) {
                        $msg=sprintf(__('Ticket flagged as answered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Answered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the the ticket answered. Try again');
                    }
                    break;
                case 'unanswered':
                    $dept = $ticket->getDept();
                    if(!$dept || !$dept->isManager($thisstaff)) {
                        $errors['err']=__('Permission Denied. You are not allowed to flag tickets');
                    } elseif($ticket->markUnAnswered()) {
                        $msg=sprintf(__('Ticket flagged as unanswered by %s'),$thisstaff->getName());
                        $ticket->logActivity(__('Ticket Marked Unanswered'),$msg);
                    } else {
                        $errors['err']=__('Problems marking the ticket unanswered. Try again');
                    }
                    break;
                case 'banemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err']=__('Permission Denied. You are not allowed to ban emails');
                    } elseif(BanList::includes($ticket->getEmail())) {
                        $errors['err']=__('Email already in banlist');
                    } elseif(Banlist::add($ticket->getEmail(),$thisstaff->getName())) {
                        $msg=sprintf(__('Email %s added to banlist'),$ticket->getEmail());
                    } else {
                        $errors['err']=__('Unable to add the email to banlist');
                    }
                    break;
                case 'unbanemail':
                    if(!$thisstaff->canBanEmails()) {
                        $errors['err'] = __('Permission Denied. You are not allowed to remove emails from banlist.');
                    } elseif(Banlist::remove($ticket->getEmail())) {
                        $msg = __('Email removed from banlist');
                    } elseif(!BanList::includes($ticket->getEmail())) {
                        $warn = __('Email is not in the banlist');
                    } else {
                        $errors['err']=__('Unable to remove the email from banlist. Try again.');
                    }
                    break;
                case 'changeuser':
                    if (!$thisstaff->canEditTickets()) {
                        $errors['err']=__('Permission Denied. You are not allowed to edit tickets');
                    } elseif (!$_POST['user_id'] || !($user=User::lookup($_POST['user_id']))) {
                        $errors['err'] = __('Unknown guest selected');
                    } elseif ($ticket->changeOwner($user)) {

                        LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['USER_EDIT'],
                                       $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);

                        $msg = sprintf(__('Ticket ownership changed to %s'),
                            Format::htmlchars($user->getName()));
                    } else {
                        $errors['err'] = __('Unable to change ticket ownership. Try again');
                    }
                    break;
                default:
                    $errors['err']=__('You must select action to perform');
            endswitch;
            break;
        default:
            $errors['err']=__('Unknown action');
        endswitch;
        if($ticket && is_object($ticket))
            $ticket->reload();//Reload ticket info following post processing
    }elseif($_POST['a']) {

        switch($_POST['a']) {
            case 'open':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreateTickets()) {
                     $errors['err'] = sprintf('%s %s',
                             sprintf(__('You do not have permission %s.'),
                                 __('to create tickets')),
                             __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;

                    $vars['cannedattachments'] = $response_form->getField('attachments')->getClean();

                    if(($ticket=Ticket::open($vars, $errors))) {
                        LogAction::log($thisstaff->getId(), $ticket->getId(), LogAction::ACTIONS['TICKET_CREATE'],
                                       $ticket->getUserId(), $_SESSION['___THREAD_ID'] ?: null);
                        $user_id = TicketReturn::get_user_id_from_phone_number($vars['phone']);
                        if($user_id){
                            $arr_ticket_id = TicketReturn::get_ticket_id_from_user($user_id);
                            if(is_array($arr_ticket_id)) {
                                $total_ticket_successful = TicketReturn::get_total_ticket_count_successful($arr_ticket_id);
                                if(is_numeric($total_ticket_successful)){
                                    TicketReturn::insertValues($ticket->ht['ticket_id'], $total_ticket_successful);
                                }
                            }
                        }   
                        $msg=__('Ticket created successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();

                        FlashMsg::set('ticket_create', $msg);
                        unset($_SESSION[':form-data']);
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the ticket. Correct the error(s) and try again');
                    }
                }
                break;
            case 'io':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreatePayments()) {
                     $errors['err'] = sprintf('%s %s',
                             sprintf(__('You do not have permission %s.'),
                                 __('to create tickets')),
                             __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;

                    $result_point = UserPoint::lookup(intval($_POST['id_history']));
                    if($result_point) {
                        $str_point_note = '(Loyalty: Cập nhật ' . $result_point->change_point . ' điểm::' . $result_point->booking_code . '::'
                            . $result_point->updater . '::' . date('d/m/Y H:i:s', strtotime($result_point->date)) . ')';
                        $vars['loyalty'] = $str_point_note;
                    }

                    if(($ticket=Ticket::io($vars, $errors))) {
                        $msg=__('Payment created successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                        FlashMsg::set('ticket_create', $msg);
                        $url = $cfg->getUrl().'scp/io.php';
                        if($_REQUEST['from_view'] === 'receipt' && $_REQUEST['receipt_id'])
                        {
                            $receipt = Receipt::lookup((int)$_REQUEST['receipt_id']);
                            if($receipt) {
                                $url = $cfg->getUrl() . 'scp/receiptlist.php?receipt_code='.$receipt->receipt_code;
                            }
                        }
                        header('Location: '.$url);
                        exit;
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the ticket. Correct the error(s) and try again');
                    }
                }
                break;
            case 'ol':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreateOfflates()) {
                    $errors['err'] = sprintf('%s %s',
                        sprintf(__('You do not have permission %s.'),
                            __('to create offlates')),
                        __('Contact admin for such access'));
                }
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;
                    if(($ticket=Ticket::ol($vars, $errors))) {
                        $msg=__('Create successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                        FlashMsg::set('ticket_create', $msg);
                        header('Location: /scp/ol.php?list=all');
                        exit;
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the off/late. Correct the error(s) and try again');
                    }
                break;
            case 'booking':
                $ticket=null;
                if(!$thisstaff || !$thisstaff->canCreateBookings()) {
                     $errors['err'] = sprintf('%s %s',
                             sprintf(__('You do not have permission %s.'),
                                 __('to create booking')),
                             __('Contact admin for such access'));
                } else {
                    $vars = $_POST;
                    $vars['uid'] = $user? $user->getId() : 0;

                    if(($ticket=Ticket::booking($vars, $errors))) {
                        $msg=__('Booking created successfully');
                        $_REQUEST['a']=null;
                        if (!$ticket->checkStaffAccess($thisstaff) || $ticket->isClosed())
                            $ticket=null;
                        Draft::deleteForNamespace('ticket.staff%', $thisstaff->getId());
                        // Drop files from the response attachments widget
                        $response_form->setSource(array());
                        $response_form->getField('attachments')->reset();
                        unset($_SESSION[':form-data']);
                        FlashMsg::set('ticket_create', $msg);
                        header('Location: /scp/booking.php');
                        exit;
                    } elseif(!$errors['err']) {
                        $errors['err']=__('Unable to create the booking. Correct the error(s) and try again');
                    }
                }
                break;
        }
    }
    if(!$errors)
        $thisstaff ->resetStats(); //We'll need to reflect any changes just made!

    if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] && !(isset($errors) && count($errors))) {
        header('Location: '.$_SERVER['HTTP_REFERER']);
        exit;
    }
endif;

/*... Quick stats ...*/
$stats= $thisstaff->getTicketsStats();

//Navigation
$nav->setTabActive('tickets');
$open_name = _P('queue-name',
    /* This is the name of the open ticket queue */
    'Open');
if($cfg->showAnsweredTickets()) {
    $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']+$stats['answered']).')',
                            'title'=>__('Open Tickets'),
                            'href'=>'tickets.php?status=open',
                            'iconclass'=>'Ticket'),
                        (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
} else {

    if ($stats) {

        $nav->addSubMenu(array('desc'=>$open_name.' ('.number_format($stats['open']).')',
                               'title'=>__('Open Tickets'),
                               'href'=>'tickets.php?status=open',
                               'iconclass'=>'Ticket'),
                            (!$_REQUEST['status'] || $_REQUEST['status']=='open'));
    }

    if($stats['answered']) {
        $nav->addSubMenu(array('desc'=>__('Answered').' ('.number_format($stats['answered']).')',
                               'title'=>__('Answered Tickets'),
                               'href'=>'tickets.php?status=answered',
                               'iconclass'=>'answeredTickets'),
                            ($_REQUEST['status']=='answered'));
    }
}

if($stats['unassigned']) {

    $nav->addSubMenu(array('desc'=>__('Unassigned').' ('.number_format($stats['unassigned']).')',
                           'title'=>__('Unassigned'),
                           'href'=>'tickets.php?status=unassigned',
                           'iconclass'=>'unassignedTickets'),
                        ($_REQUEST['status']=='unassigned'));
}

if($stats['assigned']) {

    $nav->addSubMenu(array('desc'=>__('My Tickets').' ('.number_format($stats['assigned']).')',
                           'title'=>__('Assigned Tickets'),
                           'href'=>'tickets.php?status=assigned',
                           'iconclass'=>'assignedTickets'),
                        ($_REQUEST['status']=='assigned'));

}
    if($stats['new']) {
        $nav->addSubMenu(
            array('desc'      => __('New Messages') . ' (' . number_format($stats['new']) . ')',
                  'title'     => __('New Messages'),
                  'href'      => 'tickets.php?status=assigned&lastmessage=new',
                  'iconclass' => 'assignedTickets'),
            ($_REQUEST['status'] == 'assigned' && $_REQUEST['lastmessage'] == 'new')
        );
    }

if ($stats['overdue'] > 1000) {
    $overdue_qty = ceil($stats['overdue']/1000).'K';
}

if($stats['overdue']) {
    $nav->addSubMenu(array('desc'=>__('Overdue').' ('.$overdue_qty.')',
                           'title'=>__('Stale Tickets'),
                           'href'=>'tickets.php?status=overdue',
                           'iconclass'=>'overdueTickets'),
                        ($_REQUEST['status']=='overdue'));

    if(!$sysnotice && $stats['overdue']>10)
        $sysnotice=sprintf(__('%d overdue tickets!'),$stats['overdue']);
}

if ($stats['closed'] > 1000) {
    $stats['closed'] = ceil($stats['closed']/1000).'K';
}

if($thisstaff->showAssignedOnly() && $stats['closed']) {
    $nav->addSubMenu(array('desc'=>__('My Closed Tickets').' ('.$stats['closed'].')',
                           'title'=>__('My Closed Tickets'),
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
} else {

    $nav->addSubMenu(array('desc' => __('Closed').' ('.$stats['closed'].')',
                           'title'=>__('Closed Tickets'),
                           'href'=>'tickets.php?status=closed',
                           'iconclass'=>'closedTickets'),
                        ($_REQUEST['status']=='closed'));
}

if($thisstaff->canCreateTickets()) {
    $nav->addSubMenu(array('desc'=>__('New Ticket'),
                           'title'=> __('Open a New Ticket'),
                           'href'=>'tickets.php?a=open',
                           'iconclass'=>'newTicket',
                           'id' => 'new-ticket'),
                        ($_REQUEST['a']=='open'));
}

$ost->addExtraHeader('<script type="text/javascript" src="js/ticket.js?v=1.1"></script>');
$ost->addExtraHeader('<meta name="tip-namespace" content="tickets.queue" />',
    "$('#content').data('tipNamespace', 'tickets.queue');");

$inc = 'tickets.inc.php';
if($ticket) {
    // update thậm chí khi view
    if (!$forms) $forms=DynamicFormEntry::forTicket($ticket->getId());
    // Auto add new fields to the entries
    foreach ($forms as $f) $f->addMissingFields();

    $ost->setPageTitle(sprintf(__('Ticket #%s'),$ticket->getNumber()));
    $nav->setActiveSubMenu(-1);

    if (in_array($ticket->getTopicId(), array_merge(unserialize(IO_TOPIC), []))) {
        if(!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
            function staffLoginPage($msg) {
                global $ost, $cfg;
                $_SESSION['_staff']['auth']['dest'] =
                    '/' . ltrim($_SERVER['REQUEST_URI'], '/');
                $_SESSION['_staff']['auth']['msg']=$msg;
                require(SCP_DIR.'login.php');
                exit;
            }
        }

        if (!isset($_SESSION['check_session']) || !$_SESSION['check_session']
            || !isset($_SESSION['check_session_time'])
            || (isset($_SESSION['check_session_time']) && time() - $_SESSION['check_session_time'] > 1000)) {
            $_SESSION['check_session_time'] = -10000;
            $_SESSION['check_session'] = false;
            unset($_SESSION['check_session_time']);
            unset($_SESSION['check_session']);
            staffLoginPage(__('Enter password for access'));
            exit;
        }
    }

    if (in_array($ticket->getTopicId(), unserialize(IO_TOPIC))) {
        $inc = 'ticket-view-io.inc.php';
        if($thisstaff->canCreatePayments()) {
            foreach ($nav->submenus['staff.tickets'] as $k => $m) {
                if ($m['href'] == 'tickets.php?a=open')
                    unset($nav->submenus['staff.tickets'][$k]);
            }
            $nav->setTabActive('finance');
            $ost->addExtraHeader('<meta name="tip-namespace" content="finance.io_booking" />',
                "$('#content').data('tipNamespace', 'finance.io_booking');");
        }
    }
    elseif (in_array($ticket->getTopicId(), [BOOKING_TOPIC])) {
        $inc = 'ticket-view-booking.inc.php';
        if($thisstaff->canCreateBookings()) {
            foreach ($nav->submenus['staff.tickets'] as $k => $m) {
                if ($m['href'] == 'tickets.php?a=booking')
                    unset($nav->submenus['staff.tickets'][$k]);
            }
            $nav->setTabActive('booking');
            $ost->addExtraHeader('<meta name="tip-namespace" content="booking.booking" />',
                "$('#content').data('tipNamespace', 'booking.booking');");
        }
    }
    elseif (in_array($ticket->getTopicId(), [OL_TOPIC])) {
        $inc = 'ticket-view-offlate.inc.php';
        if($thisstaff->canViewOfflates()) {
            $nav->setTabActive('dayoff');
            $ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.ol" />',
                "$('#content').data('tipNamespace', 'dayoff.ol');");
        }
    }
    else
        $inc = 'ticket-view.inc.php';
    if($_REQUEST['a']=='edit' && $thisstaff->canEditTickets() && $ticket->getTopicId() != BOOKING_TOPIC) {
        $inc = 'ticket-edit.inc.php';

    }elseif($_REQUEST['a']=='edit-io' && $thisstaff->canEditTickets()) {
        $inc = 'ticket-edit-io.inc.php';

    }elseif($_REQUEST['a']=='edit-ol' && $thisstaff->canEditOfflates() && $ticket->getTopicId() == OL_FORM){
        $inc = 'ticket-edit-ol.inc.php';

    }elseif($_REQUEST['a']=='edit-booking' && $thisstaff->canEditBookings() && $ticket->getTopicId() == BOOKING_TOPIC) {
        $inc = 'ticket-edit-booking.inc.php';

        foreach ($nav->submenus['staff.tickets'] as $k => $m) {
            if ($m['href'] == 'tickets.php?a=open')
                unset($nav->submenus['staff.tickets'][$k]);
        }
        $nav->setTabActive('booking');
        $ost->addExtraHeader('<meta name="tip-namespace" content="booking.edit_booking" />',
            "$('#content').data('tipNamespace', 'booking.edit_booking');");

    } elseif($_REQUEST['a'] == 'print' && !$ticket->pdfExport($_REQUEST['psize'], $_REQUEST['notes']))
        $errors['err'] = __('Internal error: Unable to export the ticket to PDF for print.');
} else {
	$inc = 'tickets.inc.php';
    if($_REQUEST['a']=='open' && $thisstaff->canCreateTickets())
        $inc = 'ticket-open.inc.php';
    elseif ($_REQUEST['a']=='io' && $thisstaff->canCreatePayments()) {
        $_SESSION['_staff']['auth']['finance']=true;
        $inc = 'ticket-io.inc.php';
        foreach ($nav->submenus['staff.tickets'] as $k => $m) {
            if ($m['href'] == 'tickets.php?a=open')
                unset($nav->submenus['staff.tickets'][$k]);
        }
        $nav->setTabActive('finance');
        $ost->addExtraHeader('<meta name="tip-namespace" content="finance.new_io" />',
            "$('#content').data('tipNamespace', 'finance.new_io');");
    }
    elseif ($_REQUEST['a']=='booking' && $thisstaff->canCreateBookings()) {
        if (isset($_REQUEST['new']) && $_REQUEST['new'] == 1)
            unset($_SESSION[':form-data']);

        $inc = 'ticket-booking.inc.php';
        foreach ($nav->submenus['staff.tickets'] as $k => $m) {
            if ($m['href'] == 'tickets.php?a=open')
                unset($nav->submenus['staff.tickets'][$k]);
        }
        $nav->setTabActive('booking');
        $nav->setActiveSubMenu(0,'booking');
        $ost->addExtraHeader('<meta name="tip-namespace" content="booking.new_booking" />',
            "$('#content').data('tipNamespace', 'booking.new_booking');");
    }
    elseif ($_REQUEST['a']=='ol'){
        $inc = 'ticket-ol.inc.php';
        foreach ($nav->submenus['staff.tickets'] as $k => $m) {
            if ($m['href'] == 'tickets.php?a=open')
                unset($nav->submenus['staff.tickets'][$k]);
        }
        $nav->setTabActive('dayoff');
        $nav->setActiveSubMenu(0,'dayoff');
        $ost->addExtraHeader('<meta name="tip-namespace" content="dayoff.newTicket" />',
            "$('#content').data('tipNamespace', 'dayoff.newTicket');");
    }
    elseif($_REQUEST['a'] == 'export') {
        $ts = strftime('%Y%m%d');
        $token = null;

        if (isset($_REQUEST['h']))
            $token = $_REQUEST['h'];

        if (!$token)
            $errors['err'] = __('Query token required');
        elseif (!$thisstaff || !$thisstaff->isAdmin())
            $errors['err'] = __('Only Admins can export');
        elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv'))
            $errors['err'] = __('Internal error: Unable to dump query results');
    }

    //Clear active submenu on search with no status
    if($_REQUEST['a']=='search' && !$_REQUEST['status'])
        $nav->setActiveSubMenu(-1);

    //set refresh rate if the user has it configured
    if(!$_POST && !$_REQUEST['a'] && ($min=$thisstaff->getRefreshRate())) {
        $js = "clearTimeout(window.ticket_refresh);
               window.ticket_refresh = setTimeout($.refreshTicketView,"
            .($min*60000).");";
        $ost->addExtraHeader('<script type="text/javascript">'.$js.'</script>',
            $js);
    }
}

require_once(STAFFINC_DIR.'header.inc.php');
require_once(STAFFINC_DIR.$inc);
print $response_form->getMedia();
require_once(STAFFINC_DIR.'footer.inc.php');

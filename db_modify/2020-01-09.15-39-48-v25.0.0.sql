alter table tugo_osticket_customer modify amount decimal(12) unsigned null comment 'total money của số điện thoại và booking này';

alter table tugo_osticket_customer
    add phone_number varchar(10) null;

alter table tugo_osticket_customer
    add booking_code varchar(16) null comment 'TG123-123321 (bỏ số 0 đầu)';

alter table tugo_osticket_customer
    add booking_code_trim int null comment '123456';

alter table tugo_osticket_customer
    add quantity mediumint(3) null comment 'tổng số khách của số điện thoại và booking này';

alter table tugo_osticket_customer
    add destination_name varchar(64) null;

alter table tugo_osticket_customer
    add destination_id int null;

alter table tugo_osticket_customer
    add market_id int null;

alter table tugo_osticket_customer
    add market_name varchar(64) null;


update tugo_osticket_customer set week_of_the_year = null where 1;
alter table tugo_osticket_customer modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_customer modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_booking set week_of_the_year = null where 1;
alter table tugo_osticket_booking modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_booking modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_call set week_of_the_year = null where 1;
alter table tugo_osticket_call modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_call modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_customer_age set week_of_the_year = null where 1;
alter table tugo_osticket_customer_age modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_customer_age modify week_of_year varchar(6) null comment 'YEAR_WEEK';




create table auto_action_content_campaign
(
	id int auto_increment,
	type enum('campaign', 'source', 'medium', 'term', 'content') null,
	value varchar(64) null,
	constraint auto_action_content_campaign_pk
		primary key (id)
);

create unique index auto_action_content_campaign_type_value_uindex
	on auto_action_content_campaign (type, value);

alter table auto_action_content change sms_send_one_time send_one_time_at datetime null;

alter table auto_action_content change sms_status status tinyint default 0 not null;

alter table auto_action_content drop column sms_send_after;

alter table auto_action_content change sms_send_recursion send_recursion varchar(10) null;

alter table auto_action_content change sms_send_recursion_minute recursion_minute varchar(180) null after send_recursion;

alter table auto_action_content change sms_send_recursion_hour recursion_hour varchar(72) null after recursion_minute;

alter table auto_action_content change sms_send_recursion_day_of_week recursion_day_of_week varchar(21) null;

alter table auto_action_content change sms_send_recursion_date_in_month recursion_date_in_month varchar(93) null;

alter table auto_action_content change sms_send_recursion_month recursion_month varchar(36) null;

alter table auto_action_content drop column email_send_recursion;

drop index aac_email_send_recursion_date_in_month_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_date_in_month;

create index aac_email_send_recursion_date_in_month_index
	on auto_action_content (email_send_recursion_date_in_month);

drop index aac_email_send_recursion_day_of_week_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_day_of_week;

create index aac_email_send_recursion_day_of_week_index
	on auto_action_content (email_send_recursion_day_of_week);

drop index aac_email_send_recursion_hour_index on auto_action_content;

create index aac_email_send_recursion_hour_index
	on auto_action_content (email_send_recursion_hour);

drop index aac_email_send_recursion_minute_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_minute;

create index aac_email_send_recursion_minute_index
	on auto_action_content (email_send_recursion_minute);

drop index aac_email_send_recursion_month_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_month;

create index aac_email_send_recursion_month_index
	on auto_action_content (email_send_recursion_month);

drop index aac_notification_send_recursion_date_in_month_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_date_in_month;

create index aac_notification_send_recursion_date_in_month_index
	on auto_action_content (notification_send_recursion_date_in_month);

drop index aac_notification_send_recursion_dow_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_day_of_week;

create index aac_notification_send_recursion_dow_index
	on auto_action_content (notification_send_recursion_day_of_week);

drop index aac_notification_send_recursion_hour_index on auto_action_content;

create index aac_notification_send_recursion_hour_index
	on auto_action_content (notification_send_recursion_hour);

drop index aac_notification_send_recursion_minute_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_minute;

create index aac_notification_send_recursion_minute_index
	on auto_action_content (notification_send_recursion_minute);

drop index aac_notification_send_recursion_month_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_month;

create index aac_notification_send_recursion_month_index
	on auto_action_content (notification_send_recursion_month);

drop index auto_action_content_email_run_at_index on auto_action_content;

alter table auto_action_content drop column email_run_at;

create index auto_action_content_email_run_at_index
	on auto_action_content (email_run_at);

drop index auto_action_content_email_send_recursion_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_hour;

create index auto_action_content_email_send_recursion_index
	on auto_action_content (email_send_recursion_hour);

drop index auto_action_content_email_send_time_index on auto_action_content;

alter table auto_action_content drop column email_send_one_time;

create index auto_action_content_email_send_time_index
	on auto_action_content (email_send_one_time);

drop index auto_action_content_email_status_index on auto_action_content;

alter table auto_action_content drop column email_status;

create index auto_action_content_email_status_index
	on auto_action_content (email_status);

drop index auto_action_content_notification_run_at_index on auto_action_content;

alter table auto_action_content drop column notification_run_at;

create index auto_action_content_notification_run_at_index
	on auto_action_content (notification_run_at);

drop index auto_action_content_notification_send_recursion_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_hour;

create index auto_action_content_notification_send_recursion_index
	on auto_action_content (notification_send_recursion_hour);

drop index auto_action_content_notification_send_time_index on auto_action_content;

alter table auto_action_content drop column notification_send_one_time;

create index auto_action_content_notification_send_time_index
	on auto_action_content (notification_send_one_time);

drop index auto_action_content_notification_status_index on auto_action_content;

alter table auto_action_content drop column notification_status;

create index auto_action_content_notification_status_index
	on auto_action_content (notification_status);

drop index aac_email_send_recursion_hour_index on auto_action_content;

drop index aac_email_send_recursion_minute_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_minute;

drop index aac_email_send_recursion_month_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_month;

drop index aac_notification_send_recursion_date_in_month_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_date_in_month;

drop index aac_notification_send_recursion_dow_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_day_of_week;

drop index aac_notification_send_recursion_hour_index on auto_action_content;

drop index aac_notification_send_recursion_minute_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_minute;

drop index aac_notification_send_recursion_month_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_month;

drop index auto_action_content_email_send_recursion_index on auto_action_content;

alter table auto_action_content drop column email_send_recursion_hour;

drop index auto_action_content_notification_send_recursion_index on auto_action_content;

alter table auto_action_content drop column notification_send_recursion_hour;

alter table auto_action_content change sms_run_at is_one_time tinyint(1) default 0 not null comment '0=one time';

drop index auto_action_content_email_send_time_index on auto_action_content;

drop index auto_action_content_email_status_index on auto_action_content;

alter table auto_action_content drop column email_status;

drop index auto_action_content_notification_run_at_index on auto_action_content;

alter table auto_action_content drop column notification_run_at;

drop index auto_action_content_notification_send_time_index on auto_action_content;

alter table auto_action_content drop column notification_send_one_time;

drop index auto_action_content_notification_status_index on auto_action_content;

alter table auto_action_content drop column notification_status;

alter table auto_action_content drop column email_send_one_time;

alter table auto_action_content drop column send_recursion;

alter table auto_action_content change is_one_time schedule enum('now', 'one_time', 'recursion') not null comment '0=one time';

alter table auto_action_content
	add recursion_from datetime null;

alter table auto_action_content
	add recursion_to datetime null;

alter table auto_action_content
	add aac_campaign_id int null;

create index auto_action_content_aac_campaign_id_index
	on auto_action_content (aac_campaign_id);

create index auto_action_content_recursion_from_index
	on auto_action_content (recursion_from);

create index auto_action_content_recursion_from_recursion_to_index
	on auto_action_content (recursion_from, recursion_to);

create index auto_action_content_recursion_to_index
	on auto_action_content (recursion_to);

alter table auto_action_content
	add type enum('sms', 'email', 'notification') null;

create index auto_action_content_type_index
	on auto_action_content (type);

alter table auto_action_content
	add acc_utm_source_id int null;

alter table auto_action_content
	add acc_utm_term_id int null;

alter table auto_action_content
	add acc_utm_medium_id int null;

alter table auto_action_content
	add acc_utm_content_id int null;

create index auto_action_content_acc_utm_content_id_index
	on auto_action_content (acc_utm_content_id);

create index auto_action_content_acc_utm_medium_id_index
	on auto_action_content (acc_utm_medium_id);

create index auto_action_content_acc_utm_source_id_index
	on auto_action_content (acc_utm_source_id);

create index auto_action_content_acc_utm_term_id_index
	on auto_action_content (acc_utm_term_id);

alter table auto_action_content change acc_utm_source_id aac_utm_source_id int null;

alter table auto_action_content change acc_utm_term_id aac_utm_term_id int null;

alter table auto_action_content change acc_utm_medium_id aac_utm_medium_id int null;

alter table auto_action_content change acc_utm_content_id aac_utm_content_id int null;


<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');

$qs = array();
$agent = $type = $booking_code = $info = $status = $partner = $customer_number = $tour = null;
if (isset($_REQUEST['booking_code'])) {
    $booking_code = trim($_REQUEST['booking_code']);
    $qs += ['booking_code' => $_REQUEST['booking_code']];
}

if (isset($_REQUEST['info'])) {
    $info = trim($_REQUEST['info']);
    $qs += ['info' => $_REQUEST['info']];
}

if (isset($_REQUEST['agent'])) {
    $agent = trim($_REQUEST['agent']);
    $qs += ['agent' => $_REQUEST['agent']];
}

if (isset($_REQUEST['tour'])) {
    $tour = trim(json_encode(trim($_REQUEST['tour'])), '"');
    $qs += ['tour' => $tour];
}

if (isset($_REQUEST['customer_number'])) {
    $customer_number = trim($_REQUEST['customer_number']);
    $qs += ['customer_number' => $_REQUEST['customer_number']];
}

if (isset($_REQUEST['status'])) {
    $status = intval(trim($_REQUEST['status']));
    $qs += ['status' => $_REQUEST['status']];
}

if (isset($_REQUEST['partner'])) {
    $partner = trim(json_encode(trim($_REQUEST['partner'])), '"');
    $qs += ['partner' => $_REQUEST['partner']];
}

$qwhere = ' WHERE 1 ';

if($booking_code) {
    $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $booking_code);
    $_booking_code_trim = ltrim($_booking_code_trim, '0');
    $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM b.booking_code)) LIKE '.db_input($_booking_code_trim);
}

if($customer_number) {
    $customer_number_booking_code_list = [];
    $customer_number_booking_code_list_str = '';
    $customer_number_sql = "SELECT booking_code FROM api_user u JOIN api_user_point_history p ON u.uuid=p.user_uuid WHERE customer_number LIKE ".db_input($customer_number);

    $res_customer_number = db_query($customer_number_sql);
    if ($res_customer_number) {
        while (($row = db_fetch_array($res_customer_number))) {
            $_tmp_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $row['booking_code']);
            $_tmp_booking_code_trim = ltrim($_tmp_booking_code_trim, '0');
            $customer_number_booking_code_list[] = db_input($_tmp_booking_code_trim);
        }

        $customer_number_booking_code_list_str = implode(',', $customer_number_booking_code_list);
    }

    if ($customer_number_booking_code_list)
        $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM b.booking_code)) IN ('.$customer_number_booking_code_list_str.') ';
    else
        $qwhere .= ' AND 1 = 0 ';
}

if ($info) {
    $qwhere .= ' AND ( '
    . ' customer LIKE '.db_input('%'.$info.'%')
    . ' OR phone_number LIKE '.db_input('%'.$info.'%')
    . ' OR p.receipt_code LIKE '.db_input('%'.$info.'%')
    . ' OR p.note LIKE '.db_input('%'.$info.'%')
    . ' OR b.note LIKE '.db_input('%'.$info.'%')
        . ' )';
}

if ($agent) {
    $qwhere .= " AND staff_id = ".db_input($agent);
}

if ($status)
    $qwhere .= ' AND s.booking_status_id = '.db_input($status);

if ($partner)
    $qwhere .= sprintf(" AND (  partner LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $partner)."%'");

if ($tour) {
    $qwhere .= sprintf(" AND (  booking_type LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $tour)."%'");
}

if (!isset($_REQUEST['startDate']) || empty($_REQUEST['startDate']))
    $_REQUEST['startDate'] = date('Y-m-d', time()-3600*24*200);

if (!isset($_REQUEST['endDate']) || empty($_REQUEST['endDate']))
    $_REQUEST['endDate'] = date('Y-m-d');

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDate'])):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDate'])):0;
if($startTime>$endTime && $endTime>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere .= " AND DATE(`created`) >= date(".(db_input($startTime)).")";
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere .=" AND DATE(`created`) <= date(".(db_input($endTime)).")";
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
//get date start depart
$startDepart  =($_REQUEST['startDepart'] && (strlen($_REQUEST['startDepart'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['startDepart'])):0;
$endDepart  =($_REQUEST['endDepart'] && (strlen($_REQUEST['endDepart'])>=8))?Format::userdate('Y-m-d', strtotime($_REQUEST['endDepart'])):0;

if($startDepart>$endDepart && $endDepart>0){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startDepart=$endDepart=0;
}else{
    if($startDepart){
        $qwhere .= " AND FROM_UNIXTIME(dept_date,'%Y-%m-%d') >= date(".db_input($startDepart).")";
        $qs += array('startDepart' => $_REQUEST['startDepart']);
    }
    if($endDepart){
        $qwhere .=" AND FROM_UNIXTIME(dept_date,'%Y-%m-%d') <= date(".(db_input($endDepart)).")";
        $qs += array('endDepart' => $_REQUEST['endDepart']);
    }
}


///
//Booking::create_table();
//Booking::create_view();
//pagenate
$qstr = '&amp;'. Http::build_query($qs);
$view_name = Booking::VIEW_NAME;
$qhaving = ' HAVING 1 AND (booking_balance < 0 OR remain_amount > 0 )';
$_status = trim(json_encode(trim('Hủy')), '"');
$qwhere .= sprintf(" AND (  status NOT LIKE %s ESCAPE '|'  ) ", "'%".str_replace('\\', '\\\\', $_status)."%'");
$sql = " SELECT DISTINCT b.*, SUM(ABS(IFNULL(p.amount, 0)) * (IF(IFNULL(p.topic_id, 0) = ".THU_TOPIC.", 1, -1))) as booking_balance, b.total_retail_price - SUM(ABS(IFNULL(p.amount, 0)) * (IF(IFNULL(p.topic_id, 0) = ".THU_TOPIC.", 1, 0))) as remain_amount,
            s.booking_status_id, sl.name as status_name
            FROM $view_name b LEFT JOIN payment_tmp p
            on p.booking_ticket_id=b.ticket_id 
            LEFT JOIN booking_status s ON b.ticket_id=s.booking_ticket_id
            LEFT JOIN booking_status_list sl ON s.booking_status_id=sl.id
               $qwhere
               GROUP BY b.booking_code $qhaving
               ORDER BY b.dept_date ASC ";
$total = db_count("SELECT COUNT(*) FROM ($sql) AS A");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('unfinished.php',$qs);
$res=db_query($sql . " LIMIT ".$pageNav->getStart().",".$pageNav->getLimit());

// get payment log
$res_booking_id = $res;
$booking_ids = [];
while ($res_booking_id && ($row = db_fetch_array($res_booking_id)))
    $booking_ids[] = $row['ticket_id'];
$payemnt_logs = [];
if ($booking_ids) {
    $sql_log = " SELECT
          b.ticket_id,
          p.time,
          p.receipt_code,
          p.note,
          ABS(p.amount) * (IF(p.topic_id = ".THU_TOPIC.", 1, -1)) as amount
        FROM `booking_tmp` b
          JOIN payment_tmp p
            on p.booking_ticket_id=b.ticket_id
        WHERE b.ticket_id IN (".implode(',', $booking_ids).") ";
    $res_log = db_query($sql_log);
    while ($res_log && ($row = db_fetch_array($res_log))) {
        if (!isset($payemnt_logs[ $row['ticket_id'] ]))
            $payemnt_logs[ $row['ticket_id'] ] = [];

        $payemnt_logs[ $row['ticket_id'] ][] = [
            'receipt_code' => $row['receipt_code'],
            'amount' => $row['amount'],
            'time' => $row['time'],
            'note' => $row['note'],
        ];
    }
}
//
db_data_reset($res);
if($res && ($num = db_num_rows($res))):
    $showing=$pageNav->showing().' '.$title;
else:
    $showing=__('No booking found!');
endif;
?>
<style>
    .list {
        table-layout: fixed;
        width: 1058px;
    }

    .list .quantity_cell {
        text-align: center;
        vertical-align: middle;
    }

    .list td {
        overflow: hidden;
    }

    .list .payment_log {
        padding-left: 2em;
    }

    .clipboard {
        text-align: center;
    }

    .clipboard img {
        width: 12px;
        vertical-align: middle;
    }

    .action-buttons {
        text-align: center;
    }

    .booking-note {
        width: 100%;
        resize: none;
    }
</style>
<h2><?php echo __('Unfinished Bookings');?>
</h2>

<?php
if ($thisstaff->canChangeBookingStatus()) {
    $status_default = null;
    $status_finished = null;
    $status_obj = \Booking\StatusList::getAll($status_default, $status_finished);
}
?>

<div id='filter' >
 <form action="unfinished.php" method="get">
    <div style="padding-left:2px;">
        <table>
            <tr>
                <td><b><?php echo __('Created Date'); ?></b> <?php echo __('Between'); ?></td>
                <td>
                    <input class="dp input-field" id="sd" size=15 name="startDate" value="<?php if (strtotime($_REQUEST['startDate'])) echo date('d/m/Y', strtotime($_REQUEST['startDate'])); ?>" autocomplete=OFF>
                    <input class="dp input-field" id="ed" size=15 name="endDate" value="<?php if (strtotime($_REQUEST['endDate'])) echo date('d/m/Y', strtotime($_REQUEST['endDate'])); ?>" autocomplete=OFF>
                </td>
                <td><?php echo __('Agent'); ?></td>
                <td>
                    <select name='agent' class="input-field">
                        <option value><?php echo __('All');?></option>
                        <?php
                        $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                            .' FROM '.STAFF_TABLE.' staff '
                            .' ORDER by name';
                        if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                            while(list($id,$name)=db_fetch_row($agent_res)){
                                $selected=($_REQUEST['agent'] && $id==$_REQUEST['agent'])?'selected="selected"':'';
                                echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                            }
                        }
                        ?>
                    </select>
                </td>
                <td><?php echo __('Info'); ?></td>
                <td><input type="text" class="input-field" name="info" value="<?php echo $_REQUEST['info'] ?>"></td>
            </tr>
            <tr>
                <td><?php echo __('Mã Booking'); ?></td>
                <td><input type="text" class="input-field" name="booking_code" value="<?php echo $_REQUEST['booking_code'] ?>"></td>

                <td>Trạng Thái</td>
                <td>
                    <select name="status" class="input-field">
                        <option value=><?php echo __('-- All --') ?></option>
                        <?php
                        if ($status_obj) {
                            foreach($status_obj as $row) {
                                ?>
                                <option value="<?php echo $row['id'] ?>"
                                    <?php if (isset($_REQUEST['status']) && $_REQUEST['status']==$row['id']) echo 'selected' ?>
                                ><?php echo $row['name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>
                <td>Đối tác</td>
                <td>
                    <select name="partner" class="input-field">
                        <option value=><?php echo __('-- All --') ?></option>
                        <?php
                        $sql = "SELECT DISTINCT `value` as name, id FROM ".LIST_ITEM_TABLE." li WHERE li.list_id IN (".PARTNER_LIST.")";
                        $res_list = db_query($sql);
                        if ($res_list) {
                            while(($row = db_fetch_array($res_list))) {
                                ?>
                                <option value="<?php echo $row['id'] ?>"
                                    <?php if (isset($_REQUEST['partner']) && $_REQUEST['partner']==$row['id']) echo 'selected' ?>
                                ><?php echo $row['name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Loại Booking</td>
                <td>
                    <select name="tour" class="input-field">
                        <option value=><?php echo __('-- All --') ?></option>
                        <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                            <?php foreach($booking_type_list as  $item): ?>
                                <option value="<?php echo $item['id'] ?>"
                                    <?php if (isset($_REQUEST['tour']) && in_array($item['id'],$_REQUEST['tour'])) echo 'selected' ?>
                                ><?php echo $item['name'] ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </td>

                <td><?php echo __('Số khách hàng'); ?></td>
                <td><input type="text" class="input-field" name="customer_number" value="<?php echo $_REQUEST['customer_number'] ?>"></td>
            </tr>
            <tr>
                <!--add filter start,end depart -->
                <td><?php echo __('Ngày khởi hành: Between'); ?>
                <td>
                    <input class="dp input-field" id="depart1" size=15 name="startDepart" value="<?php if (strtotime($_REQUEST['startDepart'])) echo date('d/m/Y', strtotime($_REQUEST['startDepart'])); ?>" autocomplete=OFF>
                    <input class="dp input-field" id="depart2" size=15 name="endDepart" value="<?php if (strtotime($_REQUEST['endDepart'])) echo date('d/m/Y', strtotime($_REQUEST['endDepart'])); ?>" autocomplete=OFF>
                </td>
            </tr>

            <tr>

                <td colspan="6">
                    <input type="submit" class="btn_sm btn-primary" Value="<?php echo __('Search');?>" />
                    <input type="reset" class="btn_sm btn-default" Value="<?php echo __('Reset');?>" onclick="reset_form(this)" />
                </td>
            </tr>
            <script>
                function reset_form(event) {
                    console.log(event);
                    form = $(event).parents('form');
                    form.find('input[type="text"], select, .hasDatepicker').val('');
                    form.submit();
                }
            </script>
        </table>
    </div>
 </form>
</div>
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th style="width:140px;"><?php echo __('Nhân viên/Mã Booking');?></th>
            <th><?php echo __('Loại Booking/Ngày khởi hành');?></th>
            <th><?php echo __('Số tiền phải thu');?></th>
            <th style="width:350px;"><?php echo __('Số tiền đã thu');?></th>
            <th style="width:100px;"><?php echo __('Còn lại');?></th>
            <th style="width:100px;"><?php echo __('Ngày tạo/Trạng thái');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $total=0;
        if($res):
            while ($row = db_fetch_array($res)) {
                ?>
            <tr id="<?php echo $row['ticket_id']; ?>" class="<?php if ($row['amount'] < 0) echo 'type_out' ?>">
                <td>
                    <p><?php echo _String::json_decode($row['staff']); ?></p>
                    <p>
                        <strong>
                            <a class="no-pjax" target="_blank" title="Logs"
                                  href="<?php echo $cfg->getUrl();?>scp/new_booking.php?id=<?php echo $row['ticket_id']; ?>"
                            ><?php echo Format::htmlchars($row['booking_code']); ?></a>
                        </strong>
                        <button class="btn_sm btn-xs btn-default clipboard" title="Click to copy booking code" data-clipboard-text="<?php echo Format::htmlchars($row['booking_code']); ?>">
                            <img src="/scp/images/clippy.svg" alt="">
                        </button>
                    </p>
                    <p>[<em><?php echo $row['receipt_code']; ?></em>]</p>
                </td>
                <td>
                    <p><strong><?php echo _String::json_decode($row['booking_type']); ?></strong></p>

                    <p><?php echo Format::userdate('d/m/Y', $row['dept_date']); ?></p>
                </td>
                <td>
                    <?php echo number_format($row['total_retail_price']).'đ'; ?>
                </td>
                <td>
                    <?php if(isset($payemnt_logs[$row['ticket_id']]) && is_array($payemnt_logs[$row['ticket_id']])): ?>
                        <table>
                            <?php foreach($payemnt_logs[$row['ticket_id']] as $_ticket_id => $_row): ?>
                                <tr>
                                    <td><?php echo Format::userdate('d/m/Y', $_row['time']) ?></td>
                                    <td>
                                        <?php if($_row['amount'] > 0): ?><strong>+<?php endif; ?><?php
                                            echo number_format($_row['amount']).'đ' ?><?php
                                            if($_row['amount'] > 0): ?></strong><?php endif; ?>
                                    </td>
                                    <td><?php echo $_row['receipt_code'] ?></td>
                                    <td><?php echo $_row['note'] ?></td >
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <ol class="payment_log">

                        </ol>
                    <?php endif; ?>
                </td>
                <td>
                    <?php echo number_format($row['remain_amount']).'đ'; ?>
                </td>
                <td class="action-buttons">
                    <?php if(strtotime($row['created'])): ?>
                        <p><?php echo Format::date('d/m/Y', strtotime($row['created'])); ?></p>
                    <?php endif; ?>

                    <?php if($thisstaff->canChangeBookingStatus()): ?>
                        <?php include(STAFFINC_DIR . 'templates/booking-status-options.tmpl.php'); ?>
                    <?php endif; ?>

                    <?php include(STAFFINC_DIR . 'templates/booking-note.tmpl.php'); ?>

                    <p class="log-link"><a href="/scp/unfinished.php?action=log&id=<?php echo $row['ticket_id'] ?>" class="no-pjax" target="_blank">View Log</a></p>
                </td>
            </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
     <?php if(!$res || !$num){ ?>
    <tfoot>
     <tr>
        <td>
            No booking found
        </td>
     </tr>
    </tfoot>
     <?php
     } ?>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
endif;
?>

<script>
    function addnote(e) {
        _self = $(e.target);
        if (!_self.hasClass('addnote_btn'))
            _self = _self.parent('button');
        if (!_self.hasClass('addnote_btn'))
            return false;

        id = _self.data('bookingid');
        url = '/scp/ajax.php/bookings/'+id+'/note';

        $.dialog(url, 201, function (xhr) {
            var user = $.parseJSON(xhr.responseText);
            if (cb) return cb(user);
        }, {
            onshow: function() { $('#user-search').focus(); }
        });
    }

    $(document).ready(function() {
        var clipboard = new Clipboard('.clipboard');

        $('.addnote_btn').off('click').on('click', addnote);
    });

    (function($) {
        $('.more').readmore({
            speed: 20,
            collapsedHeight: 100,
            moreLink: '<a href="#" class="read_more_link">Read more</a>',
            lessLink: '<a href="#" class="read_more_link">Close</a>',
            blockCSS: 'display: block; width: 100%; margin-bottom: 1em !important;'
        });
    })(jQuery);
</script>

<?php include_once STAFFINC_DIR.'widget.overdue.php'; ?>

<?php if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path'); ?>
<div id="user-point">
<?php
$show_options = true;
foreach ($points as $point) {
    include STAFFINC_DIR."templates/point.tmpl.php";
} ?>
</div>

<?php if ($thisstaff->canEditPointLoyalty()): ?>
    <div id="new-point-box">
        <div class="userpoint" id="new-point" data-url="<?php echo $create_point_url; ?>">
            <div class="body">
                <p><a href="#" class=" btn_sm btn-primary"><i class="icon-plus icon-large"></i> Cập nhật điểm</a></p>
            </div>
        </div>
    </div>
<?php endif;?>
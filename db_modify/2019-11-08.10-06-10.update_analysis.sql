alter table tugo_osticket_sms_phonenumber change phone_number amount integer unsigned null;
alter table tugo_osticket_call_phonenumber change phone_number amount int unsigned null;

drop table IF EXISTS tugo_osticket_analytics_booking;
CREATE TABLE IF NOT EXISTS `tugo_osticket_booking` (
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`date` VARCHAR(8) NULL COMMENT 'YYYYMMDD',
				`hour_of_day` VARCHAR(10) NULL COMMENT 'YYYYMMDDhh',
				`day_of_month` VARCHAR(2) NULL COMMENT 'DD',
				`day_of_week` VARCHAR(1) NULL COMMENT 'D DAY_OF_WEEK',
				`hour` VARCHAR(2) NULL COMMENT 'hh',
				`week_of_the_year` VARCHAR(6) NULL COMMENT 'WEEK',
				`week_of_year` VARCHAR(2) NULL COMMENT 'YEAR_WEEK',
				`minute` VARCHAR(2) NULL COMMENT 'mm',
				`month_of_the_year` VARCHAR(2) NULL COMMENT 'MONTH',
				`month_of_year` VARCHAR(6) NULL COMMENT 'YEAR_MONTH',
				`year` VARCHAR(4) NULL COMMENT 'YEAR',
				`day_of_week_name` VARCHAR(10) NULL COMMENT 'DAY_OF_WEEK_NAME',
				`minute_of_day` VARCHAR(12) NULL COMMENT 'date_hour_minute',
                `new_booking` INT NULL,
                `booking_quantity` INT NULL,
                `booking_value` DECIMAL(12) NULL,
                `staff_id` INT NULL,
                `staff_username` VARCHAR(45) NULL,
                PRIMARY KEY (`id`),
				UNIQUE INDEX `index2` (`day_of_week_name` ASC, `year` ASC, `month_of_year` ASC, `month_of_the_year` ASC, `minute` ASC, `week_of_year` ASC, `week_of_the_year` ASC, `hour` ASC, `day_of_week` ASC, `day_of_month` ASC, `hour_of_day` ASC, `date` ASC, `minute_of_day` ASC, `staff_id` ASC))


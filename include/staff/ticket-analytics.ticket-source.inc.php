<?php
$chart_data_sources = [];
$table_data_sources = [];
$source_by_day_chart = [];

TicketAnalytics::analytics($_REQUEST['from'], $_REQUEST['to'], 'source_by_day', $_REQUEST, $chart_data_sources, $table_data_sources);

$booking_dates_source = [];
while($chart_data_sources && ($row = db_fetch_array($chart_data_sources))) {
    if (!$row['sourcex']) $row['sourcex'] = 3;
    if (!isset($source_by_day_chart[ $row['sourcex'] ]))
        $source_by_day_chart[ $row['sourcex'] ] = [];

    switch ($_REQUEST['group']) {
        case 'week':
            $booking_dates_source[] = $row['week'].'-'.$row['year'];
            $source_by_day_chart[ $row['sourcex'] ][ $row['week'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'month':
            $booking_dates_source[] = $row['month'].'-'.$row['year'];
            $source_by_day_chart[ $row['sourcex'] ][ $row['month'].'-'.$row['year'] ] = $row['total'];
            break;
        case 'day':
        default:
            $booking_dates_source[] = date(DATE_FORMAT, strtotime($row['date']));
            $source_by_day_chart[ $row['sourcex'] ][ date(DATE_FORMAT, strtotime($row['date'])) ] = $row['total'];
            break;
    }
}

$booking_dates_source = array_filter(array_unique($booking_dates_source));

foreach ($source_by_day_chart as $_source => $_date_data) {
    foreach ($booking_dates_source as $_date) {
        if (!isset($source_by_day_chart[ $_source ][ $_date ]))
            $source_by_day_chart[ $_source ][ $_date ] = 0;
    }

    uksort($source_by_day_chart[ $_source ], "___date_compare");
}
?>
<td colspan="2">
    <h2>Ticket Sources <button class="btn_sm btn-xs btn-default" onclick="hide_all('ticket_sources')">Hide All</button></h2>
    <canvas id="sources" width="1000" height="300"></canvas>
</td>
<script>
    <?php $dates = [] ?>
    var sources_elm = document.getElementById("sources").getContext('2d');
    colors = {
        1: '#f55',
        2: '#5f5',
        4: '#599',
        5: '#f5f',
        3: '#555',
    };
    i = 0;
    var ticket_sources = new Chart(sources_elm, {
        type: 'bar',
        data: {
            datasets: [
                <?php foreach($source_by_day_chart as $_source => $_date): ?>
                {
                    label: '<?php if (isset(TicketAnalytics::$source_name[ $_source ])) echo TicketAnalytics::$source_name[ $_source ];  ?>',
                    backgroundColor:  <?php if ($_source) echo "colors[". $_source."]"; else echo "\"".rand_color()."\""; ?>,
                    data: $.parseJSON('<?php echo json_encode(array_values($_date)) ?>'),
                    fill: true
                    <?php $dates = array_keys($_date) ?>
                },
                <?php endforeach; ?>
            ],
            labels: $.parseJSON('<?php echo json_encode($dates) ?>'),
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            responsive: false,
            maintainAspectRatio: false,
            showLines: false, // disable for all datasets
        }
    });
</script>
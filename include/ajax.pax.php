<?php
/*********************************************************************
    ajax.users.php

    AJAX interface for  users (based on submitted tickets)
    XXX: osTicket doesn't support user accounts at the moment.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

if(!defined('INCLUDE_DIR')) die('403');

require_once INCLUDE_DIR.'tugo.user.php';
require_once INCLUDE_DIR.'class.passport.php';

class PaxAjaxAPI extends AjaxController {

    function search($type = null) {

        if(!isset($_REQUEST['passport_no']) || empty(trim($_REQUEST['passport_no'])) || strlen($_REQUEST['passport_no']) < 5) {
            Http::response(400, __('Query argument is required or too short'));
        }

        $limit = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit']:10;
        $users=array();
        $_REQUEST['passport_no'] = trim($_REQUEST['passport_no']);
        $q = $_REQUEST['passport_no'];
        $escaped = db_input($q, false);

        $sql = "SELECT DISTINCT p.*, i.*, t.phone_number FROM ost_pax p LEFT JOIN ost_pax_info i ON p.id=i.pax_id 
            LEFT JOIN ost_pax_tour t ON p.id=t.pax_id
            WHERE i.passport_no LIKE '%$escaped%' LIMIT ".$limit;

        if(($res=db_query($sql)) && db_num_rows($res)){
            while( ($row = db_fetch_array($res)) ) {
                $users[] = array(
                    'id' => $row['id'],
                    'uuid' => $row['uuid'],
                    'full_name' => $row['full_name'],
                    'phone_number' => $row['phone_number'],
                    'dob' => date('m/d/Y', strtotime($row['dob'])),
                    'gender' => $row['gender'],
                    'passport_no' => $row['passport_no'],
                    'doe' => date('m/d/Y', strtotime($row['doe'])),
                    'info' => $row['full_name']
                        .(isset($row['phone_number']) && $row['phone_number'] ?  (' - ' . $row['phone_number']) : ' ')
                        . ' - ' . $row['passport_no']
                        . ' - ' .date('d/m/Y', strtotime($row['dob'])),
                    "/bin/true" => $_REQUEST['passport_no'],
                );
            }
        }

        return $this->json_encode(array_values($users));
    }

    function savePassportPhoto() {
        global $thisstaff;

        if (!$thisstaff || !$thisstaff->getId()) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa đăng nhập',
                ]
            );
            exit;
        }

        if (!isset($_POST['booking_code']) || empty(trim($_POST['booking_code']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Điền mã booking trước khi chọn hình.',
                ]
            );
            exit;
        }
        if (!isset($_POST['data']) || empty(trim($_POST['data']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn hình.',
                ]
            );
            exit;
        }
        if (!isset($_POST['tour_id']) || empty(trim($_POST['tour_id']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn tour.',
                ]
            );
            exit;
        }
        $booking_code = trim($_POST['booking_code']);
        $tour_id = trim($_POST['tour_id']);
        $imgData = str_replace(' ','+',$_POST['data']);
        $imgData =  substr($imgData,strpos($imgData,",")+1);
        $imgData = base64_decode($imgData);
        $local_filename = md5(rand(0,9999).$_POST['name'].microtime(1));
        $filePath = ROOT_DIR.'passport_upload/'.$local_filename;
        $upload_filename = $_POST['name'];
        $file = fopen($filePath, 'w');
        fwrite($file, $imgData);
        fclose($file);

        $sql = sprintf("INSERT INTO pax_passport_photo SET 
            filename=%s,
            upload_filename=%s,
            booking_code=%s,
            tour_id=%s,
            upload_at=NOW(), 
            upload_by=%s, 
            status=0
            "
            ,db_input($local_filename)
            ,db_input($upload_filename)
            ,db_input($booking_code)
            ,db_input($tour_id)
            ,db_input($thisstaff->getId())
        );

        if (!db_query($sql)) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Lưu bị lỗi.',
                ]
            );
            exit;
        }

        echo json_encode(
            [
                'result' => 1,
                'message' => 'Đã lưu thành công. Hãy upload passport kế tiếp nếu có.',
            ]
        );
        exit;
    }

    function savePassportPhotoHasPassport() {
        global $thisstaff;

        if (!$thisstaff || !$thisstaff->getId()) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa đăng nhập',
                ]
            );
            exit;
        }
        if(!isset($_POST['passport_no']) || !$_POST['passport_no'] || trim($_POST['passport_no']) === '')
        {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa có số passport',
                ]
            );
            exit;
        }
        $passport_check = \Tugo\PassportPhotoUpload::lookup(['passport_no' => $_POST['passport_no']]);
        if($passport_check)
        {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Không thành công. Số passport này đã có ảnh',
                ]
            );
            exit;
        }
        $passport_no = trim($_POST['passport_no']);
        $imgData = str_replace(' ','+',$_POST['data']);
        $imgData =  substr($imgData,strpos($imgData,",")+1);
        $imgData = base64_decode($imgData);
        $local_filename = md5(rand(0,9999).$_POST['name'].microtime(1));
        $filePath = ROOT_DIR.'passport_upload/'.$local_filename;
        try {
            $file = fopen($filePath, 'w');
            fwrite($file, $imgData);
            fclose($file);
        }catch (Exception $ex){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => $ex->getMessage(),
                ]
            );
            exit;
        }

        $sql = sprintf("REPLACE INTO passport_photo_upload SET 
            passport_no=%s,
            filename=%s,                                    
            upload_at=NOW(), 
            upload_by=%s 
            "
            ,db_input($passport_no)
            ,db_input($local_filename)
            ,db_input($thisstaff->getId())
        );

        if (!db_query($sql)) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Lưu bị lỗi.',
                ]
            );
            exit;
        }

        echo json_encode(
            [
                'result' => 1,
                'message' => 'Đã lưu thành công.',
            ]
        );
        exit;
    }

    function reUploadPassportPhoto() {
        global $thisstaff;

        if (!$thisstaff || !$thisstaff->getId()) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa đăng nhập',
                ]
            );
            exit;
        }

        if (!isset($_POST['booking_code']) || empty(trim($_POST['booking_code']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Điền mã booking trước khi chọn hình.',
                ]
            );
            exit;
        }
        if (!isset($_POST['data']) || empty(trim($_POST['data']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn hình.',
                ]
            );
            exit;
        }
        if (!isset($_POST['tour_id']) || empty(trim($_POST['tour_id']))) {
            echo json_encode(
                [
                    'result' => 0,
                    'message' => 'Chưa chọn tour.',
                ]
            );
            exit;
        }
        $passport_id = (int)($_POST['passport_id']);

        $imgData = str_replace(' ','+',$_POST['data']);
        $imgData =  substr($imgData,strpos($imgData,",")+1);
        $imgData = base64_decode($imgData);

        $passport = \Tugo\PassportPhoto::lookup($passport_id);

        if(!$passport) {
            echo json_encode(
                [
                    'result'  => 0,
                    'message' => 'Không tìm thấy passport cần cập nhật',
                ]
            );
            exit;
        }
        $local_filename = $passport->filename;
        if($local_filename === null || $local_filename === '' )
            $local_filename = md5(rand(0,9999).$_POST['name'].microtime(1));

        $filePath = ROOT_DIR.'passport_upload/'.$local_filename;
        $upload_filename = $_POST['name'];
        $file = fopen($filePath, 'w');
        fwrite($file, $imgData);
        fclose($file);
        try{
            $passport->setAll([
                'filename'        => $local_filename,
                'upload_filename' => $upload_filename,
                'upload_at'       => new SqlFunction("NOW"),
                'status'          => 0
            ]);
            $passport->save();
        }catch (Exception $ex){
            echo json_encode(
                [
                    'result' => 0,
                    'message' => $ex->getMessage(),
                ]
            );
            exit;
        }

        echo json_encode(
            [
                'result' => 1,
                'message' => 'Đã cập nhật ảnh passport thành công.',
            ]
        );
        exit;
    }

    function getBookingQuantity() {
        $code = trim($_GET['booking_code']);
        $code = preg_replace('/^TG123\-/', '', $code);
        $code = preg_replace('/^0+/', '', $code);
        $sql = "SELECT total_quantity FROM booking_view WHERE trim(LEADING '0' FROM trim(LEADING '"
            .BOOKING_CODE_PREFIX."' FROM booking_code)) LIKE ".db_input($code)." LIMIT 1";
        $res = db_query($sql);

        if (!$res) return 0;
        $row = db_fetch_array($res);
        if (!$row) return 0;
        if (!isset($row['total_quantity'])) return 0;

        $qty = intval(trim($row['total_quantity']));
        echo json_encode(
            [
                'total' => $qty,
                'added' => 0,
                'remain' => $qty-0,
            ]
        );
        exit;
    }

}
?>

<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class PhoneNumberListModel extends \VerySimpleModel {
    static $meta = [
        'table' => PHONE_NUMBER_LIST_TABLE,
        'pk' => ['id'],
    ];
}

class PhoneNumberList extends PhoneNumberListModel {
    public static function push($phone_number, $get_id = true) {
        $phone_number = _String::getPhoneNumbers($phone_number);
        if (!$phone_number)
            throw new \Exception('Invalid phone number');

        $phone_number = $phone_number[0];
        $phone = PhoneNumberList::lookup(['phone_number' => $phone_number]);
        if (!$phone) {
            $phone = PhoneNumberList::create(['phone_number' => $phone_number]);
            $id = $phone->save();
            if (!$id) return null;
            if ($get_id) return $id;
        }

        if ($get_id) return $phone->id;
        return $phone;
    }
}

class EmailListModel extends \VerySimpleModel {
    static $meta = [
        'table' => EMAIL_LIST_TABLE,
        'pk' => ['id'],
    ];
}

class EmailList extends EmailListModel {

}

class SourceListModel extends \VerySimpleModel {
    static $meta = [
        'table' => SOURCE_LIST_TABLE,
        'pk' => ['id'],
    ];
}

class SourceList extends SourceListModel {

}

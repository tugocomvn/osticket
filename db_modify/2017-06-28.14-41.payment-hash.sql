CREATE TABLE ost_hash_data
(
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `hash` VARCHAR(33),
  `type` VARCHAR(16),
  `data` LONGTEXT
);
CREATE UNIQUE INDEX ost_hash_data_hash_uindex ON ost_hash_data (`hash`);
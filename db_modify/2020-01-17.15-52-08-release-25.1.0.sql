create table tour_net_price
(
    id           int auto_increment
        primary key,
    tour_id      int      not null,
    quantity     int      not null,
    retail_price decimal(12,0)  null,
    created_by   int      not null,
    created_at   datetime not null
);

/*branch: reservation */

alter table tour
    add status_edit_tour smallint default 0 not null;

alter table ost_groups
    add can_request_edit_tour tinyint(1) unsigned default 0 null;

alter table ost_groups
    add can_approve_edit_tour tinyint(1) unsigned default 0 null;




/*branch: visa */

create table visa_follow_action
(
    id        int auto_increment
        primary key,
    profile_id int          null,
    action_id int          null,
    time      datetime     null,
    subject   varchar(255) null,
    content   mediumtext   null
)
    collate = utf8_unicode_ci;

create index visa_follow_action_action_id_index
    on visa_follow_action (action_id);

create index visa_follow_action_profile_id_index
    on visa_follow_action (profile_id);

create index visa_follow_action_time_index
    on visa_follow_action (time);






alter table tugo_osticket_customer
	add date_value date null;

create index tugo_osticket_customer_date_value_index
	on tugo_osticket_customer (date_value);

alter table tugo_osticket_customer_age
	add date_value date null;

create index tugo_osticket_customer_age_date_value_index
	on tugo_osticket_customer_age (date_value);

alter table tugo_osticket_customer_age
	add year_of_birth int null;

alter table tugo_osticket_customer_age change year_of_birth dob_year int null;
alter table tugo_osticket_booking
	add date_value date null;
alter table tugo_osticket_ticket
	add date_value date null;

create index tugo_osticket_ticket_date_value_index
	on tugo_osticket_ticket (date_value);

create index tugo_osticket_booking_date_value_index
	on tugo_osticket_booking (date_value);



alter table tugo_osticket_call
	add date_value date null;

create index tugo_osticket_call_date_value_index
	on tugo_osticket_call (date_value);


create index tugo_osticket_sms_date_value_index
	on tugo_osticket_sms (date_value);


alter table tugo_osticket_call drop key index2;
alter table tugo_osticket_sms drop key index2;
alter table tugo_osticket_customer drop key tugo_osticket_xxid_uindex;
alter table tugo_osticket_customer_age drop key index2;
alter table tugo_osticket_booking drop key index2;

alter table tugo_osticket_sms modify week_of_the_year varchar(2) null comment 'WEEK';
alter table tugo_osticket_sms modify week_of_year varchar(6) null comment 'YEAR_WEEK';


update tugo_osticket_customer_age
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

update tugo_osticket_customer
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

update tugo_osticket_booking
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

update tugo_osticket_call
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

update tugo_osticket_ticket
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;

alter table tugo_osticket_sms
	add date_value date null;
update tugo_osticket_sms
set date_value=concat(year, "-", month_of_the_year, "-", day_of_month) where 1;


/*branch: visa */

alter table visa_follow_action
    add staff_id int unsigned default 0 not null;

create index visa_follow_action_staff_id_index on visa_follow_action(staff_id)


/*branch: visa */

create table tugo_osticket_visa
(
    id                int unsigned auto_increment
        primary key,
    year              varchar(4)  null comment 'YEAR',
    month_of_year     varchar(6)  null comment 'YEAR_MONTH',
    month_of_the_year varchar(2)  null comment 'MONTH',
    week_of_year      varchar(2)  null comment 'YEAR_WEEK',
    week_of_the_year  varchar(6)  null comment 'WEEK',
    date              varchar(8)  null comment 'YYYYMMDD',
    day_of_month      varchar(2)  null comment 'DD',
    day_of_week       varchar(1)  null comment 'D DAY_OF_WEEK',
    day_of_week_name  varchar(10) null comment 'DAY_OF_WEEK_NAME',
    hour_of_day       varchar(10) null comment 'YYYYMMDDhh',
    hour              varchar(2)  null comment 'hh',
    minute_of_day     varchar(12) null comment 'date_hour_minute',
    minute            varchar(2)  null comment 'mm',
    second_of_day     varchar(14) null comment 'YYYYMMDDhhmmss',
    second            varchar(2)  null comment 'ss',
    staff_id          int unsigned,
    staff_username    varchar(45) null,
    action_id         int,
    amount_profile       int unsigned default 0 comment 'amount_profile of staff '

);


/*branch: visa */

create table passport_photo_upload
(
    id int unsigned auto_increment,
    passport_no varchar(32) null,
    filename varchar(64) null,
    upload_at datetime null,
    upload_by int null,
    constraint passport_photo_upload_pk
        primary key (id)
);

create unique index passport_photo_upload_filename_uindex
    on passport_photo_upload (filename);

create unique index passport_photo_upload_passport_no_uindex
    on passport_photo_upload (passport_no);

alter table tour modify color_code varchar(7) null;

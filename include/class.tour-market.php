<?php
class TourMarketModel extends \VerySimpleModel {
    static $meta = [
        'table' => TOUR_MARKET_TABLE,
        'pk' => ['id'],
    ];
}

class TourMarket extends TourMarketModel {
    public static function loadList($access_market = []) {
        $where = " WHERE 1 ";
        if(isset($access_market) && is_array($access_market))
            if(count($access_market) > 0)
                $where .= " AND id IN (" . implode(",", $access_market) . ") ";
            else
                $where .= " AND 1 = 0 ";


        $sql = "SELECT * FROM ". self::$meta['table'].$where." ORDER BY name ASC";
        $res = db_query($sql);
        $data = [];
        while($res && ($row = db_fetch_array($res))) {
            $data[ $row['id'] ] = $row;
        }
        return $data;
    }
}
class MarketStatus extends GeneralObject {
    const ENABLE = 1;
    const DISABLE = 0;
}



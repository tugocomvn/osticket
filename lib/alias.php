<?php
if (!function_exists('config')) {
    function config($key, $default = null) {
        $config = \Tugo\Config::getInstance();
        if (!is_array($key))
            return $config->get($key) ?: $default;
        $config->set($key);
    }
}

if (!function_exists('load_config')) {
    function load_config($path = null) {
        return \Tugo\Config::getInstance($path);
    }
}

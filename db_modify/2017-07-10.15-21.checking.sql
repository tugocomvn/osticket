CREATE TABLE ost_checkin
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  staff_id INT,
  checkin_time DATETIME,
  ip_address VARCHAR(16),
  approved_by INT,
  note VARCHAR(255)
);
ALTER TABLE ost_checkin COMMENT = 'chấm công';


ALTER TABLE ost_groups ADD can_view_personal_checkin TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_all_checkin TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_do_checkin TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_ticket_activity TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_general_statistics TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_agent_directory TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_agent_statistics TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_guest_directory TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_edit_guest_info TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_view_organizations TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_create_organizations TINYINT(1) DEFAULT  '0';
ALTER TABLE ost_groups ADD can_edit_organizations TINYINT(1) DEFAULT  '0';
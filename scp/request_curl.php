<?php 
/** 
 * Created by PhpStorm. 
 * User: minhjoko 
 * Date: 6/6/18 
 * Time: 8:25 AM 
 */ 
class RequestUrl{ 
    public static function CallCurl($url,$apiKey,$json){ 
        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 
        $result = curl_exec($ch); 
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
        curl_close($ch); 
        echo $status_code; 
        echo "<pre>"; print_r(json_encode($result)); echo"</pre>"; 
    }
    public static function CurlPatch($url,$apiKey,$json){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo $status_code;
        echo "<pre>"; print_r(json_encode($result)); echo"</pre>";
    }
}
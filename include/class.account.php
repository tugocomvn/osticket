<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class SettlementSpendItemModel extends \VerySimpleModel {
    static $meta = [
        'table' => ACC_SPEND_ITEM,
        'pk' => ['id'],
        'ordering' => ['name'],
    ];
}

class SettlementSpendItem extends SettlementSpendItemModel {
    public static function getPagination(array $params, $offset, $limit = 30, &$total) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY name DESC ";
        $where = "  ";
        if (isset($params['name']) && !is_null($params['name'])) {
            $where .= " AND name LIKE ".db_input('%'.$params['name'].'%'). " ";
        }
        if (isset($params['description']) && !is_null($params['description'])) {
            $where .= " AND description LIKE ".db_input('%'.$params['description'].'%'). " ";
        }

        $sql = " SELECT * FROM ".ACC_SPEND_ITEM." WHERE 1 $where $order $limit ";

        $res = db_query($sql);
        $total = db_num_rows(db_query(" SELECT * FROM ".ACC_SPEND_ITEM." WHERE 1 $where "));

        return $res;
    }

    public static function getAll($view = null) {
        $order = " ORDER BY name ASC ";
        $sql = " SELECT * FROM ".ACC_SPEND_ITEM." WHERE 1 ";

        if ($view) {
            $sql .= " AND find_in_set(".db_input($view).", views)>0 ";
        }

        $sql .= "  $order  ";
        $res = db_query($sql);
        return $res;
    }
}

class SettlementExpectedDetailModel extends \VerySimpleModel {
    static $meta = [
        'table' => ACC_SPEND_DETAIL,
        'pk' => ['tour_id', 'account_item_id'],
    ];
}

class SettlementExpectedDetail extends SettlementExpectedDetailModel {
    public static function getPagination(array $params, $offset, $limit = 30, &$total) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY i.name ASC ";
        $where = "  ";

        if (isset($params['tour_id']) && intval($params['tour_id']))
            $where .= " AND tour_id= ".db_input($params['tour_id'])." ";

        if (isset($params['account_item_id']) && intval($params['account_item_id']))
            $where .= " AND account_item_id= ".db_input($params['account_item_id'])." ";

        $sql = " SELECT d.* FROM ".ACC_SPEND_DETAIL." d JOIN ".ACC_SPEND_ITEM." i ON i.id=d.account_item_id  WHERE 1 $where $order ";

        $res = db_query($sql);
        $total = db_num_rows($res);
        return $res;
    }

    public static function cloneItems($from, $to) {
        global $thisstaff;
        $sql = "
        insert into ".ACC_SPEND_DETAIL."(
            tour_id,
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            created_by,
            created_at
        )
        select
            ".db_input($to).",
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            ".db_input($thisstaff->getId()).",
            NOW()
        from ".ACC_SPEND_DETAIL."
        where tour_id=".db_input($from);

        try {
            return db_query($sql);
        } catch(Exception $ex) { return false; }
    }

    public static function cloneItems2Actual($tour_id) {
        global $thisstaff;
        $sql = "
        insert into ".ACC_SPEND_ACTUAL_DETAIL."(
            tour_id,
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            created_by,
            created_at
        )
        select
            ".db_input($tour_id).",
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            ".db_input($thisstaff->getId()).",
            NOW()
        from ".ACC_SPEND_DETAIL."
        where tour_id=".db_input($tour_id);

        try {
            return db_query($sql);
        } catch(Exception $ex) { return false; }
    }

    public static function calc_outcome($tour_id) {
        $sql = "SELECT SUM(total) as total FROM ".ACC_SPEND_DETAIL." WHERE tour_id=".db_input($tour_id);
        $res = db_query($sql);
        if (!$res) return 0;
        $total = db_result($res);
        return floatval($total);
    }
}



class SettlementActualDetailModel extends \VerySimpleModel {
    static $meta = [
        'table' => ACC_SPEND_ACTUAL_DETAIL,
        'pk' => ['tour_id', 'account_item_id'],
    ];
}

class SettlementActualDetail extends SettlementActualDetailModel {
    public static function getPagination(array $params, $offset, $limit = 30, &$total) {
        $limit = " LIMIT $limit OFFSET $offset ";
        $order = " ORDER BY i.name ASC ";
        $where = "  ";

        if (isset($params['tour_id']) && intval($params['tour_id']))
            $where .= " AND tour_id= ".db_input($params['tour_id'])." ";

        if (isset($params['account_item_id']) && intval($params['account_item_id']))
            $where .= " AND account_item_id= ".db_input($params['account_item_id'])." ";

        $sql = " SELECT d.* FROM ".ACC_SPEND_ACTUAL_DETAIL." d JOIN ".ACC_SPEND_ITEM." i ON i.id=d.account_item_id WHERE 1 $where $order ";

        $res = db_query($sql);
        $total = db_num_rows($res);
        return $res;
    }

    public static function cloneItems($from, $to) {
        global $thisstaff;
        $sql = "
        insert into ".ACC_SPEND_ACTUAL_DETAIL."(
            tour_id,
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            created_by,
            created_at
        )
        select
            ".db_input($to).",
            account_item_id,
            quantity,
            value,
            total,
            total_vnd,
            ".db_input($thisstaff->getId()).",
            NOW()
        from ".ACC_SPEND_ACTUAL_DETAIL."
        where tour_id=".db_input($from);

        try {
            return db_query($sql);
        } catch(Exception $ex) { return false; }
    }

    public static function calc_outcome($tour_id) {
        $sql = "SELECT SUM(total) as total FROM ".ACC_SPEND_ACTUAL_DETAIL." WHERE tour_id=".db_input($tour_id);
        $res = db_query($sql);
        if (!$res) return 0;
        $total = db_result($res);
        return floatval($total);
    }
}

class SettlementGeneralModel extends \VerySimpleModel {
    static $meta = [
        'table' => ACC_GENERAL,
        'pk' => ['tour_id'],
    ];
}

class SettlementGeneral extends SettlementGeneralModel {

}

class SettlementViewModel extends \VerySimpleModel {
    static $meta = [
        'table' => ACC_VIEW,
        'pk' => ['name'],
    ];
}

class SettlementView extends SettlementViewModel {
    public static function getAll() {
        $order = " ORDER BY name ASC ";
        $sql = " SELECT DISTINCT name FROM ".ACC_VIEW." WHERE 1 ";
        $sql .= "  $order  ";
        $res = db_query($sql);
        return $res;
    }
}

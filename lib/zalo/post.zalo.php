<?php

try {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        throw new Exception('POST method is required.', 405);

    if (!isset($_POST['code']) || empty($_POST['code']) || $_POST['code'] !== '*&CH*a(21*h&A^4%34pSU$v^&s*()')
        throw new Exception('Goodbye my love.', 403);

    if (!isset($_POST['phone_number']) || empty($_POST['phone_number']))
        throw new Exception('Phone number is required.', 400);

    if (!isset($_POST['message']) || empty($_POST['message']))
        throw new Exception('Message is required.', 400);

    if (!isset($_POST['checking']) || empty($_POST['checking'])
        || $_POST['checking'] !== hash('sha256', ($_POST['phone_number'].$_POST['message'].$_POST['phone_number'])))
        throw new Exception('I love you.', 403);

    $errors = [];
    if (! ( $msgId = _Zalo::send($_POST['phone_number'], $_POST['message'], $errors) ) )
        throw new Exception(isset($errors['sending_failed']) ? $errors['sending_failed']
            : (isset($errors['error']) ? $errors['error'] : 'Unknown Error'), 500);

    http_response_code(201);
    echo json_encode(['result' => 'Success', 'msgId' => $msgId]);
    exit;
} catch(Exception $e) {
    http_response_code($e->getCode() ?: 500);
    echo json_encode(['error' => $e->getMessage(), 'dump' => serialize($errors)]);
    exit;
}


<?php 
if (!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
?>
<style>
.required {
color: #FF0000;
}
</style>
<h2>Flight Ticket - Fast Create</h2>
<div class="centered">
<p>Hãng bay<span class="required">(*)</span><select name="airline" id="airline" class="input-field" style="height:30px">
    <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
        <?php while ($airlines_list && ($row = db_fetch_array($airlines_list))){
            echo sprintf('<option value="%d" %s>%s</option>',
            $row['id'],($row['id']?'"selected"':''),$row['airline_name']);
        }?>
    </select></p>
</div>
<body>
    <div class="wrapper">
        <div class="rowLayout">
                <p>
                    <button name="view" id="view">Xem trước nội dung</button>
                </p>
            <div id="message" class="message">Click "Xem trước nội dung" to load data format</div>
            <p>Chú ý: Mỗi dòng là thông tin của 01 chặng bay. Bao gồm cả code vé</p>
            <div id="main_flight_ticket"></div>
            <h2>Xem trước nội dung</h2>
            <div id="view_flight_ticket"></div>
            <p class="centered">
                <button class="btn_sm btn-primary" id="save" name="save">Confirm & Save</button>
                <a href="<?php echo $cfg->getUrl()."scp/flight-list.php"?>" class="btn_sm btn-danger">Cancel</a>
            </p>
        </div>
    </div>
</body>
<script>
    var
        $container = $("#main_flight_ticket"),
        $console = $("#message"),
        $parent = $container.parent(),
        _hot;
    _hot = new Handsontable($container[0], {
        columnSorting: true,
        startRows: 4,
        startCols: 6,
        rowHeaders: true,
        className: "htCenter",
        width: '100%',
        licenseKey: 'non-commercial-and-evaluation',
        colWidths: [150, 150, 150, 100, 250, 150],
        height: '200px',
        nestedHeaders: [
            ['HÀNH TRÌNH', 'NGÀY ĐI', 'SHCB', 'GIỜ BAY', 'CODE', 'SL'],
        ],
        columns: [
            {},
            {},
            {},
            {},
            {},
            {},
        ],
        minSpareCols: 0,
        minSpareRows: 1,
        contextMenu: true,
    });
    $parent.find('button[name=view]').click(function () {
        $.ajax({
        url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/flight-ticket/load',
        data: {data: _hot.getData()}, // returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
            if (res.result === 1) {
                var data = [], row;
                for (var i = 0, ilen = res.all_values.length; i < ilen; i++) {
                row = [];
                row[0] = res.all_values[i].airport_code_from_to;
                row[1] = res.all_values[i].departure_at;
                row[2] = res.all_values[i].flight_code;
                row[3] = res.all_values[i].departure_time;
                row[4] = res.all_values[i].arrival_time;
                row[5] = res.all_values[i].arrival_at;
                row[6] = res.all_values[i].flight_ticket_code;
                row[7] = res.all_values[i].total_quantity;
                data[res.index[i] + 1] = row;
                }
                hot.loadData(data);
                $console.text(res.message);
                messageSuccess();
            }else{
                $console.text(res.message);
                messageError();
            }
            }
        });
    });
    $parent.find('button[name=save]').click(function () {
        if (confirm('Bạn đã kiểm tra kỹ thông tin chưa?')) {    
        var airline = $('#airline').find(":selected").val();        
        $.ajax({
        url: '<?php echo $cfg->getUrl() ?>scp/ajax.php/flight-ticket/save',
        data: {"data" : _hot.getData(), "airline" : airline },
        dataType: 'json',
        type: 'POST',
        success: function (res) {
            if (res.result === 1) {
                $console.text(res.message);
                messageSuccess();
                <?php $url = $cfg->getUrl().'scp/flight-ticket-list.php'; ?>
                parent.setTimeout(function() { parent.window.location = "<?php echo $url ?>"; }, 1500);
            }else{
                $console.text(res.message);
                messageError();
            }
            }
            });
        }
    });

    function messageError(){
        document.getElementById("message").style.color = "Red";
    }

    function messageSuccess(){
        document.getElementById("message").style.color = "#4CAF50";
    }
    var
        $_container = $("#view_flight_ticket"),
        $console = $("#message"),
        $parent = $_container.parent(),
        hot;
    hot = new Handsontable($_container[0], {
        columnSorting: true,
        startRows: 4,
        startCols: 7,
        rowHeaders: true,
        className: "htCenter",
        width: '100%',
        licenseKey: 'non-commercial-and-evaluation',
        colWidths: [130, 130, 130, 80, 80, 150, 140, 110],
        height: '200px',
        nestedHeaders: [
            ['HÀNH TRÌNH', 'NGÀY ĐI', 'SHCB', {label: 'GIỜ BAY', colspan: 3}, 'CODE', 'SL'],
        ],
        columns: [
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
            {
                readOnly: true
            },
        ],
        minSpareCols: 0,
        minSpareRows: 1,
        contextMenu: true,
    });
    $("#save").attr("disabled", true);
    $("#view").click(function() {
        $("#save").attr("disabled", false);
    }); 
</script>
<?php
require('staff.inc.php');
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
if (!$thisstaff->canViewMemberList()) exit('Access Denied');

require_once "class.pax.php";
require_once INCLUDE_DIR . 'class.note.php';
require_once INCLUDE_DIR . 'class.point.php';

$user = null;
if($_GET && isset($_GET)) {
    if ($_REQUEST['id'] && !($user = \Tugo\User::get(['customer_number' => $_REQUEST['id']])))
        $errors['err'] = sprintf(__('%s: Unknown or invalid'), _N('end user', 'end users', 1));
}
$page = $user ? 'membership-view.inc.php' : 'membership.inc.php';

$nav->setTabActive('users');
require(STAFFINC_DIR . 'header.inc.php');
require(STAFFINC_DIR . $page);
include(STAFFINC_DIR . 'footer.inc.php');


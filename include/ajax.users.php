<?php
/*********************************************************************
    ajax.users.php

    AJAX interface for  users (based on submitted tickets)
    XXX: osTicket doesn't support user accounts at the moment.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

if(!defined('INCLUDE_DIR')) die('403');

include_once(INCLUDE_DIR.'class.ticket.php');
require_once INCLUDE_DIR.'class.note.php';
require_once INCLUDE_DIR.'tugo.user.php';

class UsersAjaxAPI extends AjaxController {

    /* Assumes search by emal for now */
    function search($type = null) {

        if(!isset($_REQUEST['q']) || empty(trim($_REQUEST['q'])) || strlen($_REQUEST['q']) < 3) {
            Http::response(400, __('Query argument is required or too short'));
        }

        if( !(_String::isMobileNumber($_REQUEST['q']) || strpos(trim($_REQUEST['q']), '@') !== false) ) {
            Http::response(400, __('Chỉ search số điện thoại hoặc email'));
        } // chỉ search số điện thoại hoặc email, cho nó nhanh

        $limit = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit']:10;
        $users=array();
        $emails=array();
        $_REQUEST['q'] = trim($_REQUEST['q']);

        if (!$type || !strcasecmp($type, 'remote')) {
            foreach (AuthenticationBackend::searchUsers($_REQUEST['q']) as $u) {
                $name = new PersonsName(array('first' => $u['first'], 'last' => $u['last']));
                $users[] = array('email' => $u['email'], 'name'=>(string) $name,
                    'info' => "{$u['email']} - $name (remote)",
                    'id' => "auth:".$u['id'], "/bin/true" => $_REQUEST['q']);
                $emails[] = $u['email'];
            }
        }

        if (!$type || !strcasecmp($type, 'local')) {
            $remote_emails = ($emails = array_filter($emails))
                ? ' OR email.address IN ('.implode(',',db_input($emails)).') '
                : '';

            $q = str_replace(' ', '%', $_REQUEST['q']);
            $escaped = db_input($q, false);


            if (is_numeric(preg_replace('/\s/', '', $_REQUEST['q']))) {
                $phone = _String::formatPhoneNumber($escaped);
                $sql = sprintf(
                    "SELECT DISTINCT
                          user.id,
                          email.address,
                          name,
                          search.phone_number
                        FROM %s `search`
                          JOIN %s user
                            ON search.object_id = user.id AND search.object_type = 'U' 
                            AND (search.phone_number LIKE %s OR search.phone_number LIKE %s)
                          JOIN %s email ON user.id = email.user_id"
                    , INDEX_PHONE_TABLE
                    , USER_TABLE
                    , (in_array(strlen($phone), [10, 11]) ? db_input($phone) : db_input('%'.$phone.'%'))
                    , (in_array(strlen($phone), [10, 11]) ? db_input(_string::convertMobilePhoneNumber($phone)) : db_input('%'.$phone.'%'))
                    , USER_EMAIL_TABLE
                );
            } else {
                $sql = sprintf(
                    " SELECT DISTINCT user.id, %s as address, name, v.value 
                        FROM ost_user user
                               LEFT JOIN ost_form_entry entry ON (entry.object_type = 'U' AND entry.object_id = user.id)
                               LEFT JOIN ost_form_entry_values v ON (v.entry_id = entry.id and v.field_id=%d)
                        WHERE user.id in (
                          SELECT DISTINCT email.user_id
                          FROM  ost_user_email email
                          WHERE email.address LIKE %s
                          )
                          LIMIT 10"
                    , db_input($escaped)
                    , USER_PHONE_NUMBER_FIELD
                    , db_input($escaped)
                );
            }

            if(($res=db_query($sql)) && db_num_rows($res)){
                while(list($id,$email,$name,$phonenumber)=db_fetch_row($res)) {
                    $no_add = false;
                    foreach ($users as $i=>$u) {
                        if ($u['email'] == $email) {
                            if (!isset($u['phonenumber']) || !is_numeric($u['phonenumber']) || !$u['phonenumber']) {
                                unset($users[$i]);
                            } else {
                                $no_add = true;
                            }
                            break;
                        }
                    }

                    if ($no_add) continue;

                    $info = preg_match('/\d+@tugo/', $email) ? "$email - $name" : "$email - $name - $phonenumber";
                    $name = Format::htmlchars(new PersonsName($name));
                    $users[] = array(
                        'email'=>$email,
                        'name'=>$name,
                        'info'=>$info,
                        "id" => $id,
                        "phonenumber" => is_numeric($phonenumber) ? $phonenumber : '',
                        "/bin/true" => $_REQUEST['q'],
                    );
                }
            }

            usort($users, function($a, $b) { return strcmp($a['name'], $b['name']); });
        }

        return $this->json_encode(array_values($users));

    }

    function preview($id) {
        global $thisstaff;

        if(!$thisstaff)
            Http::response(403, 'Login Required');
        elseif(!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $info = array(
                'title' => '',
                'useredit' => sprintf('#users/%d/edit', $user->getId()),
                );
        ob_start();
        echo sprintf('<div style="width:650px; padding: 2px 2px 0 5px;"
                id="u%d">', $user->getId());
        include(STAFFINC_DIR . 'templates/user.tmpl.php');
        echo '</div>';
        $resp = ob_get_contents();
        ob_end_clean();

        return $resp;

    }


    function editUser($id) {
        global $thisstaff;

        if(!$thisstaff)
            Http::response(403, 'Login Required');
        elseif(!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $info = array(
            'title' => sprintf(__('Update %s'), Format::htmlchars($user->getName()))
        );
        $forms = $user->getForms();

        include(STAFFINC_DIR . 'templates/user.tmpl.php');
    }

    function updateUser($id) {
        global $thisstaff;

        if(!$thisstaff)
            Http::response(403, 'Login Required');
        elseif(!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $errors = array();
        if ($user->updateInfo($_POST, $errors, true) && !$errors)
             Http::response(201, $user->to_json());

        $forms = $user->getForms();
        include(STAFFINC_DIR . 'templates/user.tmpl.php');
    }

    function register($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $errors = $info = array();
        if ($_POST) {
            // Register user on post
            if ($user->getAccount())
                $info['error'] = __('User already registered');
            elseif ($user->register($_POST, $errors))
                Http::response(201, 'Account created successfully');

            // Unable to create user.
            $info = Format::htmlchars($_POST);
            if ($errors['err'])
                $info['error'] = $errors['err'];
            else
                $info['error'] = __('Unable to register guest - try again!');
        }

        include(STAFFINC_DIR . 'templates/user-register.tmpl.php');
    }

    function manage($id, $target=null) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        if (!($account = $user->getAccount()))
            return self::register($id);

        $errors = array();
        $info = $account->getInfo();

        if ($_POST) {
            if ($account->update($_POST, $errors))
                Http::response(201, 'Account updated successfully');

            // Unable to update account
            $info = Format::htmlchars($_POST);

            if ($errors['err'])
                $info['error'] = $errors['err'];
            else
                $info['error'] = __('Unable to update account - try again!');
        }

        $info['_target'] = $target;

        include(STAFFINC_DIR . 'templates/user-account.tmpl.php');
    }

    function delete($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $info = array();
        if ($_POST) {
            if ($user->tickets->count()) {
                if (!$thisstaff->canDeleteTickets()) {
                    $info['error'] = __('You do not have permission to delete a guest with tickets!');
                } elseif ($_POST['deletetickets']) {
                    foreach($user->tickets as $ticket)
                        $ticket->delete();
                } else {
                    $info['error'] = __('You cannot delete a guest with tickets!');
                }
            }

            if (!$info['error'] && $user->delete())
                 Http::response(204, 'Guest deleted successfully');
            elseif (!$info['error'])
                $info['error'] = __('Unable to delete guest - try again!');
        }

        include(STAFFINC_DIR . 'templates/user-delete.tmpl.php');
    }

    function getUser($id=false) {

        if(($user=User::lookup(($id) ? $id : $_REQUEST['id'])))
           Http::response(201, $user->to_json());

        $info = array('error' => sprintf(__('%s: Unknown or invalid ID.'), _N('end guest', 'end guests', 1)));

        return self::_lookupform(null, $info);
    }

    function lookup() {
        return self::addUser();
    }

    function addUser() {

        $info = array();

        if (!AuthenticationBackend::getSearchDirectories())
            $info['lookup'] = 'local';

        if ($_POST) {
            $info['title'] = __('Add New User');
            $form = UserForm::getUserForm()->getForm($_POST);
            if (($user = User::fromForm($form)))
                Http::response(201, $user->to_json());

            $info['error'] = __('Error adding guest - try again!');
        }

        return self::_lookupform($form, $info);
    }

    function addRemoteUser($bk, $id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!$bk || !$id)
            Http::response(422, 'Backend and guest id required');
        elseif (!($backend = AuthenticationBackend::getSearchDirectoryBackend($bk))
                || !($user_info = $backend->lookup($id)))
            Http::response(404, 'Guest not found');

        $form = UserForm::getUserForm()->getForm($user_info);
        $info = array('title' => __(
            /* `remote` users are those in a remore directory such as LDAP */
            'Import Remote Guest'));
        if (!$user_info)
            $info['error'] = __('Unable to find guest in directory');

        include(STAFFINC_DIR . 'templates/user-lookup.tmpl.php');
    }

    function importUsers() {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');

        $info = array(
            'title' => __('Import Guest'),
            'action' => '#users/import',
            'upload_url' => "users.php?do=import-users",
        );

        if ($_POST) {
            $status = User::importFromPost($_POST['pasted']);
            if (is_string($status))
                $info['error'] = $status;
            else
                Http::response(201, "{\"count\": $status}");
        }
        $info += Format::input($_POST);

        include STAFFINC_DIR . 'templates/user-import.tmpl.php';
    }

    function selectUser($id) {

        if ($id)
            $user = User::lookup($id);

        $info = array('title' => __('Select Guest'));

        ob_start();
        include(STAFFINC_DIR . 'templates/user-lookup.tmpl.php');
        $resp = ob_get_contents();
        ob_end_clean();
        return $resp;

    }

    static function _lookupform($form=null, $info=array()) {

        if (!$info or !$info['title'])
            $info += array('title' => __('Lookup or create a guest'));

        ob_start();
        include(STAFFINC_DIR . 'templates/user-lookup.tmpl.php');
        $resp = ob_get_contents();
        ob_end_clean();
        return $resp;
    }

    function searchStaff() {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login required for searching');
        elseif (!$thisstaff->isAdmin())
            Http::response(403,
                'Administrative privilege is required for searching');
        elseif (!isset($_REQUEST['q']))
            Http::response(400, 'Query argument is required');

        $users = array();
        foreach (AuthenticationBackend::getSearchDirectories() as $ab) {
            foreach ($ab->search($_REQUEST['q']) as $u)
                $users[] = $u;
        }

        return $this->json_encode($users);
    }

    function updateOrg($id, $orgId = 0) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Required');
        elseif (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        $info = array();
        $info['title'] = sprintf(__('Organization for %s'),
            Format::htmlchars($user->getName()));
        $info['action'] = '#users/'.$user->getId().'/org';
        $info['onselect'] = 'ajax.php/users/'.$user->getId().'/org';

        if ($_POST) {
            if ($_POST['orgid']) { //Existing org.
                if (!($org = Organization::lookup($_POST['orgid'])))
                    $info['error'] = __('Unknown organization selected');
            } else { //Creating new org.
                $form = OrganizationForm::getDefaultForm()->getForm($_POST);
                if (!($org = Organization::fromForm($form)))
                    $info['error'] = __('Unable to create organization.')
                        .' '.__('Correct error(s) below and try again.');
            }

            if ($org && $user->setOrganization($org))
                Http::response(201, $org->to_json());
            elseif (! $info['error'])
                $info['error'] = __('Unable to add guest to organization.')
                    .' '.__('Correct error(s) below and try again.');

        } elseif ($orgId)
            $org = Organization::lookup($orgId);
        elseif ($org = $user->getOrganization()) {
            $info['title'] = sprintf(__('%s &mdash; Organization'), Format::htmlchars($user->getName()));
            $info['action'] = $info['onselect'] = '';
            $tmpl = 'org.tmpl.php';
        }

        if ($org && $user->getOrgId() && $org->getId() != $user->getOrgId())
            $info['warning'] = __("Are you sure you want to change guest's organization?");

        $tmpl = $tmpl ?: 'org-lookup.tmpl.php';

        ob_start();
        include(STAFFINC_DIR . "templates/$tmpl");
        $resp = ob_get_contents();
        ob_end_clean();

        return $resp;
    }

    function createNote($id) {
        if (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        require_once INCLUDE_DIR . 'ajax.note.php';
        $ajax = new NoteAjaxAPI();
        return $ajax->createNote('U'.$id);
    }

    function updatePoint($id) {
        if (!($user = User::lookup($id)))
            Http::response(404, 'Unknown guest');

        require_once INCLUDE_DIR . 'ajax.point.php';
        $ajax = new PointAjaxAPI();
        $uuid = $user->get('uuid');
        return $ajax->updatePoint($uuid);
    }
    function updatePointCustomer($id) {
        global $thisstaff;
        if (!$thisstaff)
            Http::response(403, "Login required");
        elseif(!$thisstaff->canEditPointLoyalty())
            Http::response(404, "Not access permission");
        elseif (!($user = \Tugo\User::get(['customer_number' => $id])))
            Http::response(404, 'Unknown guest');
        require_once INCLUDE_DIR . 'ajax.point.php';
        $ajax = new PointAjaxAPI();
        $uuid = $user['uuid'];
        return $ajax->updatePoint($uuid);
    }

    function manageForms($user_id) {
        $forms = DynamicFormEntry::forUser($user_id);
        $info = array('action' => '#users/'.Format::htmlchars($user_id).'/forms/manage');
        include(STAFFINC_DIR . 'templates/form-manage.tmpl.php');
    }

    function updateForms($user_id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, "Login required");
        elseif (!($user = User::lookup($user_id)))
            Http::response(404, "No such guest");
        elseif (!isset($_POST['forms']))
            Http::response(422, "Send updated forms list");

        // Add new forms
        $forms = DynamicFormEntry::forUser($user_id);
        foreach ($_POST['forms'] as $sort => $id) {
            $found = false;
            foreach ($forms as $e) {
                if ($e->get('form_id') == $id) {
                    $e->set('sort', $sort);
                    $e->save();
                    $found = true;
                    break;
                }
            }
            // New form added
            if (!$found && ($new = DynamicForm::lookup($id))) {
                $user->addForm($new, $sort);
            }
        }

        // Deleted forms
        foreach ($forms as $idx => $e) {
            if (!in_array($e->get('form_id'), $_POST['forms']))
                $e->delete();
        }

        Http::response(201, 'Successfully managed');
    }

    function getRank($id) {
        $customer_number = intval(trim($id));
        $user = \Tugo\User::get(['customer_number' => $customer_number]);
        if (!$user) return null;
        $rank_id = intval($user['rank']);

        if (!$rank_id) {
            $ost_user = \User::lookup(['uuid' => $user['uuid']]);
            if (!$ost_user) {
                if (!isset($user['phone_number']) || !$user['phone_number'])
                    return null;

                $ost_user = \User::lookupByPhone($user['phone_number']);

                if (!$ost_user) return null;

                $ost_user->set('uuid', $user['uuid']);
                $ost_user->save();
            }

            $user_form = $ost_user->getForms();
            $user_form = $user_form[0];
            $field = $user_form->getField('rank');
            if (!$field) return null;

            $conf = $field->get('configuration') ? json_decode($field->get('configuration'), true) : null;
            $default_rank = array_pop(array_keys($conf['default']));
            if (!$default_rank) return null;

            $rank_id = $default_rank;
        }
        $rank = DynamicListItem::lookup($rank_id);

        Http::response(201, $this->json_encode([
            'rank' => [
                'id' => $rank->getId(),
                'name' => $rank->getValue(),
                'description' => strval(array_pop($rank->getConfiguration())),
            ],
            'pax' => [
                'customer_name' => $user['customer_name'],
                'phone_number' => $user['phone_number'],
                'point' => $user['user_point'] ?: 0,
            ],
        ]), 'text/json');
    }

    function getUserFromNumber($id)
    {
        $phone_number = trim($id);
        $user = \Tugo\User::get(['phone_number' => $phone_number]);
        if (!$user) return null;
        Http::response(201, $this->json_encode([
            'pax' => [
                'uuid' => $user['uuid'],
                'customer_name' => $user['customer_name'],
                'customer_number' => $user['customer_number'],
                'phone_number' => $user['phone_number'],
                'point' => $user['user_point'] ?: 0,
            ],
        ]), 'text/json');
    }

}
?>

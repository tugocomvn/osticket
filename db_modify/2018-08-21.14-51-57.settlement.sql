ALTER TABLE account_spend_item MODIFY default_value decimal(11,2);

ALTER TABLE account_actual_detail MODIFY value decimal(11,2);
ALTER TABLE account_actual_detail MODIFY total decimal(11,2);
ALTER TABLE account_actual_detail MODIFY total_vnd decimal(11);

ALTER TABLE account_expected_detail MODIFY value decimal(11,2);
ALTER TABLE account_expected_detail MODIFY total decimal(11,2);
ALTER TABLE account_expected_detail MODIFY total_vnd decimal(11);

ALTER TABLE account_general MODIFY rate decimal(11);
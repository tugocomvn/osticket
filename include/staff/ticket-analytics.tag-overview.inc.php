<?php
TicketAnalytics::analytics($_REQUEST['from'], $_REQUEST['to'], 'tag_by_day', $_REQUEST, $chart_data, $table_data);
$total_tags = 0;
$tag_percent = $tag_overview = $tag_by_day_chart = [];
$count_ticket = 0;

while($table_data && ($row = db_fetch_array($table_data))) {
    $count_ticket++;

    if (isset($tags[ $row['tag_id'] ]) && $count_ticket <= 10) {
        $tag_overview[ $tags[ $row['tag_id'] ] ] = $row['total'];
    } else {
        if (!isset($tag_overview[ 'Other' ]))
            $tag_overview[ 'Other' ] = 0;

        $tag_overview[ 'Other' ] += $row['total'];
    }

    $total_tags += $row['total'];
}

foreach ($tag_overview as $_key => $_value) {
    $tag_percent[$_key] = round($_value * 100 / $total_tags);
}

?>
<td>
    <h2>Tags Overview</h2>
    <p>Unit: percent %</p>
    <canvas id="tags" width="300" height="300"></canvas>
</td>
<td>
    <h2>Ticket Destination</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Thị trường</th>
            <th>Số lượng</th>
        </tr>
        </thead>
        <?php foreach($tag_overview as $_name => $_quantity): ?>
            <tr>
                <td>
                    <a target="_blank"
                       href="<?php echo trim($cfg->getUrl(), '/') ?>/scp/tickets.php?a=search&query=<?php
                       echo urlencode('[').$_name.urlencode(']')
                       ?>&basic_search=Search" class="mytag_class no-pjax"><?php echo $_name ?></a>
                </td>
                <td><?php echo $_quantity ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</td>
<script>
    var i = 0;
    var tags_chart_elm = document.getElementById("tags").getContext('2d');
    var tags_chart = new Chart(tags_chart_elm, {
        type: 'pie',
        data: {
            labels: $.parseJSON('<?php echo json_encode(array_keys($tag_percent)) ?>'),
            datasets: [
                {
                    backgroundColor: poolColors(<?php echo count($tag_percent) ?>),
                    data: $.parseJSON('<?php echo json_encode(array_values($tag_percent)) ?>')
                }
            ]
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
            cutoutPercentage: 30
        }
    });
</script>

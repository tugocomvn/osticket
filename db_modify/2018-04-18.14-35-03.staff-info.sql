ALTER TABLE ost_staff ADD relative VARCHAR(128) NULL;
ALTER TABLE ost_staff ADD relative_mobile VARCHAR(24) NULL;
ALTER TABLE ost_staff ADD address VARCHAR(255) NULL;
ALTER TABLE ost_staff ADD skype VARCHAR(64) NULL;
<?php
global $cfg;
?>
<h2>Settlement <small>List</small></h2>
<div class="clearfix">
    <p class="pull-right"><a class="btn_sm btn-xs btn-primary" href="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php?action=new">add item</a></p>
</div>
<form action="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php" method="get">
    <table>
        <tr>
            <td><label for="">Name</label></td>
            <td><input type="text" name="name" class="input-field" value="<?php if (isset($_REQUEST['name'])) echo trim($_REQUEST['name']); ?>">
            <td><label for="">Description</label></td>
            <td><input type="text" name="description" class="input-field" value="<?php if (isset($_REQUEST['description'])) echo trim($_REQUEST['description']); ?>"></td>
            <td><button class="btn_sm btn-primary">Search</button></td>
        </tr>

    </table>
</form>

<p>
    <?php echo $showing ?>
</p>
<table class="list" width="1058" border="0" cellspacing="0" cellpadding="2">
<thead>
<tr>
    <th>#</th>
    <th>Name</th>
    <th>Default quantity</th>
    <th>Default value</th>
    <th>Description</th>
    <th>Views</th>
    <th>Action</th>
</tr>
</thead>
<tbody>
<?php $i = $offset + 1; ?>
    <?php while($list && ($row = db_fetch_array($list))): ?>
    <tr>
        <td><?php echo $i++; ?></td>
        <td><?php echo $row['name'] ?></td>
        <td><?php echo $row['default_quantity'] ?></td>
        <td><?php echo number_format($row['default_value'], 2, '.', ',') ?></td>
        <td><?php echo $row['description'] ?></td>
        <td><?php echo $row['views'] ?></td>
        <td>
            <a class="btn_sm btn-default btn-xs" href="<?php echo $cfg->getBaseUrl() ?>/scp/settlement.php?action=edit&id=<?php echo $row['id'] ?>">Edit</a>
        </td>
    </tr>
    <?php endwhile; ?>
</tbody>
</table>
<?php
if ($total) //Show options..
    echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
?>
<!-- Sidebar user panel (optional) -->
<div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
        <img src="<?php echo $cfg->getUrl() ?>/staff_avatar/<?php echo $thisstaff->getAvatar() ?>" class="img-circle elevation-2" alt="<?php echo $thisstaff->getFirstName() ?>">
    </div>
    <div class="info">
        <a href="#" class="d-block">Hello <?php echo $thisstaff->getFirstName() ?></a>
    </div>
</div>
<?php
if(!defined('OSTSCPINC') || !$thisstaff ) die('Access Denied');

?>
<style>
    iframe {
        width: 90%;
        height: 850px;
        margin: auto;
        border: none;
    }

    #container {
        width: 99%;
    }
    #content {
        text-align: center;
    }
</style>
<h2>Demographics</h2>
<div>
    <iframe src="https://datastudio.google.com/embed/reporting/a320f94e-66a1-442c-b745-14dc11a4fc8b/page/CFA7" frameborder="0"></iframe>
</div>

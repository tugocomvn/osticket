-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2019 at 04:24 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `tour_destination`
--

CREATE TABLE `tour_destination` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ENABLE=1, DISABLE=0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `land_rate` decimal(12,0) DEFAULT NULL,
  `flight_rate` decimal(12,0) DEFAULT NULL,
  `land_date` mediumint(3) DEFAULT NULL,
  `flight_date` mediumint(3) DEFAULT NULL,
  `tour_market_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_destination`
--

INSERT INTO `tour_destination` (`id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `land_rate`, `flight_rate`, `land_date`, `flight_date`, `tour_market_id`) VALUES
(51, 'Hàn Quốc (cũ)', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
(52, 'Đà Lạt', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(58, 'Đài Loan', 1, NULL, NULL, NULL, NULL, '170', '154', 30, -10, 948),
(59, 'Úc Sydney', 1, NULL, NULL, NULL, NULL, '289', '525', 30, -10, 946),
(60, 'Thái Lan', 1, NULL, NULL, NULL, NULL, '60', '136', 30, -10, 945),
(61, 'Campuchia', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 949),
(62, 'Phú Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(63, 'Đà Nẵng', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(64, 'Nam Du', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(65, 'Bình Ba', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(66, 'Phòng khách sạn', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(67, 'Vé Máy Bay', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(68, 'Hong Kong', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(69, 'Sing Malay Indo', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(70, 'Hong Kong - Trung Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 951),
(71, 'Sing Malay', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(72, 'Vũng Tàu', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(73, 'Miền Tây', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(74, 'Singapore', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 950),
(75, 'Sổ tiết kiệm', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(76, 'Khác (nhớ ghi vào note)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(125, 'Nhật Bản (cũ)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 944),
(126, 'Châu Âu 5 nước', 1, NULL, NULL, NULL, NULL, '789', '896', 30, -10, 940),
(127, 'Mỹ', 1, NULL, NULL, NULL, NULL, '476', '555', 30, -10, 942),
(128, 'Côn Đảo', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(129, 'Trung Quốc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 951),
(130, 'Bali', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(131, 'Nha Trang', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(132, 'Dubai', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(133, 'Sapa', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(134, 'Điệp Sơn', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(135, 'Phú Yên', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(139, 'Canada', 1, NULL, NULL, NULL, NULL, '752', '614', 30, -10, 939),
(150, 'Pháp Mono', 1, NULL, NULL, NULL, NULL, '498', '523', 30, -10, 940),
(167, 'Nhật Bản Osaka VNA', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 944),
(168, 'Nhật Bản Narita', 1, NULL, NULL, NULL, NULL, '425', '362', 30, -10, 944),
(169, 'Nhật Bản Nagoya', 1, NULL, NULL, NULL, NULL, '425', '415', 30, -10, 944),
(170, 'Hàn Quốc VJ', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
(171, 'Hàn Quốc VNA', 1, NULL, NULL, NULL, NULL, '90', '332', 90, -10, 941),
(172, 'Hàn Quốc Busan (VNA)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
(173, 'Myanmar', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(176, 'Nhật Bản HN Nagoya 4N', 1, NULL, NULL, NULL, NULL, '425', '415', 30, -10, 944),
(211, 'Hàn Quốc CA', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
(215, 'Hàn Quốc Busan (CA)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
(226, 'Hàn Quốc T\'way', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
(234, 'Thái Lan Phuket', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 945),
(242, 'Nhật Bản (SQ)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 944),
(258, 'Úc Melbourne', 1, NULL, NULL, NULL, NULL, '276', '525', 30, -10, 946),
(259, 'Châu Âu 7 nước', 1, NULL, NULL, NULL, NULL, '1073', '554', 30, -10, 940),
(260, 'Châu Âu 5 nước luxury', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 940),
(313, 'Mỹ (Free & Easy)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 942),
(314, 'Canada (Free & Easy)', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 939),
(342, 'Anh Quốc (UK-VNA)', 1, NULL, NULL, NULL, NULL, '630', '765', 30, -10, 938),
(343, 'Mỹ-Canada', 1, NULL, NULL, NULL, NULL, '1085', '752', 30, -10, 942),
(488, 'Nhật Bản Fukuoka', 1, NULL, NULL, NULL, NULL, '425', '397', 30, -10, 944),
(510, 'Bà Lụa-Trà Sư', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(511, 'Châu Âu 4 nước', 1, NULL, NULL, NULL, NULL, '945', '492', 30, -10, 940),
(554, 'Maldives', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(569, 'Cần Giờ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 947),
(599, 'Hàn Quốc JJ', 1, NULL, NULL, NULL, NULL, '70', '345', 90, -10, 941),
(653, 'Nhật Bản HN Nagoya 5N', 1, NULL, NULL, NULL, NULL, '630', '362', 30, -10, 944),
(654, 'Nhật Bản HN Osaka 5N', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 944),
(655, 'Nhật Bản Narita 3N2D', 1, NULL, NULL, NULL, NULL, '425', '397', 30, -10, 944),
(729, 'Châu Âu 3 nước', 1, NULL, NULL, NULL, NULL, '945', '492', 30, -10, 940),
(873, 'New Zealand-Úc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 946),
(882, 'Hàn Quốc-Nhật Bản', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 941),
(961, 'Visa Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1014, 'Nhật Bản Nikko', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 0),
(1030, 'Úc VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1031, 'Mỹ VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1032, 'Canada VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1033, 'Châu Âu VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1039, 'Hàn Quốc VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1061, 'Úc Sydney-Mel', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1066, 'Anh Quốc (UK-CA)', 1, NULL, NULL, NULL, NULL, '664', '572', 30, -10, 938),
(1067, 'Nhật Bản Osaka VJ', 1, NULL, NULL, NULL, NULL, '634', '362', 30, -10, 0),
(1068, 'Nhật Bản Narita VJ', 1, NULL, NULL, NULL, NULL, '429', '397', 30, -10, 0),
(1069, 'Anh Quốc (UK) VISA Lẻ', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1080, 'Thẻ Premium', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0),
(1081, 'Miền Bắc', 1, NULL, NULL, NULL, NULL, '0', '0', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tour_destination`
--
ALTER TABLE `tour_destination`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `index3` (`status`),
  ADD KEY `index4` (`land_rate`),
  ADD KEY `index5` (`flight_rate`),
  ADD KEY `index6` (`land_date`),
  ADD KEY `index7` (`flight_date`),
  ADD KEY `index8` (`tour_market_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tour_destination`
--
ALTER TABLE `tour_destination`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1082;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

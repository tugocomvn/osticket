<?php
/*********************************************************************
    logs.php

    System Logs

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
include_once INCLUDE_DIR.'booking.status.php';
require_once INCLUDE_DIR.'class.tour-destination.php';

$referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/unfinished.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/unfinished.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    global $thisstaff;

    if (isset($_REQUEST['action'])) {
        $errors = [];
        switch ($_REQUEST['action']) {
            case 'update':
                if (!$thisstaff->canChangeBookingStatus()) {
                    die ('Access denied');
                }

                if (!isset($_REQUEST['booking_id']) || !intval($_REQUEST['booking_id'])) {
                    $errors['err'] = 'Booking is required';
                    break;
                }
                $booking_id = intval($_REQUEST['booking_id']);

                if (!isset($_REQUEST['status_id']) || !intval($_REQUEST['status_id'])) {
                    $errors['err'] = 'Status is required';
                    break;
                }
                $status_id = intval($_REQUEST['status_id']);

                $bs = \Booking\Status::lookup([
                    'booking_ticket_id' => $booking_id
                ]);

                if (!$bs) {
                    $bs = \Booking\Status::create([
                        'booking_ticket_id' => $booking_id,
                        'booking_status_id' => $status_id,
                    ]);
                } else {
                    $bs->set('booking_status_id', $status_id);
                }
                $bs->save(true);

                $bsl = \Booking\StatusLog::create([
                    'booking_ticket_id' => $booking_id,
                    'booking_status_id' => $status_id,
                    'updated_at' => new \SqlFunction('NOW'),
                    'updated_by' => $thisstaff->getId(),
                ]);
                $bsl->save(true);

                FlashMsg::set('ticket_create', 'Updated successfully');
                header('Location: '.$referer);
                exit;

            default:
                break;
        }
    }
}
$booking_type_list = TourDestination::loadAllDestination();
$page='unfinished.inc.php';

if (isset($_REQUEST['action']) && $_REQUEST['action']) {
    switch (trim($_REQUEST['action'])) {
        case "log":
            $page = 'unfinished.log.inc.php';
            break;
        default:
            break;
    }
}

$nav->setTabActive('booking', 'unfinished_booking');
$ost->addExtraHeader('<meta name="tip-namespace" content="booking.unfinished_booking" />',
    "$('#content').data('tipNamespace', 'booking.unfinished_booking');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>

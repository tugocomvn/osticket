<?php

class Bootstrap {

    static function init() {
        #Disable Globals if enabled....before loading config info
        if(ini_get('register_globals')) {
           ini_set('register_globals',0);
           foreach($_REQUEST as $key=>$val)
               if(isset($$key))
                   unset($$key);
        }

        #Disable url fopen && url include
        ini_set('allow_url_fopen', 0);
        ini_set('allow_url_include', 0);

        #Disable session ids on url.
        ini_set('session.use_trans_sid', 0);
        #No cache
        session_cache_limiter('nocache');

        #Error reporting...Good idea to ENABLE error reporting to a file. i.e display_errors should be set to false
        $error_reporting = E_ALL & ~E_NOTICE;

        if (defined('E_STRICT')) # 5.4.0
            $error_reporting &= ~E_STRICT;
        if (defined('E_DEPRECATED')) # 5.3.0
            $error_reporting &= ~(E_DEPRECATED | E_USER_DEPRECATED);
        error_reporting($error_reporting); //Respect whatever is set in php.ini (sysadmin knows better??)

        #Don't display errors
        ini_set('display_errors', '0'); // Set by installer
        ini_set('display_startup_errors', '0'); // Set by installer

        if(function_exists('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
        } else { //Default when all fails. PHP < 5.
            ini_set('date.timezone', 'Asia/Ho_Chi_Minh');
        }

        if (!isset($_SERVER['REMOTE_ADDR']))
            $_SERVER['REMOTE_ADDR'] = '';
    }

    function https() {
       return
            (isset($_SERVER['HTTPS'])
                && strtolower($_SERVER['HTTPS']) == 'on')
            || (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])
                && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https');
    }

    function defineTables($prefix) {
        #Tables being used sytem wide
        define('SYSLOG_TABLE',$prefix.'syslog');
        define('ALL_ACTION_TABLE',$prefix.'all_action');
        define('SESSION_TABLE',$prefix.'session');
        define('CONFIG_TABLE',$prefix.'config');

        define('CANNED_TABLE',$prefix.'canned_response');
        define('PAGE_TABLE', $prefix.'content');
        define('FILE_TABLE',$prefix.'file');
        define('FILE_CHUNK_TABLE',$prefix.'file_chunk');

        define('ATTACHMENT_TABLE',$prefix.'attachment');
        define('USER_TABLE',$prefix.'user');
        define('USER_EMAIL_TABLE',$prefix.'user_email');
        define('USER_ACCOUNT_TABLE',$prefix.'user_account');

        define('ORGANIZATION_TABLE', $prefix.'organization');
        define('NOTE_TABLE', $prefix.'note');
        define('USER_POINT_TABLE', 'api_user_point_history');
        define('USER_RANK_TABLE', 'api_user_rank');
        define('TOUR_TABLE', $prefix.'tour');
        define('PAX_TABLE', $prefix.'pax');
        define('PAX_INFO_TABLE', $prefix.'pax_info');
        define('PAX_TOUR_TABLE', $prefix.'pax_tour');
        define('NEWS_TABLE', 'api_news');
        define('NOTIFICATION_TABLE', 'api_cloud_message');
        define('BOOKING_NOTE_TABLE', 'booking_note');
        define('BOOKING_STATUS_TABLE', 'booking_status');
        define('BOOKING_STATUS_LOG_TABLE', 'booking_status_log');
        define('BOOKING_STATUS_LIST_TABLE', 'booking_status_list');
        define('BOOKING_RESERVATION_TABLE', 'booking_reservation');
        define('BOOKING_RESERVATION_IMPORT_LOG_TABLE', 'booking_reservation_import_log');
        define('BOOKING_RESERVATION_HISTORY_TABLE', 'booking_reservation_history');
        define('BOOKING_LOG_TABLE', 'booking_tmp_log');
        define('BOOKING_DOCUMENT_TABLE', 'booking_document');

        define('STAFF_TABLE',$prefix.'staff');
        define('STAFF_OL_TABLE','offlate_tmp');
        define('STAFF_OL_VIEW','offlate_view');
        define('CHECKIN_TABLE',$prefix.'checkin');
        define('TEAM_TABLE',$prefix.'team');
        define('TEAM_MEMBER_TABLE',$prefix.'team_member');
        define('DEPT_TABLE',$prefix.'department');
        define('GROUP_TABLE',$prefix.'groups');
        define('GROUP_DEPT_TABLE', $prefix.'group_dept_access');
        define('GROUP_DES_TABLE',$prefix.'group_destination_access');

        define('FAQ_TABLE',$prefix.'faq');
        define('FAQ_TOPIC_TABLE',$prefix.'faq_topic');
        define('FAQ_CATEGORY_TABLE',$prefix.'faq_category');

        define('CALL_LOG_TABLE',$prefix.'call_log');
        define('SMS_LOG_TABLE',$prefix.'sms_log');

        define('DRAFT_TABLE',$prefix.'draft');
        define('TICKET_TABLE',$prefix.'ticket');
        define('TICKET_USER_TABLE',$prefix.'ticket_user');
        define('TICKET_CALENDAR_TABLE',$prefix.'ticket_calendar');
        define('TICKET_THREAD_TABLE',$prefix.'ticket_thread');
        define('TICKET_ATTACHMENT_TABLE',$prefix.'ticket_attachment');
        define('TICKET_LOCK_TABLE',$prefix.'ticket_lock');
        define('TICKET_EVENT_TABLE',$prefix.'ticket_event');
        define('TICKET_EMAIL_INFO_TABLE',$prefix.'ticket_email_info');
        define('TICKET_COLLABORATOR_TABLE', $prefix.'ticket_collaborator');
        define('TICKET_STATUS_TABLE', $prefix.'ticket_status');
        define('TICKET_PRIORITY_TABLE',$prefix.'ticket_priority');
        define('TICKET_PAYMENT_HISTORY_TABLE',$prefix.'ticket_payment_history');
        define('TICKET_PAYMENT','payment_tmp');
        define('BOOKING_TABLE','booking_tmp');

        define('PRIORITY_TABLE',TICKET_PRIORITY_TABLE);
        define('UUID_TABLE',$prefix.'uuid');
        define('UUID_TMP_TABLE',$prefix.'uuid_tmp');

        define('TAG_TABLE',$prefix.'tag');
        define('TICKET_TAG_TABLE',$prefix.'ticket_tag');

        define('FORM_SEC_TABLE',$prefix.'form');
        define('FORM_FIELD_TABLE',$prefix.'form_field');

        define('LIST_TABLE',$prefix.'list');
        define('LIST_ITEM_TABLE',$prefix.'list_items');

        define('FORM_ENTRY_TABLE',$prefix.'form_entry');
        define('FORM_ANSWER_TABLE',$prefix.'form_entry_values');

        define('TOPIC_TABLE',$prefix.'help_topic');
        define('SLA_TABLE', $prefix.'sla');

        define('EMAIL_TABLE',$prefix.'email');
        define('EMAIL_TEMPLATE_GRP_TABLE',$prefix.'email_template_group');
        define('EMAIL_TEMPLATE_TABLE',$prefix.'email_template');

        define('FILTER_TABLE', $prefix.'filter');
        define('FILTER_RULE_TABLE', $prefix.'filter_rule');

        define('AUTO_FILTER_TABLE', $prefix.'auto_filter');
        define('AUTO_FILTER_RULE_TABLE', $prefix.'auto_filter_rule');

        define('PLUGIN_TABLE', $prefix.'plugin');
        define('SEQUENCE_TABLE', $prefix.'sequence');

        define('API_KEY_TABLE',$prefix.'api_key');
        define('TIMEZONE_TABLE',$prefix.'timezone');
        define('INDEX_PHONE_TABLE',$prefix.'phone_number_index');
        define('INDEX_EMAIL_TABLE',$prefix.'email_index');
        define('TRACKING_TABLE',$prefix.'user_tracking');

        define('ACC_SPEND_ITEM','account_spend_item');
        define('ACC_SPEND_DETAIL','account_expected_detail');
        define('ACC_SPEND_ACTUAL_DETAIL','account_actual_detail');
        define('ACC_GENERAL','account_general');
        define('ACC_VIEW','account_view');

        define('LOGACTION_TABLE','log_action');

        define('COMMISSION_REPORT_TABLE','commission_checking');
        define('COMMISSION_BOOKING_TABLE','commission_checking_booking');

        //define table about VISA
		define('PAPER_REQUIRE_TABLE','paper_require');
        define('STATUS_JOB_TABLE','status_job');
        define('STATUS_MARITAL_TABLE','status_marital');
        define('STATUS_VISA_TABLE','status_visa');
        define('VISA_TYPE_TABLE','visa_type');
        define('PASSPORT_LOCATION_TABLE','passport_location');
        define('VISA_HISTORY_TABLE','visa_history');
        define('VISA_INFORMATION_TABLE','visa_information');
        define('VISA_TOUR_PAX_PROFILE','tour_pax_visa_profile');
        define('VISA_APPOINTMENT','visa_appointment');
        define('VISA_PROFILE_APPOINTMENT','visa_profile_appointment');
        define('VISA_FOLLOW_ACTION_TABLE','visa_follow_action');
        define('STATUS_FINANCE_TABLE','status_finance');

        define('OST_PAX_TABLE','ost_pax');
        define('OST_PAX_TOUR_TABLE','ost_pax_tour');

        // receipt
        define('RECEIPT_TABLE','receipt');
        define('RECEIPT_LOG_TABLE','receipt_log');
        define('RECEIPT_TRACKING_TABLE','receipt_tracking');
        define('RECEIPT_CONFIRMATION_CODE_TABLE','receipt_confirmation_code');
        define('RECEIPT_PRINT_TABLE', 'receipt_print');

        define('OST_TICKET_COUNT','ost_ticket_count');

        define('OST_TOUR_HISTORY','ost_tour_history');

        define('VISA_PROFILE_SMS_LOG_TABLE','visa_profile_sms_log');

        //define demographic
        define('OST_TOUR_TABLE','ost_tour');
        define('DEMOGRAPHIC_AGE_GROUP_TABLE','demographic_age_group');
        define('DEMOGRAPHIC_AGE_TABLE','demographic_age');
        define('DEMOGRAPHIC_GENDER_TABLE','demographic_gender');

        // BI
        define('BOOKING_COUNT_TABLE','booking_count');
        define('BOOKING_FOLLOW_TABLE','booking_follow');
        define('BOOKING_FOLLOW_ACTION_TABLE','booking_follow_action');
        define('BOOKING_RETURN_TABLE','booking_return');
        define('TICKET_FOLLOW_TABLE','ticket_follow');
        define('TICKET_FOLLOW_ACTION_TABLE','ticket_follow_action');
        define('TICKET_INFO_TABLE','ticket_info');
        define('PHONE_NUMBER_LIST_TABLE','phone_number_list');
        define('EMAIL_LIST_TABLE','email_list');
        define('TICKET_ANALYTICS_TABLE','ticket_analytics');
        define('BOOKING_ANALYTICS_TABLE','booking_analytics');
        define('GROWTH_COMMISSION_TABLE','growth_commission');
        define('GROWTH_COMMISSION_DETAIL_TABLE','growth_commission_detail');
        define('TRACKING_COOKIE_URLPARAM_EVENT_TABLE','tracking_cookie_urlparam_event');
        define('TRACKING_URLPARAM_TABLE','tracking_urlparam');
        define('TRACKING_COOKIE_URLPARAM_TABLE','tracking_cookie_urlparam');
        define('TRACKING_COOKIE_TABLE','tracking_cookie');
        define('SOURCE_LIST_TABLE','source_list');
        define('OBJECT_EVENT_TABLE','object_event');
        define('AUTO_ACTION_CONTENT_TABLE','auto_action_content');
        define('AUTO_ACTION_CONTENT_LOG_TABLE','auto_action_content_update_log');
        define('AUTO_ACTION_CAMPAIGN_TABLE','auto_action_content_campaign');
        define('AUTO_ACTION_WAITING_TABLE','auto_action_waiting');

        //address table
        define('CITY_TABLE','city_address');
        define('DISTRICT_TABLE','district_address');
        define('WARD_TABLE','ward_address');

        //define OTP
        define('OTP_EXPIRED',0.5); //OTP code expired hour
        define('OTP_VERIFY','verify');

        //define SMS
        define('SMS_APPOINTMENT_START', 'Tugo kinh gui den quy khach ');

        define('TOUR_PAX_VISA_PROFILE_HISTORY_TABLE', 'tour_pax_visa_profile_history');
        define('VISA_HISTORY_ACTION_LOG_TABLE', 'visa_history_action_log');
        define('VISA_PROFILE_APPOINTMENT_ACTION_LOG_TABLE', 'visa_profile_appointment_action_log');
        define('VISA_INFORMATION_ACTION_LOG_TABLE', 'visa_information_action_log');
        define('OST_PAX_TOUR_HISTORY_TABLE', 'ost_pax_tour_history');
        define('ANALYSIS_CALL_TABLE', 'tugo_osticket_call');
        define('ANALYSIS_SMS_TABLE', 'tugo_osticket_sms');
        define('ANALYSIS_AGE_TABLE', 'tugo_osticket_customer_age');
        define('ANALYSIS_PAX_TABLE', 'tugo_osticket_customer');
        define('ANALYSIS_BOOKING_TABLE', 'tugo_osticket_booking');
        define('ANALYSIS_TICKET_TABLE', 'tugo_osticket_ticket');
        define('ANALYSIS_VISA_TABLE', 'tugo_osticket_visa');
        define('STAFF_SIGNIN_TOKEN_TABLE', 'staff_signin_token');

        define('FORMAT_RECEIPT','TGF######');
        define('PASSPORT_PHOTO_TABLE','pax_passport_photo');
        define('PASSPORT_PHOTO_UPLOAD_TABLE','passport_photo_upload');

        //Tour New
        define('TOUR_NEW_TABLE','tour');
        define('TOUR_LOG_TABLE','tour_log');
        define('TOUR_EXPORT_LOG_TABLE', 'tour_export_log');
        define('TOUR_NET_PRICE_TABLE','tour_net_price');
        //flight ticket : Operator
        define('FLIGHT_TABLE','flight');
        define('FLIGHT_LOG_TABLE','flight_log');
        define('FLIGHT_TICKET_TABLE','flight_ticket');
        define('FLIGHT_TICKET_FLIGHT_OP_TABLE','flight_ticket_flight');
        define('FLIGHT_TICKET_LOG_TABLE','flight_ticket_log');
        define('FLIGHT_TICKET_TOUR_TABLE','flight_ticket_tour');
        define('PRICE_RULE_TABLE','price_rule');
        define('TOUR_PRICE_RULE_TABLE','tour_price_rule');   
        //flight ticket : Ticket Room
        $prefix_flight_ticket = 'fnd_';
        define('FLIGHT_TICKET_TR_TABLE', $prefix_flight_ticket.'flight_ticket');
        define('FLIGHT_TICKET_TR_LOG_TABLE', $prefix_flight_ticket.'flight_ticket_log');
        define('FLIGHT_TICKET_FLIGHT_TR_TABLE', $prefix_flight_ticket.'flight_ticket_flight');
        define('FLIGHT_TR_TABLE', $prefix_flight_ticket.'flight');
        define('FLIGHT_TR_LOG_TABLE', $prefix_flight_ticket.'flight_log');
        
        //tour destination
        define('TOUR_DESTINATION_TABLE','tour_destination');
        define('TOUR_MARKET_TABLE','tour_market');
    }

    function loadConfig() {
        #load config info
        $configfile='';
        if(file_exists(INCLUDE_DIR.'ost-config.php')) //NEW config file v 1.6 stable ++
            $configfile=INCLUDE_DIR.'ost-config.php';
        elseif(file_exists(ROOT_DIR.'ostconfig.php')) //Old installs prior to v 1.6 RC5
            $configfile=ROOT_DIR.'ostconfig.php';
        elseif(file_exists(INCLUDE_DIR.'settings.php')) { //OLD config file.. v 1.6 RC5
            $configfile=INCLUDE_DIR.'settings.php';
            //Die gracefully on upgraded v1.6 RC5 installation - otherwise script dies with confusing message.
            if(!strcasecmp(basename($_SERVER['SCRIPT_NAME']), 'settings.php'))
                Http::response(500,
                    'Please rename config file include/settings.php to include/ost-config.php to continue!');
        } elseif(file_exists(ROOT_DIR.'setup/'))
            Http::redirect(ROOT_PATH.'setup/');

        if(!$configfile || !file_exists($configfile))
            Http::response(500,'<b>Error loading settings. Contact admin.</b>');

        require($configfile);
        define('CONFIG_FILE',$configfile); //used in admin.php to check perm.

        # This is to support old installations. with no secret salt.
        if (!defined('SECRET_SALT'))
            define('SECRET_SALT',md5(TABLE_PREFIX.ADMIN_EMAIL));
        #Session related
        define('SESSION_SECRET', MD5(SECRET_SALT)); //Not that useful anymore...
        define('SESSION_TTL', 86400); // Default 24 hours
    }

    function connect() {
        #Connect to the DB && get configuration from database
        $ferror=null;
        $options = array();
        if (defined('DBSSLCA'))
            $options['ssl'] = array(
                'ca' => DBSSLCA,
                'cert' => DBSSLCERT,
                'key' => DBSSLKEY
            );

        if (!db_connect(DBHOST, DBUSER, DBPASS, $options)) {
            $ferror=sprintf('Unable to connect to the database — %s',db_connect_error());
        }elseif(!db_select_database(DBNAME)) {
            $ferror=sprintf('Unknown or invalid database: %s',DBNAME);
        }

        if($ferror) //Fatal error
            self::croak($ferror);
    }

    function loadCode() {
        #include required files
        require_once INCLUDE_DIR . 'class.util.php';
        require(INCLUDE_DIR . 'class.signal.php');
        require(INCLUDE_DIR . 'class.user.php');
        require(INCLUDE_DIR . 'class.auth.php');
        require(INCLUDE_DIR . 'class.pagenate.php'); //Pagenate helper!
        require(INCLUDE_DIR . 'class.log.php');
        require(INCLUDE_DIR . 'class.crypto.php');
        require(INCLUDE_DIR . 'class.timezone.php');
        require_once(INCLUDE_DIR . 'class.signal.php');
        require(INCLUDE_DIR . 'class.nav.php');
        require(INCLUDE_DIR . 'class.page.php');
        require_once(INCLUDE_DIR . 'class.format.php'); //format helpers
        require_once(INCLUDE_DIR . 'class.validator.php'); //Class to help with basic form input validation...please help improve it.
        require(INCLUDE_DIR . 'class.mailer.php');
        require_once INCLUDE_DIR . 'mysqli.php';
        require_once INCLUDE_DIR . 'class.i18n.php';
        require_once INCLUDE_DIR . 'class.search.php';
        require_once(INCLUDE_DIR . 'class.trigger.php');
        require_once(INCLUDE_DIR . 'class.string.php');
    }

    function i18n_prep() {
        ini_set('default_charset', 'utf-8');
        ini_set('output_encoding', 'utf-8');

        // MPDF requires mbstring functions
        if (!extension_loaded('mbstring')) {
            if (function_exists('iconv')) {
                function mb_strpos($a, $b) { return iconv_strpos($a, $b); }
                function mb_strlen($str) { return iconv_strlen($str); }
                function mb_substr($a, $b, $c=null) {
                    return iconv_substr($a, $b, $c); }
                function mb_convert_encoding($str, $to, $from='utf-8') {
                    return iconv($from, $to, $str); }
            }
            else {
                function mb_strpos($a, $b) {
                    $c = preg_replace('/^(\X*)'.preg_quote($b).'.*$/us', '$1', $a);
                    return ($c===$a) ? false : mb_strlen($c);
                }
                function mb_strlen($str) {
                    $a = array();
                    return preg_match_all('/\X/u', $str, $a);
                }
                function mb_substr($a, $b, $c=null) {
                    return preg_replace(
                        "/^\X{{$b}}(\X".($c ? "{{$c}}" : "*").").*/us",'$1',$a);
                }
                function mb_convert_encoding($str, $to, $from='utf-8') {
                    if (strcasecmp($to, $from) == 0)
                        return $str;
                    elseif (in_array(strtolower($to), array(
                            'us-ascii','latin-1','iso-8859-1'))
                            && function_exists('utf8_encode'))
                        return utf8_encode($str);
                    else
                        return $str;
                }
            }
            define('LATIN1_UC_CHARS', 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ');
            define('LATIN1_LC_CHARS', 'àáâãäåæçèéêëìíîïðñòóôõöøùúûüý');
            function mb_strtoupper($str) {
                if (is_array($str)) $str = $str[0];
                return strtoupper(strtr($str, LATIN1_LC_CHARS, LATIN1_UC_CHARS));
            }
            function mb_strtolower($str) {
                if (is_array($str)) $str = $str[0];
                return strtolower(strtr($str, LATIN1_UC_CHARS, LATIN1_LC_CHARS));
            }
            define('MB_CASE_LOWER', 1);
            define('MB_CASE_UPPER', 2);
            define('MB_CASE_TITLE', 3);
            function mb_convert_case($str, $mode) {
                // XXX: Techincally the calls to strto...() will fail if the
                //      char is not a single-byte char
                switch ($mode) {
                case MB_CASE_LOWER:
                    return preg_replace_callback('/\p{Lu}+/u', 'mb_strtolower', $str);
                case MB_CASE_UPPER:
                    return preg_replace_callback('/\p{Ll}+/u', 'mb_strtoupper', $str);
                case MB_CASE_TITLE:
                    return preg_replace_callback('/\b\p{Ll}/u', 'mb_strtoupper', $str);
                }
            }
            function mb_internal_encoding($encoding) { return 'UTF-8'; }
            function mb_regex_encoding($encoding) { return 'UTF-8'; }
            function mb_substr_count($haystack, $needle) {
                $matches = array();
                return preg_match_all('`'.preg_quote($needle).'`u', $haystack,
                    $matches);
            }
        }
        else {
            // Use UTF-8 for all multi-byte string encoding
            mb_internal_encoding('utf-8');
        }
        if (extension_loaded('iconv'))
            iconv_set_encoding('internal_encoding', 'UTF-8');
    }

    function croak($message) {
        $msg = $message."\n\n".THISPAGE;
        Mailer::sendmail(ADMIN_EMAIL, 'osTicket Fatal Error', $msg,
            sprintf('"osTicket Alerts"<%s>', ADMIN_EMAIL));
        //Display generic error to the user
        Http::response(500, "<b>Fatal Error:</b> Contact system administrator.");
    }
}

#Get real path for root dir ---linux and windows
$here = dirname(__FILE__);
$here = ($h = realpath($here)) ? $h : $here;
define('ROOT_DIR',str_replace('\\', '/', $here.'/'));
unset($here); unset($h);

define('INCLUDE_DIR', ROOT_DIR . 'include/'); // Set by installer
define('PEAR_DIR',INCLUDE_DIR.'pear/');
define('SETUP_DIR',ROOT_DIR.'setup/');

define('UPGRADE_DIR', INCLUDE_DIR.'upgrader/');
define('I18N_DIR', INCLUDE_DIR.'i18n/');

/*############## Do NOT monkey with anything else beyond this point UNLESS you really know what you are doing ##############*/

#Current version && schema signature (Changes from version to version)
define('THIS_VERSION', 'v1.9.12'); // Set by installer
define('GIT_VERSION', '19292ad'); // Set by installer
define('MAJOR_VERSION', '1.9');
//Path separator
if(!defined('PATH_SEPARATOR')){
    if(strpos($_ENV['OS'],'Win')!==false || !strcasecmp(substr(PHP_OS, 0, 3),'WIN'))
        define('PATH_SEPARATOR', ';' ); //Windows
    else
        define('PATH_SEPARATOR',':'); //Linux
}

//Set include paths. Overwrite the default paths.
ini_set('include_path', './'.PATH_SEPARATOR.INCLUDE_DIR.PATH_SEPARATOR.PEAR_DIR);

require(INCLUDE_DIR.'class.osticket.php');
require(INCLUDE_DIR.'class.allaction.php');
require(INCLUDE_DIR.'class.misc.php');
require(INCLUDE_DIR.'class.http.php');

// Determine the path in the URI used as the base of the osTicket
// installation
if (!defined('ROOT_PATH') && ($rp = osTicket::get_root_path(dirname(__file__))))
    define('ROOT_PATH', rtrim($rp, '/').'/');

Bootstrap::init();

#CURRENT EXECUTING SCRIPT.
define('THISPAGE', Misc::currentURL());

define('DEFAULT_MAX_FILE_UPLOADS',ini_get('max_file_uploads')?ini_get('max_file_uploads'):5);
define('DEFAULT_PRIORITY_ID',1);

#Global override
if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    // Take the left-most item for X-Forwarded-For
    $_SERVER['REMOTE_ADDR'] = trim(array_pop(
        explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])));

function tugo_get_ip_address() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        $ip=$_SERVER['HTTP_CLIENT_IP'];

    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

    else
        $ip=$_SERVER['REMOTE_ADDR'];

    if (filter_var($ip, FILTER_VALIDATE_IP))
        return $ip;

    return '';
}

<?php
global $cfg;
$ticket = $info['ticket'];
$allow = ['bookingcode', 'note', 'retailprice'];

if (!$info['title'])
    $info['title'] = 'Change Tickets Status';

?>
<h3><?php echo $info['title']; ?></h3>
<b><a class="close" href="#"><i class="icon-remove-circle"></i></a></b>
<div class="clear"></div>
<hr/>
<?php
if ($info['error']) {
    echo sprintf('<p id="msg_error">%s</p>', $info['error']);
} elseif ($info['warn']) {
    echo sprintf('<p id="msg_warning">%s</p>', $info['warn']);
} elseif ($info['msg']) {
    echo sprintf('<p id="msg_notice">%s</p>', $info['msg']);
} elseif ($info['notice']) {
    echo sprintf('<p id="msg_info"><i class="icon-info-sign"></i> %s</p>',
        $info['notice']);
}


$action = $info['action'] ?: ('#tickets/status/'. $state);
?>
<div id="ticket-status" style="display:block; margin:5px;">
    <form method="post" name="status" id="status"
          action="<?php echo $action; ?>">
        <table width="100%">
            <?php
            if ($info['extra']) {
                ?>
                <tbody>
                <tr><td colspan="2"><strong><?php echo $info['extra'];
                            ?></strong></td> </tr>
                </tbody>
                <?php
            }

            $verb = '';
            if ($state) {
                $statuses = TicketStatusList::getStatuses(array('states'=>array($state)))->all();
                $verb = TicketStateField::getVerb($state);
            }

            if ($statuses) {
                ?>
                <tbody>
                <tr>
                    <td colspan=2>
                        <span>
                        <?php
                        if (count($statuses) > 1) { ?>
                            <strong><?php echo __('Status') ?>:&nbsp;</strong>
                            <select name="status_id" class="input-field">
                            <?php
                            foreach ($statuses as $s) {
                                echo sprintf('<option value="%d" %s>%s</option>',
                                    $s->getId(),
                                    ($info['status_id'] == $s->getId())
                                        ? 'selected="selected"' : '',
                                    $s->getName()
                                );
                            }
                            ?>
                            </select>
                            <font class="error">*&nbsp;<?php echo $errors['status_id']; ?></font>
                            <?php
                        } elseif ($statuses[0]) {
                            echo  "<input type='hidden' name='status_id' value={$statuses[0]->getId()} />";
                        } ?>
                        </span>
                    </td>
                </tr>
                </tbody>
                <?php
            } ?>
            <tbody>
            <?php if (!isset($forms) || !$forms) $forms=DynamicFormEntry::forTicket($ticket->getId()); ?>
            <?php if ($forms)
                foreach ($forms as $form) {
                    $form->render(true, false, array('mode'=>'edit', 'width'=>160, 'entry'=>$form, 'allow' => $allow));
                } ?>
            </tbody>
        </table>
        <hr>
        <input type="hidden" name="change_to_success" value="1">
        <input type="hidden" name="note" value="" id="payment_note">
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="button" class="btn_sm btn-default close" name="cancel"
                       value="<?php echo __('Cancel'); ?>">
            </span>
            <span class="buttons pull-right">
                <input type="submit" class="btn_sm btn-primary" value="<?php
                echo __('Save'); ?>">
                <input type="submit" class="btn_sm btn-success" value="<?php
                echo __('Fill later'); ?>">
            </span>
        </p>
    </form>
</div>
<div class="clear"></div>
<script type="text/javascript">
    $(function() {
        // Copy checked tickets to status form.
        $('form#tickets input[name="tids[]"]:checkbox:checked')
            .each(function() {
                $('<input>')
                    .prop('type', 'hidden')
                    .attr('name', 'tids[]')
                    .val($(this).val())
                    .appendTo('form#status');
            });

        if ($('#money_explain') && $('#money')) {
            $('#money').off('input').on('input', function() {
                $('#money_explain').text((parseInt($('#money').val())).formatMoney(0, '.', ','));
            })
        }
    });
</script>

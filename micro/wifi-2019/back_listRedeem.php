<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/Pagination.php';

    $page = isset($_REQUEST['page']) ?intval($_REQUEST['page']): 1;

    $total = Awing::getTotalRedeem();

    $config = [
        'code'        => "",
        'total'       => $total,
        'limit'       => PAGE_LIMIT,
        'full'        => false,
        'querystring' => 'page'
    ];
    $offset = ($page - 1) * PAGE_LIMIT;
    $results = Awing::getListRedeem($offset);

require_once(__DIR__ . "/listRedeem.php");
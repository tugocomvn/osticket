/*branch: reservation*/
create table booking_reservation_import_log
(
    id int unsigned auto_increment,
    tour_id int null,
    content longtext null,
    created_at datetime null,
    created_by int null,
    constraint table_name_pk
        primary key (id)
);

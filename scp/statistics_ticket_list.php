<?php
/*************************************************************************
    statistics_ticket_list.php

    liet ke nhung ticket cua agent trong ngay cu the

    Minh Bao <beckerbao29@gmail.com>    
**********************************************************************/
require('staff.inc.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');

//Navigation
$nav->setTabActive('dashboard');

require_once(STAFFINC_DIR.'header.inc.php');

if(!defined('OSTSCPINC') || !$thisstaff || !@$thisstaff->isStaff()) die('Access Denied');

$to = date("Y-m-d");
$from = date("Y-m-d",strtotime("$to -30 days"));

$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
	echo $errors['err'];
}elseif($startTime==0 || $endTime==0){
	$_REQUEST['startDate'] = date("m/d/Y",strtotime($from));
	$_REQUEST['endDate']  = date("m/d/Y",strtotime($to));
}else{
	$from = date("Y-m-d",$startTime);
	$to = date("Y-m-d",$endTime);
}

$agent_username = $_REQUEST['username'];

$query = "SELECT ost_s.username,ost_t.ticket_id,title,body FROM `ost_ticket_thread` ost_t, `ost_ticket` ost, `ost_staff` ost_s WHERE ost_t.created >= '".$from."' AND ost_t.created < '".$to."' AND ost_t.ticket_id=ost.ticket_id AND ost_t.staff_id=ost_s.staff_id AND ost.topic_id=1 AND ost_s.username='".$agent_username."' AND ost_t.title NOT LIKE '%assigned to%' AND ost_t.body NOT LIKE '%status changed from%' ORDER BY `ost_s`.`username` ASC";

$result = db_query($query);

echo "All ticket of <b>" . $agent_username . "</b> from:" . $from . ", to:". $to . "<br/><br/>";

echo "<table border=1 cellpadding=0 cellspacing=0>";
echo "<tr><td>Ticket ID</td><td>Title</td><td>Body</td></tr>";
while ($row = db_fetch_array($result)) {
	echo "<tr>";
	echo "<td><a href='http://support.tugo.com.vn/scp/tickets.php?id=".$row['ticket_id']."'>" . $row['ticket_id'] . "</a></td>";
	echo "<td>".$row['title']."</td>";
	echo "<td>".$row['body']."</td>";
	echo "</tr>";
}
echo "</table>";
?>
<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canManageVisaItems()) die('Access Denied');
?>
<form action="<?php echo $url ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $mess ?></div>
    <?php endif;?>

    <input type="hidden" name="id" value="<?php  echo $_REQUEST['id']; ?>">
    <input type="hidden" name="action" value="edit">

    <a class="btn_sm btn-default" href="<?php echo $url ?>">Back</a>
    <h2 class="centered"><?php echo $title_edit.$result->name ?></h2>
    <table style="margin-left: 25%; margin-top: 10px">
        <tr>
            <td width="180">Tên<span class="error">*&nbsp;</span></td>
            <td>
                <input  class="input-field" type="text" size="28" name="name" value="<?php echo $result->name ?>">
            </td>
        </tr>
        <tr>
            <td width="180">Mô tả<span class="error">*&nbsp;</span></td>
            <td>
                <textarea class="input-field" style=" resize: none;" name="description"  cols=30 rows="3" ><?php echo $result->description ?></textarea>
            </td>
        </tr>
        <tr>
            <td width="180">Trạng thái</td>
            <td>
                <select name="status">
                    <option value="0" <?php if(!$result->status) echo 'selected' ?>>Hide</option>
                    <option value="1" <?php if($result->status) echo 'selected' ?>>Show</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="180">Nhân viên tạo</td>
            <td>
            <span ><?php echo $StaffName[$result->staff_id_create] ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">Ngày tạo</td>
            <td>
                <span><?php echo date('d/m/Y H:i:s',strtotime($result->date_create)) ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">Nhân viên chỉnh sửa</td>
            <td>
                <span><?php echo $StaffName[$result->staff_id_edit] ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">Ngày chỉnh sửa</td>
            <td>
                <span><?php echo  date('d/m/Y H:i:s',strtotime($result->date_edit)) ?></span>
            </td>
        </tr>
    </table>
    <p class="centered">
        <a class="btn_sm btn-default" href="<?php echo $url ?>">Cancel</a>
        <button class="btn_sm btn-primary" name="edit" value="edit">Save</button>
    </p>
</form>

ALTER TABLE api_device MODIFY device_id VARCHAR(48) NOT NULL COMMENT 'send notification';
ALTER TABLE api_device_log MODIFY device_id VARCHAR(48);
ALTER TABLE api_refresh_token MODIFY device_id VARCHAR(48) NOT NULL;

ALTER TABLE api_user_point_history ADD booking_code VARCHAR(20) NULL;
ALTER TABLE api_user_point_history CHANGE time date DATETIME;
ALTER TABLE api_user_point_history ADD amount INT NULL;
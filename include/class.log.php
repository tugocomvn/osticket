<?php
/*********************************************************************
    class.log.php

    Log

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require_once(INCLUDE_DIR . 'class.orm.php');

class Log {

    var $id;
    var $info;

    function Log($id){
        $this->id=0;
        return $this->load($id);
    }

    function load($id){

        $sql='SELECT * FROM '.SYSLOG_TABLE.' WHERE log_id='.db_input($id);
        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->info=db_fetch_array($res);
        $this->id=$this->info['log_id'];
        
        return $this->id;
    }

    function reload(){
        return $this->load($this->getId());
    }

    function getId(){
        return $this->id;
    }
        
    function getType(){
        return $this->info['log_type'];    
    }

    function getTitle(){
        return $this->info['title'];
    }

    function getText(){
        return $this->info['log'];
    }

    function getIp(){
        return $this->info['ip_address'];
    }

    function getCreateDate(){
        return $this->info['created'];
    }

    function getInfo(){
        return $this->info;
    }

    /*** static function ***/
    function lookup($id){
        return ($id && is_numeric($id) && ($l= new Log($id)) && $l->getId()==$id)?$l:null;
    }
}

class LogActionModel extends VerySimpleModel {
    static $meta = [
        'table' => LOGACTION_TABLE,
        'pk' => ['id'],
        'ordering' => ['created_at'],
    ];
}

class LogAction extends LogActionModel {
    const ACTIONS = [
        'TICKET_CREATE' => 1,
        'TICKET_ASSIGN' => 2,
        'USER_EDIT' => 3,
        'MISSED_CALL' => 4,
        'CUSTOMER_CARE' => 5,
        'HOT_LINE' => 6,
        'TICKET_CLOSE' => 7,
        'TICKET_TRANSFER' => 8,
        'TICKET_HANDOVER' => 9,
        'TICKET_REPLY' => 10,
        'TICKET_NOTE' => 11,
        'TICKET_SMS' => 12,
        'TICKET_UPDATE' => 13,
        'TICKET_CHANGE_STATUS' => 14,
        'TICKET_OWN' => 15,
        'TICKET_SUCCESS' => 16,
        'TICKET_DELETE' => 17,
    ];

    public static function getName($id) {
        return ucwords(strtolower(implode(' ', explode('_', array_search($id, LogAction::ACTIONS)))));
    }

    public static function log($staff_id, $ticket_id, $action_id, $user_id, $thread_id) {
        $data = [
            'staff_id' => $staff_id,
            'action_id' => $action_id,
            'ticket_id' => $ticket_id,
            'thread_id' => $thread_id,
            'user_id' => $user_id,
            'created_at' => new SqlFunction('NOW'),
        ];

        try {
            $log = parent::create($data);
            if($log) $log->save();
        } catch (Exception $ex) {  }
    }

    private static function sql(&$sql, $params) {
        if (isset($params['staff_id']) && $params['staff_id']) {
            $sql .= " AND a.staff_id = ".db_input(trim($params['staff_id']))." ";
        }

        if (isset($params['action_id']) && $params['action_id']) {
            $sql .= " AND action_id = ".db_input(trim($params['action_id']))." ";
        }

        if (isset($params['ticket_id']) && $params['ticket_id']) {
            $sql .= " AND ticket_id = ".db_input(trim($params['ticket_id']))." ";
        }

        if (isset($params['user_id']) && $params['user_id']) {
            $sql .= " AND user_id = ".db_input(trim($params['user_id']))." ";
        }

        if (isset($params['customer_name']) && $params['customer_name']) {
            $sql_ = "SELECT DISTINCT id FROM ost_user WHERE name LIKE ".db_input('%'.trim($params['customer_name']).'%')." ";
            $res_ = db_query($sql_);
            $ids = [];
            if ($res_) {
                while($res_ && ($row = db_fetch_array($res_))) {
                    $ids[] = $row['id'];
                }
            }

            $ids = array_unique(array_filter($ids));
            if ($ids) {
                $sql .= " AND a.user_id IN (".implode(',', $ids).") ";
            } else {
                $sql .= " AND 1=0 ";
            }
        }

        if (isset($params['phone_number']) && $params['phone_number']) {
            $sql_ = "SELECT DISTINCT u.id FROM ost_user u 
                JOIN ost_form_entry e ON e.object_id=u.id AND e.object_type LIKE 'U'
                JOIN ost_form_entry_values v ON v.entry_id=e.id AND v.field_id=".USER_PHONE_NUMBER_FIELD." 
                WHERE v.value LIKE ".db_input(trim($params['phone_number']))." ";
            $res_ = db_query($sql_);
            $ids = [];
            if ($res_) {
                while($res_ && ($row = db_fetch_array($res_))) {
                    $ids[] = $row['id'];
                }
            }

            $ids = array_unique(array_filter($ids));
            if ($ids) {
                $sql .= " AND a.user_id IN (".implode(',', $ids).") ";
            } else {
                $sql .= " AND 1=0 ";
            }
        }

        if (isset($params['from']) && $params['from']) {
            $sql .= " AND DATE(created_at) >= ".db_input(trim($params['from']))." ";
        }

        if (isset($params['to']) && $params['to']) {
            $sql .= " AND DATE(created_at) <= ".db_input(trim($params['to']))." ";
        }
    }

    public static function getPagination(array $params, $offset, &$total, $limit = PAGE_LIMIT) { // TODO: optimize
        $sql = " SELECT a.ticket_id, a.thread_id, a.user_id, a.created_at, a.action_id, a.staff_id
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        $limit = " LIMIT $limit OFFSET $offset ";

        static::sql($sql, $params);

        $res = db_query("$sql ORDER BY created_at DESC $limit");
        $total = db_num_rows(db_query($sql));
        return $res;
    }

    public static function getStaffNumberTicketSummaryTable(array $params) {
        $sql = " SELECT a.staff_id, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY a.staff_id ORDER BY count(a.ticket_id) DESC ");
        return $res;
    }

    public static function getStaffNumberTicketChart(array $params) {
        $sql = " SELECT a.staff_id, date(a.created_at) as date, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY a.staff_id, date(a.created_at) ORDER BY date(a.created_at) ASC ");
        return $res;
    }

    public static function getStaffActivitySummaryTable(array $params) {
        $sql = " SELECT a.staff_id, a.action_id, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY a.staff_id, a.action_id ORDER BY count(a.ticket_id) DESC ");
        return $res;
    }

    public static function getStaffActivityChart(array $params) {
        $sql = " SELECT a.staff_id, date(a.created_at) as date, a.action_id, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY a.staff_id, date(a.created_at), a.action_id ORDER BY date(a.created_at) ASC ");
        return $res;
    }

    public static function getActivitySummaryTable(array $params) {
        $sql = " SELECT a.action_id, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY a.action_id ORDER BY count(a.ticket_id) DESC ");
        return $res;
    }

    public static function getActivityChart(array $params) {
        $sql = " SELECT date(a.created_at) as date, a.action_id, count(a.ticket_id) as total
            FROM log_action a ";

        $sql .= " WHERE 1 ";

        static::sql($sql, $params);

        $res = db_query("$sql GROUP BY date(a.created_at), a.action_id ORDER BY date(a.created_at) ASC ");
        return $res;
    }

}
?>

alter table booking_tmp
	add twin_room tinyint default 0 null;

alter table booking_tmp
	add double_room tinyint default 0 null;

alter table booking_tmp
	add triple_room tinyint default 0 null;

alter table booking_tmp
	add single_room tinyint default 0 null;

alter table booking_tmp
	add other_room varchar(50) null;

alter table booking_tmp
	add referral_code int unsigned null;

create table booking_tmp_log
(
    id int unsigned auto_increment,
    ticket_id int unsigned null,
    staff_id int unsigned null,
    content longtext null,
    time datetime not null,
    constraint booking_tmp_log_pk
        primary key (id)
);

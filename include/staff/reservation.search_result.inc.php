<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canViewBookings()) die('Access Denied');

$reservation_cache = [];
?>
<style>
    td { position: relative; }

    tr.strikeout td:before {
        content: " ";
        position: absolute;
        top: 50%;
        left: 0;
        border-bottom: 1px solid #111;
        width: 100%;
    }

    tr.strikeout td:after {
        content: "\00B7";
        font-size: 1px;
    }

    /* Extra styling */
    td { width: 100px; }
    th { text-align: left; }
</style>
<table>
    <caption>Have <?php echo (int)db_num_rows($search_staff_pax) ?> results</caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Sale</th>
        <th>Hold</th>
        <th>Sure</th>
        <th>Customer / <br>Phone number</th>
        <th>Booking Code</th>
        <th>Create Time</th>
        <th>Due</th>
        <th>Note</th>
        <th>Tour/Country</th>
        <th>#</th>
    </tr>
    </thead>
    <tbody>
    <?php $count_item = 0; $i=1; ?>
    <?php while($search_staff_pax && ($history = db_fetch_array($search_staff_pax))): $count_item++; ?>
        <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue' ?>
        <?php if((int)$history['current_id'] === 0) echo 'strikeout'?>">
            <td><?php echo $i++; ?></td>
            <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
            <td>
                <?php if (isset($history['hold_qty']) && $history['hold_qty']) echo $history['hold_qty']; else echo '-'; ?>
            </td>
            <td>
                <?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; else echo '-'; ?>
            </td>
            <td>
                <?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
                <br>
                <?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?>
            </td>
            <td><?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?></td>
            <td><?php if (isset($history['created_at']) && strtotime($history['created_at'])) echo date('d/m/y H:i', strtotime($history['created_at'])); ?></td>
            <td>
                <?php
                if (isset($history['due_at']) && strtotime($history['due_at']) > 0)
                    echo date('d/m', strtotime($history['due_at']));
                ?>
                <?php if( !(isset($history['hold_qty']) && $history['hold_qty']) && !(isset($history['sure_qty']) && $history['sure_qty']) ): ?>
                    (cancel)
                <?php endif; ?>
            </td>
            <td>
                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                <br>
                <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                    <label class="label label-default label-small" for="">Visa Only</label>
                <?php endif; ?>
                <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                    <label class="label label-default label-small" for="">Security Deposit</label>
                <?php endif; ?>
                <?php if (isset($history['fe']) && $history['fe']): ?>
                    <label class="label label-default label-small" for="">FE</label>
                <?php endif; ?>
                <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                    <label class="label label-default label-small" for="">Visa Ready</label>
                <?php endif; ?>
                <?php if (isset($history['one_way']) && $history['one_way']): ?>
                    <label class="label label-default label-small" for="">One Way</label>
                <?php endif; ?>

                <?php if(isset($history['infant']) && $history['infant']): ?>
                    [ Infant: <?php echo $history['infant'] ?> ]
                <?php endif; ?>
            </td>
            <td>
                <?php
                if (isset($history['tour_id']) && $history['tour_id']) {
                    $tour = TourNew::lookup($history['tour_id']);
                    if ($tour) echo $tour->name;
                } elseif (isset($history['country']) && $history['country']) {
                    $country = DynamicListItem::lookup($history['country']);
                    echo 'Visa '.$country->getValue();
                }

                ?>
            </td>
            <td>
                <?php if(isset($history['current_id']) && (int)$history['current_id'] > 0): ?>
                    <a href="<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id=<?php echo $history['current_id'] ?>"
                       class="btn_sm btn-default btn-xs" data-title="Jump to reservation"><i class="icon-eye-open"></i></a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$count_item): ?>
        <tr><td>No Item</td></tr>
    <?php endif; ?>
    </tbody>
</table>

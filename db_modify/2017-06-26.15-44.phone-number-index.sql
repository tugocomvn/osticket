CREATE TABLE ost_phone_number_index
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  phone_number VARCHAR(16),
  object_id INT,
  object_type VARCHAR(1) COMMENT 'H, O, T, U'
);
CREATE UNIQUE INDEX ost_phone_number_index_phone_number_object_id_object_type_uindex ON ost_phone_number_index (phone_number, object_id, object_type);
CREATE INDEX ost_phone_number_index_phone_number_index ON ost_phone_number_index (phone_number);
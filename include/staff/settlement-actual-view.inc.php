<?php
global $cfg;
?>

<style>
    table textarea {
        width: 95%;
        height: 5em;
        resize: none;
    }

    .spend_list {
        counter-reset: my-badass-counter;
    }

    .spend_list .stt:before {
        content: counter(my-badass-counter) '.';
        counter-increment: my-badass-counter;
        position: absolute;
        padding-left: 0.5em;
        margin-right: 1em;
        font-style: italic;
    }

    .spend_item {
        text-align: center;
    }
</style>
<?php
$visa_pax_count = PaxTour::countPax($_REQUEST['id'], false, true);
?>
<h2>Actual Settlement <small>View</small></h2>
<p><a href="<?php echo $referer ?>" class="btn_sm btn-default">Back</a></p>
<table width="95%">
    <tr>
        <td width="65%">
            <table>
                <tr>
                    <td><label for="">Rate: 1 USD =</label></td>
                    <td colspan="2"><input type="text" name="rate" id="rate" class="input-field" step="1" disabled
                        value="<?php if (isset($settlement) && $settlement && isset($settlement->rate))
                            echo number_format($settlement->rate, 0, '.', ','); ?>"> VND</td>
                </tr>
                <tr>
                    <td><label for="">Payment Income</label></td>
                    <td><input type="text" name="income" disabled class="input-field" id="income"
                               value="<?php
                               $payment_income = PaxTour::calcIncomePayment($_REQUEST['id']);
                               if (isset($settlement->rate))
                                   echo number_format($payment_income / $settlement->rate, 2, '.', ',');
                               else echo '0';
                               ?>"> $</td>

                    <td> = <span id="income_vnd"><?php echo number_format($payment_income, 0, '.', ','); ?></span> VND</td>
                </tr>
                <tr>
                    <td><label for="">Booking Income</label></td>
                    <td>
                        <?php
                        $booking_income = 0;
                        if (isset($settlement->rate)) {
                            $booking_income = PaxTour::calcIncome($_REQUEST['id']);
                            echo number_format($booking_income / $settlement->rate, 2, '.', ',');
                        }
                        else echo '0';
                        ?>
                        = <span id=""><?php echo number_format($booking_income, 0, '.', ','); ?></span> VND
                    </td>
                </tr>
                <tr>
                    <td><label for="">Total Outcome</label></td>
                    <td><input type="text" disabled id="outcome" class="input-field"
                        value="<?php if (isset($settlement) && $settlement && isset($settlement->actual_outcome))
                            echo number_format($settlement->actual_outcome, 2, '.', ','); ?>"> $ </td>

                    <td><span id="outcome_vnd"><?php if (isset($settlement) && $settlement && isset($settlement->actual_outcome) && isset($settlement->rate))
                        echo ' = '.number_format(intval($settlement->actual_outcome * $settlement->rate), 0, '.', ',').' VND'; ?></span></td>
                </tr>
                <tr>
                    <td>Actual Profit</td>
                    <td><input type="text" class="input-field" id="profit"
                               value="<?php echo number_format($settlement->actual_profit, 2, '.', ',') ?>"
                               disabled> $</td>
                    <td><span id="profit_vnd"><?php echo ' = '.number_format($settlement->actual_profit*$settlement->rate, 0, '.', ',').' VND' ?></span></td>
                </tr>
                <tr>
                    <td><label for="">Note</label></td>
                    <td colspan="2">
                        <textarea class="input-field" name="note" disabled><?php
                            if (isset($settlement) && $settlement && isset($settlement->note)) echo trim($settlement->note);
                            ?></textarea>
                    </td>
                </tr>
            </table>
        </td>
        <td width="30%">
            <table>
                <caption>Tour Info</caption>
                <tr>
                    <th>Tour Name</th>
                    <td><?php echo $tour->name ?></td>
                </tr>
                <tr>
                    <th>Destination</th>
                    <td><?php if (isset($destination_list[$tour->destination])) echo $destination_list[$tour->destination] ?></td>
                </tr>
                <tr>
                    <th>Departure Date</th>
                    <td><?php echo $tour->gateway_present_time ?></td>
                </tr>
                <tr>
                    <th>Airline</th>
                    <td><?php if (isset($airlines_list[$tour->airline_id])) echo $airlines_list[$tour->airline_id] ?></td>
                </tr>
                <tr>
                    <th>Quantity</th>
                    <td><?php echo PaxTour::countPax($_REQUEST['id']); ?> + 1 TL</td>
                </tr>
                <tr>
                    <th>Visa Qty</th>
                    <td>
                        <?php foreach($visa_pax_count as $result => $_result): ?>
                            <span class="visa_result"><?php echo VisaResult::getResult( $result ) ?>: <?php echo $_result['total'] ?></span><br>
                        <?php endforeach; ?>
                        <span class="visa_result">+ 1</span>
                    </td>
                </tr>
                <tr>
                    <th>TL</th>
                    <?php
                    $tour_leader_item = DynamicListItem::lookup($tour->tour_leader_id);
                    ?>
                    <td><?php if ($tour_leader_item) echo $tour_leader_item->getValue() ?></td>
                </tr>
            </table>
        </td>
    </tr>

</table>
<hr>
<table width="95%">
    <caption>Spend Items</caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Item</th>
        <th>Quantity</th>
        <th>Value</th>
        <th>Total</th>
        <th>Total in VND</th>
    </tr>
    </thead>
    <tbody class="spend_list">
        <?php foreach($spend_details as $_index => $_item): ?>
            <tr class="spend_item new">
                <td class="stt"></td>
                <td>
                    <select name="item[]" id="" class="input-field item" disabled>
                        <option value=>- Select -</option>
                        <?php foreach($spend_items as $__index => $__item): ?>
                            <option value="<?php echo $__item['id'] ?>"
                                    data-qty="<?php echo $__item['default_quantity'] ?>"
                                    data-value="<?php echo $__item['default_value'] ?>"
                                    <?php if ($__item['id'] == $_item['account_item_id']) echo 'selected' ?>
                            ><?php echo $__item['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>
                    <input type="text" name="quantity[]" disabled class="input-field quantity" value="<?php echo number_format($_item['quantity'], 0, '.', ',') ?>">
                </td>
                <td>
                    <input type="text" name="value[]" id="" disabled class="input-field value" value="<?php echo number_format($_item['value'], 2, '.', ',') ?>">
                </td>
                <td class="total"><?php echo number_format($_item['quantity']*$_item['value'], 2, '.', ',') ?></td>
                <td class="total_vnd"><?php echo number_format($_item['quantity']*$_item['value']*$settlement->rate, 0, '.', ',') ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<hr>
<table width="95%">
    <tr>
        <td>
            <p class="centered">
                <a href="<?php echo $referer ?>" class="btn_sm btn-default">Back</a>
                <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/actual_settlement.php?action=edit&id=<?php echo $tour_id ?>" class="btn_sm btn-primary">Edit</a>
            </p>
        </td>
    </tr>
</table>
<?php include STAFFINC_DIR.'tour-payment-list.inc.php' ?>
<?php include STAFFINC_DIR.'tour-payment-list-group.inc.php' ?>
<table class="list" width="1058" border="0" cellspacing="0" cellpadding="2">
    <caption>Tours this day</caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Departure Date</th>
        <th>Name</th>
        <th>Airline</th>
        <th>Pax Qty</th>
        <th>Rate</th>
        <th>Income</th>
        <th>Outcome</th>
        <th>Profit</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    <?php while($list && ($row = db_fetch_array($list))): if ($row['id'] == $_REQUEST['id']) continue; ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $row['gateway_present_time'] ?></td>
            <td><?php if (isset($destination_list[$row['destination']])) echo $destination_list[$row['destination']] ?></td>
            <td><?php if (isset($airlines_list[$row['airline']])) echo $airlines_list[$row['airline']] ?></td>
            <td><?php echo PaxTour::countPax($row['id']); ?> + 1</td>
            <td><?php if($row['rate']) echo number_format($row['rate'], 0, '.', ',') ?></td>
            <td>
                <?php if($row['actual_income']) echo number_format($row['actual_income'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['actual_income'])
                            echo number_format($row['rate'] * $row['actual_income'], 0, '.', ',') . ' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if($row['actual_outcome']) echo number_format($row['actual_outcome'], 2, '.', ',').' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * $row['actual_outcome'])
                            echo number_format($row['rate'] * $row['actual_outcome'], 0, '.', ',').' VND' ?></i>
                </small>
            </td>
            <td>
                <?php if(floatval($row['actual_income']) - floatval($row['actual_outcome']))
                    echo number_format(floatval($row['actual_income']) - floatval($row['actual_outcome']), 2, ',', '.' ).' $' ?>
                <br>
                <small>
                    <i><?php if($row['rate'] * (floatval($row['actual_income']) - floatval($row['actual_outcome'])))
                            echo number_format($row['rate'] * (floatval($row['actual_income']) - floatval($row['actual_outcome'])), 0, ',', '.' ).' VND' ?></i>
                </small>
            </td>
            <td>
                <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/actual_settlement.php?action=details&id=<?php echo $row['id'] ?>"
                   class="btn_sm btn-xs btn-primary" title="View Details">Details</a>
                <a href="<?php echo $cfg->getBaseUrl(); ?>/scp/actual_settlement.php?action=edit&id=<?php echo $row['id'] ?>" class="btn_sm btn-xs btn-default">Edit</a>
            </td>
        </tr>
    <?php endwhile; ?>
    </tbody>
</table>

<?php
namespace BI;
require_once(INCLUDE_DIR . 'class.orm.php');

class GrowthCommissionModel extends \VerySimpleModel {
    static $meta = [
        'table' => GROWTH_COMMISSION_TABLE,
        'pk' => ['id'],
    ];
}

class GrowthCommission extends GrowthCommissionModel {

}

class GrowthCommissionDetailModel extends \VerySimpleModel {
    static $meta = [
        'table' => GROWTH_COMMISSION_DETAIL_TABLE,
        'pk' => ['id'],
    ];
}

class GrowthCommissionDetail extends GrowthCommissionDetailModel {

}

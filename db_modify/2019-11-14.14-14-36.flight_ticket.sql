CREATE TABLE IF NOT EXISTS `flight_ticket` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_ticket_code` VARCHAR(16) NULL,
  `net_price` DECIMAL(12,0) NULL,
  `retail_price` DECIMAL(12,0) NULL,
  `total_quantity` MEDIUMINT(3) NULL,
  `adult_quantity` MEDIUMINT(3) NULL,
  `children_quantity` MEDIUMINT(3) NULL,
  `infant_quantity` MEDIUMINT(3) NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_ticket_code` ASC),
  INDEX `index3` (`net_price` ASC),
  INDEX `index4` (`retail_price` ASC),
  INDEX `index5` (`total_quantity` ASC),
  INDEX `index6` (`adult_quantity` ASC),
  INDEX `index7` (`children_quantity` ASC),
  INDEX `index8` (`infant_quantity` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `flight_ticket_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_ticket_id` INT NULL,
  `content` LONGTEXT NULL,
  `created_at` DATETIME NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_ticket_id` ASC),
  INDEX `index3` (`created_at` ASC),
  INDEX `index4` (`created_by` ASC))
ENGINE = InnoDB;
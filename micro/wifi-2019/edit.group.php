<?php
require_once __DIR__."/../../vendor/autoload.php";
require_once __DIR__.'/config.php';
require_once  __DIR__.'/functions.php';
session_start();

if(isset($_SESSION['mess'])) {
    $mess = $_SESSION['mess'];
    unset($_SESSION['mess']);
}

if(isset($_SESSION['error'])) {
    $error = $_SESSION['error'];
    unset($_SESSION['error']);
}


if(isset($_REQUEST['id']))
{
    $id = intval($_REQUEST['id']);
    $group = Awing::getGroupById($id);
    if(is_null($group))
    {
        $mess = 'Không tìm thấy nhóm voucher';
        $error = true;
    }
}

if(isset($_REQUEST['date_start'])&& isset($_REQUEST['date_end']) && ($_REQUEST['date_start'] > $_REQUEST['date_end'])){
    $group['date_start'] = $_REQUEST['date_start'];
    $group['date_end'] = $_REQUEST['date_end'];
    $mess = 'Ngày bắt đầu không lớn hơn ngày kết thúc';
    $error = true;
}

if(isset($_POST['edit']) && $_POST['edit'] == 'edit' && !(isset($error) && $error))
{
    if(is_numeric($id)) {
        $data = [
            'name'       => trim($_REQUEST['name']),
            'content'    => trim($_REQUEST['content']),
            'date_start' => $_REQUEST['date_start'],
            'date_end'   => $_REQUEST['date_end']
        ];
        $results = Awing::updateGroup($id, $data);

        if($results > 0)
        {
            $_SESSION['mess'] = 'Cập nhật thành công';
            $_SESSION['error'] = false;
            header('Location:'.$_SERVER['HTTP_REFERER']);
            exit();
        }
    }
}

include_once __DIR__.'/edit.group.inc.php';
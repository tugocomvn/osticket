<?php
/*************************************************************************
tickets.php

Handles all tickets related actions.

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/

require('admin.inc.php');
require_once(INCLUDE_DIR.'class.ticket.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');
require_once(INCLUDE_DIR.'class.export.php');       // For paper sizes

$page='';

//At this stage we know the access status. we can process the post.
if($_POST && !$errors) {
    if (isset($_POST['do']) && ($do_action = strtolower($_POST['do']))) {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        switch ($do_action) {
            case 'import-ticket':
                echo '<link rel="stylesheet" href="./css/scp.css" media="all">';

                $status = Ticket::importFromPost($_FILES['import'] ?: $_POST['pasted']);

                if (is_string($status)) {
                    echo sprintf('<p class="error-banner">%s</p>', $status);
                }
                
                if (isset($status['ticket_error'])
                    && is_array($status['ticket_error'])
                    && count($status['ticket_error'])) { // error
                    echo sprintf('<p class="error-banner">%s</p>', implode('<br />', $status['ticket_error']));
                }

                if (isset($status['file']) && $status['file'] && $status['fail_count']) {
                    echo sprintf(
                        "<p class=\"error-banner\">Failed list (%d): <a href='/scp/download.php?path=import_tickets&name=%s&hash=%s'>click to download</a></p>",
                        $status['fail_count'],
                        $status['file'],
                        hash('sha256', 'import_tickets'.$status['file'].md5($status['file']).$status['file'])
                    );
                }

                if (isset($status['ticket_success'])
                    && is_array($status['ticket_success'])
                    && count($status['ticket_success'])) {
                    $msg = sprintf(
                        __('Successfully imported %1$d %2$s.'),
                        count($status['ticket_success']),
                        _N('ticket', 'tickets', count($status['ticket_success']))
                    );

                    echo sprintf('<p class="notice-banner">%s</p>', $msg);
                    echo "<ol>";
                    foreach ($status['ticket_success'] as $_ticket) {
                        echo sprintf(
                            '<li><a target="_blank" href="/scp/tickets.php?id=%d">%s</a></li>',
                            $_ticket["id"],
                            $_ticket['number']
                        );
                    }
                    echo "</ol>";
                }

                break;

            case 'import-payment':
                echo '<link rel="stylesheet" href="./css/scp.css" media="all">';

                $status = Ticket::importPaymentFromPost($_FILES['import'] ?: $_POST['pasted']);

                if (is_string($status)) {
                    echo sprintf('<p class="error-banner">%s</p>', $status);
                }

                if (isset($status['ticket_error'])
                    && is_array($status['ticket_error'])
                    && count($status['ticket_error'])) { // error
                    echo sprintf('<p class="error-banner">%s</p>', implode('<br />', $status['ticket_error']));
                }

                if (isset($status['file']) && $status['file'] && $status['fail_count']) {
                    echo sprintf(
                        "<p class=\"error-banner\">Failed list (%d): <a href='/scp/download.php?path=import_tickets&name=%s&hash=%s'>click to download</a></p>",
                        $status['fail_count'],
                        $status['file'],
                        hash('sha256', 'import_tickets'.$status['file'].md5($status['file']).$status['file'])
                    );
                }

                if (isset($status['ticket_success'])
                    && is_array($status['ticket_success'])
                    && count($status['ticket_success'])) {
                    $msg = sprintf(
                        __('Successfully imported %1$d %2$s.'),
                        count($status['ticket_success']),
                        _N('ticket', 'tickets', count($status['ticket_success']))
                    );

                    echo sprintf('<p class="notice-banner">%s</p>', $msg);
                    echo "<ol>";
                    foreach ($status['ticket_success'] as $_ticket) {
                        echo sprintf(
                            '<li><a target="_blank" href="/scp/tickets.php?id=%d">%s</a></li>',
                            $_ticket["id"],
                            $_ticket['number']
                        );
                    }
                    echo "</ol>";
                }
                break;

            case 'import-booking':
                echo '<link rel="stylesheet" href="./css/scp.css" media="all">';

                $status = Ticket::importBookingFromPost($_FILES['import'] ?: $_POST['pasted']);

                if (is_string($status)) {
                    echo sprintf('<p class="error-banner">%s</p>', $status);
                }

                if (isset($status['ticket_error'])
                    && is_array($status['ticket_error'])
                    && count($status['ticket_error'])) { // error
                    echo sprintf('<p class="error-banner">%s</p>', implode('<br />', $status['ticket_error']));
                }

                if (isset($status['file']) && $status['file'] && $status['fail_count']) {
                    echo sprintf(
                        "<p class=\"error-banner\">Failed list (%d): <a href='/scp/download.php?path=import_tickets&name=%s&hash=%s'>click to download</a></p>",
                        $status['fail_count'],
                        $status['file'],
                        hash('sha256', 'import_tickets'.$status['file'].md5($status['file']).$status['file'])
                    );
                }

                if (isset($status['ticket_success'])
                    && is_array($status['ticket_success'])
                    && count($status['ticket_success'])) {
                    $msg = sprintf(
                        __('Successfully imported %1$d %2$s.'),
                        count($status['ticket_success']),
                        _N('ticket', 'tickets', count($status['ticket_success']))
                    );

                    echo sprintf('<p class="notice-banner">%s</p>', $msg);
                    echo "<ol>";
                    foreach ($status['ticket_success'] as $_ticket) {
                        echo sprintf(
                            '<li><a target="_blank" href="/scp/tickets.php?id=%d">%s</a></li>',
                            $_ticket["id"],
                            $_ticket['number']
                        );
                    }
                    echo "</ol>";
                }
                break;
            default:
                break;
        }

    }

} else {
//Navigation
    $nav->setTabActive('manage');

    $inc = 'ticket-import.inc.php';

    require_once(STAFFINC_DIR . 'header.inc.php');
    require_once(STAFFINC_DIR . $inc);
    require_once(STAFFINC_DIR . 'footer.inc.php');

}

<?php
require('staff.inc.php');
include_once INCLUDE_DIR.'class.visa.php';
require_once  INCLUDE_DIR.'class.staff.php';

$allStaff = Staff::getAll();
$StaffName = array();
//----------------URL PAGE---------------------
$url =  $cfg->getUrl().'scp/visa_type.php';
$nav->setTabActive('visa');
$nav->setActiveSubMenu(1);

//convert staff[id] = name
while (($row = db_fetch_array($allStaff)) && $allStaff)
{
    $StaffName[$row['staff_id']] = $row['username'];
}

//receive data from form
if (isset($_REQUEST['id']))
    $id = $_REQUEST['id'];

if(isset($_REQUEST['name']))
    $name = $_REQUEST['name'];

if(isset($_REQUEST['description']))
    $description = $_REQUEST['description'];

if(isset($_REQUEST['status']))
    $status = $_REQUEST['status'];

if(isset($_POST['action']) && 'create' === $_POST['action'])
{
    if(!empty($name)) {
        $data = [
            'status' => 1,
            'name' => $name,
            'description' => $description,
            'staff_id_create' => $thisstaff->getId()
        ];
        $news = \Tugo\VisaType::create($data);
        $news->save(true);

        //Thong bao
        $error = false;
        $mess = "Tạo mới thành công ";
        FlashMsg::set('ticket_create', $mess);

        //Chuyen trang
        $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $url) !== false
            ? $url.'?action=viewlist': $url;
        header('Location: '. $referer);
        exit();
    }else{
        $error = true;
        $mess = "Không được trống các trường dữ liệu";
    }
}

if(isset($_POST['action']) && 'edit' === $_POST['action']) {

    $news = \Tugo\VisaType::lookup($id);
    if ($news) {
        //EDIT
        if(!empty($name)) {
            if($name === $news->name && $description === $news->description && $status == $news->status ) //kiem tra khong co thay doi du lieu
            {
                $mess = "Không có thay đổi mới";
                FlashMsg::set('ticket_create', $mess);
            }else {
                $data = [
                    'name' => $name,
                    'description' => $description,
                    'staff_id_edit' => $thisstaff->getId()
                ];
                if (isset($_REQUEST['status'])) {
                    $data['status'] = $status;
                }
                $news->setAll($data);
                $news->save(true);
                $result = \Tugo\VisaType::lookup($_REQUEST['id']);

                //THONG Bao
                $error = false;
                $mess = "Cập nhật thông tin thành công!";
                FlashMsg::set('ticket_create', $mess);

                //GOI LAI TRANG
                $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $url) !== false
                    ? $url. '?action=edit&id=' . $id : $url;
                header('Location: ' . $referer);
                exit();
            }
        }else{
            $error = true;
            $mess = "Không được để trống trường dữ liệu";
        }
    }else{
        $error = true;
        $mess = "Không tồn tại danh mục này";
    }
}

if(isset($_REQUEST['action']))
{
    switch ($_REQUEST['action']){
        case 'create':
            $title_create = "Tạo mới loại Visa";
            $page = 'visa_create.inc.php';
            break;
        case 'edit':
            $title_edit = "Chỉnh sửa loại Visa: ";
            $result = \Tugo\VisaType::lookup($id);
            $page = 'visa_edit.inc.php';
            break;
        case 'viewlist':
            $title_list = "LOẠI VISA";
            $results = \Tugo\VisaType::getAllItem(); //results set
            $page = "visa_list.inc.php";
            break;
    }
}else{
    $title_list = "LOẠI VISA";
    $results = \Tugo\VisaType::getAllItem(); //results set
    $page = "visa_list.inc.php";
}
$nav->setTabActive('visa');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');


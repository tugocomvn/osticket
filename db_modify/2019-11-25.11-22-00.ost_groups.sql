/*branch feature/tour-market*/
alter table ost_groups
    add can_edit_net_price tinyint(1) unsigned default 0 not null;

alter table ost_groups
    add can_edit_retail_price tinyint(1) unsigned default 0 not null;
	
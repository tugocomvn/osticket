alter table tugo_osticket_customer modify amount decimal(12) unsigned null comment 'total money của số điện thoại và booking này';

alter table tugo_osticket_customer
    add phone_number varchar(10) null;

alter table tugo_osticket_customer
    add booking_code varchar(16) null comment 'TG123-123321 (bỏ số 0 đầu)';

alter table tugo_osticket_customer
    add booking_code_trim int null comment '123456';

alter table tugo_osticket_customer
    add quantity mediumint(3) null comment 'tổng số khách của số điện thoại và booking này';

alter table tugo_osticket_customer
    add destination_name varchar(64) null;

alter table tugo_osticket_customer
    add destination_id int null;

alter table tugo_osticket_customer
    add market_id int null;

alter table tugo_osticket_customer
    add market_name varchar(64) null;


update tugo_osticket_customer set week_of_the_year = null where 1;
alter table tugo_osticket_customer modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_customer modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_booking set week_of_the_year = null where 1;
alter table tugo_osticket_booking modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_booking modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_call set week_of_the_year = null where 1;
alter table tugo_osticket_call modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_call modify week_of_year varchar(6) null comment 'YEAR_WEEK';

update tugo_osticket_customer_age set week_of_the_year = null where 1;
alter table tugo_osticket_customer_age modify week_of_the_year varchar(2) null comment 'WEEK';

alter table tugo_osticket_customer_age modify week_of_year varchar(6) null comment 'YEAR_WEEK';


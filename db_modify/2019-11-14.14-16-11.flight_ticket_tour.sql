CREATE TABLE IF NOT EXISTS `flight_ticket_tour` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flight_ticket_id` INT NULL,
  `tour_id` INT NULL,
  `updated_at` DATETIME NULL,
  `updated_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`flight_ticket_id` ASC),
  INDEX `index3` (`tour_id` ASC),
  UNIQUE INDEX `index4` (`flight_ticket_id` ASC, `tour_id` ASC))
ENGINE = InnoDB;
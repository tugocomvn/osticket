<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" minimum-scale="600px">
    <meta charset="utf8">
    <title>Home</title>
</head>
<style type="text/css" media="screen">
    ul{
        list-style: none;
    }
    ul:after{
        clear: both;
    }
    ul li{
        border-radius: 8px;
        background: #dddddd;
        width: 50px;
        height: 50px;
        padding: 10px;
        float: left;
        text-align: center;
        margin-right: 2px;
    }
    a{
        text-decoration: none;
    }
    ul li.active{
        color:white;
        background: blue;
    }
    .active a{
        color:white;
    }
</style>
<body>
<div class="container bg-light rounded" style="width: 80%">
    <?php include_once __DIR__.'/header.php'; ?>
    <h3 class="mt-2 text-center text-info">Danh sách Voucher</h3>
    <form  action="backListVou.php" method="get">
        <div class="input-group mb-3 mt-3 ">
            <input type="text" name="code" class="form-control border border-primary" placeholder="Nhập mã hoặc nhóm code ở đây"
                   aria-label="Recipient's username" aria-describedby="button-addon2" value=<?php if($code != ""){ echo $code; } ?>>
            <div class="input-group-append">
                <button class="btn btn btn-info" type="submit" id="check" name="search" value="search">Tìm kiếm</button>
            </div>
        </div>
    </form>
    <table class="table">

        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Mã voucher</th>
            <th scope="col">Nhóm</th>
            <th scope="col">Bắt đầu</th>
            <th scope="col">Kết thúc</th>
        </tr>
        </thead>
        <tbody>
        <?php if($results): ?>
            <?php foreach($results as $row):?>
                <tr>
                    <td scope="row"><?php echo ++$offset ?></td>
                    <td><?php echo $row['code_voucher'] ?></td>
                    <td><?php echo $row['name'] ?></td>
                    <td><?php echo date('d/m/Y',strtotime($row['date_start'])) ?></td>
                    <td><?php echo date('d/m/Y',strtotime($row['date_end'])) ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <?php $page = new Pagination($config);
    echo $page->getPagination(); ?>
</div>
</body>

</html>
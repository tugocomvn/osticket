<?php

include_once INCLUDE_DIR.'class.call.php';

class CallApiController extends ApiController {
    function getApiKey() {

        if (!$this->apikey && isset($_SERVER['HTTP_X_API_KEY']) && isset($_SERVER['REMOTE_ADDR']))
            $this->apikey = MY_API::lookupByKey($_SERVER['HTTP_X_API_KEY'], null);

        return $this->apikey;
    }

    function getRequestStructure($format, $data=null) {
        if ('json' == $format)
            return json_decode("{\"calluuid\":\"1475628518.11189\",\"direction\":\"inbound\",\"callernumber\":\"0918744794\",\"destinationnumber\":\"0926065678\",\"starttime\":\"19700101T073336\",\"answertime\":\"19700101T073337\",\"endtime\":\"19700101T073437\",\"billduration\":\"60\",\"totalduration\":\"61\",\"disposition\":\"NO ANSWER\"}", true);

        return [];
    }

    function requireApiKey() {
        # Validate the API key -- required to be sent via the X-API-Key
        # header

        if(!($key=$this->getApiKey()))
            return $this->exerr(401, __('Valid API key required'));
        elseif (!$key->isActive())
            return $this->exerr(401, __('API key not found/active'));

        return $key;
    }

    function add() {

        if(!($key=$this->requireApiKey()))
            return $this->exerr(401, __('API key not authorized'));

        $this->create();
    }

    function create() {
        ignore_user_abort(1);//Leave me a lone bro!
        @set_time_limit(0); //useless when safe_mode is on

        $data = $this->getRequest('json');

        if (!is_array($data)) {
            http_response_code(400);
            echo '2';
            exit;
        }

        $flag = true;
        $check = json_decode("{\"calluuid\":\"1475628518.11189\",\"direction\":\"inbound\",\"callernumber\":\"0918744794\",\"destinationnumber\":\"0926065678\",\"starttime\":\"19700101T073336\",\"answertime\":\"19700101T073337\",\"endtime\":\"19700101T073437\",\"billduration\":\"60\",\"totalduration\":\"61\",\"disposition\":\"NO ANSWER\"}");

        foreach ($check as $key => $value) {
            if (!isset($data[$key])) {
                $flag = false;
                break;
            }
        }

        http_response_code($flag ? 201 : 400);

        if (!$flag) {
            echo '0';
            exit;
        }

        $log = [
            'calluuid' => trim($data['calluuid']),
            'direction' => trim($data['direction']),
            'callernumber' => trim($data['callernumber']),
            'destinationnumber' => trim($data['destinationnumber']),
            'starttime' => tdate2date($data['starttime']),
            'answertime' => tdate2date($data['answertime']),
            'endtime' => tdate2date($data['endtime']),
            'billduration' => trim($data['billduration']),
            'totalduration' => trim($data['totalduration']),
            'disposition' => trim($data['disposition']),
            'add_time' => new SqlFunction('NOW'),
        ];

        $call_log = CallLog::create($log);
        if ($call_log)
            $call_log->save();
    }
}

class MY_API extends API {
    function validate($key, $ip) {
        return ($key && self::getIdByKey($key, $ip));
    }
}

function tdate2date($t_date) {
    if ($t_date
        && ($date = date_create_from_format('Ymd\THis', $t_date))
    )
        return $date->format('Y-m-d H:i:s');
    return null;
}

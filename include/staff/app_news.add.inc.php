<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

?>
<style>
    table input {
        width: 90%;
    }

    table textarea {
        width: 90%;
        height: 200px;
        resize: none;
    }

</style>

<div class="pull-left" style="width:700;padding-top:5px;">
    <h2><?php echo __('Add a News'); ?></h2>
    <p><a href="/scp/app_news.php" class="btn_sm btn-default">Back</a></p>
</div>

<div class="clear"></div>

<form action="app_news.php" method="POST" name="lists">
    <?php csrf_token(); ?>
    <input type="hidden" name="a" value="save" >
    <input type="hidden" name="id" value="<?php echo $news_id ?>" >
    <?php
    $referer = isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/scp/app_news.php') !== false
        ? $_SERVER['HTTP_REFERER'] : '/scp/app_news.php';
    ?>
    <input type="hidden" name="referer" value="<?php echo $referer ?>">
    <table width="1058px">
        <tr>
            <td>Title</td>
            <td width="80%"><input class="input-field" required type="text" name="title" value="<?php
                if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['title'])) echo $_REQUEST['title'];
                elseif (isset($news) && $news) echo $news->title
                ?>"></td>
        </tr>
        <tr>
            <td>Content</td>
            <td><textarea class="input-field" name="content" required id="" cols="30" rows="10"><?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['content'])) echo $_REQUEST['content'];
                    elseif (isset($news) && $news) echo $news->content
                ?></textarea></td>
        </tr>
        <tr>
            <td>User UUID</td>
            <td>
                <p><input class="input-field" type="text" name="user_uuid" value="<?php
                    if (isset($errors['save']) && $errors['save'] && isset($_REQUEST['user_uuid'])) echo $_REQUEST['user_uuid'];
                    elseif (isset($news) && $news) echo $news->user_uuid
                    ?>"></p>
                <p>Separate by comma</p>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a href="/scp/app_news.php" class="btn_sm btn-default">Cancel</a> <button class="btn_sm btn-primary">Save</button>
            </td>
        </tr>
    </table>
</form>

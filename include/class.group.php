<?php
/*********************************************************************
    class.group.php

    User Group - Everything about a group!

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

class Group {

    var $id;
    var $ht;

    var $members;
    var $departments;
    var $destinations;

    function Group($id){

        $this->id=0;
        return $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT grp.*,grp.group_name as name, grp.group_enabled as isactive, count(staff.staff_id) as users '
            .'FROM '.GROUP_TABLE.' grp '
            .'LEFT JOIN '.STAFF_TABLE.' staff USING(group_id) '
            .'WHERE grp.group_id='.db_input($id).' GROUP BY grp.group_id ';
        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->ht=db_fetch_array($res);
        $this->id=$this->ht['group_id'];
        $this->members=array();
        $this->departments = array();
        $this->destinations = array();

        return $this->id;
    }

    function reload(){
        return $this->load();
    }

    function getHashtable() {
        return $this->ht;
    }

    function getInfo(){
        return  $this->getHashtable();
    }

    function getId(){
        return $this->id;
    }

    function getName(){
        return $this->ht['name'];
    }

    function getNumUsers(){
        return $this->ht['users'];
    }


    function isEnabled(){
        return ($this->ht['isactive']);
    }

    function isActive(){
        return $this->isEnabled();
    }

    //Get members of the group.
    function getMembers() {

        if(!$this->members && $this->getNumUsers()) {
            $sql='SELECT staff_id FROM '.STAFF_TABLE
                .' WHERE group_id='.db_input($this->getId())
                .' ORDER BY lastname, firstname';
            if(($res=db_query($sql)) && db_num_rows($res)) {
                while(list($id)=db_fetch_row($res))
                    if(($staff=Staff::lookup($id)))
                        $this->members[]= $staff;
            }
        }

        return $this->members;
    }

    //Get departments the group is allowed to access.
    function getDepartments() {

        if(!$this->departments) {
            $sql='SELECT dept_id FROM '.GROUP_DEPT_TABLE
                .' WHERE group_id='.db_input($this->getId());
            if(($res=db_query($sql)) && db_num_rows($res)) {
                while(list($id)=db_fetch_row($res))
                    $this->departments[]= $id;
            }
        }

        return $this->departments;
    }

    function getDestinations(){
        if(!$this->destinations) {
            $sql = 'SELECT destination FROM ' . GROUP_DES_TABLE
                . ' WHERE group_id =' . db_input($this->getId());
            if (($res = db_query($sql)) && db_num_rows($res)) {
                while (list($id) = db_fetch_row($res))
                    $this->destinations[] = $id;
            }
        }
        if (!$this->destinations) $this->destinations = [];
        return $this->destinations;
    }

    function updateDeptAccess($depts) {


        if($depts && is_array($depts)) {
            foreach($depts as $k=>$id) {
                $sql='INSERT IGNORE INTO '.GROUP_DEPT_TABLE
                    .' SET group_id='.db_input($this->getId())
                    .', dept_id='.db_input($id);
                db_query($sql);
            }
        }

        $sql='DELETE FROM '.GROUP_DEPT_TABLE.' WHERE group_id='.db_input($this->getId());
        if($depts && is_array($depts)) // just inserted departments IF any.
            $sql.=' AND dept_id NOT IN('.implode(',', db_input($depts)).')';

        db_query($sql);

        return true;
    }

    function updateTourAccess($des) {


        if($des && is_array($des)) {
            foreach($des as $k=>$id) {
                $sql='INSERT IGNORE INTO '.GROUP_DES_TABLE
                    .' SET group_id='.db_input($this->getId())
                    .', destination='.db_input($id);
                db_query($sql);
            }
        }

        $sql='DELETE FROM '.GROUP_DES_TABLE.' WHERE group_id='.db_input($this->getId());
        if($des && is_array($des) && count(array_filter($des))) // just inserted departments IF any.
            $sql.=' AND destination NOT IN('.implode(',', db_input($des)).')';

        db_query($sql);

        return true;
    }

    function update($vars,&$errors) {

        if(!Group::save($this->getId(),$vars,$errors))
            return false;

        $this->updateDeptAccess($vars['depts']);
        $this->updateTourAccess($vars['des']);
        $this->reload();

        return true;
    }

    function delete() {

        //Can't delete with members
        if($this->getNumUsers())
            return false;

        $res = db_query('DELETE FROM '.GROUP_TABLE.' WHERE group_id='.db_input($this->getId()).' LIMIT 1');
        if(!$res || !db_affected_rows($res))
            return false;

        //Remove dept access entry.
        db_query('DELETE FROM '.GROUP_DEPT_TABLE.' WHERE group_id='.db_input($this->getId()));

        return true;
    }

    /*** Static functions ***/
    function getIdByName($name){
        $sql='SELECT group_id FROM '.GROUP_TABLE.' WHERE group_name='.db_input(trim($name));
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id)=db_fetch_row($res);

        return $id;
    }

    function lookup($id){
        return ($id && is_numeric($id) && ($g= new Group($id)) && $g->getId()==$id)?$g:null;
    }

    function create($vars, &$errors) {
        if(($id=self::save(0,$vars,$errors)) && ($group=self::lookup($id))){
            $group->updateDeptAccess($vars['depts']);
            $group->updateTourAccess($vars['des']);
        }
        return $id;
    }

    function save($id,$vars,&$errors) {
        if($id && $vars['id']!=$id)
            $errors['err']=__('Missing or invalid group ID');

        if(!$vars['name']) {
            $errors['name']=__('Group name required');
        }elseif(strlen($vars['name'])<3) {
            $errors['name']=__('Group name must be at least 3 chars.');
        }elseif(($gid=Group::getIdByName($vars['name'])) && $gid!=$id){
            $errors['name']=__('Group name already exists');
        }

        if($errors) return false;

        $sql=' SET updated=NOW() '
            .', group_name='.db_input(Format::striptags($vars['name']))
            .', group_enabled='.db_input($vars['isactive'])
            .', can_create_tickets='.db_input($vars['can_create_tickets'])
            .', can_delete_tickets='.db_input($vars['can_delete_tickets'])
            .', can_edit_tickets='.db_input($vars['can_edit_tickets'])
            .', can_assign_tickets='.db_input($vars['can_assign_tickets'])
            .', can_transfer_tickets='.db_input($vars['can_transfer_tickets'])
            .', can_close_tickets='.db_input($vars['can_close_tickets'])
            .', can_ban_emails='.db_input($vars['can_ban_emails'])
            .', can_manage_premade='.db_input($vars['can_manage_premade'])
            .', can_manage_faq='.db_input($vars['can_manage_faq'])
            .', can_post_ticket_reply='.db_input($vars['can_post_ticket_reply'])
            .', can_view_staff_stats='.db_input($vars['can_view_staff_stats'])
            .', can_export_tickets='.db_input($vars['can_export_tickets'])
            .', can_view_payment='.db_input($vars['can_view_payment'])
            .', can_view_payment_chart='.db_input($vars['can_view_payment_chart'])
            .', can_create_payment='.db_input($vars['can_create_payment'])
            .', can_edit_payment='.db_input($vars['can_edit_payment'])
            .', can_view_booking='.db_input($vars['can_view_booking'])
            .', can_create_booking='.db_input($vars['can_create_booking'])
            .', can_edit_booking='.db_input($vars['can_edit_booking'])
            .', can_export_booking='.db_input($vars['can_export_booking'])
            .', can_change_booking_status='.db_input($vars['can_change_booking_status'])
            .', can_accept_offlate='.db_input($vars['can_accept_offlate'])
            .', can_view_offlate='.db_input($vars['can_view_offlate'])
            .', can_create_offlate='.db_input($vars['can_create_offlate'])
            .', can_edit_offlate='.db_input($vars['can_edit_offlate'])
            .', can_view_operator_list='.db_input($vars['can_view_operator_list'])
            .', can_edit_operator_list='.db_input($vars['can_edit_operator_list'])
            .', can_create_operator_list_item='.db_input($vars['can_create_operator_list_item'])
            .', op_edit_pax='.db_input($vars['op_edit_pax'])
            .', op_edit_settlement='.db_input($vars['op_edit_settlement'])
            .', op_view_settlement_list='.db_input($vars['op_view_settlement_list'])
            .', op_view_expected_settlement='.db_input($vars['op_view_expected_settlement'])
            .', op_view_actual_settlement='.db_input($vars['op_view_actual_settlement'])
            // new roles
            .', can_view_personal_checkin='.db_input($vars['can_view_personal_checkin'])
            .', can_view_all_checkin='.db_input($vars['can_view_all_checkin'])
            .', can_do_checkin='.db_input($vars['can_do_checkin'])
            .', can_view_ticket_activity='.db_input($vars['can_view_ticket_activity'])
            .', can_view_general_statistics='.db_input($vars['can_view_general_statistics'])
            .', can_view_agent_directory='.db_input($vars['can_view_agent_directory'])
            .', can_view_ticket_analytics='.db_input($vars['can_view_ticket_analytics'])
            .', can_view_booking_returning='.db_input($vars['can_view_booking_returning'])
            .', can_view_agent_statistics='.db_input($vars['can_view_agent_statistics'])
            .', can_view_guest_directory='.db_input($vars['can_view_guest_directory'])
            .', can_edit_guest_info='.db_input($vars['can_edit_guest_info'])
            .', can_view_organizations='.db_input($vars['can_view_organizations'])
            .', can_create_organizations='.db_input($vars['can_create_organizations'])
            .', can_edit_organizations='.db_input($vars['can_edit_organizations'])
            .', can_view_tour_list='.db_input($vars['can_view_tour_list'])
            .', notes='.db_input(Format::sanitize($vars['notes']))
            //visa access
            .', can_manage_visa_items='.db_input($vars['can_manage_visa_items'])
            .', can_manage_visa_pax_documents='.db_input($vars['can_manage_visa_pax_documents'])
            .', can_view_visa_documents='.db_input($vars['can_manage_visa_pax_documents']||$vars['can_view_visa_documents']) //if has mange else has view
            .', can_view_member_list='.db_input($vars['can_view_member_list'])
            .', can_edit_point_loyalty='.db_input($vars['can_edit_point_loyalty'])
            .', can_cancel_tour='.db_input(($vars['can_cancel_tour']))
            .', can_view_retail_price='.db_input(($vars['can_view_retail_price']))
            .', can_view_net_price='.db_input(($vars['can_view_net_price']))
            .', can_hold_reservation='.db_input(($vars['can_hold_reservation']))
            .', can_cancel_member_reservation='.db_input(($vars['can_cancel_member_reservation']))
            .', can_cancel_leader_reservation='.db_input(($vars['can_cancel_leader_reservation']))
            .', can_approve_leader='.db_input(($vars['can_approve_leader']))
            .', can_approve_operator='.db_input(($vars['can_approve_operator']))
            .', can_approve_visa='.db_input(($vars['can_approve_visa']))
            .', can_view_approve_leader='.db_input(($vars['can_view_approve_leader']))
            .', can_view_approve_operator='.db_input(($vars['can_view_approve_operator']))
            .', can_view_approve_visa='.db_input(($vars['can_view_approve_visa']))
            .', can_cancel_super_reservation='.db_input(($vars['can_cancel_super_reservation']))

            .', can_hold_reservation='.db_input(($vars['can_hold_reservation']))
            //receipt access
            .', can_view_receipt='.db_input($vars['can_view_receipt'])
            .', can_edit_receipt='.db_input($vars['can_edit_receipt'])
            .', can_approve_receipt='.db_input($vars['can_approve_receipt'])
            .', can_view_receipt_list='.db_input($vars['can_view_receipt_list'])
            .', can_edit_retail_price='.db_input(($vars['can_edit_retail_price']))
            .', can_edit_net_price='.db_input(($vars['can_edit_net_price']))
            .', can_reject_receipt='.db_input($vars['can_reject_receipt'])
            .', can_manage_all_receipt='.db_input($vars['can_manage_all_receipt'])
            .', can_request_edit_tour='.db_input(($vars['can_request_edit_tour']))
            .', can_approve_edit_tour='.db_input($vars['can_approve_edit_tour'])
            .', can_create_receipt='.db_input($vars['can_create_receipt']);
        if($id) {

            $sql='UPDATE '.GROUP_TABLE.' '.$sql.' WHERE group_id='.db_input($id);
            if(($res=db_query($sql)))
                return true;

            $errors['err']=sprintf(__('Unable to update %s.'), __('this group'))
               .' '.__('Internal error occurred');

        }else{
            $sql='INSERT INTO '.GROUP_TABLE.' '.$sql.',created=NOW()';
            if(($res=db_query($sql)) && ($id=db_insert_id()))
                return $id;

            $errors['err']=sprintf(__('Unable to create %s.'), __('this group'))
               .' '.__('Internal error occurred');
        }

        return false;
    }
}
?>

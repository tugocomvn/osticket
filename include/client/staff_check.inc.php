<!doctype html>
<html  lang="en">
<head>
    <title>Kiểm tra thông tin nhân viên Tugo | tugo.com.vn</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta charset="utf8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>

</head>
<body>
<?php
require_once 'class.staff.php';
if ($_POST) {
    $staff = null;
    $params=[
        'response' => $_REQUEST['g-recaptcha-response'],
        'secret' => '6LfNMiMUAAAAAGGjXOpp1-QUN40a_Ysep0im6mNb',
    ];
    $defaults = array(
        CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => $params,
    );
    $ch = curl_init();
    curl_setopt_array($ch, $defaults);
    $res = curl_exec($ch);
    if ($res) $res = json_decode($res);
    if (true === $res->success) {
        if (isset($_REQUEST['phonenumber']) && $_REQUEST['phonenumber']) {
            $keyword = trim($_REQUEST['phonenumber']);
            if (strpos($keyword, '@') !== false) {
                $id = Staff::findByEmail($keyword);
            } else {
                $id = Staff::findAllByPhone($keyword);
            }

            if ($id)
                $staff = Staff::lookup($id);
        }
    }
}
?>


<style>

    .wrapper h1,
    .wrapper h3,
    .wrapper h4,
    .wrapper p
    {
        text-align: center;
    }
    h1 {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-weight: lighter;
        font-size: 48px;
        color: #660066;
    }
    h3, h4, p, input, button {
        font-family: "Lato", "Helvetica Neue", arial, helvetica, sans-serif;
    }

    h3, h4 {
        font-weight: normal;
    }

    input {
        padding: 0.25em;
        font-size: 20px;
        border-radius: 4px;
        border: 1px solid lightgrey;
    }

    button {
        border: 1px solid #660066;
        padding: 0.25em 0.5em;
        font-size: 20px;
        border-radius: 4px;
        background: #660066;
        color: white;
    }

    button:focus, input:focus {
        outline: none;
        border: 1px solid #606;
    }

    button:hover {
        background: #808;
    }

    button:active {
        background: #505;
    }

    input:hover{
        background-color: #ECECEC;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <h1>Kiểm tra thông tin nhân viên Tugo</h1>
            <form action="/staff-check.php" method="post" class="">
                <?php csrf_token() ?>
                <div class="form-group">
                    <label for="">Điền số điện thoại hoặc email cần kiểm tra:</label>
                    <input class="form-control" type="text" required title="Điền số điện thoại hoặc email cần kiểm tra vào ô này" placeholder="Số điện thoại / email" name="phonenumber" value="<?php if (isset($_REQUEST['phonenumber'])) echo $_REQUEST['phonenumber'] ?>">
                </div>
                <div class="g-recaptcha" data-sitekey="6LfNMiMUAAAAAJRFufTYrOcxkbQyAOQea9ORnsLB"></div>

                <button class="btn">Kiểm Tra</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <?php if ($_POST) : ?>
                <hr>
                <h3>Kết quả</h3>
                <?php if($staff): $avatar = $staff->getAvatar(); ?>
                <p class="alert alert-success">Đây là nhân viên Tugo</p>
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="<?php if (isset($avatar) && !empty($avatar)) echo $cfg->getUrl().'staff_avatar/'.$avatar;
                        else echo '/scp/images/sunny/ui-bg_diagonals-medium_20_d34d17_40x40.png'; ?>" alt="<?php echo $staff->getName() ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $staff->getName() ?></h5>
                            <p class="card-text">
                                Số điện thoại:<br /><?php echo $staff->getMobilePhone() ?>
                                <br />
                                Địa chỉ email:<br /><?php echo $staff->getEmail() ?></p>
                        </div>
                    </div>
                <?php else: ?>
                    <p class="alert alert-danger">Không tìm thấy nhân viên có số điện thoại/email này</p>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <hr>
            <div class="footer">
                <p>Quý khách có thể liên lạc với Tugo qua các kênh:</p>
                <ul>
                    <li>Tổng đài <strong>1900 5555 43</strong></li>
                    <li>Facebook <strong><a
                                href="https://www.facebook.com/tugo.com.vn/">Tugo</a></strong> (<strong><a
                                href="https://www.facebook.com/tugo.com.vn/">fb.com/tugo.com.vn</a></strong>)</li>
                    <li>Email: <strong><a href="mailto:support@tugo.com.vn">support@tugo.com.vn</a></strong></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Access Denied');

$qs = array();
$qwhere =' WHERE 1 ';

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']=__('Entered date span is invalid. Selection ignored.');
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere.=' AND DATE(log.pay_time)>='.db_input(date('Y-m-d', $startTime));
        $qs += array('startDate' => $_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere.=' AND DATE(log.pay_time)<='.db_input(date('Y-m-d', $endTime));
        $qs += array('endDate' => $_REQUEST['endDate']);
    }
}
$sortOptions=array('id'=>'log.id', 'ticket_id'=>'ticket_id'
                   ,'created'=>'log.pay_time', 'poster' => 'poster');
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'id';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'log.pay_time';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$qselect = 'SELECT log.*, ticket.number, user.name as guest, staff.username ';
$qfrom=' FROM '.TICKET_PAYMENT_HISTORY_TABLE.' log ';

$join = ' LEFT JOIN ' . TICKET_TABLE . ' ticket ON ticket.ticket_id = log.ticket_id '
    . ' LEFT JOIN  ' . STAFF_TABLE . ' staff ON staff.staff_id=log.staff_id '
    . ' LEFT JOIN  ' . USER_TABLE . ' user ON user.id=log.user_id ';

$total=db_count("SELECT count(*) $qfrom $qwhere");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
//pagenate
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('sms.php',$qs);
$qs += array('order' => ($order=='DESC' ? 'ASC' : 'DESC'));
$qstr = '&amp;'. Http::build_query($qs);
$query="$qselect $qfrom $join $qwhere ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' '.$title;
else
    $showing=__('No logs found!');
?>

<h2><?php echo __('Payment History');?>
    &nbsp;<i class="help-tip icon-question-sign" href="#system_logs"></i>
</h2>

<div id='filter' >
    <form action="payment.php" method="get">
        <div style="padding-left:2px;">
            <b><?php echo __('Date Span'); ?></b>&nbsp;<i class="help-tip icon-question-sign" href="#date_span"></i>
            <?php echo __('Between'); ?>:
            <input class="dp" id="sd" size=15 name="startDate" value="<?php echo Format::htmlchars($_REQUEST['startDate']); ?>" autocomplete=OFF>
            &nbsp;&nbsp;
            <input class="dp" id="ed" size=15 name="endDate" value="<?php echo Format::htmlchars($_REQUEST['endDate']); ?>" autocomplete=OFF>&nbsp;&nbsp;
            <input type="submit" Value="<?php echo __('Go!');?>" />
        </div>
    </form>
</div>
<form action="payment.php" method="POST" name="logs">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="mass_process" >
    <input type="hidden" id="action" name="a" value="" >
    <table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
        <caption><?php echo $showing; ?></caption>
        <thead>
        <tr>
            <th width="320"><a <?php echo $title_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=title"><?php echo __('Ticket Number');?></a></th>
            <th width="320"><a <?php echo $body_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=body"><?php echo __('Title');?></a></th>
            <th width="100"><a  <?php echo $poster_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=poster"><?php echo __('Guest');?></a></th>
            <th width="200" nowrap><a  <?php echo $date_sort; ?>href="payment.php?<?php echo $qstr; ?>&sort=date"><?php echo __('Staff');?></a></th>
            <th width="120"><a  <?php echo $ip_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=ip"><?php echo __('Amount');?></a></th>
            <th width="120"><a  <?php echo $ip_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=ip"><?php echo __('Payment Type');?></a></th>
            <th width="120"><a  <?php echo $ip_sort; ?> href="payment.php?<?php echo $qstr; ?>&sort=ip"><?php echo __('Time');?></a></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $total=0;
        $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
        if($res && db_num_rows($res)):
            while ($row = db_fetch_array($res)) {
                $sel=false;
                if($ids && in_array($row['log_id'],$ids))
                    $sel=true;
                ?>
                <tr id="<?php echo $row['log_id']; ?>">
                    <td>&nbsp;<a target="_blank" href="/scp/tickets.php?id=<?php echo $row['ticket_id']; ?>#thread-id-<?php echo $row['thread_id']; ?>"><?php echo $row['number'] ?></a></td>
                    <td><?php echo Format::htmlchars($row['note']); ?></td>
                    <td><?php echo $row['guest']; ?></td>
                    <td><?php echo $row['username']; ?></td>
                    <td><?php echo number_format($row['pay_amount']); ?></td>
                    <td><?php echo $row['pay_method']; ?></td>
                    <td>&nbsp;<?php echo Format::db_daydatetime($row['pay_time']); ?></td>
                </tr>
                <?php
            } //end of while.
        endif; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="7">
                <?php if($res && $num){ ?>
                    <?php echo __('Select');?>:&nbsp;
                    <a id="selectAll" href="#ckb"><?php echo __('All');?></a>&nbsp;&nbsp;
                    <a id="selectNone" href="#ckb"><?php echo __('None');?></a>&nbsp;&nbsp;
                    <a id="selectToggle" href="#ckb"><?php echo __('Toggle');?></a>&nbsp;&nbsp;
                <?php }else{
                    echo __('No logs found');
                } ?>
            </td>
        </tr>
        </tfoot>
    </table>
    <?php
    if($res && $num): //Show options..
        echo '<div>&nbsp;'.__('Page').':'.$pageNav->getPageLinks().'&nbsp;</div>';
    endif;
    ?>
</form>

<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');
$countHold = TourNew::countHoldCurrent($tour->id);
if($countHold > 0 && (int)$tour->status_edit_tour !== TourNew::APPROVAL_EDIT_TOUR)
    die('Access denied');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
table.list tbody td, table.list thead th {
    padding-top: 0.75em;
    padding-bottom: 0.75em;
    vertical-align: middle;
}
.input-field{
    padding: 6px 12px;
    border: 1px solid #ccc;
    margin-bottom: 15px;
}
.border-bottom td {
    border-bottom: 1px solid #eee;
    padding-bottom: 1em;
}

.border-bottom+tr td {
    padding-top: 1em;
}

.required {
    color: #FF0000;
}

.form-edit{
    margin: auto;
}
.code {
    text-transform: uppercase; 
    width:120px;
}
.flight_code {
    text-transform: uppercase;
    width:50px;
}
.airport_code {
    text-transform: uppercase; 
    width:30px;
}

.ui-button {
    padding: 0;
}

.ui-selectmenu-icon.ui-icon {
    margin-top: 0.75em;
}
.ui-selectmenu-open {
    z-index: 999;
}
</style>
<p><a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/tour-list.php"?>">Back</a></p>
<h2>Tour - Edit </h2>
<form action="<?php echo $cfg->getUrl()."scp/tour-list.php?id=".$tour->id."&layout=edit"?>" method="POST">
    <?php csrf_token(); ?>
    <input type="hidden" id="tour_id" name="tour_id" value="<?php echo $tour->id ?>">
    <table class="form-edit">
        <tr>
            <td class="label-input">Điểm đến<span class="required">(*)</span></td>
        </tr>
        <tr>
            <td>
                <select name="booking_type" id="booking_type" class="input-field" style="height:30px" required>
                <option value=>- Select -</option>
                    <?php if(is_array($booking_type_list) && $booking_type_list): ?>
                        <?php foreach($booking_type_list as  $item): ?>
                            <option value="<?php echo $item['id'] ?>"
                                    <?php if (isset($results->destination) && $results->destination == $item['id'])
                                        echo 'selected' ?>
                            ><?php echo $item['name'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label-input">Hãng Bay<span class="required">(*)</span></td>
        </tr>
        <tr>
            <td>
                <select required name="airline" id="airline" class="input-field" style="height:30px">
                <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($airlines_list && ($row = db_fetch_array($airlines_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($tour->airline_id == $row['id']?'selected="selected"':''),$row['airline_name']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Điểm quá cảnh</td>
        </tr>
        <tr>
            <td>
                <select name="transit_airport" id="transit_airport" class="input-field" style="height:30px">
                <option value=> BAY THẲNG </option>
                    <?php while ($transits_airport_list && ($row = db_fetch_array($transits_airport_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($tour->transit_airport_id == $row['id']?'selected="selected"':'') ,$row['transit_airport']);
                }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Điểm khởi hành</td>
        </tr>
        <tr>
            <td>
                <select id="gateway_id" class="input-field" name="gateway_id" style="height:30px">
                    <?php while ($departure_flight_list && ($row = db_fetch_array($departure_flight_list))){
                        echo sprintf('<option value="%d" %s>%s</option>',
                        $row['id'],($tour->gateway_id == $row['id']?'selected="selected"':'') ,$row['departure_flight']);
                    }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Ngày và giờ tập trung<span class="required">(*)</span> (có thể ước lượng 3-4 giờ trước giờ bay)</td>
        </tr>
        <tr>
            <td>
                <input type="time" size=15 name="gateway_present_time"
                       value="<?php if (strtotime($tour->gateway_present_time))
                           echo date('H:i', strtotime($tour->gateway_present_time)); ?>" required>
                <input class="dp input-field" type="text" size=15 name="gateway_present_date"
                    value="<?php if(strtotime($tour->gateway_present_time))
                        echo date('d/m/Y', strtotime($tour->gateway_present_time)); ?>" required>
            </td>
        </tr>
        <tr>
            <td>Ngày khởi hành<span class="required">(*)</span></td>
        </tr>
        <tr>
            <td>
                <input type="text" class="dp input-field" size="15" name="departure_date"
                       value="<?php if(strtotime($tour->departure_date))
                           echo date('d/m/Y', strtotime($tour->departure_date)) ?>">
            </td>
        </tr>
        <tr>
            <td>Ngày về<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <input class="dp input-field" type="text" id="return_time" size=15 name="return_date"
                       value="<?php if (strtotime($tour->return_time))
                           echo date('d/m/Y', strtotime($tour->return_time)); ?>" required>
            </td>
        </tr>
        <?php if($thisstaff->canEditRetailPrice() || $thisstaff->canViewRetailPrice()):?>
            <tr>
                <td>Giá bán khách</td>
            </tr>
            <tr>
                <td>
                    <input type="number" class="input-field" min="0" name="retail_price" id="retail_price" value="<?php echo  $results->retail_price ?>"
                        <?php if(!$thisstaff->canEditRetailPrice()) echo 'readonly'?>><span id="retail_price_money"></span>
                    <script>
                        $('#retail_price').off('change, keyup').on('change, keyup', function() {
                            money_format();
                        });
                    </script>
                </td>
            </tr>
        <?php endif;?>
        <?php if ($thisstaff->canEditNetPrice() || $thisstaff->canViewNetPrice()):?>
        <tr>
            <td>Giá net</td>
        </tr>
        <tr>
            <td>
                <div class="repeater-net-price">
                    <div data-repeater-list="group-loyalty-net-price">
                        <?php if (!$tour_net_price->num_rows): ?>
                            <div data-repeater-item>
                                <table>
                                    <tr>
                                        <td>
                                        Số lượng  <=
                                        <input type="text" style="width: 20px;" name="quantity" maxlength="3" class="input-field">
                                        <span>Giá</span>
                                        <input type="text" class="input-field net_price" min="0" name="retail_price" id="first_net_price"><span id="first_net_price_money"></span>
                                        </td>
                                        <script>
                                            $('#first_net_price').off('change, keyup').on('change, keyup', function() {
                                                money_format();
                                            });
                                        </script>
                                    </tr>
                                    <tr>
                                        <td>
                                            Số lượng  <=
                                            <input type="text" style="width: 20px;" name="_quantity" maxlength="3" class="input-field">
                                            <span>Giá</span>
                                            <input type="text" class="input-field net_price" min="0" name="_retail_price" id="second_net_price"><span id="second_net_price_money"></span>
                                        </td>
                                        <script>
                                            $('#second_net_price').off('change, keyup').on('change, keyup', function() {
                                                money_format();
                                            });
                                        </script>
                                    </tr>
                                    <tr>
                                        <td>
                                            Số lượng  <=
                                            <input type="text" style="width: 20px;" name="__quantity" maxlength="3" class="input-field">
                                            <span>Giá</span>
                                            <input type="text" class="input-field net_price" min="0" name="__retail_price" id="third_net_price"><span id="third_net_price_money"></span>
                                        </td>
                                        <script>
                                            $('#third_net_price').off('change, keyup').on('change, keyup', function() {
                                                money_format();
                                            });
                                        </script>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Trường hợp còn lại, Giá</span>
                                            <input type="text" class="input-field net_price" min="0" name="max_retail_price" id="max_net_price"><span id="net_price_money"></span>
                                        </td>
                                        <script>
                                            $('#max_net_price').off('change, keyup').on('change, keyup', function() {
                                                money_format();
                                            });
                                        </script>
                                    </tr>
                                </table>
                            </div>
                        <?php else: ?>
                            <?php while($tour_net_price && ($row = db_fetch_array($tour_net_price))): ?>
                                <?php $max_quantity = TourNetPrice::max_quantity($results->id); ?>
                                <div data-repeater-item>
                                    <table>
                                    <?php if((int)$row['quantity'] < (int)$max_quantity): ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                                Số lượng  <=
                                                <input type="text" style="width: 20px;" maxlength="3" class="input-field"  name="quantity" value ="<?php echo $row['quantity'] ?>">
                                                <span>Giá</span>
                                                <input type="text" class="input-field retail_price" min="0" id="retail_price" name="retail_price" value="<?php echo $row['retail_price'] ?>">
                                            </td>
                                        </tr>
                                    <?php else: ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                                <input type="hidden" name="quantity" value="<?php echo $max_quantity ?>">
                                                <span>Trường hợp còn lại, Giá</span>
                                                <input type="text" class="input-field" min="0" id="retail_price" name="retail_price" value="<?php echo $row['retail_price'] ?>">
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    </table>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </td>
        </tr>
        <?php endif;?>
        <tr>
            <td>Tour kích cầu
                <input type="checkbox" name="stimulus_tour" id="stimulus_tour" value="1" <?php echo $tour->stimulus_tour?'checked="checked"':''; ?>>
            </td>
        </tr>
        <tr>
            <td>Tour leader</td>
        </tr>
        <tr>
            <td>
                <select name="tour_leader" id="tour_leader" class="input-field" style="height:30px">
                  <option value>&mdash; <?php echo __('Select'); ?> &mdash;</option>
                    <?php while ($leaders_list && ($row = db_fetch_array($leaders_list))){
                    echo sprintf('<option value="%d" %s>%s</option>',
                    $row['id'],( $results->tour_leader_id == $row['id']?'selected="selected"':'') ,$row['name_tl']);
                }?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Số khách tối đa<span class="required">(*)</span> </td>
        </tr>
        <tr>
            <td>
                <input style="width :80px;" type="number"
                       name="pax_quantity" class="input-field" min="0"
                       value="<?php echo $tour->pax_quantity ?>" required>
            </td>
        </tr>
        <tr>
            <td>Số lượng leader</td>
        </tr>
        <tr>
            <td>
                <input style="width :80px;" type="number" max="4"
                    class="input-field" min="0" name="tl_quantity" value="<?php if(isset($tour->tl_quantity)) echo $results->tl_quantity; ?>">
            </td>
        </tr>
        <tr>
            <td>Trạng thái</td>
        </tr>
        <tr>
            <td>
                <select style="height: 28px;" class="input-field status" name="status">
                <option value="0">&mdash; <?php echo __('Select'); ?> &mdash;</option>
                <?php foreach(TourStatus::caseTitleName() as $_id => $_name): ?>
                    <option value="<?php echo $_id ?>"
                        <?php if( $tour->status == $_id) echo "selected" ?>
                    ><?php echo $_name ?></option>
                <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Chọn màu</td>
        </tr>
        <tr>
            <td>
                <select name="color" id="color">
                    <option value=>- Select -</option>
                    <?php foreach($list_colors as $key => $value): ?>
                        <option value="<?php echo $key ?>"
                                <?php if($key===$tour->color_code): ?>
                                    selected
                                <?php endif; ?>
                            <?php if (isset($tour->color_code) && $tour->color_code == $key)
                                echo "selected" ?>
                            data-style="background-color: <?php echo $key ?>"
                        >
                            <?php echo $key ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Notes</td>
        </tr>
        <tr>
            <td>
                <textarea rows="5" cols="50" name="note" id="note" 
                placeholder="Nhập nội dung ...."><?php $decode_note = json_decode($tour->note, true); if(is_array($decode_note)){ echo implode(" ",$decode_note); }else{echo trim($tour->note);} ?></textarea>
            </td>
        </tr>
    </table>
    <hr>
    <div>
        <h3>Tour Name:<span class="required">(*)</span> </h3>
        <input class="input-field" type="text" style="width: 500px;" id="tour_name" name="tour_name" value="<?php echo $tour->name ?>" required>
    </div>
    <div class="repeater" data-limit="4">
        <table class="list" border="0" cellspacing="1" cellpadding="2" width="1058">
            <thead>
                <tr>
                    <th style="width:50px;">STT</th>
                    <th style="width:150px;">CODE</th>
                    <th style="width:120px;">NGÀY</th>
                    <th style="width:100px;">SHCB</th>
                    <th style="width:150px;">HÀNH TRÌNH</th>
                    <th colspan="2"> GIỜ BAY (Định dạng 24h)</th>
                    <th>#</th>
                </tr>
            </thead>
            <?php $no = $offset; ?>
                <tbody data-repeater-list="group-loyalty">
                    <?php if (!$flight_ticket->num_rows): ?>
                        <tr data-repeater-item="group-loyalty" class="pax_item" data-limit="5">
                            <td> 
                            </td>
                            <td>
                                <input id="code" type="text" maxlength="15" class="input-field code" name="flight_ticket_code">
                            </td>
                            <td>
                                <input style="width:90px;" type="text" class="dp_tour_h dob new" name="departure_at"
                                    value="">
                            </td>
                            <td>
                                <input type="text" maxlength="6" class="input-field flight_code" name="flight_code"
                                    value="">
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="input-field airport_code" name="airport_code_from"
                                    value="">
                                <span>-</span>
                                <input type="text" maxlength="3" class="input-field airport_code" name="airport_code_to"
                                    value="">
                            </td>
                            <td>
                                <input type="time" name="departure_time"
                                    value="">
                            </td>
                            <td>
                                <input type="time" name="arrival_time"
                                    value="">
                                <input style="width:90px;" class="dp_tour_h dob new" name="arrival_at"
                                    value="">
                            </td>
                            <td>
                                <button class="btn_sm btn-primary btn-xs" type="button"
                                    onclick="deleteRow(this)">Clear</button>
                            </td>
                        </tr>
                    <?php else: ?>
                    <?php while($flight_ticket && ($row = db_fetch_array($flight_ticket))): ?>
                        <tr data-repeater-item="group-loyalty" class="pax_item">
                            <input type="hidden" name="flight_ticket_id" value="<?php echo $row['id_flight_ticket'] ?>">
                            <input type="hidden" name="flight_id" value="<?php echo $row['flight_id'] ?>">
                            <td><?php echo ++$offset; ?></td>
                            <td>
                                <input id="code" type="text" maxlength="15" class="input-field code" name="flight_ticket_code"
                                value="<?php if(!empty($row['flight_ticket_code'])) echo $row['flight_ticket_code']?>">
                            </td>
                            <td>
                                <input style="width:90px;" type="text" class="dp_tour_h dob new" name="departure_at"
                                    value="<?php if(!empty($row['departure_at'])
                                        && $row['departure_at'] !== '0000-00-00 00:00:00')
                                        echo date('d/m/Y',strtotime($row['departure_at'])) ?>">
                            </td>
                            <td>
                                <input type="text" maxlength="6" class="input-field flight_code" name="flight_code"
                                    value="<?php if(!empty($row['flight_code'])) echo $row['flight_code'] ?>">
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="input-field airport_code" name="airport_code_from"
                                    value="<?php if(isset($row['airport_code_from'])) echo $row['airport_code_from'] ?>">
                                <span>-</span>
                                <input type="text" maxlength="3" class="input-field airport_code" name="airport_code_to"
                                    value="<?php if(isset($row['airport_code_to'])) echo $row['airport_code_to'] ?>">
                            </td>
                            <td>
                                <input type="time" name="departure_time"
                                    value="<?php if(!empty($row['departure_at'])
                                        && $row['departure_at'] !== '0000-00-00 00:00:00')
                                            echo date('H:i',strtotime($row['departure_at'])) ?>">
                            </td>
                            <td>
                                <input type="time" name="arrival_time"
                                    value="<?php if(!empty($row['arrival_at'])
                                        && $row['arrival_at'] !== '0000-00-00 00:00:00')
                                            echo date('H:i',strtotime($row['arrival_at'])) ?>">
                                <input style="width:90px;" class="dp_tour_h dob new" name="arrival_at"
                                        value="<?php if(!empty($row['arrival_at'])
                                            && $row['arrival_at'] !== '0000-00-00 00:00:00')
                                            echo date('d/m/Y',strtotime($row['arrival_at'])) ?>">
                            </td>
                            <td>
                                <button class="btn_sm btn-primary btn-xs" type="button"
                                        onclick="deleteRow(this)">Clear</button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </tbody>
                <input data-repeater-create type="button" class="btn_sm btn-xs btn-default" value="&plus; Thêm chặng bay"/>
        </table>
    </div>
    <p class="centered">
        <button class="btn_sm btn-success import" type="submit" name="action" value="save">Confirm & Save</button>
        <a href="<?php echo $cfg->getUrl()."scp/tour-list.php"?>" class="btn_sm btn-danger">Cancel</a>
    </p>
</form>

<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( document ).ready(function() {
        $('.repeater-net-price').repeater({           
        });
        getConfig().then(function(c) { this_config = c; datePicker(); });
        $('.repeater').repeater({
            isFirstItemUndeletable: false,
            show: function () {
            var limitcount = $(this).parents(".repeater").data("limit");
            var itemcount = $(this).parents(".repeater").find("tr[data-repeater-item]").length;
            if (limitcount) {
                if (itemcount <= limitcount) {
                    $(this).slideDown();
                    datePicker();
                } else {
                    $(this).remove();
                }
            } else {
                $(this).slideDown();
            }
            if (itemcount >= limitcount) {
                $(".repeater input[data-repeater-create]").hide("slow");
            }
            },
        });   
        function datePicker() {
            $('.dp_tour_h.new').datepicker({
                changeYear: true,
                yearRange: "2000:2033",
                shortYearCutoff: 50,
                showButtonPanel: true,
                selectOtherMonths: false,
                numberOfMonths: 1,
                buttonImage: './images/cal.png',
                showOn:'both',
                minDate: new Date(<?php echo (time()) ?>),
                dateFormat: $.translate_format(this_config.date_format||'dd/mm/yy')
            }).removeClass('new');
        } 
        money_format();
        var transit_airport = $('#transit_airport').find(":selected").val();

        $.widget( "custom.xselectmenu", $.ui.selectmenu, {
            _renderItem: function( ul, item ) {
                var li = $( "<li>" ),
                    wrapper = $( "<div>", { text: item.label } );

                if ( item.disabled ) {
                    li.addClass( "ui-state-disabled" );
                }

                $( "<span>", {
                    style: item.element.attr( "data-style" ),
                    "class": "ui-icon ui-icon-blank " + item.element.attr( "data-class" )
                })
                    .appendTo( wrapper );

                return li.append( wrapper ).appendTo( ul );
            },
            _renderButtonItem: function( item ) {
                var buttonItem = $( "<span>", {
                    "class": "ui-selectmenu-text"
                })
                this._setText( buttonItem, item.label );

                buttonItem
                    .css( "background-color", item.value )
                    .css("padding", "0.75em")
                    .css('color', 'white');

                return buttonItem;
            }
        });

        $( "#color" ).xselectmenu();
    });
    function deleteRow(btn) {
        var row = btn.parentNode.parentNode;
        row_indexTable = row.rowIndex-1;
        $("input[name='group-loyalty["+row_indexTable+"][flight_ticket_code]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][departure_at]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][flight_code]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][airport_code_from]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][airport_code_to]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][departure_time]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][arrival_time]']").removeAttr('value');
        $("input[name='group-loyalty["+row_indexTable+"][arrival_at]']").removeAttr('value');
    }
    $('.import').off('click').on('click', function(e) {
        if (!confirm('Bạn đã kiểm tra kĩ thông tin chưa ?')) {
            e.preventDefault();
            return false;
        }
    });
    function money_format(){
        first_net_price = parseInt($('#first_net_price').val());
        second_net_price = parseInt($('#second_net_price').val());
        third_net_price = parseInt($('#third_net_price').val());
        max_net_price = parseInt($('#max_net_price').val());
        retail_price = parseInt($('#retail_price').val());
        maximum_discount = parseInt($('#maximum_discount').val());
        $('#first_net_price_money').text(first_net_price.formatMoney(0, '.', ',')+'đ');
        $('#second_net_price_money').text(second_net_price.formatMoney(0, '.', ',')+'đ');
        $('#third_net_price_money').text(third_net_price.formatMoney(0, '.', ',')+'đ');
        $('#net_price_money').text(max_net_price.formatMoney(0, '.', ',')+'đ');
        $('#retail_price_money').text(retail_price.formatMoney(0, '.', ',')+'đ');
        $('#maximum_discount_money').text(maximum_discount.formatMoney(0, '.', ',')+'đ');
    }
</script>

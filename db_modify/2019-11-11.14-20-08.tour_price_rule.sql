CREATE TABLE IF NOT EXISTS `tour_price_rule` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tour_id` INT NOT NULL,
  `price_rule_id` INT NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `updated_by` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique` (`tour_id` ASC, `price_rule_id` ASC),
  INDEX `index3` (`tour_id` ASC),
  INDEX `index4` (`price_rule_id` ASC))
ENGINE = InnoDB;
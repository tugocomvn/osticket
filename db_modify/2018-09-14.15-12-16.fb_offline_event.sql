CREATE TABLE fb_offline_event_upload
(
  id int PRIMARY KEY AUTO_INCREMENT,
  phone_number varchar(12),
  event_time datetime
);
CREATE UNIQUE INDEX fb_offline_event_upload_phone_number_event_time_uindex ON fb_offline_event_upload (phone_number, event_time);
ALTER TABLE fb_offline_event_upload MODIFY event_time varchar(25);
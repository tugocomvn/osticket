<style>
    .pax_list {
        counter-reset: my-badass-counter;
    }

    .pax_list .stt:before {
        content: counter(my-badass-counter);
        counter-increment: my-badass-counter;
        position: absolute;
        background: white;
        padding-left: 1em;
    }

    .tl-info {
        font-size: 13pt;
        color: red;
    }

    iframe {
        border: none;
        width: 100%;
        min-height: 100px;
        height: auto;
        max-height: 300px;
    }
</style>

<h2><?php echo __('Import pax list to tour'); ?> | <small>Tour: <?php echo $tour->name ?></small></h2>
<?php $referer = isset($_SERVER['HTTP_REFERER'])
        && strpos($_SERVER['HTTP_REFERER'], '/scp/tour-list.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/tour-list.php'; ?>

<div>
    <h3>Tour Info:</h3>
    <?php
    $airlines_list = ListUtil::getList(AIRLINE_LIST);
    $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
    $flight_code = FlightOP::searchFlightCodeFromTour((int)$tour->id);
    $departure_flight_code = '';
    $return_flight_code = '';
    if(count($flight_code) === 4){
        $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
        $return_flight_code = $flight_code[2].'-'.$flight_code[3];
    }elseif(count($flight_code) === 2){
        $departure_flight_code = $flight_code[0];
        $return_flight_code = $flight_code[1];
    } ?>
    <br />
    <span>Airline: <?php echo $airlines_list[$tour->airline_id] ?></span>
    <br>
    <span>Departure flight code: <?php echo $departure_flight_code ?> </span>
    <br>
    <span>Return flight code: <?php echo $return_flight_code ?> </span>
    <br>
    <span>Tour leader: <?php if($tour_leader) echo $tour_leader->getValue() ?></span>
</div>

<div>
    <hr>
    <form action="/scp/tour-guest.php?tour=<?php echo $tour_id ?>" target="submitframe" method="post" id="form" enctype="multipart/form-data">
        <input type="hidden" name="tour_id" value="<?php echo $tour_id ?>">
        <input type="hidden" name="action" value="import">
        <?php csrf_token(); ?>
        <input type="hidden" name="referer" value="<?php echo $referer ?>" >

        <input type="file" name="file" class="btn_sm" accept=".xls, .xlsx">
        <input type="hidden" name="type" value="<?php echo $_REQUEST['layout'] ?>">
        <button type="button" class="btn_sm btn-primary" id="submit_btn">Import</button>

    </form>
    <hr>
    <h3>Result</h3>
    <iframe src="" name="submitframe" id="submitframe" frameborder="0">

    </iframe>
</div>

<div>
    <p style="text-align:center;">
        <a class="no-pjax btn_sm btn-default" href="<?php echo $referer ?>">Back</a>
    </p>
</div>

<div>
    <hr>
    <h3>Tour Leader info:</h3>
    <?php
    if (isset($tour_leader)
        && $tour_leader
        && isset($tour_leader->value)
        && is_array(array_keys($tour_leader->value))
        && isset(array_keys($tour_leader->value)[0])
        && ($tour = DynamicListItem::lookup(array_keys($tour_leader->value)[0]))
        && ($conf = $tour->getConfigurationForm())) {
        foreach ($conf->getFields() as $f) {
            ?>
            <div class="field-label tl-info">
                <label for="<?php echo $f->getWidget()->name; ?>"
                       style="vertical-align:top;padding-top:0.2em"> <?php echo Format::htmlchars($f->get('label')); ?>:</label>
                <?php $f->render('text'); ?>
            </div>
            <?php
        }
    } else {
        ?><strong><em>Chưa chọn TL</em></strong><?php
    } ?>
</div>

<script>
    function submit_form(e) {
        if (!$('#form')[0].checkValidity()) {
            $('#submitButton').click();
            return false;
        }

        $('#form').submit();
    }

    $(document).ready(function() {
        $('#submit_btn').off('click').on('click', submit_form);
    })
</script>

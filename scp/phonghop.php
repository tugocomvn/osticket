<?php
/*********************************************************************
dashboard.php

Staff's Dashboard - basic stats...etc.

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');
$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.phonghop" />',
                     "$('#content').data('tipNamespace', 'dashboard.phonghop');");
require(STAFFINC_DIR.'header.inc.php');
?>

<style>
    iframe {
        width: 100%;
    }
</style>

<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23B39DDB&amp;ctz=Asia%2FHo_Chi_Minh&amp;src=c2NtcDB0OTkybHBlYWdncHQ1Y3RhZGthaXNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%2381910B&amp;title=Ph%C3%B2ng%20H%E1%BB%8Dp%20Tugo%20118&amp;showNav=1&amp;showPrint=0&amp;showTz=0" style="border-width:0" width="1080" height="600" frameborder="0" scrolling="no"></iframe>

<?php
include(STAFFINC_DIR.'footer.inc.php');
?>

<table class="list" width="1058" border="0" cellspacing="0" cellpadding="2">
    <caption>Payments for this tour - GROUP BY BOOKING CODE</caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Booking Code</th>
        <th>Quantity</th>
        <th>Amount</th>
        <th>Amount per Pax</th>
    </tr>
    </thead>
    <tbody>
    <?php $res = PaxTour::getPaymentsGroup($_REQUEST['id']) ?>
    <?php $i=1; $count = 0; $total = 0; ?>
    <?php while($res && ($row = db_fetch_array($res))): ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <?php $total = PaxTour::CountBookingCodeInDifferentTour($row['booking_code']) ?>
            <?php if(isset($total) && $total > 1): ?>
                <td><a style="color : red" href="<?php echo $cfg->getBaseUrl() ?>/scp/io.php?booking_code=<?php echo $row['booking_code'] ?>" target="_blank" class="no-pjax"><?php echo BOOKING_CODE_PREFIX.$row['booking_code'] ?></a></td>
            <?php else : ?>
                <td><a href="<?php echo $cfg->getBaseUrl() ?>/scp/io.php?booking_code=<?php echo $row['booking_code'] ?>" target="_blank" class="no-pjax"><?php echo BOOKING_CODE_PREFIX.$row['booking_code'] ?></a></td>
            <?php endif; ?>
            <td><?php echo number_format($row['quantity'], 0, '.', ','); $count += $row['quantity']; ?></td>
            <td><?php echo ($row['topic_id'] == CHI_TOPIC ? '-' : '').number_format($row['amount'], 0, '.', ',');
                if ($row['topic_id'] == CHI_TOPIC)
                    $total -= $row['amount'];
                else
                    $total += $row['amount'];
                ?></td>
            <td><?php echo number_format($row['amount'] / $row['quantity'], 0, '.', ',')  ?></td>
        </tr>
    <?php endwhile; ?>
    <tr>
        <th colspan="2">Total</th>
        <th><?php echo number_format($count, 0, '.', ','); ?></th>
        <th><?php echo number_format($total, 0, '.', ','); ?></th>
        <th></th>
    </tr>
    </tbody>
</table>
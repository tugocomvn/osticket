
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Create group</title>
</head>
<body>

<div class="container bg-light rounded" style="width: 80%;">
    <?php include_once __DIR__.'/header.php'; ?>
    <div class="form-group">
        <form action="back_create_group.php" method="post">
            <h4 class="my-3 text-center">Tạo nhóm voucher</h4>

            <div class="row my-3">
            <h5 class="col-3">Tên</h5>
            <input type="text" placeholder="" class="form-control-plaintext col-9 border border-info rounded"
                   name="name" required="true" value="<?php if(isset($_REQUEST['name'])) echo $_REQUEST['name']; ?>">
            </div>

            <div class="row my-3">
            <h5 class="col-3">Campaign ID</h5>
            <input type="text" placeholder="" class="form-control-plaintext col-9 border border-info rounded"
                   name="campaignId" required="true" value="<?php if(isset($_REQUEST['campaignId'])) echo $_REQUEST['campaignId']; ?>">
            </div>

            <div class="row my-3">
            <h5 class="col-3">Nội dung</h5>
            <textarea  type="text" placeholder="" class="form-control-plaintext col-9 border border-info rounded" rows="4"
                   name="content"><?php if(isset($_REQUEST['content'])) echo $_REQUEST['content']; ?></textarea>
            </div>

            <div class="row my-3">
                <h5 class="col-3 ">Bắt đầu</h5>
                <input class="form-control col-9" type="date" name="from" required="true" value="<?php if(!is_null($from)) echo $from ?>">
            </div>
            <div class="row my-3">
                <h5 class="col-3">Kết thúc</h5>
                <input class="form-control col-9" type="date" name="to" required="true" value="<?php if(!is_null($to)) echo $to ?>">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-lg rounded" name="create" value="create">Tạo nhóm</button>
            </div>
        </form>
    </div>

    <?php if (isset($result) && ($result['result'] === false)): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Thông báo!</strong> <?php echo $result['mess']?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php elseif (isset($result) && ($result['result'] === true)): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Thành công!</strong> <?php echo $result['mess']?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
</div>
</body>
</html>

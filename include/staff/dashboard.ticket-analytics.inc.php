<?php
include_once INCLUDE_DIR.'class.ticket-analytics.php';
include_once INCLUDE_DIR.'class.tag.php';
// --------------------------------------------------------------------
function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

$tags_name = $chart_data = $table_data = [];

$tags = Tag::getAllTags();
$tags_name = array_values($tags);

if (!isset($_REQUEST['group']) || !in_array($_REQUEST['group'], ['day', 'week', 'month']))
    $_REQUEST['group'] = 'day';

if (!isset($_REQUEST['from']) || !$_REQUEST['from'])
    $_REQUEST['from'] = date('Y-m-d', time()-24*3600*30);

if (!isset($_REQUEST['to']) || !$_REQUEST['to'])
    $_REQUEST['to'] = date('Y-m-d');

$year_from = explode('-', $_REQUEST['from']);
$year_from = $year_from[0];
$year_to = explode('-', $_REQUEST['to']);
$year_to = $year_to[0];

$two_year = false;
if ($year_from != $year_to) {
    $two_year = true;
}

switch ($_REQUEST['group']) {
    case 'week':
        define('DATE_FORMAT', 'W-Y');
        break;
    case 'month':
        define('DATE_FORMAT', 'm-Y');
        break;
    case 'day':
    default:
        if ($two_year)
            define('DATE_FORMAT', 'Y-M-d D');
        else
            define('DATE_FORMAT', 'M-d D');
        break;
}

switch ($_REQUEST['group']) {
    case 'week':
    case 'month':
        function ___date_compare($a, $b) {
            $a = explode('-', $a);
            $b = explode('-', $b);

            if ($a[1] > $b[1]) return 1;
            if ($a[1] < $b[1]) return -1;
            return $a[0] - $b[0];
        }
        break;
    case 'day':
    default:
        function ___date_compare($a, $b) {
            $_a = date_create_from_format(DATE_FORMAT, $a)->getTimestamp();
            $_b = date_create_from_format(DATE_FORMAT, $b)->getTimestamp();

            return $_a - $_b;
        }
        break;
}
?>
<h1>Analytics</h1>

<form action="">
    <table width="95%">
        <tr>
            <td>From: </td>
            <td>
                <input type="text" name="from" class="dp input-field"
                       value="<?php if(isset($_REQUEST['from'])) echo trim(date('d/m/Y', strtotime($_REQUEST['from']))); else echo date('d/m/Y', time()-24*3600*30) ?>">
            </td>
            <td>To: </td>
            <td>
                <input type="text" name="to" class="dp input-field"
                       value="<?php if(isset($_REQUEST['to'])) echo trim(date('d/m/Y', strtotime($_REQUEST['to']))); else echo date('d/m/Y', time()-24*3600*0) ?>">
            </td>
            <td>Group: </td>
            <td>
                <select name="group" id="group">
                    <option value="day" <?php
                        if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'day') echo 'selected';
                        ?>>Day</option>
                    <option value="week" <?php
                        if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'week') echo 'selected';
                        ?>>Week</option>
                    <option value="month" <?php
                        if (isset($_REQUEST['group']) && $_REQUEST['group'] == 'month') echo 'selected';
                        ?>>Month</option>
                </select>
            </td>
            <td><button class="btn_sm btn-primary">Apply</button></td>
        </tr>
    </table>
</form>
<table width="100%">
    <tr>
        <?php include_once STAFFINC_DIR.'ticket-analytics.tag-overview.inc.php' ?>
    </tr>
    <tr>
        <?php include_once STAFFINC_DIR.'ticket-analytics.tag-by-day.inc.php' ?>
    </tr>
    <tr>
        <?php include_once STAFFINC_DIR.'ticket-analytics.ticket-source.inc.php' ?>
    </tr>
<!--    <tr>-->
    <?php include_once STAFFINC_DIR.'ticket-analytics.tag-source-overview.inc.php' ?>
<!--    </tr>    -->
    <tr>
        <?php include_once STAFFINC_DIR.'ticket-analytics.total-ticket-by-day.inc.php' ?>
        <?php include_once STAFFINC_DIR.'ticket-analytics.total-booking-by-day.inc.php' ?>
    </tr>
</table>

<p>
    <span
            class="btn_sm btn-xs btn-<?php if(isset($status_obj[ $row['booking_status_id'] ]) && isset($status_obj[ $row['booking_status_id'] ]['class']))
                echo $status_obj[ $row['booking_status_id'] ]['class'];
            else echo 'default'; ?> action-button "
            data-dropdown="#action-dropdown-statuses_<?php echo $row['ticket_id'] ?>">
    <i class="icon-caret-down pull-right"></i>
    <a class="tickets-action"
       href="#statuses"><i
                class=""></i> <?php
        if (isset($row['status_name'])) echo $row['status_name']; else echo __('Status'); ?></a>
    </span>
    <div id="action-dropdown-statuses_<?php echo $row['ticket_id'] ?>"
         class="action-dropdown anchor-right">

        <ul>
            <?php
            foreach ($status_obj as $status):
                if (isset($row['booking_status_id']) && isset($status['id']) && $row['booking_status_id'] == $status['id']) continue;
                ?>
                <form method="post" id="form_<?php echo $row['ticket_id'] ?>_<?php echo $status['id'] ?>" name="form" action="/scp/unfinished.php">
                    <?php csrf_token(); ?>
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="booking_id" value="<?php echo $row['ticket_id'] ?>">
                    <input type="hidden" name="status_id" value="<?php echo $status['id'] ?>">
                </form>
                <li> <a class="no-pjax"
                        href="#" onclick="document.getElementById('form_<?php echo $row['ticket_id'] ?>_<?php echo $status['id'] ?>').submit();return false();"
                    ><i class=""></i> <?php
                        echo $status['name']; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

    </div>

</p>
<div class="userpoint" data-id="<?php echo $point->id; ?>">
    <div class="header">
        <div class="header-left">
            <i class="point-type icon-<?php echo $point->getExtIconClass(); ?>"
                title="<?php echo $point->getIconTitle(); ?>"></i>&nbsp;
            <span class="point-number"><?php echo abs($point->change_point) ?></span>
            <?php echo $point->getFormattedTime(); ?>
        </div>
        <div class="header-right">
<?php
            echo $point->getStaff()->getName();
 ?>
        </div>
    </div>
    <div class="body editable">
        <p>
            <strong><?php echo $point->display(); ?></strong> ::
            <?php if(!empty($point->booking_code)): ?>
                <?php echo $point->booking_code; ?> ::
            <?php endif; ?>
            <?php if(!empty($point->note)): ?>
                <?php echo $point->note; ?>
            <?php endif; ?>
        </p>


    </div>
</div>

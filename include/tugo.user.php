<?php
namespace Tugo;
include_once INCLUDE_DIR.'../vendor/autoload.php';
include_once INCLUDE_DIR.'class.myemail.php';
include_once INCLUDE_DIR.'class.user-rank.php';
include_once INCLUDE_DIR.'class.user.php';

use Application;
use FacebookAds\Exception\Exception;
use Endroid\QrCode\QrCode;

class User {
    public static function generateUUID() {
        return substr(
            md5(
                uniqid('vn.tugo.app.userid', true)
                .time()
                .microtime(true)
                .rand(1, 999999)
            ),
            0,
            16
        );
    }

    public static function generateCustomerNumber() {
        $value = rand(111111, 999999);
        $checksum = static::_calc_checksum($value);
        return strval($value).strval($checksum);
    }

    public static function validateCustomerNumber($custumer_number) {
        $value = substr($custumer_number, 0, 6);
        $checksum = static::_calc_checksum($value);
        $custumer_number_checksum = substr($custumer_number, -3, 3);
        if ($checksum != $custumer_number_checksum) return false;
        return true;
    }

    private static function _calc_checksum($value) {
        $num = (substr($value, 0, 1) + substr($value, 1, 2))
            * substr($value, 3, 3);
        return substr($num, -3, 3);
    }

    public static function register($phonenumber, $name, $device_id , $is_cron = false,$dob=false) {
        $phonenumber = trim($phonenumber);
        $name = trim($name);
        $device_id = trim($device_id);

        if (empty($phonenumber))
            throw new Exception('Chưa nhập số điện thoại', 1510);

        if (empty($name) || !is_string($name))
            throw new Exception('Chưa nhập tên', 1511);

        if(!$is_cron) {
            if (empty($device_id) || !is_string($device_id))
                throw new Exception('Chưa có mã thiết bị', 1512);
        }

        if (!\_String::isMobileNumber($phonenumber) && '0000000009' != $phonenumber)
            throw new Exception('Số điện thoại không đúng', 1513) ;

        $phonenumber = \_String::formatPhoneNumber($phonenumber);

        $user = static::get(['phone_number' => $phonenumber]);
        if ($user)
            throw new Exception('Khách đã đăng ký thông tin.', 1525);
        if($dob && !empty($dob))
            $user = static::_add($phonenumber, $name,$dob);
        else
            $user = static::_add($phonenumber, $name);
        return $user;
    }

    public static function login($phonenumber, $device_id, $otp, $cloud_message_token = null) {
        $phonenumber = trim($phonenumber);
        $device_id = trim($device_id);
        $otp = trim($otp);

        if ('0000000009' == $phonenumber && '5555' == $otp) { // TEST
            $user = static::get(['phone_number' => $phonenumber]);
            if (!$user) throw new Exception('Không tìm thấy thông tin khách, vui lòng thử lại.', 1004);
            static::_login($phonenumber, $device_id, $cloud_message_token);
            return $user;
        } // END TEST ----------------------------------------------------
        elseif('0000000009' == $phonenumber && '5555' != $otp) {
            throw new Exception('OTP không đúng.', 1523);
        }

        if (empty($phonenumber))
            throw new Exception('Chưa nhập số điện thoại', 1510);

        if (!\_String::isMobileNumber($phonenumber))
            throw new Exception('Số điện thoại không đúng', 1513);

        if (empty($device_id) || !is_string($device_id))
            throw new Exception('Chưa có mã thiết bị', 1512);

        if (empty($otp))
            throw new Exception('Chưa có mã OTP', 1524);

        $phonenumber = \_String::formatPhoneNumber($phonenumber);

        if (!App::verifyOTP($phonenumber, $otp))
            throw new Exception('OTP không đúng.', 1523);

        $user = static::get(['phone_number' => $phonenumber]);
        if (!$user)
            throw new Exception('Khách chưa đăng ký thông tin.', 1517);

        static::_login($phonenumber, $device_id, $cloud_message_token);

        return $user;
    }

    public static function refresh($user_uuid = null, $phonenumber = null, $device_id = null, $refresh_token) {
        $user = null;

        if (!is_null($user_uuid)) $user_uuid = trim($user_uuid);
        if (!is_null($phonenumber)) $phonenumber = trim($phonenumber);
        if (!is_null($device_id)) $device_id = trim($device_id);

        if ($user_uuid) $user = static::get(['uuid' => $user_uuid]);
        if ($phonenumber) {
            $phonenumber = \_String::formatPhoneNumber($phonenumber);
            $user = static::get(['phone_number' => $phonenumber]);
        }

        if (!$user) throw new Exception('Khách chưa đăng ký thông tin.', 1517);

        if (!Token::verifyRefreshToken($user['uuid'], $device_id, $refresh_token))
            throw new Exception('Mã refresh token không hợp lệ', 1521);

        $token = Token::getNewToken($user, $device_id);

        if (!$token) throw new Exception('Lỗi tạo mã refresh token', 1522);

        return $token;
    }

    public static function logout($payload, $device_id) {
        return static::_logout($payload, $device_id);
    }

    private static function _login($phonenumber, $device_id, $cloud_message_token = null) {
        $phonenumber = trim($phonenumber);
        $device_id = trim($device_id);
        if ($cloud_message_token)
            $cloud_message_token = trim($cloud_message_token);

        $phonenumber = \_String::formatPhoneNumber($phonenumber);
        $user = static::get(['phone_number' => $phonenumber]);
        if (!$user)
            throw new Exception('Không tìm thấy thông tin khách', 1518);

        if (!static::_set_device_status($user['uuid'], $device_id, $cloud_message_token, Device::LOGIN))
            throw new Exception('Lỗi, vui lòng thử lại', 1519);

        return $user;
    }

    private static function _logout($payload, $device_id) {
        $payload = (array) $payload;
        $device_id = trim($device_id);

        if (!$payload) throw new Exception('Không tìm thấy thông tin khách', 1518);
        Token::removeRefreshToken($payload['uuid'], $device_id);

        if (!static::_set_device_status($payload['uuid'], $device_id, null, Device::LOGOUT))
            throw new Exception('Lỗi, vui lòng thử lại', 1520);

        return true;
    }

    private static function _set_device_status($user_uuid, $device_id, $cloud_message_token = null, $status) {
        $user_uuid = db_input($user_uuid);
        $device_id = db_input($device_id);
        $cloud_message_token = db_input($cloud_message_token);

        $status = is_numeric($status) && intval($status) >= 1 ? 1 : 0;

        if ($status == 0) {
            $sql = "SELECT COUNT(*) as counter FROM api_device
                WHERE user_uuid LIKE $user_uuid
                  AND device_id LIKE $device_id
            ";

            $res = db_query($sql);
            if (!$res) return false;
            if (! ($row = db_fetch_array($res)) || !isset($row['counter']) || $row['counter'] != 1)
                return false;
        }

        $sql = "REPLACE INTO api_device SET 
            user_uuid = $user_uuid,
            device_id = $device_id,
            is_logged_in = $status,
            cloud_message_token = $cloud_message_token
        ";

        return db_query($sql);
    }

    private static function _add($phonenumber, $name,$dob = false) {
        $phonenumber = trim($phonenumber);
        $name = trim($name);

        if (empty($phonenumber))
            throw new Exception('Chưa nhập số điện thoại', 1510);

        if (!\_String::isMobileNumber($phonenumber) && '0000000009' != $phonenumber)
            throw new Exception('Số điện thoại không đúng', 1513);

        if (empty($name))
            throw new Exception('Chưa nhập tên', 1511);

        $phonenumber = \_String::formatPhoneNumber($phonenumber);

        if (static::get(['phone_number' => $phonenumber]))
            throw new Exception('Thông tin khách đã có sẵn', 1514);

        // tạo mã số UUID - dùng lưu trữ
        $uuid = static::_generate_unique_number_code('uuid');
        // tạo số khách hàng, hiển thị trên giao diện
        $number = static::_generate_unique_number_code('customer_number');

        try {
            $vars = [
                'name' => $name,
                'phone' => $phonenumber,
                'uuid' => $uuid,
            ];
            $check = \User::checkAndCreate($vars);

            if ($check) {
                $check->set('uuid', $uuid);
                if (($_user_form = $check->getForms())
                    && ($user_form = $_user_form[0])
                    && ($field = $user_form->getField('rank'))
                ) {
                    $conf = $field->get('configuration') ? json_decode($field->get('configuration'), true) : null;
                    $default_rank = array_pop(array_keys($conf['default']));

                    if ($default_rank) {
                        foreach ($_user_form as $cd) {
                            if ( !(($f=$cd->getForm()) && $f->get('type') == 'U') ) continue;

                            if (($rank = $f->getField('rank'))) {
                                $cd->setAnswer('rank', $default_rank);
                            }

                            // DynamicFormEntry::save returns the number of answers updated
                            if ($cd->save()) {
                                $check->updated = \SqlFunction::NOW();
                            }
                        }
                    }
                }

                $check->save();
            }
        } catch (Exception $ex) {

        }

        $number = db_input($number);
        $uuid = db_input($uuid);
        $phonenumber = db_input($phonenumber);
        $name = db_input($name);

        $sql = "INSERT INTO api_user SET 
            uuid = $uuid,
            customer_number = $number,
            customer_name = $name,
            phone_number = $phonenumber,
            register_at = NOW()
        ";
        //insert DOB
        if($dob && !empty($dob)){
            $sql .= " ,dob = ".db_input($dob);
        }
        return db_query($sql);
    }

    private static function _generate_unique_number_code($type) {
        $try = 0;
        $max_tries = 10; // prevent infinity loop
        $flag = false;
        $number = null;
        do {
            $try++;
            if ($try >= $max_tries) {
                $flag = true;
                break;
            }

            switch ($type) {
                case 'uuid':
                    $number = static::generateUUID();
                    break;
                case 'customer_number':
                    $number = static::generateCustomerNumber();
                    break;
                default:
                    throw new Exception('Invalid type', 1515);
                    break;
            }

        } while (static::get([$type => $number]));

        if ($flag || is_null($number))
            throw new Exception("Không tạo được mã loại $type, vui lòng thử lại.", 1516);

        return $number;
    }

    public static function update($payload, $name) {
        $payload = (array) $payload;
        if (!$payload) throw new Exception('Không tìm thấy thông tin khách', 1518);

        $name = trim($name);
        if (empty($name))
            throw new Exception('Chưa nhập tên', 1511);

        $name = db_input($name);
        $user_uuid = db_input($payload['uuid']);
        $sql = " UPDATE api_user SET customer_name = $name WHERE uuid LIKE $user_uuid LIMIT 1 ";

        if (!db_query($sql)) throw new Exception('Cập nhật bị lỗi', 1520);

        $user = static::getProfile($payload);

        if (!$user) throw new Exception('Không tìm thấy thông tin khách', 1518);

        return $user;
    }

    public static function get($vars) {
        $sql = " SELECT * FROM api_user WHERE 1 ";
        $vars = array_filter($vars);

        foreach ($vars as $key => $value) {
            $value = trim($value);
            if (empty($value)) throw new Exception('Giá trị rỗng', 1522);

            switch ($key) {
                case 'phone_number':
                    $_phone_number = preg_replace('/[^0-9]/', '', $value);
                    if (!preg_match('/^0/', $_phone_number))
                        $_phone_number = '0'.$_phone_number;
                    $sql .= " AND phone_number LIKE ".db_input($_phone_number);
                    break;
                case 'uuid':
                    $sql .= " AND uuid LIKE ".db_input($value);
                    break;
                case 'customer_number':
                    $sql .= " AND customer_number LIKE ".db_input($value);
                    break;
                default:
                    throw new Exception('Get by column '.$key.' is not supported');
            }
        }

        $sql .= " LIMIT 1 ";
        $res = db_query($sql);
        if (!$res) return null;

        $user = db_fetch_array($res);
        if (!$user) return null;

        return $user;
    }

    public static function set($vars, $uuid) {
        $sql = " UPDATE api_user SET ";
        $vars = array_filter($vars);

        foreach ($vars as $key => $value) {
            $value = trim($value);

            switch ($key) {
                case 'rank':
                    $sql .= " rank = ".db_input($value);
                    break;
                default:
                    throw new Exception('Updating column '.$key.' is not supported');
                    break;
            }
        }

        $sql .= " WHERE uuid LIKE ".db_input($uuid);
        return db_query($sql);
    }

    public static function getName($phonenumber) {
        $phonenumber = \_String::formatPhoneNumber($phonenumber);
        $user = static::get(['phonenumber' => $phonenumber]);
        return $user['customer_name'];
    }

    public static function getPhoneNumber($user_uuid) {
        $user = static::get(['uuid' => $user_uuid]);
        return $user['phone_number'];
    }

    public static function getPoint($user_uuid) {
        $user = static::get(['uuid' => $user_uuid]);
        return isset($user['user_point']) && is_numeric($user['user_point']) ? intval($user['user_point']) : 0;
    }

    public static function getInfo($user_uuid) {
        return static::get(['uuid' => $user_uuid]);
    }

    public static function changePoint($user_uuid, $point) {
        $user_uuid = trim($user_uuid);
        if (!static::get(['uuid' => $user_uuid]))
            throw new Exception('Khách chưa đăng ký', 1517);

        if (!$point)
            throw new Exception('Điểm phải là số âm hoặc số dương', 1527);

        return static::_update_point($user_uuid, $point);
    }

    private static function _update_point($uuid, $point) {
        $uuid = db_input($uuid);
        $sql = "UPDATE api_user SET user_point=user_point".($point > 0 ? "+".$point : "-".abs($point))." WHERE uuid LIKE ".$uuid;
        return db_query($sql);
    }

    public static function addPoint($user_uuid, $point) {
        return static::changePoint($user_uuid, abs(intval($point)));
    }

    public static function subPoint($user_uuid, $point) {
        return static::changePoint($user_uuid, (-1)*abs(intval($point)));
    }

    public static function getPointHistory($payload, &$total, $page, $limit) {
        $payload = (array) $payload;
        if (!$payload || !isset($payload['uuid'])) throw new Exception('Không tìm thấy thông tin khách', 1518);

        return Point::get($payload['uuid'], $total, $page, $limit);
    }

    public static function generateQRCode($user) {
        $user = (array) $user;
        if (!$user) return null;
        $qrcode_string = $user['customer_name']." - ".$user['customer_number']." - ".$user['phone_number'];
        $file_name = md5($qrcode_string).'.png';
        $path = '/public_file/qrcode/';
        if (!file_exists(__DIR__.'/..'.$path))
            @mkdir(__DIR__.'/..'.$path, 0777);
        $file_path = __DIR__.'/..'.$path.$file_name;

        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $url = $protocol.$_SERVER['HTTP_HOST'].$path.$file_name;

        if (file_exists($file_path))
            return $url;

        $qrCode = new QrCode();
        $qrCode->setText($qrcode_string)
            ->setSize(500)
            ->setMargin(20)
            ->setErrorCorrectionLevel('high')
            ->setForegroundColor(array('r' => 102, 'g' => 0, 'b' => 102, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
        ;
        $qrCode->setWriterByExtension('png');
        $qrCode->writeFile($file_path);

        return $url;
    }

    public static function sendRequest($user, $email, $content, $type) {
        if (empty(trim($content))) throw new Exception('Chưa điền nội dung', 1526);
        $content = trim($content);
        $user = (array) $user;
        if (!$user) throw new Exception('Khách chưa đăng ký thông tin.', 1517);
        $email = trim($email) ?: '-- chưa có --';
        $content = "<p>".$content."</p>";

        $subject = "[APP][".$type."] Phản hồi từ khách :: ".date('d M Y H:i:s');
        $content .= "
        <hr><p>Thông tin khách: </p>
        <ul>
        <li>Tên khách: ".$user['customer_name']."</li>
        <li>Email: ".$email."</li>
        <li>Số điện thoại: ".$user['phone_number']."</li>
        <li>Số khách hàng: ".$user['customer_number']."</li>
        </ul>
        <p>Thời gian gửi: ".date('d M Y, H:i:s')."</p>
        <hr>
        <p><small>Thông tin được gửi thông qua app Tugo Loyalty</small></p>
        ";
        $to = config('app_email_'.strtolower($type));
        $to_name = config('app_email_'.strtolower($type).'_name');
        $from = config('app_email_from');
        $from_name = config('app_email_from_name');
        return \MyEmail::send($to, $to_name, $subject, $content, $from, $from_name);
    }

    public static function getProfile($payload) {
        $payload = (array) $payload;
        if (!$payload) throw new Exception('Khách chưa đăng ký thông tin.', 1517);

        $user_data = [];
        $user = User::get(['uuid' => $payload['uuid']]);
        if (!$user) throw new Exception('Khách chưa đăng ký thông tin.', 1517);
        $user_data['uuid'] = $user['uuid'];
        $user_data['customer_name'] = $user['customer_name'];
        $user_data['customer_number'] = $user['customer_number'];
        $user_data['phone_number'] = $user['phone_number'];
        $user_data['user_point'] = $user['user_point'];
        $user_data['qrcode'] = static::generateQRCode($user);

        return $user_data ?: [];
    }
}

class Point {
    public static function get($user_uuid, &$total, $page = 1, $limit = 10) {
        $user_uuid = db_input($user_uuid);

        $sql = "SELECT count(*) as total FROM api_user_point_history WHERE user_uuid LIKE $user_uuid";
        $res = db_query($sql);
        $total = 0;
        if ($res) {
            $row = db_fetch_array($res);
            if ($row && isset($row['total'])) {
                $total = $row['total'];
            }
        }

        $offset = ($page - 1) * $limit;
        $sql = "SELECT * FROM api_user_point_history WHERE user_uuid LIKE $user_uuid ORDER BY `date` DESC LIMIT $offset, $limit";
        $res = db_query($sql);
        if (!$res) return [];
        $result = [];

        while (($row = db_fetch_array($res))) {
            $result[] = [
                "id" => $row['id'],
                "booking_code" => $row['booking_code'] ?: '',
                "point" => $row['change_point'] ?: 0,
                "date" => $row['date'],
                "amount" => /*$row['amount'] ?:*/ 0,
            ];
        }

        return $result;
    }
}

class Device {
    const LOGIN = 1;
    const LOGOUT = 0;

    public static function generateUUID() {
        return substr(
            md5(
                uniqid('vn.tugo.app.deviceid', true)
                .time()
                .microtime(true)
                .rand(1, 999999)
            ),
            0,
            16
        );
    }

    public static function get($vars) {
        $sql = " SELECT * FROM api_device WHERE is_logged_in = 1 ";
        $vars = array_filter($vars);

        foreach ($vars as $key => $value) {
            $value = trim($value);
            if (empty($value)) throw new Exception('Giá trị rỗng', 1522);

            switch ($key) {
                case 'device_id':
                    $sql .= " AND device_id LIKE ".db_input($value);
                    break;
                case 'uuid':
                    $sql .= " AND user_uuid LIKE ".db_input($value);
                    break;
                default:
                    throw new Exception('Get by column '.$key.' is not supported');
            }
        }

        $res = db_query($sql);
        if (!$res) return null;

        $tokens = [];
        while (($row = db_fetch_array($res))) {
            if (isset($row['cloud_message_token']) && !empty(trim($row['cloud_message_token'])))
                $tokens[] = $row['cloud_message_token'];
        }

        return $tokens;
    }
}

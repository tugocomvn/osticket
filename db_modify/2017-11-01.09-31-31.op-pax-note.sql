ALTER TABLE ost_pax_tour ADD note TEXT NULL;
ALTER TABLE ost_pax_tour
  MODIFY COLUMN note TEXT AFTER phone_number;
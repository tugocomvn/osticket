<?php
function ___render_checkin_row($row, $_date, &$i) {
    ?><tr>
    <td><?php echo $i++ ?></td>
    <td><?php echo $row['firstname'].' '.$row['lastname'] ?></td>
    <td><?php if (isset($row['missing']) && $row['missing']) echo '-- Absence --'; else echo $row['username']; ?></td>
    <td class="<?php
    if(isset($row['checkin_time']) && $row['checkin_time'] && date('H', strtotime($row['checkin_time'])) >= 8) echo ' late ';
    if (isset($row['missing']) && $row['missing']) echo ' absence ';
    ?>"
    >
        <?php if (isset($row['missing']) && $row['missing']) echo $_date; else echo $row['checkin_time'];  ?>
        <?php if(date('w', strtotime($_date)) == 0) echo ' (Sunday) ' ?>
        <?php if(isset($row['checkin_time']) && $row['checkin_time'] && date('H', strtotime($row['checkin_time'])) >= 8) echo ' (late) '; ?>
    </td>
    </tr><?php
}
?>
<hr>
<style>
    td.late,
    td.absence {
        background-color: rgba(255, 97, 95, 0.37) !important;
    }
</style>
<div style="background-color: rgba(255,0,255,0.05);padding: 30px 0;">
    <h2><?php echo __('ALL Checkin Summary'); ?></h2>

    <?php
    include_once INCLUDE_DIR.'class.checkin.php';

    if (!isset($_REQUEST['from']) || empty($_REQUEST['from']))
        $_REQUEST['from'] = date('01/m/Y', time());

    if (!isset($_REQUEST['to']) || empty($_REQUEST['to']))
        $_REQUEST['to'] = date('d/m/Y', time());

    if (!isset($_REQUEST['export']) || !$_REQUEST['export'])
        $_REQUEST['export'] = 0;

    $from = date_create_from_format('d/m/Y', $_REQUEST['from'])
        ? date_create_from_format('d/m/Y', $_REQUEST['from'])->format('Y-m-d') : date('Y-m-d');
    $to = date_create_from_format('d/m/Y', $_REQUEST['to'])
        ? date_create_from_format('d/m/Y', $_REQUEST['to'])->format('Y-m-d') : date('Y-m-d');

    if (!isset($_REQUEST['agent']) || empty($_REQUEST['agent']))
        $_REQUEST['agent'] = 0;

    $data = Checkin::getByStaff($_REQUEST['agent'], $from, $to, $_REQUEST['export'] ? 999 : 32, $_REQUEST['export']);
    ?>

    <div>
        <form action="" method="get" class="well form-inline">
            <?php csrf_token(); ?>
            <table class="">
                <tr>
                    <td><b><?php echo __('From'); ?></b></td>
                    <td>
                        <input type="text" class="input-field datep" name="from" value="<?php echo $_REQUEST['from'] ?>">
                    </td>
                    <td><b><?php echo __('To'); ?></b></td>
                    <td>
                        <input type="text" class="input-field datep" name="to" value="<?php echo $_REQUEST['to'] ?>">
                    </td>
                    <td>
                        <button class="btn_sm btn-default">Search</button>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Agent'); ?></td>
                    <td>
                        <select name='agent' class="input-field">
                            <option value><?php echo __('All');?></option>
                            <?php
                            $sql='SELECT staff_id,CONCAT_WS(" ", firstname, lastname) as name '
                                .' FROM '.STAFF_TABLE.' staff '
                                .' ORDER by name';
                            if(($agent_res=db_query($sql)) && db_num_rows($agent_res)) {
                                while(list($id,$name)=db_fetch_row($agent_res)){
                                    $selected=($_REQUEST['agent'] && $id==$_REQUEST['agent'])?'selected="selected"':'';
                                    echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td></td>
                    <td><input type="checkbox" name="late" value="1" <?php if (isset($_REQUEST['late']) && $_REQUEST['late']) echo 'checked' ?> > Late/Absence</td>
                    <td>
                        <button class="btn_sm btn-success" formaction="export-checkin.php" name="export" value="1">Export</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <?php if(!$data): ?>
        <p>No data.</p>
    <?php else: ?>
        <table class="dashbard_list list" border="0" cellspacing="1" cellpadding="2" width="1058">
            <thead>
            <tr>
                <th>#</th>
                <th>Staff</th>
                <th>Username</th>
                <th>Checkin time</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $checkins = [];
            while (($row = db_fetch_array($data))) {
                $checkins[ date('Y-m-d', strtotime($row['checkin_time'])) ][ $row['staff_id'] ] = $row;
            }

            if (isset($_REQUEST['agent']) && $_REQUEST['agent']) {
                for ($date = $from; strtotime($date) <= strtotime($to); $date = date('Y-m-d', strtotime($date) + 24 * 3600)) {
                    if (!isset($checkins[$date][ $_REQUEST['agent'] ]))
                        $checkins[$date][ $_REQUEST['agent'] ] = [
                            'missing' => 1,
                        ];
                }
            }

            krsort($checkins);

            $i = 1;
            foreach ($checkins as $_date => $row) {
                if (isset($_REQUEST['agent']) && $_REQUEST['agent']) {
                    ___render_checkin_row($row[ $_REQUEST['agent'] ], $_date, $i);
                } else {
                    foreach ($row as $_staff_id => $_row) {
                        ___render_checkin_row($_row, $_date, $i);
                    }
                }
            }
            ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<script>
    var conf;
    /* Get config settings from the backend */
    getConfig().then(function(c) {
        conf = c;
        // Datepicker
        $('.datep').datepicker({
            numberOfMonths: 1,
            showButtonPanel: true,
            buttonImage: './images/cal.png',
            showOn:'both',
            dateFormat: $.translate_format(c.date_format||'d/m/Y')
        });

    });
</script>

<?php
/**
 * Created by PhpStorm.
 * User: minhjoko
 * Date: 6/14/18
 * Time: 10:59 AM
 */
if(!function_exists('staffLoginPage')) { //Ajax interface can pre-declare the function to  trap expired sessions.
    function staffLoginPage($msg) {
        global $ost, $cfg;
        $_SESSION['_staff']['auth']['dest'] =
            '/' . ltrim($_SERVER['REQUEST_URI'], '/');
        $_SESSION['_staff']['auth']['msg']=$msg;
        $_SESSION['_staff']['auth']['dayoff']=true;
        require(SCP_DIR.'login.php');
        exit;
    }
}


$info=array();
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);

if (!$info['topicId'])
    $info['topicId'] = $cfg->getDefaultTopicId();

$form = null;
if ($info['topicId'] && ($topic=Topic::lookup($info['topicId']))) {
    $form = $topic->getForm();
    if ($_POST && $form) {
        $form = $form->instanciate();
        $form->isValid();
    }
}

if ($_POST)
    $info['duedate'] = Format::date($cfg->getDateFormat(),
        strtotime($info['duedate']));
?>
<form action="tickets.php?a=ol" method="post" id="save"  enctype="multipart/form-data">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="create">
    <input type="hidden" name="a" value="ol">
    <h2><?php echo __('Create a Off/Late');?></h2>
    <table class="form_table fixed" width="1058" border="0" cellspacing="0" cellpadding="2">
        <thead>
        <!-- This looks empty - but beware, with fixed table layout, the user
             agent will usually only consult the cells in the first row to
             construct the column widths of the entire toable. Therefore, the
             first row needs to have two cells -->
        <tr><td></td><td></td></tr>
        <tr>
            <th colspan="2">
                <h4><?php echo __('New Off/Late');?></h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th colspan="2">
                <em><strong><?php echo __('Off/Late Information and Options');?></strong>:</em>
            </th>
        </tr>
        <tr>
            <td width="160" class="required">
                <?php echo __('Type'); ?>:
            </td>
            <td>
                <select name="topicId" onchange="javascript:
                        var data = $(':input[name]', '#dynamic-form').serialize();
                        $('input, button').prop('disabled', true);
                        $.ajax(
                          'ajax.php/form/help-topic/' + this.value,
                          {
                            data: data,
                            dataType: 'json',
                            success: function(json) {
                              if (json.type && json.type == 'ol') { // form ke toan

                              } else {

                              }
                              $('input, button').prop('disabled', false);
                              $('#dynamic-form').empty().append(json.html);
                              $(document.head).append(json.media);
                            }
                          });">
                    <?php $selected = null;
                    if ($topics=Topic::getHelpTopics()) {
                        if (count($topics) == 1)
                            $selected = 'selected="selected"';
                        else { ?>
                        <?php                   }
                        foreach($topics as $id =>$name) {
                            if (!in_array($id, [OL_FORM])) continue;
                            echo sprintf('<option value="%d" %s %s>%s</option>',
                                $id, ($info['topicId']==$id)?'selected="selected"':'',
                                $selected, $name);
                        }
                        if (count($topics) == 1 && !$form) {
                            if (($T = Topic::lookup($id)))
                                $form =  $T->getForm();
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['topicId']; ?></font>
            </td>
        </tr>

        </tbody>
        <tbody id="dynamic-form">
        <?php
        if ($form) {
            print $form->getForm()->getMedia();
            include(STAFFINC_DIR .  'templates/dynamic-form-ol.tmpl.php');
        }
        ?>
        </tbody>
        <?php
        if($thisstaff->canAssignTickets()) { ?>
            <tr>
                <td width="160" style="font-weight: bold"><?php echo __('Người duyệt');?>:</td>
                <td>
                    <select id="assignId" name="assignId" required>
                        <option value=>&mdash; <?php echo __('Select Staff Accept');?> &mdash;</option>
                        <?php
                        if(($users=Staff::getAvailableStaffMembers())) {
                            foreach($users as $id => $name) {
                                if ($thisstaff->getId() == $id) continue;
                                $k="s$id";
                                echo sprintf('<option value="%s" %s>%s</option>',
                                    $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                            }
                        }
                        ?>
                    </select>&nbsp;<span class='error'>&nbsp;<?php echo $errors['assignId']; ?></span>
                </td>
            </tr>
        <?php } ?>
    </table>
    <p style="text-align:center;">
        <button type="submit" name="submit" class="btn_sm btn-primary">Create</button>
        <input id="reset_btn"type="reset"  name="reset"  class="btn_sm btn-default" value="<?php echo __('Reset');?>">
        <input type="button" name="cancel" class="btn_sm btn-danger" value="<?php echo __('Cancel');?>" onclick="javascript:
        $('.richtext').each(function() {
            var redactor = $(this).data('redactor');
            if (redactor && redactor.opts.draftDelete)
                redactor.deleteDraft();
        });
        window.location.href='tickets.php';
    ">
    </p>
</form>
<script>
    (function($) {
        setTimeout(function() {
            $('[name="topicId"]').change();
        }, 1000)
    })(jQuery);
    $('#reset_btn').click(function () {
        window.location.reload();
    })


</script>
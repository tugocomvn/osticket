<?php
if(!defined('OSTADMININC') || !$thisstaff->isAdmin()) die('Access Denied');

?>

<form action="/scp/facebook.php" method="post" name="custom_audience">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="mass_process" >
    <input type="hidden" id="action" name="a" value="" >
    <?php $audience_sets = config('audience_set'); ?>
    <p>
        Phone number from source:
        <select name="source" id="" required>
            <option value>-- Select --</option>
            <option value="all">All</option>
            <option value="operator">Operator</option>
        </select>
        upload to Audience Set:
        <select name="audience_set" id="" class="input-field" required>
            <option value>-- Select --</option>
            <?php foreach($audience_sets as $set): ?>
                <?php list($set_id, $set_name) = explode('/', $set) ?>
                <option value="<?php echo $set_id  ?>"><?php echo $set_name ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <button class="btn_sm btn-primary" type="submit" value="1" name="phone_number">Upload Phone numbers</button>
    </p>

</form>

<?php if(isset($errors) && $errors): ?>
<ul>
    <?php foreach($errors as $k => $e): ?>
    <li><?php echo $k.": ".$e; ?></li>
    <?php endforeach; ?>
</ul>

<?php endif; ?>
<p>Done. Total: <?php echo $count ?></p>
<p><a href="/public_file/<?php echo $fb_audience_file_name ?>" class="no-pjax" target="_blank">Data Download</a></p>

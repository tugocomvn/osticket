<?php

require __DIR__.'/../../vendor/autoload.php';
use \Curl\Curl;

date_default_timezone_set('Asia/Ho_Chi_Minh');

$server = 'http://testing.tugo.com.vn/remote-watch/server/check.php?';
$server_name = 'Tugo Website';
check_server($server, $server_name);

$server = 'http://support.tugo.com.vn/remote-watch/server/check.php?';
$server_name = 'osTicket';
check_server($server, $server_name);

function check_server($server, $server_name) {
    echo date('Y-m-d H:i:s'). " - Checking server $server_name\r\n";
    $msg = '';
    $time = 0;
    $send_mail = false;
    _check_server($server, $time, $send_mail, $msg);
    if ($send_mail || $time >=3) {
        _check_server($server, $time, $send_mail, $msg);

        if ($send_mail || $time >=3) {
            _check_server($server, $time, $send_mail, $msg);

            if ($send_mail || $time >=3) {
                if ($time >= 3)
                    $msg .= '<p>Long time response</p>';

                send_email($server_name, $msg);
            }
        }
    }
}

function send_email($server_name, $message) {
    require __DIR__.'/../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.zoho.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'tugo@buu.vn';                 // SMTP username
    $mail->Password = 'W>Bjaq<85[9]sG7A';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    $mail->setFrom('tugo@buu.vn', NOTIFICATION_TITLE);
    $mail->addAddress('phamquocbuu@gmail.com', 'Buu Pham');     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $server_name.' - Server issue - #'.uniqid();
    $mail->Body    = $message;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if(!$mail->send()) {
        echo "Message could not be sent.\r\n";
        echo 'Mailer Error: ' . $mail->ErrorInfo."\r\n";
    } else {
        echo "Message has been sent\r\n";
    }
}

function _check_server($server, &$time, &$send_mail, &$msg) {
    $send_mail = true;
    $tmp_msg = '';
    $start = time();
    $param = [];
    $param['param1'] = uniqid("", true);
    $param['param2'] = uniqid("", true);
    $md5 = md5(implode('_', [$param['param1'], $param['param2']]));
    $string = 'VN71BfFVgWnyq52M2tnf7ZQZ3k2Hw54nJJd5es4S4f64x6u59PxEMxMy594SbEMS';
    $h = rand(1, strlen($string)-1);

    $param['param5'] = substr($string, 0, $h);
    $param['param7'] = substr($string, $h, strlen($string)-1);

    $request_params = '';

    foreach ($param as $key => $value)
        $request_params .= $key.'='.urlencode($value).'&';

    $request_params = trim($request_params, '&');
    $url = $server.$request_params;

    $curl = new Curl();
    $curl->get($url);

    try {
        if ($curl->error) {
            $tmp_msg .= date('Y-m-d H:i:s').' - Server remote checking - Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
            echo $tmp_msg;
            $msg .= '<p>'.$tmp_msg.'</p>';
        } else {
            $tmp_msg .= date('Y-m-d H:i:s').' - Server remote checking - ';
            $res = $curl->response;
            $ok = true;
            if ( ! ( isset($res->result)
                && isset($res->result->params)
                && isset($res->result->time)
                && isset($res->result->total) )
            ) {
                $tmp_msg .= ' Error: ' ;
                if (!isset($res->result->params) || $res->result->params !== $md5)
                    $tmp_msg .= ' Invalid params return; ';
                if (!isset($res->result->time)
                    || !date_create_from_format('Y-m-d H:i:s', $res->result->time)
                    || date_diff(date_create_from_format('Y-m-d H:i:s', $res->result->time), new DateTime())->format('%a') > 2
                )
                    $tmp_msg .= ' MySQL time difference; ';
                if (!isset($res->result->total) || !$res->result->total || $res->result->total < 0)
                    $tmp_msg .= ' Cannot count tickets; ';

                $ok = false;
            }

            if (isset($res->errors) && $res->errors) {
                foreach ($res->errors as $e) {
                    $tmp_msg .= " $e; ";
                }

                $ok = false;
            }

            if ($ok) {
                $send_mail = false;
                $tmp_msg .= ' OK ';
            }
        }
    } catch (Exception $e) {
        $tmp_msg .= $e->getMessage();
    }

    echo $tmp_msg;
    $msg .= '<p>'.$tmp_msg .'</p>';
    echo "\r\n";

    $end = time();
    $time = $end - $start;
}

/*branch: reservation*/
create table tour_export_log
(
	id int auto_increment,
	tour_id int null,
	content longtext null,
	filename text null,
	created_by int null,
	created_at datetime null,
	constraint tour_export_log_pk
		primary key (id)
);


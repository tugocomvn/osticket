<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!($thisstaff->canApproveOperatorReservation() || $thisstaff->canViewApproveOperator()))
    die('Access Denied');
?>
<table>
    <caption><span class="label label-important">Operator Reviewed</span></caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Sale</th>
        <th>Qty</th>
        <th>Customer / <br>Phone number</th>
        <th>Booking Code/Price</th>
        <th>Note</th>
        <th>Tour/Country</th>
        <th>Reviewed At</th>
    </tr>
    </thead>
    <tbody>
    <?php $count_item = 0; $i=1; ?>
    <?php while($opReviewed && ($history = db_fetch_array($opReviewed))): $count_item++; ?>
        <tr class="<?php if (strtotime($history['due_at']) > 0 && strtotime($history['due_at']) <= time()) echo 'overdue' ?>">
            <td><?php echo $i++; ?></td>
            <td><?php if (isset($all_staff[ $history['staff_id'] ])) echo $all_staff[ $history['staff_id'] ]; else echo '- Tugo -'; ?></td>
            <td><?php if (isset($history['sure_qty']) && $history['sure_qty']) echo $history['sure_qty']; ?></td>
            <td><?php if (isset($history['customer_name']) && $history['customer_name']) echo $history['customer_name']; ?>
                <br><?php if (isset($history['phone_number']) && $history['phone_number']) echo $history['phone_number']; ?></td>
            <td>
                <?php if (isset($history['booking_code']) && $history['booking_code']) echo $history['booking_code']; ?>
                <hr>
                <?php if (isset($history['total_retail_price']) && $history['total_retail_price']) echo number_format($history['total_retail_price'], 0, '.', '.').'đ'; ?>
            </td>
            <td>
                <?php if (isset($history['note']) && $history['note']) echo $history['note']; ?>
                <?php if (isset($history['visa_only']) && $history['visa_only']): ?>
                    <label class="label label-default label-small" for="">Visa Only</label>
                <?php endif; ?>
                <?php if (isset($history['security_deposit']) && $history['security_deposit']): ?>
                    <label class="label label-default label-small" for="">Security Deposit</label>
                <?php endif; ?>
                <?php if (isset($history['fe']) && $history['fe']): ?>
                    <label class="label label-default label-small" for="">FE</label>
                <?php endif; ?>
                <?php if (isset($history['visa_ready']) && $history['visa_ready']): ?>
                    <label class="label label-default label-small" for="">Visa Ready</label>
                <?php endif; ?>
                <?php if (isset($history['one_way']) && $history['one_way']): ?>
                    <label class="label label-default label-small" for="">One Way</label>
                <?php endif; ?>

                <?php if(isset($history['infant']) && $history['infant']): ?>
                    [ Infant: <?php echo $history['infant'] ?> ]
                <?php endif; ?>
            </td>
            <td class="goto" onclick="window.location='<?php echo $cfg->getUrl() ?>scp/reservation.php?reservation_id=<?php echo $history['id'] ?>'">
                <?php
                if (isset($history['tour_id']) && $history['tour_id']) {
                    $tour = TourNew::lookup($history['tour_id']);
                    if ($tour) echo $tour->name;
                } elseif (isset($history['country']) && $history['country']) {
                    $country = TourMarket::lookup($history['country']);
                    if($country)
                        echo 'Visa '.$country->name;
                }

                ?>
            </td>
            <td>
                <?php echo Format::date('d/m/y H:i', strtotime($history['op_review_at'])) ?>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$count_item): ?>
        <tr><td>No Item</td></tr>
    <?php endif; ?>
    </tbody>
</table>
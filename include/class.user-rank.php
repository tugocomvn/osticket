<?php
require_once(INCLUDE_DIR . 'class.orm.php');

class UserRankModel extends VerySimpleModel {
    static $meta = array(
        'table' => USER_RANK_TABLE,
        'pk' => array('id'),
        'ordering' => array('name')
    );
}

class UserRank extends UserRankModel {

}

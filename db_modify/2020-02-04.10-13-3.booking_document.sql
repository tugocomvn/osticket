/*branch booking*/
create table booking_document
(
    id int unsigned auto_increment,
    ticket_id int unsigned null,
    action_id tinyint null,
    filename text null comment 'name+extend',
    created_by int null,
    created_at datetime null,
    constraint booking_document_log_pk
        primary key (id)
);

<?php

/**
 * @author buupham
 */

require_once INCLUDE_DIR . 'class.orm.php';

class CalendarModel extends VerySimpleModel {
    static $meta = array(
        'table' => TICKET_CALENDAR_TABLE,
        'pk' => array('id'),
        'joins' => array(
            'ticket' => array(
                'constraint' => array('ticket_id' => 'TicketModel.ticket_id')
            ),
            'created' => array(
                'constraint' => array('created_by' => 'UserModel.id')
            ),
            'updated' => array(
                'constraint' => array('updated_by' => 'UserModel.id')
            )
        )
    );

    function getId() {
        return $this->id;
    }
}

class Calendar extends CalendarModel {
    private $ticket;
    private $id;

    function Calendar($ticket) {
        $this->ticket = $ticket;
        $this->id = 0;
        $this->load();
    }

    function load() {

        if(!$this->getTicketId())
            return null;

        $sql = sprintf(
            "SELECT t.ticket_id as id, count(DISTINCT c.id) as calendar FROM %s t JOIN %s c ON t.ticket_id=c.ticket_id 
                WHERE t.ticket_id=%d GROUP BY t.ticket_id",
            TICKET_TABLE,
            TICKET_CALENDAR_TABLE,
            db_input($this->getTicketId())
        );

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->ht = db_fetch_array($res);
        $this->id = $this->ht['id'];

        return true;
    }

    function getId() {
        return $this->id;
    }

    function getTicketId() {
        return $this->getTicket()?$this->getTicket()->getId():0;
    }

    function getTicket() {
        return $this->ticket;
    }

    static function lookup($ticket) {

        return ($ticket
            && is_object($ticket)
            && ($calendar = new Calendar($ticket))
            && $calendar->getId()
        )?$calendar:null;
    }

    function getEntries() {
        $sql = sprintf(
            "SELECT t.ticket_id, c.* FROM %s t JOIN %s c ON t.ticket_id=c.ticket_id 
                WHERE t.ticket_id=%d ",
            TICKET_TABLE,
            TICKET_CALENDAR_TABLE,
            db_input($this->getTicketId())
        );

        $entries = array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while($row=db_fetch_array($res)) {
                $entries[] = [
                    'id' => $row['id'],
                    'content' => Format::htmlchars($row['title']),
                    'start' => $row['start_date'],
                    'end' => $row['end_date'],
                    'create' => $row['created_by'] && ($_staff = Staff::lookup($row['created_by']))? $_staff->getName() : '',
                ];
            }
        }

        return $entries;
    }

    function getAllEntries($staff_id) {
        $from = db_input(date('Y-m-d H:i:s', time()-3600*24*30));
        $to = db_input(date('Y-m-d H:i:s', time()+3600*24*30));
        $where_date = " AND ( (start_date >= $from AND start_date <= $to) OR (end_date >= $from AND end_date <= $to) ) ";
        $where_staff = " AND created_by = ". db_input($staff_id);
        $where = " $where_date $where_staff ";
        $sql = sprintf(
            "SELECT t.ticket_id, t.number, c.* FROM %s t JOIN %s c ON t.ticket_id=c.ticket_id 
                WHERE 1 $where ",
            TICKET_TABLE,
            TICKET_CALENDAR_TABLE
        );

        $entries = array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while($row=db_fetch_array($res)) {
                $entries[] = [
                    'id' => $row['id'],
                    'content' => ("<a class=\\\"no-pjax\\\" title=\\\"".Format::htmlchars($row['title'])."\\\" target=\\\"_blank\\\" href=\\\"/scp/tickets.php?id=".$row['ticket_id']."\\\">".Format::htmlchars($row['title']).'-'.$row['number']."</a>"),
                    'start' => $row['start_date'],
                    'end' => $row['end_date'],
                    'create' => $row['created_by'] && ($_staff = Staff::lookup($row['created_by']))? $_staff->getName() : '',
                    'tooltip' => Format::htmlchars($row['title']),
                ];
            }
        }

        return $entries;
    }

    function getTodayEntries($staff_id = 0) {
        $sql = sprintf(
            "SELECT t.ticket_id, t.number, s.staff_id, s.email, c.* 
                FROM %s t JOIN %s c ON t.ticket_id=c.ticket_id 
                JOIN %s s ON s.staff_id=t.staff_id
                WHERE DATE(c.start_date)=DATE(NOW()) ",
            TICKET_TABLE,
            TICKET_CALENDAR_TABLE,
            STAFF_TABLE
        );

        if ($staff_id)
            $sql .= " AND s.staff_id=".$staff_id;

        $entries = array();
        if(($res=db_query($sql)) && db_num_rows($res)) {
            while($row=db_fetch_array($res)) {
                if (!isset($entries[$row['staff_id']]))
                    $entries[$row['staff_id']] = [
                        'email' => $row['email'],
                        'list' => [],
                    ];

                $entries[$row['staff_id']]['list'][] = [
                    'id'      => $row['ticket_id'],
                    'content' => Format::htmlchars($row['title']),
                    'email'   => $row['email'],
                    'number'  => $row['number'],
                    'start'   => $row['start_date'],
                    'end'     => $row['end_date'],
                    'create'  => $row['created_by'] && ($_staff = Staff::lookup($row['created_by']))? $_staff->getName() : '',
                ];
            }
        }

        if ($staff_id)
            return isset($entries[$staff_id]) ? $entries[$staff_id] : [];
        return $entries;
    }

    function getEntry($id) {
        return CalendarEntry::lookup($id, $this->getTicketId());
    }
}

class CalendarEntry
{

    var $id;
    var $ht;

    var $staff;
    var $ticket;

    function CalendarEntry($id, $tid=0)
    {
        $this->id = 0;
        $this->load($id, $tid);
        return $this;
    }

    function load($id = 0, $ticketId=0)
    {
//        if (!$id && !($id = $this->getId()))
//            return false;

        $sql = 'SELECT c.*'
            . ' FROM ' . TICKET_CALENDAR_TABLE . ' c WHERE  c.id=' . db_input($id);

        if($ticketId)
            $sql.=' AND c.ticket_id='.db_input($ticketId);

        if (!($res = db_query($sql)) || !db_num_rows($res))
            return false;

        $this->ht = db_fetch_array($res);
        $this->id = $this->ht['id'];

//        $this->staff = new Staff($this->ht['created_by']); // TODO: what the fuck it is?
        $this->ticket = new Ticket($this->ht['ticket_id']);

        return true;
    }

    function getTicketId() {
        return $this->getTicket()?$this->getTicket()->getId():0;
    }

    function getTicket() {
        return $this->ticket;
    }

    function reload()
    {
        return $this->load();
    }

    function getId()
    {
        return $this->id;
    }

    function getTitle()
    {
        return $this->ht['title'];
    }

    function getCreateDate()
    {
        return $this->ht['created_at'];
    }

    function getUpdateDate()
    {
        return $this->ht['updated_at'];
    }

    function lookup($id, $tid=0) {
        return ($id
            && is_numeric($id)
            && ($e = new CalendarEntry($id, $tid))
            && $e->getId()==$id
        )?$e:null;
    }

    function add($vars, &$errors) {
        return self::create($vars, $errors);
    }

    function update($vars, &$errors) {
        global $cfg;
//
//        if (!isset($vars['id']) || !$vars['id'])
//            return false;
//
//        if (!isset($vars['ticket_id']) || !$vars['ticket_id'])
//            return false;
//
//        if (!isset($vars['title']) || !$vars['title'])
//            return false;
//
//        if (!isset($vars['start_date']) || !$vars['start_date'])
//            return false;
//
//        if (!isset($vars['end_date']) || !$vars['end_date'])
//            return false;

        $sql = sprintf(
            'UPDATE %s SET title = %s, start_date = %s, end_date = %s, extra = %s, updated_by = %s, updated_at = NOW() 
                WHERE id = %s  AND created_by = %s  AND ticket_id = %s ',
            TICKET_CALENDAR_TABLE,
            db_input($vars['title']),
            db_input($vars['start_date']),
            db_input($vars['end_date']),
            isset($vars['extra']) && !empty($vars['extra']) ? db_input(json_encode($vars['extra'])) : "''",
            db_input($vars['staff_id']),
            db_input($vars['id']),
            db_input($vars['staff_id']), // only author can update
            db_input($vars['ticket_id'])
        );

        return db_query($sql);
    }

    function delete($vars, &$errors) {
        global $cfg;
//
//        if (!isset($vars['id']) || !$vars['id'])
//            return false;
//
//        if (!isset($vars['ticket_id']) || !$vars['ticket_id'])
//            return false;
//
//        if (!isset($vars['title']) || !$vars['title'])
//            return false;
//
//        if (!isset($vars['start_date']) || !$vars['start_date'])
//            return false;
//
//        if (!isset($vars['end_date']) || !$vars['end_date'])
//            return false;

        $sql = sprintf(
            'DELETE FROM %s WHERE id = %s AND created_by = %s AND ticket_id = %s ',
            TICKET_CALENDAR_TABLE,
            db_input($vars['id']),
            db_input($vars['staff_id']), // only author can delete
            db_input($vars['ticket_id'])
        );

        return db_query($sql);
    }

    function create($vars, &$errors) {
        global $cfg;

//        if (!isset($vars['ticket_id']) || !$vars['ticket_id'])
//            return false;
//
//        if (!isset($vars['title']) || !$vars['title'])
//            return false;
//
//        if (!isset($vars['start_date']) || !$vars['start_date'])
//            return false;
//
//        if (!isset($vars['end_date']) || !$vars['end_date'])
//            return false;

        $sql = sprintf(
            'INSERT INTO %s SET ticket_id = %d, title = %s, start_date = %s, end_date = %s, extra = %s, created_by = %d, created_at = NOW() ',
            TICKET_CALENDAR_TABLE,
            db_input($vars['ticket_id']),
            db_input($vars['title']),
            db_input($vars['start_date']),
            db_input($vars['end_date']),
            isset($vars['extra']) && !empty($vars['extra']) ? db_input($vars['extra']) : "''",
            db_input($vars['staff_id'])
        );

        if (db_query($sql))
            return db_insert_id();
        return false;
    }
}


<?php
require('staff.inc.php');
$nav->setTabActive('dashboard');
$nav->setActiveSubMenu(2);
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.dashboard" />',
    "$('#content').data('tipNamespace', 'dashboard.dashboard');");
require(STAFFINC_DIR.'header.inc.php');
function unparse_url($parsed_url) {
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
    return "$scheme$user$pass$host$port$path$query$fragment";
}
function removeParamUrl($url, $params_key = []){
    $arr_url = parse_url($url);

    if($arr_url['query']) {
        parse_str($arr_url['query'], $url_query);

        foreach ($url_query as $key => $value) {
            if (in_array($key, $params_key))
                unset($url_query[$key]);
        }

        if(!isset($url_query) || empty($url_query))
            unset($arr_url['query']);
        else
            $arr_url['query'] = http_build_query($url_query);

        return unparse_url($arr_url);
    }
    return $url;
}

function getUrlContent($iframe){
    $match = [];
    try{
        if(preg_match('/src="([^"]+)"/', $iframe, $match) === 1)
        {
            if(empty($match)) return null;
            return $match[1];
        }

        // <iframe text ></iframe>
        if(preg_match('/iframe/', $iframe, $match) === 1)
            return null;

    }catch (Exception $ex){
        return null;
    }
    return $iframe;
}

?>
<a class="btn_sm btn-default" href="<?php echo $cfg->getUrl()."scp/listDrive.php"?>">Back</a>

<?php
if (isset($_REQUEST['id'])):
    $object = DynamicListItem::lookup((int)$_REQUEST['id']);

    if(!$object|| $object->getListId() == 0 || $object->status == 0  ) {
        echo 'FILE NOT EXISTS';
        include(STAFFINC_DIR.'footer.inc.php');
        exit();
    }
    $iframe  = $object->getConfiguration();
    $content = getUrlContent($iframe[PROPERTIES2]);
    $content = removeParamUrl($content, ['usp']);
    $content = str_replace(['view','edit'], 'preview', $content);
    ?>
    <style>
        iframe
        {
            height: 550px;
        }

        #container,
        iframe {
            width: 100%;
        }
    </style>
    <h2><?php echo $iframe[PROPERTIES1]?></h2>
    <?php if($content !== null && $content): ?>
        <iframe src="<?php echo $content ?>"></iframe>
    <?php else:
        echo "URL NOT FOUND"; endif; ?>
<?php else:
    echo "FILE NOT FOUND"; endif;?>
<?php
include(STAFFINC_DIR.'footer.inc.php');
?>

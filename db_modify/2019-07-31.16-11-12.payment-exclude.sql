alter table payment_tmp
  add exclude tinyint default 0 not null after outcome_type_id;

create index payment_tmp_exclude_index
  on payment_tmp (exclude);


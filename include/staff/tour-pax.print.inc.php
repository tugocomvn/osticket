<style>
    @media all {
        #nav,
        #sub_nav,
        #info,
        #header p,
        #footer {
            display: none;
            background-color: transparent;
        }

        #header {
            border: none;
        }

        body,
        #header,
        #content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }

        button,
        .btn_sm {
            display: none;
        }

        td {
            color: black;
        }

        tbody td,
        thead th {
            width: auto;
            padding: 0.5em;
        }
    }

    @media screen {
        .btn_print,
        .btn_back {
            display: inline-block;
        }
    }


    .pax_list {
        counter-reset: my-badass-counter;
    }

    .pax_list .stt:before {
        content: counter(my-badass-counter);
        counter-increment: my-badass-counter;
        background: white;
        padding-left: 1em;
    }
</style>

<?php $referer = isset($_SERVER['HTTP_REFERER'])
&& strpos($_SERVER['HTTP_REFERER'], '/scp/tour-list.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/tour-list.php'; ?>

<h2 class="e-print"><?php echo __('Pax List'); ?> | <small>Tour: <?php echo $tour->name ?></small></h2>
<div>
    <div class="pull-right">
    <button class="btn_sm btn-danger btn_print" onclick="window.()"><i class="icon-print"></i> Print</button>
        <a class="no-pjax btn_sm btn-default btn_back" onclick="window.close()">Close</a>
    </div>
    <div class="clearfix"></div>
</div>
<div>
    <div class="pull-left">
        <h3>Tour Info:</h3>
        <?php
        $airlines_list = ListUtil::getList(AIRLINE_LIST);
        $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
        $flight_code = FlightOP::searchFlightCodeFromTour((int)$tour->id);
        $departure_flight_code = '';
        $return_flight_code = '';
        if(count($flight_code) === 4){
            $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
            $return_flight_code = $flight_code[2].'-'.$flight_code[3];
        }elseif(count($flight_code) === 2){
            $departure_flight_code = $flight_code[0];
            $return_flight_code = $flight_code[1];
        } ?>
        <br />
        <?php $airlines_list[$tour->airline_id]?>
        <span>Airline: <?php echo $airlines_list[$tour->airline_id] ?></span>
        <br>
        <span>Departure flight code: <?php echo $departure_flight_code ?> </span>
        <br>
        <span>Return flight code: <?php echo $return_flight_code ?> </span>
        <br>
        <span>Tour leader: <?php if($tour_leader) echo $tour_leader->getValue() ?></span>
    </div>
    <div class="pull-right">
        <h3>Tour Leader info:</h3>
        <?php
        if ($tour->tour_leader_id) {
            $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
            if (isset($tour_leader)
                && $tour_leader
                && isset($tour_leader->value)
                && ($conf = $tour_leader->getConfigurationForm())) {
                foreach ($conf->getFields() as $f) {
                    ?>
                    <div class="field-label tl-info">
                        <label for="<?php echo $f->getWidget()->name; ?>"
                               style="vertical-align:top;padding-top:0.2em"> <?php echo Format::htmlchars($f->get('label')); ?>:</label>
                        <?php $f->render('text'); ?>
                    </div>
                    <?php
                }
            } else {
                ?><strong><em>Chưa chọn TL</em></strong><?php
            }
        } else {
            ?><strong><em>Chưa chọn TL</em></strong><?php
        }
        ?>
         <?php
        $total_pax = PaxTour::countPax($tour_id);
        $total_pax_by_room = PaxTour::countPax($tour_id, true);
        ?>
        <p><strong>Total: <?php echo $total_pax ?></strong> + 1 TL</p>
        <?php foreach($total_pax_by_room as $_room): ?>
            <p><?php echo $_room['room_type'] ?>: <?php echo $_room['total'] ?></p>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<table class="form_table outter_table" width="1058" border="0" cellspacing="0" cellpadding="2">
    <thead>
    <tr>
        <th>STT</th>
        <th>Passport No.</th>
        <th>Full name</th>
        <th>DOB</th>
        <th>Phone Number</th>
        <th>Gender</th>
        <th>Nationality</th>
        <th>Booking Code</th>
        <th>Room</th>
        <th>Note</th>
    </tr>
    </thead>
    <tbody data-repeater-list="group-loyalty" class="pax_list">
    <?php while(($row = db_fetch_array($tour_pax_list))): ?>
        <tr>
            <td class="stt"></td>
            <td>
                <?php echo $row['passport_no'] ?>
            </td>
            <td>
                <?php echo $row['full_name'] ?>
            </td>
            <td><?php if(isset($row['dob']) && strtotime($row['dob']))  echo date('d/m/Y', strtotime($row['dob'])) ?></td>
            <td><?php echo $row['phone_number'] ?></td>
            <td>
                <?php if($row['gender'] == 0) echo "F"; else echo 'M'; ?>
            </td>
            <td><?php if (isset($row['nationality']) && isset($country_list[ $row['nationality'] ])) echo $country_list[ $row['nationality'] ] ?></td>
            <td><?php echo $row['booking_code'] ?></td>
            <td><?php echo $row['room'] ?> - <?php echo $row['room_type'] ?></td>
            <td>
                <?php echo $row['note'] ?>
                <?php if(isset($row['tl']) && $row['tl']): ?>
                    <p>
                        <span class="label label-invert label-small">Ghép với TL</span>
                    </p>
                <?php endif; ?>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$tl_room): ?>
        <tr>
            <td class="stt"></td>
            <td colspan="7">Tour Leader</td>
            <td colspan="2">DBL</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<div>
    <p style="text-align:center;">
        <a class="btn_sm btn-default btn_back" href="<?php echo $referer ?>">Back</a>
    </p>
</div>

<script>
    $(document).ready(function() {
        window.print();
    });
</script>
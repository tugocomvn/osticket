<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Groups</title>
</head>
<body>
<div class="container bg-light" style="width: 80%">
    <?php include_once __DIR__.'/header.php'; ?>
    <h3 class="mt-3 text-center text-info">Danh sách Voucher Group</h3>
    <form  action="groups.list.php" method="get">
        <div class="input-group mb-3 mt-3 ">
            <input type="text" name="code" class="form-control border border-primary" placeholder="tên nhóm"
                   aria-label="Recipient's username" aria-describedby="button-addon2" value=<?php if(isset($code)): echo $code; endif;?>>
            <div class="input-group-append">
                <button class="btn btn btn-info" type="submit" id="check" name="search" value="search">Tìm kiếm</button>
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Tên</th>
            <th scope="col">Campign ID</th>
            <th scope="col">Nội dung</th>
            <th scope="col">Bắt đầu</th>
            <th scope="col">Kết thúc</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php if(isset($groups)): ?>
            <?php foreach($groups as $data):?>
                <tr>
                    <td scope="row"><?php echo ++$offset ?></td>
                    <td><?php echo $data['name'] ?></td>
                    <td><?php echo $data['campaignId'] ?></td>
                    <td><?php echo $data['content'] ?></td>
                    <td><?php echo date('d/m/Y',strtotime($data['date_start'])) ?></td>
                    <td><?php echo date('d/m/Y',strtotime($data['date_end'])) ?></td>
                    <td>
                        <a class="btn btn-primary" href="edit.group.php?id=<?php echo $data['group_id']; ?>">Edit</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<?php $page = new Pagination($config);
echo $page->getPagination(); ?>

</body>

</html>
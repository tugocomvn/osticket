<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canCreatePayments()) die('Access Denied');
?>
<div class="pull-right flush-right" style="padding-right:5px;">
    <b><a href="<?php echo $cfg->getUrl()."scp/report_commission.php?action=create_report"?>" class="Icon newstaff">Tạo báo cáo mới</a></b>
</div>
<h2>Booking Check</h2>
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1058">
    <caption>Booking Check List</caption>
     <thead>
        <tr>
            <th scope="col">Người tạo báo cáo</th>
            <th scope="col">Báo cáo của nhân viên</th>
            <th scope="col">Báo cáo tháng</th>
            <th scope="col">Ngày tạo</th>
            <th scope="col">Ghi chú</th>
            <th scope="col">#</th>
        </tr>
    </thead>
     <tbody>
     <?php $i=1; foreach ($results as $data):
         $link = $cfg->getUrl()."scp/report_commission.php?action=check_booking&id=".$data['id'];
         ?>
         <tr>
             <td><?php echo $allStaff[$data['staff_create_id']]?></td>
             <td><?php echo $allStaff[$data['staff_id_of_report']] ?></td>
             <td><?php echo $data['month_of_report'].'-'.$data['year_of_report'] ?></td>
             <td><?php echo $data['date_create'] ?></td>
             <td><?php echo $data['note'] ?></td>
             <td><a class="btn_sm btn-default btn-xs" href="<?php echo $link?>">View</a></td>
         </tr>
     <?php endforeach; ?>
     </tbody>

</table>

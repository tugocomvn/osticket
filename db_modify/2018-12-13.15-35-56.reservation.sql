CREATE TABLE booking_reservation
(
  id int PRIMARY KEY AUTO_INCREMENT,
  tour_id int,
  staff_id int,
  customer_name varchar(32),
  phone_number varchar(32),
  created_at datetime,
  due_at datetime,
  hold_qty smallint(2),
  sure_qty smallint(2),
  note text,
  security_deposit tinyint(1),
  isoverdue tinyint(1),
  infant smallint(2),
  visa_only tinyint(1),
  fe tinyint(1),
  visa_ready tinyint(1),
  one_way tinyint(1)
);
CREATE INDEX booking_reservation_tour_id_index ON booking_reservation (tour_id);
CREATE INDEX booking_reservation_staff_id_index ON booking_reservation (staff_id);
CREATE INDEX booking_reservation_customer_name_index ON booking_reservation (customer_name);
CREATE INDEX booking_reservation_phone_number_index ON booking_reservation (phone_number);
CREATE INDEX booking_reservation_due_at_index ON booking_reservation (due_at);
CREATE INDEX booking_reservation_isoverdue_index ON booking_reservation (isoverdue);
CREATE INDEX booking_reservation_visa_only_index ON booking_reservation (visa_only);
CREATE INDEX booking_reservation_visa_ready_index ON booking_reservation (visa_ready);
CREATE INDEX booking_reservation_hold_qty_index ON booking_reservation (hold_qty);
CREATE INDEX booking_reservation_sure_qty_index ON booking_reservation (sure_qty);
CREATE INDEX booking_reservation_created_at_index ON booking_reservation (created_at);
CREATE INDEX booking_reservation_fe_index ON booking_reservation (fe);
CREATE INDEX booking_reservation_infant_index ON booking_reservation (infant);
CREATE INDEX booking_reservation_one_way_index ON booking_reservation (one_way);
CREATE INDEX booking_reservation_security_deposit_index ON booking_reservation (security_deposit);

CREATE TABLE booking_reservation_history
(
  id int PRIMARY KEY AUTO_INCREMENT,
  booking_reservation_id int,
  tour_id int,
  staff_id int,
  customer_name varchar(32),
  phone_number varchar(32),
  created_at datetime,
  due_at datetime,
  hold_qty smallint(2),
  sure_qty smallint(2),
  note text,
  security_deposit tinyint(1),
  isoverdue tinyint(1),
  infant smallint(2),
  visa_only tinyint(1),
  fe tinyint(1),
  visa_ready tinyint(1),
  one_way tinyint(1)
);
CREATE INDEX booking_reservation_booking_reservation_id_index ON booking_reservation_history (booking_reservation_id);
CREATE INDEX booking_reservation_tour_id_index ON booking_reservation_history (tour_id);
CREATE INDEX booking_reservation_staff_id_index ON booking_reservation_history (staff_id);
CREATE INDEX booking_reservation_customer_name_index ON booking_reservation_history (customer_name);
CREATE INDEX booking_reservation_phone_number_index ON booking_reservation_history (phone_number);
CREATE INDEX booking_reservation_due_at_index ON booking_reservation_history (due_at);
CREATE INDEX booking_reservation_isoverdue_index ON booking_reservation_history (isoverdue);
CREATE INDEX booking_reservation_visa_only_index ON booking_reservation_history (visa_only);
CREATE INDEX booking_reservation_visa_ready_index ON booking_reservation_history (visa_ready);
CREATE INDEX booking_reservation_hold_qty_index ON booking_reservation_history (hold_qty);
CREATE INDEX booking_reservation_sure_qty_index ON booking_reservation_history (sure_qty);
CREATE INDEX booking_reservation_created_at_index ON booking_reservation_history (created_at);
CREATE INDEX booking_reservation_fe_index ON booking_reservation_history (fe);
CREATE INDEX booking_reservation_infant_index ON booking_reservation_history (infant);
CREATE INDEX booking_reservation_one_way_index ON booking_reservation_history (one_way);
CREATE INDEX booking_reservation_security_deposit_index ON booking_reservation_history (security_deposit);

ALTER TABLE ost_tour ALTER COLUMN quantity SET DEFAULT 0;
ALTER TABLE ost_tour ALTER COLUMN hold_quantity SET DEFAULT 0;
ALTER TABLE ost_tour ALTER COLUMN sure_quantity SET DEFAULT 0;


ALTER TABLE booking_reservation CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;
ALTER TABLE booking_reservation_history CONVERT TO CHARACTER SET utf8 COLLATE  utf8_unicode_ci;

ALTER TABLE booking_reservation ADD booking_code varchar(16) NULL;
CREATE INDEX booking_reservation_booking_code_index ON booking_reservation (booking_code);

ALTER TABLE booking_reservation_history ADD booking_code varchar(16) NULL;
CREATE INDEX booking_reservation_history_booking_code_index ON booking_reservation_history (booking_code);

ALTER TABLE booking_reservation ADD created_by int NULL;
CREATE INDEX booking_reservation_created_by_index ON booking_reservation (created_by);
ALTER TABLE booking_reservation ADD updated_by int NULL;
CREATE INDEX booking_reservation_updated_by_index ON booking_reservation (updated_by);

ALTER TABLE booking_reservation_history ADD created_by int NULL;
CREATE INDEX booking_reservation_history_created_by_index ON booking_reservation_history (created_by);

ALTER TABLE booking_reservation ADD country int NULL;
CREATE INDEX booking_reservation_country_index ON booking_reservation (country);


ALTER TABLE booking_reservation_history ADD country int NULL;
CREATE INDEX booking_reservation_history_country_index ON booking_reservation_history (country);

alter table booking_reservation
  add status tinyint null;

create index booking_reservation_status_index
  on booking_reservation (status);

alter table booking_reservation
  add updated_at datetime null;

create index booking_reservation_updated_at_index
  on booking_reservation (updated_at);

alter table booking_reservation
  add leader_review_by int null;

alter table booking_reservation
  add leader_review_at datetime null;

alter table booking_reservation
  add op_review_by int null;

alter table booking_reservation
  add op_review_at datetime null;

create index booking_reservation_leader_review_at_index
  on booking_reservation (leader_review_at);

create index booking_reservation_leader_review_by_index
  on booking_reservation (leader_review_by);

create index booking_reservation_op_review_at_index
  on booking_reservation (op_review_at);

create index booking_reservation_op_review_by_index
  on booking_reservation (op_review_by);


alter table booking_reservation_history
  add leader_review_by int null;

alter table booking_reservation_history
  add leader_review_at datetime null;

alter table booking_reservation_history
  add op_review_by int null;

alter table booking_reservation_history
  add op_review_at datetime null;

create index booking_reservation_history_leader_review_at_index
  on booking_reservation_history (leader_review_at);

create index booking_reservation_history_leader_review_by_index
  on booking_reservation_history (leader_review_by);

create index booking_reservation_history_op_review_at_index
  on booking_reservation_history (op_review_at);

create index booking_reservation_history_op_review_by_index
  on booking_reservation_history (op_review_by);

alter table booking_reservation
  add booking_code_trim varchar(10) null;

create index booking_reservation_booking_code_trim_index
  on booking_reservation (booking_code_trim);


<?php
if(!defined('OSTSCPINC') || !$thisstaff) die('Invalid path');

if(!$thisstaff->canManageVisaItems()) die('Access Denied');
?>

<form action="<?php echo $url ?>" method="post">
    <?php csrf_token(); ?>

    <?php if(isset($error) && $error): ?>
        <div id="msg_error"><?php echo $mess ?></div>
    <?php endif;?>

    <input type="hidden" name="id" value="<?php  echo $_REQUEST['id']; ?>">
    <input type="hidden" name="action" value="create">

    <a class="btn_sm btn-default" href="<?php echo $url;?>">Back</a>
    <h2 class="centered"><?php echo $title_create ?></h2>

    <table style="margin-left: 25%; margin-top: 10px">
        <tr class="required">
            <td width="150">Tên<span class="error">*&nbsp;</span></td>
            <td >
                <input class="input-field" type="text" size="28" name="name" value="<?php echo $result->name ?>" required>

            </td>
        </tr>
        <tr class="required">
            <td width="150">Mô tả danh mục</td>
            <td>
                <textarea class="input-field" style=" resize: none;" name="description"  cols="30" rows="4" ></textarea>

            </td>
        </tr>
    </table>
    <p class="centered">
        <a class="btn_sm btn-default" href="<?php echo $url;?>">Back</a>
        <button class="btn_sm btn-primary" name="create" value="create">Save</button>
    </p>
</form>

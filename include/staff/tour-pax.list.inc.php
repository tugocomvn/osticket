<style>
    .pax_list {
        counter-reset: my-badass-counter;
    }

    .pax_list .stt:before {
        content: counter(my-badass-counter) '.';
        counter-increment: my-badass-counter;
        position: absolute;
        padding-left: 0.5em;
        margin-right: 1em;
        font-style: italic;
    }

    .tl-info {
        font-size: 13pt;
        color: red;
    }

    .label-female {
        background: deeppink;
    }

    .label-passport {
        background: #efefef !important;
        color: black !important;
        font-weight: normal;
        text-shadow: none;
    }

    tbody td, thead th {
        width: auto;
    }

    tbody tr:nth-child(2n) td {
        background-color: #fff7ff;
    }

    tbody tr td {
        padding-top: 0.5em;
        padding-bottom: 0.5em;
    }
</style>

<h2><?php echo __('Pax List'); ?> | <small>Tour: <?php echo $tour->name ?></small></h2>
<?php $referer = isset($_SESSION['op_referer']) && $_SESSION['op_referer'] ? $_SESSION['op_referer'] : (isset($_SERVER['HTTP_REFERER'])
        && strpos($_SERVER['HTTP_REFERER'], '/scp/tour-list.php') !== false
    ? $_SERVER['HTTP_REFERER'] : '/scp/tour-list.php'); ?>

<div>
    <div class="pull-left">
        <h3>Tour Info:</h3>
        <?php
        $airlines_list = ListUtil::getList(AIRLINE_LIST);
        $tour_leader = DynamicListItem::lookup((int)$tour->tour_leader_id);
        $flight_code = FlightOP::searchFlightCodeFromTour((int)$tour->id);
        $departure_flight_code = '';
        $return_flight_code = '';
        if(count($flight_code) === 4){
            $departure_flight_code = $flight_code[0].'-'.$flight_code[1];
            $return_flight_code = $flight_code[2].'-'.$flight_code[3];
        }elseif(count($flight_code) === 2){
            $departure_flight_code = $flight_code[0];
            $return_flight_code = $flight_code[1];
        } ?>
        <br />
        <?php $airlines_list[$tour->airline_id]?>
        <span>Airline: <?php echo $airlines_list[$tour->airline_id] ?></span>
        <br>
        <span>Departure flight code: <?php echo $departure_flight_code ?> </span>
        <br>
        <span>Return flight code: <?php echo $return_flight_code ?> </span>
        <br>
        <span>Tour leader: <?php if($tour_leader) echo $tour_leader->getValue() ?></span>
    </div>

    <div class="pull-right">
        <?php
        $total_pax = PaxTour::countPax($tour_id);
        $total_pax_by_room = PaxTour::countPax($tour_id, true);
        ?>
        <p><strong>Total: <?php echo $total_pax ?></strong> + 1 TL</p>
        <?php foreach($total_pax_by_room as $_room): ?>
            <p><?php echo $_room['room_type'] ?>: <?php echo $_room['total'] ?></p>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
</div>

<div>
    <p style="text-align:center;">
        <a class="btn_sm btn-default no-pjax" href="<?php echo $cfg->getUrl() ?>scp/tour-list.php">Back</a> <a href="/scp/tour-guest.php?tour=<?php echo $tour_id ?>" class="no-pjax btn_sm btn-success">Add/Edit Pax</a>
    </p>
</div>
<table class="form_table outter_table" width="1058" border="0" cellspacing="0" cellpadding="2">
    <thead>
    <tr>
        <th>STT</th>
        <th>Passport No./DOE</th>
        <th>Full name</th>
        <th>Phone Number</th>
        <th>DOB/ Nationality</th>
        <th>Booking Code/Sale</th>
        <th>Room</th>
        <th>Receiving Date</th>
        <th>Visa Apply</th>
        <th>Visa Estimated Due</th>
        <th style="max-width: 50px;">Note</th>
    </tr>
    </thead>
    <tbody data-repeater-list="group-loyalty" class="pax_list">
    <?php while(($row = db_fetch_array($tour_pax_list))): ?>
        <tr>
            <td class="stt"></td>
            <td>
                <?php
                $path = null;
                $passport_photo = \Tugo\PassportPhotoUpload::lookup(['passport_no' => trim($row['passport_no'])]);
                if($passport_photo) //load file name in table passport_photo_upload
                    $path = $passport_photo->filename;

                if (!$path) {
                    $pax_passport_photo = \Tugo\PassportPhoto::getFileNamePassport(null, trim($row['passport_no']),  1);
                    if ($pax_passport_photo) {
                        $pax_passport_photo = db_fetch_array($pax_passport_photo);
                        if($pax_passport_photo && $pax_passport_photo['filename']){
                            //load file name in table pax_passport_photo
                            $path = $pax_passport_photo['filename'];
                        }
                    }
                }
                ?>
                    <?php if($path): ?>
                        <span class="label label-passport">
                        <a href="<?php echo $cfg->getUrl() ?>passport_upload/<?php echo $path ?>" target="_blank" class="no-pjax">
                            <strong><?php echo $row['passport_no'] ?></strong>
                        </a>
                        </span>

                        <a class="no-pjax" href="<?php echo $cfg->getUrl() ?>scp/download_passport.php?path=<?php
                            echo $path ?>&name=<?php
                            echo $row['passport_no'] ?>">
                            <i class="icon-download"></i>
                        </a>
                    <?php else: ?>
                        <?php echo $row['passport_no'] ?>
                    <?php endif; ?>
                <br>
                <?php if(strtotime($row['doe']) <= time()+3600*24*30*6): ?>
                    <span class="label label-small label-danger">
                    <strong>
                        <?php if(isset($row['doe']) && strtotime($row['doe']))  echo date('d/m/Y', strtotime($row['doe'])) ?>
                    </strong>
                    </span>
                <?php else: ?>
                    <small><?php if(isset($row['doe']) && strtotime($row['doe']))  echo date('d/m/Y', strtotime($row['doe'])) ?></small>
                <?php endif; ?>
            </td>
            <td>

                <?php if($row['gender'] == 0): ?>
                    <span class="label label-female"><i class="icon-female"></i></span>
                <?php else: ?>
                    <span class="label label-primary"><i class="icon-male"></i></span>
                <?php endif; ?>
                <?php echo $row['full_name'] ?>
            </td>
            <td><?php echo $row['phone_number'] ?></td>
            <td>
                <?php if(isset($row['dob']) && strtotime($row['dob']))  echo date('d/m/Y', strtotime($row['dob'])) ?>
                <br>
                <?php if('VN' !== $row['nationality']): ?>
                    <span class="label label-small label-warning">
                    <?php if (isset($row['nationality']) && isset($country_list[ $row['nationality'] ])) echo $country_list[ $row['nationality'] ] ?>
                    </span>
                <?php else: ?>
                    <small><?php if (isset($row['nationality']) && isset($country_list[ $row['nationality'] ])) echo $country_list[ $row['nationality'] ] ?></small>
                <?php endif; ?>
            </td>
            <td>
                <a class="no-pjax" target="_blank" href="<?php echo $cfg->getUrl();?>scp/new_booking.php?id=<?php echo $row['ticket_id'] ?>"><?php echo $row['booking_code'] ?></a>
                <br>
                <small><?php echo $row['staff'] ?></small>
            </td>
            <td><?php echo $row['room'] ?> - <?php echo $row['room_type'] ?></td>
            <td><?php if(isset($row['receiving_date']) && strtotime($row['receiving_date']) && $row['receiving_date'] !== '0000-00-00 00:00:00')
                echo date('d/m/Y', strtotime($row['receiving_date'])) ?></td>
            <td><?php if(isset($row['apply_date']) && strtotime($row['apply_date']) && $row['apply_date'] !== '0000-00-00 00:00:00')
                echo date('d/m/Y', strtotime($row['apply_date'])) ?></td>
            <td>
                <?php if(isset($row['visa_estimated_due_date']) && strtotime($row['visa_estimated_due_date']) && $row['visa_estimated_due_date'] !== '0000-00-00 00:00:00')
                    echo date('d/m/Y', strtotime($row['visa_estimated_due_date'])) ?>
                <br>
                <?php if (isset($row['visa_result'])): ?>
                    <?php if ($row['visa_result'] == 0): ?>
                        <span class="label label-small label-default">N/A</span>
                    <?php endif; ?>

                    <?php if ($row['visa_result'] == 1): ?>
                        <span class="label label-small label-primary">Visa <i class="icon-check"></i></span>
                    <?php endif; ?>

                    <?php if ($row['visa_result'] == 2): ?>
                        <span class="label label-small label-danger">Visa <i class="icon-remove"></i></span>
                    <?php endif; ?>

                    <?php if ($row['visa_result'] == 3): ?>
                        <span class="label label-small label-inverse">Visa <i class="icon-mail-forward"></i></span>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td>
                <?php echo $row['note'] ?>
                <?php if(isset($row['tl']) && $row['tl']): ?>
                    <p>
                        <span class="label label-invert label-small">Ghép với TL</span>
                    </p>
                <?php endif; ?>
            </td>
        </tr>
    <?php endwhile; ?>
    <?php if(!$tl_room): ?>
    <tr>
        <td class="stt"></td>
        <td colspan="5">Tour Leader</td>
        <td colspan="5">DBL</td>
    </tr>
    <?php endif; ?>
    </tbody>
</table>
<div>
    <p style="text-align:center;">
        <a class="btn_sm btn-default no-pjax" href="<?php echo $referer ?>">Back</a> <a href="/scp/tour-guest.php?tour=<?php echo $tour_id ?>" class="no-pjax btn_sm btn-success">Add/Edit Pax</a>
    </p>
</div>

<div>
    <hr>
    <h3>Tour Leader info:</h3>
    <?php
    if ($tour->tour_leader_id) {
        $tour_leader = DynamicListItem::lookup($tour->tour_leader_id);
        if (isset($tour_leader)
            && $tour_leader
            && isset($tour_leader->value)
            && ($conf = $tour_leader->getConfigurationForm())) {
            foreach ($conf->getFields() as $f) {
                ?>
                <div class="field-label tl-info">
                    <label for="<?php echo $f->getWidget()->name; ?>"
                           style="vertical-align:top;padding-top:0.2em"> <?php echo Format::htmlchars($f->get('label')); ?>:</label>
                    <?php $f->render('text'); ?>
                </div>
                <?php
            }
        } else {
            ?><strong><em>Chưa chọn TL</em></strong><?php
        }
    } else {
        ?><strong><em>Chưa chọn TL</em></strong><?php
    }
    ?>
</div>

<div class="row">
    <div class="col-lg-6">
        <?php include STAFFINC_DIR.'bi.daterangepicker.widget.php' ?>
    </div>
    <div class="col-lg-3">
        <?php include STAFFINC_DIR.'bi.metric.widget.php' ?>
    </div>
    <div class="col-lg-3">
        <?php include STAFFINC_DIR.'bi.mode.widget.php' ?>
    </div>
</div>

<div class="row">
    <div class="col">
        <?php include STAFFINC_DIR.'bi.tickets.overview.chart.php' ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col">
        <?php include STAFFINC_DIR.'bi.tickets.overview.table.php' ?>
    </div>
</div>
<!-- /.row -->
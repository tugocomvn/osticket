<?php
require('staff.inc.php');
if(!$thisstaff->canViewBookings()) die('Access Denied');

include_once INCLUDE_DIR.'class.booking.php';
include_once INCLUDE_DIR.'class.pax.php';
include_once INCLUDE_DIR.'class.tour-market.php';
include_once INCLUDE_DIR.'class.tour-destination.php';
include_once INCLUDE_DIR.'class.pax.php';
session_start();
global $thisstaff, $cfg;
define('SCRIPT_ERROR', '
<script>
            ost_set_parent_iframe_height();
            parent.$(\'.submit_btn\').prop(\'disabled\', false);
        </script>
');
$page_body ='reservation.inc.php';
$users = Staff::getSaleOnly();
function get_AllMemberStaff(){
    global $thisstaff;
    $arr_team = $thisstaff->getTeams();
    $staff_member = [];
    foreach ($arr_team as $value)
    {
        $id_team = (int)$value;
        $team = new Team($id_team);

        //get member in teams
        if($thisstaff->getId() === $team->getLeadId()){
            $arr_staff = $team->getIdMembers();
            foreach ($arr_staff as $id_staff) {
                if(!isset($staff_member[$id_staff]))
                    $staff_member[$id_staff] = 1;
            }
        }
    }
    return $staff_member;
}

function ___hold($data, &$result) {
    global $thisstaff;
    if(!$thisstaff->canHoldReservation())
        $result['permission'] = 'Không thể thực hiện hành động này';

    if(isset($data['staff_id']) && (int)$data['staff_id'] === 0)
        $result['staff'] = 'Bắt buộc chọn nhân viên giữ chỗ';

    $visa_only = isset($data['visa_only']) && $data['visa_only'];
    if($visa_only){
        if(!isset($data['booking_code']) || !trim($data['booking_code'])){
            $result['booking_code'] = 'Mã booking code không được để trống';
        }else{
            $booking_check = Booking::lookup(['booking_code' => trim($data['booking_code'])]);
            if (!$booking_check)
                $result['booking_code'] = 'Mã booking không tồn tại';
        }
    }

    if (!$visa_only) {
        $tour = TourNew::lookup(['id' => $_POST['tour_id']]);
        if (!$tour) {
            $result['tour'] = 'Tour không tồn tại';
        }

        if (!isset($tour->pax_quantity) || !(int)$tour->pax_quantity) {
            $result['tour_quantity'] = 'Tour chưa cài số lượng khách tối đa. Liên hệ leader.';
        }
        if(BookingReservation::countOverHold($tour->id) > 0)
        {
            $result['tour'] = "Không giữ chỗ 1 tour OVER quá 1 lần!";
        }



        $data['hold_qty'] = isset($data['hold_qty']) ? (int)$data['hold_qty'] : 0;
        $data['sure_qty'] = isset($data['sure_qty']) ? (int)$data['sure_qty'] : 0;
        if ( !$data['hold_qty'] && !$data['sure_qty'] ) {
            $result['quantity'] = 'Số chỗ giữ hoặc số chỗ Sure phải lớn hơn 0';
        } else {
            if($data['hold_qty'] && ($data['hold_qty'] > $tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity - $data['sure_qty'])){
                $data['note'] = $data['note'] ." OVER";
                $data['over'] = 1;
            }
            if($data['sure_qty'] && !(($tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity > 0)
                && ($data['sure_qty'] <= $tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity))){
                $result['quantity'] = "Số lượng chỗ sure không hợp lệ, phải là số và nhỏ hơn số khách còn trống của đoàn";
            }
        }
        $destination = TourDestination::lookup($tour->destination);
        if ($destination) {
            $country = $destination->tour_market_id;
            if ($country) {
                $data['country'] = $country;
            }
        }

    } elseif (!isset($data['country']) || !$data['country']) {
        $result['country'] = 'Country không được để trống';
    }

    if (!isset($data['customer_name']) || empty(trim($data['customer_name']))) {
        $result['customer_name'] = 'Customer Name không được để trống';
    }
    if (!isset($data['phone_number']) || empty(trim($data['phone_number']))) {
        $result['phone_number'] = 'Phone number không được để trống';
    }

    $all_sure = false;
    if (!isset($data['due']) || !( $due = date_create_from_format('d/m/Y', $data['due']) )) {
        $result['due'] = 'Due date không hợp lệ';

    } else {
        if ((int)trim($data['sure_qty']) && !(int)trim($data['hold_qty'])) {
            $all_sure = true;
        } else {
            $Utime = $due->format('U');
            if ($Utime <= time())
                $result['due'] = 'Ngày hết hạn phải lớn hơn ngày hiện tại';
            if ($Utime - time() > 3600*24*3)
                $result['due'] = 'Ngày hết hạn không xa quá 3 ngày';
        }
    }

    $data['phone_number'] = str_replace([',', '.', ' '], '', $data['phone_number']);

    if (preg_match('/[^0-9]/', $data['phone_number'])) {
        $result['phone_number'] = 'Số điện thoại phải là số';
    }
    //check booking when Create
    if((int)$data['sure_qty'] > 0 || (isset($data['booking_code']) && !empty(trim($data['booking_code'])))) {
        if(!trim($data['booking_code']))
            $result['booking_code'] = 'Chưa có mã booking';
        else {
            $booking_check = Booking::lookup(['booking_code' => trim($data['booking_code'])]);
            if (!$booking_check) {
                $result['booking_code'] = 'Mã booking không tồn tại';
            }
        }
    }

    $_data = [
        'tour_id' => $visa_only ? 0 : $data['tour_id'],
        'staff_id' => $data['staff_id'],
        'country' => (int)trim($data['country']),
        'customer_name' => trim($data['customer_name']),
        'booking_code' => trim($data['booking_code']),
        'phone_number' => $data['phone_number'],
        'infant' => (int)trim($data['infant']),
        'sure_qty' => (int)trim($data['sure_qty']),
        'hold_qty' => (int)trim($data['hold_qty']),
        'due_at' => $all_sure ? null : $due->format('Y-m-d').' '.date('H:i:s'),
        'created_at' => new SqlFunction('NOW'),
        'note' => trim($data['note']),
        'fe' => $data['fe'] ? 1 : 0,
        'visa_only' => $data['visa_only'] ? 1 : 0,
        'visa_ready' => $data['visa_ready'] ? 1 : 0,
        'security_deposit' => $data['security_deposit'] ? 1 : 0,
        'one_way' => $data['one_way'] ? 1 : 0,
        'over' => isset($data['over']) ? 1: 0,
    ];

    if ($result) return false;

    if (!($id = BookingReservation::hold($_data)))
        $result['save'] = 'Cannot save';

    if ($result) return false;
    if(isset($data['over']) && $data['over'] === 1)
        echo "<div id=\"msg_warning\">Cảnh báo: Giữ chỗ quá số lượng còn trống của tour.</div>";
    $result['success'] = 1;
    return $id;
}

function ___cancel($data, &$result, $type = 'all') {
    global $thisstaff;
    $permission_cancel = false;
    $reservation = BookingReservation::lookup(['id' => (int)$data['id']]);
    $staff_member = get_AllMemberStaff();
    //check permission cancel
    if($reservation && $reservation->staff_id && $staff_member[$reservation->staff_id]
                    && $thisstaff->canCancelLeaderReservation()) //leader of staff owner
        $permission_cancel = true;
    if($thisstaff->canCancelMemberReservation() || $thisstaff->canCancelSuperReservation())
        $permission_cancel = true;

    if($permission_cancel) {
        if (!BookingReservation::cancel($data, $type))
            $result['save'] = 'Cannot cancel';
    }else
        $result['permission'] = 'Không thể thực hiện hành động này';

    if ($result) return false;

    $result['success'] = 1;
    return true;
}

function ___update($data, &$result) {
    global $thisstaff;
    if(!$thisstaff->canHoldReservation())
        $result['permission'] = 'Không thể thực hiện hành động này';

    if(isset($data['staff_id']) && (int)$data['staff_id'] === 0)
        $result['staff'] = 'Bắt buộc chọn nhân viên giữ chỗ';

    $visa_only = isset($data['visa_only']) && $data['visa_only'];
    if($visa_only){
        if(!isset($data['booking_code']) || !trim($data['booking_code'])){
            $result['booking_code'] = 'Mã booking code không được để trống';
        }else{
            $booking_check = Booking::lookup(['booking_code' => trim($data['booking_code'])]);
            if (!$booking_check)
                $result['booking_code'] = 'Mã booking không tồn tại';
        }
    }
    $cancel = false;

    if ((!isset($data['hold_qty']) || !(int)$data['hold_qty'])
        && (!isset($data['sure_qty']) || !(int)$data['sure_qty'])
    ) {
        $cancel = true;
    }

    if (!$visa_only) {
        $tour = TourNew::lookup(['id' => $data['tour_id']]);
        if (!$tour) {
            $result['tour'] = 'Tour không tồn tại';
        }

        $reservation = BookingReservation::lookup(['id' => $data['reservation_id']]);
        if (!$reservation) {
            $result['reservation'] = 'Giữ chỗ không tồn tại';
        }

        if (!isset($tour->pax_quantity) || !(int)$tour->pax_quantity) {
            $result['tour_quantity'] = 'Tour chưa cài số lượng khách tối đa. Liên hệ leader.';
        }


        if($data['hold_qty'] > $reservation->hold_qty || $data['sure_qty'] > $reservation->sure_qty) {
            if (BookingReservation::countOverHold($tour->id) > 0) {
                if (!$reservation->over) {
                    $result['tour'] = "Không giữ chỗ 1 tour OVER quá 1 lần!";
                }
            }

            if (!$cancel && ($reservation->hold_qty != $data['hold_qty']) && $data['hold_qty'] - $reservation->hold_qty > $tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity) {
                $data['note'] = $data['note'] . " OVER";
                $data['over'] = 1;
            }

            if (!$cancel && $reservation->sure_qty != $data['sure_qty'] && $data['sure_qty']
                && !(($tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity > 0)
                    && ((int)$data['sure_qty'] <= $tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity))) {
                $result['quantity'] = "Số lượng chỗ sure không hợp lệ, phải là số và nhỏ hơn số khách còn trống của đoàn";
            }
        }
        $destination = TourDestination::lookup($tour->destination);
        if ($destination) {
            $country = $destination->tour_market_id;
            if ($country) {
                $data['country'] = $country;
            }
        }

    } elseif (!isset($data['country']) || !(int)trim($data['country'])) {
        $result['country'] = 'Nơi đến không được để trống';
    }

    //check booking when Edit
    if($data['booking_code'] && (int)$reservation->status !== 0 && trim($data['booking_code']) != $reservation->booking_code)
        $result['update'] = 'Không được chỉnh sửa booking code';

    if((int)$data['sure_qty'] > 0 || (isset($data['booking_code']) && !empty(trim($data['booking_code'])))) {
        if(!trim($data['booking_code']))
            $result['booking_code'] = 'Chưa có mã booking';
        else {
            $booking_check = Booking::lookup(['booking_code' => trim($data['booking_code'])]);
            if (!$booking_check) {
                $result['booking_code'] = 'Mã booking không tồn tại';
            }
        }
    }
    if (!isset($data['customer_name']) || empty(trim($data['customer_name']))) {
        $result['customer_name'] = 'Tên khách không được để trống';
    }
    if (!isset($data['phone_number']) || empty(trim($data['phone_number']))) {
        $result['phone_number'] = 'Số điện thoại khách không được để trống';
    }

    $data['phone_number'] = str_replace([',', '.', ' '], '', $data['phone_number']);

    if (preg_match('/[^0-9]/', $data['phone_number'])) {
        $result['phone_number'] = 'Số điện thoại phải là số';
    }
    $all_sure = false;
    if (!$cancel) {
        if ((int)trim($data['sure_qty']) && !(int)trim($data['hold_qty'])) {
            $all_sure = true;
            $data['due'] = null;

        } elseif (!isset($data['due']) || empty($data['due']) || !$data['due'] || !( $due = date_create_from_format('d/m/Y', $data['due']) )) {
            $result['due'] = 'Ngày hết hạn không hợp lệ';

        } else {
            $Utime = $due->format('U');
            if ($Utime <= time())
                $result['due'] = 'Ngày hết hạn phải lớn hơn ngày hiện tại';
            if ($Utime - time() > 3600*24*3)
                $result['due'] = 'Ngày hết hạn không xa quá 3 ngày';
        }
    }

    if ($result) return false;

    $_data = [
        'id' => $data['reservation_id'],
        'tour_id' => $visa_only ? 0 : $data['tour_id'],
        'staff_id' => $data['staff_id'],
        'country' => (int)trim($data['country']),
        'booking_code' => trim($data['booking_code']),
        'customer_name' => trim($data['customer_name']),
        'phone_number' => str_replace([',', '.', ' '], '', $data['phone_number']),
        'infant' => (int)trim($data['infant']),
        'sure_qty' => (int)trim($data['sure_qty']),
        'hold_qty' => (int)trim($data['hold_qty']),
        'due_at' => $cancel || $all_sure || !$due ? null : $due->format('Y-m-d').' '.date('H:i:s'),
        'note' => trim($data['note']),
        'fe' => $data['fe'] ? 1 : 0,
        'visa_only' => $data['visa_only'] ? 1 : 0,
        'visa_ready' => $data['visa_ready'] ? 1 : 0,
        'security_deposit' => $data['security_deposit'] ? 1 : 0,
        'one_way' => $data['one_way'] ? 1 : 0,
        'over' => isset($data['over']) ? 1: 0,
    ];

    if ($cancel) {
        return ___cancel($_data, $result);
    }

    if ($result) return false;

    if (!($id = BookingReservation::update($_data)))
        $result['save'] = 'Cannot save';

    if ($result) return false;

    if(isset($data['over']) && $data['over'] === 1)
        echo "<div id=\"msg_warning\">Cảnh báo: Giữ chỗ quá số lượng còn trống của tour.</div>";
    $result['success'] = 1;
    return $id;
}

function ___clear() {
    BookingReservation::setOverdue();
    BookingReservation::deleteOverdue();
}

function __updateSure($data, &$result){
    $tour = TourNew::lookup(['id' => (int)$data['tour_id']]);
    if (!$tour) {
        $result['tour'] = 'Tour không tồn tại';
    }

    $reservation = BookingReservation::lookup(['id' => (int)$data['reservation_id']]);
    if (!$reservation) {
        $result['reservation'] = 'Giữ chỗ không tồn tại';
        return false;
    }
    //check booking
    if(isset($reservation->booking_code) && !empty(trim($reservation->booking_code))) {
        $booking_check = Booking::lookup(['booking_code' => trim($reservation->booking_code)]);
        if (!$booking_check)
            $result['booking_code'] = 'Mã booking không tồn tại';
    } else
        $result['booking_code'] = 'Chưa có mã booking';

    if(!$reservation->hold_qty) {
        $result['hold_qty'] = 'Giữ chỗ đã sure all';
    }
    $data = $reservation->ht;

    if(isset($reservation->hold_qty) && $reservation->hold_qty &&
        !($tour->pax_quantity - $tour->sure_quantity - $tour->hold_quantity + $reservation->hold_qty >= 0)){
        $result['sure_qty'] = "Số lượng chỗ sure không hợp lệ, phải là số và nhỏ hơn số khách còn trống của đoàn";
    }
    $data['sure_qty'] = $reservation->hold_qty + $reservation->sure_qty;
    $data['hold_qty'] = 0;
    if (!($id = BookingReservation::update($data)))
        $result['save'] = 'Cannot save';

    if ($result) return false;

    $result['success'] = 1;
    return $id;
}

function __extend($id, &$result){
    $reservation = BookingReservation::lookup(['id' => $id]);
    if (!$reservation) {
        $result['reservation'] = 'Giữ chỗ không tồn tại';
        return false;
    }
    $data = $reservation->ht;

    //MAX_EXTEND_RESERVATION(day)
    if(isset($data['due_at']) && $data['due_at'] && (int)$data['extend_due'] < MAX_EXTEND_RESERVATION){
        if(strtotime($data['due_at']) <= time())
            $data['due_at'] = date('Y-m-d H:i:s', (time() + MAX_TIME_EXTEND_RESERVATION*3600) ); //extend hour
        else
            $data['due_at'] = date('Y-m-d H:i:s', (strtotime($data['due_at']) + MAX_TIME_EXTEND_RESERVATION*3600) ); //extend hour

        $data['extend_due'] = (int)$data['extend_due'] + 1;
    }else{
        $result['due_at'] = "Gia hạn không thành công. Bạn chỉ được gia hạn tối đa 02 lần.";
        $result['due_date'] = $data['due_at'];
        return false;
    }

    if (!BookingReservation::update($data))
        $result['save'] = 'Cannot save';

    if ($result) return false;

    $result['success'] = 1;
    return $id;
}

function __cancelTour($data, &$result){
    global $thisstaff;
    if(!$thisstaff->canCancelTour()){
        $result['permission'] = 'Không thể thực hiện hành động này';
        return false;
    }

    $tour_id = (int)$data['tour_id'];
    $tour = TourNew::lookup(['id' => $tour_id]);
    if (!$tour) {
        $result['tour'] = 'Tour không tồn tại';
        return false;
    }

    $tour->setAll([
        'status' => 0,
    ]);
    $tour->save();

    $result['success'] = 1;
    return true;
}

function __transferDate($reservation_id, $tour_transfer, &$result){
    $reservation = BookingReservation::lookup(['id' => (int)$reservation_id]);

    if (!$reservation) {
        $result['reservation'] = 'Giữ chỗ không tồn tại';
        return false;
    }
    $data = $reservation->ht;
    $data['tour_id'] = (int)$tour_transfer;
    $data['due'] = date('d/m/Y', strtotime($data['due_at']));
    unset($data['over']);

    $_POST['tour_id'] = (int)$tour_transfer;
    //create new reservation
    $id_new = ___hold($data,$result);

    $data['id'] = (int)$reservation_id;
    $data['reason'] = 'transfer date (tour)';
    //cancel tour have extend day
    ___cancel($data,$result);
    if (!isset($result['success']) || !$result['success']) return false;

    $result['success'] = 1;
    return $id_new;
}

function itinerary($itinerary_a, $itinerary_b, $itinerary_c, $itinerary_d){
    if(empty($itinerary_a) || empty($itinerary_b))
        return '';

    if(empty($itinerary_c) || empty($itinerary_d) ){
        // khong co qua canh
        return 'Đi: '.$itinerary_a. ' Về: '.$itinerary_b;

    }else{
        //co qua canh
        $itinerary_a = explode('-', str_replace(' ','', $itinerary_a));
        $itinerary_b = explode('-', str_replace(' ','', $itinerary_b));
        $itinerary_1 = $itinerary_a[0].'-'.$itinerary_b[1].'('.$itinerary_a[1].')';

        $itinerary_c = explode('-', str_replace(' ','', $itinerary_c));
        $itinerary_d = explode('-', str_replace(' ','', $itinerary_d));
        $itinerary_2 = $itinerary_c[0].'-'.$itinerary_d[1].'('.$itinerary_c[1].')';

        return 'Đi: '.$itinerary_1.' Về: '.$itinerary_2;
    }
}

function __approval_request($data, &$result){
    $tour = TourNew::lookup(['id' => (int)$data['tour_id']]);
    if (!$tour) {
        $result['tour'] = 'Tour không tồn tại';
    }

    $reservation = BookingReservation::lookup(['id' => (int)$data['reservation_id']]);
    if (!$reservation) {
        $result['reservation'] = 'Giữ chỗ không tồn tại';
        return false;
    }
    //check booking
    if(isset($reservation->booking_code) && !empty(trim($reservation->booking_code))) {
        $booking_check = Booking::lookup(['booking_code' => trim($reservation->booking_code)]);
        if (!$booking_check)
            $result['booking_code'] = 'Mã booking không tồn tại';
    } else
        $result['booking_code'] = 'Chưa có mã booking';

    if($reservation->hold_qty || (int)$reservation->sure_qty <= 0)
        $result['hold_qty'] = 'Giữ chỗ chưa sure all';

    $data = $reservation->ht;
    $data['status'] = BookingReservation::APPROVAL_REQUEST;

    if (!($id = BookingReservation::update($data)))
        $result['save'] = 'Cannot request';

    if ($result) return false;

    $result['success'] = 1;
    return $id;
}

if (isset($_POST) && isset($_POST['reservation_save']) && isset($_POST['reservation_id']) && (int)$_POST['reservation_id']) {
    echo '<link rel="stylesheet" href="'.ROOT_PATH.'scp/css/scp.css?v=1.10" media="all">';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'js/jquery-1.8.3.min.js"></script>';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'scp/js/custom.js"></script>';
    $reservation_id = 0;

    if (!isset($_POST['tour_id']) && (!isset($_POST['visa_only']) || !$_POST['visa_only'])) {
        ?>
        <div id="msg_error">Thông tin tour không hợp lệ (tour ID)</div>
        <?php
        echo SCRIPT_ERROR;
        exit;
    }

    $result = [];
    try {
        db_start_transaction();
        $reservation_id = ___update($_POST, $result);
    } catch(Exception $ex) {

    }

    if (!isset($result['success']) || !$result['success']) {
        foreach ($result as $_id => $_value) {
            ?>
            <div id="msg_error"><?php echo $_value ?> (<?php echo $_id ?>)</div>
            <?php
        }
        ?>
        <?php
        echo SCRIPT_ERROR;
        db_rollback();
        exit;
    }

    ?>
    <div id="msg_notice">Cập nhật thành công. Trang sẽ tự refresh sau 3 giây.</div>
    <script>
        ost_set_parent_iframe_height();
        parent._reload(<?php echo $reservation_id ?: 0 ?>);
    </script>
    <?php
    db_commit();

    exit;
}
if (isset($_GET) && isset($_GET['reservation_cancel']) && isset($_GET['reservation_id']) && (int)$_GET['reservation_id']) {
    echo '<link rel="stylesheet" href="'.ROOT_PATH.'scp/css/scp.css?v=1.10" media="all">';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'js/jquery-1.8.3.min.js"></script>';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'scp/js/custom.js"></script>';

    $result = [];
    try {
        db_start_transaction();
        $data['id'] = $_GET['reservation_id'];
        $data['reason'] = $_GET['reason'];
        $type = trim($_GET['type'])?:'';
        switch ($type){
            case 'hold':
                ___cancel($data,$result, 'hold');
                break;
            case 'sure':
                ___cancel($data,$result, 'sure');
                break;
            default: ___cancel($data,$result);
                break;
        }
    } catch(Exception $ex) {

    }

    if (!isset($result['success']) || !$result['success']) {
        foreach ($result as $_id => $_value) {
            ?>
            <div id="msg_error"><?php echo $_value ?> (<?php echo $_id ?>)</div>
            <?php
        }
        echo SCRIPT_ERROR;
        db_rollback();
        exit;
    }

    ?>
    <div id="msg_notice">Cập nhật thành công. Trang sẽ tự refresh sau 3 giây.</div>
    <script>
        ost_set_parent_iframe_height();
        setTimeout(function() {
            window.location.href = '<?php echo $cfg->getUrl() ?>scp/reservation.php' + window.location.hash;
            }, 3000
        );
    </script>
    <?php
    db_commit();

    exit;
}

/////// TODO: review
if (isset($_POST) && isset($_POST['action']) && in_array($_POST['action'], ['opreview', 'leaderreview','visareview'])
    && isset($_POST['reservation_id']) && (int)$_POST['reservation_id']) {
    $result = [];
    $name_confirm = '';
    try {
        db_start_transaction();

        $reservation = BookingReservation::lookup(['id' => $_POST['reservation_id']]);
        if (!$reservation) {
            $result['reservation'] = 'Giữ chỗ không tồn tại';
        }

        if (isset($reservation->hold_qty) && $reservation->hold_qty > 0)
            $result['hold_qty'] = 'Phải sure hết mới confirm';

        if (!isset($reservation->sure_qty) || !$reservation->sure_qty)
            $result['sure_qty'] = 'Phải sure hết mới confirm';

        if ($result) throw new Exception('Error');

        switch ($_POST['action']) {
            case 'leaderreview':
                $name_confirm = 'leader_confirm';
                if(!$thisstaff->canApproveLeaderReservation()){
                    $result['permission'] = 'Không thể thực hiện hành động này';
                    throw new Exception('Error');
                }

                if (isset($reservation->status) && (int)$reservation->status <> BookingReservation::APPROVAL_REQUEST) {
                    $result['status'] = 'Không đúng trạng thái';
                    throw new Exception('Error');
                }

                $reservation->setAll(
                    [
                        'status' => BookingReservation::APPROVAL_LEADER,
                        'leader_review_by' => $thisstaff->getId(),
                        'leader_review_at' => new SqlFunction('NOW'),
                    ]
                );
                $reservation->save();

                ?>
                <script>
                    parent.$('iframe[name=leader_confirm_<?php echo $_POST['reservation_id'] ?>]')
                        .parents('td')
                        .find('button')
                        .text('DONE')
                        .removeClass('btn-info')
                        .addClass('btn-default')
                        .prop('disabled', true);
                </script>
                <?php
                break;
            case 'opreview':
                $name_confirm = 'op_confirm';

                if(!$thisstaff->canApproveOperatorReservation()){
                    $result['permission'] = 'Không thể thực hiện hành động này';
                    throw new Exception('Error');
                }

                if (isset($reservation->status) && (int)$reservation->status <> BookingReservation::APPROVAL_VISA) {
                    $result['status'] = 'Không đúng trạng thái';
                    throw new Exception('Error');
                }

                $reservation->setAll(
                    [
                        'status' => BookingReservation::APPROVAL_OPERATOR,
                        'op_review_by' => $thisstaff->getId(),
                        'op_review_at' => new SqlFunction('NOW'),
                    ]
                );
                $reservation->save();

                ?>
                <script>
                    parent.$('iframe[name=op_confirm_<?php echo $_POST['reservation_id'] ?>]')
                       .parents('td')
                       .find('button')
                       .text('DONE')
                       .removeClass('btn-info')
                       .addClass('btn-default')
                       .prop('disabled', true);
//                </script>
                <?php
                break;
            case 'visareview':
                $name_confirm = 'visa_confirm';

                if(!$thisstaff->canApproveVisaReservation()){
                    $result['permission'] = 'Không thể thực hiện hành động này';
                    throw new Exception('Error');
                }

                if (isset($reservation->status) && (int)$reservation->status <> BookingReservation::APPROVAL_LEADER) {
                    $result['status'] = 'Không đúng trạng thái';
                    throw new Exception('Error');
                }

                $reservation->setAll(
                    [
                        'status' => BookingReservation::APPROVAL_VISA,
                        'visa_review_by' => $thisstaff->getId(),
                        'visa_review_at' => new SqlFunction('NOW'),
                    ]
                );
                $reservation->save();

                ?>
                <script>
                    parent.$('iframe[name=visa_confirm_<?php echo $_POST['reservation_id'] ?>]')
                        .parents('td')
                        .find('button')
                        .text('DONE')
                        .removeClass('btn-info')
                        .addClass('btn-default')
                        .prop('disabled', true);
                </script>
                <?php
                break;

        }

        $result['success'] = 1;
    } catch(Exception $ex) {

    }

    if (!isset($result['success']) || !$result['success']) {
    ?>
        <script>
            parent.$('iframe[name=<?php echo $name_confirm.'_'.$_POST['reservation_id'] ?>]')
                .parents('td')
                .find('button')
                .text('ERROR')
                .removeClass('info')
                .addClass('danger')
                .prop('disabled', true);
        </script>
    <?php

        db_rollback();
        exit;
    }
    ?>
    <style>
        body {
            overflow: hidden;
        }
    </style>
    <?php
    db_commit();

    exit;
}

if (isset($_POST) && $_POST) {
    echo '<link rel="stylesheet" href="'.ROOT_PATH.'scp/css/scp.css?v=1.10" media="all">';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'js/jquery-1.8.3.min.js"></script>';
    echo '<script type="text/javascript" src="'.ROOT_PATH.'scp/js/custom.js"></script>';
    $reservation_id = 0;

    if (!isset($_POST['tour_id']) && (!isset($_POST['visa_only']) || !$_POST['visa_only'])) {
        ?>
        <div id="msg_error">Thông tin tour không hợp lệ (tour ID)</div>
        <?php
        echo SCRIPT_ERROR;
        exit;
    }

    if (!isset($_POST['action']) || !$_POST['action']) {
        ?>
        <div id="msg_error">Hành động không hợp lệ (action)</div>
        <?php
        echo SCRIPT_ERROR;
        exit;
    }

    $result = [];
    try {
        db_start_transaction();
        switch ($_POST['action']) {
            case 'hold':
            case 'sure':
                $reservation_id = ___hold($_POST, $result);
                break;
            case 'detail_action':
                if (!isset($_POST['reservation_id']) || !$_POST['reservation_id']
                    || !( $reservation = BookingReservation::lookup( $_POST['reservation_id'] ) )
                    || !isset($reservation->staff_id) || !$reservation->staff_id
                ) {
                    ?>
                    <div id="msg_error">Thông tin tour không hợp lệ</div>
                    <?php
                    echo SCRIPT_ERROR;
                    exit;
                }
                if ($reservation->staff_id !== $thisstaff->getId()) {
                    ?>
                    <div id="msg_error">Không thể cập nhật giữ chỗ của người khác</div>
                    <?php
                    echo SCRIPT_ERROR;
                    exit;
                }

                $reservation_id = ___update($_POST, $result);
                break;

            case 'sure_all':
                $reservation_id = __updateSure($_POST,$result);
                break;
            case 'extend':
                $id = (int)$_POST['reservation_id'];
                $reservation_id = __extend($id,$result);
                break;
            case 'cancel_tour':
                if(!$thisstaff->canCancelTour()){
                    $result['access'] = "Không thể thực hiện hành động này";
                }else {
                    __cancelTour($_POST, $result);
                }
                break;
            case 'transfer_date':
                if(isset($_SESSION['tour_current']) && isset($_SESSION['reservation_current'])) {
                    $reservation_id = (int)$_SESSION['reservation_current'];
                    $tour_transfer = TourNew::lookup((int)$_POST['tour_transfer']);
                    $tour_current = TourNew::lookup((int)$_SESSION['tour_current']);

                    if ($tour_current && $tour_transfer && $tour_current->destination === $tour_transfer->destination
                        && (int)$_POST['tour_transfer'] !== (int)$_SESSION['tour_current']) {
                        $reservation_id = __transferDate($reservation_id, (int)$_POST['tour_transfer'], $result);
                    } else {
                        $result['tour'] = 'Không thể chuyển tour này';
                    }
                    unset($_SESSION['tour_current']);
                    unset($_SESSION['reservation_current']);
                }else{
                    $result['transfer'] = "Có lỗi xảy ra, vui lòng thử lại sau";
                }
                break;
            case 'approval_request':
                $reservation_id = __approval_request($_POST, $result);
                break;
            default:
                ?>
                <div id="msg_error">Hành động không hợp lệ (switch action)</div>
                <?php
                echo SCRIPT_ERROR;
                exit;
                break;
        }
    } catch(Exception $ex) {

    }

    if (!isset($result['success']) || !$result['success']) {
        foreach ($result as $_id => $_value) {
            ?>
            <div id="msg_error"><?php echo $_value ?> (<?php echo $_id ?>)</div>
            <?php
        }
        echo SCRIPT_ERROR;

        db_rollback();
        exit;
    }

    ?>
    <div id="msg_notice">Cập nhật thành công. Trang sẽ tự refresh sau 3 giây.</div>
    <script>ost_set_parent_iframe_height();</script>
    <script>
        if (parent.document.getElementById("<?php echo $_POST['tour_id'] ?>"))
            parent.document.getElementById("<?php echo $_POST['tour_id'] ?>").reset();
        else
            parent.document.getElementById("hold__visa").reset();

        parent._reload(<?php echo $reservation_id ?>);
    </script>
    <?php
    db_commit();

    exit;
}

//only access when have permission
$group_market = $thisstaff->getGroup()->getDestinations();
$group_des = TourDestination::getDestinationFromMarket($group_market);

$country_list = TourMarket::loadList($group_market);

if (isset($_GET) && isset($_GET['reservation_update']) && isset($_GET['reservation_id']) && (int)$_GET['reservation_id']) {
    $reservation = BookingReservation::lookup($_GET['reservation_id']);
    if (!$reservation) exit;
    include_once STAFFINC_DIR.'templates/reservation-update.inc.php';
    exit;
}


if (isset($_REQUEST['from']) && strtotime($_REQUEST['from'])) {
    $from = trim($_REQUEST['from']);
} else {
    $_REQUEST['from'] = date('Y-m-d', time()-24*3600*10);
    $from = date('Y-m-d', time()-24*3600*10);
}

if (isset($_REQUEST['to']) && strtotime($_REQUEST['to'])) {
    $to = trim($_REQUEST['to']);
} else {
    $_REQUEST['to'] = date('Y-m-d', time()+24*3600*60);
    $to = date('Y-m-d', time()+24*3600*60);
}

$reservation_id = isset($_REQUEST['reservation_id']) && (int)trim($_REQUEST['reservation_id'])
    ? (int)trim($_REQUEST['reservation_id']) : null;
if ($reservation_id) {
    $_REQUEST['from'] = '';
    $from = '';
    $_REQUEST['to'] = '';
    $to = '';
}
if(isset($_REQUEST['staff_id_search']) && $_REQUEST['staff_id_search'])
    $id_staffReservation = (int)$_REQUEST['staff_id_search'];
else
    $id_staffReservation = (int)$thisstaff->getId();
$params = [
    'staff_id' => trim($_REQUEST['staff_id_search']),
    'from' => $from,
    'to' => $to,
    'reservation_id' => $reservation_id,
    'tour_name' => trim($_REQUEST['tour_name']),
    'country' => trim($_REQUEST['country']),
    'quantity' => trim($_REQUEST['quantity']),
    'airline' => trim($_REQUEST['airline']),
    'customer_name' => trim($_REQUEST['customer_name']),
    'customer_phonenumber' => trim($_REQUEST['customer_phonenumber']),
    'group_des' => $group_des,
    'group_market' => $group_market
];
if (isset($_GET) && isset($_GET['transfer_date'])) {
    if(!isset($_SESSION['tour_current']))
        $_SESSION['tour_current'] =  (int)$_GET['tour_id'];
    if(!isset($_SESSION['reservation_current']))
        $_SESSION['reservation_current'] = (int)$_GET['reservation_id'];

    $tour = TourNew::lookup(['id' => $_GET['tour_id']]);
    $params['reservation_id'] = '';
    $params['destination'] = (int)$tour->destination;
    $params['transfer_date'] = 1;
    $page_body = 'reservation-transfer.inc.php';
}

if (isset($_REQUEST['tour']) && $_REQUEST['tour'])
    $params['tour'] = $_REQUEST['tour'];

___clear();

$total = 0;
$page = isset($_REQUEST['p']) && (int)$_REQUEST['p'] ? (int)$_REQUEST['p'] : 1;
$offset = ($page-1)*PAGE_LIMIT;
$pageNav = new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('reservation.php', $params);
$showing=$pageNav->showing().' '._N('tours', 'tours', $total);
//$tours = Tour::getPagination($params, $offset, PAGE_LIMIT, $total);
$tours = TourNew::getPaginationReservation($params, $offset, PAGE_LIMIT, $total);
$arr_tour_id = [];
while($tours && ($row = db_fetch_array($tours))){
    $arr_tour_id[] = $row['id'];
}
db_data_seek($tours,0);
$visa_country = BookingReservation::tourWithVisa(0, 0, $params);
$upcoming = BookingReservation::upcoming();
$staffReservation = BookingReservation::staffReservation($id_staffReservation, $arr_tour_id, $params);

$recently = BookingReservation::recently();
$airlines_list_ = DynamicList::lookup(AIRLINE_LIST);
$airlines_list = ListUtil::getList(AIRLINE_LIST);
if($params['customer_name'] || $params['customer_phonenumber'] || $params['staff_id'])
    $search_staff_pax =  BookingReservationHistory::search_staff_pax($params);

$all_staff = Staff::getStaffMembers();
$all_member_staff = get_AllMemberStaff();

if ($offset > $total) {
    $page = ceil($offset/PAGE_LIMIT);
    header('Location: '.$cfg->getBaseUrl().'/scp/reservation.php?page='.$page);
    exit;
}



$nav->setTabActive('booking', 'reservation');
$ost->addExtraHeader('<meta name="tip-namespace" content="reservation.list" />',
                     "$('#content').data('tipNamespace', 'reservation.list');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page_body);
include(STAFFINC_DIR.'footer.inc.php');


<?php
namespace Tugo;

require_once(INCLUDE_DIR . 'class.orm.php');

class PassportPhotoModel extends \VerySimpleModel{
    static $meta = [
        'table' => PASSPORT_PHOTO_TABLE,
        'pk' => ['id'],
    ];
}

class PassportPhoto extends PassportPhotoModel
{
    const PASSPORT_SCAN_WAITING = 0;
    const PASSPORT_SCAN_SUCCESS = 1;
    const PASSPORT_SCAN_FAILURE = -1;

    static public function getPassportUpload($params, $offset, $limit = 30, &$total)
    {
        $sql = 'select ot.name as `tour_name`, ot.id as `tour_id`,pp.id as `passport_id`,pp.booking_code, pp.upload_by, pp.upload_at, pp.status, pp.`result`, pp.filename  
                from pax_passport_photo pp
                join ost_tour ot on pp.tour_id = ot.id
                where 1 ';
        if (isset($params['startTime']) && $params['startTime']) {
            $sql .= ' and date(pp.upload_at) >= ' . db_input($params['startTime']);
        }

        if (isset($params['endTime']) && $params['endTime']) {
            $sql .= ' and date(pp.upload_at) <= ' . db_input($params['endTime']);
        }

        if (isset($params['tour_name']) && $params['tour_name']) {
            $sql .= ' and ot.name like ' . db_input('%' . $params['tour_name'] . '%');
        }

        if (isset($params['booking_code']) && $params['booking_code']) {
            $sql .= ' and pp.booking_code like ' . db_input('%' . $params['booking_code'] . '%');
        }

        if (isset($params['passport_code']) && $params['passport_code']) {
            $sql .= ' and pp.result like ' . db_input('%' . $params['passport_code'] . '%');
        }

        if (isset($params['staff']) && $params['staff']) {
            $sql .= ' and pp.upload_by = ' . $params['staff'];
        }

        if (isset($params['passport_id']) && $params['passport_id']) {
            $sql .= ' and pp.id = ' . (int)$params['passport_id'];
        }

        if (isset($params['tour_id']) && $params['tour_id']) {
            $sql .= ' and ot.id = ' . (int)$params['tour_id'];
        }

        $order_by = ' order by pp.id desc ';
        $limit = " LIMIT $limit OFFSET $offset";
        $total = db_num_rows(db_query($sql));

        $sql = $sql . $order_by . $limit;
        return db_query($sql);
    }

    static public function getFileNamePassport($booking_code, $passport_no, $status = 1){
        $sql = ' Select filename from '.PASSPORT_PHOTO_TABLE;
        $qwhere = ' WHERE 1 ';
        $qwhere .= ' AND status = '.(int)$status;
        if ($booking_code) {
            $_booking_code_trim = str_replace(BOOKING_CODE_PREFIX, '', $booking_code);
            $_booking_code_trim = ltrim($_booking_code_trim, '0');
            $qwhere .= ' AND trim(LEADING \'0\' FROM trim(LEADING \''.BOOKING_CODE_PREFIX.'\' FROM booking_code)) LIKE '.db_input($_booking_code_trim);
        }
        if($passport_no)
            $qwhere .= ' AND result is not null AND result LIKE '. db_input('%' .$passport_no . '%');

        return db_query($sql.$qwhere);
    }

}

class PassportPhotoUploadModel extends \VerySimpleModel{
    static $meta =[
        'table' => PASSPORT_PHOTO_UPLOAD_TABLE,
        'pk' => ['id']
    ];
}
class PassportPhotoUpload extends  PassportPhotoUploadModel{

}

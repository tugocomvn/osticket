<?php
require_once INCLUDE_DIR.'class.staff.php';
use Endroid\QrCode\QrCode;

class QrCodeToken{
    public static function createQrCode( $url , &$error, $token = null){
        if(!$token)
            $token = self::createToken();

        db_start_transaction();
        try {
            $id = self::saveToken($token);
            if (!$id && !$url) {
                throw new Exception('Không thể tạo qrcode lúc này, vui lòng thử lại!');
            }
            $url .= '&token=' . $token;

            $qrCode = new QrCode();
            $qrCode->setText($url)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                // Path to your logo with transparency
                ->setLogoSize(98)
                ->setImageType(QrCode::IMAGE_TYPE_PNG);

            // Send output of the QRCode directly
            header('Content-Type: '.$qrCode->getContentType());
            $qrCode->render();

            db_commit();
            exit();
        }catch (Exception $ex){
            $error = $ex->getMessage();
            db_rollback();
        }
    }

    public static function createAndSaveQrCode( $url , &$error, $token = null){
        global $thisstaff;
        if(!$token)
            $token = self::createToken();

        db_start_transaction();
        try {
            $id = self::saveToken($token);
            if (!$id && !$url) {
                throw new Exception('Không thể tạo qrcode lúc này, vui lòng thử lại!');
            }
            $url .= '&token=' . $token;

            $qrCode = new QrCode($url);

            $file_name = md5(mt_rand(0,9999).$thisstaff->getId().time()).'.png';
            $directory = __DIR__.'/../qr_code/';

            if (! is_dir($directory) && ! mkdir($directory) && ! is_dir($directory))
                $error = 'Directory ' . $directory . ' was not created';

            $destination = $directory.$file_name;
            $qrCode->save($destination);

            db_commit();
            return $file_name;
        }catch (Exception $ex){
            $error = $ex->getMessage();
            db_rollback();
        }
        return '';
    }

    public static function createToken(){
        return hash("sha256", md5(time().microtime()));
    }

    public static function saveToken($token){
        global $thisstaff;

        $data = [
            'staff_id' => $thisstaff->getId(),
            'user_name' => $thisstaff->getUserName(),
            'signin_token' => $token,
            'create_at' => new SqlFunction("NOW"),
            'expired_at' => date('Y-m-d H:i:s', time() + EXPIRE_TOKEN_SECONDS),
        ];

        $new = StaffSigninToken::create($data);
        $id = $new->save();
        if(!$id)
            return 0;
        return $id;
    }
}

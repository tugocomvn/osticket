<?php
/*********************************************************************
dashboard.php

Staff's Dashboard - basic stats...etc.

Peter Rotich <peter@osticket.com>
Copyright (c)  2006-2013 osTicket
http://www.osticket.com

Released under the GNU General Public License WITHOUT ANY WARRANTY.
See LICENSE.TXT for details.

vim: expandtab sw=4 ts=4 sts=4:
 **********************************************************************/
require('staff.inc.php');
$nav->setTabActive('analytics');
$ost->addExtraHeader('<meta name="tip-namespace" content="analytics.ticket_analytics" />',
                     "$('#content').data('tipNamespace', 'analytics.ticket_analytics');");
require(STAFFINC_DIR.'header.inc.php');
?>

<?php if($thisstaff->can_view_ticket_analytics()): ?>
    <?php include INCLUDE_DIR.'staff/dashboard.ticket-analytics.inc.php'; ?>
<?php endif; ?>

<?php
include(STAFFINC_DIR.'footer.inc.php');
?>

<?php
$staff_id = $thisstaff ? $thisstaff->getId() : 0;
$calendar = Ticket::getAllCalendarEntries($staff_id);

?>
<div id="jqxLoader"></div>
<div id='scheduler'></div>

<script type="text/javascript">
    $(document).ready(function () {
        var _add = false;
        if ($("#jqxLoader"))
            $("#jqxLoader").jqxLoader({ text: "", width: 60, height: 60 });

        appointments = JSON.parse('<?php echo json_encode($calendar); ?>');
        // prepare the data
        var source =
            {
                dataType: 'json',
                dataFields: [
                    { name: 'id', type: 'string' },
                    { name: 'content', type: 'string' },
                    { name: 'tooltip', type: 'string' },
                    { name: 'start', type: 'date', format: "yyyy-MM-dd HH:mm" },
                    { name: 'end', type: 'date', format: "yyyy-MM-dd HH:mm" },
                    { name: 'create', type: 'string' }
                ],
                id: 'id',
                localData: appointments
            };
        var adapter = new $.jqx.dataAdapter(source);
        $("#scheduler").jqxScheduler({
            date: new $.jqx.date(),
            width: 936,
            height: 550,
            rowsHeight: 10,
            source: adapter,
            editDialog: false,
            editDialogCreate: function (dialog, fields, editAppointment) {
                // hide repeat option
                fields.repeatContainer.hide();
                // hide status option
                fields.statusContainer.hide();
                // hide timeZone option
                fields.timeZoneContainer.hide();
                // hide color option
                fields.colorContainer.hide();
                fields.resourceLabel.hide();
                fields.locationLabel.hide();
                fields.allDayLabel.hide();
//                fields.descriptionLabel.hide();
                fields.allDayContainer.hide();
//                fields.descriptionContainer.hide();
                fields.locationContainer.hide();
                fields.repeatContainer.hide();
                fields.repeatLabel.hide();
                fields.saveButton.hide();
                fields.deleteButton.hide();
                fields.deleteSeriesButton.hide();
                fields.deleteExceptionsButton.hide();

                fields.subjectLabel.html("Title");
                fields.fromLabel.html("Start");
                fields.toLabel.html("End");
            },
            ready: function () {
                $("#scheduler").jqxScheduler('beginAppointmentsUpdate');
                appointments.forEach(function(elm, index) {
                    $("#scheduler").jqxScheduler('setAppointmentProperty', elm.id, 'resizable', false);
                    $("#scheduler").jqxScheduler('setAppointmentProperty', elm.id, 'draggable', false);
                    $("#scheduler").jqxScheduler('setAppointmentProperty', elm.id, 'readOnly', true);
                });
                $("#scheduler").jqxScheduler('endAppointmentsUpdate');

            },
            appointmentDataFields:
                {
                    from: "start",
                    to: "end",
                    id: "id",
                    subject: "content",
                    tooltip: "tooltip"
                },
            appointmentTooltips: false,
            view: 'weekView',
            views:
                [
                    {
                        type: "dayView",
                        timeRuler: { scaleStartHour: 8, scaleEndHour: 17 },
                        workTime:
                            {
                                fromDayOfWeek: 1,
                                toDayOfWeek: 6,
                                fromHour: 8,
                                toHour: 18
                            }
                    },
                    {
                        type: "weekView",
                        timeRuler: { scaleStartHour: 8, scaleEndHour: 17 },
                        workTime:
                            {
                                fromDayOfWeek: 1,
                                toDayOfWeek: 6,
                                fromHour: 8,
                                toHour: 18
                            }
                    },
                    {
                        type: "monthView",
                        timeRuler: { scaleStartHour: 8, scaleEndHour: 17 },
                        workTime:
                            {
                                fromDayOfWeek: 1,
                                toDayOfWeek: 6,
                                fromHour: 8,
                                toHour: 17
                            }
                    }
                ]
        });

        $("#scheduler").on('appointmentDoubleClick', function (event) {
            event.preventDefault();
            return false;
        });

        $("#scheduler").on('cellDoubleClick', function (event) {
            event.preventDefault();
            return false;
        });

        $("#scheduler").on('cellClick', function (event) {
            event.preventDefault();
            return false;
        });
    });
</script>
